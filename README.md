# <a id="top">e-NON</a>

**This project is the reference implementation reader/writer of the e-NON data format.**  
It includes:
* binary format serializer/deserializer
* text format serializer/deserializer (e-NON-txt)
* intermediate (languate agnostic) "element" representation
* Java data binding 
* JSON reader/writer to/from "element" representation

The e-NON format is specified at https://e-non.org

> e-NON = _(electronic)_ **Network Object Notation:** an object serialization format that supports binary data without string encoding.

e-NON contains the same basic data types as JSON, (map, array, primitives, etc.) but rendered in a more compact form and allowing raw binary data.  Binary encoded types are written in network byte order.

Extensions to the e-NON format have been defined to suit common (and uncommon) data serialization needs.

## <a id="contents">Contents</a>
* [Version](#version)
* [Purpose](#purpose)
* [Nomenclature](#nomenclature)
* [Quick Start](#quick-start)
	* [Java 8](#java-8)
* [Optional Java Dependencies](#optional-java-dependencies)
	* [Project Reactor (Flux)](#project-reactor-flux)
	* [ReactiveX](#reactivex)
	* [JCache](#jcache)
* [Feature Sets](#feature-sets)
	* [e-NON-0 (zero)](#e-non-0-zero)
	* [e-NON-G](#e-non-g)
	* [e-NON-M](#e-non-m)
	* [e-NON-N](#e-non-n)
	* [e-NON-S](#e-non-s)
	* [e-NON-X](#e-non-x)
	* [e-NON-Y](#e-non-y)
	* [e-NON-Z](#e-non-z)
* [Use Cases](#use-cases)<div style="font-size:smaller;">
	* [Proprietary Storage](#proprietary-storage)
	* [Streaming](#streaming)
	* [Crypto & Compression](#crypto-and-compression)
	* [Homogeneous Data Sets](#homogeneous-data-sets)

## <a>Version</a>
The e-NON version described in this document is **0** _(zero)_.

## <a>Purpose</a>
The **e-NON** format is intended as a more efficient and flexible data transfer format compared to JSON and other text-based formats such as XML.

JSON has been a huge success in the software industry, but it has limitations that make it difficult to use for some applications.  In particular, it is inefficient with binary data.

Protocols more "binary-friendly" than HTTP are gaining acceptance.  e-NON was inspired as a means of encoding object data in the RSocket payload.

[(top)](#top)
## <a>Nomenclature</a>
The following names for the e-NON format and processing system are acceptable:

* **e-NON**    
The domain name e-non.org has been registered for the e-NON project.  Using the name in this form reinforces that identity.  "e-NON" is used in this document.

* **eNON**   
This more compact version is natural considering that many of us have transitioned from "e-mail" to "email".

* **.enon**   
This should be the extension used for files containing e-NON data.

* Coding Conventions   
In code, the name should remain consistent with coding conventions.  Depending on the language and situation, these may be appropriate:
	* **enon** for Java packages, variable and method identifier prefixes, etc.
	* **Enon** for Java class names, mid-sections of camel-case identifiers, etc.
	* **ENON** i.e. for static constants

[(top)](#top)

## <a>Quick Start</a>
### <a>Java</a>
This version uses the `java.util.streams` API in combination with `java.util.function.Consumer` to support a functional programming style.

> _Due to the serial nature of the data stream, e-NON readers and writers do not enable parallel operation.  However, the implementation is thread-safe, so multiple readers and writers can be operating concurrently on different e-NON streams._

#### Maven
~~~xml
<dependency>
  <groupId>org.e-non</groupId>
  <artifactId>e-non</artifactId>
  <version>0.2-SNAPSHOT</version>
</dependency>
~~~
#### Writer
~~~java
import org.enon.Writer;
// ...
// write an object to an OutputStream
Writer writer = new Writer(outputStream);

// serialize an object to the stream
writer.write(object);

// closes the outputStream
writer.close();
~~~
#### Reader
~~~java
import org.enon.Reader;
// ...
// read an object from an InputStream
Reader reader = new Reader(inputStream);

// deserialize an object from the stream
MyThing thing1 = reader.read(MyThing.class);

// closes the inputStream
reader.close();
~~~

[(top)](#top)

## <a>Optional Java Dependencies</a>
The core implementation requires no external dependencies.  However, if the following libraries are provided, additional features become enabled.

### <a>Project Reactor (Flux)</a>
Readers and writers with Flux-based streams can be used.
~~~xml
<dependency>
  <groupId>io.projectreactor</groupId>
  <artifactId>reactor-core</artifactId>
  <version>3.2.2.RELEASE</version>
</dependency>
~~~

### <a>ReactiveX</a>
Readers and writers with Rx Observable streams can be used.
~~~xml
<dependency>
  <groupId>com.netflix.rxjava</groupId>
  <artifactId>rxjava-core</artifactId>
  <version>0.20.7</version>
</dependency>
~~~

[(top)](#top)

## <a>Feature Sets</a>
### <a>e-NON-0 _(zero)_</a>
e-NON-0 is the minimum feature set that all implementations have to support. 

* Basic data types are available:
**`null, false, true, nano-int, int, double, string, number, byte[], list, map`**

* Only one top-level (root) entry is allowed.  Any of the supported types can be the root entry.

> These data types are widely supported in many programming languages.  e-NON-0 is analogous to JSON plus binary support for int, double and byte[].

|type|description|
|---|---|
|null|Represents a null value
|false, true|Boolean values
|nano-int|1-byte numeric value (binary)
|int|4-byte numeric value (binary)
|double|IEEE 754 double-precision "binary64" value, 8 bytes (binary)
|string|UTF-8 encoded string
|number|arbitrary numeric value, encoded as a UTF-8 string
|byte[]|data block of arbitrary length (binary)
|list|an ordered collection of any of these data types
|map|an unordered collection of key-value pairs, in which the key and value may be any of these types

### <a>e-NON-G</a>
e-NON-G adds support for the _**G**lossary_ and _map references_. 

### <a>e-NON-M</a>
e-NON-M adds support for _**M**etadata_. 

### <a>e-NON-N</a>
e-NON-N adds support for large **N**umbers and alternate floating point representations.

* Data types:
	**`big int, big decimal`**

### <a>e-NON-S</a>
e-NON-S adds support for _**S**treaming_ and chunked data blocks.

* Unbounded _map_ and _list_ sizes are supported.

* Multiple top-level (root) entries are allowed.

* Chunked records are supported.

### <a>e-NON-X</a>
e-NON-X is _e**X**tended_ e-NON, adding more data types and features:

* Extended data types:
	**`byte, char, short, long, float, NaN, +/-infinity, temporal, array, bitset`**
	
> e-NON-X adds more data types in binary form.  This adds efficiency for languages that have native support for these types.

|type|description
|---|---|
|byte|1-byte numeric value (binary)
|char|UTF-8 encoded char value (binary)
|short|2-byte numeric value (binary)
|long|8-byte numeric value (binary)
|float|IEEE 754 single-precision "binary32" value, 4 bytes (binary)
|NaN, +/-infinity|1-byte codes to represent these special values
|temporal|compact encoding of temporal (time, date, duration, etc.) structures (binary)
|array|compact ordered lists of fixed-length "primitive" types: byte, short, int, long, float, double (binary)
|bitset|special representation for boolean[] array values, 1-bit per value (binary)

### <a>e-NON-Y</a>
e-NON-Y adds support for encr**Y**ption.

### <a>e-NON-Z</a>
e-NON-Z adds support for g**Z**ip compression.

[(top)](#top)

---
&copy; _2018-2020 Giant Head Software, LLC_