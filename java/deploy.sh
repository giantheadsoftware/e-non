#!/bin/bash

WD=`pwd`

for PROJECT in e-non e-non-json ; do
	cd $WD/$PROJECT
	echo `pwd`
	echo ; echo "Building jdk8 artifacts for $PROJECT..."
	mvn clean install -Pjava8
	
	if [ $? -eq 0 ] ; then
		echo ; echo "Build with deploy to OSSRH repo for $PROJECT..."
		mvn deploy -Pcentral,ossrh
			if [ $? -ne 0 ] ; then
				return -1
			fi
	fi
done
