/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

module org.enon {
	exports org.enon;
	exports org.enon.bind.property;
	exports org.enon.bind.property.pojo;
	exports org.enon.bind.temporal;
	exports org.enon.bind.value;
	exports org.enon.cache;
	exports org.enon.codec;
	exports org.enon.codec.coder;
	exports org.enon.codec.decoder;
	exports org.enon.element;
	exports org.enon.element.array;
	exports org.enon.element.meta;
	exports org.enon.element.reader;
	exports org.enon.element.writer;
	exports org.enon.event;
	exports org.enon.exception;
	exports org.enon.stream.java8;
	exports org.enon.tool;
	exports org.enon.tool.validate;
	exports org.enon.txt;
	exports org.enon.util;
	exports org.enon.xform;
	
	requires java.desktop;
}
