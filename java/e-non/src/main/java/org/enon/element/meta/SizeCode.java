/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.meta;

import org.enon.exception.SizeException;

/**
 *
 * @author clininger $Id: $
 */
public enum SizeCode {
	SMALL(null, 250L),
	MEDIUM((byte) 0xFF, 0x0FFFFL), // max unsigned short, 2^16 - 1
	LARGE((byte) 0xFE, Long.MAX_VALUE),
	UNBOUNDED((byte) 0xFD, null);

	public final Byte code;
	private final Long max;

	/**
	 * Constructor
	 *
	 * @param code byte code
	 * @param max max value allowed for this code. Make sure it is not negative.
	 */
	private SizeCode(Byte code, Long max) {
		this.code = code;
		this.max = max;
	}

	public static SizeCode forSize(Long size) {
		if (size == null || size < 0) {
			return UNBOUNDED;
		}
		for (SizeCode code : SizeCode.values()) {
			if (code.max != null && size <= code.max) {
				return code;
			}
		}
		throw new SizeException("Invalid size: " + String.valueOf(size));
	}

	public static SizeCode forPrefix(byte prefix) {
		if (Byte.toUnsignedInt(prefix) <= SMALL.max) {
			return SMALL;
		}

		switch (prefix) {
			case (byte) 0xFF:
				return MEDIUM;

			case (byte) 0xFE:
				return LARGE;

			case (byte) 0xFD:
				return UNBOUNDED;

			default:
				return null;
		}
	}
}
