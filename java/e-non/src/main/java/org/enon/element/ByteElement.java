/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import java.io.OutputStream;
import org.enon.ReaderContext;
import org.enon.element.reader.BinaryElementReader;

/**
 *
 * @author clininger $Id: $
 */
public class ByteElement extends FixedSizeElement implements ScalarElement, NumericElement {

	public static final int CONTENT_LENGTH = 1;

	protected final byte value;
	private byte[] contentBytes;

	public ByteElement(byte value) {
		this(ElementType.BYTE_ELEMENT, value);
	}
	
	protected ByteElement(ElementType type, byte value) {
		super(type, CONTENT_LENGTH);
		this.value = value;
	}

	@Override
	public Byte getValue() {
		return value;
	}

	@Override
	public String toString() {
		return super.toString() + "(" + value + ")";
	}

	@Override
	public byte[] getContentBytes() {
		if (contentBytes == null) {
			contentBytes = new byte[]{value};
		}
		return contentBytes;
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends FixedSizeElement.Writer<ByteElement> {

		@Override
		protected void writeContent(ByteElement element, OutputStream out) throws IOException {
			out.write(element.value);
		}

	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		@Override
		public ByteElement read(ReaderContext ctx) throws IOException {
			return new ByteElement(ctx.getDataInputStream().readByte());
		}

	}
}
