/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import java.io.OutputStream;
import org.enon.ReaderContext;
import org.enon.element.reader.BinaryElementReader;
import org.enon.util.SerializeUtil;

/**
 *
 * @author clininger $Id: $
 */
public class LongElement extends FixedSizeElement implements ScalarElement, NumericElement {

	public static final int CONTENT_LENGTH = 8;

	private final long value;
	private byte[] contentBytes;

	public LongElement(long value) {
		super(ElementType.LONG_ELEMENT, CONTENT_LENGTH);
		this.value = value;
	}

	@Override
	public Long getValue() {
		return value;
	}

	@Override
	public byte[] getContentBytes() {
		if (contentBytes == null) {
			contentBytes = SerializeUtil.getInstance().networkBytes(value);
		}
		return contentBytes;
	}

	@Override
	public String toString() {
		return super.toString() + "(" + value + ")";
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends FixedSizeElement.Writer<LongElement> {

		@Override
		protected void writeContent(LongElement element, OutputStream out) throws IOException {
			out.write(element.getContentBytes());
		}

	}

	//////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		@Override
		public LongElement read(ReaderContext ctx) throws IOException {
			return new LongElement(ctx.getDataInputStream().readLong());
		}

	}
}
