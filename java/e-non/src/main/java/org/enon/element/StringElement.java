/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.MetaElement;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.element.meta.MetaSize;
import org.enon.element.reader.BinaryElementReader;
import org.enon.element.writer.BinaryElementWriter;

/**
 *
 * @author clininger $Id: $
 */
public class StringElement extends AbstractElement implements ScalarElement {

	protected final String value;
	private byte[] contentBytes;

	public StringElement(String value) {
		this(value, null);
	}

	public StringElement(char value) {
		this(String.valueOf(value), null);
	}

	public StringElement(char[] value) {
		this(String.valueOf(value), null);
	}

	public StringElement(String value, List<MetaElement> meta) {
		super(ElementType.STRING_ELEMENT, value.getBytes(CHARSET).length, meta);
		this.value = value;
	}

	protected StringElement(ElementType type, String value, List<MetaElement> meta) {
		super(type, value.getBytes(CHARSET).length, meta);
		this.value = value;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public byte[] getContentBytes() {
		if (contentBytes == null) {
			contentBytes = value.getBytes(CHARSET);
		}
		return contentBytes;
	}

	@Override
	public String toString() {
		final int maxLen = 20;
		int len = Math.min(maxLen, value.length());
		return super.toString() + "(\"" + value.substring(0, len)
				+ (value.length() > maxLen ? "...\"" : "\")");
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends BinaryElementWriter<StringElement> {

		@Override
		protected void writeContent(StringElement element, OutputStream out) throws IOException {
			out.write(element.getContentBytes());
		}

	}

	/////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		@Override
		public StringElement read(ReaderContext ctx) throws IOException {
			MetaSize metaSize = MetaSize.READER.read(ctx.getDataInputStream());
			byte[] bytes = readBytes(metaSize.getSize(), ctx.getDataInputStream());
			return new StringElement(new String(bytes), metaSize.getMetaElements());
		}

	}
}
