/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.Size;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.enon.FeatureSet;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.cache.EnonCache;
import org.enon.cache.EnonCacheFactory;
import org.enon.element.meta.MetaElement;

/**
 * The MapElement is implemented as an extension of LIstElement.  The only difference in the serialization is that MapElement entries are
 * read/written in pairs.
 *
 * @author clininger $Id: $
 * @param <K> The key element type
 * @param <V> The value element type
 */
public class MapElement<K extends Element, V extends Element> extends ListElement<MapEntryElement<K, V>> {

	private Size myMapId = Size.SIZE_ZERO;
	private EnonCache<String, Long> mapIds;

	public MapElement(Collection<MapEntryElement<K, V>> entries) {
		this(entries, null);
	}

	public MapElement(Collection<MapEntryElement<K, V>> entries, List<MetaElement> metaList) {
		super(ElementType.MAP_ELEMENT, entries, metaList);
	}

	public MapElement(int capacity) {
		this(capacity, null);
	}

	public MapElement(int capacity, List<MetaElement> metaList) {
		super(ElementType.MAP_ELEMENT, capacity, metaList);
	}

	public MapElement(Map<K, V> map) {
		this(map, null);
	}

	public MapElement(Map<K, V> map, List<MetaElement> metaList) {
		this(map.entrySet().stream().map(MapEntryElement::new).collect(Collectors.toList()), metaList);
	}
	
	protected MapElement(ElementType type, Map<K, V> map, List<MetaElement> metaList) {
		super(type, map.entrySet().stream().map(MapEntryElement::new).collect(Collectors.toList()), metaList);
	}

	@Override
	public MapElement inContainer(ElementContainer containerElement) {
		super.inContainer(containerElement);
		return this;
	}

	/**
	 * Provide the entries in map form
	 * @return A Map instance
	 */
	public Map<K, V> getMap() {
		return entries.stream().collect(Collectors.toMap(e -> e.getValue().getKey(), e-> e.getValue().getValue(), (t, u) -> u, () -> new LinkedHashMap<>()));
	}

	@Override
	public EnonCache<String, Long> getMapIds() {
		if (mapIds != null) {
			return mapIds;
		}
		if (getConfig().hasFeature(FeatureSet.G)) {
			mapIds = EnonCacheFactory.getInstance().create(UUID.randomUUID().toString(), String.class, Long.class);
		}
		return mapIds;
	}

	public Size getMyMapId() {
		return myMapId;
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends ListElement.Writer<MapElement<Element, Element>> {

		@Override
		public void write(MapElement<Element, Element> mapElement) {
			cacheMapId(mapElement, mapElement.getWriterContext());
			super.write(mapElement);
			if (mapElement.mapIds != null) {
				mapElement.mapIds.close();
			}
		}

		@Override
		protected void writeHeader(MapElement element, OutputStream out) throws IOException {
			super.writeHeader(element, out);
			writeMapId(element, out);
		}

		protected void writeMapId(MapElement element, OutputStream out) throws IOException {
			if (element.getConfig().hasFeature(FeatureSet.G)) {
				element.myMapId.write(out);
			}
		}

		protected Size cacheMapId(MapElement element, WriterContext ctx) {
			EnonCache<String, Long> cache = element.getMapIds();
			if (cache != null) {
				element.myMapId = ctx.getIdGenerator().nextId();
				cache.put(element.elementId, element.myMapId.getSize());
			}
			return element.myMapId;
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	public static class Reader extends ListElement.Reader {
		private final MapEntryElement.Reader entryReader = new MapEntryElement.Reader();

		@Override
		protected ListElement newElement(int size, List<MetaElement> meta) {
			return new MapElement(size, meta);
		}

		@Override
		protected MapEntryElement parseNextEntry(ReaderContext ctx) throws IOException {
			return entryReader.read(ctx);
		}

	}
}
