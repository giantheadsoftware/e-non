/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.meta;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.enon.exception.SizeException;
import org.enon.util.SerializeUtil;

/**
 *
 * @author clininger $Id: $
 */
public class Size {

	public static final Size SIZE_ZERO = new Size(0L);
	public static final Reader READER = new Reader();

	private final long size;
	private final SizeCode code;
	private final SerializeUtil serializeUtil = SerializeUtil.getInstance();

	/**
	 * Create a size.
	 *
	 * @param size Muse be positive
	 * @throws SizeException For negative numbers
	 */
	public Size(long size) {
		this.size = size;
		code = SizeCode.forSize(size);
	}

	private Size(SizeCode code, long size) {
		this.size = size;
		this.code = code;
	}

	public long getSize() {
		return size;
	}

	public SizeCode getCode() {
		return code;
	}

	public void write(OutputStream out) throws IOException {
		switch (code) {
			case SMALL:
				out.write((byte)size);
				break;
			case MEDIUM:
				out.write(code.code);
				out.write(serializeUtil.networkBytes((short) size));
				break;
			case LARGE:
				out.write(code.code);
				out.write(serializeUtil.networkBytes(size));
				break;
			case UNBOUNDED:
				out.write(SizeCode.UNBOUNDED.code);
				break;
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	public static class Reader {

		/**
		 * Read the size from the stream. Use this method when the prefix has not already been read from the stream.
		 *
		 * @param in eNON stream
		 * @return The size if valid size prefix. Otherwise, null.
     * @throws IOException on io error
		 */
		public Size read(DataInputStream in) throws IOException {
			byte sizeCodePrefix = in.readByte();
			return read(sizeCodePrefix, in);
		}

		/**
		 * Read the size from the stream. Use this method when the prefix has already been read from the stream.
		 *
		 * @param prefix The SizeMeta prefix
		 * @param in eNON stream
		 * @return The size if valid size prefix. Otherwise, null.
     * @throws IOException on io error
		 */
		public Size read(byte prefix, DataInputStream in) throws IOException {
			SizeCode code = SizeCode.forPrefix(prefix);
			if (code != null) {
				switch (code) {
					case SMALL:
						return new Size(SizeCode.SMALL, Byte.toUnsignedLong(prefix));
					case MEDIUM:
						return new Size(SizeCode.MEDIUM, in.readUnsignedShort());
					case LARGE:
						return new Size(SizeCode.LARGE, in.readLong());
					case UNBOUNDED:
						return new Size(SizeCode.UNBOUNDED, -1L);
				}
			}
			return null;
		}
	}

	@Override
	public String toString() {
		return "Size[" + code.name() + "]" + size;
	}

}
