/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.element.ByteElement;
import org.enon.element.ElementType;
import org.enon.element.ScalarElement;
import org.enon.element.meta.MetaElement;
import org.enon.element.meta.MetaSize;
import org.enon.element.reader.BinaryElementReader;

/**
 *
 * @author clininger $Id: $
 */
public class ByteArrayElement extends ArrayElement implements ScalarElement {

	protected final byte[] array;

	public ByteArrayElement(byte[] array) {
		this(array, null);
	}

	public ByteArrayElement(byte[] array, List<MetaElement> metaList) {
		super(ArrayElementSubtype.BYTE_SUB_ELEMENT, array.length, ByteElement.CONTENT_LENGTH, metaList);
		this.array = array;
		this.contentBytes = array;
	}
	
	protected ByteArrayElement(ArrayElementSubtype entryType, byte[] array, List<MetaElement> metaList) {
		super(entryType, array.length, ByteElement.CONTENT_LENGTH, metaList);
		this.array = array;
		this.contentBytes = array;
	}

	@Override
	public ElementType getType() {
		return subType == ArrayElementSubtype.BYTE_SUB_ELEMENT ? ElementType.BLOB_ELEMENT : type;
	}

	@Override
	public byte[] getValue() {
		return array;
	}

	@Override
	public byte[] getContentBytes() {
		return array;
	}

	@Override
	public List<? extends ByteElement> asElements() {
		ArrayList<ByteElement> elements = new ArrayList<>(array.length);
		for (byte entry : array) {
			elements.add(new ByteElement(entry));
		}
		return elements;
	}

	@Override
	protected ArrayElementWriter getBinaryWriter() {
		return new Writer();
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends ArrayElementWriter<ByteArrayElement> {

		@Override
		protected void writeHeader(ByteArrayElement element, OutputStream out) throws IOException {
			// always use the 1-byte BLOB prefix rather than the 2-byte array prefix
			out.write(ElementType.BLOB_ELEMENT.prefix);
			writeSizeMeta(element, out);
		}

		@Override
		protected void writeContent(ByteArrayElement element, OutputStream out) throws IOException {
			out.write(element.array);
		}
	}

	///////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		@Override
		public ByteArrayElement read(ReaderContext ctx) throws IOException {
			MetaSize metaSize = MetaSize.READER.read(ctx.getDataInputStream());
			byte[] bytes = readBytes(metaSize.getSize(), ctx.getDataInputStream());
			return new ByteArrayElement(bytes, metaSize.getMetaElements());
		}

	}
}
