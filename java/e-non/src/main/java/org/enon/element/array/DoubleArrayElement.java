/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.element.DoubleElement;
import org.enon.element.meta.MetaElement;
import org.enon.element.meta.MetaSize;
import org.enon.element.reader.BinaryElementReader;
import org.enon.util.SerializeUtil;

/**
 *
 * @author clininger $Id: $
 */
public class DoubleArrayElement extends ArrayElement {

	private final double[] array;

	public DoubleArrayElement(double[] array) {
		this(array, null);
	}

	public DoubleArrayElement(double[] array, List<MetaElement> metaList) {
		super(ArrayElementSubtype.DOUBLE_SUB_ELEMENT, array.length, DoubleElement.CONTENT_LENGTH, metaList);
		this.array = array;
	}

	@Override
	public double[] getValue() {
		return array;
	}

	@Override
	public List<DoubleElement> asElements() {
		ArrayList<DoubleElement> elements = new ArrayList<>(array.length);
		for (double entry : array) {
			elements.add(new DoubleElement(entry));
		}
		return elements;
	}

	@Override
	protected ArrayElementWriter getBinaryWriter() {
		return new Writer();
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends ArrayElementWriter<DoubleArrayElement> {

		@Override
		protected void writeContent(DoubleArrayElement element, OutputStream out) throws IOException {
			SerializeUtil serializeUtil = SerializeUtil.getInstance();
			for (double value : element.array) {
				out.write(serializeUtil.networkBytes(value));
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		@Override
		public DoubleArrayElement read(ReaderContext ctx) throws IOException {
			MetaSize metaSize = MetaSize.READER.read(ctx.getDataInputStream());
			double[] array = new double[(int)metaSize.getSize().getSize()];
			for (int i = 0; i < array.length; i++) {
				array[i] = ctx.getDataInputStream().readDouble();
			}
			return new DoubleArrayElement(array, metaSize.getMetaElements());
		}

	}
}
