/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.Size;
import org.enon.element.meta.MetaElement;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.enon.EnonContext;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public interface Element {

	Charset CHARSET = StandardCharsets.UTF_8;

	/**
	 * @return the element's type
	 */
	ElementType getType();

	/**
	 * @return the element's size
	 */
	Size getSize();

	/**
	 * @return the data object wrapped by this Element.
	 */
	Object getValue();

	/**
	 * @return the list of metadata or this element
	 */
	List<MetaElement> getMeta();

	/**
	 * Set the container for this element.
	 *
	 * @param <E> the element type returned by this method, varies by subclass
	 * @param containerElement The container element which is the immediate parent of this element
	 * @return this element
	 */
	<E extends Element> E inContainer(ElementContainer containerElement);

	/**
	 * @return the previously set container element
	 */
	ElementContainer getContainer();

	/**
	 * Return true if the direct parent of this element is the RootContainer.
	 * @return true if this element is at the top level.
	 */
	default boolean isRoot() {
		return getContainer() instanceof RootContainer;
	}

	/**
	 * add metadata to this element
	 * @param <E> this element's type
	 * @param meta The meta element to add
	 * @return this element
	 */
	<E extends Element> E addMeta(MetaElement meta);

	String getElementId();

	/**
	 * Get the context associated with this element.  The context contains the config and other properties of the current read or write session.
	 * The context is assigned to the RootContainer.  Other elements gain access to the context by following the container hierarchy to the root.
	 * This method returns the context regardless of why type it is, reader or writer.
	 * @return The current context
	 */
	EnonContext getContext();

	/**
	 * If the current context is a ReaderContext, return it.
	 * @return The ReaderContext if the current session is reading.
	 * @throws EnonReadException if the context is not a ReaderContext
	 */
	ReaderContext getReaderContext();

	/**
	 * If the current context is a WriterContext, return it.
	 * @return The WriterContext if the current session is writing.
	 * @throws EnonReadException if the context is not a WriterContext
	 */
	WriterContext getWriterContext();
}
