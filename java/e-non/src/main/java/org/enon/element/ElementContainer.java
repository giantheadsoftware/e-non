/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.Size;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.enon.cache.EnonCache;
import org.enon.element.meta.MetaElement;
import org.enon.exception.EnonElementException;

/**
 * A ContainerElement is one that contains other elements.  It is the opposite of ScalarElement.
 * @author clininger $Id: $
 * @param <E> The element type contained within the container
 */
public interface ElementContainer<E extends Element> {

	/**
	 * Get the parent container, if any.
	 * @return Parent container or null.
	 */
	ElementContainer getContainer();

	/**
	 * Return the internal collection of elements.
	 * @return element collection
	 */
	default Collection<E> getContents() {
		return Collections.EMPTY_LIST;
	}

	/**
	 * The map-id cache will be maintained in a top-level map element, if at all. List elements should pass this request to their containers.
	 * The RootElement should return null. Only a top-level MapElement should return a value.
	 *
	 * @return the cache that maintains the map-id values or null
	 */
	default EnonCache<String, Long> getMapIds() {
		return null;
	}

	/**
	 * Set all of the elements to be in this container.
	 * @param containedElements collection of elements that should be in this container
	 * @return the same containedElements collection
	 */
	default Collection<E> applyContainer(Collection<E> containedElements) {
		containedElements.forEach(element -> element.inContainer(this));
		return containedElements;
	}

	/**
	 * Add an element to the container. The container of the element is set to this container.
	 * @param <CE> The type of this container element
	 * @param element The element to add
	 * @return this container
	 */
	default <CE extends ElementContainer<E>> CE add(E element) {
		element.inContainer(this);
		getContents().add(element);
		return (CE) this;
	}

	/**
	 * Return the declared number of elements in the container.  This is the size that was passed in to the 	constructor,
	 * and may differ from the number of elements in the internal collection.
 * @return the declared number of elements in the container.
	 */
	default Size getSize() {
		return Size.SIZE_ZERO;
	}

	/**
	 * Verify that the number of elements in the collection matches the size that was declared in the constructor.
	 */
	default void validateCapacity() {
		if (getSize().getSize() != getContents().size()) {
			throw new EnonElementException("Container element does not have the correct number of elements");
		}
	}

	/**
	 * Get the metadata associated with this container.  By default, this is the EMPTY_LIST.
	 * @return EMPTY_LIST
	 */
	default List<MetaElement> getMeta() {
		return Collections.EMPTY_LIST;
	}
}
