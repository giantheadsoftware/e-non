/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.writer;

import java.io.IOException;
import java.io.OutputStream;
import org.enon.element.Element;
import org.enon.element.meta.MetaElement;
import org.enon.exception.EnonWriteException;

/**
 *
 * @author clininger $Id: $
 * @param <E> The element type written by this writer
 */
public abstract class BinaryElementWriter<E extends Element> implements ElementWriter<E> {

	@Override
	public void write(final E element) {
		try {
			OutputStream out = element.getWriterContext().getOutputStream();
			writeHeader(element, out);
			writeContent(element, out);
		} catch (IOException x) {
			throw new EnonWriteException("Failed to write " + element.getType(), x);
		}
	}

	protected void writeHeader(E element, OutputStream out) throws IOException {
		out.write(element.getType().prefix);
		writeSizeMeta(element, out);
	}

	protected void writeContent(E element, OutputStream out) throws IOException {
		// default implementation writes no content
	}

	protected void writeSizeMeta(E element, OutputStream out) throws IOException {
		if (element.getMeta() != null) {
			for (MetaElement meta : element.getMeta()) {
				meta.write(out);
			}
		}
		element.getSize().write(out);
	}
}
