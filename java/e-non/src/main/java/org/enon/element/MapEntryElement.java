/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.element.reader.BinaryElementReader;
import org.enon.element.writer.BinaryElementWriter;

/**
 * This element contains a key-value pair entered into a MapElement
 *
 * @author clininger $Id: $
 * @param <K> Element type of the key
 * @param <V> Element type of the value
 */
public class MapEntryElement<K extends Element, V extends Element> extends AbstractElement {

	private final K key;
	private final V value;
	private Map.Entry<K, V> entry;

	public MapEntryElement(K key, V value) {
		super(ElementType.MAP_ENTRY_ELEMENT, key.getSize().getSize() + value.getSize().getSize());
		this.key = key;
		this.value = value;
	}

	public MapEntryElement(Map.Entry<K, V> entry) {
		this(entry.getKey(), entry.getValue());
		this.entry = entry;
	}

	@Override
	public MapEntryElement<K, V> inContainer(ElementContainer containerElement) {
		super.inContainer(containerElement);
		key.inContainer(containerElement);
		value.inContainer(containerElement);
		return this;
	}

	@Override
	public Map.Entry<K, V> getValue() {
		if (entry == null) {
			entry = createEntry();
		}
		return entry;
	}

	public K getEntryKey() {
		return getValue().getKey();
	}

	public V getEntryValue() {
		return getValue().getValue();
	}

	protected Map.Entry<K, V> createEntry() {
		return new Map.Entry<K, V>() {
			@Override
			public K getKey() {
				return key;
			}

			@Override
			public V getValue() {
				return value;
			}

			@Override
			public V setValue(V value) {
				throw new UnsupportedOperationException("This entry is immutable");
			}
		};
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends BinaryElementWriter<MapEntryElement> {

		@Override
		protected void writeHeader(MapEntryElement element, OutputStream out) throws IOException {
			// no header to write
		}

		@Override
		protected void writeContent(MapEntryElement element, OutputStream out) throws IOException {
			WriterContext ctx = element.getWriterContext();
			ctx.write(element.key);
			ctx.write(element.value);
		}

	}

	//////////////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		/**
		 * Read the next 2 elements and interpret them as a key-value map entry.
		 * @return new MapEntryElement
		 * @throws IOException on io error
		 */
		@Override
		public MapEntryElement read(ReaderContext ctx) throws IOException {
			Element key = ctx.parseNextElement();
			Element value = ctx.parseNextElement();
			return new MapEntryElement(key, value);
		}

	}
}
