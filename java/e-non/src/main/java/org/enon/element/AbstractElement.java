/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.Size;
import org.enon.element.meta.MetaElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.enon.EnonConfig;
import org.enon.EnonContext;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.exception.EnonException;

/**
 *
 * @author clininger $Id: $
 */
public abstract class AbstractElement implements Element {

	protected final String elementId = null; //UUID.randomUUID().toString();
	protected final ElementType type;
	private Size size;
	private List<MetaElement> metaList;
	private ElementContainer containerElement;
	private RootContainer rootContainer;
	private boolean isRoot;
	private EnonContext ctx;

	protected AbstractElement(ElementType type, long size) {
		this.type = type;
		this.size = new Size(size);
	}

	protected AbstractElement(ElementType type, long size, List<MetaElement> metaList) {
		this(type, size);
		this.metaList = metaList;
	}

	@Override
	public String getElementId() {
		return elementId;
	}

	@Override
	public AbstractElement inContainer(ElementContainer containerElement) {
		this.containerElement = containerElement;
		if (containerElement instanceof RootContainer) {
			this.isRoot = true;
			rootContainer = (RootContainer) containerElement;
		} else {
			// try to locate the root container, but don't fail if the path doesn't exist yet
			rootContainer = pathToRoot();
		}
		return this;
	}

	@Override
	public ElementContainer getContainer() {
		return containerElement;
	}

	@Override
	public boolean isRoot() {
		return isRoot;
	}

	@Override
	public ElementType getType() {
		return type;
	}

	@Override
	public Size getSize() {
		return size;
	}

	@Override
	public List<MetaElement> getMeta() {
		List<MetaElement> allMeta = metaList != null ? new ArrayList<>(metaList) : new ArrayList<>();
		allMeta.addAll(getContainerMeta());
		return allMeta;
	}

	protected List<MetaElement> getContainerMeta() {
		return containerElement != null ? containerElement.getMeta() : Collections.EMPTY_LIST;
	}

	@Override
	public AbstractElement addMeta(MetaElement meta) {
		if (meta == null) {
			return this;
		}
		if (metaList == null) {
			metaList = new LinkedList<>();
		}
		metaList.add(meta);
		return this;
	}

	/**
	 * Get the RootContainer at the top level.  The RootContainer provides access to the the context, config, etc.
	 * @return The RootContainer instance that ultimately contains this element.
	 * @throws EnonException if there is no path to a root container
	 */
	public RootContainer getRootContainer() {
		if (rootContainer != null) {
			return rootContainer;
		}
		// try to locate the root, fail if no path exists
		rootContainer = pathToRoot();
		if (rootContainer != null) {
			return rootContainer;
		}
		throw new EnonException("No path to root: Make sure to call inContainer() on the element with a non-null value.");
	}

	/**
	 * Follows the chain of containers up to the top, which will either be null or a RootContainer.
	 * @return The RootContainer or null, if the top object is not a RootContainer.
	 */
	private RootContainer pathToRoot() {
		// follow the path to root
		ElementContainer nextParent = getContainer();
		while (nextParent != null && !(nextParent instanceof RootContainer)) {
			nextParent = nextParent.getContainer();
		}
		return (RootContainer) nextParent;
	}

	@Override
	public EnonContext getContext() {
		return getRootContainer().getContext();
	}

	@Override
	public ReaderContext getReaderContext() {
		return getRootContainer().getReaderContext();
	}

	@Override
	public WriterContext getWriterContext() {
		return getRootContainer().getWriterContext();
	}

	protected EnonConfig getConfig() {
		return getContext().getConfig();
	}

	@Override
	public String toString() {
		return (getType() != null ? getType().name() : "NO_TYPE")
				+ "[" + (getSize() != null ? getSize().getSize() : "?") + "]";
	}
}
