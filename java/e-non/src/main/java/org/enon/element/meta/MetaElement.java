/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.meta;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Immutable POJO containing metadata.
 * @author clininger
 * $Id: $
 */
public abstract  class MetaElement
{
	public static final byte META_CODE = (byte)0xFB;

	private final byte prefix;
//	private final String data;

	public MetaElement(byte prefix/*, String data*/)
	{
		this.prefix = prefix;
//		this.data = data;
	}

	public byte getPrefix()
	{
		return prefix;
	}

//	public String getData()
//	{
//		return data;
//	}

	public void write(OutputStream out) throws IOException
	{
		out.write(META_CODE);
		out.write(prefix);
		writeContent(out);
	}

	protected abstract void writeContent(OutputStream out) throws IOException;
}
