/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.array.ArrayElementSubtype;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import org.enon.exception.EnonElementException;
import org.enon.exception.EnonException;

/**
 * Data types supported by eNON
 *
 * @author clininger $Id: $
 */
public enum ElementType {
	NULL_ELEMENT('N'),
	FALSE_ELEMENT('0'),
	TRUE_ELEMENT('1'),
	INFINITY_POS_ELEMENT('+'),
	INFINITY_NEG_ELEMENT('-'),
	NAN_ELEMENT('?'),
	/** NANO_INT has a range of prefixes: 0x80..0xFF */
	NANO_INT_ELEMENT((char) 0x80),
	BYTE_ELEMENT('b'),
	SHORT_ELEMENT('s'),
	INT_ELEMENT('i'),
	LONG_ELEMENT('l'),
	FLOAT_ELEMENT('f'),
	DOUBLE_ELEMENT('d'),
	CHAR_8BIT_ELEMENT('`'),
	STRING_ELEMENT('\"'),
	NUMBER_ELEMENT('n'),
	TEMPORAL_ELEMENT('t', TemporalElementSubtype.class),
	BLOB_ELEMENT('B'),
	ARRAY_ELEMENT('A', ArrayElementSubtype.class),
	LIST_ELEMENT('['),
	MAP_ELEMENT('{'),
	MAP_ENTRY_ELEMENT((char)0), // the prefix of MAP_ENTRY_ELEMENT is never written to the e-NON stream, but we want an element type for it
	MAP_REF_ELEMENT('@'),
	GLOSSARY_REF_ELEMENT('G');

	public static final short MIN_NANO_INT_PREFIX = 0x80;
	public static final short MAX_NANO_INT_PREFIX = 0xFF;

	private static final Map<Byte, ElementType> lookup = new HashMap<>(ElementType.values().length);

	static {
		for (ElementType type : values()) {
			if (type.prefix != 0) {
				// only lookup non-zero prefixes
				if (null != lookup.put(type.prefix, type)) {
					// perform a quick ckeck for potential programming error
					throw new IllegalArgumentException("Duplicate type prefix for type: " + type.name() + ": '" + type.prefix + "'");
				}
			}
		}
		// add all the nan int prefixes to the lookup
		for (short nano = MIN_NANO_INT_PREFIX + 1; nano <= MAX_NANO_INT_PREFIX; nano++) {
			if (null != lookup.put((byte) nano, NANO_INT_ELEMENT)) {
				// perform a quick ckeck for potential programming error
				throw new IllegalArgumentException("Duplicate type prefix for nano-int: '" + Integer.toHexString(nano) + "'");
			}
		}
	}

	/**
	 * ASCII prefix for the type
	 */
	public final byte prefix;
	private final Class<? extends ElementSubtype> subTypeClass;
	private final List<? extends ElementSubtype> subTypes;
	private final Function<Byte, ElementSubtype> subTypeLookupFunction;

	/**
	 * Construct an element with an 8-bit prefix char.
	 * @param prefix The prefix for the element type.  Zero is a special value, meaning that
	 * the element will not be associated with a prefix at all.  A zero prefix will not be
	 * put into the lookup table.
	 */
	private ElementType(char prefix) {
		this.prefix = (byte) prefix;
		subTypeClass = null;
		subTypes = null;
		subTypeLookupFunction = null;
	}

	/**
	 * Construct an element with an 8-bit prefix char and an array of valid subtypes.
	 * @param prefix The prefix for the element type.  A zero prefix is not supported for a type having subtypes.
	 * @param subTypeClass
	 */
	private ElementType(char prefix, Class<? extends ElementSubtype> subTypeClass) {
		if (prefix == 0) {
			throw new EnonException("Invalid zero(0) prefix for element type having subtypes.");
		}
		this.prefix = (byte)prefix;
		this.subTypeClass = subTypeClass;
		try {
			// get all subtypes as a list
			subTypes = Arrays.asList((ElementSubtype[]) subTypeClass.getMethod("values").invoke(null));
			// create a function that will lookup the subtype by subtype prefix
			// use the static forPrefix(byte) method on the subtype class
			final Method forPrefixMethod = subTypeClass.getMethod("forPrefix", Byte.class);
			subTypeLookupFunction = subPrefix -> {
				try {
					return (ElementSubtype) forPrefixMethod.invoke(null, subPrefix);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
					throw new EnonElementException("Could not get subtype for prefix: ", ex);
				}
			};
		} catch (NoSuchMethodException ex) {
			throw new EnonElementException("ElementSubtype implementation must be enum type with a static forPrefix(Byte) method: "+subTypeClass);
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			throw new EnonElementException("Can't access subtype values: ", ex);
		}
	}

	/**
	 * @param prefix the prefix
	 * @return the type associated with the prefix
	 */
	public static final ElementType forPrefix(Byte prefix) {
		return lookup.get(prefix);
	}

	/**
	 * @return True if this type requires subtypes
	 */
	public boolean hasSubTypes() {
		return subTypeClass != null;
	}

	/**
	 * Test whether the subtype is valid for this type.
	 * @param subType Potential subtype to test
	 * @return True if this type declares the subtype to be valid.
	 */
	public boolean isValidSubType(ElementSubtype subType) {
		return subType != null && subTypeClass == subType.getClass();
	}

	/**
	 * Get the valid subtypes for this type
	 * @return List of valid subtypes
	 */
	public List<? extends ElementSubtype> getSubTypes() {
		return subTypes;
	}

	public ElementSubtype subtypeForPrefix(byte prefix) {
		if (subTypeLookupFunction != null) {
			return subTypeLookupFunction.apply(prefix);
		}
		return null;
	}
}
