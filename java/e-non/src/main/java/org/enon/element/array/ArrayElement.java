/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import org.enon.element.AbstractElement;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.ScalarElement;
import org.enon.element.meta.MetaElement;
import org.enon.element.writer.BinaryElementWriter;
import org.enon.exception.EnonElementException;
import org.enon.exception.EnonException;
import org.enon.element.SubtypedElement;

/**
 *
 * @author clininger $Id: $
 */
public abstract class ArrayElement extends AbstractElement implements ScalarElement, SubtypedElement<ArrayElementSubtype> {

	protected final ArrayElementSubtype subType;
	protected final int entrySize;
	protected byte[] contentBytes;

	protected ArrayElement(ArrayElementSubtype entryType, long size, int entrySize, List<MetaElement> metaList) {
		super(ElementType.ARRAY_ELEMENT, size, metaList);
		this.subType = entryType;
		this.entrySize = entrySize;
	}

	public static ByteArrayElement create(byte[] array, List<MetaElement> metaList) {
		return new ByteArrayElement(array, metaList);
	}

	public static BitArrayElement create(boolean[] array, List<MetaElement> metaList) {
		return new BitArrayElement(array, metaList);
	}

	public static ShortArrayElement create(short[] array, List<MetaElement> metaList) {
		return new ShortArrayElement(array, metaList);
	}

	public static IntArrayElement create(int[] array, List<MetaElement> metaList) {
		return new IntArrayElement(array, metaList);
	}

	public static LongArrayElement create(long[] array, List<MetaElement> metaList) {
		return new LongArrayElement(array, metaList);
	}

	public static FloatArrayElement create(float[] array, List<MetaElement> metaList) {
		return new FloatArrayElement(array, metaList);
	}

	public static DoubleArrayElement create(double[] array, List<MetaElement> metaList) {
		return new DoubleArrayElement(array, metaList);
	}

	/**
	 * Create an ArrayElement when the type of array is not known at compile-time.
	 * @param array Object which must be an array type, and must have a supported content type.
	 * @param metaList Optional metadata
	 * @return An ArrayElement instance appropriate for the content.
	 * @throws EnonElementException if the array object is not of the correct type.
	 */
	public static ArrayElement create(Object array, List<MetaElement> metaList) {
		if (array.getClass().isArray()) {
			Class componentClass = array.getClass().getComponentType();
			if (byte.class == componentClass) {
				return create((byte[])array, metaList);
			}
			if (boolean.class == componentClass) {
				return create((boolean[])array, metaList);
			}
			if (short.class == componentClass) {
				return create((short[])array, metaList);
			}
			if (int.class == componentClass) {
				return create((int[])array, metaList);
			}
			if (long.class == componentClass) {
				return create((long[])array, metaList);
			}
			if (float.class == componentClass) {
				return create((float[])array, metaList);
			}
			if (double.class == componentClass) {
				return create((double[])array, metaList);
			}
			throw new EnonElementException("Invalid array component type: "+componentClass.getName());
		}
		throw new EnonElementException("Invalid array type: "+array.getClass().getName());
	}

	/**
	 * Instantiate the appropriate ArrayElement based on the subType.  The array value will have the specified size, but will not be
	 * initialized with data.
	 * @param subType A valid ARRAY_ELEMENT sub type
	 * @param length The length of the array data, must not be negative!
	 * @param metaList Optional metadata
	 * @return An ArrayElement instance
	 * @throws EnonElementException if the subtype is invalid for ARRA_ELEMENT.
	 */
	public static ArrayElement createEmpty(ArrayElementSubtype subType, int length, List<MetaElement> metaList) {
		if (!ElementType.ARRAY_ELEMENT.isValidSubType(subType)) {
			throw new EnonElementException("Invalid subtype for array: "+subType);
		}
		switch (subType) {
			case BYTE_SUB_ELEMENT:
				return create(new byte[length], metaList);
			case DOUBLE_SUB_ELEMENT:
				return create(new double[length], metaList);
			case FALSE_SUB_ELEMENT:
				return create(new boolean[length], metaList);
			case FLOAT_SUB_ELEMENT:
				return create(new float[length], metaList);
			case INT_SUB_ELEMENT:
				return create(new int[length], metaList);
			case LONG_SUB_ELEMENT:
				return create(new long[length], metaList);
			case SHORT_SUB_ELEMENT:
				return create(new short[length], metaList);
			default:
				throw new EnonException("Missing implementation for ARRAY subtype "+subType);
		}
	}

	@Override
	public ArrayElementSubtype getSubType() {
		return subType;
	}

	/**
	 * Get the array entries as a list of elements.
	 * @return A list of elements that wrap the original array entries.
	 */
	public abstract List<? extends Element> asElements();

	@Override
	public byte[] getContentBytes() {
		if (contentBytes != null) {
			return contentBytes;
		}

		// construct a byte[] in network byte order
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream((int)getSize().getSize() * entrySize)) {
			getBinaryWriter().writeContent(this, baos);
			contentBytes = baos.toByteArray();
			return contentBytes;
		} catch (IOException x) {
			throw new EnonElementException("Failed to generate content bytes for "+getClass().getSimpleName(), x);
		}
	}

	protected abstract ArrayElementWriter getBinaryWriter();

	/////////////////////////////////////////////////////////////////////////////////
	public static abstract class ArrayElementWriter<A extends ArrayElement> extends BinaryElementWriter<A> {

		/**
		 * Override the default behavior: write the Array prefix then the entry type prefix.
		 * @param element The element to write
		 * @param out stream on which to write
		 * @throws IOException on io error
		 */
		@Override
		protected void writeHeader(A element, OutputStream out) throws IOException {
			// always use the 1-byte BLOB prefix rather than the 2-byte array prefix
			out.write(ElementType.ARRAY_ELEMENT.prefix);
			out.write(element.subType.prefix);
			writeSizeMeta(element, out);
		}

		// by redeclaring this method here we can access it in the containing class
		@Override
		protected abstract void writeContent(A element, OutputStream out) throws IOException;

	}

}
