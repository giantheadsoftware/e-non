/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.element.meta.MetaElement;
import org.enon.element.meta.MetaSize;
import org.enon.element.reader.BinaryElementReader;

/**
 * This element writes a numeric value as a String
 *
 * @author clininger $Id: $
 */
public class NumberElement extends StringElement implements NumericElement {

	/**
	 * Converts the number to a string using the number's toString() implementation
	 *
	 * @param value The number value
	 */
	public NumberElement(Number value) {
		this(value, null);
	}

	/**
	 * Converts the number to a string using the number's toString() implementation
	 *
	 * @param value The number value
	 * @param meta List of metadata
	 */
	public NumberElement(Number value, List<MetaElement> meta) {
		super(ElementType.NUMBER_ELEMENT, String.valueOf(value), meta);
	}

	/**
	 * Provide a String representation of a number. WARNING: for performance reasons, this string is not validated. Be sure to provide a valid number string!
	 *
	 * @param value The number as a string.
	 */
	public NumberElement(String value) {
		this(value, null);
	}

	/**
	 * Provide a String representation of a number. WARNING: for performance reasons, this string is not validated. Be sure to provide a valid number string!
	 *
	 * @param value The number as a string.
   * @param meta optional metadata
	 */
	public NumberElement(String value, List<MetaElement> meta) {
		super(ElementType.NUMBER_ELEMENT, value, meta);
	}

	/////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		@Override
		public NumberElement read(ReaderContext ctx) throws IOException {
			MetaSize metaSize = MetaSize.READER.read(ctx.getDataInputStream());
			byte[] bytes = readBytes(metaSize.getSize(), ctx.getDataInputStream());
			return new NumberElement(new String(bytes), metaSize.getMetaElements());
		}

	}
}
