/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.meta;

import java.util.HashMap;
import java.util.Map;

/**
 * Data types supported by eNON
 * @author clininger
 * $Id: $
 */
public enum MetaType
{
	GLOSSARY_META((byte)'G'),
	COUNT_META((byte)'#'),
	COMMENT_META((byte)'/'),
	TYPE_META((byte)'t'),
	VENDOR_META((byte)'v');

	private static final Map<Byte, MetaType> lookup = new HashMap<>(MetaType.values().length);

	static
	{
		for (MetaType type : values())
			if (null != lookup.put(type.prefix, type))
				throw new IllegalArgumentException("Duplicate type prefix '"+type.prefix+"'");
	}

	/** ASCII prefix for the type */
	public final byte prefix;

	private MetaType(byte prefix)
	{
		if ((byte)prefix < 0)
			throw new IllegalArgumentException("Invalid type prefix '0x"+Integer.toHexString(prefix)
					+"' for "+this.name()+". MetaType.prefix must be in ASCII (7-bit) character group.");
		this.prefix = prefix;
	}


	/**
   * @param prefix prefix
   * @return the type associated with the prefix
   */
	public static final MetaType forPrefix(byte prefix)
	{
		return lookup.get(prefix);
	}
}
