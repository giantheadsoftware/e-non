/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.reader;

import java.io.DataInputStream;
import java.io.IOException;
import org.enon.ReaderContext;
import org.enon.element.Element;
import org.enon.element.meta.Size;
import org.enon.element.meta.MetaSize;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public interface BinaryElementReader extends ElementReader<ReaderContext> {

	/**
	 * Read the element which appears next in the stream.Assume that the element's prefix has already been read and that the element's header and content is the next thing in the stream.
	 * @param <E> reference to the Element type
	 * @param ctx The read context
	 * @return An instance of the Reader's element type T
	 * @throws IOException on io error
	 */
	@Override
	<E extends Element> E read(ReaderContext ctx) throws IOException;

	/**
	 * For types that support size/meta headers, read the meta/size headers.
	 * The default implementation reads no bytes form the stream and returns null.
	 * @param in The data stream to read from
	 * @return a MetaSize structure or null
	 */
	default MetaSize readMetaSize(DataInputStream in) {
		return null;
	}

	/**
	 * Read the requested number of bytes from the stream.
	 *
	 * @param size Contains the number of bytes to read
	 * @param in The stream to read
	 * @return the bytes from the stream
	 * @throws IOException on io error
	 */
	default byte[] readBytes(Size size, DataInputStream in) throws IOException {
		if (size.getSize() > Integer.MAX_VALUE) {
			throw new EnonReadException("Maximum read length exceeded: " + size.getSize() + " > " + Integer.MAX_VALUE);
		}
		byte[] bytes = new byte[(int) size.getSize()];
		int readLen = 0;
		while (readLen >= 0 && readLen < size.getSize()) {
			int len = in.read(bytes, readLen, (int) size.getSize() - readLen);
			if (len == -1) {
				throw new EnonReadException("Unexpected end of data reached.");
			}
			readLen += len;
		}
		return bytes;
	}
}
