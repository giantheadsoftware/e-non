/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.enon.element.meta;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
/**
 * Immutable container that holds a size and/or meta elements.
 * @author clininger
 * $Id: $
 */
public class MetaSize
{
	public static final Reader READER = new Reader();

	private final Size size;
	private final List<MetaElement> metaElements;

	public MetaSize(Size size, List<MetaElement> metaElements)
	{
		this.size = size;
		this.metaElements = Collections.unmodifiableList(metaElements);
	}

	public Size getSize()
	{
		return size;
	}

	public List<MetaElement> getMetaElements()
	{
		return metaElements;
	}

	/////////////////////////////////////////////////////////////////////////////
	/**
	 * Read a metasize element from the stream.  This is used for sized elements that may also
	 * have metadata attached.
	 */
	public static class Reader
	{
		protected Reader() {
		}
		
		public MetaSize read(DataInputStream in) throws IOException
		{
			byte prefix = in.readByte();
			while(prefix == MetaElement.META_CODE)
			{
				throw new UnsupportedOperationException("Metadata not implemented yet");
			}
			Size size = Size.READER.read(prefix, in);
			return new MetaSize(size, Collections.EMPTY_LIST);
		}
	}
}
