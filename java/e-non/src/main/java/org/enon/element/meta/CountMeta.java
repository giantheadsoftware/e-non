/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.enon.element.meta;

import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author clininger
 * $Id: $
 */
public class CountMeta extends MetaElement
{
	private final Size size;

	public CountMeta(long count)
	{
		super(MetaType.COUNT_META.prefix);
		size = new Size(count);
	}

	public CountMeta(Size size)
	{
		super(MetaType.COUNT_META.prefix);
		this.size = size;
	}

	@Override
	protected void writeContent(OutputStream out) throws IOException
	{
		size.write(out);
	}

}
