/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

/**
 * A ScalarElement is one that does not contain any other elements.  It is the opposite of a ContainerElement.
 * The content portion of a ScalarElement will just be raw data value(s), as opposed to elements which have their
 * own prefixes, meta-size, etc.
 * 
 * @author clininger $Id: $
 */
public interface ScalarElement {
	
	/**
	 * Get the bytes that make up the content portion of the element.
	 * @return byte array.  Values in this array will be in network byte order, where appropriate.  While it is not guaranteed
	 * that this is the same data that was read from an e-NON stream, implementations of this method should return a value
	 * that would be valid for an e-NON stream.
	 */
	byte[] getContentBytes();
}
