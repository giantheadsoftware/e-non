/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.element.IntElement;
import org.enon.element.meta.MetaElement;
import org.enon.element.meta.MetaSize;
import org.enon.element.reader.BinaryElementReader;
import org.enon.util.SerializeUtil;

/**
 *
 * @author clininger $Id: $
 */
public class IntArrayElement extends ArrayElement {

	private final int[] array;

	public IntArrayElement(int[] array) {
		this(array, null);
	}

	public IntArrayElement(int[] array, List<MetaElement> metaList) {
		super(ArrayElementSubtype.INT_SUB_ELEMENT, array.length, IntElement.CONTENT_LENGTH, metaList);
		this.array = array;
	}

	@Override
	public int[] getValue() {
		return array;
	}

	@Override
	public List<IntElement> asElements() {
		ArrayList<IntElement> elements = new ArrayList<>(array.length);
		for (int entry : array) {
			elements.add(new IntElement(entry));
		}
		return elements;
	}

	@Override
	protected ArrayElementWriter getBinaryWriter() {
		return new Writer();
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends ArrayElementWriter<IntArrayElement> {

		@Override
		protected void writeContent(IntArrayElement element, OutputStream out) throws IOException {
			SerializeUtil serializeUtil = SerializeUtil.getInstance();
			for (int value : element.array) {
				out.write(serializeUtil.networkBytes(value));
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		@Override
		public IntArrayElement read(ReaderContext ctx) throws IOException {
			MetaSize metaSize = MetaSize.READER.read(ctx.getDataInputStream());
			int[] array = new int[(int)metaSize.getSize().getSize()];
			for (int i = 0; i < array.length; i++) {
				array[i] = ctx.getDataInputStream().readInt();
			}
			return new IntArrayElement(array, metaSize.getMetaElements());
		}

	}
}
