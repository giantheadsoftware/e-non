/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.util.HashMap;
import java.util.Map;
import org.enon.element.ElementSubtype;

/**
 * Data types supported by eNON
 *
 * @author clininger $Id: $
 */
public enum ArrayElementSubtype implements ElementSubtype<ArrayElementSubtype> {
	FALSE_SUB_ELEMENT('0'),
	BYTE_SUB_ELEMENT('b'),
	SHORT_SUB_ELEMENT('s'),
	INT_SUB_ELEMENT('i'),
	LONG_SUB_ELEMENT('l'),
	FLOAT_SUB_ELEMENT('f'),
	DOUBLE_SUB_ELEMENT('d');

	private static final Map<Byte, ArrayElementSubtype> lookup = new HashMap<>(ArrayElementSubtype.values().length);

	static {
		for (ArrayElementSubtype type : values()) {
			if (null != lookup.put(type.prefix, type)) {
				// perform a quick ckeck for potential programming error
				throw new IllegalArgumentException("Duplicate type prefix for type: " + type.name() + ": '" + type.prefix + "'");
			}
		}
	}

	/**
	 * ASCII prefix for the type
	 */
	public final byte prefix;

	private ArrayElementSubtype(char prefix) {
		this.prefix = (byte) prefix;
	}

	/**
	 * @param prefix the prefix
	 * @return the type associated with the prefix
	 */
	public static final ArrayElementSubtype forPrefix(Byte prefix) {
		return lookup.get(prefix);
	}

	@Override
	public byte getPrefix() {
		return prefix;
	}

}
