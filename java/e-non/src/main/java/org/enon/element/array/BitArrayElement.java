/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.element.ConstantElement;
import org.enon.element.meta.MetaElement;
import org.enon.element.meta.MetaSize;
import org.enon.element.reader.BinaryElementReader;
import org.enon.exception.EnonElementException;

/**
 *
 * @author clininger $Id: $
 */
public class BitArrayElement extends ArrayElement {
	/** A mask for every bit in a byte */
	private static final byte[] BIT_MASKS = new byte[]{
		(byte)0x80, (byte)0x40, (byte)0x20, (byte)0x10, (byte)0x08, (byte)0x04, (byte)0x02, (byte)0x01
	};

	private final boolean[] array;

	public BitArrayElement(boolean[] array) {
		this(array, null);
	}

	public BitArrayElement(boolean[] array, List<MetaElement> metaList) {
		// pass 0 as the entry size.  It's actually 1/8, not an int.  We'll have to override the
		// getContentBytes method to handle it as a special case.
		super(ArrayElementSubtype.FALSE_SUB_ELEMENT, array.length, 0, metaList);
		this.array = array;
	}
	
	/**
	 * Based on the boolean array length, compute the number of bytes needed for the bitset.
	 * @param arrayLength Size of the boolean[]
	 * @return Number of bytes needed to hold all the values in a bitset.
	 */
	public static long computeSize(int arrayLength) {
		int wholeBytes = arrayLength / 8;
		int extraBits = arrayLength % 8;
		return extraBits == 0 ? wholeBytes : wholeBytes + 1;
	}

	@Override
	public boolean[] getValue() {
		return array;
	}

	@Override
	public byte[] getContentBytes() {
		if (contentBytes != null) {
			return contentBytes;
		}
		
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream((int)computeSize(array.length))) {
			getBinaryWriter().writeContent(this, baos);
			contentBytes = baos.toByteArray();
			return contentBytes;
		} catch (IOException x) {
			throw new EnonElementException("Failed to generate content bytes for BitArray.", x);
		}
	}

	@Override
	public List<ConstantElement> asElements() {
		ArrayList<ConstantElement> elements = new ArrayList<>(array.length);
		for (boolean entry : array) {
			elements.add(entry ? ConstantElement.trueInstance() : ConstantElement.falseInstance());
		}
		return elements;
	}

	@Override
	protected ArrayElementWriter getBinaryWriter() {
		return new Writer();
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends ArrayElementWriter<BitArrayElement> {

		@Override
		protected void writeContent(BitArrayElement element, OutputStream out) throws IOException {
			int wholeBytes = element.array.length / 8;
			int extraBits = element.array.length % 8;
			int offset;
			byte bitset;
			// write out the whole bytes
			for (int i = 0; i < wholeBytes; i++) {
				offset = i*8;
				bitset = (byte)((element.array[offset] ? BIT_MASKS[0] : 0)
						+ (element.array[offset + 1] ? BIT_MASKS[1] : 0)
						+ (element.array[offset + 2] ? BIT_MASKS[2] : 0)
						+ (element.array[offset + 3] ? BIT_MASKS[3] : 0)
						+ (element.array[offset + 4] ? BIT_MASKS[4] : 0)
						+ (element.array[offset + 5] ? BIT_MASKS[5] : 0)
						+ (element.array[offset + 6] ? BIT_MASKS[6] : 0)
						+ (element.array[offset + 7] ? BIT_MASKS[7] : 0));
				out.write(bitset);
			}
			if (extraBits > 0) {
				// write out any extra bits that don't make a whole byte
				bitset = 0;
				offset = wholeBytes * 8;
				for (int i = 0; i < extraBits; i++) {
					bitset += element.array[offset + i] ? BIT_MASKS[i] : 0;
				}
				out.write(bitset);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		@Override
		public BitArrayElement read(ReaderContext ctx) throws IOException {
			MetaSize metaSize = MetaSize.READER.read(ctx.getDataInputStream());
			// create the boolean array
			boolean[] array = new boolean[(int) (metaSize.getSize().getSize())];

			// determine how many bytes to read from the stream
			int wholeBytes = array.length / 8;
			int extraBits = array.length % 8;
			
			// unpack the bytes
			for (int i = 0; i < wholeBytes; i++) {
				// read the next byte
				int nextByte = ctx.getInputStream().read();
				// convert the bitset into boolean values by testing the bits
				int offset = i*8;
				array[offset] = (BIT_MASKS[0] & nextByte) != 0;
				array[offset + 1] = (BIT_MASKS[1] & nextByte) != 0;
				array[offset + 2] = (BIT_MASKS[2] & nextByte) != 0;
				array[offset + 3] = (BIT_MASKS[3] & nextByte) != 0;
				array[offset + 4] = (BIT_MASKS[4] & nextByte) != 0;
				array[offset + 5] = (BIT_MASKS[5] & nextByte) != 0;
				array[offset + 6] = (BIT_MASKS[6] & nextByte) != 0;
				array[offset + 7] = (BIT_MASKS[7] & nextByte) != 0;
			}
			if (extraBits > 0) {
				// unpack any extra bits that don't make a whole byte
				int lastByte = ctx.getInputStream().read();
				int offset = wholeBytes*8;
				for (int i = 0; i < extraBits; i++) {
					// test the bit to get a boolean value
					array[offset + i] = (BIT_MASKS[i] & lastByte) != 0;
				}
			}
			return new BitArrayElement(array, metaSize.getMetaElements());
		}

	}
}
