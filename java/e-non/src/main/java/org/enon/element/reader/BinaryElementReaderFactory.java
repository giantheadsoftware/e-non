/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.reader;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.element.array.ByteArrayElement;
import org.enon.element.ByteElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.ElementSubtype;
import org.enon.element.ElementType;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.ListElement;
import org.enon.element.LongElement;
import org.enon.element.MapElement;
import org.enon.element.MapEntryElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.ShortElement;
import org.enon.element.StringElement;
import org.enon.element.TemporalElement;
import org.enon.element.TemporalElementSubtype;
import org.enon.element.array.BitArrayElement;
import org.enon.element.array.DoubleArrayElement;
import org.enon.element.array.FloatArrayElement;
import org.enon.element.array.IntArrayElement;
import org.enon.element.array.LongArrayElement;
import org.enon.element.array.ShortArrayElement;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public class BinaryElementReaderFactory implements ElementReaderFactory<BinaryElementReader> {

	private volatile Map<ElementType, BinaryElementReader> readers;
	private volatile Map<ElementType, Map<? extends ElementSubtype, BinaryElementReader>> subTypeReaders;

	public BinaryElementReaderFactory(EnonConfig config) {
		// config not currently used
	}

	/**
	 * Read the prefix(es) of the next element in the stream and lookup a reader for the associated element type.
	 * @param in e-NON input stream
	 * @return A BinaryElementReader or null if at the end of the input stream
	 * @throws IOException if unable to read from the stream
	 * @throws EnonReadException if the prefix and/or sub-prefix are not valid or if no reader can be found matching the next prefix
	 */
	@Override
	public BinaryElementReader reader(InputStream in) throws IOException {
		Byte prefix = readPrefix(in);
		if (prefix == null) {
			return null;
		}

		ElementType type = ElementType.forPrefix(prefix);
		if (type == null) {
			throw new EnonReadException("Unknown element prefix: "+(char)prefix.byteValue()+" ("+prefix+")");
		}

		// handle special cases
		if (ConstantElement.CONSTANT_TYPES.contains(type)) {
			return ConstantElement.Reader.getInstance(type);
		} else if (ElementType.NANO_INT_ELEMENT == type) {
			return new NanoIntElement.Reader(prefix);
		}

		BinaryElementReader reader;
		ElementSubtype subType = null;

		// check whether a subtype is required for this type
		if (!type.hasSubTypes()) {
			// No subtypes
			reader = lookupReader(type, null);
		} else {
			// read a subtype prefix
			prefix = readPrefix(in);
			if (prefix == null) {
				throw new EnonReadException("Missing required subtype for type "+type);
			}
			subType = type.subtypeForPrefix(prefix);
			if (subType != null && type.isValidSubType(subType)) {
				reader = lookupReader(type, subType);
			} else {
				throw new EnonReadException("Invalid subtype for type "+type+": "+(char)prefix.byteValue()+" ("+prefix+")");
			}
		}
		if (reader == null) {
			throw new EnonReadException("No reader found for type "+type+(subType != null ? "-"+subType : ""));
		}
		return reader;
	}

	/**
	 * Once the type and subtype have been determined from the stream, use those to lookup readers from our maps.
	 * @param type Main type
	 * @param subType Subtype, when necessary
	 * @return A reader for the type/subtype pair, null if not found.
	 */
	protected BinaryElementReader lookupReader(ElementType type, ElementSubtype subType) {
		if (subType == null) {
			// no subtype specified
			return getReaders().get(type);
		}
		if (!type.hasSubTypes()) {
			throw new EnonReadException("Type "+type+" does not support sub types.");
		}
		// subtype specified, 2-byte prefix
		Map<ElementType, Map<? extends ElementSubtype, BinaryElementReader>> subTypeReadersLocal;
		if ((subTypeReadersLocal = subTypeReaders) == null) {
			synchronized (this) {
				if ((subTypeReadersLocal = subTypeReaders) == null) {
					subTypeReaders = subTypeReadersLocal = createSubTypeReaderMap();
				}
			}
		}
		Map<? extends ElementSubtype, BinaryElementReader> subTypeMap = subTypeReadersLocal.get(type);
		if (subTypeMap != null) {
			return subTypeMap.get(subType);
		}
		throw new EnonReadException("Subtypes not configured for type "+type);
	}


	/**
	 * Read the next byte as a prefix.
	 * @return The byte read from the stream of null if EoS is reached.
	 * @throws IOException
	 */
	private Byte readPrefix(InputStream in) throws IOException {
		int read = in.read();
		if (read == -1)	{
			return null;  // end of stream reached
		}
		return (byte)read;
	}

	protected Map<ElementType, BinaryElementReader> getReaders() {
		Map<ElementType, BinaryElementReader> readersLocal;
		if ((readersLocal = readers) == null) {
			synchronized (this) {
				if ((readersLocal = readers) == null) {
					readers = readersLocal = createReaderMap();
				}
			}
		}
		return readersLocal;
	}

	protected Map<ElementType, BinaryElementReader> createReaderMap() {
		Map<ElementType, BinaryElementReader> readerMap = new HashMap<>();

		for (ElementType type : ElementType.values()) {
			switch(type) {
				case NULL_ELEMENT:
				case FALSE_ELEMENT:
				case INFINITY_NEG_ELEMENT:
				case INFINITY_POS_ELEMENT:
				case NAN_ELEMENT:
				case TRUE_ELEMENT:
				case NANO_INT_ELEMENT:
					// these don't have readers: the prefix itself defines the value
					break;
				case BLOB_ELEMENT:
					readerMap.put(type, new ByteArrayElement.Reader());
					break;
				case BYTE_ELEMENT:
					readerMap.put(type, new ByteElement.Reader());
					break;
				case DOUBLE_ELEMENT:
					readerMap.put(type, new DoubleElement.Reader());
					break;
				case FLOAT_ELEMENT:
					readerMap.put(type, new FloatElement.Reader());
					break;
				case INT_ELEMENT:
					readerMap.put(type, new IntElement.Reader());
					break;
				case LIST_ELEMENT:
					readerMap.put(type, new ListElement.Reader());
					break;
				case LONG_ELEMENT:
					readerMap.put(type, new LongElement.Reader());
					break;
				case MAP_ELEMENT:
					readerMap.put(type, new MapElement.Reader());
					break;
				case MAP_ENTRY_ELEMENT:
					readerMap.put(type, new MapEntryElement.Reader());
					break;
				case NUMBER_ELEMENT:
					readerMap.put(type, new NumberElement.Reader());
					break;
				case SHORT_ELEMENT:
					readerMap.put(type, new ShortElement.Reader());
					break;
				case STRING_ELEMENT:
					readerMap.put(type, new StringElement.Reader());
					break;
				case ARRAY_ELEMENT:
					// do not put the array element reader here
					break;
				case GLOSSARY_REF_ELEMENT:
				case MAP_REF_ELEMENT:
					break;
			}
		}
		return readerMap;
	}

	/**
	 * Create a map of subtype maps.  Override this to customize the subtype readers.
	 * @return new map instance: key = ElementType, value = map of ElementSubtype -&gt; BinaryElementReader
	 */
	protected Map<ElementType, Map<? extends ElementSubtype, BinaryElementReader>> createSubTypeReaderMap() {
		Map<ElementType, Map<? extends ElementSubtype, BinaryElementReader>> map = new HashMap<>();
		map.put(ElementType.ARRAY_ELEMENT, createArrayReaderMap());
		map.put(ElementType.TEMPORAL_ELEMENT, createTemporalReaderMap());
		return map;
	}

	/**
	 * Generate a map of ArrayElement subtype readers.  Override this to customize the array readers.
	 * @return ArrayElement subtype readers, mapped to their subtypes
	 */
	protected Map<ArrayElementSubtype, BinaryElementReader> createArrayReaderMap() {
		Map<ArrayElementSubtype, BinaryElementReader> readerMap = new HashMap<>();

		for (ArrayElementSubtype type : ArrayElementSubtype.values()) {
			switch(type) {
				case BYTE_SUB_ELEMENT:
					readerMap.put(ArrayElementSubtype.BYTE_SUB_ELEMENT, new ByteArrayElement.Reader());
					break;
				case DOUBLE_SUB_ELEMENT:
					readerMap.put(ArrayElementSubtype.DOUBLE_SUB_ELEMENT, new DoubleArrayElement.Reader());
					break;
				case FALSE_SUB_ELEMENT:
					readerMap.put(ArrayElementSubtype.FALSE_SUB_ELEMENT, new BitArrayElement.Reader());
					break;
				case FLOAT_SUB_ELEMENT:
					readerMap.put(ArrayElementSubtype.FLOAT_SUB_ELEMENT, new FloatArrayElement.Reader());
					break;
				case INT_SUB_ELEMENT:
					readerMap.put(ArrayElementSubtype.INT_SUB_ELEMENT, new IntArrayElement.Reader());
					break;
				case LONG_SUB_ELEMENT:
					readerMap.put(ArrayElementSubtype.LONG_SUB_ELEMENT, new LongArrayElement.Reader());
					break;
				case SHORT_SUB_ELEMENT:
					readerMap.put(ArrayElementSubtype.SHORT_SUB_ELEMENT, new ShortArrayElement.Reader());
					break;
			}
		}
		return readerMap;
	}

	/**
	 * Generate a map of TemporalElement subtype readers.  Override this to customize the temporal element readers.
	 * The TemporalElement.Reader class can handle all types.
	 * @return TemporalElement subtype readers, mapped to their subtypes
	 */
	protected Map<TemporalElementSubtype, BinaryElementReader> createTemporalReaderMap() {
		Map<TemporalElementSubtype, BinaryElementReader> map = new HashMap<>();
		for (TemporalElementSubtype subType : TemporalElementSubtype.values()) {
			map.put(subType, new TemporalElement.Reader(subType));
		}
		return map;
	}
}
