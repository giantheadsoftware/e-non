/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author clininger $Id: $
 */
public enum TemporalElementSubtype implements ElementSubtype {
	MAP('{', 0),
	NONE('0', 0), // default local time: midnight
	YEAR('1', 2), // Y
	YEAR_MONTH('2', 3), // YM
	FULL_DATE('3', 4), // YMD
	FULL_DATE_H('4', 5), // YMDh
	FULL_DATE_HM('5', 6), // YMDhm
	FULL_DATE_TIME('6', 7), // YMDhms
	FULL_DATE_TIME_MS('7', 8), // YMDhmS
	FULL_DATE_TIME_NS('8', 11), // YMDhmsn
	LOCAL_TIME_S('s', 3), // hms
	INSTANT_UTC('i', 8), // i
	OFFSET_DATE_TIME_S('T', 8), // YMDhmso
	OFFSET_TIME_HM('M', 3), // hmo
	OFFSET_TIME_S('S', 4), // hmso
	OFFSET_INSTANT('I', 9), // io
	ZONED_TIME_MS('z', 0), // hmSz
	ZONED_INSTANT('Z', 0); // iz

	private static final Map<Byte, TemporalElementSubtype> lookup = new HashMap<>(TemporalElementSubtype.values().length);

	static {
		for (TemporalElementSubtype type : values()) {
			if (null != lookup.put(type.prefix, type)) {
				// perform a quick ckeck for potential programming error
				throw new IllegalArgumentException("Duplicate type prefix for type: " + type.name() + ": '" + type.prefix + "'");
			}
		}
	}
	
	public final byte prefix;
	public final int size;

	private TemporalElementSubtype(char prefix, int size) {
		this.prefix = (byte)prefix;
		this.size = size;
	}
	
	public static TemporalElementSubtype forPrefix(Byte prefix) {
		return lookup.get(prefix);
	}

	@Override
	public byte getPrefix() {
		return prefix;
	}
}
