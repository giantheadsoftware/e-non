/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.MetaElement;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import org.enon.element.writer.BinaryElementWriter;

/**
 *
 * @author clininger $Id: $
 */
public abstract class FixedSizeElement extends AbstractElement {

	public static final byte[] EMPTY_BYTES = new byte[0];

	/**
	 * Implementations should pass in a constant value for the size.
	 *
	 * @param type The element type
	 * @param size the fixed size
	 */
	protected FixedSizeElement(ElementType type, long size) {
		super(type, size);
	}


	/**
	 * Metadata can't be added to a fixed size element.
	 *
	 * @param meta ignored
	 * @return this element
	 */
	@Override
	public FixedSizeElement addMeta(MetaElement meta) {
		return this;
	}

	/**
	 * Metadata does not apply to fixed size elements. Meta may be inherited from containers.
	 *
	 * @return Empty list
	 */
	@Override
	public List<MetaElement> getMeta() {
		return getContainerMeta();
	}

	//////////////////////////////////////////////////////////////////////////////////////
	public static class Writer<E extends FixedSizeElement> extends BinaryElementWriter<E> {

		@Override
		protected void writeSizeMeta(E element, OutputStream out) throws IOException {
			// don't write size for fixed size types
		}

		/**
		 * Default implementation writes no content.  Override to writ something
		 * @param element The element being written
		 * @param out the stream on which to write
		 * @throws IOException on io error
		 */
		@Override
		protected void writeContent(E element, OutputStream out) throws IOException {
			// no content
		}

	}
}
