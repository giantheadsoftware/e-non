/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.EnonContext;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.exception.EnonElementException;
import org.enon.exception.EnonReadException;
import org.enon.exception.EnonWriteException;

/**
 *
 * @author clininger $Id: $
 */
public class RootContainer implements ElementContainer {

	private final EnonContext ctx;
	private final WriterContext writerCtx;
	private final ReaderContext readerCtx;

	/**
	 * Constructor.  The WriterContext and ReaderContext both create a RootElement and pass themselves into this constructor.
	 * Rather than using this constructor directly, it's generally better to create the context (which has to be done anyway)
	 * then call getRoot() on that context.
	 *
	 * @param ctx An EnonContext instance.  If this value is a WriterContext instance, then getWriterContext() will return this value.  If it's
	 * a ReaderContext, the getReadreContext() will return this value.
	 */
	public RootContainer(EnonContext ctx) {
		this.ctx = ctx;
		this.writerCtx = ctx instanceof WriterContext ? (WriterContext)ctx : null;
		this.readerCtx = ctx instanceof ReaderContext ? (ReaderContext)ctx : null;
	}

	@Override
	public RootContainer add(Element element) {
		throw new EnonElementException("Cannot add to the RootElement");
	}

	public EnonContext getContext() {
		return ctx;
	}

	public WriterContext getWriterContext() {
		if (writerCtx != null) {
			return writerCtx;
		}
		throw new EnonWriteException("The RootContainer was not initialized with a WriterContext");
	}

	public ReaderContext getReaderContext() {
		if (readerCtx != null) {
			return readerCtx;
		}
		throw new EnonReadException("The RootContainer was not initialized with a ReaderContext");
	}

	/**
	 * The RootContainer is not contained by any other container.
	 * @return Always null
	 */
	@Override
	public ElementContainer getContainer() {
		return null;
	}

}
