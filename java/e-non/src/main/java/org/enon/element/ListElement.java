/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.cache.EnonCache;
import org.enon.element.meta.MetaElement;
import org.enon.element.meta.MetaSize;
import org.enon.element.reader.BinaryElementReader;
import org.enon.element.writer.BinaryElementWriter;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 * @param <E> The Element type contained in the list
 */
public class ListElement<E extends Element> extends AbstractElement implements ElementContainer<E> {

	protected final Collection<E> entries;

	public ListElement(Collection<E> entries) {
		this(entries, null);
	}

	public ListElement(Collection<E> entries, List<MetaElement> metaList) {
 		this(ElementType.LIST_ELEMENT, entries, metaList);
	}

	public ListElement(int capacity) {
		this(capacity, null);
	}

	public ListElement(int capacity, List<MetaElement> metaList) {
		this(ElementType.LIST_ELEMENT, capacity, metaList);
	}

	protected ListElement(ElementType type, int capacity, List<MetaElement> metaList) {
		super(type, capacity, metaList);
		this.entries = new ArrayList<>(capacity);
	}

	public ListElement(ElementType type, Collection<E> entries, List<MetaElement> metaList) {
		super(type, entries.size(), metaList);
		this.entries = entries;
	}

	@Override
	public ListElement inContainer(ElementContainer containerElement) {
		super.inContainer(containerElement);
		applyContainer(entries);
		return this;
	}

	@Override
	public Collection<E> getContents() {
		return entries;
	}

	@Override
	public List<E> getValue() {
		validateCapacity();
		return Collections.unmodifiableList(new ArrayList<>(entries));
	}

	/**
	 * The List can't contain its own mapIds. Defer to the next higher container.
	 *
	 * @return the parent container of this Element
	 */
	@Override
	public EnonCache<String, Long> getMapIds() {
		return getContainer().getMapIds();
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer<LE extends ListElement<? extends Element>> extends BinaryElementWriter<LE> {

		@Override
		public void write(final LE listElement) {
			listElement.validateCapacity();
			super.write(listElement);
		}

		@Override
		protected void writeContent(LE element, OutputStream out) throws IOException {
			WriterContext ctx = element.getWriterContext();
			element.entries.stream()
					.forEach(ctx::write);
		}

	}

	/////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		@Override
		public ListElement read(final ReaderContext ctx) throws IOException {
			// read the size and construct a list to hold the content
			final MetaSize metaSize = MetaSize.READER.read(ctx.getDataInputStream());
			final long size = metaSize.getSize().getSize();
			if (size > Integer.MAX_VALUE)
				throw new EnonReadException("ListElement exceeds maximum length: "+Integer.MAX_VALUE);

			// create the ListElement with the expected size
			final ListElement listElement = newElement((int)size, metaSize.getMetaElements());

			// try to parse the expected number of elements
			for (int i = 0; i < size; i++) {
				final Element e = parseNextEntry(ctx);
				if (e == null) {
					break;
				}
				listElement.add(e);
			}

			if (listElement.getContents().size() < size) {
				throw new EnonReadException("Could not read expected number of elements.");
			}
			listElement.validateCapacity();
			return listElement;
		}

		protected ListElement newElement(int size, List<MetaElement> meta) {
			return new ListElement(size, meta);
		}

		protected Element parseNextEntry(final ReaderContext ctx) throws IOException {
			return ctx.parseNextElement();
		}
	}
}
