/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.reader;

import java.io.IOException;
import java.io.InputStream;
import org.enon.Prolog;

/**
 *
 * @author clininger $Id: $
 * @param <ER> Type of ElementReader returned by this factory
 */
public interface ElementReaderFactory<ER extends ElementReader> {

	/**
	 * Get an element reader based on the next element type in the stream.
	 * @param <R> Type of reader
	 * @param in Stream where elements are coming from
	 * @return ElementReader instance, or null if no reader exists for the prefix.
	 * @throws java.io.IOException on io error
	 */
	<R extends ER> R reader(InputStream in) throws IOException;

//	/**
//	 * Get an element reader based on the element type & subtype.  Some elements require a 2-byte prefix,
//	 * the type and a subtype.
//	 * @param <R> Type of element reader
//	 * @param type type of element to read
//	 * @param subType Subtype of element to read
//	 * @return ElementReader instance, or null if no reader exists for the prefix.
//	 */
//	<R extends ER> R reader(ElementType type, ElementSubType subType);

	/**
	 * Get a reader for the prolog.  Default: Prolog.Reader.INSTANCE
	 * @param <R> Reader type
	 * @return Prolog.Reader instance
	 */
	default <R extends Prolog.Reader> R prologReader() {
		return (R) Prolog.Reader.INSTANCE;
	}
}
