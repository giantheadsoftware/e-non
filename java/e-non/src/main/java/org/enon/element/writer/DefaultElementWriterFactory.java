/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.writer;

import java.util.HashMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.element.array.ByteArrayElement;
import org.enon.element.ByteElement;
import org.enon.element.DoubleElement;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.ListElement;
import org.enon.element.LongElement;
import org.enon.element.MapElement;
import org.enon.element.MapEntryElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.ShortElement;
import org.enon.element.StringElement;
import org.enon.element.ConstantElement;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.ElementSubtype;
import org.enon.element.array.BitArrayElement;
import org.enon.element.array.DoubleArrayElement;
import org.enon.element.array.FloatArrayElement;
import org.enon.element.array.IntArrayElement;
import org.enon.element.array.LongArrayElement;
import org.enon.element.array.ShortArrayElement;
import org.enon.exception.EnonException;
import org.enon.element.SubtypedElement;
import org.enon.element.TemporalElement;
import org.enon.element.TemporalElementSubtype;

/**
 *
 * @author clininger $Id: $
 */
public class DefaultElementWriterFactory implements ElementWriterFactory {

	private volatile Map<ElementType, ElementWriter> writers;
	private volatile Map<ElementType, Map<? extends ElementSubtype, ElementWriter>> subtypeWriters;

	public DefaultElementWriterFactory(EnonConfig config) {
		// config not currently needed
	}

	@Override
	public <E extends Element> ElementWriter<E> writer(E e) {
		ElementType type = e.getType();
		if (!type.hasSubTypes()) {
			return getWriters().get(e.getType());
		} else {
			Map<? extends ElementSubtype, ElementWriter> subTypeMap = getSubtypeWriters().get(type);
			if (subTypeMap == null) {
				throw new EnonException("Subtypes not registered for "+type);
			}
			// check for subtype
			return subTypeMap.get(((SubtypedElement) e).getSubType());
		}
	}

	protected Map<ElementType, ElementWriter> getWriters() {
		Map<ElementType, ElementWriter> writersLocal;
		if ((writersLocal = writers) == null) {
			synchronized (this) {
				if ((writersLocal = writers) == null) {
					writers = writersLocal = createWriterMap();
				}
			}
		}
		return writersLocal;
	}

	protected Map<ElementType, Map<? extends ElementSubtype, ElementWriter>> getSubtypeWriters() {
		Map<ElementType, Map<? extends ElementSubtype, ElementWriter>> subtypeWritersLocal;
		if ((subtypeWritersLocal = subtypeWriters) == null) {
			synchronized (this) {
				if ((subtypeWritersLocal = subtypeWriters) == null) {
					subtypeWriters = subtypeWritersLocal = createSubWriterMap();
				}
			}
		}
		return subtypeWritersLocal;
	}

	protected Map<ElementType, ElementWriter> createWriterMap() {
		Map<ElementType, ElementWriter> writerMap = new HashMap<>();

		for (ElementType type : ElementType.values()) {
			switch (type) {
				case BLOB_ELEMENT:
					writerMap.put(type, new ByteArrayElement.Writer());
					break;
				case BYTE_ELEMENT:
					writerMap.put(type, new ByteElement.Writer());
					break;
				case DOUBLE_ELEMENT:
					writerMap.put(type, new DoubleElement.Writer());
					break;
				case FALSE_ELEMENT:
					writerMap.put(type, new ConstantElement.Writer());
					break;
				case FLOAT_ELEMENT:
					writerMap.put(type, new FloatElement.Writer());
					break;
				case INFINITY_NEG_ELEMENT:
					writerMap.put(type, new ConstantElement.Writer());
					break;
				case INFINITY_POS_ELEMENT:
					writerMap.put(type, new ConstantElement.Writer());
					break;
				case INT_ELEMENT:
					writerMap.put(type, new IntElement.Writer());
					break;
				case LIST_ELEMENT:
					writerMap.put(type, new ListElement.Writer());
					break;
				case LONG_ELEMENT:
					writerMap.put(type, new LongElement.Writer());
					break;
				case MAP_ELEMENT:
					writerMap.put(type, new MapElement.Writer());
					break;
				case MAP_ENTRY_ELEMENT:
					writerMap.put(type, new MapEntryElement.Writer());
					break;
				case NANO_INT_ELEMENT:
					writerMap.put(type, new NanoIntElement.Writer());
					break;
				case NAN_ELEMENT:
					writerMap.put(type, new ConstantElement.Writer());
					break;
				case NULL_ELEMENT:
					writerMap.put(type, new ConstantElement.Writer());
					break;
				case NUMBER_ELEMENT:
					writerMap.put(type, new NumberElement.Writer());
					break;
				case SHORT_ELEMENT:
					writerMap.put(type, new ShortElement.Writer());
					break;
				case STRING_ELEMENT:
					writerMap.put(type, new StringElement.Writer());
					break;
				case TRUE_ELEMENT:
					writerMap.put(type, new ConstantElement.Writer());
					break;
				case ARRAY_ELEMENT:
				case GLOSSARY_REF_ELEMENT:
				case MAP_REF_ELEMENT:
					break;
			}
		}
		return writerMap;
	}

	protected Map<ElementType, Map<? extends ElementSubtype, ElementWriter>>createSubWriterMap() {
		Map<ElementType, Map<? extends ElementSubtype, ElementWriter>> map = new HashMap<>();
		map.put(ElementType.ARRAY_ELEMENT, createArrayWriterMap());
		map.put(ElementType.TEMPORAL_ELEMENT, createTemporalWriterMap());
		return map;
	}
	
	protected Map<ArrayElementSubtype, ElementWriter> createArrayWriterMap() {
		Map<ArrayElementSubtype, ElementWriter> map = new HashMap<>();
		for (ArrayElementSubtype arrayType : ArrayElementSubtype.values()) {
			switch (arrayType) {
				case BYTE_SUB_ELEMENT:
					map.put(arrayType, new ByteArrayElement.Writer());
					break;
				case DOUBLE_SUB_ELEMENT:
					map.put(arrayType, new DoubleArrayElement.Writer());
					break;
				case FALSE_SUB_ELEMENT:
					map.put(arrayType, new BitArrayElement.Writer());
					break;
				case FLOAT_SUB_ELEMENT:
					map.put(arrayType, new FloatArrayElement.Writer());
					break;
				case INT_SUB_ELEMENT:
					map.put(arrayType, new IntArrayElement.Writer());
					break;
				case LONG_SUB_ELEMENT:
					map.put(arrayType, new LongArrayElement.Writer());
					break;
				case SHORT_SUB_ELEMENT:
					map.put(arrayType, new ShortArrayElement.Writer());
					break;
				default:
					throw new EnonException("Element Writer Factory needs to register a reader for ArrayElement sub type "+arrayType);
			}
		}
		return map;
	}
	
	protected Map<TemporalElementSubtype, ElementWriter> createTemporalWriterMap() {
		Map<TemporalElementSubtype, ElementWriter> map = new HashMap<>();
		ElementWriter writer = new TemporalElement.Writer();
		for (TemporalElementSubtype subType : TemporalElementSubtype.values()) {
			map.put(subType, writer);
		}
		return map;
	}
}
