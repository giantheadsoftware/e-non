/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.enon.ReaderContext;
import static org.enon.element.ElementType.NULL_ELEMENT;
import static org.enon.element.ElementType.TRUE_ELEMENT;
import static org.enon.element.ElementType.FALSE_ELEMENT;
import static org.enon.element.ElementType.INFINITY_POS_ELEMENT;
import static org.enon.element.ElementType.INFINITY_NEG_ELEMENT;
import static org.enon.element.ElementType.NAN_ELEMENT;
import org.enon.element.reader.BinaryElementReader;

/**
 *
 * @author clininger $Id: $
 */
public class ConstantElement extends FixedSizeElement {
	public static final int CONTENT_LENGTH = 0;
	public static final Set<ElementType> CONSTANT_TYPES = EnumSet.of(NULL_ELEMENT, TRUE_ELEMENT, FALSE_ELEMENT, INFINITY_POS_ELEMENT, INFINITY_NEG_ELEMENT, NAN_ELEMENT);
	
	private final Object value;
	
	private ConstantElement(ElementType type, Object value) {
		super(type, CONTENT_LENGTH);
		this.value = value;
	}
	
	public static ConstantElement nullInstance() {
		return new ConstantElement(ElementType.NULL_ELEMENT, null);
	}
	
	public static ConstantElement trueInstance() {
		return new ConstantElement(ElementType.TRUE_ELEMENT, true);
	}
	
	public static ConstantElement falseInstance() {
		return new ConstantElement(ElementType.FALSE_ELEMENT, false);
	}
	
	public static ConstantElement positiveInfinityInstance() {
		return new ConstantElement(ElementType.INFINITY_POS_ELEMENT, Float.POSITIVE_INFINITY);
	}
	
	public static ConstantElement negativeInfinityInstance() {
		return new ConstantElement(ElementType.INFINITY_NEG_ELEMENT, Float.NEGATIVE_INFINITY);
	}
	
	public static ConstantElement nanInstance() {
		return new ConstantElement(ElementType.NAN_ELEMENT, Float.NaN);
	}

	@Override
	public Object getValue() {
		return value;
	}
	
	public static ConstantElement instanceOf(ElementType constantElementType) {
		switch (constantElementType) {
			case NULL_ELEMENT:
				return nullInstance();
			case TRUE_ELEMENT:
				return trueInstance();
			case FALSE_ELEMENT:
				return falseInstance();
			case INFINITY_POS_ELEMENT:
				return positiveInfinityInstance();
			case INFINITY_NEG_ELEMENT:
				return negativeInfinityInstance();
			case NAN_ELEMENT:
				return nanInstance();
			default:
				return null;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	public static class Writer extends FixedSizeElement.Writer {
		
	}
	
	////////////////////////////////////////////////////////////////////////////
	/**
	 * Singleton map returning reader instances for constant types.
	 */
	public static class Reader implements BinaryElementReader {
		private static final Map<ElementType, Reader> INSTANCES = new HashMap<>();
		static {
			INSTANCES.put(NULL_ELEMENT, new Reader(nullInstance()));
			INSTANCES.put(TRUE_ELEMENT, new Reader(trueInstance()));
			INSTANCES.put(FALSE_ELEMENT, new Reader(falseInstance()));
			INSTANCES.put(INFINITY_POS_ELEMENT, new Reader(positiveInfinityInstance()));
			INSTANCES.put(INFINITY_NEG_ELEMENT, new Reader(negativeInfinityInstance()));
			INSTANCES.put(NAN_ELEMENT, new Reader(nanInstance()));
		}
		
		private final ConstantElement value;

		private Reader(ConstantElement value) {
			this.value = value;
		}
		
		/**
		 * Get the reader for this type.
		 * @param type type of reader
		 * @return reader that returns an instance of the requested type
		 */
		public static Reader getInstance(ElementType type) {
			return INSTANCES.get(type);
		}

		/**
		 * Returns a singleton Element for the constant type
		 * @param <E> Generic element type
		 * @param ctx Reader context - ignored.
		 * @return Singleton instance of the constant element type 
		 */
		@Override
		public <E extends Element> E read(ReaderContext ctx) {
			return (E) value;
		}
		
	}
}
