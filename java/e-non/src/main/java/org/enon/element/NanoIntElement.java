/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import java.io.OutputStream;
import org.enon.ReaderContext;
import org.enon.element.reader.BinaryElementReader;
import org.enon.exception.EnonElementException;

/**
 *
 * @author clininger $Id: $
 */
public class NanoIntElement extends FixedSizeElement implements ScalarElement, NumericElement {

	public static final int CONTENT_LENGTH = 0;
	
	private static final short NANOINT_BIAS = 191;
	public static final short MIN_VALUE = ElementType.MIN_NANO_INT_PREFIX - NANOINT_BIAS;
	public static final short MAX_VALUE = ElementType.MAX_NANO_INT_PREFIX - NANOINT_BIAS;

	private final byte value;

	/**
	 * Create a new NanoIntElement with the given unbiased value
	 * @param value Unbiased value
	 */
	public NanoIntElement(byte value) {
		super(ElementType.NANO_INT_ELEMENT, 0);
		if (isNanoInt(value)) {
			this.value = value;
		} else {
			throw new EnonElementException("Invalid value for nano-int: " + value);
		}
	}

	/**
	 * Test whether the value is within the nano-int range
	 * @param value value to test
	 * @return true if the value is in the nano-int range
	 */
	public static boolean isNanoInt(long value) {
		return value >= MIN_VALUE && value <= MAX_VALUE;
	}

	/**
	 * Unbias the prefix to determine the byte value
	 * @param prefix The biased prefix
	 * @return NanoIntElement instance
	 */
	public static NanoIntElement parse(byte prefix) {
		return new NanoIntElement((byte) (Byte.toUnsignedInt(prefix) - NANOINT_BIAS));
	}

	@Override
	public byte[] getContentBytes() {
		return EMPTY_BYTES;
	}

	/**
	 * @return the unbiased (byte) value of the nano-int.
	 */
	@Override
	public Byte getValue() {
		return value;
	}

	@Override
	public String toString() {
		return super.toString() + "(" + value + ")";
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends FixedSizeElement.Writer<NanoIntElement> {

		@Override
		protected void writeContent(NanoIntElement element, OutputStream out) throws IOException {
			// there is not content, only the prefix
		}

		@Override
		protected void writeHeader(NanoIntElement element, OutputStream out) throws IOException {
			out.write(new byte[]{(byte)(element.value + NANOINT_BIAS)});
		}

	}
	
	//////////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		private final byte biasedValue;

		public Reader(byte biasedValue) {
			this.biasedValue = biasedValue;
		}
		
		@Override
		public <E extends Element> E read(ReaderContext ctx) throws IOException {
			return (E) parse(biasedValue);
		}
		
	}
}
