/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.writer;

import org.enon.Prolog;
import org.enon.element.Element;

/**
 *
 * @author clininger $Id: $
 */
public interface ElementWriterFactory {
	/**
	 * Get a writer capable of writing the element.
	 * @param <E> The Element type
	 * @param e The element to write
	 * @return ElementWriter instance
	 */
	<E extends Element> ElementWriter<E> writer(E e);

	/**
	 * Get a writer to write the prolog. Default: Prolog.Writer.INSTANCE
	 * @param <W> Writer type
	 * @return Prolog.Writer instance
	 */
	default <W extends Prolog.Writer> W prologWriter() {
		return (W) Prolog.Writer.INSTANCE;
	}
}
