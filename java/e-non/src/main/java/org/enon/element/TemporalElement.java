/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.ReaderContext;
import org.enon.bind.temporal.EnonTemporalField;
import org.enon.bind.temporal.TemporalVO;
import org.enon.element.meta.MetaElement;
import org.enon.element.reader.BinaryElementReader;
import org.enon.element.writer.BinaryElementWriter;
import org.enon.exception.EnonReadException;
import org.enon.util.SerializeUtil;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import static org.enon.bind.temporal.TemporalVO.*;

/**
 * This element writes a numeric value as a String
 *
 * @author clininger $Id: $
 */
public class TemporalElement extends AbstractElement implements SubtypedElement<TemporalElementSubtype> {

	private static final byte MAP_END = (byte)'}';
	private static final int EIGHTEEN_HOURS = 18 * 60 * 60;
	private static final int FIFTEEN_MINUTES = 60 * 15;

	private final TemporalVO value;
	private final TemporalElementSubtype subType;

	/**
	 * Construct a TemporalElement from a field map.
	 *
	 * @param value The calendar field values
	 * @param subType subtype
	 * @param size The size that is computed for all of the fields in the map (see computeSize())
	 * @param meta Optional list of metadata
	 */
	protected TemporalElement(TemporalVO value, TemporalElementSubtype subType, long size, List<MetaElement> meta) {
		super(ElementType.TEMPORAL_ELEMENT, size, meta);
		this.value = value;
		this.subType = subType;
	}

	/**
	 * Create a TemporalElement using the fields provided.All fields in the map will be included, unless mutually exclusive.
	 * @param temporalVO temporal fields and their values
	 * @return A new CalendarElement
	 */
	public static TemporalElement create(TemporalVO temporalVO) {
		TemporalElementSubtype subType = computeSubtype(temporalVO);
		return new TemporalElement(temporalVO, subType, subType.size, null);
	}

	/**
	 * Determine the subtype based on the fields included
	 * @param temporalVO contains the fields to analyze
	 * @return The length of the element in bytes
	 */
	protected static TemporalElementSubtype computeSubtype(TemporalVO temporalVO) {
		// check eligibility for compact form
		if (temporalVO.year != null && (temporalVO.year < Short.MIN_VALUE || temporalVO.year > Short.MAX_VALUE)) {
			// year must be in short int range
			return TemporalElementSubtype.MAP;
		}
		if (temporalVO.offsetSeconds != null) {
			// offset must be no more than +/- 18hr and have exact 15 minute intervals
			if (Math.abs(temporalVO.offsetSeconds) > EIGHTEEN_HOURS || temporalVO.offsetSeconds % FIFTEEN_MINUTES != 0) {
				return TemporalElementSubtype.MAP;
			}
		}
		// look for exact match of a known subtype based on fieldMask
		switch(temporalVO.fieldMask) {
			case NONE_MASK:
				return TemporalElementSubtype.NONE;
			case YEAR_MASK:
				return TemporalElementSubtype.YEAR;
			case YEAR_MONTH_MASK:
				return TemporalElementSubtype.YEAR_MONTH;
			case FULL_DATE_MASK:
				return TemporalElementSubtype.FULL_DATE;
			case FULL_DATE_H_MASK:
				return TemporalElementSubtype.FULL_DATE_H;
			case FULL_DATE_HM_MASK:
				return TemporalElementSubtype.FULL_DATE_HM;
			case FULL_DATE_TIME_S_MASK:
				return TemporalElementSubtype.FULL_DATE_TIME;
			case FULL_DATE_TIME_MS_MASK:
				return TemporalElementSubtype.FULL_DATE_TIME_MS;
			case FULL_DATE_TIME_NS_MASK:
				return TemporalElementSubtype.FULL_DATE_TIME_NS;
			case OFFSET_DATE_TIME_S_MASK:
				return TemporalElementSubtype.OFFSET_DATE_TIME_S;
			case TIME_S_MASK:
				return TemporalElementSubtype.LOCAL_TIME_S;
			case OFFSET_TIME_HM_MASK:
				return TemporalElementSubtype.OFFSET_TIME_HM;
			case OFFSET_TIME_S_MASK:
				return TemporalElementSubtype.OFFSET_TIME_S;
			case ZONED_TIME_S_MASK:
			case ZONED_TIME_MS_MASK:
				return TemporalElementSubtype.ZONED_TIME_MS;
			case INSTANT_MASK:
				return TemporalElementSubtype.INSTANT_UTC;
			case OFFSET_INSTANT_MASK:
				return TemporalElementSubtype.OFFSET_INSTANT;
			case ZONED_INSTANT_MASK:
				return TemporalElementSubtype.ZONED_INSTANT;
		}

		// an exact match of fields was not found.  Apply a more rigorous search
		// taking into account defaults for time fields, which would have caused
		// their bits to be unset in the fieldMask

		// check for a full date, YMD
		if ((temporalVO.fieldMask & FULL_DATE_MASK) == FULL_DATE_MASK) {
			// a full date is provided.  Determine how many other fields are needed
			// work from most accurate to least accurate

			// is there a zoneId?
			if (temporalVO.zoneId != null) {
				// no full-date subtypes allow a zoneId.  Use a map.
				return TemporalElementSubtype.MAP;
			}

			// are nanos present?
			if ((temporalVO.fieldMask & NANO_MASK) != 0) {
				return TemporalElementSubtype.FULL_DATE_TIME_NS;
			}

			// are millis present?
			if ((temporalVO.fieldMask & MILLI_MASK) != 0) {
				return TemporalElementSubtype.FULL_DATE_TIME_MS;
			}

			// are seconds present?
			if ((temporalVO.fieldMask & SECOND_MASK) != 0) {
				// there is an offset option for date-time with seconds
				if (temporalVO.offsetSeconds != null) {
					return TemporalElementSubtype.OFFSET_DATE_TIME_S;
				}
				return TemporalElementSubtype.FULL_DATE_TIME;
			}

			// are minutes present?
			if ((temporalVO.fieldMask & MINUTE_MASK) != 0) {
				return TemporalElementSubtype.FULL_DATE_HM;
			}

			// are hours present?
			if ((temporalVO.fieldMask & HOUR_MASK) != 0) {
				return TemporalElementSubtype.FULL_DATE_H;
			}

			// no time fields are present - just a full date (should have been matched by the switch above)
			return TemporalElementSubtype.FULL_DATE;
		}

		// check for absence of all date fields, YMD
		if ((temporalVO.fieldMask & FULL_DATE_MASK) == 0) {
			// no date fields are provided.  Determine which time fields are needed
			// work from most accurate to least accurate

			// are nanos present?
			if ((temporalVO.fieldMask & NANO_MASK) != 0) {
				// no options with nanos
				return TemporalElementSubtype.MAP;
			}
			// are millis present?
			if ((temporalVO.fieldMask & MILLI_MASK) != 0) {
				// if there are millis, need a zoneId
				if (temporalVO.isZoned()) {
					return TemporalElementSubtype.ZONED_TIME_MS;
				}
				return TemporalElementSubtype.MAP;
			}
			//are seconds present?
			if ((temporalVO.fieldMask & SECOND_MASK) != 0) {
				if (temporalVO.isZoned()) {
					// zoneId not supported
					return TemporalElementSubtype.MAP;
				}
				if (temporalVO.isOffset()) {
					return TemporalElementSubtype.OFFSET_TIME_S;
				}
				return TemporalElementSubtype.LOCAL_TIME_S;
			}
			// are minutes present?
			if ((temporalVO.fieldMask & MINUTE_MASK) != 0) {
				if (temporalVO.isOffset()) {
					return TemporalElementSubtype.OFFSET_TIME_HM;
				}
				return TemporalElementSubtype.MAP;
			}

			// are hours present?
			if ((temporalVO.fieldMask & HOUR_MASK) != 0) {
				// there is no subtype for just hours
				return TemporalElementSubtype.MAP;
			}

			// finally, no date and no time fields (should have been matched by teh switch above)
			return TemporalElementSubtype.NONE;
		}

		return TemporalElementSubtype.MAP;

	}

	@Override
	public TemporalVO getValue() {
		return value;
	}



	@Override
	public TemporalElementSubtype getSubType() {
		return subType;
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Writer extends BinaryElementWriter<TemporalElement> {
		private final SerializeUtil serializeUtil = SerializeUtil.getInstance();

		/**
		 * Override the default behavior: write the Array prefix then the entry type prefix.
		 * @param element The element to write
		 * @param out stream on which to write
		 * @throws IOException on io error
		 */
		@Override
		protected void writeHeader(TemporalElement element, OutputStream out) throws IOException {
			out.write(ElementType.TEMPORAL_ELEMENT.prefix);
			out.write(element.subType.prefix);
		}

		// by redeclaring this method here we can access it in the containing class
		@Override
		protected void writeContent(TemporalElement element, OutputStream out) throws IOException {
			TemporalVO tvo = element.getValue();
			switch (element.subType) {
				case NONE:
					break;

				case YEAR:
					out.write(serializeUtil.networkBytes(tvo.year.shortValue()));
					break;

				case YEAR_MONTH:
					out.write(serializeUtil.networkBytes(tvo.year.shortValue()));
					out.write(serializeUtil.networkBytes(tvo.month));
					break;

				case FULL_DATE:
					out.write(serializeUtil.networkBytes(tvo.year.shortValue()));
					out.write(serializeUtil.networkBytes(tvo.month));
					out.write(serializeUtil.networkBytes(tvo.day));
					break;

				case FULL_DATE_H:
					out.write(serializeUtil.networkBytes(tvo.year.shortValue()));
					out.write(serializeUtil.networkBytes(tvo.month));
					out.write(serializeUtil.networkBytes(tvo.day));
					out.write(serializeUtil.networkBytes(tvo.hour));
					break;

				case FULL_DATE_HM:
					out.write(serializeUtil.networkBytes(tvo.year.shortValue()));
					out.write(serializeUtil.networkBytes(tvo.month));
					out.write(serializeUtil.networkBytes(tvo.day));
					out.write(serializeUtil.networkBytes(tvo.hour));
					out.write(serializeUtil.networkBytes(tvo.minute));
					break;

				case FULL_DATE_TIME:
					out.write(serializeUtil.networkBytes(tvo.year.shortValue()));
					out.write(serializeUtil.networkBytes(tvo.month));
					out.write(serializeUtil.networkBytes(tvo.day));
					out.write(serializeUtil.networkBytes(tvo.hour));
					out.write(serializeUtil.networkBytes(tvo.minute));
					out.write(serializeUtil.networkBytes(tvo.sec));
					break;

				case FULL_DATE_TIME_MS:
					out.write(serializeUtil.networkBytes(tvo.year.shortValue()));
					out.write(serializeUtil.networkBytes(tvo.month));
					out.write(serializeUtil.networkBytes(tvo.day));
					out.write(serializeUtil.networkBytes(tvo.hour));
					out.write(serializeUtil.networkBytes(tvo.minute));
					out.write(serializeUtil.networkBytes((int)tvo.getMsInMinute()));
					break;

				case FULL_DATE_TIME_NS:
					out.write(serializeUtil.networkBytes(tvo.year.shortValue()));
					out.write(serializeUtil.networkBytes(tvo.month));
					out.write(serializeUtil.networkBytes(tvo.day));
					out.write(serializeUtil.networkBytes(tvo.hour));
					out.write(serializeUtil.networkBytes(tvo.minute));
					out.write(serializeUtil.networkBytes(tvo.nanos));
					break;

				case OFFSET_DATE_TIME_S:
					out.write(serializeUtil.networkBytes(tvo.year.shortValue()));
					out.write(serializeUtil.networkBytes(tvo.month));
					out.write(serializeUtil.networkBytes(tvo.day));
					out.write(serializeUtil.networkBytes(tvo.hour));
					out.write(serializeUtil.networkBytes(tvo.minute));
					out.write(serializeUtil.networkBytes(tvo.sec));
					out.write(serializeUtil.networkBytes((byte)(tvo.getOffsetIntervals())));
					break;

				case LOCAL_TIME_S:
					out.write(serializeUtil.networkBytes(tvo.hour));
					out.write(serializeUtil.networkBytes(tvo.minute));
					out.write(serializeUtil.networkBytes(tvo.sec));
					break;

				case OFFSET_TIME_HM:
					out.write(serializeUtil.networkBytes(tvo.hour));
					out.write(serializeUtil.networkBytes(tvo.minute));
					out.write(serializeUtil.networkBytes((byte)(tvo.getOffsetIntervals())));
					break;

				case OFFSET_TIME_S:
					out.write(serializeUtil.networkBytes(tvo.hour));
					out.write(serializeUtil.networkBytes(tvo.minute));
					out.write(serializeUtil.networkBytes(tvo.sec));
					out.write(serializeUtil.networkBytes((byte)(tvo.getOffsetIntervals())));
					break;

				case ZONED_TIME_MS: {
					out.write(serializeUtil.networkBytes(tvo.hour));
					out.write(serializeUtil.networkBytes(tvo.minute));
					Integer msInMinute = tvo.getMsInMinute();
					if (msInMinute != null) {
						out.write(serializeUtil.networkBytes(msInMinute.shortValue()));
					} else {
						out.write(serializeUtil.networkBytes((short)(tvo.sec*1000)));
					}

					out.write((byte)tvo.zoneId.length());
					out.write(tvo.zoneId.getBytes(CHARSET));
					break;
				}

				case INSTANT_UTC:
					out.write(serializeUtil.networkBytes(tvo.instant));
					break;

				case OFFSET_INSTANT:
					out.write(serializeUtil.networkBytes(tvo.instant));
					out.write(serializeUtil.networkBytes((byte)(tvo.getOffsetIntervals())));
					break;

				case ZONED_INSTANT:
					out.write(serializeUtil.networkBytes(tvo.instant));
					out.write((byte)tvo.zoneId.length());
					out.write(tvo.zoneId.getBytes(CHARSET));
					break;

				case MAP:
					Integer msInMinute = (Integer) tvo.get(EnonTemporalField.MS_IN_MINUTE);
					for (EnonTemporalField field : EnonTemporalField.values()) {
						Object value = tvo.get(field);
							switch (field) {
								case HOUR:
								case MINUTE:
								case OFFSET_INTERVALS:
									// can default to 0
									writeNumericMapEntry(out, tvo, field, 0L);
									break;

								case SECOND:
								case NANOS:
									// only write seconds & nanos if MS_IN_MINUTE has no value
									if (msInMinute == null || msInMinute == 0) {
										// can default to 0
										writeNumericMapEntry(out, tvo, field, 0L);
									}
									break;

								case MS_IN_MINUTE:
									if (msInMinute != null && msInMinute != 0) {
										out.write(field.key);
										out.write(serializeUtil.networkBytes(msInMinute.shortValue()));
									}
									break;

								case OFFSET_SECONDS:
									if (tvo.getOffsetIntervals() == 0) {
										writeNumericMapEntry(out, tvo, field, 0L);
									}
									break;

								case ZONE_ID:
									if (value != null) {
										// ZONE_ID is not numeric: write as a string
										out.write(field.key);
										out.write((byte)((String)value).length());
										out.write(((String)value).getBytes(CHARSET));
									}
									break;

								default:
									// can't default: must be written if not null.
									writeNumericMapEntry(out, tvo, field, null);
									break;
						}
					}
					out.write(MAP_END);
					break;
			}
		}

		/**
		 * Write the field to the output stream if not null and not default.
		 *
		 * @param out the output stream to write
		 * @param tvo The VO holding the value
		 * @param field The field within the VO
		 * @param defaultValue Default value or null if the value can't be defaulted
		 */
		private void writeNumericMapEntry(OutputStream out, TemporalVO tvo, EnonTemporalField field, Number defaultValue) throws IOException {
			Number value = (Number) tvo.get(field);
			if (value != null && (defaultValue == null || defaultValue.longValue() != value.longValue())) {
				out.write(field.key);
				out.write(serializeUtil.networkBytes(value));
			}
		}
	}


	/////////////////////////////////////////////////////////////////////////////
	public static class Reader implements BinaryElementReader {

		private final TemporalElementSubtype subtype;

		/**
		 * Create an instance that expects to read the subtype from the front of the stream
		 */
		public Reader() {
			subtype = null;
		}

		/**
		 * Create an instance specifically to read this subtype
		 * @param subtype subtype
		 */
		public Reader(TemporalElementSubtype subtype) {
			this.subtype = subtype;
		}

		@Override
		public TemporalElement read(ReaderContext ctx) throws IOException {
			TemporalElementSubtype readingSubtype = this.subtype;
			if (readingSubtype == null) {
				int subTypeByte = ctx.getInputStream().read();
				if (subTypeByte == -1) {
					throw new EnonReadException("End of stream reached while expcting temporal element subtype");
				}
				readingSubtype = TemporalElementSubtype.forPrefix((byte)subTypeByte);
				if (readingSubtype == null) {
					throw new EnonReadException("Unrecognized temporal subtype: "+subTypeByte+"( "+(char)subTypeByte+" )");
				}
			}

			TemporalVO.Builder tvoBuilder = new Builder();
			DataInputStream in = ctx.getDataInputStream();
			switch (readingSubtype) {
				case NONE:
					break;

				case YEAR:
					tvoBuilder.year(in.readShort());
					break;

				case YEAR_MONTH:
					tvoBuilder.year(in.readShort());
					tvoBuilder.month(in.readByte());
					break;

				case FULL_DATE:
					tvoBuilder.year(in.readShort());
					tvoBuilder.month(in.readByte());
					tvoBuilder.day(in.readByte());
					break;

				case FULL_DATE_H:
					tvoBuilder.year(in.readShort());
					tvoBuilder.month(in.readByte());
					tvoBuilder.day(in.readByte());
					tvoBuilder.hour(in.readByte());
					break;

				case FULL_DATE_HM:
					tvoBuilder.year(in.readShort());
					tvoBuilder.month(in.readByte());
					tvoBuilder.day(in.readByte());
					tvoBuilder.hour(in.readByte());
					tvoBuilder.minute(in.readByte());
					break;

				case FULL_DATE_TIME:
					tvoBuilder.year(in.readShort());
					tvoBuilder.month(in.readByte());
					tvoBuilder.day(in.readByte());
					tvoBuilder.hour(in.readByte());
					tvoBuilder.minute(in.readByte());
					tvoBuilder.second(in.readByte());
					break;

				case FULL_DATE_TIME_MS: {
					tvoBuilder.year(in.readShort());
					tvoBuilder.month(in.readByte());
					tvoBuilder.day(in.readByte());
					tvoBuilder.hour(in.readByte());
					tvoBuilder.minute(in.readByte());
					int millisInMinute = in.readUnsignedShort();
					tvoBuilder.second((byte)(millisInMinute / 1000));
					tvoBuilder.nanos((millisInMinute % 1000) * 1000000);
					break;
				}

				case FULL_DATE_TIME_NS:
					tvoBuilder.year(in.readShort());
					tvoBuilder.month(in.readByte());
					tvoBuilder.day(in.readByte());
					tvoBuilder.hour(in.readByte());
					tvoBuilder.minute(in.readByte());
					tvoBuilder.second(in.readByte());
					tvoBuilder.nanos(in.readInt());
					break;

				case LOCAL_TIME_S:
					tvoBuilder.hour(in.readByte());
					tvoBuilder.minute(in.readByte());
					tvoBuilder.second(in.readByte());
					break;

				case INSTANT_UTC:
					tvoBuilder.instant(in.readLong());
					break;

				case OFFSET_DATE_TIME_S:
					tvoBuilder.year(in.readShort());
					tvoBuilder.month(in.readByte());
					tvoBuilder.day(in.readByte());
					tvoBuilder.hour(in.readByte());
					tvoBuilder.minute(in.readByte());
					tvoBuilder.second(in.readByte());
					tvoBuilder.offsetSeconds(in.readByte()*FIFTEEN_MINUTES);
					break;

				case OFFSET_TIME_HM:
					tvoBuilder.hour(in.readByte());
					tvoBuilder.minute(in.readByte());
					tvoBuilder.offsetSeconds(in.readByte()*FIFTEEN_MINUTES);
					break;

				case OFFSET_TIME_S:
					tvoBuilder.hour(in.readByte());
					tvoBuilder.minute(in.readByte());
					tvoBuilder.second(in.readByte());
					tvoBuilder.offsetSeconds(in.readByte()*FIFTEEN_MINUTES);
					break;

				case OFFSET_INSTANT:
					tvoBuilder.instant(in.readLong());
					tvoBuilder.offsetSeconds(in.readByte()*FIFTEEN_MINUTES);
					break;

				case ZONED_TIME_MS: {
					tvoBuilder.hour(in.readByte());
					tvoBuilder.minute(in.readByte());
					int millisInMinute = in.readUnsignedShort();
					tvoBuilder.second((byte)(millisInMinute / 1000));
					tvoBuilder.nanos((millisInMinute % 1000) * 1000000);
					tvoBuilder.zoneId(readZoneId(in));
					break;
				}

				case ZONED_INSTANT:
					tvoBuilder.instant(in.readLong());
					tvoBuilder.zoneId(readZoneId(in));
					break;

				case MAP:
					byte fieldKey = readNextFieldKey(in);
					while (MAP_END != fieldKey) {
						EnonTemporalField field = EnonTemporalField.forKey((byte) fieldKey);
						if (field == null) {
							throw new EnonReadException("Unrecognized field key: " + fieldKey + "( " + (char) fieldKey + " )");
						}
						switch (field) {
							case INSTANT:
								tvoBuilder.instant(in.readLong());
								break;
							case YEAR:
								tvoBuilder.year(in.readInt());
								break;
							case MONTH:
								tvoBuilder.month(in.readByte());
								break;
							case DAY:
								tvoBuilder.day(in.readByte());
								break;
							case HOUR:
								tvoBuilder.hour(in.readByte());
								break;
							case MINUTE:
								tvoBuilder.minute(in.readByte());
								break;
							case SECOND:
								tvoBuilder.second(in.readByte());
								break;
							case NANOS:
								tvoBuilder.nanos(in.readInt());
								break;
							case MS_IN_MINUTE:
								int msInMinute = in.readUnsignedShort();
								tvoBuilder.second((byte)(msInMinute/1000)).nanos((msInMinute%1000)*1000000);
								break;
							case OFFSET_SECONDS:
								tvoBuilder.offsetSeconds(in.readInt());
								break;
							case OFFSET_INTERVALS:
								tvoBuilder.offsetSeconds(in.readByte()*FIFTEEN_MINUTES);
								break;
							case ZONE_ID:
								tvoBuilder.zoneId(readZoneId(in));
								break;
							default:
								throw new EnonReadException("Unexpected temporal field: "+field);
						}
						fieldKey = readNextFieldKey(in);
					}
					break;

				default:
					throw new EnonReadException("Unexpected temporal subtype: "+readingSubtype);
			}
			return create(tvoBuilder.build());
		}

		private String readZoneId(DataInputStream in) throws IOException {
			byte[] bytes = new byte[in.readByte()];  // read the string byte-length & create an array that size
			int readSize = in.read(bytes);
			if (readSize < bytes.length) {
				throw new EnonReadException("Could not read full length of zone ID");
			}
			return new String(bytes, CHARSET);
		}

		private byte readNextFieldKey(DataInputStream in) throws IOException {
			int fieldKey = in.read();
			if (fieldKey == -1) {
				throw new EnonReadException("End of stream reached while expectin temporal field key");
			}
			return (byte)fieldKey;
		}
	}
}
