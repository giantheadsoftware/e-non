/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Set;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public class Prolog {

	/**
	 * the version of the eNON format that follows this prolog.
	 */
	private final short version;
	/**
	 * the features required by the data that follows this prolog.
	 */
	private final Set<FeatureSet> requiredFeatures;

	public Prolog(short version, Set<FeatureSet> requiredFeatures) {
		this.version = version;
		this.requiredFeatures = Collections.unmodifiableSet(requiredFeatures);
	}

	public short getVersion() {
		return version;
	}

	public Set<FeatureSet> getRequiredFeatures() {
		return requiredFeatures;
	}

	/**
	 * Test the version and features of this prolog for compatibility with the config provided.
	 *
	 * @param config The capabilities of this reader
	 * @throws EnonReadException if this prolog exceeds the features of the config.
	 */
	public void testReadability(EnonConfig config) {
		if (version < config.getMinReadVersion() || version > EnonConfig.FORMAT_VERSION) {
			throw new EnonReadException("Can't read eNON version " + version);
		}
		if (!config.getFeatures().containsAll(requiredFeatures)) {
			throw new EnonReadException("Can't read eNON with features " + requiredFeatures
					+ ". Only features " + config.getFeatures() + " are supported by this reader");
		}
	}

	/**
	 * Test the version and features of this prolog for compatibility with the config provided.
	 *
	 * @param config The capabilities of this reader
	 * @throws EnonReadException if this prolog exceeds the features of the config.
	 */
	public void testWritability(EnonConfig config) {
		if (version != EnonConfig.FORMAT_VERSION) {
			throw new EnonReadException("Can't write eNON version " + version);
		}
		if (!config.getFeatures().containsAll(requiredFeatures)) {
			throw new EnonReadException("Can't write eNON with features " + requiredFeatures
					+ ". Only features " + config.getFeatures() + " are supported by this writer");
		}
	}

	@Override
	public String toString() {
		String features = "";
		for (FeatureSet feature : requiredFeatures) {
			features += feature.name();
		}

		return "Prolog{version: " + version
				+ "; features: \"" + features + "\"}";
	}

	///////////////////////////////////////////////////////////////////////////////////
	/**
	 * Default Prolog writer: writes binary prolog
	 */
	public static class Writer {
		public static final Writer INSTANCE = new Writer();

		public void write(Prolog prolog, OutputStream out) throws IOException {
			// write the version as an unsigned byte
			out.write(prolog.getVersion());
			out.write(FeatureSet.toBitmask(prolog.getRequiredFeatures()));
		}


	}

	///////////////////////////////////////////////////////////////////////////////////
	/**
	 * Default Prolog reader: reads binary prolog
	 */
	public static class Reader<RC extends ReaderContext> {
		public static final Reader INSTANCE = new Reader();

		public Prolog read(RC ctx) throws IOException {
			short version = (short) ctx.getDataInputStream().readUnsignedByte();

			byte featureBits = ctx.getDataInputStream().readByte();
			Set<FeatureSet> requiredFeatures = FeatureSet.fromBitmask(featureBits);

			Prolog prolog = new Prolog(version, requiredFeatures);
			return prolog;
		}

	}
}
