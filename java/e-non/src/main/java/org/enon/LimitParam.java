/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

/**
 * Set of parameters that can be limited by the eNON reader/writer.
 *
 * @author clininger $Id: $
 */
public enum LimitParam {
	MAX_TOP_LEVEL_ELEMENT_COUNT,
	MAX_ELEMENT_SIZE,
	MAX_LIST_ELEMENT_COUNT,
	MAX_MAP_PROPERTY_COUNT,
	MAX_MAP_KEY_SIZE,
	MAX_BLOB_SIZE,
	MAX_STRING_LENGTH

}
