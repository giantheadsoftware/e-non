/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.exception.EnonDecoderException;
import org.enon.util.ReflectUtil;

/**
 *
 * @author clininger $Id: $
 */
public class CharacterDecoder implements Decoder<Character> {

	protected CharacterDecoder() {
	}

	@Override
	public Character decode(Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}

		switch (e.getType()) {
			case BYTE_ELEMENT:
			case NANO_INT_ELEMENT:
			case SHORT_ELEMENT:
				return numberToChar(Short.toUnsignedInt(((Number)e.getValue()).shortValue()));

			case INT_ELEMENT:
			case LONG_ELEMENT:
				long value = ((Number)e.getValue()).longValue();
				if (value < 0 || value > Integer.MAX_VALUE) {
					throw new EnonDecoderException("Numeric value "+value+" is not a valid codepoint.");
				}
				return numberToChar((int)value);

			case NUMBER_ELEMENT:
			case STRING_ELEMENT:
				String strValue = String.valueOf(e.getValue());
				if (strValue.length() == 1) {
					return strValue.charAt(0);
				}
				throw new EnonDecoderException("Can't interpret string as char: "+strValue);
		}
		throw new EnonDecoderException("Unable to decode " + e.getType() + " to boolean");
	}

	protected char numberToChar(int codePoint) {
		char[] chars = Character.toChars(codePoint);
		if (chars.length == 1) {
			return chars[0];
		}
		throw new EnonDecoderException("Codepoint "+codePoint+" requires more than 1 char to represent.");
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<CharacterDecoder> {

		public static final CharacterDecoder INSTANCE = new CharacterDecoder();

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return (Character.TYPE == targetType || Character.class.isAssignableFrom(ReflectUtil.getinstance().classOfType(targetType)))
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected CharacterDecoder create(Type targetType) {
			return INSTANCE;
		}

	}
}
