/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.enon.ReaderContext;
import org.enon.element.ElementType;
import org.enon.element.MapElement;
import org.enon.element.Element;
import org.enon.exception.EnonDecoderException;
import org.enon.util.ReflectUtil;

/**
 *
 * @author clininger $Id: $
 * @param <M> The map type produced by this decoder
 */
public class MapDecoder<M extends Map> implements Decoder<M> {

	/** cache constructors to minimize lookup time */
	protected static final Map<String, Constructor<? extends Map>> CONSTRUCTOR_CACHE = new WeakHashMap<>();

	protected final Class<M> targetType;
	protected final Type keyType;
	protected final Type valueType;

	protected MapDecoder(Class<M> targetType, Type keyType, Type valueType) {
		this.targetType = targetType;
		this.keyType = keyType;
		this.valueType = valueType;
	}

	@Override
	public M decode(final Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}

		if (elementType == ElementType.MAP_ELEMENT) {
			final M map = newMap((int) e.getSize().getSize());

			ReaderContext ctx = e.getReaderContext();
			final DecoderFactory decoderFactory = ctx.getConfig().getDecoderFactory();
			final Decoder keyDecoder = decoderFactory.decoderFor(keyType);
			final Decoder valueDecoder = decoderFactory.decoderFor(valueType);

			((MapElement<Element, Element>)e).getValue().stream()
					.forEach(entry -> {
						Object key = keyDecoder.decode(entry.getValue().getKey() );
						Object value = valueDecoder.decode(entry.getValue().getValue() );
						map.put(key, value);
					});

			return map;
		}

		throw new EnonDecoderException("Unable to decode " + e.getType() + " to map");
	}


	protected M newMap(int size) {

		try {
			if (targetType == null || Modifier.isAbstract(targetType.getModifiers()))
				return defaultMap(size);

			final Constructor<M> constructor = findConstructor();
			final M map = constructor.newInstance(size);

			return map;
		} catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException x) {
			throw new EnonDecoderException("Unable to construct collection instance for type " + targetType.getName(), x);
		}
	}

	/**
	 * Attempt to locate a constructor for the target collection type.
	 * @return a constructor for the targetType map
   * @throws EnonDecoderException if no suitable constructor is found
	 */
	protected Constructor<M> findConstructor() {
		Constructor<M> constructor;
		constructor = (Constructor<M>) CONSTRUCTOR_CACHE.get(targetType.getName());

		if (constructor != null) {
			return constructor;
		}
		// not cached, have to locate
		try {
			constructor = targetType.getConstructor(int.class);
			synchronized (CONSTRUCTOR_CACHE) {
				// synchronized to prevent conncurrent modification.
				// Don't care if a value gets overwritten in the rare concurrent case; should be the same value.
				CONSTRUCTOR_CACHE.put(targetType.getName(), constructor);
			}
			return constructor;
		} catch (NoSuchMethodException x) {
			throw new EnonDecoderException("Unable to find suitable constructor for type " + targetType.getName());
		}
	}

	/**
	 * Instantiate a default collection when the targetType is null or abstract.<p>
	 * Defaults:
	 * <ul>
	 * <li>Map -&gt; LinkedHashMap</li>
	 * <li>SortedMap -&gt; TreeMap</li>
	 * <li>NavigableMap -&gt; TreeMap</li>
	 * <li>ConcurrentMap -&gt; ConcurrentHashMap</li>
	 * <li>null -&gt; LinkedHashMap</li>
	 * </ul>
	 * @param size The initial size
	 * @return A new map instance.
	 * @throws EnonDecoderException if a default is not defined for the targetType
	 */
	protected M defaultMap(int size) {
		if (targetType == null || Map.class == targetType) {
			return (M) new LinkedHashMap(size);
		}
		if (SortedMap.class == targetType || NavigableMap.class == targetType) {
			return (M) new TreeMap();
		}
		if (ConcurrentMap.class == targetType) {
			return (M) new ConcurrentHashMap(size);
		}
		throw new EnonDecoderException("No default for abstract map type "+targetType.getName());
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<MapDecoder> {

		private final MapDecoder nullInstance = new MapDecoder(null, null, null);

		/**
		 * Override the default to allow handling of null type.  Calls super for non null types.
		 * @param targetType the type to decode for
		 * @return decoder for the target type
		 */
		@Override
		public MapDecoder decoderFor(Type targetType) {
			if (targetType == null) {
				return nullInstance;
			}
			return super.decoderFor(targetType);
		}

		/**
		 * Override the default to allow handling of null type.  Calls super for non null types.
		 * @param targetTypeName the type to decode for
		 * @return decoder for the target type
		 */
		@Override
		public MapDecoder decoderFor(String targetTypeName) {
			if (targetTypeName == null) {
				return nullInstance;
			}
			return super.decoderFor(targetTypeName);
		}

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return (Map.class.isAssignableFrom(ReflectUtil.getinstance().classOfType(targetType)))
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		/**
		 * Create a MapDecoder from the type provided.  If the type is a ParameterizedType, then it may contain information about
		 * the key and value types of the map.
		 * @param type The Map type to be produced by the MapDecoder
		 * @return a new MapDecoder instance
		 */
		@Override
		protected MapDecoder create(Type type) {
			if (type == null) {
				// return a decoder that creates the default collection type, the items are decoded using NaturalDecoder
				return nullInstance;
			}
			Class targetClass = ReflectUtil.getinstance().classOfType(type);
			Type[] collectionTypeArgs = ReflectUtil.getinstance().findArgsOfSuper(Map.class, type);
			return new MapDecoder(targetClass, collectionTypeArgs[0], collectionTypeArgs[1]);
		}

		/**
		 * Special factory method when the target is known to be a Map.
		 * @param targetClass Type of map.  If null or abstract, an appropriate default is used.
		 * @param keyType key type.  If null, Object.class is used
		 * @param valueType Value type.  If null, Object is used.
		 * @return Decoder that produces the map type.
		 */
		public MapDecoder decoderFor(Class<? extends Map> targetClass, Type keyType, Type valueType) {
			final String targetTypeName = targetClass != null ? targetClass.getName() : "null";
			final String keyTypeName = keyType != null ? keyType.getTypeName(): "null";
			final String valueTypeName = valueType != null ? valueType.getTypeName(): "null";
			final String cacheKey = targetTypeName + '<' + keyTypeName + ", " + valueTypeName + '>';

			MapDecoder instance = getDecoderCache().get(cacheKey);
			if (instance != null) {
				return instance;
			}
			cacheAcceptLevel(cacheKey, AcceptLevel.PREFER);
			return cacheDecoder(cacheKey, new MapDecoder(targetClass, keyType, valueType));
		}
	}
}
