/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.enon.element.ElementType;
import org.enon.element.Element;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.enon.util.IntType;
import org.enon.util.ReflectUtil;

/**
 * This decoder just takes the "natural" value from the element without further translation.
 *
 * @author clininger $Id: $
 */
public class EnumDecoder implements Decoder<Enum> {
  protected final Class<Enum> enumTargetType;
  protected final Map<String, Enum> enumValuesByName;

  protected EnumDecoder(Class<Enum> enumType) {
    this.enumTargetType = enumType;
    enumValuesByName = Arrays.stream(enumTargetType.getEnumConstants()).collect(Collectors.toMap(e -> e.name(), e -> e));
  }

  @Override
  public Enum decode(final Element e) {
    if (e.getType() == ElementType.NULL_ELEMENT) {
      return null;
    }
    switch(e.getType()) {
      case STRING_ELEMENT:
        Enum enumValue = enumValuesByName.get(((StringElement)e).getValue());
        if (enumValue != null) {
          return enumValue;
        }
        throw new EnonDecoderException("Enum " + enumTargetType.getName() + " does not contain a value " + (((StringElement)e).getValue()));

      case BYTE_ELEMENT:
      case INT_ELEMENT:
      case LONG_ELEMENT:
      case NANO_INT_ELEMENT:
      case SHORT_ELEMENT:
      case FLOAT_ELEMENT:
      case DOUBLE_ELEMENT:
        // if the element numeric, attempt to scale/cast the value to the target type
        int ord = IntType.INT_OBJECT.cast((Number)e.getValue()).intValue();
        if (ord >=0 && ord < enumValuesByName.size()) {
          return enumTargetType.getEnumConstants()[ord];
        }
        throw new EnonDecoderException("Enum index out of range for "+enumTargetType.getName()+": "+ord);
    }
		throw new EnonDecoderException("Unable to decode " + e.getType() + " to " + enumTargetType.getName());
  }

  /////////////////////////////////////////////////////////////////////
  public static class Acceptor extends TargetTypeAcceptor<EnumDecoder> {
    private final Map<String, EnumDecoder> decodersByEnumName = new HashMap<>();

    @Override
    protected AcceptLevel evaluate(Type targetType) {
      return (Enum.class.isAssignableFrom(ReflectUtil.getinstance().classOfType(targetType)))
          ? AcceptLevel.PREFER : AcceptLevel.NEVER;
    }

    @Override
    protected EnumDecoder create(Type targetType) {
      EnumDecoder decoder = decodersByEnumName.get(targetType.getTypeName());
      if (decoder != null) {
        return decoder;
      }
      Class<Enum> enumType = ReflectUtil.getinstance().classOfType(targetType);
      if (enumType != null) {
        decoder = new EnumDecoder(enumType);
        decodersByEnumName.put(targetType.getTypeName(), decoder);
        return decoder;
      }
      return null;
    }

  }
}
