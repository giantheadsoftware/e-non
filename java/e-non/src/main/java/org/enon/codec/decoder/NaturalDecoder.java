/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;
import org.enon.element.Element;

/**
 * This decoder just takes the "natural" value from the element without further translation.
 *
 * @author clininger $Id: $
 */
public class NaturalDecoder implements Decoder<Object> {
	protected final NumberDecoder numberDecoder = new NumberDecoder.Acceptor().decoderFor(Number.class);
	protected final CollectionDecoder collectionDecoder = new CollectionDecoder.Acceptor().decoderFor(Collection.class);
	protected final MapDecoder mapDecoder = new MapDecoder.Acceptor().decoderFor(Map.class);

	protected NaturalDecoder() {
	}

	@Override
	public Object decode(final Element e) {
		switch(e.getType()) {
			case LIST_ELEMENT:
				return collectionDecoder.decode(e);
			case MAP_ELEMENT:
				return mapDecoder.decode(e);
			case NUMBER_ELEMENT:
				// by default, NumberElement returns a String value.  Decoding it as a Number makes more sense.
				return numberDecoder.decode(e);
			default:
				return e.getValue();
		}
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<NaturalDecoder> {

		public static final NaturalDecoder INSTANCE = new NaturalDecoder();

		/**
		 * Override the default to accept null.
		 */
		@Override
		public NaturalDecoder decoderFor(Type targetType) {
			if (targetType == null) {
				return INSTANCE;
			}
			return super.decoderFor(targetType);
		}

		/**
		 * Override the default to accept null.
		 */
		@Override
		public NaturalDecoder decoderFor(String targetTypeName) {
			if (targetTypeName == null) {
				return INSTANCE;
			}
			return super.decoderFor(targetTypeName);
		}

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return targetType == null || Object.class == reflectUtil.classOfType(targetType)
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected NaturalDecoder create(Type targetType) {
			return INSTANCE;
		}

	}

}
