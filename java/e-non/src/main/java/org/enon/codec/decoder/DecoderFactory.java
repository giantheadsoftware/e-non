/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 *
 * @author clininger $Id: $
 * @param <D> The type of decoder produced by this factory
 */
public interface DecoderFactory<D extends Decoder> {

  /**
   * Return a decoder capable of producing the target type.
   *
   * @param targetType The type that the decoder must create. If null, the decoder may choose a default type based on the Element that is being decoded.
   * @return a decoder for the target type
   */
  D decoderFor(Type targetType);

  /**
   * Return a decoder capable of producing the target type, identified by name.
   *
   * @param targetTypeName The type that the decoder must create. This is not necessarily a classname if a type mapping is available.
   *                       If null, the decoder may choose a default type based on the Element that is being decoded.
   * @return a decoder for the target type
   */
  D decoderFor(String targetTypeName);

  /**
   * Return a decoder that can decode a ListElement into a collection. The desired collection type and element type can be specified.
   *
   * @param targetClass The Collection type that should be created by the decoder. If abstract, a default implementation will be created,
   *                    compatible with the provided abstract type. If null, a default collection type will be created.
   * @param itemType    The type of item expected in the final collection. If null, the natural value of the elements will be produced.
   * @return A decoder capable of decoding a ListElement and creating a Collection.
   */
  CollectionDecoder collectionDecoder(Class<? extends Collection> targetClass, Type itemType);

  /**
   * Return a decoder that can decode a MapElement into a map. The desired map, key and value types can be specified.
   *
   * @param targetClass The Map type that should be created by the decoder. If abstract, a default implementation will be created,
   *                    compatible with the provided abstract type. If null, a default map type will be created.
   * @param keyType     The map's key type. If null, the natural value type of the elements will be produced.
   * @param valueType   The map's value type. If null, the natural value type of the elements will be produced.
   * @return A decoder capable of decoding a ListElement and creating a Collection.
   */
  MapDecoder mapDecoder(Class<? extends Map> targetClass, Type keyType, Type valueType);

  /**
   * Get a copy of the default acceptor class list. Override this method to customize the list.
   * The DefaultDecoderFactory checks these factories in order to find a decoder that can produce
   * a specific data type.
   *
   * @return A modifiable list of TargetTypeAcceptor. Each call returns a new copy of the list.
   */
  default List<Class<? extends TargetTypeAcceptor<? extends Decoder>>> getTargetTypeAcceptors() {
    return Arrays.asList(new Class[]{
      NaturalDecoder.Acceptor.class,
      DeferredDecoder.Acceptor.class,
      StringDecoder.Acceptor.class,
      IntDecoder.Acceptor.class,
      RealDecoder.Acceptor.class,
      BooleanDecoder.Acceptor.class,
      EnumDecoder.Acceptor.class,
      CharacterDecoder.Acceptor.class,
			TemporalDecoder.Acceptor.class,
      ImmutableDecoder.Acceptor.class,
      MapDecoder.Acceptor.class, // MapDecoder has to appear before CollectionDecoder
      CollectionDecoder.Acceptor.class,
      BlobDecoder.Acceptor.class, // BLOB has to appear before array
			CharArrayDecoder.Acceptor.class, // char[] has to appear before array
      ArrayDecoder.Acceptor.class,
      NumberDecoder.Acceptor.class,
      ObjectPropertyDecoder.Acceptor.class
    });
  };
}
