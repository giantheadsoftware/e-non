/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.math.BigDecimal;
import java.util.EnumSet;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.codec.DataCategory;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.FloatElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.Element;
import org.enon.exception.EnonCoderException;
import org.enon.bind.value.NativeValue;

/**
 *
 * @author clininger $Id: $
 */
public class RealCoder extends AbstractCoder {

	/**
	 * Construct with a native value.It's up to the client to ensure that the value is a valid real number. For example, if the value is passed in as a String or char, it must be
 parse-able as a number.
	 *
	 * @param config The current config
	 */
	public RealCoder(EnonConfig config) {
		super(config);
	}

	@Override
	public Element getElement(NativeValue nativeValue) {
		Object value = nativeValue.getValue();
		if (value == null) {
			return ConstantElement.nullInstance();
		}
		if (value instanceof Float) {
			Float fValue = (Float)value;
			if (fValue.isNaN()) {
				if (hasFeatureX) {
					return ConstantElement.nanInstance();
				} else {
					throw new EnonCoderException("The value (Float)NaN is not supporeted by the e-non 0 feature set.  Enable FeatureSet X for NaN values");
				}
			}
			if (fValue.isInfinite()) {
				if (hasFeatureX) {
					return fValue > 0 ? ConstantElement.positiveInfinityInstance() : ConstantElement.negativeInfinityInstance();
				} else {
					throw new EnonCoderException("The value (Float)+/-Infinity is not supporeted by the e-non 0 feature set.  Enable FeatureSet X for infinite values");
				}
			}
			return hasFeatureX ? new FloatElement(fValue) : new NumberElement(fValue);
		}
		if (value instanceof Double) {
			Double dValue = (Double)value;
			if (dValue.isNaN()) {
				if (hasFeatureX) {
					return ConstantElement.nanInstance();
				} else {
					throw new EnonCoderException("The value (Double)NaN is not supporeted by the e-non 0 feature set.  Enable FeatureSet X for NaN values");
				}
			}
			if (dValue.isInfinite()) {
				if (hasFeatureX) {
					return dValue > 0 ? ConstantElement.positiveInfinityInstance() : ConstantElement.negativeInfinityInstance();
				} else {
					throw new EnonCoderException("The value (Double)+/-Infinity is not supporeted by the e-non 0 feature set.  Enable FeatureSet X for infinite values");
				}
			}
			return new DoubleElement(dValue);
		}
		if (value instanceof BigDecimal) {
			return new NumberElement((Number) value);
		}
		try {
			if (value instanceof CharSequence) {
				// use big decimal to handle any size number string
				return new NumberElement(new BigDecimal(String.valueOf(value)));
			}
			if (value instanceof Character) {
				byte numValue = Byte.parseByte(String.valueOf(value));
				return new NanoIntElement(numValue);
			}
		} catch (NumberFormatException ex) {
			// drop to the exception thrown below
		}
		throw new EnonCoderException("Could not extract real number value from type "
				+ value.getClass() + " with value of " + String.valueOf(value));
	}

	@Override
	public Set<DataCategory> handles() {
		return EnumSet.of(DataCategory.NUM_REAL);
	}

}
