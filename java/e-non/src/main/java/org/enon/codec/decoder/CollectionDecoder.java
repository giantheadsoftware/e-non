/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.WeakHashMap;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.ListElement;
import org.enon.exception.EnonDecoderException;
import org.enon.util.ReflectUtil;

/**
 * Decode a ListElement to create a Collection.
 * @author clininger $Id: $
 * @param <C> The type of collection produced by this decoder
 */
public class CollectionDecoder<C extends Collection> implements Decoder<C> {

	/** valid argument sets for Collection constructors. Look for a constructor with a size arg then no-arg. */
	protected static final Class[][] CONSTRUCTOR_ARGS = new Class[][]{new Class[]{int.class}, new Class[]{}};

	/** cache constructors to minimize lookup time */
	protected static final Map<String, Constructor<? extends Collection>> CONSTRUCTOR_CACHE = new WeakHashMap<>();

	/** the collection type this decoder must produce */
	protected final Class<C> targetClass;
	/** the type of item in the collection */
	protected final Type itemType;

	/**
	 * Constructor.
	 * @param targetClass The type of collection to create. If abstract or null, then a default implementation will be chosen.
	 * @param itemType The item type of the collection. If null, then the natural type of the eNON element will be used.
	 */
	protected CollectionDecoder(Class<C> targetClass, Type itemType) {
		this.targetClass = targetClass;
		this.itemType = itemType;
	}

	@Override
	public C decode(final Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}

		if (e.getType() == ElementType.LIST_ELEMENT) {
			final C collection = newCollection((int) e.getSize().getSize());

			final Decoder decoder = e.getReaderContext().getConfig().getDecoderFactory().decoderFor(itemType);

			((ListElement<Element>) e).getValue().stream()
					.map(decoder::decode)
					.forEach(item -> collection.add(item));

			return collection;
		}

		throw new EnonDecoderException("Unable to decode " + e.getType() + " to collection");
	}

	protected C newCollection(int size) {

		try {
			if (targetClass == null || Modifier.isAbstract(targetClass.getModifiers()))
				return defaultCollection(size);

			final Constructor<C> constructor = findConstructor();
			final C collection = constructor.getParameterCount() == 1 ? constructor.newInstance(size) : constructor.newInstance();

			return collection;
		} catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException x) {
			throw new EnonDecoderException("Unable to construct collection instance for type " + targetClass.getName(), x);
		}
	}

	/**
	 * Attempt to locate a constructor for the target collection type.
	 * @return a constructor for the targetClass
    * @throws EnonDecoderException if no suitable constructor can be found
	 */
	protected Constructor<C> findConstructor() {
		Constructor<C> constructor;
		constructor = (Constructor<C>) CONSTRUCTOR_CACHE.get(targetClass.getName());
		if (constructor != null) {
			return constructor;
		}
		// not cached, have to locate
		for (Class[] args : CONSTRUCTOR_ARGS) {
			try {
				constructor = targetClass.getConstructor(args);
				synchronized (CONSTRUCTOR_CACHE) {
					// synchronized to prevent conncurrent modification.
					// Don't care if a value gets overwritten in the rare concurrent case; should be the same value.
					CONSTRUCTOR_CACHE.put(targetClass.getName(), constructor);
				}
				return constructor;
			} catch (NoSuchMethodException x) {
				// try again...
			}
		}
		throw new EnonDecoderException("Unable to find suitable constructor for Collection type " + targetClass.getName()
				+ ". Can accept constructor with 'int size' arg or no args.");
	}

	/**
	 * Instantiate a default collection when the targetType is null or abstract.<p>
	 * Defaults:
	 * <ul>
	 * <li>Collection -&gt; ArrayList</li>
	 * <li>List -&gt; ArrayList</li>
	 * <li>Set -&gt; LinkedHashSet</li>
	 * <li>Queue -&gt; ArrayDeque</li>
	 * <li>Deque -&gt; ArrayDeque</li>
	 * <li>SortedSet -&gt; TreeSet</li>
	 * <li>NavigableSet -&gt; TreeSet</li>
	 * <li>null -&gt; ArrayList</li>
	 * </ul>
	 * @param size The initial size
	 * @return A new collection instance.
	 * @throws EnonDecoderException if a default is not defined for the targetType
	 */
	protected C defaultCollection(int size) {
		if (targetClass == null || List.class == targetClass || Collection.class == targetClass) {
			return (C) new ArrayList(size);
		}
		if (Set.class == targetClass) {
			return (C) new LinkedHashSet(size);
		}
		if (Queue.class == targetClass || Deque.class == targetClass) {
			return (C) new ArrayDeque(size);
		}
		if (SortedSet.class == targetClass || NavigableSet.class == targetClass) {
			return (C) new TreeSet();
		}
		throw new EnonDecoderException("No default for abstract collection type "+targetClass.getName());
	}


	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<CollectionDecoder> {

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return (Collection.class.isAssignableFrom(ReflectUtil.getinstance().classOfType(targetType)))
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected CollectionDecoder create(Type type) {
			if (type == null) {
				// return a decoder that creates the default collection type, the items are decoded using NaturalDecoder
				return new CollectionDecoder(null, null);
			}
			Class targetClass = reflectUtil.classOfType(type);
			Type[] collectionTypeArgs = reflectUtil.findArgsOfSuper(Collection.class, type);
			return new CollectionDecoder(targetClass, collectionTypeArgs[0]);
		}

		/**
		 * Special factory method for collections.  This factory is used when the target type is known to be a collection.
		 * The collection item type is passed explicitly, which is helpful for runtime collections where the generic type can't be
		 * queried by reflection.
		 * @param targetClass Collection type.  If null or abstract, an appropriate default will be used.
		 * @param itemType Collection item type.  If null, the Object.class is assumed.
		 * @return A decoder for the target collection type
		 */
		public CollectionDecoder decoderFor(Class<? extends Collection> targetClass, Type itemType) {
			final String targetTypeName = targetClass != null ? targetClass.getName() : "null";
			final String genericTypeName = itemType != null ? '<' + itemType.getTypeName() + '>' : "";
			final String cacheKey = targetTypeName + genericTypeName;

			CollectionDecoder decoder = getDecoderCache().get(cacheKey);
			if (decoder == null) {
				// not cached yet
				cacheAcceptLevel(cacheKey, AcceptLevel.PREFER);
				decoder = new CollectionDecoder(targetClass, itemType);
				cacheDecoder(cacheKey, decoder);
			}
			return decoder;
		}

	}
}
