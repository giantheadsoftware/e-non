/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.util.Set;
import org.enon.codec.DataCategory;
import org.enon.element.Element;
import org.enon.bind.value.NativeValue;

/**
 *
 * @author clininger $Id: $
 */
public interface Coder<NV extends NativeValue, E extends Element> {

	/**
	 * @return the set of data categories this coder can handle
	 */
	Set<DataCategory> handles();

	/**
   * @param nativeValue the native value for which to get the element
	 * @return the element which is constructed by this coder from the native value
	 */
	E getElement(NV nativeValue);
}
