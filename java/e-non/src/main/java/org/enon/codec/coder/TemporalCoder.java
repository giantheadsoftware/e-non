/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.bind.temporal.EnonTemporalField;
import org.enon.bind.value.NativeValueFactory;
import org.enon.bind.value.TemporalValue;
import org.enon.codec.DataCategory;
import org.enon.element.Element;
import org.enon.bind.temporal.TemporalVO;
import org.enon.element.TemporalElement;

/**
 *
 * @author clininger $Id: $
 */
public class TemporalCoder extends AbstractCoder<TemporalValue, Element> {

	private final boolean isX;

	public TemporalCoder(final EnonConfig config) {
		super(config);
		isX = config.hasFeature(FeatureSet.X);
	}

	@Override
	public Set<DataCategory> handles() {
		return isX ? EnumSet.of(DataCategory.TEMPORAL) : Collections.EMPTY_SET;
	}

	@Override
	public Element getElement(final TemporalValue temporalValue) {
		NativeValueFactory nativeValueFactory = config.getNativeValueFactory();
		CoderFactory coderFactory = config.getCoderFactory();
		if (isX) {
			return temporalMapToElement(temporalValue.getValue());
		}
		// can't use TemporalElement:  just use a map
		return coderFactory.encode(nativeValueFactory.nativeValue(asTemporalMap(temporalValue.getValue())));
	}
	
	/**
	 * Create a map from the TemporalVO.  Use the field prefix as the key for each field.
	 * @param temporalVO VO to map
	 * @return TemporalVO field values as a map
	 */
	protected Map<Character, Object> asTemporalMap(TemporalVO temporalVO) {
		Map<Character, Object> map = new LinkedHashMap<>();
		for (EnonTemporalField field : EnonTemporalField.values()) {
			map.put((char)field.key, temporalVO.get(field));
		}
		return map;
	}

	protected TemporalElement temporalMapToElement(TemporalVO temporalMap) {
		return TemporalElement.create(temporalMap);
	}
}
