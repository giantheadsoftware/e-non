/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.util.EnumSet;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.codec.DataCategory;
import org.enon.element.ConstantElement;
import org.enon.element.Element;
import org.enon.bind.value.NativeValue;

/**
 * Coder for the "constant" values null, true, false
 *
 * @author clininger $Id: $
 */
public class NullCoder extends AbstractCoder {

	public NullCoder(EnonConfig config) {
		super(config);
	}

	@Override
	public Set<DataCategory> handles() {
		return EnumSet.of(DataCategory.NULL);
	}

	@Override
	public Element getElement(NativeValue nativeValue) {
		return ConstantElement.nullInstance();
	}

}
