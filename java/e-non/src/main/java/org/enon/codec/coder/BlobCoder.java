/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.util.EnumSet;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.bind.value.BlobValue;
import org.enon.codec.DataCategory;
import org.enon.element.array.ByteArrayElement;

/**
 * Coder for the "constant" values null, true, false
 *
 * @author clininger $Id: $
 */
public class BlobCoder extends AbstractCoder<BlobValue, ByteArrayElement> {

	public BlobCoder(EnonConfig config) {
		super(config);
	}

	@Override
	public Set<DataCategory> handles() {
		return EnumSet.of(DataCategory.BYTES);
	}

	@Override
	public ByteArrayElement getElement(BlobValue blobValue) {
		return new ByteArrayElement(blobValue.getValue());
	}
}
