/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import org.enon.codec.DataCategory;
import org.enon.element.Element;
import org.enon.bind.value.NativeValue;

/**
 *
 * @author clininger $Id: $
 */
public interface CoderFactory {

	/**
	 * return a coder that knows how to encode the NativeValue category to eNON
	 * @param dataCategory Category to convert
	 * @return a coder for the category
	 */
	Coder coder(DataCategory dataCategory);

	/**
	 * Get a coder for the nativeValue then call decode() on that coder.
	 * @param nativeValue value to encode
	 * @return encoded value
	 */
	default Element encode(NativeValue nativeValue) {
		return coder(nativeValue.getCategory()).getElement(nativeValue);
	}
}
