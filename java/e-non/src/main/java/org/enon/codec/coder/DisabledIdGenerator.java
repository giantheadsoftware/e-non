/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import org.enon.element.meta.Size;

/**
 * IdGenerator that always returns 0. A 0 means that no map ID
 *
 * @author clininger $Id: $
 */
public class DisabledIdGenerator implements IdGenerator {

	@Override
	public Size nextId() {
		return Size.SIZE_ZERO;
	}
}
