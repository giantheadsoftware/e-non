/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.ListElement;
import org.enon.element.NanoIntElement;
import org.enon.element.ScalarElement;
import org.enon.exception.EnonDecoderException;
import org.enon.util.SerializeUtil;

/**
 *
 * @author clininger $Id: $
 */
public class BlobDecoder implements Decoder<byte[]> {

	private final IntDecoder<Byte> intDecoder = new IntDecoder.Acceptor().create(byte.class);

	protected BlobDecoder() {
	}

	@Override
	public byte[] decode(Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}

		switch (e.getType()) {

			case BLOB_ELEMENT:
			case BYTE_ELEMENT:
			case DOUBLE_ELEMENT:
			case INT_ELEMENT:
			case FLOAT_ELEMENT:
			case LONG_ELEMENT:
			case SHORT_ELEMENT:
			case STRING_ELEMENT:
			case NUMBER_ELEMENT:
			case ARRAY_ELEMENT:
				return ((ScalarElement)e).getContentBytes();
				
			case LIST_ELEMENT:
				// attempt to decode each element in the array as a byte value
				try (ByteArrayOutputStream baos = new ByteArrayOutputStream((int)e.getSize().getSize())) {
					((ListElement<Element>)e).getValue().stream().map(intDecoder::decode).forEach(baos::write);
					return baos.toByteArray();
				} catch (IOException x) {}
				

			case TRUE_ELEMENT:
				return new byte[]{(byte)1};
			case FALSE_ELEMENT:
				return new byte[]{(byte)0};

			case NANO_INT_ELEMENT:
				return new byte[]{((NanoIntElement)e).getValue()};

			case INFINITY_NEG_ELEMENT:
			case INFINITY_POS_ELEMENT:
			case NAN_ELEMENT:
				return SerializeUtil.getInstance().networkBytes((float)e.getValue());

		}
		throw new EnonDecoderException("Unable to decode " + e.getType() + " to byte[]");
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<BlobDecoder> {

		private final BlobDecoder instance = new BlobDecoder();

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return byte[].class == reflectUtil.classOfType(targetType) ? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected BlobDecoder create(Type targetType) {
			return instance;
		}

	}

}
