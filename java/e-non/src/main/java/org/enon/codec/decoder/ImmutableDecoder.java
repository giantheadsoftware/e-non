/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.beans.ConstructorProperties;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.ReaderContext;
import org.enon.bind.property.ConstructorProperty;
import org.enon.element.ElementType;
import org.enon.element.Element;
import org.enon.exception.EnonDecoderException;
import org.enon.exception.EnonException;
import org.enon.util.ReflectUtil;

/**
 * Decode a MapElement into some target object.<p>
 * This implementation does a "best effort" merge:  properties are only assigned when they exist in both the target class
 * and the MapElement.  Properties that exist in one but not the other are ignored.
 * @author clininger $Id: $
 * @param <O> The object type produced by this decoder
 */
public class ImmutableDecoder<O> implements Decoder<O> {

	/** the type being created */
	protected final Class<O> targetType;
	/** constructor for the type being created */
	protected final Constructor<O> targetConstructor;
	/** annotation that defines what properties are settable in the constructor */
	protected final ConstructorProperties constructorProperties;
	/** A map of the properties in the target type */
	protected final ConstructorProperty[] properties;
	/** The decoder that will decode the MapElement that represents the target object in the e-NON stream */
	protected final MapDecoder<Map<String, Element>> mapDecoder
					= new MapDecoder.Acceptor().decoderFor(HashMap.class, String.class, Element.class);

	/**
	 * Constructor
	 * @param targetConstructor Constructor for the targetType
	 * @param constructorProperties annotation that defines what properties are settable in the constructor
	 */
	protected ImmutableDecoder(Constructor<O> targetConstructor, ConstructorProperties constructorProperties) {
		this.targetConstructor = targetConstructor;
		this.constructorProperties = constructorProperties;
		this.targetType = targetConstructor.getDeclaringClass();

		String[] propNames = constructorProperties.value();
		Type[] types = targetConstructor.getGenericParameterTypes();
		this.properties = new ConstructorProperty[Math.min(propNames.length, types.length)];

		ReflectUtil rUtil = ReflectUtil.getinstance();

		for (int i = 0; i < propNames.length; i++) {
			if (types.length > i) {
				properties[i] = new ConstructorProperty(constructorProperties.value()[i], types[i]);
			}
		}
	}

	@Override
	public O decode(final Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}

		if (elementType == ElementType.MAP_ELEMENT) {
			Map<String, Element> map = mapDecoder.decode(e);

			final ReaderContext ctx = e.getReaderContext();

			Object[] values = Arrays.stream(properties)
					// if the property has a value in the map, decode it.  Otherwise, set it to null.
					.map(prop -> map.get(prop.getName()) != null ? ctx.decode(prop.getPropertyType(), map.get(prop.getName())): null)
					.toArray();
			// instantiate the target object
			try {
				return targetConstructor.newInstance(values);
			} catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException x) {
				throw new EnonDecoderException("Unable to decode IMMUTABLE object of type "+targetType.getName(), x);
			}

			// get the decoder factory from the context - need it to decode property values
//			final DecoderFactory decoderFactory = e.getReaderContext().getConfig().getDecoderFactory();
//
//			Arrays.stream(constructorProperties.value())
//					.map(property -> map.get(property))
//					.map(element -> )
//					.toArray()
//			// for all the properties existing in the target type, look for a matching entry in the MapElement
//			properties.entrySet().stream()
//					.forEach(entry -> {
//						WritableProperty prop = entry.getValue();
//						Element propertyElement = map.get(prop.getName());
//						if (propertyElement != null) {
//							try {
//								Object value = ctx.decode(prop.getPropertyType(), propertyElement);
//								prop.writeValue(o, value);
//							} catch (EnonException x) {
//								throw new EnonDecoderException("Unable to decode object property '"+prop.getName()+"' of type "+targetType.getName(), x);
//							}
//						}
//					});
//			return o;
		}

		throw new EnonDecoderException("Unable to decode " + e.getType() + " to "+targetType.getName());
	}

	/**
	 * Instantiate a new O
	 * @return new O instance
	 */
	protected O newO() {
		try {
			return targetConstructor.newInstance();
		} catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException x) {
			throw new EnonDecoderException("Unable to construct collection instance for type " + targetType.getName(), x);
		}
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<ImmutableDecoder> {

		protected final EnonConfig config;

		public Acceptor(EnonConfig config) {
			this.config = config;
		}

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			try {
				Class c = ReflectUtil.getinstance().classOfType(targetType); // classOfType will throw if type can't resolve to a class
				int modifiers = c.getModifiers();
				// can't instantiate abstract or non-public classes
				if (Modifier.isAbstract(modifiers) ||  !Modifier.isPublic(modifiers))
					return AcceptLevel.NEVER;
				// need to have a constructor with @ConstructorProperties annotation
				for (Constructor con : c.getConstructors()) {
					if (con.getDeclaredAnnotationsByType(ConstructorProperties.class).length > 0) {
						return AcceptLevel.PREFER;
					}
				}
				return AcceptLevel.NEVER;
			} catch (EnonException x) {
				return AcceptLevel.NEVER;
			}
		}

		@Override
		protected ImmutableDecoder create(Type targetType) {
			try {
				for (Constructor con : reflectUtil.classOfType(targetType).getConstructors()) {
					Annotation[] annos = con.getDeclaredAnnotationsByType(ConstructorProperties.class);
					if (annos.length > 0) {
						return new ImmutableDecoder(con, (ConstructorProperties) annos[0]);
					}
				}
				throw new EnonDecoderException("Unable to create ObjectPropertyDecoder for target type: call evaluate first: "+targetType.getTypeName());
			} catch (EnonException x) {
				throw new EnonDecoderException("Unable to create ObjectPropertyDecoder for target type "+targetType.getTypeName(), x);
			}
		}
	}
}
