/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec;

/**
 * These are the general data type buckets. The codecs may choose how to represent specific values within these categories based on features available, efficiency, etc.
 *
 * @author clininger $Id: $
 */
public enum DataCategory {
	/**
	 * all null values, regardless the intended type.
	 */
	NULL,
	/**
	 * true or false.
	 */
	BOOLEAN,
	/**
	 * integer numbers.
	 */
	NUM_INT,
	/**
	 * real numbers.
	 */
	NUM_REAL,
	/**
	 * string and character values.
	 */
	STRING,
	/**
	 * date, time, timestamp, calendar, etc. values.
	 */
	TEMPORAL,
	/**
	 * Any values in an iterable collection.
	 */
	COLLECTION,
	/**
	 * Any values in key-value pairs, including POJOs with read/write properties.
	 */
	MAP,
	/**
	 * An array of primitive values (can not have null values).
	 */
	PRIMITIVE_ARRAY,
	/**
	 * Array or stream of raw data.
	 */
	BYTES
}
