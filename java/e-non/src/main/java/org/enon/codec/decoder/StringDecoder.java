/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import org.enon.element.ElementType;
import org.enon.element.Element;
import org.enon.util.ReflectUtil;

/**
 * This decoder just takes the "natural" value from the element without further translation.
 *
 * @author clininger $Id: $
 */
public class StringDecoder implements Decoder<String> {

	protected StringDecoder() {
	}

	@Override
	public String decode(final Element e) {
		if (e.getType() == ElementType.NULL_ELEMENT) {
			return null;
		}
		return String.valueOf(e.getValue());
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<StringDecoder> {

		public final StringDecoder INSTANCE = new StringDecoder();

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return (String.class.isAssignableFrom(ReflectUtil.getinstance().classOfType(targetType)))
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected StringDecoder create(Type targetType) {
			return INSTANCE;
		}

	}
}
