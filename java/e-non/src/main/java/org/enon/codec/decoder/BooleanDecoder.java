/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import org.enon.codec.Value;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.exception.EnonDecoderException;
import org.enon.util.ReflectUtil;

/**
 *
 * @author clininger $Id: $
 */
public class BooleanDecoder implements Decoder<Boolean> {

	protected BooleanDecoder() {
	}

	@Override
	public Boolean decode(Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}
		Object eValue = e.getValue();

		Class valueClass = eValue.getClass();
		if (valueClass == Boolean.class || valueClass == Boolean.TYPE) {
			return (boolean) eValue;
		}

		switch (e.getType()) {
			case BYTE_ELEMENT:
			case DOUBLE_ELEMENT:
			case FLOAT_ELEMENT:
			case INT_ELEMENT:
			case LONG_ELEMENT:
			case NANO_INT_ELEMENT:
			case SHORT_ELEMENT:
				return ((Number)eValue).longValue() != 0;

			case NUMBER_ELEMENT:
				return new BigDecimal((String)eValue).compareTo(BigDecimal.ZERO) != 0;

			case STRING_ELEMENT:
				Boolean bool = Value.trueOrFalse(String.valueOf(eValue));
				if (bool != null) {
					return bool;
				}
				throw new EnonDecoderException("Unable to interpret string as boolean: "+String.valueOf(eValue));

			case INFINITY_NEG_ELEMENT:
			case INFINITY_POS_ELEMENT:
				return true;

			case NAN_ELEMENT:
				return false;
		}
		throw new EnonDecoderException("Unable to decode " + e.getType() + " to boolean");
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends  TargetTypeAcceptor<BooleanDecoder> {

		public static final BooleanDecoder INSTANCE = new BooleanDecoder();

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return  (Boolean.TYPE == targetType || Boolean.class.isAssignableFrom(ReflectUtil.getinstance().classOfType(targetType)))
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected BooleanDecoder create(Type targetType) {
			return INSTANCE;
		}

	}
}
