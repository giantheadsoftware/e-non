/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.math.BigInteger;
import java.util.EnumSet;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.codec.DataCategory;
import org.enon.element.ByteElement;
import org.enon.element.IntElement;
import org.enon.element.LongElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.ShortElement;
import org.enon.element.Element;
import org.enon.exception.EnonCoderException;
import org.enon.bind.value.NativeValue;

/**
 *
 * @author clininger $Id: $
 */
public class IntCoder extends AbstractCoder {

	/**
	 * Construct with a native value. It's up to the client to ensure that the value is a valid integer number. For example, if the value is passed in as a String or char, it must be
	 * parse-able as a number.
	 *
	 * @param config The current config
	 */
	public IntCoder(EnonConfig config) {
		super(config);
	}

	@Override
	public Element getElement(NativeValue nativeValue) {
		Object value = nativeValue.getValue();
		if (value instanceof BigInteger) {
			return encodeBigInt((BigInteger) value);
		}
		if (value instanceof Number) {
			// NOTE: if a float or double managed to reach here it will be rounded truncated
			return smallestElement(((Number) value).longValue());
		}
		try {
			if (value instanceof CharSequence) {
				// use big int to handle any size number
				return encodeBigInt(new BigInteger(String.valueOf(value)));
			}
			if (value instanceof Character) {
				byte numValue = Byte.parseByte(String.valueOf(value));
				return new NanoIntElement(numValue);
			}
		} catch (NumberFormatException ex) {
			// drop to the exception thrown below
		}
		throw new EnonCoderException("Could not extract integer value from type "
				+ value.getClass() + " with value of " + String.valueOf(value));
	}

	private Element encodeBigInt(BigInteger value) {
		try {
			long longVal = value.longValueExact();
			return smallestElement(longVal);
		} catch (ArithmeticException ax) {
			return new NumberElement(value);
		}
	}

	/**
	 * Return the Element with the smallest representation of the number.
	 */
	private Element smallestElement(long longVal) {
		// use NanoInt when possible
		if (longVal <= NanoIntElement.MAX_VALUE && longVal >= NanoIntElement.MIN_VALUE) {
			return new NanoIntElement((byte) longVal);
		}

		// use Byte or Int based on feature set */
		if (longVal <= Byte.MAX_VALUE && longVal >= Byte.MIN_VALUE) {
			return config.hasFeature(FeatureSet.X) ? new ByteElement((byte) longVal) : new IntElement((byte) longVal);
		}

		// use Short or Int based on feature set
		if (longVal <= Short.MAX_VALUE && longVal >= Short.MIN_VALUE) {
			return config.hasFeature(FeatureSet.X) ? new ShortElement((short) longVal) : new IntElement((short) longVal);
		}

		// use Int
		if (longVal <= Integer.MAX_VALUE && longVal >= Integer.MIN_VALUE) {
			return new IntElement((int) longVal);
		}

		// use Long or Number based on feature set
		return config.hasFeature(FeatureSet.X) ? new LongElement(longVal) : new NumberElement(longVal);
	}

	@Override
	public Set<DataCategory> handles() {
		return EnumSet.of(DataCategory.NUM_INT);
	}

}
