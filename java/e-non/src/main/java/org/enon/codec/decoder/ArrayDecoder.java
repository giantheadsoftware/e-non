/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import org.enon.ReaderContext;
import org.enon.element.ElementType;
import org.enon.element.ListElement;
import org.enon.element.Element;
import org.enon.element.array.ArrayElement;
import org.enon.exception.EnonDecoderException;

/**
 * Decode a ListElement, creating an array.
 * @author clininger $Id: $
 * @param <A> The array type decoded by this decoder (i.e. ArrayDecoder&lt;int[]&gt;)
 */
public class ArrayDecoder<A> implements Decoder<A> {

	/** the type of item in the collection */
	protected final Class destComponentType;
	protected final boolean primitive;

	/**
	 * Constructor.
	 *
	 * @param arrayType An array class that defines the target array type to be decoded.
	 */
	protected ArrayDecoder(Class<A> arrayType) {
		destComponentType = arrayType.getComponentType();
		primitive = destComponentType.isPrimitive();
	}

	@Override
	public A decode(final Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}

		switch (e.getType()) {
			case ARRAY_ELEMENT:
				ArrayElement ae = (ArrayElement) e;
				Object arrayValue = ae.getValue();

				// if the target type is an exact match to the element value, return the value directly.
				Class srcComponentType = arrayValue.getClass().getComponentType();
				if (srcComponentType == destComponentType) {
					return (A)arrayValue;
				}
				// not an exact array type match: get the array contents as Elements and use existing decoders to transalate them to the target type.
				return decodeEntries(ae.asElements(), e.getReaderContext(), false);

			case LIST_ELEMENT:
				return decodeEntries(((ListElement<Element>)e).getValue(), e.getReaderContext(), primitive);

			case MAP_ELEMENT:
			case GLOSSARY_REF_ELEMENT:
			case MAP_REF_ELEMENT:
				throw new EnonDecoderException("Unable to decode " + e.getType() + " to array");

			case STRING_ELEMENT:
			case NUMBER_ELEMENT:
				if (destComponentType == char.class || destComponentType == Character.class) {
					String value = (String) e.getValue();
					A result = (A)Array.newInstance(destComponentType, value.length());
					for (int i = 0; i < Array.getLength(result); i++) {
						Array.setChar(result, i, value.charAt(i));
					}
					return result;
				}
				// intentionally drop down to default -- do not put a break here
			default:
				// try to create an array of 1 entry
				ArrayList<Element> listOf1 = new ArrayList<>(1);
				listOf1.add(e);
				return decodeEntries(listOf1, e.getReaderContext(), primitive);
		}
	}

	/**
	 * Decode each entry in the list and add it to the result array.
	 * @param entries The entries to decode and add to the array
	 * @param ctx ReaderContext - contains the decoder factory used to decode the entries
	 * @param nullCheck True if it's necessary to check for null.  Should be true if the entries may contain null and the destination cannot accept nulls.
	 * @return The elements decoded into the target array type.
	 */
	private A decodeEntries(Collection<? extends Element> entries, ReaderContext ctx, boolean nullCheck) {
		final A result = (A)Array.newInstance(destComponentType, entries.size());
		final Decoder decoder = ctx.getConfig().getDecoderFactory().decoderFor(destComponentType);

		int i = 0;
		for (Element item : entries) {
			if (nullCheck && item.getType() == ElementType.NULL_ELEMENT) {
				throw new EnonDecoderException("Can't assign null to array of " + destComponentType.getName());
			}
			Object value = decoder.decode(item);
			Array.set(result, i++, value);
		}
		return result;
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<ArrayDecoder> {

		@Override
		protected AcceptLevel evaluate(Type t) {
			Class targetClass = reflectUtil.classOfType(t);
			return targetClass.isArray() ? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected ArrayDecoder create(Type targetType) {
			return new ArrayDecoder(reflectUtil.classOfType(targetType));
		}
	}
}
