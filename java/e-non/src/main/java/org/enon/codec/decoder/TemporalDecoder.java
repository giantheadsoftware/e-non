/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.function.UnaryOperator;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.bind.temporal.TemporalVO;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.LongElement;
import org.enon.element.NumberElement;
import org.enon.element.StringElement;
import org.enon.element.TemporalElement;
import org.enon.exception.EnonDecoderException;

/**
 *
 * @author clininger $Id: $
 */
public class TemporalDecoder implements Decoder<Object> {

	/** Map to cache unary functions which convert from one TemporalAccessor type to another. */
	private final Map<String, UnaryOperator<TemporalAccessor>> taFactoryFns = new HashMap<>();

	protected final Class targetClass;
	private final TemporalTargetType targetType;
	protected final ZoneId localZoneId;

	protected TemporalDecoder(Class targetClass, ZoneId localZoneId) {
		this.targetClass = targetClass;
		targetType = TemporalTargetType.byTypeName(targetClass.getName());
		this.localZoneId = localZoneId;
	}

	@Override
	public Object decode(Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}

		switch (e.getType()) {
			case TEMPORAL_ELEMENT:
				try {
					return fromTemporalVO(((TemporalElement)e).getValue());
				} catch (DateTimeException x) {
					throw new EnonDecoderException("Unable to decode " + e.getType() + " to " + targetClass.getName(), x);
				}

			case LONG_ELEMENT:
				return fromLong(((LongElement)e).getValue());

			case NUMBER_ELEMENT:
				try {
					return fromLong(Long.parseLong(((NumberElement)e).getValue()));
				} catch (NumberFormatException x) {
					throw new EnonDecoderException("Can't interpret number value as date/time: "+e.getValue());
				}

			case STRING_ELEMENT:
				return fromString(((StringElement)e).getValue());
		}
		throw new EnonDecoderException("Unable to decode " + e.getType() + " to " + targetClass.getName());
	}

	/**
	 * Produce the temporal value from the Epoch milliseconds
	 * @param longValue Number of milliseconds from the Epoch
	 * @return temporal value of some type -- will not be null
   * @throws EnonDecoderException if the value can't be decoded as a temporal type
	 */
	protected Object fromLong(long longValue) {
		if (Date.class == targetClass) {
			return new Date(longValue);
		}
		if (targetClass.isAssignableFrom(GregorianCalendar.class)) {
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTimeInMillis(longValue);
			return cal;
		}
		if (TemporalAccessor.class.isAssignableFrom(targetClass)) {
			Instant instant = Instant.ofEpochMilli(longValue);
			if (Instant.class == targetClass) {
				return instant;
			}
			if (LocalDate.class == targetClass) {
				return instant.atZone(localZoneId).toLocalDate();
			}
			if (LocalDateTime.class == targetClass) {
				return instant.atZone(localZoneId).toLocalDateTime();
			}
			if (LocalTime.class == targetClass) {
				return instant.atZone(localZoneId).toLocalTime();
			}
			if (MonthDay.class == targetClass) {
				return MonthDay.from(instant.atZone(localZoneId));
			}
			if (OffsetDateTime.class == targetClass) {
				return instant.atOffset(ZoneOffset.UTC);
			}
			if (OffsetTime.class == targetClass) {
				return instant.atOffset(ZoneOffset.UTC).toOffsetTime();
			}
			if (Year.class == targetClass) {
				return Year.from(instant.atZone(localZoneId));
			}
			if (YearMonth.class == targetClass) {
				return YearMonth.from(instant.atZone(localZoneId));
			}
			if (TemporalAccessor.class == targetClass || ZonedDateTime.class == targetClass) {
				return instant.atZone(ZoneOffset.UTC);
			}
		}
		throw new EnonDecoderException("Can't decode long value to temporal type "+targetClass.getName());
	}

	protected Object fromString(String longValue) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Determine whether the TemporalVO has enough data to produce the target type
	 * @param tvo value being decoded
	 * @return true if the tvo has enough data
	 */
	protected boolean canDecode(TemporalVO tvo) {
		if (targetType != null) {
			switch (targetType) {
				case DATE:
				case GREGORIAN_CALENDAR:
				case INSTANT:
				case LOCAL_DATE:
				case LOCAL_DATE_TIME:
				case OFFSET_DATE_TIME:
				case ZONED_DATE_TIME:
					return tvo.isInstant() || tvo.isDate();

				case LOCAL_TIME:
				case OFFSET_TIME:
					return true;

				case MONTH_DAY:
					return tvo.isInstant() || (tvo.month != null && tvo.day != null);

				case YEAR:
					return tvo.isInstant() || tvo.year != null;

				case YEAR_MONTH:
					return tvo.isInstant() || (tvo.year != null && tvo.month != null);
			}
		}
		return false;
	}

	protected Object fromTemporalVO(TemporalVO tvo) {
		if (canDecode(tvo)) {
			// we've verified that the tvo can be decoded to the target type - don't need to test again, just decode

			if (tvo.isInstant()) {
				// call special method for decoding instants
				return fromEpochMillis(tvo.instant, tvo.nanos, tvo.offsetSeconds, tvo.zoneId);
			}

			// the tvo is not an instant, so we need to populate based on individual temporal fields
			switch (targetType) {
				case DATE: {
					long epochSeconds;
					if (tvo.zoneId != null) {
						epochSeconds = ZonedDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneId.of(tvo.zoneId)).toEpochSecond();
					} else if (tvo.offsetSeconds != null) {
						epochSeconds = OffsetDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.ofTotalSeconds(tvo.offsetSeconds)).toEpochSecond();
					} else {
						epochSeconds = ZonedDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, localZoneId).toEpochSecond();
					}
					return new Date(epochSeconds * 1000 + tvo.nanos / 1000000);
				}

				case GREGORIAN_CALENDAR: {
					return asCalendar(tvo);
				}

				case INSTANT:
					long seconds = asCalendar(tvo).getTimeInMillis() / 1000;
					return Instant.ofEpochSecond(seconds, tvo.nanos);

				case YEAR:
					return Year.of(tvo.year);

				case YEAR_MONTH:
					return YearMonth.of(tvo.year, tvo.month);

				case MONTH_DAY:
					return MonthDay.of(tvo.month, tvo.day);

				case LOCAL_DATE:
					return LocalDate.of(tvo.year, tvo.month, tvo.day);

				case LOCAL_DATE_TIME:
					return LocalDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos);

				case LOCAL_TIME:
					return LocalTime.of(tvo.hour, tvo.minute, tvo.sec, tvo.nanos);

				case OFFSET_DATE_TIME:
					if (tvo.zoneId != null) {
						return ZonedDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneId.of(tvo.zoneId)).toOffsetDateTime();
					}
					if (tvo.offsetSeconds != null) {
						return OffsetDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.ofTotalSeconds(tvo.offsetSeconds));
					}
					return OffsetDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.UTC);

				case OFFSET_TIME:
					if (tvo.zoneId != null) {
						return OffsetTime.of(tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.of(tvo.zoneId));
					}
					if (tvo.offsetSeconds != null) {
						return OffsetTime.of(tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.ofTotalSeconds(tvo.offsetSeconds));
					}
					return OffsetTime.of(tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.UTC);

				case ZONED_DATE_TIME:
					if (tvo.zoneId != null) {
						return ZonedDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneId.of(tvo.zoneId));
					}
					if (tvo.offsetSeconds != null) {
						return ZonedDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.ofTotalSeconds(tvo.offsetSeconds));
					}
					return ZonedDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.UTC);

			}

		}
		// If the TVO contains instant data, process some special cases that are easier this way
//		if (tvo.isInstant()) {
//			if (Instant.class == targetClass) {
//				// instant doesn't care about zone offset: can just use the epoch time
//				return Instant.ofEpochMilli(tvo.instant);
//			}
//			if (Date.class == targetClass) {
//				// Date doesn't care about zone offset: can just use the epoch time
//				return new Date(tvo.instant);
//			}
//			if (tvo.isLocal()) {
//				// if the VO has no zone offset data, then reuse the fromLong() method.
//				return fromLong(tvo.instant);
//			}
//		}
//		if (Date.class == targetClass && tvo.isDate()) {
//			return asCalendar(tvo).getTime();
//		}
//		if (targetClass.isAssignableFrom(GregorianCalendar.class)) {
//			return asCalendar(tvo);
//		}
//		// Convert the TVO to a TemporalAccessor and attempt to produce a result with that.
//		TemporalAccessor ta = asTemporalAccessor(tvo);
//		if (ta != null && TemporalAccessor.class.isAssignableFrom(targetClass)) {
//			return fromTa(ta);
//		}
		throw new EnonDecoderException(MessageFormat.format("Can''t decode temporalVO value to temporal type {0} ({1})", targetType, targetClass.getName()));
	}

	/**
	 * Attempt to decode from epochMillis and optional offset or zoneId.If both zonId and offset are provided, then zoneId is used.
	 * @param epochMillis millis from the epoch
	 * @param nanos number of nanoseconds to add to the millis
	 * @param offsetSec offset from UTC in seconds, optional
	 * @param zoneId optional zoneId string
	 * @return target type instance
	 */
	protected Object fromEpochMillis(long epochMillis, Integer nanos, Integer offsetSec, String zoneId) {
		switch (targetType) {
			case DATE:
				return new Date(epochMillis);

			case GREGORIAN_CALENDAR:
				GregorianCalendar cal = (GregorianCalendar) new GregorianCalendar.Builder().setInstant(epochMillis).build();
				if (zoneId != null) {
					cal.setTimeZone(TimeZone.getTimeZone(zoneId));
				}
				if (offsetSec != null) {
					cal.setTimeZone(TimeZone.getTimeZone(ZoneOffset.ofTotalSeconds(offsetSec)));
				}
				return cal;

			case INSTANT: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				return nanos == null ? i : i.plusNanos(nanos);
			}

			case LOCAL_DATE: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				if (zoneId != null) {
					return i.atZone(ZoneId.of(zoneId)).toLocalDate();
				}
				if (offsetSec != null) {
					return i.atOffset(ZoneOffset.ofTotalSeconds(offsetSec)).toLocalDate();
				}
				return i.atZone(localZoneId).toLocalDate();
			}

			case LOCAL_DATE_TIME: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				if (zoneId != null) {
					return i.atZone(ZoneId.of(zoneId)).toLocalDateTime();
				}
				if (offsetSec != null) {
					return i.atOffset(ZoneOffset.ofTotalSeconds(offsetSec)).toLocalDateTime();
				}
				return i.atZone(localZoneId).toLocalDateTime();
			}

			case LOCAL_TIME: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				if (zoneId != null) {
					return i.atZone(ZoneId.of(zoneId)).toLocalTime();
				}
				if (offsetSec != null) {
					return i.atOffset(ZoneOffset.ofTotalSeconds(offsetSec)).toLocalTime();
				}
				return i.atZone(localZoneId).toLocalTime();
			}

			case MONTH_DAY: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				OffsetDateTime odt;
				if (zoneId != null) {
					odt = OffsetDateTime.ofInstant(i, ZoneId.of(zoneId));
				}	else if (offsetSec != null) {
					odt = OffsetDateTime.ofInstant(i, ZoneOffset.ofTotalSeconds(offsetSec));
				} else {
					odt = OffsetDateTime.ofInstant(i, localZoneId);
				}
				return MonthDay.from(odt);
			}

			case OFFSET_DATE_TIME: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				if (zoneId != null) {
					return i.atZone(ZoneId.of(zoneId)).toOffsetDateTime();
				}
				if (offsetSec != null) {
					return i.atOffset(ZoneOffset.ofTotalSeconds(offsetSec));
				}
				return i.atZone(localZoneId).toOffsetDateTime();
			}

			case OFFSET_TIME: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				if (zoneId != null) {
					return OffsetTime.ofInstant(i, ZoneId.of(zoneId));
				}
				if (offsetSec != null) {
					return OffsetTime.ofInstant(i, ZoneOffset.ofTotalSeconds(offsetSec));
				}
				return OffsetTime.ofInstant(i, localZoneId);
			}

			case YEAR: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				OffsetDateTime odt;
				if (zoneId != null) {
					odt = OffsetDateTime.ofInstant(i, ZoneId.of(zoneId));
				}	else if (offsetSec != null) {
					odt = OffsetDateTime.ofInstant(i, ZoneOffset.ofTotalSeconds(offsetSec));
				} else {
					odt = OffsetDateTime.ofInstant(i, localZoneId);
				}
				return Year.from(odt);
			}

			case YEAR_MONTH: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				OffsetDateTime odt;
				if (zoneId != null) {
					odt = OffsetDateTime.ofInstant(i, ZoneId.of(zoneId));
				}	else if (offsetSec != null) {
					odt = OffsetDateTime.ofInstant(i, ZoneOffset.ofTotalSeconds(offsetSec));
				} else {
					odt = OffsetDateTime.ofInstant(i, localZoneId);
				}
				return YearMonth.from(odt);
			}

			case ZONED_DATE_TIME: {
				Instant i = Instant.ofEpochMilli(epochMillis);
				if (zoneId != null) {
					return ZonedDateTime.ofInstant(i, ZoneId.of(zoneId));
				}
				if (offsetSec != null) {
					return ZonedDateTime.ofInstant(i, ZoneOffset.ofTotalSeconds(offsetSec));
				}
				return ZonedDateTime.ofInstant(i, localZoneId);
			}
		}
		throw new EnonDecoderException("Can't decode "+targetClass.getName()+" from instant value");
	}

	/**
	 * Convert a TemporalVO to a Calendar instance.
	 * @param tvo VO to convert
	 * @return A new GergorianCalendar instance
	 */
	protected GregorianCalendar asCalendar(TemporalVO tvo) {
		GregorianCalendar cal;
		switch (tvo.fieldMask) {
			case TemporalVO.FULL_DATE_MASK:
				cal = new GregorianCalendar(tvo.year, tvo.month - 1, tvo.day);
				break;
			case TemporalVO.FULL_DATE_TIME_S_MASK:
				cal = new GregorianCalendar(tvo.year, tvo.month - 1, tvo.day, tvo.hour, tvo.minute, tvo.sec);
				break;
			default: {
				// set individual fields
				cal = new GregorianCalendar();
				cal.clear();  // discard current time values
				if (tvo.isInstant()) {
					cal.setTimeInMillis(tvo.instant);
				} else {
//					if (tvo.era != null) { TODO: support era
//						cal.set(Calendar.ERA, tvo.era);
//					}
					if (tvo.year != null) {
						cal.set(Calendar.YEAR, tvo.year);
					}
					if (tvo.month != null) {
						cal.set(Calendar.MONTH, tvo.month - 1);
					}
					if (tvo.day != null) {
						cal.set(Calendar.DAY_OF_MONTH, tvo.day);
					}
					if (tvo.hour != null) {
						cal.set(Calendar.HOUR_OF_DAY, tvo.hour);
					}
					if (tvo.minute != null) {
						cal.set(Calendar.MINUTE, tvo.minute);
					}
					if (tvo.sec != null) {
						cal.set(Calendar.SECOND, tvo.sec);
					}
					if (tvo.nanos != null) {
						cal.set(Calendar.MILLISECOND, tvo.nanos / 1000000);
					}
				}
			}
			break;
		}
		if (tvo.zoneId != null) {
			cal.setTimeZone(TimeZone.getTimeZone(tvo.zoneId));
		} else if (tvo.offsetSeconds != null) {
			cal.setTimeZone(TimeZone.getTimeZone(ZoneOffset.ofTotalSeconds(tvo.offsetSeconds)));
		}
		return cal;
	}

//	/**
//	 * Attempt to construct the most complete TemporalAccessor instance possible from the TemporalVO.  The TemporalAccessor classes
//	 * in the java.time package will synthesize multiple derivative fields which are needed to translate from one TemporalAccessor
//	 * to another.
//	 * <p>This is intended to be an intermediate step, the result passed on to fromTa() which will convert this result to the
//	 * desired target type.
//	 * @param tvo
//	 * @return the most capable TemporalAccessor instance that can be derived from the fields available in the TVO.  Return null if no
//	 * TemporalAccessor implementation is suitable.
//	 */
//	protected TemporalAccessor asTemporalAccessor(TemporalVO tvo) {
//		// process the instant value
//		if (tvo.isInstant()) {
//			Instant instant = Instant.ofEpochMilli(tvo.instant);
//			if (tvo.zoneId != null) {
//				return ZonedDateTime.ofInstant(instant, ZoneId.of(tvo.zoneId));
//			}
//			if (tvo.offsetSeconds != null) {
//				return OffsetDateTime.ofInstant(instant, ZoneOffset.ofTotalSeconds(tvo.offsetSeconds));
//			}
//			return OffsetDateTime.ofInstant(instant, ZoneOffset.UTC);
//		}
//
//		// process as date-time (time is always present due to defaults)
//		if (tvo.isDate()) {
//			if (tvo.zoneId != null) {
//				return ZonedDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneId.of(tvo.zoneId));
//			}
//			if (tvo.offsetSeconds != null) {
//				return OffsetDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.ofTotalSeconds(tvo.offsetSeconds));
//			}
//			return LocalDateTime.of(tvo.year, tvo.month, tvo.day, tvo.hour, tvo.minute, tvo.sec, tvo.nanos);
//		}
//
//		// process as just a time of day
//		if (!tvo.isDefaultTime()) {
//			if (tvo.offsetSeconds != null) {
//				return OffsetTime.of(tvo.hour, tvo.minute, tvo.sec, tvo.nanos, ZoneOffset.ofTotalSeconds(tvo.offsetSeconds));
//			}
//			return LocalTime.of(tvo.hour, tvo.minute, tvo.sec, tvo.nanos);
//		}
//
////		// process as just a date
////		if (tvo.isDate()) {
////			return LocalDate.of(tvo.year, tvo.month, tvo.day);
////		}
//
//		// process some special cases
//		switch (tvo.fieldMask) {
//			case TemporalVO.YEAR_MASK:
//				return Year.of(tvo.year);
//			case TemporalVO.YEAR_MONTH_MASK:
//				return YearMonth.of(tvo.year, tvo.month);
//			case TemporalVO.MONTH_DAY_MASK:
//				return MonthDay.of(tvo.month, tvo.day);
//		}
//
//		return null;
//	}

//	/**
//	 * Attempt to produce the target TemporalAccessor type based on a (potentially) different TemporalAccessor type.
//	 * @param arcTa
//	 * @return
//	 */
//	protected TemporalAccessor fromTa(TemporalAccessor arcTa) {
//		// trivial case: same type or any type
//		if (TemporalAccessor.class == targetClass || arcTa.getClass() == targetClass) {
//			return arcTa;
//		}
//		// check the convertFn cache
//		String targetTypeName = targetClass.getName();
//		UnaryOperator<TemporalAccessor> convertFn = taFactoryFns.get(targetTypeName);
//		if (convertFn != null) {
//			return convertFn.apply(arcTa);
//		}
//		// converter not cached: look it up via brute-force method
//		if (Instant.class == targetClass) {
//			convertFn = ta -> Instant.from(ta);
//		} else if (LocalDate.class == targetClass) {
//			convertFn = ta -> LocalDate.from(ta);
//		} else if (LocalDateTime.class == targetClass) {
//			convertFn = ta -> LocalDateTime.from(ta);
//		} else if (LocalTime.class == targetClass) {
//			convertFn = ta -> LocalTime.from(ta);
//		} else if (MonthDay.class == targetClass) {
//			convertFn = ta -> MonthDay.from(ta);
//		} else if (OffsetDateTime.class == targetClass) {
//			convertFn = ta -> OffsetDateTime.from(ta);
//		} else if (OffsetTime.class == targetClass) {
//			convertFn = ta -> OffsetTime.from(ta);
//		} else if (Year.class == targetClass) {
//			convertFn = ta -> Year.from(ta);
//		} else if (YearMonth.class == targetClass) {
//			convertFn = ta -> YearMonth.from(ta);
//		} else if (ZonedDateTime.class == targetClass) {
//			convertFn = ta -> ZonedDateTime.from(ta);
//		} else {
//			convertFn = ta -> null;  // be sure to cache a function, especially in the case where there is no match
//		}
//		taFactoryFns.put(targetTypeName, convertFn);
//		return convertFn.apply(arcTa);
//	}

	/////////////////////////////////////////////////////////////////////
	protected enum TemporalTargetType {
		DATE (Date.class.getName()),
		GREGORIAN_CALENDAR(GregorianCalendar.class.getName()),
		INSTANT(Instant.class.getName()),
		LOCAL_DATE(LocalDate.class.getName()),
		LOCAL_DATE_TIME(LocalDateTime.class.getName()),
		LOCAL_TIME(LocalTime.class.getName()),
		MONTH_DAY(MonthDay.class.getName()),
		OFFSET_DATE_TIME(OffsetDateTime.class.getName()),
		OFFSET_TIME(OffsetTime.class.getName()),
		YEAR(Year.class.getName()),
		YEAR_MONTH(YearMonth.class.getName()),
		ZONED_DATE_TIME(ZonedDateTime.class.getName());

		private static final Map<String, TemporalTargetType> VALUES_BY_TYPE_NAME = new HashMap<>();
		static {
			for (TemporalTargetType ttt : values()) {
				VALUES_BY_TYPE_NAME.put(ttt.targetType, ttt);
			}
		}
		protected final String targetType;

		private TemporalTargetType(String targetType) {
			this.targetType = targetType;
		}

		protected static final TemporalTargetType byTypeName(String typeName) {
			return VALUES_BY_TYPE_NAME.get(typeName);
		}
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<TemporalDecoder> {
		private final EnonConfig config;

		public Acceptor(EnonConfig config) {
			this.config = config;
		}

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return config.hasFeature(FeatureSet.X) && TemporalTargetType.byTypeName(targetType.getTypeName()) != null
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected TemporalDecoder create(Type targetType) {
			return new TemporalDecoder(reflectUtil.classOfType(targetType), ZoneId.systemDefault());
		}

	}
}
