/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.enon.exception.EnonDecoderException;

/**
 *
 * @author clininger $Id: $
 */
public class Value {

	public static Set<String> TRUE_STRINGS = new HashSet<>(
			Arrays.asList("true", "True", "TRUE", "t", "T", "yes", "Yes", "YES", "y", "Y", "on", "On", "ON", "1")
	);
	public static Set<String> FALSE_STRINGS = new HashSet<>(
			Arrays.asList("false", "False", "FALSE", "f", "F", "no", "No", "NO", "n", "N", "off", "Off", "OFF", "0")
	);

	/**
	 * Return TRUE if the strValue is one of the TRUE_STRINGS, FALSE if it is one of the FALSE strings, 
	 * null if it is neither.
	 * @param strValue The value to test
	 * @return Boolean value
	 */
	public static Boolean trueOrFalse(String strValue) {
		if (Value.TRUE_STRINGS.contains(strValue)) {
			return true;
		}
		if (Value.FALSE_STRINGS.contains(strValue)) {
			return false;
		}
		for (String t : Value.TRUE_STRINGS) {
			if (t.compareToIgnoreCase(strValue) == 0) {
				return true;
			}
		}
		for (String t : Value.FALSE_STRINGS) {
			if (t.compareToIgnoreCase(strValue) == 0) {
				return false;
			}
		}
		return null;
	}
	
	/**
	 * Call trueOrFalse(String), but throw an exception if the result is null
	 * @param strValue value to test
	 * @return boolean value
	 * @throws EnonDecoderException if the value is neither true nor false
	 */
	public static boolean trueOrFalseNotNull(String strValue) {
		Boolean bool = trueOrFalse(strValue);
		if (bool != null) {
			return bool;
		}
		throw new EnonDecoderException("Unable to interpret string as boolean: "+strValue);
	}
}
