/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.util.EnumSet;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.codec.DataCategory;
import org.enon.codec.Value;
import org.enon.element.ConstantElement;
import org.enon.element.Element;
import org.enon.exception.EnonCoderException;
import org.enon.bind.value.NativeValue;

/**
 * Coder for the "constant" values null, true, false
 *
 * @author clininger $Id: $
 */
public class BooleanCoder extends AbstractCoder {

	public BooleanCoder(EnonConfig config) {
		super(config);
	}

	@Override
	public Set<DataCategory> handles() {
		return EnumSet.of(DataCategory.BOOLEAN);
	}

	@Override
	public Element getElement(NativeValue nativeValue) {
		return extractBoolean(nativeValue) ? ConstantElement.trueInstance() : ConstantElement.falseInstance();
	}

	/**
	 * Extract the value if it is a boolean already. Override this method to interpret other values as boolean
   * <p>Interprets the following as boolean:
   * <ul>
   * <li>String: value != null </li>
   * <li>Character: value != null </li>
   * <li>Number: value != 0</li>
   * </ul>
	 *
	 * @param nativeValue value to interpret as boolean
	 * @return boolean value
   * @throws EnonCoderException if the value can't be interpreted as boolean (see above)
	 */
	protected boolean extractBoolean(NativeValue nativeValue) {
		Object value = nativeValue.getValue();
		if (value instanceof Boolean) {
			return (Boolean) value;
		} else if (value instanceof String) {
			Boolean result = testString((String) value);
			if (result != null) {
				return result;
			}
		} else if (value instanceof Character) {
			Boolean result = testString("" + ((Character) value));
			if (result != null) {
				return result;
			}
		} else if (value instanceof Number) {
			return ((Number) value).intValue() != 0;
		}
		throw new EnonCoderException("Could not extract boolean value from type "
				+ value.getClass() + " with value of " + String.valueOf(value));
	}

	private Boolean testString(String value) {
		// accept some commonly used boolean values
		if (trueStrings().contains(value)) {
			return true;
		}
		if (falseStrings().contains(value)) {
			return false;
		}
		for (String trueString : trueStrings()) {
			if (0 == trueString.compareToIgnoreCase(value)) {
				return true;
			}
		}
		for (String falseString : falseStrings()) {
			if (0 == falseString.compareToIgnoreCase(value)) {
				return false;
			}
		}
		return null;
	}

	protected Set<String> trueStrings() {
		return Value.TRUE_STRINGS;
	}

	protected Set<String> falseStrings() {
		return Value.FALSE_STRINGS;
	}
}
