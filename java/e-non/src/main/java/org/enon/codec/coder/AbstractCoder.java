/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.element.Element;
import org.enon.bind.value.NativeValue;

/**
 *
 * @author clininger $Id: $
 * @param <NV> Type of native value
 * @param <E> Type of Element
 */
public abstract class AbstractCoder<NV extends NativeValue, E extends Element> implements Coder<NV, E> {

	protected final EnonConfig config;
	protected final boolean hasFeatureX;

	public AbstractCoder(EnonConfig config) {
		this.config = config;
		this.hasFeatureX = config.hasFeature(FeatureSet.X);
	}

	public EnonConfig getConfig() {
		return config;
	}

}
