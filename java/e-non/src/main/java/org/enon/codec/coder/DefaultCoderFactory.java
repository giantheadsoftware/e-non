/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.lang.reflect.InvocationTargetException;
import java.util.EnumMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.codec.DataCategory;
import org.enon.exception.EnonCoderException;

/**
 *
 * @author clininger $Id: $
 */
public class DefaultCoderFactory implements CoderFactory {

	private final EnonConfig config;

	private volatile Map<DataCategory, Coder> coderCache;

	public DefaultCoderFactory(EnonConfig config) {
		this.config = config;
	}

	@Override
	public Coder coder(DataCategory dataCategory) {
		Map<DataCategory, Coder> coderCacheLocal = this.coderCache;
		if (coderCacheLocal == null) {
			synchronized(this) {
				coderCacheLocal = this.coderCache;
				if (coderCacheLocal == null) {
					this.coderCache = coderCacheLocal = new EnumMap<>(DataCategory.class);
				}
			}
		}

		Coder coder = coderCacheLocal.get(dataCategory);
		if (coder != null) {
			return coder;
		}

		// look up the class of the coder based on the data category
		Class<? extends Coder> coderClass = coderClass(dataCategory);

		// instantiate the coder
		try {
			coder = coderClass.getConstructor(EnonConfig.class).newInstance(config);
			coderCacheLocal.put(dataCategory, coder);
			return coder;
		} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException
				| InstantiationException | InvocationTargetException ex) {
			throw new EnonCoderException("Could not construct coder: " + coderClass.getName(), ex);
		}
	}

	/**
	 * Select a coder implementation class based on the data category
	 *
	 * @param datCat
	 * @return
	 */
	private Class<? extends Coder> coderClass(DataCategory datCat) {
		switch (datCat) {
			case STRING:
				return StringCoder.class;
			case NULL:
				return NullCoder.class;
			case NUM_INT:
				return IntCoder.class;
			case NUM_REAL:
				return RealCoder.class;
			case BOOLEAN:
				return BooleanCoder.class;
			case MAP:
				return MapCoder.class;
			case COLLECTION:
				return ListCoder.class;
			case BYTES:
				return BlobCoder.class;
			case PRIMITIVE_ARRAY:
				return ArrayCoder.class;
			case TEMPORAL:
				return TemporalCoder.class;
			default:
				throw new EnonCoderException("Unsupported Data Category: " + datCat.name());
		}
	}
}
