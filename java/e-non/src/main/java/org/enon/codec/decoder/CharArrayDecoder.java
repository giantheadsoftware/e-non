/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.ScalarElement;
import org.enon.element.array.ByteArrayElement;
import org.enon.exception.EnonDecoderException;

/**
 *
 * @author clininger $Id: $
 */
public class CharArrayDecoder implements Decoder<char[]> {

	protected CharArrayDecoder() {
	}

	@Override
	public char[] decode(Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}

		if (e.getType() == ElementType.BLOB_ELEMENT) {
			CharBuffer buf = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(((ByteArrayElement) e).getContentBytes()));
			char[] result = new char[buf.limit()];
			buf.get(result);
			return result;
		}

		if (e instanceof ScalarElement) {
			return String.valueOf(e.getValue()).toCharArray();
		}

		throw new EnonDecoderException("Unable to decode " + e.getType() + " to char[]");
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<CharArrayDecoder> {

		private final CharArrayDecoder instance = new CharArrayDecoder();

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return char[].class == reflectUtil.classOfType(targetType) ? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected CharArrayDecoder create(Type targetType) {
			return instance;
		}

	}

}
