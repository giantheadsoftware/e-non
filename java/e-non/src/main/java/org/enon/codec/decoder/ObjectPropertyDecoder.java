/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.ReaderContext;
import org.enon.bind.property.WritableProperty;
import org.enon.bind.property.WriteAccessor;
import org.enon.element.ElementType;
import org.enon.element.Element;
import org.enon.exception.EnonDecoderException;
import org.enon.exception.EnonException;
import org.enon.util.ReflectUtil;

/**
 * Decode a MapElement into some target object.<p>
 * This implementation does a "best effort" merge:  properties are only assigned when they exist in both the target class
 * and the MapElement.  Properties that exist in one but not the other are ignored.
 * @author clininger $Id: $
 * @param <O> The object type produced by this decoder
 */
public class ObjectPropertyDecoder<O> implements Decoder<O> {

	/** the type being created */
	protected final Class<O> targetType;
	/** constructor for the type being created */
	protected final Constructor<O> targetConstructor;
	/** A map of the properties in the target type */
	protected final Map<String, WritableProperty> properties;
	/** The decoder that will decode the MapElement that represents the target object in the e-NON stream */
	protected final MapDecoder<Map<String, Element>> mapDecoder
					= new MapDecoder.Acceptor().decoderFor(HashMap.class, String.class, Element.class);

	/**
	 * Constructor
	 * @param targetConstructor Constructor for the targetType
	 * @param accessor Provides access to the target type's writable properties
	 */
	protected ObjectPropertyDecoder(Constructor<O> targetConstructor, WriteAccessor accessor) {
		this.targetConstructor = targetConstructor;
		this.targetType = targetConstructor.getDeclaringClass();
		this.properties = (Map<String, WritableProperty>) accessor.getwritableProperties(targetType);
	}

	@Override
	public O decode(final Element e) {
		ElementType elementType = e.getType();
		if (elementType == ElementType.NULL_ELEMENT) {
			return null;
		}

		if (elementType == ElementType.MAP_ELEMENT) {
			Map<String, Element> map = mapDecoder.decode(e);

			// instantiate the target object
			final O o = newO();

			// get the decoder factory from the context - need it to decode property values
//			final DecoderFactory decoderFactory = e.getReaderContext().getConfig().getDecoderFactory();
			ReaderContext ctx = e.getReaderContext();

			// for all the properties existing in the target type, look for a matching entry in the MapElement
			properties.entrySet().stream()
					.forEach(entry -> {
						WritableProperty prop = entry.getValue();
						Element propertyElement = map.get(prop.getName());
						if (propertyElement != null) {
							try {
								Object value = ctx.decode(prop.getPropertyType(), propertyElement);
								prop.writeValue(o, value);
							} catch (EnonException x) {
								throw new EnonDecoderException("Unable to decode object property '"+prop.getName()+"' of type "+targetType.getName(), x);
							}
						}
					});
			return o;
		}

		throw new EnonDecoderException("Unable to decode " + e.getType() + " to "+targetType.getName());
	}

	/**
	 * Instantiate a new O
	 * @return new O instance
	 */
	protected O newO() {
		try {
			return targetConstructor.newInstance();
		} catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException x) {
			throw new EnonDecoderException("Unable to construct collection instance for type " + targetType.getName(), x);
		}
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<ObjectPropertyDecoder> {

		protected final EnonConfig config;

		public Acceptor(EnonConfig config) {
			this.config = config;
		}

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			try {
				Class c = ReflectUtil.getinstance().classOfType(targetType); // classOfType will throw if type can't resolve to a class
				int modifiers = c.getModifiers();
				// can't instantiate abstract of non-public classes
				if (Modifier.isAbstract(modifiers) ||  !Modifier.isPublic(modifiers))
					return AcceptLevel.NEVER;
				// need to have a public no-arg constructor
				return c.getConstructor() != null ? AcceptLevel.PREFER : AcceptLevel.NEVER;
			} catch (EnonException | NoSuchMethodException x) {
				return AcceptLevel.NEVER;
			}
		}

		@Override
		protected ObjectPropertyDecoder create(Type targetType) {
			try {
				Constructor c = reflectUtil.classOfType(targetType).getConstructor();
				return new ObjectPropertyDecoder(c, config.getPropertyAccessorFactory().getPojoWriteAccessor());
			} catch (EnonException | NoSuchMethodException x) {
				throw new EnonDecoderException("Unable to create ObjectPropertyDecoder for target type "+targetType.getTypeName(), x);
			}
		}
	}
}
