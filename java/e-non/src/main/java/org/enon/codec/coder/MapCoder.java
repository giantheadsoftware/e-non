/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.util.EnumSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import org.enon.EnonConfig;
import org.enon.bind.value.MapValue;
import org.enon.codec.DataCategory;
import org.enon.element.Element;
import org.enon.element.MapElement;
import org.enon.element.MapEntryElement;
import org.enon.bind.value.NativeValue;

/**
 *
 * @author clininger $Id: $
 */
public class MapCoder extends AbstractCoder<MapValue, MapElement> {

	/**
	 * Need to have this constructor signature for the CoderFactory
	 *
	 * @param config the EnonConfig
	 */
	public MapCoder(EnonConfig config) {
		super(config);
	}

	@Override
	public Set<DataCategory> handles() {
		return EnumSet.of(DataCategory.MAP);
	}

	@Override
	public MapElement getElement(MapValue nativeValue) {
		List<MapEntryElement<Element,Element>> entries = ((MapValue<NativeValue, NativeValue>) nativeValue).getValue().entrySet().stream()
				.map(this::toMapEntryElement)
				.collect(Collectors.toList());

		return new MapElement(entries);
	}

	private MapEntryElement<Element, Element> toMapEntryElement(Entry<? extends NativeValue, ? extends NativeValue> nativeEntry) {
		NativeValue nativeEntryKey = nativeEntry.getKey();
		Element key = config.getCoderFactory().encode(nativeEntryKey);
		NativeValue nativeEntryValue = nativeEntry.getValue();
		Element value = config.getCoderFactory().encode(nativeEntryValue);
		return new MapEntryElement<>(key, value);
	}
}
