/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import static org.enon.element.ElementType.*;
import org.enon.element.Element;
import org.enon.exception.EnonDecoderException;
import org.enon.util.IntType;
import org.enon.util.ReflectUtil;

/**
 *
 * @author clininger $Id: $
 * @param <T> Type of number produced by this decoder
 */
public class IntDecoder<T extends Number> implements Decoder<T> {

	protected final IntType intTargetType;

	protected IntDecoder(IntType intTargetType) {
		this.intTargetType = intTargetType;
	}

	@Override
	public T decode(Element e) {
		if (e.getType() == NULL_ELEMENT) {
			if (intTargetType.isPrimitive()) {
				throw new EnonDecoderException("Attempt to assign null to primitive value");
			}
			return null;
		}

		// if the value type already matches the target type just use the value as-is
		Object value = e.getValue();
		if (intTargetType.type == value.getClass()) {
			return (T) value;
		}

		try {
			switch (e.getType()) {
				case BYTE_ELEMENT:
				case INT_ELEMENT:
				case LONG_ELEMENT:
				case NANO_INT_ELEMENT:
				case SHORT_ELEMENT:
				case FLOAT_ELEMENT:
				case DOUBLE_ELEMENT:
					// if the element numeric, attempt to scale/cast the value to the target type
					return (T) intTargetType.cast((Number) value);

				case NUMBER_ELEMENT:
				case STRING_ELEMENT:
					// string-based type; attempt to interpret as real number
					return (T) intTargetType.fromString(String.valueOf(value));

				case TRUE_ELEMENT:
					return (T) intTargetType.cast(1);

				case FALSE_ELEMENT:
					return (T) intTargetType.cast(0);
			}
		} catch (Exception x) {
			throw new EnonDecoderException("Unable to decode " + e.getType() + " to " + intTargetType.type, x);
		}
		throw new EnonDecoderException("Unable to decode " + e.getType() + " to " + intTargetType.type);
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<IntDecoder> {

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return IntType.valueOfType(ReflectUtil.getinstance().classOfType(targetType)) != null
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected IntDecoder create(Type targetType) {
			IntType intType = IntType.valueOfType(ReflectUtil.getinstance().classOfType(targetType));
			if (intType != null) {
				return new IntDecoder(intType);
			}
			return null;
		}
	}

}
