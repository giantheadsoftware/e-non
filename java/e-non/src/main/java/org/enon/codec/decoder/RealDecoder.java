/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import static org.enon.element.ElementType.*;
import org.enon.element.Element;
import org.enon.exception.EnonDecoderException;
import org.enon.util.RealType;
import org.enon.util.ReflectUtil;

/**
 *
 * @author clininger $Id: $
 * @param <T> The type of Number this decoder decodes. Can be Double, Float, or BigDecimal.
 */
public class RealDecoder<T extends Number> implements Decoder<T> {

	protected final RealType realType;

	protected RealDecoder(RealType realType) {
		this.realType = realType;
	}

	@Override
	public T decode(final Element e) {
		if (e.getType() == NULL_ELEMENT) {
			if (realType.isPrimitive()) {
				throw new EnonDecoderException("Attempt to assign null to primitive value");
			}
			return null;
		}

		// if the value type already matches the target type just use the value as-is
		Object value = e.getValue();
		if (realType.type == value.getClass()) {
			return (T) value;
		}

		try {
			switch (e.getType()) {
				case FLOAT_ELEMENT:
				case DOUBLE_ELEMENT:
				case INFINITY_NEG_ELEMENT:
				case INFINITY_POS_ELEMENT:
				case NAN_ELEMENT:
				case BYTE_ELEMENT:
				case INT_ELEMENT:
				case LONG_ELEMENT:
				case NANO_INT_ELEMENT:
				case SHORT_ELEMENT:
					return (T) realType.cast((Number) value);

				case STRING_ELEMENT:
				case NUMBER_ELEMENT:
					return (T) realType.fromString(String.valueOf(value));

				case TRUE_ELEMENT:
					return (T) realType.cast(1.0);

				case FALSE_ELEMENT:
					return (T) realType.cast(0.0);
			}
		} catch (Exception x) {
			throw new EnonDecoderException("Unable to decode " + e.getType() + " to " + realType.type, x);
		}
		throw new EnonDecoderException("Unable to decode " + e.getType() + " to " + realType.type);
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<RealDecoder> {

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return RealType.valueOfType(ReflectUtil.getinstance().classOfType(targetType)) != null
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected RealDecoder create(Type targetType) {
			return new RealDecoder(RealType.valueOfType(ReflectUtil.getinstance().classOfType(targetType)));
		}
	}

}
