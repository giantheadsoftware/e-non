/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.enon.EnonConfig;
import org.enon.exception.EnonDecoderException;

/**
 *
 * @author clininger $Id: $
 */
public class DefaultDecoderFactory implements DecoderFactory<Decoder> {

	private final EnonConfig config;

	private volatile Map<String, Decoder> decoderCache;
	private List<DecoderFactory> factories;
	private volatile List<TargetTypeAcceptor<? extends Decoder>> acceptors;
	private final CollectionDecoder.Acceptor listDecoderFactory = new CollectionDecoder.Acceptor();
	private final MapDecoder.Acceptor mapDecoderFactory = new MapDecoder.Acceptor();

	public DefaultDecoderFactory(EnonConfig config) {
		this.config = config;
	}

	/**
	 * Look up a decoder by a type name.  If the name is null, the NaturalDcoder is used.
	 * If a decoder is cached for the name, that decoder is used.  Otherwise, if the name is a fully qualified class name,
	 * a decoder may be created for that class.  If no decoder can be created, the NaturalDecoder is selected, which
	 * @param targetTypeName A type name.  This is not necessarily a full classname, but something which may be found in the decoder cache.
	 * @return a decoder that will produce the appropriate data type.
	 * @throws EnonDecoderException if no decoder can be found.
	 */
	@Override
	public Decoder decoderFor(String targetTypeName) {
		if (targetTypeName == null) {
			return NaturalDecoder.Acceptor.INSTANCE;
		}
		Decoder decoder = getDecoderCache().get(targetTypeName);
		if (decoder == null) {
			decoder = createDecoder(targetTypeName);
			getDecoderCache().put(targetTypeName, decoder);
		}
		return decoder;
	}

	/**
	 * Return a decoder capable of producing the target type. Supported Type implementations are Class and ParameterizedType. A ParameterizedType can be used with
	 * ListDecoder and MapDecoder to provide the item types within those containers. Generally, a ParameterizedType only contains generic type information when it
	 * is taken from a class definition, such as ((Method)m).getGenericReturnType() or ((Method)m).getGenericParameterTypes()
	 *
	 * @param targetType The type that the decoder must create.
	 * @return a decoder for the target type
	 */
	@Override
	public Decoder decoderFor(Type targetType) {
		if (targetType == null) {
			return NaturalDecoder.Acceptor.INSTANCE;
		}
		Decoder decoder = getDecoderCache().get(targetType.getTypeName());
		if (decoder == null) {
			decoder = createDecoder(targetType);
			getDecoderCache().put(targetType.getTypeName(), decoder);
		}
		return decoder;
	}

	@Override
	public CollectionDecoder collectionDecoder(Class<? extends Collection> targetClass, Type itemType) {
		return listDecoderFactory.decoderFor(targetClass, itemType);
	}

	@Override
	public MapDecoder mapDecoder(Class<? extends Map> targetClass, Type keyType, Type valueType) {
		return mapDecoderFactory.decoderFor(targetClass, keyType, valueType);
	}

	/**
	 * Create a decoder when one is not available in the cache.<p>
	 * Some decoders are less practical to cache, such as the List &amp; Map decoders. Those decoders are not unique based only on the container type but also on the
	 * entry types.
	 *
	 * @param targetType The type of value that the decoder must produce
	 * @return A decoder for the target type.
	 * @throws EnonDecoderException if a decoder for the type can't be created
	 */
	protected Decoder createDecoder(final Type targetType) {
//		Decoder decoder = decoderForType(targetType);
		TargetTypeAcceptor tta = acceptorForType(acceptor -> acceptor.accept(targetType));
		if (tta != null) {
			Decoder decoder = tta.decoderFor(targetType);
			getDecoderCache().put(targetType.getTypeName(), decoder);
			return decoder;
		}
		throw new EnonDecoderException("Can't locate a decoder for type "+(targetType != null ? targetType.getTypeName() : "null"));
	}

	/**
	 * Create a decoder when one is not available in the cache.<p>
	 * Some decoders are less practical to cache, such as the List &amp; Map decoders. Those decoders are no unique based only on the container type but also on the
	 * entry types.
	 *
	 * @param targetTypeName The type of value that the decoder must produce
	 * @return A decoder for the target type.
	 * @throws EnonDecoderException if a decoder for the type can't be created
	 */
	protected Decoder createDecoder(String targetTypeName) {
		TargetTypeAcceptor tta = acceptorForType(acceptor -> acceptor.accept(targetTypeName));
		if (tta != null) {
			Decoder decoder = tta.decoderFor(targetTypeName);
			getDecoderCache().put(targetTypeName, decoder);
			return decoder;
		}
//		for (DecoderFactory factory : getFactories()) {
//			Decoder decoder = factory.decoderFor(targetTypeName);
//			if (decoder != null) {
//				getDecoderCache().put(targetTypeName, decoder);
//				return decoder;
//			}
//		}
		// attempt to use targetTypeName as a classname.  This allows it to be handled
		try {
			return createDecoder(Class.forName(targetTypeName));
		} catch (ClassNotFoundException | EnonDecoderException x) {
			throw new EnonDecoderException("Can't locate a decoder for type name "+targetTypeName, x);
		}
	}

	/**
	 * Constructs the decoder cache if it doesn't exist. Subclasses can override this to customize the decoder cache. The decoder cache is checked first when
	 * searching for a decoder for a target type. The default cache does not change during DecoderFactoryOperation, but subclasses are allwoed to do what they
	 * want with the cache.
	 *
	 * @return the decoder cache
	 */
	protected Map<String, Decoder> getDecoderCache() {
		Map<String, Decoder> decoderCacheLocal;
		if ((decoderCacheLocal = decoderCache) == null) {
			synchronized(this) {
				if ((decoderCacheLocal = decoderCache) == null) {
					decoderCache = decoderCacheLocal = new HashMap<>();
				}
			}
		}
		return decoderCacheLocal;
	}

//	protected List<DecoderFactory> getFactories() {
//		if (factories != null) {
//			return factories;
//		}
//		return createFactoryList();
//	}

	/**
	 * Iterate through the list of acceptorImplClasses looking for the one that makes the greatest claim on the type via its accept() method.
	 * <p>
	 * For each acceptor, the <b>fn</b> will be invoked to determine the AcceptLevel of that acceptor.
	 * <p>
	 * The first Acceptor that returns PREFER will be used, if any. Otherwise, the first acceptor returning the greatest claim will be used.
	 * If all acceptors claim NEVER, then null is returned.
	 *
	 * @param fn Function that will receive TargetTypeAcceptor instances.  The function should use the acceptor to evaluate the type and return an AcceptLevel for the type.
	 * @return The preferred acceptor for this type, or null if no acceptors will handle it.
	 */
	protected TargetTypeAcceptor acceptorForType(Function<TargetTypeAcceptor, TargetTypeAcceptor.AcceptLevel> fn) {
		List<TargetTypeAcceptor<? extends Decoder>> acceptorsLocal = getAcceptors();
		// set up iteration variables
		TargetTypeAcceptor.AcceptLevel currentLevel = TargetTypeAcceptor.AcceptLevel.NEVER;
		TargetTypeAcceptor currentAcceptor = null;

		// go through the list of acceptors, looking for the acceptor that can best handle the type
		for (TargetTypeAcceptor<? extends Decoder> acceptor : acceptorsLocal) {
			// get the AcceptLevel of the acceptor for this type
			TargetTypeAcceptor.AcceptLevel level = fn.apply(acceptor);

			// return the first acceptor that claims to PREFER the type
			if (TargetTypeAcceptor.AcceptLevel.PREFER == level) {
				return acceptor;
			}
			// as long as no acceptor says PREFER, keep track of the highest AcceptLevel we find.
			if (level != TargetTypeAcceptor.AcceptLevel.NEVER && level.ordinal() < currentLevel.ordinal()) {
				currentLevel = level;
				currentAcceptor = acceptor;
			}
		}
		return currentAcceptor;
	}

	/**
	 * Get (or create) the list of TargetTypeAcceptor instances that can be used to determine which
	 * decoder to apply.
	 * @return list of target type acceptors
	 */
	protected List<TargetTypeAcceptor<? extends Decoder>> getAcceptors() {
		List<TargetTypeAcceptor<? extends Decoder>> acceptorsLocal;
		// initialize the acceptors list if necessary
		if ((acceptorsLocal = acceptors) == null) {
			synchronized (this) {
				if ((acceptorsLocal = acceptors) == null) {
					acceptors = acceptorsLocal = getTargetTypeAcceptors().stream()
							.map(TargetTypeAcceptor.instantiateFn(config))
							.collect(Collectors.toList());
				}
			}
		}
		return acceptorsLocal;
	}

//	private synchronized List<DecoderFactory> createFactoryList() {
//		if (factories == null) {  // test this again in the synchronized block to avoid duplication
//			List<DecoderFactory> creating = new ArrayList<>();
//			getTargetTypeAcceptors().stream().forEach(factoryClass -> {
//				Constructor<? extends DecoderFactory> c;
//				try {
//					c = factoryClass.getConstructor(EnonConfig.class);
//					creating.add(c.newInstance(config));
//				} catch (NoSuchMethodException x) {
//					try {
//						c = factoryClass.getConstructor();
//						creating.add(c.newInstance());
//					} catch (NoSuchMethodException ex) {
//						throw new EnonDecoderException("Can't find viable constructor for DecoderFactory: "+factoryClass.getName(), ex);
//					} catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException x2) {
//					throw new EnonDecoderException("Can't construct DecoderFactory: "+factoryClass.getName(), x2);
//					}
//				} catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException x) {
//					throw new EnonDecoderException("Can't construct DecoderFactory: "+factoryClass.getName(), x);
//				}
//			});
//			factories = creating;
//		}
//		return factories;
//	}
}
