/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import static org.enon.element.ElementType.NULL_ELEMENT;
import org.enon.element.Element;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.enon.util.IntType;
import org.enon.util.ReflectUtil;

/**
 * Attempt to decode the element into any type of number.  If the number appears to be integral,
 * return the smallest number type that will contain the value.
 * @author clininger $Id: $
 */
public class NumberDecoder implements Decoder<Number> {

	protected NumberDecoder() {
	}

	@Override
	public Number decode(final Element e) {
		switch (e.getType()) {
			case NULL_ELEMENT:
				return null;

			case BYTE_ELEMENT:
			case SHORT_ELEMENT:
			case INT_ELEMENT:
			case LONG_ELEMENT:
				return IntType.compact((Number) e.getValue());

			case FLOAT_ELEMENT:
			case DOUBLE_ELEMENT:
			case INFINITY_NEG_ELEMENT:
			case INFINITY_POS_ELEMENT:
			case NAN_ELEMENT:
				return (Number) e.getValue();

			case NUMBER_ELEMENT:
			case STRING_ELEMENT:
				BigDecimal bigD = null;
				try {
					bigD = new BigDecimal(((StringElement) e).getValue());
					BigInteger bigI = bigD.toBigIntegerExact();
					return IntType.compact(bigI);
				} catch (ArithmeticException x) {
					return bigD;
				} catch (NumberFormatException x) {
					return Float.NaN;
				}

			case FALSE_ELEMENT:
				return (byte)0;

			case TRUE_ELEMENT:
				return (byte)1;
		}
		throw new EnonDecoderException("Unable to decode " + e.getType() + " to Number");
	}

	/////////////////////////////////////////////////////////////////////
	public static class Acceptor extends TargetTypeAcceptor<NumberDecoder> {

		public final NumberDecoder INSTANCE = new NumberDecoder();

		@Override
		protected AcceptLevel evaluate(Type targetType) {
			return (Number.class.isAssignableFrom(ReflectUtil.getinstance().classOfType(targetType)))
					? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		protected NumberDecoder create(Type targetType) {
			return INSTANCE;
		}

	}
}


