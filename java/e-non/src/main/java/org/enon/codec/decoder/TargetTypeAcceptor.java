/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.enon.EnonConfig;
import org.enon.exception.EnonDecoderException;
import org.enon.util.ReflectUtil;

/**
 * A TargetTypeAcceptor is used to determine what types are acceptable to a Decoder implementation.
 * This is intended to provide the DecoderFactory with information needed to select a decoder for a given type.
 * <p>
 * For example, a Decoder implementation that handles primitive types might return PREFER
 * when a primitive class is passed to the accept() method.
 * <p>
 * The TargetTypeAcceptor also serves as a factory for a Decoder implementation. An instance is
 * returned by the decoderFor() method.
 * <p>
 * The TargetTypeAccpetor is intended to be used in a two 2-phase process.  First, the accept() method is called
 * on multiple TargetTypeAcceptors to determine which decoder is most qualified to decode the type.  Then,
 * the decoderFor() method is called for just one of the TargetTypeAcceptors.
 * <p>
 * <b>Note that the decoderFor() method does not re-test the accept() result.</b>  It would be possible to
 * call decoderFor() even when accept() returns NEVER.
 * <p>
 * A TargetTypeAccessor implementation is typically associated with a single Decoder class, i.e. as inner classes
 * within the Decoder classes that they support.  However, the decoderFor() method may return any decoder type, so
 * the 1:1 pattern is not mandatory.
 *
 * @author clininger $Id: $
 * @param <D> Specific Decoder type produced by this Acceptor
 */
public abstract class TargetTypeAcceptor<D extends Decoder> {

	/**
	 * Levels of "acceptance" ordered from most preferred to least preferred
	 */
	public enum AcceptLevel {
		/** prefer to accept */
		PREFER,
		/** can accept if no other prefers */
		IF_NECESSARY,
		/** can handle what no other will accept */
		LAST_RESORT,
		/** can't accept */
		NEVER
	}

	private volatile Map<String, AcceptLevel> acceptLevels;
	private volatile Map<String, D> decoderCache;

	protected final ReflectUtil reflectUtil = ReflectUtil.getinstance();

	/**
	 * Return a function that constructs a TargetTypeAcceptor using the EnonConfig passed in.
	 * The function accepts a TargetTypeAcceptor subclass arg to be instantiated.  It searches the class for a suitable constructor.
	 * If the class has a constructor taking a config, prefer that one.  Otherwise, a no-arg constructor can be used.
	 * @param <TTA> Type var indicating a TargetTypeacceptor subclass
	 * @param config The current config of the eNON reading environment
	 * @return new instance
	 */
	public static <TTA extends TargetTypeAcceptor<? extends Decoder>> Function<Class<? extends TTA>, TTA> instantiateFn(final EnonConfig config) {
		return acceptorType -> {
			try {
				Constructor<TTA> configConstructor = null;
				Constructor<TTA> noArgConstructor = null;
				for (Constructor c : acceptorType.getConstructors()) {
					if (c.getParameterCount() == 0) {
						noArgConstructor = c;
					} else if (c.getParameterCount() == 1 && EnonConfig.class == c.getParameterTypes()[0]) {
						configConstructor = c;
					}
				}
				if (configConstructor != null) {
					return configConstructor.newInstance(config);
				}
				if (noArgConstructor != null) {
					return noArgConstructor.newInstance();
				}
				throw new EnonDecoderException("TargetTypeAcceptor no suitable constructor: " + acceptorType.getName());
			} catch (IllegalAccessException | IllegalArgumentException | InstantiationException
					| InvocationTargetException | SecurityException x) {
				throw new EnonDecoderException("Unable to instantiate TargetTypeAcceptor "+ acceptorType.getName());
			}
		};
	}

	/**
	 * Determine the level of preference to decode the type name.  This method will not return null.
	 * @param typeName Name of the type to test for acceptance level
	 * @return The accept level, not null
	 */
	public AcceptLevel accept(String typeName) {
		if (typeName == null || typeName.isEmpty()) {
			return AcceptLevel.NEVER;
		}
		AcceptLevel acceptLevel = getAcceptLevels().get(typeName);
		if (acceptLevel != null) {
			return acceptLevel;
		}
		Type type = reflectUtil.forName(typeName);
		if (type != null) {
			return cacheAcceptLevel(typeName, evaluate(type));
		}
		throw new EnonDecoderException("Error resolving type name as a type: "+typeName);
	}

	/**
	 * Determine the level of preference to decode the type.  This method will not return null.
	 * @param t the type to test for acceptance level
	 * @return The accept level, not null
	 */
	public AcceptLevel accept(Type t) {
		if (t == null) {
			return AcceptLevel.NEVER;
		}
		AcceptLevel acceptLevel = getAcceptLevels().get(t.getTypeName());
		if (acceptLevel != null) {
			return acceptLevel;
		}
		return cacheAcceptLevel(t.getTypeName(), evaluate(t));
	}

	/**
	 * Determine the AcceptLevel for the targetType.  Implementations may create &amp; cache decoder instances from within the evaluate() method.
	 * If a decoder is not created within evaluate(), then the decodeFor() methods must be overridden so that the decoder instances can be created/cached
	 * at that time.
	 * @param targetType The type to evaluate
	 * @return the AcceptLevel, should not be null.
	 */
	protected abstract AcceptLevel evaluate(Type targetType);

	/**
	 * Fetch a decoder for the targetType.  The default implementation checks the decoderCache for a previously created instance.
	 * @param targetType target type of the decoding
	 * @return decoder instance, null if not cached
	 */
	public D decoderFor(Type targetType) {
		String targetTypeName = targetType != null ? targetType.getTypeName() : null;
		D decoder = getDecoderCache().get(targetTypeName);
		if (decoder != null) {
			return decoder;
		}
		AcceptLevel acceptLevel = accept(targetType);
		if (acceptLevel != AcceptLevel.NEVER) {
			return cacheDecoder(targetTypeName, create(targetType));
		}
		return null;
	}

	/**
	 * Fetch a decoder for the targetTypeName.  The default implementation checks the decoderCache for a previously created instance.
	 * @param targetTypeName target type name of the decoding
	 * @return decoder instance, null if not cached
	 */
	public D decoderFor(String targetTypeName) {
		D decoder = getDecoderCache().get(targetTypeName);
		if (decoder != null) {
			return decoder;
		}
		AcceptLevel acceptLevel = accept(targetTypeName);
		if (acceptLevel != AcceptLevel.NEVER) {
			return cacheDecoder(targetTypeName, create(reflectUtil.forName(targetTypeName)));
		}
		return null;
	}

	/**
	 * Construct (or fetch) a decoder instance for this type.  May return null.
	 * @param targetType type that should be produced by the decoder
	 * @return decoder instance
	 */
	protected abstract D create(Type targetType);

	protected D cacheDecoder(String targetTypeName, D decoder) {
		Map<String, D> decoderCacheLocal = getDecoderCache();
		synchronized(decoderCacheLocal) {
			decoderCacheLocal.put(targetTypeName, decoder);
		}
		return decoder;
	}

	protected AcceptLevel cacheAcceptLevel(String targetTypeName, AcceptLevel acceptLevel) {
		Map<String, AcceptLevel> acceptLevelsLocal = getAcceptLevels();
		synchronized(acceptLevelsLocal) {
			acceptLevelsLocal.put(targetTypeName, acceptLevel);
		}
		return acceptLevel;
	}

	/**
	 * Get or create the decoderCache map.
	 * @return empty cache
	 */
	protected Map<String, D> getDecoderCache() {
		Map<String, D> decoderCacheLocal;
		if ((decoderCacheLocal = decoderCache) == null) {
			synchronized(this) {
				if ((decoderCacheLocal = decoderCache) == null) {
					decoderCacheLocal = new HashMap<>();
					addDefaultDecoders(decoderCacheLocal);
					decoderCache = decoderCacheLocal;
				}
			}
		}
		return decoderCacheLocal;
	}

	protected void addDefaultDecoders(Map<String, D> cache) {
		// by default, add nothing
	}

	/**
	 * Get or create the acceptLevels map.
	 * @return empty map
	 */
	protected Map<String, AcceptLevel> getAcceptLevels() {
		Map<String, AcceptLevel> acceptLevelsLocal;
		if ((acceptLevelsLocal = acceptLevels) == null) {
			synchronized(this) {
				if ((acceptLevelsLocal = acceptLevels) == null) {
					acceptLevelsLocal = new HashMap<>();
					addDefaultAcceptLevels(acceptLevelsLocal);
					acceptLevels = acceptLevelsLocal;
				}
			}
		}
		return acceptLevelsLocal;
	}

	protected void addDefaultAcceptLevels(Map<String, AcceptLevel> cache) {
		// by default add nothing
	}
}
