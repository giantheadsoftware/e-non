/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.io.IOException;
import java.io.OutputStream;
import org.enon.element.Element;
import org.enon.exception.EnonException;

/**
 *
 * @author clininger $Id: $
 */
public class Writer {

	protected final WriterContext ctx;

	/**
	 * Create a Writer with the default conf settings (eNON-0)
	 *
	 * @param toStream Stream to receive the data
	 */
	public Writer(final OutputStream toStream) {
		this(toStream, EnonConfig.defaults());
	}

	/**
	 * Create a Writer with the provided config.
	 *
	 * @param out Stream to receive the data
	 * @param config Configuration to use.
	 */
	public Writer(final OutputStream out, final EnonConfig config) {
		ctx = new WriterContext(out, config == null ? EnonConfig.defaults() : config);
	}

	protected Writer(WriterContext ctx) {
		this.ctx = ctx;
	}

	/**
	 * Get the config used by this Writer instance.  Delegates to the WriterContext
	 *
	 * @return The Writer's config
	 */
	public EnonConfig getConfig() {
		return ctx.getConfig();
	}

	/**
	 * Write the object to the stream provided in the constructor
	 *
	 * @param <W> writer type of this object
	 * @param object The object to write as eNON
	 * @return this writer
	 */
	public <W extends Writer> W write(final Object object) {
		ctx.writeProlog();
		return writeObject(object);
	}

	protected <W extends Writer> W writeObject(final Object object) {
		final Element element = getConfig().getCoderFactory().encode(getConfig().getNativeValueFactory().nativeValue(object));
		element.inContainer(ctx.getRoot());
		ctx.write(element);
		return (W) this;
	}

	public void flush() throws IOException {
		ctx.getOutputStream().flush();
	}

	/**
	 * Close the output stream used by this Writer.
	 */
	public void close() {
		try {
			ctx.getOutputStream().close();
		} catch (IOException ex) {
			throw new EnonException("Could not close output stream", ex);
		}
	}

}
