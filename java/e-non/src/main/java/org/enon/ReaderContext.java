/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Set;
import org.enon.element.Element;
import org.enon.element.reader.ElementReader;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public class ReaderContext extends EnonContext {

	private final InputStream in;
	private DataInputStream dataIn;

	public ReaderContext(InputStream in) {
		this(in, null);
	}

	public ReaderContext(InputStream in, EnonConfig config) {
		super(config);
		this.in = in;
		if (in instanceof DataInputStream) {
			dataIn = (DataInputStream) in;
		}
	}

	/**
	 * Use the ElementReaderFactory (provided by the config) to read the prolog from the input stream.
	 * @return The prolog
	 * @throws EnonReadException if ah IOException prevents reading
	 */
	public Prolog readProlog() {
		if (getProlog() != null) {
			throw new EnonReadException("The prolog can only be read once for a given stream.");
		}
		try {
			setProlog(config.getElementReaderFactory().prologReader().read(this));
		} catch (IOException x) {
			throw new EnonReadException("Unable to read prolog: ", x);
		}
		return getProlog();
	}

	public short getVersion() {
		if (getProlog() != null) {
			return getProlog().getVersion();
		}
		throw new EnonReadException("Must read the prolog before asking the version");
	}

	public Set<FeatureSet> getRequiredFeatures() {
		if (getProlog() != null) {
			return getProlog().getRequiredFeatures();
		}
		throw new EnonReadException("Must read the prolog before asking the required features");
	}

	/**
	 *
	 * @return the e-NON input stream
	 */
	public InputStream getInputStream() {
		return in;
	}

	/**
	 * Wraps the InputStream in a DataInputStream.
	 * @return new or previously created data stream
	 */
	public DataInputStream getDataInputStream() {
		if (dataIn == null) {
			dataIn = new DataInputStream(in);
		}
		return dataIn;
	}

	/**
	 * Read the next element from the input stream and wraps it into a ReadingElement.
	 * @return The next element or null if the end of stream has been reached
	 * @throws IOException on io error
	 */
	public Element parseNextElement() throws IOException {
		ElementReader reader = config.getElementReaderFactory().reader(in);
		return reader != null ? reader.read(this) : null;
	}

	/**
	 * Use the DecoderFactory (provided by the config) to lookup a decoder instance for the type.  Use that decoder to
	 * produce the result.
	 * @param <T> Type variable for required output type
	 * @param type Class of the required output type
	 * @param e The element to decode
	 * @return The decoded value
	 */
	public <T> T decode(Type type, Element e) {
		return (T) config.getDecoderFactory().decoderFor(type)
				.decode(e);
	}
}
