/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import org.enon.element.RootContainer;
import org.enon.event.EventPublisher;
import org.enon.event.NoOpEventPublisher;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public abstract class EnonContext {

	protected final EnonConfig config;
	private Prolog prolog;
	private RootContainer root;
	private EventPublisher eventPublisher = new NoOpEventPublisher();

	public EnonContext(EnonConfig config) {
		this.config = config != null ? config : EnonConfig.defaults();
	}

	public EnonConfig getConfig() {
		return config;
	}

	public Prolog getProlog() {
		return prolog;
	}

	public void setProlog(Prolog prolog) {
		if (this.prolog != null) {
			throw new EnonReadException("The prolog can only be set once.");
		}
		this.prolog = prolog;
	}

	public <C extends EnonContext> C eventPublisher(EventPublisher eventPublisher) {
		this.eventPublisher = eventPublisher;
		return (C) this;
	}

	public EventPublisher getEventPublisher() {
		return eventPublisher;
	}

	/**
	 * Lazy-initialize the RootContainer
	 * @return The root container associated with this context
	 */
	public RootContainer getRoot() {
		if (root == null) {
			root = new RootContainer(this);
		}
		return root;
	}

}
