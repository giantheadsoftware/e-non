/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.enon.element.ElementType;
import org.enon.exception.EnonException;

/**
 * The set of elements appearing in eNON-txt.
 *
 * Having multi-character prefixes improves readability, but is less compact and a bit more complicated to process.
 *
 * @author clininger $Id: $
 */
public enum TxtElementType {
	PROLOG("#enon-txt", null),
	NULL("null", ElementType.NULL_ELEMENT),
	TRUE("true", ElementType.TRUE_ELEMENT),
	FALSE("false", ElementType.FALSE_ELEMENT),
	INFINITY_POS("+", ElementType.INFINITY_POS_ELEMENT),
	INFINITY_NEG("-", ElementType.INFINITY_NEG_ELEMENT),
	NAN("nan", ElementType.NAN_ELEMENT),
	BYTE("b", ElementType.BYTE_ELEMENT),
	SHORT("s", ElementType.SHORT_ELEMENT),
	INT("i", ElementType.INT_ELEMENT),
	LONG("l", ElementType.LONG_ELEMENT),
	FLOAT("f", ElementType.FLOAT_ELEMENT),
	DOUBLE("d", ElementType.DOUBLE_ELEMENT),
	NUMBER("n", ElementType.NUMBER_ELEMENT),
	STRING("\"", ElementType.STRING_ELEMENT),
	STRING_UNICODE("U\"", null),
	BLOB("B", ElementType.BLOB_ELEMENT),
	LIST("[", ElementType.LIST_ELEMENT),
	MAP("{", ElementType.MAP_ELEMENT),
	LIST_END("]", null),
	MAP_END("}", null),
	ARRAY("(", ElementType.ARRAY_ELEMENT);

	/** These types may have continuation lines: STRING, STRING_UNICODE, BLOB */
	public static final Set<TxtElementType> CONTINUABLE_TYPES = EnumSet.of(STRING, STRING_UNICODE, BLOB, ARRAY);
	
	private static final Map<String, TxtElementType> BY_PREFIX = new HashMap<>();
	private static final Map<ElementType, TxtElementType> BY_ELEMENT_TYPE = new HashMap<>();
	private static final TxtElementType[] SORTED = Arrays.copyOf(TxtElementType.values(), TxtElementType.values().length);

	static {
		// populate the lookup maps
		for (TxtElementType te : values()) {
			if (null != BY_PREFIX.put(te.prefix, te)) {
				throw new EnonException("Coding error: Duplicate TxtElement prefix: "+te.prefix);
			}
			if (te.elementType != null) {
				if (null != BY_ELEMENT_TYPE.put(te.elementType, te)) {
					throw new EnonException("Coding error: Duplicate TxtElement element type: "+te.prefix);
				}
			}
		}
		// sort longest prefix first
		Arrays.sort(SORTED, (teType1, teType2) -> Integer.compare(teType2.prefix.length(), teType1.prefix.length()));
	}

	/** The prefix is the first non-whitespace chars to appear on a line. */
	public final String prefix;
	public final ElementType elementType;

	private TxtElementType(final String prefix, ElementType elementType) {
		this.prefix = prefix;
		this.elementType = elementType;
	}

	/**
	 * Return the TxtElementType that is an exact match for the prefix.  If non-exact matching is required,
	 * use forLine().
	 * @param prefix Prefix that might match one of the TxtElementType prefixes
	 * @return Matching TxtElement type, or null if no match.
	 */
	public static TxtElementType forPrefix(final String prefix) {
		return BY_PREFIX.get(prefix);
	}

	/**
	 * Get the TxtElementType associated with the ElementType.
	 * @param type The type to look up
	 * @return The associated TxtElementType or null if there is none.
	 */
	public static TxtElementType forElementType(ElementType type) {
		return BY_ELEMENT_TYPE.get(type);
	}

	/**
	 * Return the TxtElementType by examining the start of the line.  The longest prefix that
	 * matches the start of the line is used to determine the result.  This method does not trim leading white space - do that before calling
	 * @param line String that may or may not begin with a TxtElementType prefix.
	 * @return The matching TxtElementType or null if there is no match.
	 */
	public static TxtElementType forLine(String line) {
		if (line == null || line.length() == 0) {
			return null;
		}
		// try an exact match first
		TxtElementType type = forPrefix(line);
		if (type != null) {
			return type;
		}
		// iterate the prefixes looking for a match
		for (TxtElementType teType : SORTED) {
			if (line.startsWith(teType.prefix)) {
				return teType;
			}
		}
		return null;
	}
	
	/**
	 * @return true is this type accepts continuation lines
	 */
	public boolean mayContinue() {
		return CONTINUABLE_TYPES.contains(this);
	}
}
