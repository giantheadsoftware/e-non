/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.enon.element.ElementSubtype;
import org.enon.exception.EnonElementException;

/**
 *
 * @author clininger $Id: $
 */
public class TxtElement {
	
	private final TxtElementType type;
	private final ElementSubtype subType;
	private final String strValue;
	private List<ContinuationElement> continuationElements;

	/**
	 * Construct a new TxtElement without a subtype.
	 * @param type The element type
	 * @param strValue The string that appears immediately after the element prefix on the same line as the prefix.
	 * This value may be an empty string, and should not end with a newline char.  Continuation lines should not be included here.
	 */
	public TxtElement(TxtElementType type, String strValue) {
		this(type, null, strValue);
	}

	/**
	 * Construct a new TxtElement.
	 * @param type The element type
	 * @param subType The element subtype, for types that support subtypes
	 * @param strValue The string that appears immediately after the element and subtype prefixes on the same line as the prefixes.
	 * This value may be an empty string, and should not end with a newline char.  Continuation lines should not be included here.
	 */
	public TxtElement(TxtElementType type, ElementSubtype subType, String strValue) {
		this.type = type;
		this.strValue = strValue;
		this.subType = subType;
	}

	/**
	 * The type of this element.
	 * @return the type of this element
	 */
	public TxtElementType getType() {
		return type;
	}

	/**
	 * The subtype of the element.
	 * @return the subtype of the element
	 */
	public ElementSubtype getSubType() {
		return subType;
	}

	/**
	 * The text that appears immediately after the prefix, on the same line as the prefix.
	 * @return The text immediately following the prefix
	 */
	public String getStrValue() {
		return strValue;
	}
	
	/**
	 * A list of continuation elements that have been added to this TxtElement.
	 * @return A non-null list of continuation elements (may be empty).
	 */
	public List<ContinuationElement> getContinuationElements() {
		return continuationElements != null ? continuationElements : Collections.EMPTY_LIST;
	}
	
	/**
	 * Add a continuation line to this TxtElement.
	 * @param type The continuation type
	 * @param value The text immediately following the continuation prefix, up to the end of the line.  
	 * This should not contain the continuation prefix nor the newline character.
	 * @throws EnonElementException if the TxtElement does not support continuation lines.
	 */
	public void addContinuation(TxtContinuationType type, String value) {
		if (!this.type.mayContinue()) {
			throw new EnonElementException("Txt Elment type does not support continuation lines: "+this.type);
		}
		if (continuationElements == null) {
			continuationElements = new LinkedList<>();
		}
		continuationElements.add(new ContinuationElement(type, value));
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Inner class representing a continuation.
	 */
	public static class ContinuationElement {
		public final TxtContinuationType type;
		public String value;

		public ContinuationElement(TxtContinuationType type, String value) {
			this.type = type;
			this.value = value;
		}
		
	}
}
