/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.enon.element.array.ByteArrayElement;
import org.enon.element.Element;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.ElementType;
import org.enon.element.ListElement;
import org.enon.element.MapElement;
import org.enon.element.StringElement;
import org.enon.element.array.ArrayElement;
import org.enon.element.writer.ElementWriter;

/**
 *
 * @author clininger $Id: $
 */
public class TxtElementWriter implements ElementWriter<Element> {
	public static final String PROLOG = "#enon-txt";
	public static final String CR_UNICODE_ESCAPE = "\\uD;";

	@Override
	public void write(Element e) {
		TxtWriterContext txtCtx = (TxtWriterContext)e.getWriterContext();
		PrintWriter pw = txtCtx.getPrintWriter();
		String pad = txtCtx.isPadded() ? " " : "";
		TxtElementType txtType = TxtElementType.forElementType(e.getType());

		ElementType type = e.getType();
		switch(type) {
			case NULL_ELEMENT:
			case TRUE_ELEMENT:
			case FALSE_ELEMENT:
			case INFINITY_NEG_ELEMENT:
			case INFINITY_POS_ELEMENT:
			case NAN_ELEMENT:
				indentln(pw, txtCtx, txtType.prefix);
				break;

			case BYTE_ELEMENT:
			case DOUBLE_ELEMENT:
			case FLOAT_ELEMENT:
			case INT_ELEMENT:
			case LONG_ELEMENT:
			case NUMBER_ELEMENT:
			case SHORT_ELEMENT:
				indentln(pw, txtCtx, txtType.prefix + pad + String.valueOf(e.getValue()));
				break;

			case NANO_INT_ELEMENT:
				indentln(pw, txtCtx, "b" + pad + String.valueOf(e.getValue()));
				break;

			case STRING_ELEMENT:
				// no padding on string element
 				indentln(pw, txtCtx, wrapString(txtCtx, txtType, ((StringElement)e).getValue()));
				break;

			case LIST_ELEMENT:
				indent(pw, txtCtx, txtType.prefix);
				if (((ListElement)e).getValue().isEmpty()) {
					pw.println(pad + ']');
				} else {
					pw.println();
					txtCtx.containerBegin();
					((ListElement<Element>)e).getValue().stream().forEach(entry -> {
						txtCtx.write(entry);
					});
					txtCtx.containerEnd();
					indentln(pw, txtCtx, ']');
				}
				break;

			case ARRAY_ELEMENT:
				ArrayElementSubtype subType = ((ArrayElement)e).getSubType();
				// convert the array to a space-delimited string
				StringBuilder sb = new StringBuilder();
				int length = Array.getLength(e.getValue());
				for (int i = 0; i < length; i++) {
					sb.append(' ').append(Array.get(e.getValue(), i));
				}
				indentln(pw, txtCtx, wrapString(txtCtx, txtType, (char)subType.prefix + sb.toString()));
				break;

			case MAP_ELEMENT:
				indent(pw, txtCtx, txtType.prefix);
				if (((MapElement)e).getValue().isEmpty()) {
					pw.println(pad + '}');
				} else {
					txtCtx.containerBegin();
					((MapElement<Element,Element>)e).getValue().stream().forEach(entry -> {
						pw.println();
						txtCtx.write(entry.getValue().getKey());
						txtCtx.write(entry.getValue().getValue());
					});
					txtCtx.containerEnd();
					indentln(pw, txtCtx, '}');
				}
				break;

			case BLOB_ELEMENT:
				indent(pw, txtCtx, txtType.prefix + pad);
				String lineSeparator = '\n' + padString(txtCtx) + TxtContinuationType.BREAK.prefix + pad;
				Base64.Encoder encoder;
				if (txtCtx.getBlockWidth() != null) {
					encoder = Base64.getMimeEncoder(txtCtx.getBlockWidth(), lineSeparator.getBytes(txtCtx.getCharset()));
				} else {
					encoder = Base64.getEncoder();
				}
				pw.println(encoder.encodeToString(((ByteArrayElement)e).getValue()));
				break;

			default:
				indentln(pw, txtCtx, "// unknown element "+type);
				break;
		}
	}

	/**
	 * Compute the leading white space based on the current container nesting level and the tab width from the context
	 * @param txtCtx
	 * @return
	 */
	private String padString(TxtWriterContext txtCtx) {
		char[] chars = new char[txtCtx.getIndentLevel() * txtCtx.getTabWidth()];
		Arrays.fill(chars, ' ');
		return chars.length > 0 ? String.valueOf(chars) : "";
	}

	private void indent(final PrintWriter pw, TxtWriterContext txtCtx, String line) {
		pw.print(padString(txtCtx));
		pw.print(line);
	}

	private void indentln(final PrintWriter pw, TxtWriterContext txtCtx, String line) {
		indent(pw, txtCtx, line);
		pw.println();
	}

	private void indentln(final PrintWriter pw, TxtWriterContext txtCtx, char c) {
		indentln(pw, txtCtx, String.valueOf(c));
	}

	/**
	 * Wrap long strings to some length less than or equal to the block width in the context,
	 * unless the block width is null.  If the string has a space or hyphen within 15 chars before the
	 * block width, the break will occur there.  Otherwise, the break will occur at the block width.
	 * @param txtCtx Contains the block width
	 * @param string The string to be wrapped.
	 * @return A string with new lines and indentation spaces inserted where the lines need to break.
	 */
	private String wrapString(TxtWriterContext txtCtx, TxtElementType type, String string) {
		final String pad = padString(txtCtx);
		final String hardline = '\n' + pad + TxtContinuationType.BREAK.prefix;  // 'hard' continuation line
		final String softline = '\n' + pad + TxtContinuationType.SOFT_BREAK.prefix; // soft continuation line
		final Integer blockWidth = txtCtx.getBlockWidth() != null ? txtCtx.getBlockWidth() - 1 : null;

		// create a list to hold the intermediate lines
		final LinkedList<String> lines = new LinkedList<>();
//		// a boolean which is true if the original string ends with '\n', '\r'.  This is used to add an extra continuation at the end.
		final Character lastChar = string.length() > 0 ? string.charAt(string.length()-1) : null;

		// Locate the indices of line terminators, \n and \r
		final Iterator<Integer> lineTerminators = findTerminators(string).iterator();

		int lineStart = 0;
		String continuation;
		boolean needsUnicodeEscape = false;

		String nextLine = nextLine(string, lineStart, lineTerminators);
		while (nextLine != null) {
			lineStart += nextLine.length();
			String currentLine = nextLine;
			
			// get the terminator for this line
			char terminator = (lineStart < string.length()) ? string.charAt(lineStart) : 0;
			if ('\r' == terminator) {
				// if the terminator is \r, add the escaped \\uD; and end with soft break
				currentLine += CR_UNICODE_ESCAPE;
				continuation = softline;
				needsUnicodeEscape = true;
			} else {
				// otherwise, end with hard break
				continuation = hardline;
			}

			lineStart++; // skip past the terminator
			// read ahead to determine if the current line is the last line
			nextLine = nextLine(string, lineStart, lineTerminators);
			boolean lastLine = nextLine == null;

			if (blockWidth == null || currentLine.length() <= blockWidth) {
				// if we don't need to wrap, just add the line to the line list.  If it's not the last line, add a hard break.
				lines.add(currentLine + (lastLine ? "" : continuation));
			} else {
				// need to wrap this line.
				String remainder = currentLine;
				while (remainder.length() > blockWidth) {
					int splitPoint = findSplitPoint(remainder, blockWidth);
					lines.add(remainder.substring(0, splitPoint));
					remainder = softline + remainder.substring(splitPoint);
				}
				lines.add(remainder + (lastLine ? "" : continuation));
			}
		}
		if (lastChar != null && lastChar == '\n') {
			lines.add(hardline);
		}

		if (type == TxtElementType.STRING && needsUnicodeEscape) {
			// switch to unicode string if needed
			lines.offerFirst(TxtElementType.STRING_UNICODE.prefix);
		} else {
			lines.offerFirst(type.prefix);
		}
		return lines.stream().reduce("", (t, u) -> t+u);
	}

	/**
	 * Return a list of all indices in the string containing a line terminator, either \r or \n.
	 * @param string String to search for line terminators
	 * @return list of line terminator indices
	 */
	private List<Integer> findTerminators(String string) {
		if (string == null || string.isEmpty()) {
			return Collections.EMPTY_LIST;
		}
		char[] chars = string.toCharArray();
		List<Integer> result = new LinkedList<>();
		int i;
		char c;
		int len = chars.length;
		for (i = 0; i < len; i++) {
			c = chars[i];
			if (c == '\n' || c == '\r') {
				result.add(i);
			}
		}
		return result;
	}
	
	/**
	 * Get a substring of the string param that begins at start and ends at the next terminator in the terminators iterator.<br>
	 * If there are no more terminators, run to the end of the string.<br>
	 * If the start is beyond the end of the string, return null.
	 * @param string The string which is the source of the next line
	 * @param start Where to start the next line
	 * @param terminators Iterator pointing to the next line terminator
	 * @return Substring of string, or null
	 */
	private String nextLine(String string, int start, Iterator<Integer> terminators) {
		if (start < string.length()) {
			if (terminators.hasNext()) {
				int end = terminators.next();
				return string.substring(start, end);
			}
			return string.substring(start);
		}
		return null;
	}
	/**
	 * Try to find whitespace or hyphen within 20% of the block limit.  If found, return that location.
	 * Otherwise, return the block width.
	 * @param line line that needs to be wrapped.  Make sure line is at least max chars long!
	 * @return
	 */
	private int findSplitPoint(String line, int max) {
		int lowerBound = (int)(max * .8);
		for (int i = max - 1; i > lowerBound - 1; i--) {
			if (Character.isWhitespace(line.charAt(i)) || line.charAt(i) == '-') {
				// add 1 to include the space/hyphen in the line before the split
				return i+1;
			}
		}
		return max;
	}
}
