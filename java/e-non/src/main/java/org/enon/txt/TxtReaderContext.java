/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.ReaderContext;
import org.enon.element.Element;
import org.enon.exception.EnonException;

/**
 *
 * @author clininger $Id: $
 */
public class TxtReaderContext extends ReaderContext {
	private static final EnonConfig DEFAULT_TXT_CONFIG = defaultConfig();

	private final TxtElementInputStream txtElementStream;
	private final Charset charset;
	private final TxtElementReaderFactory elementReaderFactory;

	public TxtReaderContext(InputStream in, EnonConfig config, Charset charset) {
		super(in, config);
		this.charset = charset;
		txtElementStream = new TxtElementInputStream(in, charset);
		elementReaderFactory = (TxtElementReaderFactory) config.getElementReaderFactory();
	}

	public static TxtReaderContext create(InputStream in) {
		return create(in, DEFAULT_TXT_CONFIG);
	}

	public static TxtReaderContext create(InputStream in, EnonConfig config) {
		return create(in, config, StandardCharsets.UTF_8);
	}

	public static TxtReaderContext create(InputStream in, Charset charset) {
		return create(in, DEFAULT_TXT_CONFIG, charset);
	}

	public static TxtReaderContext create(InputStream in, EnonConfig config, Charset charset) {
		validateConfig(config);
		return new TxtReaderContext(in, config, charset);
	}

	private static EnonConfig defaultConfig() {
		return new EnonConfig.Builder()
				.feature(FeatureSet.X).
				elementReaderFactoryImpl(TxtElementReaderFactory.class)
				.build();
	}

	private static void validateConfig(EnonConfig config) {
		if (!(config.getElementReaderFactory() instanceof TxtElementReaderFactory)) {
			throw new EnonException("When using the TxtReaderContext, the ElementREaderFactory in the config should be a subclass of TxtElementReaderFactory.");
		}
	}

	public Charset getCharset() {
		return charset;
	}

	@Override
	public Element parseNextElement() throws IOException {
		return elementReaderFactory.reader(null).read(this);
	}

	public TxtElementInputStream getTxtElementStream() {
		return txtElementStream;
	}

}
