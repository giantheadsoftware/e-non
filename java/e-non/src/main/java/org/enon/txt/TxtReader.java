/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.InputStream;
import java.nio.charset.Charset;
import org.enon.EnonConfig;
import org.enon.Prolog;
import org.enon.Reader;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public class TxtReader extends Reader {
	private final TxtReaderContext txtCtx;

	/**
	 * Allow subclasses to provide a custom context
	 * @param ctx context
	 */
	protected TxtReader(TxtReaderContext ctx) {
		super(ctx);
		this.txtCtx = ctx;
	}

	/**
	 * Create a TxtReader for the provided input stream.  Uses the default config and charset.
	 * @param fromStream Stream to read
	 * @return new instance
	 */
	public static TxtReader create(InputStream fromStream) {
		return new TxtReader(TxtReaderContext.create(fromStream));
	}

	/**
	 *
	 * Create a TxtReader for the provided input stream and charset.  Uses the default config.
	 * @param in Stream to read
	 * @param charset charset of the stream
	 * @return new instance
	 */
	public static TxtReader create(InputStream in, Charset charset) {
		return new TxtReader(TxtReaderContext.create(in, charset));
	}

	/**
	 * Create a TxtReader
	 * @param in Stream to read
	 * @param config Config to use.  Note that the ElementReaderFactory implementation must be TxtElementReaderFactory (or an extension)
	 * @param charset charset of the stream
	 * @return new instance
	 */
	public static TxtReader create(InputStream in, EnonConfig config, Charset charset) {
		return new TxtReader(TxtReaderContext.create(in, config, charset));
	}

	@Override
	protected TxtReaderContext getReaderContext() {
		return txtCtx;
	}

	/**
	 * Read the prolog from the input stream and verify that the read was OK
	 */
	@Override
	protected void validateProlog() {
		Prolog prolog = ctx.readProlog();
		if (prolog == null) {
			throw new EnonReadException("Invalid eNON-txt prolog");
		}
	}

}
