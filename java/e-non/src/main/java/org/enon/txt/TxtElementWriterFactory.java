/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.IOException;
import java.io.OutputStream;
import org.enon.EnonConfig;
import org.enon.Prolog;
import org.enon.element.Element;
import org.enon.element.writer.DefaultElementWriterFactory;
import org.enon.element.writer.ElementWriter;

/**
 *
 * @author clininger $Id: $
 */
public class TxtElementWriterFactory extends DefaultElementWriterFactory {

	private final TxtElementWriter txtElementWriter = new TxtElementWriter();
	private final Prolog.Writer txtPrologWriter = new Prolog.Writer() {
		@Override
		public void write(Prolog prolog, OutputStream out) throws IOException {
			out.write("#enon-txt\n".getBytes());
		}
	};
	
	public TxtElementWriterFactory(EnonConfig config) {
		super(config);
	}

	@Override
	public <E extends Element> ElementWriter<E> writer(E e) {
		if (e == null || e.getType() == null) {
			return null;
		}
		switch(e.getType()) {
				case GLOSSARY_REF_ELEMENT:
				case MAP_REF_ELEMENT:
					return null;
				default:
					return (ElementWriter<E>) txtElementWriter;
		}
	}

	@Override
	public <W extends Prolog.Writer> W prologWriter() {
		return (W) txtPrologWriter;
	}

}
