/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Prolog;
import org.enon.element.reader.ElementReaderFactory;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public class TxtElementReaderFactory implements ElementReaderFactory<TxtElementReader>{

	private final TxtElementReader reader = new TxtElementReader();
	private final TxtPrologReader prologReader = new TxtPrologReader();

	public TxtElementReaderFactory(EnonConfig config) {
		// config not used
	}

	@Override
	public TxtElementReader reader(InputStream in) {
		return reader;
	}

	@Override
	public <R extends Prolog.Reader> R prologReader() {
		return (R) prologReader;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	public static class TxtPrologReader extends Prolog.Reader<TxtReaderContext> {
		@Override
		public Prolog read(TxtReaderContext ctx) throws IOException {
			TxtElement txtElement = ctx.getTxtElementStream().read();
			if (txtElement != null && TxtElementType.PROLOG == txtElement.getType()) {
				return new Prolog(EnonConfig.FORMAT_VERSION, EnumSet.noneOf(FeatureSet.class));
			}
			throw new EnonReadException("Unable to read prolog");
		}

	}
}
