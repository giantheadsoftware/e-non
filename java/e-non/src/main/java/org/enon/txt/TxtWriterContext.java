/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.WriterContext;

/**
 * This context exposes a PrintWriter created on top of the OutputStream
 * @author clininger $Id: $
 */
public class TxtWriterContext extends WriterContext {
	private static final EnonConfig DEFAULT_TXT_CONFIG = defaultConfig();
	private final PrintWriter printWriter;
	private Charset charset = StandardCharsets.UTF_8;
	private int tabWidth = 3;
	private Integer blockWidth = 76;
	private int indentLevel = 0;
	private boolean pad = true;

	public TxtWriterContext(final OutputStream out) {
		super(out, DEFAULT_TXT_CONFIG);
		this.printWriter = new PrintWriter(new OutputStreamWriter(out, charset));
	}

	public TxtWriterContext(final OutputStream out, Charset charset) {
		super(out, DEFAULT_TXT_CONFIG);
		if (charset != null )
			this.charset = charset;
		this.printWriter = new PrintWriter(new OutputStreamWriter(out, charset));
	}

	public TxtWriterContext(final OutputStream out, final EnonConfig config) {
		super(out, config);
		this.printWriter = new PrintWriter(new OutputStreamWriter(out, charset));
	}

	public TxtWriterContext(final OutputStream out, final EnonConfig config, Charset charset) {
		super(out, config);
		if (charset != null)
			this.charset = charset;
		this.printWriter = new PrintWriter(new OutputStreamWriter(out, charset));
	}

	/**
	 * When a config is not provided, create a default one.  The default enables FeatureSet.X
	 * and uses the TxtElementWriterFactory.
	 * @return a default config
	 */
	private static EnonConfig defaultConfig() {
		return new EnonConfig.Builder()
				.feature(FeatureSet.X)
				.elementWriterFactoryImpl(TxtElementWriterFactory.class)
				.build();
	}

	public PrintWriter getPrintWriter() {
		return printWriter;
	}

	public Charset getCharset() {
		return charset;
	}

	public TxtWriterContext minify() {
		tabWidth = 0;
		blockWidth = null;
		pad = false;
		return this;
	}

	public TxtWriterContext tabWidth(final int width) {
		this.tabWidth = Math.max(0, width);
		return this;
	}

	public TxtWriterContext blockWidth(final Integer width) {
		if (width == null) {
			this.blockWidth = null;
		} else {
			this.blockWidth = Math.max(40, width);
		}
		return this;
	}

	public TxtWriterContext padded(final boolean pad) {
		this.pad = pad;
		return this;
	}

	public int getTabWidth() {
		return tabWidth;
	}

	public Integer getBlockWidth() {
		return blockWidth;
	}

	public int getIndentLevel() {
		return indentLevel;
	}

	public boolean isPadded() {
		return pad;
	}

	public void containerBegin() {
		indentLevel++;
	}

	public void containerEnd() {
		indentLevel--;
	}

	public void flush() {
		printWriter.flush();
	}

}
