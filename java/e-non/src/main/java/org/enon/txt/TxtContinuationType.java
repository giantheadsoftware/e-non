/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.util.HashMap;
import java.util.Map;
import org.enon.exception.EnonException;

/**
 * Symbols and patterns for continuation lines
 * @author clininger $Id: $
 */
public enum TxtContinuationType {
	BREAK('&'),
	SOFT_BREAK('\\');

	private static final Map<Character, TxtContinuationType> BY_PREFIX = new HashMap<>();

	static {
		// populate the lookup maps
		for (TxtContinuationType tcType : values()) {
			if (null != BY_PREFIX.put(tcType.prefix, tcType)) {
				throw new EnonException("Coding error: Duplicate TxtElement prefix: "+tcType.prefix);
			}
		}
	}

	public final char prefix;

	private TxtContinuationType(char prefix) {
		this.prefix = prefix;
	}

	/**
	 * Return the TxtContinuationType that is an exact match for the prefix.  If non-exact matching is required,
	 * use forLine().
	 * @param prefix Prefix that might match one of the TxtElementType prefixes
	 * @return Matching TxtElement type, or null if no match.
	 */
	public static TxtContinuationType forPrefix(final Character prefix) {
		return BY_PREFIX.get(prefix);
	}

	/**
	 * Return the TxtContinuationType by examining the start of the line.
	 * This method does not trim leading white space - do that before calling
	 * @param line String that may or may not begin with a TxtContinuationType prefix.
	 * @return The matching TxtContinuationType or null if there is no match.
	 */
	public static TxtContinuationType forLine(String line) {
		if (line == null || line.length() == 0) {
			return null;
		}
		// since all continuation prefixes have a single char, just check the leading char of the line
		return forPrefix(line.charAt(0));
	}
}
