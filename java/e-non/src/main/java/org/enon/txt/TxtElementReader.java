/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Base64;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.enon.codec.Value;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.array.ByteArrayElement;
import org.enon.element.ByteElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.Element;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.ListElement;
import org.enon.element.LongElement;
import org.enon.element.MapElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.ShortElement;
import org.enon.element.StringElement;
import org.enon.element.array.ArrayElement;
import org.enon.element.reader.ElementReader;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public class TxtElementReader implements ElementReader<TxtReaderContext> {

	/** pattern for locating unicode escape sequences within string data */
	private static final Pattern UNICODE_ESCAPE_PATTERN = Pattern.compile("\\\\[u]([0-9a-fA-F]+);");
	/** pattern for locating space-delimited array entries */
	private static final Pattern ARRAY_ENTRY_PATTERN = Pattern.compile("\\S+");

	@Override
	public Element read(TxtReaderContext ctx) throws IOException {
		TxtElementInputStream reader = ctx.getTxtElementStream();
		TxtElement txtElement = reader.read();
		if (txtElement == null) {
			return null;
		}
		return parseTxtElement(txtElement, reader);
	}

	/**
	 * Convert the TxtElement into an Element. Call recursively to process List and Map contents.
	 *
	 * @param txtElement The TxtElement to convert
	 * @param reader Reads List & Map contents
	 * @return An Element representing the TxtElement
	 * @throws IOException on read error from the input stream
	 */
	private Element parseTxtElement(TxtElement txtElement, TxtElementInputStream reader) throws IOException {
		switch (txtElement.getType()) {
			// handle constant values
			case NULL:
				return ConstantElement.nullInstance();
			case FALSE:
				return ConstantElement.falseInstance();
			case TRUE:
				return ConstantElement.trueInstance();
			case INFINITY_NEG:
				return ConstantElement.negativeInfinityInstance();
			case INFINITY_POS:
				return ConstantElement.positiveInfinityInstance();
			case NAN:
				return ConstantElement.nanInstance();

			// Byte and nano-int
			case BYTE:
				byte byteValue = Byte.parseByte(firstToken(txtElement.getStrValue()));
				if (NanoIntElement.isNanoInt(byteValue)) {
					return new NanoIntElement(byteValue);
				}
				return new ByteElement(byteValue);

			// numeric values
			case SHORT:
				return new ShortElement(Short.parseShort(firstToken(txtElement.getStrValue())));
			case INT:
				return new IntElement(Integer.parseInt(firstToken(txtElement.getStrValue())));
			case LONG:
				return new LongElement(Long.parseLong(firstToken(txtElement.getStrValue())));
			case FLOAT:
				return new FloatElement(Float.parseFloat(firstToken(txtElement.getStrValue())));
			case DOUBLE:
				return new DoubleElement(Double.parseDouble(firstToken(txtElement.getStrValue())));
			case NUMBER:
				return new NumberElement(firstToken(txtElement.getStrValue()));

			// String
			case STRING:
				return new StringElement(getFullText(txtElement));

			case STRING_UNICODE:
				return new StringElement(replaceUnicode(getFullText(txtElement)));

			// byte[]
			case BLOB:
				return new ByteArrayElement(decodeBlob(txtElement));

			case LIST:
				// look for the closing bracket on the same line, which indicates an empty list
				if (TxtElementType.LIST_END.prefix.equals(firstToken(txtElement.getStrValue()))) {
					return new ListElement(Collections.EMPTY_LIST);
				}
				// read the following lines as list items
				List<Element> listEntries = new LinkedList<>();
				TxtElement listEntry = reader.read();
				while (listEntry != null && TxtElementType.LIST_END != listEntry.getType()) {
					listEntries.add(parseTxtElement(listEntry, reader));
					listEntry = reader.read();
				}
				if (listEntry == null) {
					throw new EnonReadException("End of data found while reading list contents");
				}
				return new ListElement(listEntries);

			case ARRAY:
				return createArray(txtElement);

			case MAP:
				// look for the closing brace on the same line which indicates an empty map
				if (TxtElementType.MAP_END.prefix.equals(firstToken(txtElement.getStrValue()))) {
					return new MapElement(Collections.EMPTY_MAP);
				}
				// read the following lines as map entries
				Map<Element, Element> mapEntries = new LinkedHashMap<>();
				TxtElement txtKey = reader.read();
				while (txtKey != null && TxtElementType.MAP_END != txtKey.getType()) {
					Element keyElement = parseTxtElement(txtKey, reader);
					TxtElement txtValue = reader.read();
					if (txtValue != null) {
						Element valueElement = parseTxtElement(txtValue, reader);
						mapEntries.put(keyElement, valueElement);
					} else {
						throw new EnonReadException("End of data found while reading map value");
					}
					txtKey = reader.read();
				}
				if (txtKey == null) {
					throw new EnonReadException("End of data found while reading map key");
				}
				return new MapElement(mapEntries);

			case LIST_END:
			case MAP_END:
				// these will be handled within the LIST, ARRAY, & MAP cases
				throw new EnonReadException("Unexpected " + txtElement.getType());

			default:
				throw new EnonReadException("Unsupported txt element type: " + txtElement.getType());
		}
	}

	/**
	 * Return the first non-whitespace token on the line. Skip any leading white space then stop at the first whitespace after the start of text.
	 *
	 * @param content The entire content line
	 * @return the first non-whitespace token on the line
	 */
	private String firstToken(String content) {
		int[] bounds = tokenBounds(content, 0);
		int begin = bounds[0];
		int end = bounds[1];
		if (begin == end) {
			return "";
		}
		if (end == content.length()) {
			return begin == 0 ? content : content.substring(begin);
		}
		return content.substring(begin, end);
	}

	/**
	 * Fetch the next token starting from a given index in the string. A token is a group of non-whitespace chars surrounded by whitespace.
	 *
	 * @param content The content to search
	 * @param startAt The index where the search should begin
	 * @return the start and end indices of the token.<br>
	 * If the line is blank starting at startAt, then begin == end == content.length
	 *
	 */
	private int[] tokenBounds(String content, int startAt) {
		int length = content.length();
		int begin = startAt;
		while (begin < length && Character.isWhitespace(content.charAt(begin))) {
			begin++;
		}
		if (begin == length) {
			return new int[]{length, length};
		}
		int end = begin + 1;
		while (end < length && !Character.isWhitespace(content.charAt(end))) {
			end++;
		}
		return new int[]{begin, end};
	}

//	/**
//	 * Return 1 or 2 chars representing the first char on the line. The result will be 1 char in length unless the char is a supplemental Unicode char requiring
//	 * 2. If there are no chars in the txt element, then a '\n' newline is returned.
//	 *
//	 * @param txtElement The txt element containing the char value.
//	 * @return
//	 */
//	private char[] getCharValue(TxtElement txtElement) {
//		String value = txtElement.getStrValue().trim();
//		if (value.isEmpty()) {
//			throw new EnonReadException("No char value provided");
//		}
//		if (value.length() == 1) {
//			// a single char was provided, just return that.
//			return Character.toChars(value.codePointAt(0));
//		}
//		// a multi-char string was provided.  This is OK if it's an escape sequence starting with '\'
//		if ('\\' == value.charAt(0)) {
//			String hexValue;
//			switch (value.charAt(1)) {
//				case 'n':
//					return new char[]{'\n'};
//				case 's':
//					return new char[]{' '};
//				case 't':
//					return new char[]{'\t'};
//				case 'r':
//					return new char[]{'\r'};
//				case 'u':
//					hexValue = value.substring(2);  // expect hex digits following the u
//					break;
//				default:
//					hexValue = value.substring(1);  // expect hex digits following the \
//					break;
//			}
//			// attempt to compute the codepoint for the hexValue
//			try {
//				return Character.toChars(Integer.parseInt(hexValue, 16));
//			} catch (NumberFormatException x) {
//				throw new EnonReadException("Invalid char escape sequence: '"+value+"'");
//			} catch (IllegalArgumentException x) {
//				throw new EnonReadException("Invalid unicode codepoint: '"+value+"'");
//			}
//		} else {
//			throw new EnonReadException("Can't parse value as char: '"+value+"'");
//		}
//	}
//
	/**
	 * Append all continuation lines to the end of the first line.  The data starts with the character
	 * Hard breaks cause a newline to be inserted, soft breaks do not.
	 *
	 * @param txtElement Element containing String text, optionally with continuation lines.
	 * @return The full text of the element
	 */
	private String getFullText(TxtElement txtElement) {
		StringBuilder sb = new StringBuilder(txtElement.getStrValue());
		txtElement.getContinuationElements().stream().forEach(cont -> {
			switch (cont.type) {
				case BREAK:
					sb.append('\n').append(cont.value);
					break;
				case SOFT_BREAK:
					sb.append(cont.value);
					break;
			}
		});
		return sb.toString();
	}

	/**
	 * Search and replace Unicode escape sequences in the text. Uses regex pattern replacement.
	 *
	 * @param unicodeString String potentially containing Unicode escape sequences
	 * @return String with Unicode escape sequences replaced by the matching Unicode chars.
	 */
	private String replaceUnicode(String unicodeString) {
		Matcher m = UNICODE_ESCAPE_PATTERN.matcher(unicodeString);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, new String(unicode(m.group(1))));
		}
		m.appendTail(sb);
		return sb.toString();
	}

	/**
	 * Try to process the string as a Unicode escape sequence "\\uXXXX" where XXXX is the codepoint in hex.
	 *
	 * @param unicode String that should represent a Unicode codepoint in hex
	 * @return a Unicode char or pair of chars.
	 * @throws EnonReadException if the string is not a valid Unicode value
	 */
	private char[] unicode(String unicode) {
		if (unicode.length() > 8) {
			throw new EnonReadException("Unicode value out of range: "+unicode);
		}
		Integer codepoint = Integer.parseInt(unicode, 16);
		if (!Character.isValidCodePoint(codepoint)) {
			throw new EnonReadException("Invalid unicode character specified: " + unicode);
		}
		return Character.toChars(codepoint);
	}

	/**
	 * Decode a possibly multi-line Base64 BLOB back to the original bytes.
	 *
	 * @param txtElement Contains BLOB data, optionally with continuation lines, or a file path
	 * @return
	 */
	private byte[] decodeBlob(TxtElement txtElement) {
		String blobStrValue = txtElement.getStrValue().trim();
		if (blobStrValue.startsWith(TxtElementType.STRING.prefix)) {
			// file path was specified.  Try to read it.
			// @TODO: set CWD for relative file paths?
			File blobFile = new File(blobStrValue.substring(1));
			try {
				FileInputStream fis = new FileInputStream(blobFile);
				byte[] bytes = new byte[fis.available()];
				int readCount = fis.read(bytes);
				if (readCount == bytes.length) {
					return bytes;
				}
				throw new EnonReadException("Unable to read all bytes from binary file");
			} catch (FileNotFoundException ex) {
				throw new EnonReadException("Could not locate BLOB file specified in eNON-txt", ex);
			} catch (IOException ex) {
				throw new EnonReadException("Could not read BLOB bytes from file "+blobFile.getPath(), ex);
			}
		} else {
			// base64 data should follow
			StringBuilder sb = new StringBuilder(blobStrValue);
			txtElement.getContinuationElements().stream().forEach(cont -> {
				sb.append(cont.value.trim());
			});
			return Base64.getDecoder().decode(sb.toString());
		}
	}

	private ArrayElement createArray(TxtElement txtElement) {
		// collect all of the array entries, remove newlines
		String allEntries = getFullText(txtElement);

		// separate the entries by white space
		List<String> values = new LinkedList<>();
		Matcher m = ARRAY_ENTRY_PATTERN.matcher(allEntries);
		while (m.find()) {
			values.add(m.group());
		}

		// create an array element with the correct size
		ArrayElement result = ArrayElement.createEmpty((ArrayElementSubtype) txtElement.getSubType(), values.size(), null);

		// populate the array
		Object resultArray = result.getValue();  // this is the array to populate
		// create an array populating function based on the array type
		BiConsumer<Integer, String> arraySetFunc;
		switch ((ArrayElementSubtype)txtElement.getSubType()) {
			case BYTE_SUB_ELEMENT:
				arraySetFunc = (i, value) -> Array.setByte(resultArray, i, Byte.parseByte(value));
				break;
			case DOUBLE_SUB_ELEMENT:
				arraySetFunc = (i, value) -> Array.setDouble(resultArray, i, Double.parseDouble(value));
				break;
			case FALSE_SUB_ELEMENT:
				arraySetFunc = (i, value) -> Array.setBoolean(resultArray, i, Value.trueOrFalseNotNull(value));
				break;
			case FLOAT_SUB_ELEMENT:
				arraySetFunc = (i, value) -> Array.setFloat(resultArray, i, Float.parseFloat(value));
				break;
			case INT_SUB_ELEMENT:
				arraySetFunc = (i, value) -> Array.setInt(resultArray, i, Integer.parseInt(value));
				break;
			case LONG_SUB_ELEMENT:
				arraySetFunc = (i, value) -> Array.setLong(resultArray, i, Long.parseLong(value));
				break;
			case SHORT_SUB_ELEMENT:
				arraySetFunc = (i, value) -> Array.setShort(resultArray, i, Short.parseShort(value));
				break;
			default:
				throw new EnonReadException("Unexpected array subtype:"+txtElement.getSubType());
		}
		// set the values
		int i = 0;
		for (String value : values) {
			arraySetFunc.accept(i++, value);
		}
		return result;
	}
}
