/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.enon.element.ElementSubtype;
import org.enon.exception.EnonReadException;

/**
 * A "stream" that returns the next TxtElement from the raw input. Raw lines are parsed from the input stream.
 * Blank lines are skipped.  Continuation lines are attached to the continued element.
 * @author clininger $Id: $
 */
public class TxtElementInputStream {
	/** reader for the raw input stream */
	private final BufferedReader reader;
	
	/** temporarily hold the next input line */
	private String nextLine;

	/**
	 * Constructor
	 * @param in text input stream
	 * @param charset charset of the stream
	 */
	public TxtElementInputStream(InputStream in, Charset charset) {
		reader = new BufferedReader(new InputStreamReader(in, charset));
	}
	
	/**
	 * Read the next TxtElement from the stream.
	 * @return The next text element, or null if no more elements
	 * @throws IOException for stream reading errors
	 * @throws EnonReadException if invalid eNON-txt is detected
	 */
	public TxtElement read() throws IOException {
		String line = "";
		// loop until we get a non-empty line or reach the end of input.
		while (line.isEmpty()) {
 			line = trimLine(getNextLine());
			if (line == null) {
				return null;  // end of input reached
			}
		}
		// determine the type from the first char(s) on the line
		TxtElementType type = TxtElementType.forLine(line);
		if (type != null) {
			// legitimate type found, create a TxtElement instance
			TxtElement result = null;
			
			if (type.elementType != null && type.elementType.hasSubTypes()) {
				// this type requires a subtype.  Check the next char after the prefix.
				if (line.length() <= type.prefix.length()) {
					// the line ended without specifying a subtype
					throw new EnonReadException("Element type "+type+" requires a subtype.");
				}
				// the subtype must be a single byte.  Verify that the codepoint can be a byte value
				int codepoint = line.codePointAt(type.prefix.length());
				if (codepoint < 256) {
					ElementSubtype subtype = type.elementType.subtypeForPrefix((byte)(codepoint & 0xFF));
					if (subtype != null && type.elementType.isValidSubType(subtype)) {
						result = new TxtElement(type, subtype, line.substring(type.prefix.length() + 1));
					}
				}
				if (result == null) {
					throw new EnonReadException("Invalid sub type for element "+type+": '"+line.charAt(type.prefix.length())+"'");
				}
			} else {
				// create a type without a subtype
				result = new TxtElement(type, line.substring(type.prefix.length()));
			}
			
			// look for continuation lines, if the type supports them
			if (type.mayContinue()) {
				// the type may have continuation lines.  Fetch all of them and add them to the TxtElement
				line = trimLine(getNextLine());
				TxtContinuationType cont = TxtContinuationType.forLine(line);
				while (cont != null) {
					result.addContinuation(cont, line.substring(1));
					line = trimLine(getNextLine());
					cont = TxtContinuationType.forLine(line);					
				}
				nextLine = line;
			}
			return result;
		}
		throw new EnonReadException("Can't parse element: "+line);
	}

	/**
	 * Read the next line from the stream.
	 * @return next line from the stream, or null if the end of data has been reached
	 * @throws IOException 
	 */
	private String getNextLine() throws IOException {
		// the next line may have already been read while scanning for continuation lines.
		if (nextLine != null) {
			String result = nextLine;
			nextLine = null;
			return result;
		}
		return reader.readLine();
	}

	/**
	 * Remove leading white space from the line
	 * @param line the line to trim
	 * @return the line without any leading white space.  Returns null only if the input line is null.
	 */
	protected String trimLine(String line) {
		if (line == null) {
			return null;
		}
		if (line.isEmpty()) {
			return line;
		}
		// locate the first non-white-space char
		int begin = 0;
		while (begin < line.length() && Character.isWhitespace(line.charAt(begin))) {
			begin++;
		}
		if (begin == line.length()) {
			return "";
		}
		return begin == 0 ? line : line.substring(begin);
	}
		
}
