/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.OutputStream;
import java.nio.charset.Charset;
import org.enon.Writer;

/**
 * Writes to eNON-txt
 * @author clininger $Id: $
 */
public class TxtWriter extends Writer {

	private final TxtWriterContext txtCtx;

	/**
	 * Subclasses can provide a custom config to control the writing process.
	 * @param ctx custom writer context
	 */
	protected TxtWriter(TxtWriterContext ctx) {
		super(ctx);
		this.txtCtx = ctx;
	}

	/**
	 * Create a config that uses the TxtWriterContext and TxtElementWriterFactory
	 * @param toStream Stream where the eNON-txt will be written
	 * @return custom config
	 */
	public static TxtWriter create(OutputStream toStream) {
		TxtWriterContext txtCtx = new TxtWriterContext(toStream);
		return new TxtWriter(txtCtx);
	}

	/**
	 * Create a config that uses the TxtWriterContext and TxtElementWriterFactory
	 * @param toStream Stream to receive the eNON-txt data
	 * @param charset charset to use for the stream
	 * @return custom config
	 */
	public static TxtWriter create(OutputStream toStream, Charset charset) {
		TxtWriterContext txtCtx = new TxtWriterContext(toStream, charset);
		return new TxtWriter(txtCtx);
	}

	@Override
	protected TxtWriter writeObject(Object object) {
		super.writeObject(object);
		txtCtx.getPrintWriter().flush();
		return this;
	}

	@Override
	public TxtWriter write(Object object) {
		super.write(object);
		txtCtx.getPrintWriter().flush();
		return this;
	}

	public TxtWriter minify() {
		txtCtx.minify();
		return this;
	}

	public TxtWriter tabWidth(int width) {
		txtCtx.tabWidth(width);
		return this;
	}

	public TxtWriter blockWidth(Integer width) {
		txtCtx.blockWidth(width);
		return this;
	}

	public TxtWriter padded(boolean pad) {
		txtCtx.padded(pad);
		return this;
	}

	public int getTabWidth() {
		return txtCtx.getTabWidth();
	}

	public Integer getBlockWidth() {
		return txtCtx.getBlockWidth();
	}

	public boolean isPadded() {
		return txtCtx.isPadded();
	}

	@Override
	public void flush() {
		txtCtx.flush();
	}

}
