/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.util;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import org.enon.exception.EnonException;

/** *
 * There is minimal checking for special cases because null, infinity, NaN, etc. are normally handled by special elements.
 *
 * @author clininger $Id: $
 */
public enum RealType {
	FLOAT_OBJECT(Float.class,
			n -> n.floatValue(), // "cast" number to float
			str -> Float.valueOf(str), // value from string
			() -> Float.NaN
	),
	FLOAT(float.class,
			n -> n.floatValue(), // "cast" number to float
			str -> Float.valueOf(str),
			() -> Float.NaN
	),
	DOUBLE_OBJECT(Double.class,
			n -> n.doubleValue(),  // "cast" number to double
			str -> Double.valueOf(str), // value from string
			() -> Double.NaN
	),
	DOUBLE(double.class,
			n -> n.doubleValue(),   // "cast" number to double
			str -> Double.valueOf(str), // value from string
			() -> Double.NaN
	),
	BIG_DECIMAL(BigDecimal.class,
			n -> n instanceof BigDecimal ? (BigDecimal)n : new BigDecimal(n.doubleValue()),  // "cast" number to BigDecimal
			str -> new BigDecimal(str), // value from string
			() -> {throw new EnonException("Invalid BigDecimal number");}
	);

	private static final Map<Class<? extends Number>, RealType> BY_TYPE = new HashMap<>();
	private static final Set<Number> INFINITE = new HashSet<>(Arrays.asList(new Number[]{
		Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY
	}));

	static {
		for (RealType intType : values()) {
			BY_TYPE.put(intType.type, intType);
		}
	}

	public final Class<? extends Number> type;
	private final Function<Number, Number> castFn;
	private final Function<String, Number> fromStringFn;
	private final Supplier<Number> nanFn;

	private RealType(Class<? extends Number> type, Function<Number, Number> castFn, Function<String, Number> fromStringFn, Supplier<Number> nanFn) {
		this.type = type;
		this.castFn = castFn;
		this.fromStringFn = fromStringFn;
		this.nanFn = nanFn;
	}

	public static RealType valueOfType(Class<? extends Number> type) {
		return BY_TYPE.get(type);
	}

	/**
	 * Test number for being infinite. Returns true if Double or Float is positive or negative infinity.
	 * Always returns false for BigDecimal.
	 * @param value Value to test
	 * @return true if value is infinite
	 */
	public static boolean isInfinite(Number value) {
		return INFINITE.contains(value);
	}

	public boolean isPrimitive() {
		return type.isPrimitive();
	}

	public Number fromString(String value) throws NumberFormatException {
		try {
			Number result = fromStringFn.apply(value);
			if (isInfinite(result)) {
				throw new IllegalArgumentException("String value out of range for "+type.getName());
			}
			return result;
		} catch (NumberFormatException x) {
			return nanFn.get();
		}
	}

	/**
	 * Never compact to smaller type.  Always return the original instance.
	 *
	 * @param value The value to compact.
	 * @return The same value as was passed in
	 */
	public static Number compact(Number value) {
		return value;
	}

	/**
	 * Attempt to return the number value as the type of this RealType.
	 * @param value the value to cast
	 * @return the value as this type
	 * @throws IllegalArgumentException if the value is out of range for this type
	 */
	public Number cast(Number value) {
		final Class valueClass = value.getClass();
		if (valueClass == type) {
			return value;
		}

		Number result = castFn.apply(value);
		if (isInfinite(result) && !isInfinite(value)) {
			// value went from finite to infinite: range error
			throw new IllegalArgumentException("Value out of range for "+type.getName());
		}
		return result;
	}
}
