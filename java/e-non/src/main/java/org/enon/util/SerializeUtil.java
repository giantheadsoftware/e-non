/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.util;

import org.enon.exception.EnonException;

/**
 * Util functions to convert values to network byte order.
 * @author clininger $Id: $
 */
public class SerializeUtil {
	private volatile static SerializeUtil instance;

	private SerializeUtil() {
	}

	public static SerializeUtil getInstance() {
		SerializeUtil instanceLocal;
		if ((instanceLocal = instance) == null) {
			synchronized (SerializeUtil.class) {
				if ((instanceLocal = instance) == null) {
					instance = instanceLocal = new SerializeUtil();
				}
			}
		}
		return instanceLocal;
	}

	/**
	 * @param value the value to transform
	 * @return the bytes in IEEE 754 format, big endian
	 */
	public byte[] networkBytes(byte value) {
		return new byte[] { value };
	}

	/**
	 * @param value the value to transform
	 * @return the bytes in IEEE 754 format, big endian
	 */
	public byte[] networkBytes(short value) {
		return new byte[] {
			(byte)((value & 0xFF00) >> 8),
			(byte)((value & 0x00FF))
		};
	}

	/**
	 * @param value the value to transform
	 * @return the bytes in IEEE 754 format, big endian
	 */
	public byte[] networkBytes(int value) {
		return new byte[] {
			(byte)((value & 0xFF000000) >> 24),
			(byte)((value & 0xFF0000) >> 16),
			(byte)((value & 0xFF00) >> 8),
			(byte)(value & 0xFF)
		};
	}

	/**
	 * @param value the value to transform
	 * @return the bytes in IEEE 754 format, big endian
	 */
	public byte[] networkBytes(long value) {
		return new byte[] {
			(byte)((value & 0xFF00000000000000L) >> 56),
			(byte)((value & 0xFF000000000000L) >> 48),
			(byte)((value & 0xFF0000000000L) >> 40),
			(byte)((value & 0xFF00000000L) >> 32),
			(byte)((value & 0xFF000000) >> 24),
			(byte)((value & 0xFF0000) >> 16),
			(byte)((value & 0xFF00) >> 8),
			(byte)(value & 0xFF)
		};
	}

	/**
	 * @param value value to transform
	 * @return the bytes in IEEE 754 format, big endian
	 */
	public byte[] networkBytes(float value) {
		return networkBytes(Float.floatToIntBits(value));
	}

	/**
	 * @param value value to transform
	 * @return the bytes in IEEE 754 format, big endian
	 */
	public byte[] networkBytes(double value) {
		return networkBytes(Double.doubleToLongBits(value));
	}

	/**
	 * Get the bytes for Byte, Short, Integer, Long, Float, and Double
	 * @param value Numeric value
	 * @return network bytes for the numeric value
	 */
	public byte[] networkBytes(Number value) {
		Class numberType = value.getClass();
		if (Byte.class == numberType) {
			return (networkBytes(value.byteValue()));
		}
		if (Short.class == numberType) {
			return (networkBytes(value.shortValue()));
		}
		if (Integer.class == numberType) {
			return (networkBytes(value.intValue()));
		}
		if (Long.class == numberType) {
			return (networkBytes(value.longValue()));
		}
		if (Float.class == numberType) {
			return (networkBytes(value.floatValue()));
		}
		if (Double.class == numberType) {
			return (networkBytes(value.doubleValue()));
		}
		throw new EnonException("can't get network bytes for type "+numberType.getName());
	}
}
