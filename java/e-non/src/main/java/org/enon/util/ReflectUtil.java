/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.HashMap;
import java.util.Map;
import org.enon.exception.EnonException;

/**
 * Reflection operations
 * @author clininger $Id: $
 */
public class ReflectUtil {
	private static final ReflectUtil INSTANCE = new ReflectUtil();

	public static ReflectUtil getinstance() {
		return INSTANCE;
	}
	
	private static final Map<String, Class> PRIMITIVE_CLASSNAME_MAP = new HashMap<>();
	static {
		for (Class c : new Class[]{
			boolean.class, byte.class, char.class, short.class, int.class, long.class, float.class, double.class,
			boolean[].class, byte[].class, char[].class, short[].class, int[].class, long[].class, float[].class, double[].class
		}) {
			PRIMITIVE_CLASSNAME_MAP.put(c.getName(), c);
		}
	}

	/**
	 * Determine the actual type params of a specific super type.  For example, if the fromType is ArrayList&lt;String&gt; and the findSuper is Collection.class,
	 * then the result will be [String.class].  If the findSuper is Map.class and the fromType is HashMap&lt;String, List&lt;Long&gt;&gt; then the result will be
	 * [String.class, List&lt;Long&gt;]<p>
	 *
	 * If findSuper is not a parameterized type, an empty array is returned.<p>
	 *
	 * If a parameter is not specified or resolves to Object, then null is returned for that param.<p>
	 *
	 * If a parameter is declared using a type variable or a wildcard, then the (upper) bound of that variable is used.  For example, both of these List declarations would produce
	 * [Number.class] if called with a findSuper of Collection.class:
	 * <pre>   public &lt;N extends Number&gt; void setList(List&lt;N&gt; list);
	 *   public void setList(List&lt;? extends Number&gt; list);</pre><p>
	 *
	 * Declarations with multiple bounds are not supported by default:
	 * <pre>   public &lt;NQ extends Number &amp; Queue&gt; void setList(List&lt;NQ&gt; list);</pre>
	 * <i>NOTE: This type could be handled by using a custom decoder.</i><p>
	 *
	 * @param findSuper The super class whose actual arguments are determined.
	 * @param fromType The type of the actual declaration (i.e. the return type of param type os a Method).
	 * @return An array of Type objects that are applied to the given super class base on the declaration of the fromType.
	 */
	public Type[] findArgsOfSuper(final Class findSuper, final Type fromType) {
		Type[] superTypeParams = findSuper.getTypeParameters();
		if (superTypeParams.length == 0) {
			return new Class[0];  // if the target class has no params, returm empty array
		}

		// extract the actuals from the fromType
		TypeVarActual[] fromActuals = TypeVarActual.ofType(fromType);
		// push the actuals up to the super class
		TypeVarActual[] superActuals = typeVarActuals(findSuper, fromType, fromActuals);

		// extract the final arg types
		return TypeVarActual.getArgTypes(superActuals);
	}

	/**
	 * Recursively walk up from the fromType to the targetSuper.  At each step, resolve the actual generic params until the params of the targetSuper class are known.
	 * @param targetSuper The super class for which the generic type args are to be determined.
	 * @param fromType The subtype that may or may not declare actual type args
	 * @param actuals The actual type parameters and/or type variables of fromType.
	 * @return An array of TypeVarActual values containing the actual arguments at the current level in the hierarchy.
	 */
	private TypeVarActual[] typeVarActuals(Class targetSuper, Type fromType, TypeVarActual[] actuals) {
		Class fromClass = classOfType(fromType);
		if (fromClass != targetSuper) {
			Type nextSuper = superTowardType(targetSuper, fromType);

   			 return typeVarActuals(targetSuper, nextSuper, transferTypeArgsToSuper(nextSuper, actuals));
		}
		return actuals;
	}

	/**
	 * Return the class associated with Type t.  This is either 
	 * <ul>
	 * <li>t itself if t is a Class</li>
	 * <li>t.getRawType() is t is a Parameterized type</li>
	 * <li>The upper bound if t is a type variable or wildcard with a single bound</li>
	 * </ul>
	 * @param t Type from which to extract a class
	 * @return the class associated with the type.
	 * @throws EnonException if t is neither Class nor ParameterizedType.
	 */
	public Class classOfType(Type t) {
		Type result;
		if (t instanceof Class) {
			return (Class) t;
		} else if (t instanceof ParameterizedType) {
			result = ((ParameterizedType)t).getRawType();
		} else if (t instanceof TypeVariable || t instanceof WildcardType) {
			result = singleUpperBound(t);
		} else {
			throw new EnonException("Cannot extract class from Type "+(t != null ? t.getTypeName() : "null"));
		}
		return result instanceof Class ? (Class)result : classOfType(result);
	}
	
	/**
	 * If Type t is a TypeVariable or WildcardType with a single bound, return the upper bound of that bound.
	 * If t is not TypeVariable of WildcardType, just returns t.
	 * @param t Some type.
	 * @return Single upper bound of t, or t itself.
	 */
	public Type singleUpperBound(Type t) {
		if (t instanceof TypeVariable) {
			// if TypeVariable, return the bound, if there is only 1
			Type[] bounds = ((TypeVariable) t).getBounds();
			if (bounds.length == 1) {
				return bounds[0] == Object.class ? null : bounds[0];
			} else {
				throw new EnonException("Can't handle type var with more than one upper bound: " + t.getTypeName());
			}
		} else if (t instanceof WildcardType) {
			// if WildcardType, return the upper bound, if there is only 1
			Type[] bounds = ((WildcardType) t).getUpperBounds();
			// Wildcard syntax doesn't seem to allow multiuple bounds, but since the method returns an array, should test it.
			if (bounds.length == 1) {
				return bounds[0] == Object.class ? null : bounds[0];
			} else {
				throw new EnonException("Can't handle wildcard with more than one upper bound: " + t.getTypeName());
			}
		}
		return t; // if the type is not bounded, just return the type
	}

	/**
	 * Locate the first superclass or interface of subType that is also a subtype of targetSuper.  If called repeatedly,
	 * this method will take a direct path from subType to targetSuper.  If targetSuper is directly extended by subType then
	 * targetSuper is returned.
	 * @param targetSuper The super class that we're headed toward
	 * @param subType The sub type that we're starting from
	 * @return The immediate superclass or interface of subType that also inherits from targetSuper.
	 * @throws EnonException if no superclass or interface extends targetSuper (implies that subType does not extend targetSuper).
	 */
	public Type superTowardType(Class targetSuper, Type subType) {
		Class current = classOfType(subType);
		if (current == targetSuper) {
			return subType;
		}
		Type genericSuper = current.getGenericSuperclass();
		if (genericSuper != null && targetSuper.isAssignableFrom(classOfType(genericSuper))) {
			return genericSuper;
		}
		for (Type genericIface : current.getGenericInterfaces()) {
			if (targetSuper.isAssignableFrom(classOfType(genericIface))) {
				return genericIface;
			}
		}
		throw new EnonException("No superclass or super interface that extends "+targetSuper.getName());
	}
	
	public Type forName(String typeName) {
		String baseName = typeName;
		if (typeName.contains("<")) {
			// parameterized type
			baseName = typeName.substring(0, typeName.indexOf('<'));
			// @TODO: extract type params
		}
		Class primitive = PRIMITIVE_CLASSNAME_MAP.get(baseName);
		if (primitive != null) {
			return primitive;
		}
		if (baseName.indexOf('.') > 0) {
			try {
				return Class.forName(baseName);
			} catch (ClassNotFoundException x) {
				return null;
			}
		}
		// name has no dots -- not a valid classname.  Need to use name mapper
		return null;
	}

	/**
	 * Map the type parameter actuals from a subType into the type parameters of a super type.  The Java reflection API does not
	 * automatically provide this information by following getGenericSuperClass() or getGenericInterfaces().  Actual type information
	 * may only be available at the lowest level where it is declared.  As a result, this method examines the type vars declared in the
	 * super class and maps the actuals from the subclass into those super-type vars.  By calling this iteratively from leaf class to
	 * abstract class, the actuals can be transferred all the way up.
	 * @param superType The super type.  Should be a direct super class or interface of the subtype where the actuals were obtained.
	 * @param subTypeActuals The actual type params of a class immediately below superType.
	 * @return An array matching the type vars of the super class, paired with the actual types declared in the subclass.
	 */
	private TypeVarActual[] transferTypeArgsToSuper(Type superType, TypeVarActual[] subTypeActuals) {
		TypeVarActual[] superActuals = TypeVarActual.ofType(superType);
		return TypeVarActual.merge(superActuals, subTypeActuals);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * DTO class that pairs a type variable name (i.e. "E" or "T") with an actual param value.
	 */
	private static class TypeVarActual {
		/** type variable name */
		private final String tvarName;
		/** the actual type assigned to the type var */
		private final Type actual;

		/**
		 * Constructor
		 * @param tvarName immutable type var name
		 * @param actual immutable actual type value
		 */
		public TypeVarActual(String tvarName, Type actual) {
			this.tvarName = tvarName;
			this.actual = actual;
		}

		/**
		 * Scan the type for type parameters (TypeVariable) and actual type arguments if available.  If the type is a normal Class, then only the
		 * TypeVariables declared for the class are returned.  However, it the type is a ParameterizedType, then the actuals are assigned to the vars. (Note that the
		 * actuals may themselves by type variables or wildcards to be resolved later).
		 * @param type The type to scan for parameters
		 * @return An array of TypeVarActual objects populated from the data in the type.
		 * @throws EnonException if the type is neither a Class nor a ParameterizedType
		 */
		public static TypeVarActual[] ofType(Type type) {
			if (type instanceof Class) {
				TypeVariable[] typeVars = ((Class)type).getTypeParameters();
				TypeVarActual[] result = new TypeVarActual[typeVars.length];
				for (int i = 0; i < typeVars.length; i++) {
					result[i] = new TypeVarActual(typeVars[i].getName(), typeVars[i]);
				}
				return result;
			}
			if (type instanceof ParameterizedType) {
				Class raw = (Class) ((ParameterizedType)type).getRawType();
				TypeVariable[] typeVars = raw.getTypeParameters();
				Type[] actuals = ((ParameterizedType)type).getActualTypeArguments();
				TypeVarActual[] result = new TypeVarActual[actuals.length];
				for (int i = 0; i < actuals.length; i++) {
						result[i] = new TypeVarActual(typeVars[i].getName(), actuals[i]);
				}
				return result;
			}
			throw new EnonException("Can't get actual type vars of type "+(type != null ? type.getTypeName() : "null"));
		}

		/**
		 * Merge the fromActuals[] into the toActuals[] array. The arrays do not have to be the same size.  Merging is done by the TypeVariable name (tvarName).
		 * The resulting array will be of the same size as the toActuals[] array and have the same type variable names as toActuals[].  If the fromActuals[] array
		 * contains actual type arguments for any of the vars in toActuals[], then those actual args are applied.<p>
		 * The toActuals[] array is intended to be the actuals of a super type, and fromActuals[] of a subtype.
		 * @param toActuals Defines the variables for the destination array
		 * @param fromActuals Possibly contains actual args to be assigned to the type variables
		 * @return A new array having merges fromActuals[] into toActuals[].  The original arrays are not modified.
		 */
		public static TypeVarActual[] merge(final TypeVarActual[] toActuals, final TypeVarActual[] fromActuals) {
			// match the result to the toActuals[] array
			TypeVarActual[] result = new TypeVarActual[toActuals.length];
			// iterate the toActuals[] array
			for (int i = 0; i < result.length; i++) {
				result[i] = toActuals[i];  // by default, use the toActual value
				// search for matching actuals in the fromActuals[] array; match by TypeVar name
				if (toActuals[i].actual instanceof TypeVariable) {
					for (TypeVarActual fromActual : fromActuals) {
						if (((TypeVariable)toActuals[i].actual).getName().equals(fromActual.tvarName)) {
							// if a match is found, replace the result with a new entry containing the fromActual value and the toValue TypeVar name.
							result[i] = new TypeVarActual(toActuals[i].tvarName, fromActual.actual);
							break;
						}
					}
				}
			}
			return result;
		}

		/**
		 * Extract an actual argument type from the TypeVarActual instance.  If the actual type is Class or ParameterizedType, then return the
		 * actual itself.  If the actual value is TypeVariable or WildCardType, then return the (upper) bound.<br>
		 * If the actual type is not provided or resolves to Object.class, then null is returned.
		 * @return The actual type parameter of this instance, or null.
		 * @throws EnonException if the actual is none of Class | ParameterizedType | TypeVariable | WildcardType<br>
		 *  - or - if a TpyeVariable or WildcardType has more than 1 bound.
		 */
		public Type getArgType() {
			Type result;
			if (actual instanceof Class || actual instanceof ParameterizedType) {
				// if Class or Parameterized type, just retuen that value
				result = actual;
			} else if (actual instanceof TypeVariable || actual instanceof WildcardType) {
				// if bounded type, return the upper bound
				result = INSTANCE.singleUpperBound(actual);
			} else {
				throw new EnonException("Unsupported type: "+(actual != null ? actual.getTypeName() : "null"));
			}
			return result;
		}

		/**
		 * Iterate through the array of actuals and extract the actual type arguments.
		 * @param actuals Array of TypeVarACtual instances
		 * @return new array containing the actual type arguments from the TypeVarACtual instances
		 */
		public static Type[] getArgTypes(TypeVarActual[] actuals) {
			Type[] result = new Type[actuals.length];
			for (int i = 0; i < actuals.length; i++) {
				result[i] = actuals[i].getArgType();
			}
			return result;
		}

		/**
		 * Provided for better debugging visibility in the IDE: not publicly accessible, not used in the code.
		 * @return string representation
		 */
		@Override
		public String toString() {
			return tvarName + " = " + String.valueOf(actual);
		}
	}
}
