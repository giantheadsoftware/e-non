/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.function.UnaryOperator;
import org.enon.exception.EnonException;

/**
 * Integral types ordered smallest to largest.
 *
 * @author clininger $Id: $
 */
public enum IntType {
	BYTE_OBJECT(Byte.class, 0xFFFFFFFFFFFFFF80L, n -> n.byteValue()),
	BYTE(byte.class, 0xFFFFFFFFFFFFFF80L, n -> n.byteValue()),
	SHORT_OBJECT(Short.class, 0xFFFFFFFFFFFF8000L, n -> n.shortValue()),
	SHORT(short.class, 0xFFFFFFFFFFFF8000L, n -> n.shortValue()),
	INT_OBJECT(Integer.class, 0xFFFFFFFF80000000L, n -> n.intValue()),
	INT(int.class, 0xFFFFFFFF80000000L, n -> n.intValue()),
	LONG_OBJECT(Long.class, 0x8000000000000000L, n -> n.longValue()),
	LONG(long.class, 0x8000000000000000L, n -> n.longValue()),
	BIG_INTEGER(BigInteger.class, null, n -> n instanceof BigInteger ? n : BigInteger.valueOf(n.longValue()));

	private static final Map<Class<? extends Number>, IntType> BY_TYPE = new HashMap<>();

	static {
		for (IntType intType : values()) {
			BY_TYPE.put(intType.type, intType);
		}
	}

	public final Class<? extends Number> type;
	private final Long valueMask;
	private final UnaryOperator<Number> castFn;

	private IntType(Class<? extends Number> type, Long valueMask, UnaryOperator<Number> castFunc) {
		this.type = type;
		this.valueMask = valueMask;
		this.castFn = castFunc;
	}

	public static IntType valueOfType(Class<? extends Number> type) {
		return BY_TYPE.get(type);
	}

	public boolean isPrimitive() {
		return type.isPrimitive();
	}

	/**
	 * Determine whether the value is within the range of this IntType.
	 *
	 * @param value The value to test
	 * @return true if the value could be represented by this IntType.
	 */
	public boolean inRange(Number value) {
		IntType valueType = valueOfType(value.getClass());
		if (valueType == null) {
			return false; // don't evaluate non-integer numbers
		}

		// use the sort order of the IntType values. Items later in the list are big enough to hold items earlier in the list.
		// Note that since the value arg is Number the value will always be boxed.  We'll never get the primitive class types here.
		if (ordinal() >= valueType.ordinal()) {
			return true;
		}

		// If this instance == BIG_INTEGER, true would have been returned by the ordinal() check.
		// Assume this != BIG_INTEGER.
		// Also assume this.valueMask != null, this.minValue != null, this.maxValue != null

		// The value might be out of range.  Test whether any bits in the value are outside our valueMask.
		try {
			long longValue = (valueType == BIG_INTEGER) ? ((BigInteger) value).longValueExact() : value.longValue();
			long masked = longValue & this.valueMask;
			return masked == 0 || masked == valueMask;
		} catch (ArithmeticException x) {
			// BigInteger value is beyond "long" range.
			// we already know that "this" is not BIG_INTEGER, therfore, out of range.
			return false;
		}
	}

	/**
	 * Return the value in the smallest Number representation that will contain it. For example, the value 27L will be returned as (byte)27.
	 *
	 * @param value The value to compact.
	 * @return The value in the same or smaller type
	 */
	public static Number compact(Number value) {
		if (null == valueOfType(value.getClass())) {
			return value; // only compact IntType types
		}
		Number result = value;
		for (IntType intType : values()) {
			if (intType.inRange(value)) {
				result = intType.castFn.apply(value);
				break;
			}
		}
		return result;
	}

	/**
	 * Attempt to return the value "cast" to this int type.
	 * @param value The value to "cast"
	 * @return the value represented by this int type
	 * @throws IllegalArgumentException if the value is out of range for this type
	 */
	public Number cast(Number value) {
		if (inRange(value)) {
			return this.castFn.apply(value);
		}
		if (RealType.valueOfType(value.getClass()) != null) {
			return fromReal(value);
		}
		throw new IllegalArgumentException("Value is invalid type or out of range for "+this.type.getName());
	}

	/**
	 * Attempt to interpret the string as this int type.
	 * @param str String to interpret
	 * @return value of this int type
	 * @throws EnonException if there is a loss of precision
	 * @throws NumberFormatException if the string is not a number
	 */
	public Number fromString(String str) {
		try {
			BigDecimal bd = ((BigDecimal)RealType.BIG_DECIMAL.fromString(str));
			return cast(bd.toBigIntegerExact());
		} catch (ArithmeticException x) {
			// loss of precision
			throw new EnonException("Loss of precision, string -> int", x);
		}
	}

	/**
	 * Not integral value, check for loss of precision.
	 * @param real value to check
	 * @return Integral value of the real if there is no fractional part
	 * @throws EnonException if there is a loss of precision
	 */
	private Number fromReal(Number real) {
		try {
			BigDecimal bd = (BigDecimal) RealType.BIG_DECIMAL.cast(real);
			return cast(bd.toBigIntegerExact());
		} catch (ArithmeticException x) {
			// loss of precision
			throw new EnonException("Loss of precision, real -> int", x);
		} catch (IllegalArgumentException x) {
			throw new EnonException("Real numeric value expected instead of "+real.getClass().getName()+": "+real, x);
		}
	}
}
