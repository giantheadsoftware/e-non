/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.enon;

import org.enon.element.Element;
import org.enon.exception.EnonException;

/**
 * This class takes a ReaderContext and a WrterContext and pipes them together.
 * The ReaderContext is used to import a data stream into the Element domain, 
 * which is then piped to the writer.  The Data Binding domain is not invoked.
 * @author clininger
 * $Id: $
 */
public abstract class Xformer extends Reader {
	private final ReaderContext readerContext;
	private final WriterContext writerContext;

	protected Xformer(ReaderContext readerContext, WriterContext writerContext) {
		super(readerContext);
		this.readerContext = readerContext;
		this.writerContext = writerContext;
	}
	
	public void xform() {
		try {
			validateProlog();
			writerContext.writeProlog();
			Element e = readerContext.parseNextElement();
			while (e != null) {
				e.inContainer(writerContext.getRoot());
				writerContext.write(e);
				e = readerContext.parseNextElement();
			}
		} catch (Exception x) {
			throw new EnonException("Error transforming stream ", x);
		}
	}
}
