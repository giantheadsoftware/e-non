/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import org.enon.cache.EnonCache;
import org.enon.cache.EnonCacheFactory;
import org.enon.codec.coder.IdGenerator;
import org.enon.element.Element;
import org.enon.element.writer.ElementWriterFactory;
import org.enon.exception.EnonWriteException;

/**
 * The WriterContext contains shared resources and maintains state during the eNON write process.
 *
 * @author clininger $Id: $
 */
public class WriterContext extends EnonContext {

	private final OutputStream outputStream;
	private final ElementWriterFactory writerFactory;
	private EnonCache<String, Long> writeGlossary;
	private IdGenerator idGenerator = null;

	/**
	 * Construct a context using the default config and the DefaultElementWriterFactory.
	 * @param out The output stream to write data.
	 */
	public WriterContext(OutputStream out) {
		this(out, EnonConfig.defaults());
	}

	/**
	 * Constructor a context using a custom ElementWriterFactory and custom config.
	 * @param out The output stream to write data
	 * @param config configuration
	 */
	public WriterContext(OutputStream out, EnonConfig config) {
		super(config == null ? EnonConfig.defaults() : config);
		this.writerFactory = this.config.getElementWriterFactory();
		this.outputStream = out;
	}

	/**
	 * Allow subclasses to construct without an OutputStream.
	 */
	protected WriterContext() {
		this(EnonConfig.defaults());
	}

	protected WriterContext(EnonConfig config) {
		super(config == null ? EnonConfig.defaults() : config);
		this.writerFactory = this.config.getElementWriterFactory();
		outputStream = null;
	}

	public OutputStream getOutputStream() {
		return outputStream;
	}

	public EnonCache<String, Long> getWriteGlossary() {
		if (writeGlossary == null && config.hasFeature(FeatureSet.G)) {
			writeGlossary = EnonCacheFactory.getInstance().create(UUID.randomUUID().toString(), String.class, Long.class);
		}
		return writeGlossary;
	}

	public IdGenerator getIdGenerator() {
		return idGenerator;
	}

	public void writeProlog() {
		try {
			writerFactory.prologWriter().write(new Prolog(EnonConfig.FORMAT_VERSION, config.getFeatures()), getOutputStream());
		} catch (IOException x) {
			throw new EnonWriteException("Failed to write prolog: ", x);
		}
	}
	/**
	 * Delegate writing to the ElementWriterFactory.
	 * @param e Element e must have a container that is (or has a path to) the RootContainer
	 */
	public void write(Element e) {
  		writerFactory.writer(e).write(e);
	}
}
