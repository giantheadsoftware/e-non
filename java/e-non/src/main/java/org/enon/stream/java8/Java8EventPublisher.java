/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.stream.java8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.enon.event.EnonEvent;
import org.enon.event.ErrorEvent;
import org.enon.event.EventPublisher;
import org.enon.event.EventType;
import org.enon.exception.EnonException;

/**
 *
 * @author clininger $Id: $
 */
public class Java8EventPublisher implements EventPublisher {

	private final Set<Java8EventListener> listeners = new HashSet<>();
	private final LinkedList<EnonEvent> fifoQueue = new LinkedList();
	protected boolean complete;
	private PublisherThread publisherThread;

	public void addListener(Java8EventListener listener) {
		if (listener == null) {
			throw new EnonException("Cannot add null listener to the even publisher.");
		}
		synchronized (listeners) {
			listeners.add(listener);
			// start the processing thread when the first listener is added
			if (publisherThread == null) {
				publisherThread = new PublisherThread();
				publisherThread.start();
			}
		}
	}

	public boolean removeListener(Java8EventListener listener) {
		synchronized (listeners) {
			return listeners.remove(listener);
		}
	}

	@Override
	public void publish(EnonEvent event) {
		// don't push events to the queue if there are no listeners
		synchronized (listeners) {
			if (listeners.isEmpty()) {
				return;
			}
		}
		synchronized (fifoQueue) {
			fifoQueue.add(event);
			fifoQueue.notify();
		}
	}

	@Override
	public void complete() {
		synchronized (fifoQueue) {
			complete = true;
			fifoQueue.notify();
		}
	}

	@Override
	public void fail(ErrorEvent error) {
		synchronized (fifoQueue) {
			fifoQueue.add(error);
			fifoQueue.notify();
		}
	}

	/**
	 * A Thread to process events
	 */
	private class PublisherThread extends Thread {

		public PublisherThread() {
			super(() -> {
				final List<EnonEvent> events = new ArrayList<>(10);
				while (!complete) {
					// check the fifo queue in a sync block
					synchronized (fifoQueue) {
						while (fifoQueue.isEmpty()) {
							try {
								fifoQueue.wait();
							} catch (InterruptedException x) {
								complete = true;
								break;
							}
						}
						// copy the fifo queue to a local non-sync list
						events.addAll(fifoQueue);
						// we have a local copy of the queue, can exit sync
					}
					if (!complete) {
						events.stream().forEach(action -> {
							synchronized (listeners) {
								listeners.parallelStream()
										.forEach(listener -> listener.accept(action));
							}
						});
					}
					events.clear();
				}
				// complete
				synchronized (listeners) {
					final EnonEvent completeEvent = new EnonEvent(EventType.COMPLETE, "Process complete");
					listeners.parallelStream()
							.forEach(listener -> listener.accept(completeEvent));
					listeners.clear();
				}
				// exit the thread process
			});
		}
	}
}
