/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.stream.java8.util;

import java.util.function.Consumer;

/**
 * Wrap another Consumer with an implementation that will allow the consumer to be terminated. Receiving a null value will terminate the consumer. It can also be terminated with
 * the terminate() method.
 * <p>
 * Upon termination, the consumerImpl will receive one null item to signal completion. No other items will be provided. The consumerImpl is removed so its reference is released.<p>
 * Producers feeding this consumer can check the terminated value to determine whether to continue providing values. This is a sort of cancel feature.
 * <p>
 * This is not designed to be thread-safe.
 *
 * @author clininger $Id: $
 */
public class TerminatingConsumer<T> implements Consumer<T> {

	protected Consumer<T> impl;
	private boolean terminated;

	/**
	 * Wrap the consumer.
	 *
	 * @param impl Must not be null
	 */
	public TerminatingConsumer(Consumer<T> impl) {
		this.impl = impl;
	}

	@Override
	public void accept(T t) {
		if (!terminated) {
			if (t == null) {
				terminate();
			} else {
				impl.accept(t);
			}
		}
	}

	public boolean isTerminated() {
		return terminated;
	}

	public void terminate() {
		if (!terminated) {
			terminated = true;
			impl.accept(null);
			impl = null;
		}
	}
}
