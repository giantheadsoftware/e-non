/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.stream.java8;

import org.enon.stream.java8.util.LimitedConsumer;
import java.io.DataInputStream;
import java.io.InputStream;
import java.util.function.Consumer;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Reader;
import org.enon.element.Element;
import org.enon.event.EnonEvent;
import org.enon.event.EventType;
import org.enon.exception.EnonException;
import org.enon.exception.EnonReadException;
import org.enon.bind.value.NativeValue;

/**
 * This class is slightly complicated due to the lack of a proper stream/reactive/publish-subscribe API in Java 8. Without introducing external libraries, a cancelable subscription
 * was simulated using special implementations of functional interface Consumer: LimitedConsumer and TerminatingConsumer.
 *
 * @author clininger $Id: $
 */
 class Java8Reader extends Reader {

	public Java8Reader(InputStream fromStream) {
		super(fromStream);
	}

	public Java8Reader(DataInputStream in, EnonConfig config) {
		super(in, config);
	}

	public void read(Consumer<Object> consumer) {
		decodeAll(new LimitedConsumer(consumer, new Consumer<NativeValue>() {
			@Override
			public void accept(NativeValue nativeValue) {
				if (nativeValue == null) {
					consumer.accept(null);
				} else {
					try {
						consumer.accept(nativeValue.getValue());
					} catch (EnonException x) {
						consumer.accept(null);
						throw x;
					} catch (Exception x) {
						consumer.accept(null);
						throw new EnonReadException("Could not bind to object: " + nativeValue, x);
					}
				}
			}
		}
		));
	}

	/**
	 * Use the decoder to convert parsed elements to native values.
	 *
	 * @param consumer Receives each native value
	 */
	protected void decodeAll(final LimitedConsumer<Object> consumer) {
		parseAll(new LimitedConsumer<>(consumer, (Element element) -> {
			try {
				// decode the element and pass to the consumer
				consumer.accept(ctx.decode((Class)null, element));
			} catch (EnonException x) {
				consumer.accept(null);
				throw x;
			} catch (Exception x) {
				consumer.accept(null);
				throw new EnonReadException("Could not decode element: " + element, x);
			}
		}));
	}

	/**
	 * The consumer will receive elements until there are no more elements. The last "element" will be a null value (not a NUllElement). The consumer can stop consuming and release
	 * resources when null is received.
	 *
	 * @param consumer To receive elements from the eNON stream
	 */
	protected void parseAll(final LimitedConsumer<Element> consumer) {
		try {
			validateProlog();

			if ((consumer.getLimit() == null || consumer.getLimit() > 1) && !getConfig().hasFeature(FeatureSet.S)) {
				consumer.changeLimit(1L);
			}

			Element next;
			do {
				next = ctx.parseNextElement();
				if (next != null) {
					next.inContainer(getReaderContext().getRoot());
				}
				consumer.accept(next);
			} while (next != null && !consumer.isTerminated());
			consumer.accept(null);
			if (ctx.getInputStream().available() > 0) {
				ctx.getEventPublisher().publish(new EnonEvent(EventType.WARNING, "Data remains in the eNON input stream after reading the requested elements"));
			}
		} catch (EnonException x) {
			consumer.accept(null);
			throw x;
		} catch (Exception x) {
			consumer.accept(null);
			throw new EnonReadException("Could not read eNON stream: ", x);
		}
	}
//
//	@Override
//	protected Java8RootElement readRoot(ReaderContext ctx) throws IOException {
//		return new Java8RootElement.Reader().read(ctx);
//	}

}
