/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.stream.java8.util;

import java.util.function.Consumer;

/**
 * The LimitedConsumer allows a finite yet unknown number of items to be sent to a consumer. Unfortunately, the Java 8 Stream has no facility for that.
 *
 * @author clininger $Id: $
 * @param <T> The type accepted by the consumer
 */
public class LimitedConsumer<T> extends TerminatingConsumer<T> {

	/**
	 * the max number of items that can be accepted
	 */
	private Long limit;
	/**
	 * the number of items that have already been accepted
	 */
	private long count;

	/**
	 * Wrap the provided consumer with a limit for how many items it will receive.
	 *
	 * @param limit Maximum number of items to receive; null indicates no limit
	 * @param consumerImpl The consumer implementation
	 */
	public LimitedConsumer(Long limit, Consumer<T> consumerImpl) {
		super(consumerImpl);
		this.limit = limit;
	}

	/**
	 * Take the limit from the limitProvider if it is a LimitedConsumer. Otherwise, unlimited.
	 *
	 * @param limitProvider Consumer that may provide a limit
	 * @param consumerImpl Consumer that will perform the consuming
	 */
	public LimitedConsumer(Consumer limitProvider, Consumer<T> consumerImpl) {
		super(consumerImpl);
		limit = getLimit(limitProvider);
	}

	/**
	 * If the consumer is a LimitedConsumer, return its limit, else return null
	 *
	 * @param consumer Possibly LimitedConsumer
	 * @return limit or null if unlimited
	 */
	public static Long getLimit(Consumer consumer) {
		return consumer instanceof LimitedConsumer ? ((LimitedConsumer) consumer).limit : null;
	}

	protected boolean isUnderLimit() {
		return limit == null || count < limit;
	}

	public Long getLimit() {
		return limit;
	}

	/**
	 * Reset the limit to the provided value. WARNING: if he new limit is not greater than the current count the consumer will be terminated. Termination can not be reversed.
	 *
	 * @param limit the new limit
	 */
	public void changeLimit(Long limit) {
		this.limit = limit;
		if (!isUnderLimit()) {
			terminate();
		}
	}

	public long getCount() {
		return count;
	}

	@Override
	public void accept(T t) {
		if (!isTerminated() && isUnderLimit()) {
			super.accept(t);
			if (limit != null) {
				count++;
				if (!isUnderLimit()) {
					// we crossed the limit
					terminate();
				}
			}
		}
	}

}
