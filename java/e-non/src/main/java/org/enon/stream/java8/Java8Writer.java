/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.stream.java8;

import java.io.OutputStream;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.LimitParam;
import org.enon.Writer;

/**
 * Writer implementation using Java 8 compatible APIs.  Specifically:
 * <ul>
 * <li>A Java8EventPublisher is used for events.
 * <li>Streams of objects are processed using the Java streams API.
 * </ul>
 * @author clininger $Id: $
 */
public class Java8Writer extends Writer {

	private final Java8EventPublisher eventPublisher = new Java8EventPublisher();

	/**
	 * Constructor using the default configuration
	 * @param toStream Stream where eNON data will be written
	 */
	public Java8Writer(OutputStream toStream) {
		this(toStream, EnonConfig.defaults());
	}

	/**
	 * Constructor
	 * @param out Stream where eNON data will be written
	 * @param config configuration to apply to the writing session
	 */
	public Java8Writer(OutputStream out, EnonConfig config) {
		super(out, config);
		ctx.eventPublisher(eventPublisher);
	}

	/**
	 * Enable processing events to be delivered to the listener.  Multiple listeners can be
	 * added.
	 * @param listener to receive events.
	 * @return this writer object
	 */
	public Java8Writer addListener(Java8EventListener listener) {
		eventPublisher.addListener(listener);
		return this;
	}

	/**
	 * Remove a previously added listener.
	 * @param listener listener to remove
	 * @return true if the listener had been added and has been removed.
	 */
	public boolean removeListener(Java8EventListener listener) {
		return eventPublisher.removeListener(listener);
	}

	public Java8Writer writeStream(Stream<Object> objects) {
		final CountLimiter countLimiter = new CountLimiter();

		ctx.writeProlog();

		objects
				.filter(countLimiter)
				.forEach(this::writeObject);

		return this;
	}

	///////////////////////////////////////////////////////////////////////
	private class CountLimiter implements Predicate<Object> {

		private final Long limit;
		private boolean limitExceeded;
		private long count = 0l;

		public CountLimiter() {
			Long tentativeLimit = null;

			// apply config limits
			if (!getConfig().getFeatures().contains(FeatureSet.S)) {
				tentativeLimit = 1l;
			} else if (getConfig().getLimits().containsKey(LimitParam.MAX_TOP_LEVEL_ELEMENT_COUNT)) {
				tentativeLimit = getConfig().getLimits().get(LimitParam.MAX_TOP_LEVEL_ELEMENT_COUNT);
			}

			this.limit = tentativeLimit;
		}

		@Override
		public boolean test(Object t) {
			if (limit == null) {
				return true;
			}
			if (limitExceeded) {
				return false;  // @TODO: throw, log, notify count exceeded?
			}
			if (++count > limit) {
				limitExceeded = true;
			}
			return !limitExceeded;
		}

	}
}
