/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author clininger $Id: $
 */
public enum EnonControl {
	CTL_START_SERIES((char) 0x02),
	CTL_END_SERIES((char) 0x03),
	CTL_EOT((char) 0x04),
	CTL_META((char) 0x1B);

	private static final Map<Character, EnonControl> lookup = new HashMap<>(EnonControl.values().length);

	static {
		for (EnonControl type : values()) {
			if (null != lookup.put(type.prefix, type)) {
				throw new IllegalArgumentException("Duplicate type prefix '" + type.prefix + "'");
			}
		}
	}

	/**
	 * ASCII prefix for the type
	 */
	public final char prefix;

	private EnonControl(char prefix) {
		this.prefix = prefix;
	}

	/**
   * @param prefix the prefix
	 * @return the type associated with the prefix
	 */
	public static final EnonControl forPrefix(char prefix) {
		return lookup.get(prefix);
	}
}
