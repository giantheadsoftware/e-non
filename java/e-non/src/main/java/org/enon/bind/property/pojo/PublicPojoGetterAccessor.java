/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property.pojo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.enon.bind.property.ReadAccessor;
import org.enon.bind.property.ReadableProperty;
import org.enon.bind.value.NativeValueFactory;
import org.enon.bind.value.PojoPropertyValue;
import org.enon.cache.EnonCache;
import org.enon.cache.EnonCacheFactory;
import org.enon.exception.EnonBindException;

/**
 *
 * @author clininger $Id: $
 */
public class PublicPojoGetterAccessor implements ReadAccessor {

	private final NativeValueFactory nativeValueFactory;
	private final EnonCache<String, ? super Map<String, ReadableProperty>> cache;
	private final PojoPropertyUtil util = new PojoPropertyUtil();

	public PublicPojoGetterAccessor(NativeValueFactory nativeValueFactory) {
		this.nativeValueFactory = nativeValueFactory;
		cache = EnonCacheFactory.getInstance().create(UUID.randomUUID().toString(), String.class, Map.class);
	}

	@Override
	public Map<String, ? extends ReadableProperty> getReadableProperties(Class pojoClass) {
		String className = pojoClass.getName();
		Map<String, ReadableProperty> props = (Map<String, ReadableProperty>) cache.get(className);
		if (props == null) {
			props = locateReadableProperties(pojoClass);
			synchronized (cache) {
				cache.put(className, props);
			}
		}
		return props;
	}

	/**
	 * Identify the methods on the class that conform to getter conventions
	 *
	 * @param c Class to scan for getters
	 * @return list of public getter property
	 */
	private Map<String, ReadableProperty> locateReadableProperties(Class c) {
		Method[] methods = c.getMethods();
		return  Arrays.stream(methods)
				.map(this::readablePropertyFor)
				.filter(rp -> rp != null)
				.collect(Collectors.toMap(ReadableProperty::getName, Function.identity()));
	}
	
	private ReadableProperty readablePropertyFor(final Method method) {
		final String getterName = util.getterName(method);
		if (getterName != null) {
			return new ReadableProperty() {
				@Override
				public String getName() {
					return util.propertyName(method);
				}

				@Override
				public PojoPropertyValue readValue(Object fromInstance) {
					try {
						return new PojoPropertyValue(getName(), nativeValueFactory.nativeValue(method.invoke(fromInstance)));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException x) {
						throw new EnonBindException("Could not read value of property "+getName(), x);
					}
				}
			};
		}
		return null;
	}
}
