/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.Map;
import java.util.stream.Collectors;
import org.enon.EnonConfig;

/**
 *
 * @author clininger $Id: $
 */
public class PojoValue extends MapValue<StringValue, NativeValue> {

	protected final Object pojo;
	protected Map<StringValue, NativeValue> map;

	private PojoValue(Object pojo, EnonConfig config) {
		super(null, config);
		this.pojo = pojo;
	}

	public Object getPojo() {
		return pojo;
	}

	/**
	 * Return the property names &amp; values as a map. This value is populated lazily and the result is cached in case it is accessed again.
	 *
	 * @return map of propertyName -&gt; nativeValue
	 */
	@Override
	public Map<StringValue, NativeValue> getValue() {
		if (map != null) {
			return map;
		}
		map = config.getPropertyAccessorFactory().getPojoReadAccessor().getReadableProperties(pojo.getClass()).entrySet().stream()
				.map(entry -> entry.getValue())
				.map(reader -> reader.readValue(pojo))
				.collect(Collectors.toMap(ppv -> ppv.getKey(), ppv -> ppv.getValue()));
		return map;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + pojo.getClass().getName() + "]";
	}

	/**
	 * Acceptor for PojoValue
	 */
	public static class Acceptor extends ValueTypeAcceptor<PojoValue> {

		public Acceptor(EnonConfig config) {
			super(config);
		}

		@Override
		public AcceptLevel accept(Class t) {
			return (t != null) ? AcceptLevel.LAST_RESORT : AcceptLevel.NEVER;
		}

		@Override
		public PojoValue toNativeValue(Class type, Object value) {
			return new PojoValue(value, config);
		}

	}
}
