/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static org.enon.bind.value.ValueTypeAcceptor.AcceptLevel.*;
import org.enon.EnonConfig;
import org.enon.exception.EnonException;

/**
 * The default factory uses a list of NativeValueAcceptors to examine native objects and produce NativeValue instances. The NativeValueAcceptors are checked in order, and the first
 * one which accepts the object will produce the NativeValue.<p>
 * By default, the last acceptor is the PojoValue acceptor, which will accept all objects.<p>
 * Subclasses can provide a different list of acceptors.<p>
 * When an input object is successfully matched with an acceptor, the class of the object is cached so that it will be matched immediately with the same acceptor on subsequent
 * calls.<p>
 * The reason this class is not a singleton is to support multiple configurations. Only one instance of this class is needed per configuration.
 *
 * @author clininger $Id: $
 */
public class DefaultNativeValueFactory implements NativeValueFactory {

	/**
	 * singleton instance of the NullNativeValue
	 */
	protected static final NativeValue NULL_VALUE = NullNativeValue.INSTANCE;

	/** the config for this session */
	private final EnonConfig config;

	/**
	 * List of classes that implement ValueTypeAcceptor. Each needs to have a constructor
	 * that with the argument (EnonConfig).
	 */
	private final List<Class<? extends ValueTypeAcceptor>> acceptorImplClasses;

	/**
	 * array of acceptors in order of precedence
	 */
	private ValueTypeAcceptor[] acceptors;

	/**
	 * previously matched acceptors cached by the class names they matched to
	 */
	private volatile Map<String, ValueTypeAcceptor> acceptorCache;

	/**
	 * Use the default acceptor implementations.
	 *
	 * @param config Config passed to the ValueTypeAcceptors during construction
	 */
	public DefaultNativeValueFactory(EnonConfig config) {
		this.config = config;
		this.acceptorImplClasses = getDefaultAccptors();
	}

	/**
	 * Provide a customized array of acceptor implementation classes.
	 *
	 * @param config This config is passed to the constructor of the ValueTypeAcceptors.
	 * @param acceptorImpls Acceptor classes to use, overriding the default array.
	 * All classes must have a constructor that accepts the EnonConfig as a parameter.
	 * These impls will be tested in order when attempting to create a NativeValue from an Object.
	 */
	public DefaultNativeValueFactory(EnonConfig config, List<Class<? extends ValueTypeAcceptor>> acceptorImpls) {
		this.config = config;
		this.acceptorImplClasses = acceptorImpls;
	}

	/**
	 * Lazy-initialize the cache in case subclasses don't want to use it.
	 *
	 * @return The instance of the cache, never returns null.
	 */
	protected Map<String, ValueTypeAcceptor> getCache() {
		Map<String, ValueTypeAcceptor> acceptorCacheLocal = acceptorCache;
		if (acceptorCacheLocal == null) {
			synchronized(this) {
				acceptorCacheLocal = acceptorCache;
				if (acceptorCacheLocal == null) {
					acceptorCache = acceptorCacheLocal = Collections.synchronizedMap(new HashMap<>());
				}
			}
		}
		return acceptorCacheLocal;
	}

	/**
	 * Construct a NativeValue instance for the object.<br>
	 * If the object is null, a NullNativeValue is returned.<br>
	 * If a ValueTypeAcceptor is cached for the object's class, use that to create the value.<br>
	 * Otherwise, iterate the ValueTypeAcceptor list for an acceptor to handle the object.<br>
	 * If no acceptors take the object, null is returned.
	 *
	 * @param object object to wrap
	 * @return See description.
	 */
	@Override
	public NativeValue nativeValue(Object object) {
		if (object == null) {
			return NULL_VALUE;
		}

		ValueTypeAcceptor cachedAcceptor = getCached(object);
		Class type = object.getClass();
		if (cachedAcceptor != null) {
			return cachedAcceptor.toNativeValue(type, object);
		}

		// not cached, test the available acceptors
		ValueTypeAcceptor acceptor = acceptorForClass(type);
		if (acceptor != null) {
			return acceptor.toNativeValue(type, object);
		}

		return null;
	}

	/**
	 * Check the acceptor cache for an entry previously matched to the object's className. Only exact matches are checked; superclasses and interfaces are not searched.
	 *
	 * @param object The object to test. The cache is searched by the object's class name.
	 * @return A ValueTypeAcceptor instance or null if not cached.
	 */
	protected ValueTypeAcceptor getCached(Object object) {
		return getCached(object.getClass().getName());
	}

	/**
	 * Check the acceptor cache for an entry previously matched to the object's className. Only exact matches are checked; superclasses and interfaces are not searched.
	 *
	 * @param className The class name to check in the cache.
	 * @return A ValueTypeAcceptor instance or null if not cached.
	 */
	protected ValueTypeAcceptor getCached(String className) {
		return getCache().get(className);
	}

	/**
	 * Add the class name to the cache with its matching acceptor.
	 *
	 * @param className The key to the item in the cache
	 * @param acceptor The acceptor to associate with the class name.
	 */
	protected void cache(String className, ValueTypeAcceptor acceptor) {
		getCache().put(className, acceptor);
	}

	/**
	 * Iterate through the list of acceptorImplClasses looking for the one that makes the greatest claim on the type via its accept() method.
	 * The first Acceptor that returns PREFER will be used, if any.
	 * Otherwise, the first acceptor returning the next greatest claim will be used.
	 * If all acceptors claim NEVER, then null is returned.
	 *
	 * @param type The type to test for an acceptor
	 * @return The preferred acceptor for this type, or null if no acceptors will handle it.
	 */
	protected ValueTypeAcceptor acceptorForClass(Class type) {
		// initialize the acceptors list if necessary
		if (acceptors == null) {
			acceptors = acceptorImplClasses.stream()
					.map(this::instantiate)
					.collect(Collectors.toList())
					.toArray(new ValueTypeAcceptor[]{});
		}
		// set up iteration variables
		ValueTypeAcceptor.AcceptLevel currentLevel = NEVER;
		ValueTypeAcceptor currentAcceptor = null;

		// go through the list of acceptors, looking for the acceptor that can best handle the type
		for (ValueTypeAcceptor acceptor : acceptors) {
			// get the AcceptLevel of the acceptor for this type
			ValueTypeAcceptor.AcceptLevel level = acceptor.accept(type);

			// return the first acceptor that claims to PREFER the type
			if (PREFER == level) {
				cache(type.getName(), acceptor);
				return acceptor;
			}
			// as long as no acceptor says PREFER, keep track of the highest AcceptLevel we find.
			if (level != NEVER && level.ordinal() < currentLevel.ordinal()) {
				currentLevel = level;
				currentAcceptor = acceptor;
			}
		}
		// cache all results, even null, to prevent this process from being repeated for this type
		cache(type.getName(), currentAcceptor);

		return currentAcceptor;
	}

	/**
	 * Instantiate a ValueTypeAcceptor class.
	 *
	 * @param implClass The class to instantiate
	 * @param config The config passed to the constructor of the implClass
	 * @return A new instance of the implClass
	 * @throws EnonException if the instantiation fails
	 */
	private ValueTypeAcceptor instantiate(Class<? extends ValueTypeAcceptor> implClass) {
		try {
			return implClass.getConstructor(EnonConfig.class).newInstance(config);
		} catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException
				| NoSuchMethodException | SecurityException x) {
			throw new EnonException("Could not instantiate all ValueTypeAcceptor instances", x);
		}
	}

}
