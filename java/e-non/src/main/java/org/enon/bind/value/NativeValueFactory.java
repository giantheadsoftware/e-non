/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author clininger $Id: $
 */
@FunctionalInterface
public interface NativeValueFactory {

	/**
	 * Attempt to create a NativeValue wrapper for the object.
	 *
	 * @param object object to wrap
	 * @return a native value wrapper for the object. Returns null if the factory doesn't know how to wrap the object.
	 */
	NativeValue nativeValue(Object object);

	/**
	 * Get a copy of the default acceptor class list. Override this method to customize the list.
	 *
	 * @return A modifiable list of ValueTypeAcceptor. Each call returns a new copy of the list.
	 */
	default List<Class<? extends ValueTypeAcceptor>> getDefaultAccptors() {
		return Arrays.asList(new Class[]{
			TemporalValue.Acceptor.class,
			StringValue.Acceptor.class,
			ScalarValue.Acceptor.class,
			CollectionValue.Acceptor.class,
			MapValue.Acceptor.class,
			BlobValue.Acceptor.class,
			ArrayValue.Acceptor.class,
			ImmutableValue.Acceptor.class,
			PojoValue.Acceptor.class
		}
		);
	}
;

}
