/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.codec.DataCategory;

/**
 *
 * @author clininger $Id: $
 * @param <T> Data type backing the ScalarValue
 */
public class ScalarValue<T> extends AbstractValue<T> {

	protected ScalarValue(DataCategory category, Class<? super T> type, T value) {
		super(category, type, value);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + type.getSimpleName() + ")"
				+ "[" + String.valueOf(getValue()) + "]";
	}

	/**
	 * Acceptor for the ScalarValue class
	 */
	public static class Acceptor extends ValueTypeAcceptor<ScalarValue> {

		private static final Map<Class, DataCategory> CATEGORIES = new HashMap<>();
		private static final Map<Class, AcceptLevel> ACCEPTS = new HashMap<>();

		static {
			CATEGORIES.put(boolean.class, DataCategory.BOOLEAN);
			CATEGORIES.put(Boolean.class, DataCategory.BOOLEAN);
			CATEGORIES.put(byte.class, DataCategory.NUM_INT);
			CATEGORIES.put(Byte.class, DataCategory.NUM_INT);
			CATEGORIES.put(short.class, DataCategory.NUM_INT);
			CATEGORIES.put(Short.class, DataCategory.NUM_INT);
			CATEGORIES.put(int.class, DataCategory.NUM_INT);
			CATEGORIES.put(Integer.class, DataCategory.NUM_INT);
			CATEGORIES.put(long.class, DataCategory.NUM_INT);
			CATEGORIES.put(Long.class, DataCategory.NUM_INT);
			CATEGORIES.put(float.class, DataCategory.NUM_REAL);
			CATEGORIES.put(Float.class, DataCategory.NUM_REAL);
			CATEGORIES.put(double.class, DataCategory.NUM_REAL);
			CATEGORIES.put(Double.class, DataCategory.NUM_REAL);
			CATEGORIES.put(BigInteger.class, DataCategory.NUM_INT);
			CATEGORIES.put(BigDecimal.class, DataCategory.NUM_REAL);

			for (Class c : CATEGORIES.keySet()) {
				ACCEPTS.put(c, AcceptLevel.PREFER);
			}
		}

		public Acceptor(EnonConfig config) {
			super(config);
		}

		@Override
		protected Map<Class, AcceptLevel> acceptsClasses() {
			return ACCEPTS;
		}

		@Override
		public ScalarValue toNativeValue(final Class type, final Object value) {
//			Class acceptedType = type.isPrimitive() ? WRAPPERS.get(type) : type;
			DataCategory datCat = CATEGORIES.get(type);
			if (datCat == null) {
				// if we don't have a direct match, look for subclasses
				for (Class c : CATEGORIES.keySet()) {
					if (c.isAssignableFrom(type)) {
						datCat = CATEGORIES.get(c);
					}
				}
			}
			return new ScalarValue(datCat, type, value);
		}
	}

}
