/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property;

import org.enon.bind.property.pojo.PublicPojoPropertyAccessor;
import org.enon.EnonConfig;
import org.enon.bind.property.pojo.PublicFinalFieldAccessor;
import org.enon.bind.property.pojo.PublicPojoGetterAccessor;

/**
 *
 * @author clininger $Id: $
 */
public class DefaultPropertyAccessorFactory implements PropertyAccessorFactory {

	private final PublicPojoPropertyAccessor pojoAccessor;
	private final PublicPojoGetterAccessor getterAccessor;
	private final PublicFinalFieldAccessor fieldAccessor;

	public DefaultPropertyAccessorFactory(EnonConfig config) {
		pojoAccessor = new PublicPojoPropertyAccessor(config.getNativeValueFactory());
		getterAccessor = new PublicPojoGetterAccessor(config.getNativeValueFactory());
		fieldAccessor = new PublicFinalFieldAccessor(config.getNativeValueFactory());
	}

	@Override
	public ReadAccessor getPojoReadAccessor() {
		return pojoAccessor;
	}

	@Override
	public WriteAccessor getPojoWriteAccessor() {
		return pojoAccessor;
	}

	@Override
	public ReadAccessor getPublicGetterReadAccessor() {
		return getterAccessor;
	}

	@Override
	public ReadAccessor getPublicFinalFieldReadAccessor() {
		return fieldAccessor;
	}
}
