/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property;

import java.lang.reflect.Type;

/**
 *
 * @author clininger $Id: $
 */
public interface WritableProperty {
	
	/**
	 * @return The name of the property
	 */
	String getName();
	
	Type getPropertyType();

	/**
	 * Set the value into the instance.
	 * @param toInstance The instance to modify
	 * @param value the value to write
	 */
	void writeValue(Object toInstance, Object value);
}
