/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.Map;

/**
 *
 * @author clininger $Id: $
 */
public class PojoPropertyValue implements Map.Entry<StringValue, NativeValue> {

	private final String key;
	private NativeValue value;

	public PojoPropertyValue(String key) {
		this.key = key;
	}

	public PojoPropertyValue(String key, NativeValue value) {
		this(key);
		setValue(value);
	}

	/**
	 * @return the name of the property
	 */
	@Override
	public StringValue getKey() {
		return new StringValue(key);
	}

	@Override
	public NativeValue getValue() {
		return value;
	}

	@Override
	public final NativeValue setValue(NativeValue value) {
		NativeValue prev = value;
		this.value = value;
		return prev;
	}

}
