/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.enon.EnonConfig;
import org.enon.codec.DataCategory;

/**
 *
 * @author clininger $Id: $
 */
public class CollectionValue extends AbstractValue<Collection<? extends NativeValue>> {

	private CollectionValue(Class<Collection<? extends NativeValue>> type, Collection<? extends NativeValue> value) {
		super(DataCategory.COLLECTION, type, value);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + type.getSimpleName() + ")"
				+ "[" + value.size() + "]";
	}

	/////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Accept anything that extends Collection or is an array.  Because this Acceptor will handle arrays it will compete with
	 * the ArrayValue.Acceptor.  The ArrayValueAccpetor
	 */
	public static class Acceptor extends ValueTypeAcceptor<CollectionValue> {

		private final NativeValueFactory nativeValueFactory;

		public Acceptor(EnonConfig config) {
			super(config);
			nativeValueFactory = config.getNativeValueFactory();
		}


		/**
		 * Accept any type of collection or array.
		 * @param t The type to be tested
		 * @return True if t is a Collection or array.
		 */
		@Override
		public AcceptLevel accept(Class t) {
			if (t != null) {
				if (Collection.class.isAssignableFrom(t)) {
					return AcceptLevel.PREFER;
				}
				if (t.isArray()) {
					// primitive arrays should be handled elsewhere if at all possible.  Non-primitive arrays must be converted to lists.
					return t.getComponentType().isPrimitive() ? AcceptLevel.LAST_RESORT : AcceptLevel.PREFER;
				}
			}
			return AcceptLevel.NEVER;
		}

		/**
		 * Convert each of the collection entries into a NativeValue then construct a CollectionValue with those values in a List.
		 * The original collection is iterated in its natural order.  That order is then captured in a List.
		 * @param type Collection subtype
		 * @param value the collection instance
		 * @return A new CollectionValue instance
		 */
		@Override
		public CollectionValue toNativeValue(Class type, Object value) {
			if (type.isArray()) {
				int length = Array.getLength(value);
				Collection<Object> collection = new ArrayList<>(length);
				for (int i = 0; i < length; i++) {
					collection.add(Array.get(value, i));
				}
				value = collection;
			}

			// convert the stream to a List<NativeValue>
			List<NativeValue> nvCollection = ((Collection<Object>)value).stream()
					.map(nativeValueFactory::nativeValue)
					.collect(Collectors.toList());
			return new CollectionValue((Class<Collection<? extends NativeValue>>) type, nvCollection);
		}
	}
}
