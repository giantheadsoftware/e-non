/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.codec.DataCategory;

/**
 *
 * @author clininger $Id: $
 * @param <K> Type of the map key
 * @param <V> Type of the map value
 */
public class MapValue<K extends NativeValue, V extends NativeValue> extends AbstractValue<Map<K, V>> {

	/**
	 * send an empty map to the superclass. We won't be using it.
	 */
	private static final Map VALUE = Collections.EMPTY_MAP;

	/**
	 * This is the original map that we want to wrap when getValue() is called
	 */
	protected final Map<? extends Object, Object> map;
	protected final EnonConfig config;

	/**
	 * cache the value returned by getValue()
	 */
	protected Map<K, V> valueMap;

	protected MapValue(Map<? extends Object, Object> map, EnonConfig config) {
		super(DataCategory.MAP, (Class<Map<K, V>>) VALUE.getClass(), VALUE);
		this.map = map;
		this.config = config;
	}

	@Override
	public Map<K, V> getValue() {
		if (valueMap != null) {
			return valueMap;
		}
		Map<K, V> result = new LinkedHashMap<>();
		map.entrySet().stream()
				.map(this::toNativeWrapperEntry)
				.forEach(entry -> result.put(entry.getKey(), entry.getValue()));
		valueMap = result;
		return result;
	}

	protected Map.Entry<K, V> toNativeWrapperEntry(Map.Entry<? extends Object, Object> entry) {
		Object entryKey = entry.getKey();
		K key = (K) config.getNativeValueFactory().nativeValue(entryKey);

		Object entryValue = entry.getValue();
		V nativeValue = (V) config.getNativeValueFactory().nativeValue(entryValue);

		return new AbstractMap.SimpleImmutableEntry(key, nativeValue);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + map.getClass().getSimpleName() + ")"
				+ "[" + map.size() + "]";
	}

	/**
	 * Acceptor class for MapValue
	 */
	public static class Acceptor extends ValueTypeAcceptor<MapValue> {

		public Acceptor(EnonConfig config) {
			super(config);
		}

		@Override
		public AcceptLevel accept(Class t) {
			return (t != null && Map.class.isAssignableFrom(t)) ? AcceptLevel.PREFER : AcceptLevel.NEVER;
		}

		@Override
		public MapValue toNativeValue(Class type, Object value) {
			return new MapValue((Map) value, config);
		}

	}
}
