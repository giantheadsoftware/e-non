/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.temporal;

import java.util.HashMap;
import java.util.Map;

/**
 * field header masks, arranged from low-bit to high-bit
 */
public enum EnonTemporalField {
	ERA('E'),
	YEAR('Y'),
	MONTH('M'),
	DAY('D'),
	HOUR('h'),
	MINUTE('m'),
	SECOND('s'),
	MS_IN_MINUTE('S'),
	NANOS('n'),
	INSTANT('i'),
	OFFSET_SECONDS('o'),
	OFFSET_INTERVALS('O'),
	ZONE_ID('z');

	private static final Map<Byte, EnonTemporalField> LOOKUP = new HashMap<>(EnonTemporalField.values().length);

	static {
		for (EnonTemporalField type : values()) {
			if (null != LOOKUP.put(type.key, type)) {
				// perform a quick ckeck for potential programming error
				throw new IllegalArgumentException("Duplicate type prefix for type: " + type.name() + ": '" + type.key + "'");
			}
		}
	}

	public final byte key;

	private EnonTemporalField(char key) {
		this.key = (byte)key;
	}

	public static EnonTemporalField forKey(byte key) {
		return LOOKUP.get(key);
	}

}
