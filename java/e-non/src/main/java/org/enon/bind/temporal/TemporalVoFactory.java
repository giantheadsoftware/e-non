/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.temporal;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import static java.time.temporal.ChronoField.*;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.enon.exception.EnonBindException;
import org.enon.exception.EnonCoderException;

/**
 *
 * @author clininger $Id: $
 */
public class TemporalVoFactory {
	private static TemporalVoFactory instance;

	/**
	 * Fields ordered to optimize placement in the TemporalValue, based on field priority
	 */
	private final ChronoField[] supportedChronoFields = new ChronoField[]{
		INSTANT_SECONDS,
		ERA,
		YEAR,
		MONTH_OF_YEAR,
		DAY_OF_MONTH,
		HOUR_OF_DAY,
		MINUTE_OF_HOUR,
		SECOND_OF_MINUTE,
		NANO_OF_SECOND,
		MICRO_OF_SECOND,
		MILLI_OF_SECOND,
		OFFSET_SECONDS
	};
	private final Map<Class, Function<Object, TemporalVO>> factoryFnCache = new HashMap<>();

	protected TemporalVoFactory() {
	}

	public static TemporalVoFactory getInstance() {
		if (instance != null) {
			return instance;
		}
		instance = new TemporalVoFactory();
		return instance;
	}

	public TemporalVO create(final Object object) {
		Class cls = object.getClass();
		Function<Object, TemporalVO> factoryFn = factoryFnCache.get(cls);
		if (factoryFn == null) {
			factoryFn = getFactoryFn(cls);
			factoryFnCache.put(cls, factoryFn);
		}
		return factoryFn.apply(object);
	}

	/**
	 * A "temporal mapper" is a function that converts a temporal object into a map of TemporalField and values.
	 *
	 * @param temporalType the type for which to get the factory function
	 * @return the factory function
	 */
	protected Function<Object, TemporalVO> getFactoryFn(final Class temporalType) {
		// factory fn for Date
		if (Date.class.isAssignableFrom(temporalType)) {
			return tVal -> TemporalVO.createInstant(((Date) tVal).getTime());
		}
		// factory fn for TemporalAccessor
		if (Instant.class.isAssignableFrom(temporalType)) {
			return tVal -> new TemporalVO.Builder()
					.instant(((Instant)tVal).getEpochSecond()*1000 + ((Instant)tVal).getNano()/1000000)
					.nanos(((Instant)tVal).getNano()%1000000).build();
		}
		if (TemporalAccessor.class.isAssignableFrom(temporalType)) {
			return tVal -> {
				TemporalAccessor ta = (TemporalAccessor) tVal;
				TemporalVO.Builder builder = new TemporalVO.Builder();
				for (ChronoField field : supportedChronoFields) {
					if (ta.isSupported(field)) {
						switch(field) {
							case INSTANT_SECONDS:
								long nanos = ta.getLong(NANO_OF_SECOND);
								long millis = nanos / 1000000;
								TemporalVO.Builder tvoBuilder = new TemporalVO.Builder().instant(ta.getLong(INSTANT_SECONDS)*1000 + millis).nanos((int) (nanos % 1000000));
								if (ta instanceof ZonedDateTime && ((ZonedDateTime)ta).getZone() != null) {
									tvoBuilder.zoneId(((ZonedDateTime)ta).getZone().toString());
								} else if (ta.isSupported(OFFSET_SECONDS)) {
									tvoBuilder.offsetSeconds(ta.get(OFFSET_SECONDS));
								}
								return tvoBuilder.build();

							case ERA:
								// @TODO: support non-current era
								if (1 != ta.get(field)) {
									throw new EnonBindException("Only the current era is supported");
								}
								break;

							case YEAR:
								builder.year(ta.get(field));
								break;

							case MONTH_OF_YEAR:
								builder.month((byte) ta.get(MONTH_OF_YEAR));
								break;

							case DAY_OF_MONTH:
								builder.day((byte)ta.get(field));
								break;

							case HOUR_OF_DAY:
								builder.hour((byte) ta.get(field));
								break;

							case MINUTE_OF_HOUR:
								builder.minute((byte) ta.get(field));
								break;

							case SECOND_OF_MINUTE:
								builder.second((byte) ta.get(field));
								break;

							case NANO_OF_SECOND:
								builder.nanos(ta.get(field));
								break;

							case OFFSET_SECONDS:
								builder.offsetSeconds(ta.get(field));
								break;
						}
					}
				}
				return builder.build();
			};
		}
//		if (TemporalAmount.class.isAssignableFrom(temporalType)) {
//			return tVal -> {
//
//			};
//		}
		// factory fn for Calendar
		if (Calendar.class.isAssignableFrom(temporalType)) {
			return tVal -> TemporalVO.createInstantZoned(((Calendar)tVal).getTimeInMillis(), ((Calendar)tVal).getTimeZone().getID());
		}
		throw new EnonCoderException("Can't create temporal value from native value " + temporalType.getName());
	}

	/**
	 * Return the number of milliseconds of second, unless the precision is greater than milliseconds
	 * @param ta TemporalAccessor
	 * @return number of millis in second, or null if precision is greater than millisecond.
	 */
	private Short getMillis(TemporalAccessor ta) {
		if (ta.isSupported(NANO_OF_SECOND)) {
			int nano = ta.get(NANO_OF_SECOND);
			if (nano % 1000000 == 0) {
				return (short)(nano / 1000000);
			}
			return null;
		}
		if (ta.isSupported(MICRO_OF_SECOND)) {
			int micro = ta.get(MICRO_OF_SECOND);
			if (micro % 1000 == 0) {
				return (short)(micro / 1000);
			}
			return null;
		}
		if (ta.isSupported(MILLI_OF_SECOND)) {
			return (short)ta.get(MILLI_OF_SECOND);
		}
		return (short)0;
	}

}
