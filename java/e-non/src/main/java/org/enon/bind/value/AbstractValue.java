/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import org.enon.codec.DataCategory;
import org.enon.exception.EnonException;

/**
 * NativeValue associates a DataCategory with some object. The object of type T is the "value".
 *
 * @author clininger $Id: $
 * @param <T> The type of the native value encapsulated
 */
public abstract class AbstractValue<T> implements NativeValue<T> {

	protected final DataCategory category;
	protected final Class<? super T> type;
	protected final T value;

	public AbstractValue(DataCategory category, Class<? super T> type, T value) {
		if (type == null || value == null || category == null) {
			throw new EnonException("type, value & category are all required");
		}
		this.category = category;
		this.type = type;
		this.value = value;
	}

	@Override
	public DataCategory getCategory() {
		return category;
	}

	@Override
	public Class<? super T> getDeclaredType() {
		return type;
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + String.valueOf(getValue()) + "]";
	}
}
