/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.bind.temporal.TemporalVO;
import org.enon.bind.temporal.TemporalVoFactory;
import org.enon.codec.DataCategory;

/**
 *
 * @author clininger $Id: $
 */
public class TemporalValue extends AbstractValue<TemporalVO> {

	protected TemporalValue(Class<? super Object> type, TemporalVO value) {
		super(DataCategory.TEMPORAL, type, value);
	}

	/////////////////////////////////////////////////////////////////////////////////
	public static class Acceptor extends ValueTypeAcceptor<TemporalValue> {

		private final Set<Class> ACCEPT_CLASSES = new HashSet<>();
		private final Map<Class, AcceptLevel> ACCEPTS;
		private final TemporalVoFactory temporalValueFactory = TemporalVoFactory.getInstance();

		public Acceptor(EnonConfig config) {
			super(config);
			ACCEPT_CLASSES.add(Date.class);
			ACCEPT_CLASSES.add(Calendar.class);
			ACCEPT_CLASSES.add(Duration.class);
			ACCEPT_CLASSES.add(Instant.class);
			ACCEPT_CLASSES.add(LocalDate.class);
			ACCEPT_CLASSES.add(LocalDateTime.class);
			ACCEPT_CLASSES.add(LocalTime.class);
			ACCEPT_CLASSES.add(MonthDay.class);
			ACCEPT_CLASSES.add(OffsetDateTime.class);
			ACCEPT_CLASSES.add(OffsetTime.class);
			ACCEPT_CLASSES.add(Period.class);
			ACCEPT_CLASSES.add(Year.class);
			ACCEPT_CLASSES.add(YearMonth.class);
			ACCEPT_CLASSES.add(ZonedDateTime.class);
			ACCEPT_CLASSES.add(ZoneId.class);
			ACCEPT_CLASSES.add(ZoneOffset.class);
			ACCEPTS = new HashMap<>();
			ACCEPT_CLASSES.forEach(cls -> ACCEPTS.put(cls, AcceptLevel.PREFER));
		}
		
		@Override
		protected Map<Class, AcceptLevel> acceptsClasses() {
			return ACCEPTS;
		}

		@Override
		public TemporalValue toNativeValue(Class t, Object u) {
			return new TemporalValue(t, temporalValueFactory.create(u));
		}
		
	}
}
