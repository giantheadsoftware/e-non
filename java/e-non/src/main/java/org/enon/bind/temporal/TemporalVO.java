/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.temporal;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import org.enon.exception.EnonException;

/**
 * Immutable container for a temporal value.  There are many ways to represent temporal values both 
 * in the Java model and on the eNON stream.  The TemporalVO serves as a normalized means of storing 
 * temporal data that can mediate between the two domains.
 * @author clininger $Id: $
 */
public class TemporalVO {
	/** no temporal fields are set, or all are default */
	public static final int NONE_MASK = 0;
	/** Single bit which is set when instant is not null */
	public static final int INSTANT_MASK = 0x0001;
	/** Single bit which is set when era is not null */
	public static final int ERA_MASK     = 0x0002;
	/** Single bit which is set when year is not null */
	public static final int YEAR_MASK    = 0x0004;
	/** Single bit which is set when month is not null */
	public static final int MONTH_MASK   = 0x0008;
	/** Single bit which is set when day is not null */
	public static final int DAY_MASK     = 0x0010;
	/** Single bit which is set when hour is not null */
	public static final int HOUR_MASK    = 0x0020;
	/** Single bit which is set when minute is not null */
	public static final int MINUTE_MASK  = 0x0040;
	/** Single bit which is set when sec is not null */
	public static final int SECOND_MASK  = 0x0080;
	/** Single bit which is set when nanos is not null and has magnitude grater than 1 ms*/
	public static final int MILLI_MASK   = 0x0100;
	/** Single bit which is set when nanos is not null */
	public static final int NANO_MASK    = 0x0200;
	/** Single bit which is set when offsetSeconds is not null */
	public static final int OFFSET_MASK  = 0x0400;
	/** Single bit which is set when zoneId is not null */
	public static final int ZONEID_MASK  = 0x0800;


	/** Combination of YEAR_MASK + MONTH_MASK */
	public static final int YEAR_MONTH_MASK = YEAR_MASK + MONTH_MASK;
	/** Combination of MONTH_MASK + DAY_MASK*/
	public static final int MONTH_DAY_MASK = MONTH_MASK + DAY_MASK;
	/** Combination of YEAR_MASK + MONTH_MASK + DAY_MASK */
	public static final int FULL_DATE_MASK = YEAR_MASK + MONTH_MASK + DAY_MASK;
	/** Combination of FULL_DATE_MASK + HOUR_MASK*/
	public static final int FULL_DATE_H_MASK = FULL_DATE_MASK + HOUR_MASK;
	/** Combination of FULL_DATE_MASK + HOUR_MASK + MINUTE_MASK*/
	public static final int FULL_DATE_HM_MASK = FULL_DATE_H_MASK + MINUTE_MASK;
	/** Combination of HOUR_MASK + MINUTE_MASK + SECOND_MASK */
	public static final int TIME_S_MASK = HOUR_MASK + MINUTE_MASK + SECOND_MASK;
	/** Combination of DATE_MASK + TIME_S_MASK */
	public static final int FULL_DATE_TIME_S_MASK = FULL_DATE_MASK + TIME_S_MASK;
	/** Combination of FULL_DATE_TIME_S_MASK + MILLI_MASK */
	public static final int FULL_DATE_TIME_MS_MASK = FULL_DATE_TIME_S_MASK + MILLI_MASK;
	/** Combination of FULL_DATE_TIME_S_MASK + NANO_MASK */
	public static final int FULL_DATE_TIME_NS_MASK = FULL_DATE_TIME_S_MASK + NANO_MASK;
	/** Combination of FULL_DATE_TIME_S_MASK + OFFSET_MASK */
	public static final int OFFSET_DATE_TIME_S_MASK = FULL_DATE_TIME_S_MASK + OFFSET_MASK;
	/** Combination of HOUR_MASK + MINUTE_MASK + OFFSET_MASK */
	public static final int OFFSET_TIME_HM_MASK = HOUR_MASK + MINUTE_MASK + OFFSET_MASK;
	/** Combination of TIME_S_MASK + OFFSET_MASK */
	public static final int OFFSET_TIME_S_MASK = TIME_S_MASK + OFFSET_MASK;
	/** Combination of TIME_S_MASK + ZONEID_MASK */
	public static final int ZONED_TIME_S_MASK = TIME_S_MASK + ZONEID_MASK;
	/** Combination of TIME_S_MASK + MILLI_MASK + ZONEID_MASK */
	public static final int ZONED_TIME_MS_MASK = TIME_S_MASK +MILLI_MASK + ZONEID_MASK;
	/** Combination of INSTANT_MASK + OFFSET_MASK */
	public static final int OFFSET_INSTANT_MASK = INSTANT_MASK + OFFSET_MASK;
	/** Combination of INSTANT_MASK + ZONEID_MASK */
	public static final int ZONED_INSTANT_MASK = INSTANT_MASK + ZONEID_MASK;
	
	/** All of the time-of-day fields */
	public static final int TIME_FIELDS_MASK = HOUR_MASK + MINUTE_MASK + SECOND_MASK +MILLI_MASK + NANO_MASK;

	/**
	 * The instant is a long value that represents the numb er of milliseconds from the Unix epoch, midnight 1 January 1970.
	 * The instant has priority over other date-time values, and will override them if provided.  The instant may be used in
	 * combination with a time zone ID or offset.
	 */
	public final Long instant;
	/**
	 * The era is an integer indicating the era in which the year is interpreted.  If era is provided, the year is interpreted to be a year within the specified era.
	 * If null, then the year is the "proleptic" year.
	 */
	public final Byte era;
	/**
	 * See "era" for a description of how year is interpreted.
	 */
	public final Integer year;
	/**
	 * Number of the month within the year, the first month is 1.
	 */
	public final Byte month;
	/**
	 * If month is not null, number of the day within the month, the first day is 1.  If month is null, number of day within the year, starting at 1.
	 */
	public final Byte day;
	/**
	 * Number of the hour within the day, 0-23.
	 */
	public final Byte hour;
	/**
	 * Number of the minute within the hour, 0-59.
	 */
	public final Byte minute;
	/**
	 * Number of the second within the minute, 0-59.
	 */
	public final Byte sec;
	/**
	 * Number of nanoseconds within the second, 0-999,999,999.  This value will override millis, and micros.
	 */
	public final Integer nanos;
	/**
	 * Number of seconds the time is offset from UTC.  This value may be overridden by zoneId.
	 */
	public final Integer offsetSeconds;
	/**
	 * ID of the timezone, as per the IANA TZ database. (i.e. "America/New_York") This may override offsetSeconds.
	 */
	public final String zoneId;

	/**
	 * bit mask indicating which fields are set
	 */
	public final int fieldMask;

	/**
	 * Accuracy of seconds field:
	 * <ul><li>9: nanoseconds</li>
	 * <li>3: milliseconds</li>
	 * <li>0: seconds</li>
	 * <li>-1: no seconds specified</li>
	 * </ul>
	 */
	public final int accuracy;

	/**
	 * Create a new TemporalValue by providing all fields.  All fields are nullable.  Refer to the field documentation for more detail.
	 * @param instant Number of millis since the Unix epoch
	 * @param era Calendar era, controls how the year is interpreted. Use null for proleptic year.  CURRENTLY NOT SUPPORTED: VALUE IS ALWAYS NULL.
	 * @param year Year, as determined by the era.  Proleptic year if era == null.
	 * @param month Month of the year, starting at 1
	 * @param day Day of month, starting at 1
	 * @param hour Hour of the day, 0-23
	 * @param minute Minute of the hour, 0-59
	 * @param sec Second of the minute, 0-59
	 * @param millis Millisecond of the minute, 0-59,999
	 * @param nanos Nanosecond of the minute, 0-59,999,999,999
	 * @param offsetSeconds Timezone offset from UTC in seconds
	 * @param zoneId Zone ID as defined by the IANA TZ database.
	 */
	private TemporalVO(Long instant, Byte era, Integer year, Byte month, Byte day, Byte hour, Byte minute, Byte sec, Integer nanos, Integer offsetSeconds, String zoneId) {
		int mask = 0;
		this.instant = instant;
		mask += this.instant != null ? INSTANT_MASK : 0;

		this.era = null;  // @TODO:  support non-current era
		mask += this.era != null ? ERA_MASK : 0;

		this.year = year;
		mask += this.year != null ? YEAR_MASK : 0;

		this.month = month;
		mask += this.month != null ? MONTH_MASK : 0;

		this.day = day;
		mask += this.day != null ? DAY_MASK : 0;

		this.hour = hour != null ? hour : 0;
		mask += (this.hour != null && this.hour != 0) ? HOUR_MASK : 0;

		this.minute = minute != null ? minute : 0;
		mask += (this.minute != null && this.minute != 0) ? MINUTE_MASK : 0;

		this.sec = sec != null ? sec : 0;
		mask += (this.sec != null && this.sec != 0) ? SECOND_MASK : 0;

		this.nanos = nanos != null ? nanos : 0;
		if (this.nanos != null && this.nanos != 0) {
			// if nanos contains fractions less than 1 millisec, apply NANO_MASK, otherwise MILLI_MASK
			mask += ((this.nanos % 1000000) != 0) ? NANO_MASK : MILLI_MASK;
		}

		this.offsetSeconds = offsetSeconds;
		mask += this.offsetSeconds != null ? OFFSET_MASK : 0;

		this.zoneId = zoneId;
		mask += this.zoneId != null ? ZONEID_MASK : 0;

		fieldMask = mask;

		accuracy = accuracy();
	}

	/**
	 * Create an instant without a timezone.  This is interpreted as UTC.
	 * @param instant instant value
	 * @return new TemporalValue instance
	 */
	public static TemporalVO createInstant(long instant) {
		return new TemporalVO(instant, null, null, null, null, null, null, null, null, null, null);
	}

	/**
	 * Create an instant with a UTC offset.  This is a distinct moment in time.
	 * @param instant instant value, UTC
	 * @param offsetSeconds seconds offset from UTC
	 * @return new TemporalValue instance
	 */
	public static TemporalVO createInstantOffset(long instant, int offsetSeconds) {
		return new TemporalVO(instant, null, null, null, null, null, null, null, null, offsetSeconds, null);
	}

	/**
	 * Create an instant with a timezone.This is a distinct moment in time.
	 * @param instant instant value, UTC
	 * @param zoneId IANA TZDB ID
	 * @return new TemporalValue instance
	 */
	public static TemporalVO createInstantZoned(long instant, String zoneId) {
		return new TemporalVO(instant, null, null, null, null, null, null, null, null, null, zoneId);
	}

	/**
	 * Create a date.
	 * @param year Proleptic year (i.e. 1975 or -535)
	 * @param month Month within the year, starting at 1
	 * @param day Day in the month, starting at 1
	 * @return A new TemporalValue instance
	 */
	public static TemporalVO createDate(int year, byte month, byte day) {
		return new TemporalVO(null, null, year, month, day, null, null, null, null, null, null);
	}

	/**
	 * Create a time of day without a timezone (a.k.a. local time)
	 * @param hour Hour of the day, 0-23
	 * @param minute Minute of the hour, 0-59
	 * @param sec Second of the minute, 0-59
	 * @return A new TemporalValue instance
	 */
	public static TemporalVO createTime(byte hour, byte minute, byte sec) {
		return new TemporalVO(null, null, null, null, null, hour, minute, sec, null, null, null);
	}

	/**
	 * Create a time of day with a timezone.
	 * @param hour Hour of the day, 0-23
	 * @param minute Minute of the hour, 0-59
	 * @param sec Second of the minute, 0-59
	 * @param offsetSeconds seconds offset from UTC
	 * @return A new TemporalValue instance
	 */
	public static TemporalVO createTimeOffset(byte hour, byte minute, byte sec, int offsetSeconds) {
		return new TemporalVO(null, null, null, null, null, hour, minute, sec, null, offsetSeconds, null);
	}

	/**
	 * Create a date+time without a timezone (a.k.a. local date-time)
	 * @param year Proleptic year (i.e. 1975 or -535)
	 * @param month Month within the year, starting at 1
	 * @param day Day in the month, starting at 1
	 * @param hour Hour of the day, 0-23
	 * @param minute Minute of the hour, 0-59
	 * @param sec Second of the minute, 0-59
	 * @return A new TemporalValue instance
	 */
	public static TemporalVO createDateTime(int year, byte month, byte day, byte hour, byte minute, byte sec) {
		return new TemporalVO(null, null, year, month, day, hour, minute, sec, null, null, null);
	}

	/**
	 * Create a date+time without a timezone (a.k.a.local date-time)
	 * @param year Proleptic year (i.e. 1975 or -535)
	 * @param month Month within the year, starting at 1
	 * @param day Day in the month, starting at 1
	 * @param hour Hour of the day, 0-23
	 * @param minute Minute of the hour, 0-59
	 * @param sec Second of the minute, 0-59
	 * @param zoneId IANA TZDB name
	 * @return A new TemporalValue instance
	 */
	public static TemporalVO createZonedDateTime(int year, byte month, byte day, byte hour, byte minute, byte sec, String zoneId) {
		return new TemporalVO(null, null, year, month, day, hour, minute, sec, null, null, zoneId);
	}

	/**
	 * Milliseconds in Minute is used as a less verbose alternative to sec+nanos.
	 * It applies only when the value for nanos has a value in the range 1ms..999ms.
	 * When this condition is met, then Integer value of (sec*1000 + nanos/1000000) is returned.
	 * This will be in the range of 1..59,999 which can be represented by an unsigned short.
	 * @return non-zero, positive value or null
	 */
	public Integer getMsInMinute() {
		// only return MS_IN_MINUTE if seconds are in the range 0..59, and there are no fractional ms
		if (sec < 0 || sec >= 60 || (nanos % 1000000 != 0)) {
			return null;
		}
		// only return MS_IN_MINUTE if nanos exceed 1ms
		int ms = nanos / 1000000;
		return ms > 0 && ms < 1000 ? (int) (sec * 1000 + ms) : null;
	}

	/**
	 * If the offsetSeconds is divisible by 15 minute intervals, return the number of intervals.
	 * @return Number of 15 minute intervals offset from UTC
	 */
	public Byte getOffsetIntervals() {
		if (offsetSeconds != null && offsetSeconds != 0 && offsetSeconds % (15 * 60) == 0) {
			return (byte)(offsetSeconds / (15 * 60));
		}
		return (byte)0;
	}
	
	/**
	 * @return true if the instant field is set
	 */
	public boolean isInstant() {
		return instant != null;
	}

	/**
	 * @return true if there is an offset or if there is a zoneId and a date from which to compute an offset
	 */
	public boolean isOffset() {
		return offsetSeconds != null;
	}

	/**
	 * @return true if the zoneId is set
	 */
	public boolean isZoned() {
		return zoneId != null;
	}

	/**
	 * @return true if neither isOffset() nor isZoned() is true
	 */
	public boolean isLocal() {
		return !isOffset() && !isZoned();
	}

	/**
	 * @return true if the year, month, and day are provided
	 */
	public boolean isDate() {
		return (fieldMask & FULL_DATE_MASK) == FULL_DATE_MASK;
	}

	/**
	 * @return true if at least one of the time fields is present
	 */
	public boolean isTime() {
		return (fieldMask & TIME_FIELDS_MASK) != 0;
	}

	/**
	 * @return true if isDate() and isTime()
	 */
	public boolean isDateTime() {
		return isDate() && isTime();
	}
	
	/**
	 * 
	 * @return true if none of the time related fields are set -- defaults to midnight 00:00:00.0
	 */
	public boolean isDefaultTime() {
		return (fieldMask & TIME_FIELDS_MASK) == 0;
	}

	/**
	 * Determine the number of decimal places needed to represent the seconds & subseconds of this VO
	 * @return the precision in terms of decimal places: 0, 3, or 9; -1 means that no seconds are specified
	 */
	private int accuracy() {
		if (nanos != 0) {
			if (nanos % 1000000 == 0) {
				return 3;
			}
			return 9;
		}
		if (isInstant()) {
			return 3;
		}
		return 0;
	}

	/**
	 * Get the value for a given field
	 * @param field field to get
	 * @return Value of the field, may be null
	 */
	public Object get(EnonTemporalField field) {
		switch(field) {
			case INSTANT:
				return instant;
				
			case DAY:
				return day;

			case ERA:
				return era;

			case HOUR:
				return hour;

			case MINUTE:
				return minute;

			case MONTH:
				return month;

			case OFFSET_SECONDS:
				return offsetSeconds;
				
			case OFFSET_INTERVALS:
				return getOffsetIntervals();

			case SECOND:
				return sec;

			case YEAR:
				return year;

			case NANOS:
				return nanos;
				
			case MS_IN_MINUTE:
				return getMsInMinute();

			case ZONE_ID:
				return zoneId;

			default:
				throw new EnonException("No such field: "+field);
		}
	}
	
	public String toIsoString() {
		if (instant != null) {
			if (zoneId == null && (offsetSeconds == null || offsetSeconds == 0)) {
				return Instant.ofEpochMilli(instant).plusNanos(nanos).toString();
			} else if (zoneId != null) {
				return ZonedDateTime.ofInstant(Instant.ofEpochMilli(instant).plusNanos(nanos), ZoneId.of(zoneId)).toString();
			} else if (offsetSeconds != null) {
				return OffsetDateTime.ofInstant(Instant.ofEpochMilli(instant).plusNanos(nanos), ZoneOffset.ofTotalSeconds(offsetSeconds)).toString();
			}
		}
		NumberFormat twoDigits = new DecimalFormat("00");
		NumberFormat nanosFmt = new DecimalFormat("00.000###");
		String date = "";
		if (isDate()) {
			date += year != null ? year.toString() : "";
			date += month != null ? ('-'+twoDigits.format(month)) : "";
			date += day != null ? ('-'+twoDigits.format(day)) : "";
		}
		
		String time = "";
		String zone = "";
		if (isTime()) {
			time += date.isEmpty() ? "" : 'T';
			time += hour != null ? twoDigits.format(hour) : "";
			time += minute != null ? (':'+twoDigits.format(minute)) : "";
			if (nanos == null || nanos == 0) {
				time += sec != null ? (':'+twoDigits.format(sec)) : "";
			} else {
				double dSec = (sec != null ? sec : 0) + (double)nanos / 1000000000;
				time += ':'+nanosFmt.format(dSec);
			}			
		
			zone = "";
			if (zoneId != null) {
				zone = '['+zoneId+']';
			} else if (offsetSeconds != null && offsetSeconds != 0) {
				zone = (offsetSeconds >= 0 ? "+" : "") + twoDigits.format(offsetSeconds / 3600) + ':'+twoDigits.format((Math.abs(offsetSeconds)%3600 / 60));
			}
		}
		return date + (time.isEmpty() ? "" : time + (zone != null ? zone : ""));
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": "
				+ ((instant != null) ? (char)EnonTemporalField.INSTANT.key+String.valueOf(instant) : "")
				+ ((year != null) ? (char)EnonTemporalField.YEAR.key+String.valueOf(year) : "")
				+ ((month != null) ? (char)EnonTemporalField.MONTH.key+String.valueOf(month) : "")
				+ ((day != null) ? (char)EnonTemporalField.DAY.key+String.valueOf(day) : "")
				+ ((hour != null && hour != 0) ? (char)EnonTemporalField.HOUR.key+String.valueOf(hour) : "")
				+ ((minute != null && minute != 0) ? (char)EnonTemporalField.MINUTE.key+String.valueOf(minute) : "")
				+ ((sec != null && sec != 0) ? (char)EnonTemporalField.SECOND.key+String.valueOf(sec) : "")
				+ ((nanos != null && nanos != 0) ? (char)EnonTemporalField.NANOS.key+String.valueOf(nanos) : "")
				+ (getOffsetIntervals() != null ? (char)EnonTemporalField.OFFSET_INTERVALS.key+String.valueOf(getOffsetIntervals()) : "")
				+ ((getOffsetIntervals() == null && offsetSeconds != null && offsetSeconds != 0) ? (char)EnonTemporalField.OFFSET_SECONDS.key+String.valueOf(offsetSeconds) : "")
				+ ((zoneId != null) ? (char)EnonTemporalField.ZONE_ID.key+String.valueOf(zoneId) : "")
				;
	}

	/////////////////////////////////////////////////////////////////////////////////
	/**
	 * A builder used to create TemporalValue instances with field combinations not provided by the create* factory methods.
	 * Values with sub-second accuracy are created with this builder
	 */
	public static class Builder {
		private final Map<EnonTemporalField, Object> map = new HashMap<>();

		/**
		 * Construct a TemporalValue from the previously specified fields.
		 * @return A new TemporalValue instance
		 */
		public TemporalVO build() {
			return new TemporalVO(
					(Long)map.get(EnonTemporalField.INSTANT),
					(Byte)map.get(EnonTemporalField.ERA),
					(Integer)map.get(EnonTemporalField.YEAR),
					(Byte)map.get(EnonTemporalField.MONTH),
					(Byte)map.get(EnonTemporalField.DAY),
					(Byte)map.get(EnonTemporalField.HOUR),
					(Byte)map.get(EnonTemporalField.MINUTE),
					(Byte)map.get(EnonTemporalField.SECOND),
					(Integer)map.get(EnonTemporalField.NANOS),
					(Integer)map.get(EnonTemporalField.OFFSET_SECONDS),
					(String)map.get(EnonTemporalField.ZONE_ID));
		}

		public Builder from(TemporalVO tvo) {
			if (tvo.instant != null) {
				instant(tvo.instant);
			}
			if (tvo.era != null) {
				era(tvo.era);
			}
			if (tvo.year != null) {
				year(tvo.year);
			}
			if (tvo.month != null) {
				month(tvo.month);
			}
			if (tvo.day != null) {
				day(tvo.day);
			}
			if (tvo.hour != null) {
				hour(tvo.hour);
			}
			if (tvo.minute != null) {
				minute(tvo.minute);
			}
			if (tvo.sec != null) {
				second(tvo.sec);
			}
			if (tvo.nanos != null) {
				nanos(tvo.nanos);
			}
			if (tvo.offsetSeconds != null) {
				offsetSeconds(tvo.offsetSeconds);
			}
			if (tvo.zoneId != null ) {
				zoneId(tvo.zoneId);
			}
			return this;
		}

		public Builder date(int year, byte month, byte day) {
			return year(year).month(month).day(day);
		}

		public Builder time(byte hour, byte minute, byte second) {
			return hour(hour).minute(minute).second(second);
		}

		public Builder instant(long instant) {
			map.put(EnonTemporalField.INSTANT, instant);
			return this;
		}

		/**
		 * CURRENTLY NOT SUPPORTED.  VALUES WILL BE IGNORED.  Era will be null, and year will be interpreted as proleptic.
		 * @param era 1 is the current era, 0 is the previous era.
		 * @return This Builder instance
		 */
		public Builder era(byte era) {
			map.put(EnonTemporalField.ERA, era);
			return this;
		}

		public Builder year(int year) {
			map.put(EnonTemporalField.YEAR, year);
			return this;
		}

		public Builder month(byte month) {
			map.put(EnonTemporalField.MONTH, month);
			return this;
		}

		public Builder day(byte day) {
			map.put(EnonTemporalField.DAY, day);
			return this;
		}

		public Builder hour(byte hour) {
			map.put(EnonTemporalField.HOUR, hour);
			return this;
		}

		public Builder minute(byte minute) {
			map.put(EnonTemporalField.MINUTE, minute);
			return this;
		}

		public Builder second(byte second) {
			map.put(EnonTemporalField.SECOND, second);
			return this;
		}

		/**
		 * Set the nanoseconds.  
		 * @param nanos number of nanoseconds
		 * @return this Builder instance
		 */
		public Builder nanos(int nanos) {
			map.put(EnonTemporalField.NANOS, nanos);
			return this;
		}

		public Builder offsetSeconds(int offsetSeconds) {
			map.put(EnonTemporalField.OFFSET_SECONDS, offsetSeconds);
			return this;
		}

		public Builder zoneId(String zoneId) {
			map.put(EnonTemporalField.ZONE_ID, zoneId);
			return this;
		}
	}
}
