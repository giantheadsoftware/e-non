/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property.pojo;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.enon.bind.property.ReadAccessor;
import org.enon.bind.property.ReadableProperty;
import org.enon.bind.property.WritableProperty;
import org.enon.bind.property.WriteAccessor;
import org.enon.bind.value.NativeValueFactory;
import org.enon.cache.EnonCache;
import org.enon.cache.EnonCacheFactory;

/**
 * Search for public properties in the POJO class. A public property is one that has both a public getter and a public setter.
 *
 * @author clininger $Id: $
 */
public class PublicPojoPropertyAccessor implements ReadAccessor, WriteAccessor {

	private final NativeValueFactory nativeValueFactory;
	private final EnonCache<String, ? super Map<String, PublicPojoProperty>> cache;

	public PublicPojoPropertyAccessor(NativeValueFactory nativeValueFactory) {
		this.nativeValueFactory = nativeValueFactory;
		cache = EnonCacheFactory.getInstance().create(UUID.randomUUID().toString(), String.class, Map.class);
	}

	@Override
	public Map<String, ? extends ReadableProperty> getReadableProperties(Class pojoClass) {
		return getProperties(pojoClass);
	}

	@Override
	public Map<String, ? extends WritableProperty> getwritableProperties(Class pojoClass) {
		return getProperties(pojoClass);
	}

	public Map<String, PublicPojoProperty> getProperties(Class pojoClass) {
		String className = pojoClass.getName();
		Map<String, PublicPojoProperty> props = (Map<String, PublicPojoProperty>) cache.get(className);
		if (props == null) {
			props = locatePublicProperties(pojoClass);
			synchronized (cache) {
				cache.put(className, props);
			}
		}
		return props;
	}

	/**
	 * Identify the methods on the class that conform to getter conventions
	 *
	 * @param c Class to scan for getters
	 * @return list of public getter property
	 */
	private Map<String, PublicPojoProperty> locatePublicProperties(Class c) {
		Method[] methods = c.getMethods();
		MethodPairing pairing = new MethodPairing();
		Map<String, PublicPojoProperty> props = Arrays.stream(methods)
				.map(pairing::pair)
				.filter(pp -> pp != null)
				.collect(Collectors.toMap(PublicPojoProperty::getName, Function.identity()));
		return props;
	}

  ////////////////////////////////////////////////////////////////////////////////////////////////
	private class MethodPairing {
		private final Map<String, Method> getters = new HashMap<>();
		private final Map<String, Method> setters = new HashMap<>();
		private final PojoPropertyUtil util = new PojoPropertyUtil();

		/**
		 * Store getter and setter methods until a matching pair is found.  When a pair is found, create a PublicPojoProperty for the getter/setter pair.
		 * @param method
		 * @return a PublicPojoProperty if the method is a getter or setter and has a matching setter or getter.
		 */
		public PublicPojoProperty pair(Method method) {
			// test whether this method could be a getter
			String propertyId = util.getterName(method);
			if (propertyId != null) {
				// looks like a getter - is there ea matching setter?
				Method setter = setters.get(propertyId);
				if (setter != null) {
					// found a setter, create the property
					return buildProperty(propertyId, method, setter);
				}
				// didn't find a matching setter; cache the getter
				getters.put(propertyId, method);
			} else {
				// method was not a getter candidate, maybe it's a setter
				propertyId = util.setterName(method);
				if (propertyId != null) {
					// looks like a getter - is there ea matching setter?
					Method getter = getters.get(propertyId);
					if (getter != null) {
					// found a getter, create the property
					return buildProperty(propertyId, getter, method);
					}
					// didn't find a matching setter; cache the getter
					setters.put(propertyId, method);
				}
			}
			return null;
		}

		/**
		 * Build a property based on the getter. If a matching setter is found then a property can be created.
		 *
		 * @param getter A conventional POJO getter method
		 * @return A property instance if a matching setter is found, otherwise null
		 */
		private PublicPojoProperty buildProperty(String propertyId, Method getter, Method setter) {
			return new PublicPojoProperty(propertyId.substring(0, propertyId.indexOf("::")), getter, setter, nativeValueFactory);
		}
	}
}
