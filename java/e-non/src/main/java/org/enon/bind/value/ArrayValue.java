/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.codec.DataCategory;

/**
 *
 * @author clininger $Id: $
 */
public class ArrayValue extends AbstractValue {

	private ArrayValue(Class type, Object value) {
		super(DataCategory.PRIMITIVE_ARRAY, type, value);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + value.getClass().getComponentType().getSimpleName() + "]";
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	public static class Acceptor extends ValueTypeAcceptor<ArrayValue> {
		private final boolean isX;
		
		public Acceptor(EnonConfig config) {
			super(config);
			isX = config.hasFeature(FeatureSet.X);
		}

		@Override
		public AcceptLevel accept(Class t) {
			if (isX && t != null && t.isArray() && t.getComponentType().isPrimitive())  {
				if (t == char[].class) {
					// char is not considered "primitive" because it has variable length in unicode
					return AcceptLevel.NEVER;
				}
				// can handle byte[], but should defer to BlobValue
				return t == byte[].class ? AcceptLevel.IF_NECESSARY : AcceptLevel.PREFER;
			}
			return AcceptLevel.NEVER;
		}

		@Override
		public ArrayValue toNativeValue(Class type, Object value) {
			return new ArrayValue(type, value);
		}

	}

}
