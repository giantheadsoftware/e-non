/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.codec.DataCategory;

/**
 *
 * @author clininger $Id: $
 */
public class BlobValue extends AbstractValue<byte[]> {

	private BlobValue(byte[] value) {
		super(DataCategory.BYTES, byte[].class, value);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + value.length + "]";
	}

	public static class Acceptor extends ValueTypeAcceptor<BlobValue> {
		private static final Map<Class, AcceptLevel> ACCEPTS = new HashMap<>();
		static {
			ACCEPTS.put(byte[].class, AcceptLevel.PREFER);
			ACCEPTS.put(char[].class, AcceptLevel.PREFER);
		}

		public Acceptor(EnonConfig config) {
			super(config);
		}

		@Override
		protected Map<Class, AcceptLevel> acceptsClasses() {
			return ACCEPTS;
		}

		@Override
		public BlobValue toNativeValue(Class type, Object value) {
			if (type == char[].class) {
				// have to decode char[] into byte[]
				ByteBuffer buf = StandardCharsets.UTF_8.encode(CharBuffer.wrap((char[])value));
				value = new byte[buf.limit()];
				buf.get((byte[])value);
			}
			return new BlobValue((byte[]) value);
		}

	}
}
