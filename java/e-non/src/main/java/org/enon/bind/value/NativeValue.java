/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import org.enon.codec.DataCategory;

/**
 * The NativeValue is a wrapper around some native type.
 *
 * <strong>The NativeValue is the bridge between app-specific types and the limited types supported by eNON.</strong> The valueType and value exposed by this interface needs to be
 * something that the eNON codecs understand, no matter what the wrapped Java object may be.
 * <br>
 * For example, a POJO would probably have its properties exposed in map form.
 * <br>
 * NativeValue implementations are provided for commonly used Java types.
 * <br>
 * The term "native" here means that it is a Java type as opposed to a platform-independent eNON type. NativeValue can wrap any Java thing: primitive values or classes extending
 * Object -- everything.
 *
 * @author clininger $Id: $
 */
public interface NativeValue<T> {

	/**
	 * @return the DataCategory of the contained value to be used by the Coder to interpret the value
	 */
	DataCategory getCategory();

	/**
	 * @return the value of this NativeValue instance
	 */
	T getValue();

	/**
	 * @return the declared type of the contained native data, which may be a super class
	 */
	Class<? super T> getDeclaredType();
}
