/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property;

import java.lang.reflect.Type;

/**
 *
 * @author clininger $Id: $
 */
public class ConstructorProperty {
	public final String name;
	public final Type type;

	public ConstructorProperty(String name, Type type) {
		this.name = name;
		this.type = type;
	}
	
	/**
	 * @return The name of the property
	 */
	public String getName() {
		return name;
	}
	
	public Type getPropertyType() {
		return type;
	}

}
