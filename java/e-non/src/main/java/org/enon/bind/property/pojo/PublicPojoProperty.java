/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property.pojo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import org.enon.bind.property.ReadableProperty;
import org.enon.bind.property.WritableProperty;
import org.enon.bind.value.NativeValueFactory;
import org.enon.bind.value.PojoPropertyValue;
import org.enon.exception.EnonBindException;
import org.enon.bind.value.NativeValue;

/**
 * Contains the getter method for the property
 */
public class PublicPojoProperty implements ReadableProperty, WritableProperty {

	private final String name;
	private final Method getter;
	private final Method setter;
	private Type propertyType;
	private final NativeValueFactory nativeValueFactory;

	/**
	 * Create a new immutable instance.  Make sure the return type of the getter matches the arg type of the setter!
	 * @param propertyName The name of the property
	 * @param getter getter method for the property
	 * @param settersetter method for the property
	 * @param nvf NativeValueFactory instance that will be used to wrap the value after reading
	 */
	PublicPojoProperty(String propertyName, Method getter, Method setter, NativeValueFactory nvf) {
		name = propertyName;
		this.getter = getter;
		this.setter = setter;
		nativeValueFactory = nvf;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Type getPropertyType() {
		if (propertyType == null) {
		propertyType = getter.getGenericReturnType();
		}
		return propertyType;
	}

	@Override
	public PojoPropertyValue readValue(Object fromInstance) {
		try {
			Object value = getter.invoke(fromInstance);
			NativeValue nativeValue = nativeValueFactory.nativeValue(value);
			return new PojoPropertyValue(name, nativeValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			throw new EnonBindException("Could not read property: "+getter.getDeclaringClass().getName()+"."+getter.getName(), ex);
		}
	}

	@Override
	public void writeValue(Object toInstance, Object value) {
		try {
			setter.invoke(toInstance, value);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			throw new EnonBindException("Could not write property: "+setter.getDeclaringClass().getName()+"."+setter.getName(), ex);
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[name=" + name + "]";
	}
}
