/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property;

import java.lang.reflect.Field;
import org.enon.bind.value.NativeValue;
import org.enon.bind.value.NativeValueFactory;
import org.enon.bind.value.PojoPropertyValue;
import org.enon.exception.EnonBindException;

/**
 *
 * @author clininger $Id: $
 */
public class PublicFinalFieldProperty implements ReadableProperty {
	private final Field field;
	private final NativeValueFactory nativeValueFactory;

	public PublicFinalFieldProperty(Field field, NativeValueFactory nativeValueFactory) {
		this.field = field;
		this.nativeValueFactory = nativeValueFactory;
	}

	@Override
	public String getName() {
		return field.getName();
	}

	@Override
	public PojoPropertyValue readValue(Object fromInstance) {
		try {
			NativeValue nativeValue = nativeValueFactory.nativeValue(field.get(fromInstance));
			return new PojoPropertyValue(getName(), nativeValue);
		} catch (IllegalAccessException | IllegalArgumentException ex) {
			throw new EnonBindException("Could not read property: "+field.getDeclaringClass().getName()+"."+field.getName(), ex);
		}
	}

}
