/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.beans.ConstructorProperties;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.enon.EnonConfig;
import org.enon.bind.property.ConstructorProperty;
import org.enon.bind.property.ReadableProperty;
import org.enon.exception.EnonBindException;
import org.enon.exception.EnonException;
import org.enon.util.ReflectUtil;

/**
 *
 * @author clininger $Id: $
 */
public class ImmutableValue extends MapValue<StringValue, NativeValue> {
	private final static Map<String, Map<String, ReadableProperty>> READABLE_PROPS_BY_CLASSNAME = new HashMap<>();
	private final ReflectUtil reflectUtil = ReflectUtil.getinstance();

	protected final Object immu;
	protected Map<StringValue, NativeValue> nvMap;

	private ImmutableValue(Object immu, EnonConfig config) {
		super(null, config);
		this.immu = immu;
	}

	public Object getPojo() {
		return immu;
	}

	/**
	 * Return the property names &amp; values as a map. This value is populated lazily and the result is cached in case it is accessed again.
	 *
	 * @return A map of the PojoPropertyValues, as name &amp; value pairs
	 */
	@Override
	public Map<StringValue, NativeValue> getValue() {
		if (nvMap != null) {
			return nvMap;
		}
		// figure out what properties are available in the constructor
		Map<String, ReadableProperty> readers = READABLE_PROPS_BY_CLASSNAME.get(immu.getClass().getName());
		if (readers == null) {
			String[] propNames = null;
			Type[] types = null;
			for (Constructor con : reflectUtil.classOfType(immu.getClass()).getConstructors()) {
				Annotation[] annos = con.getDeclaredAnnotationsByType(ConstructorProperties.class);
				if (annos.length > 0) {
					propNames = ((ConstructorProperties)annos[0]).value();
					types = con.getGenericParameterTypes();
				}
			}
			if (propNames == null || types == null) {
				throw new EnonBindException("Couldn't locate constructor properties of class "+immu.getClass().getName());
			}
			Map<String, ConstructorProperty> properties = new HashMap<>(Math.min(propNames.length, types.length));

			ReflectUtil rUtil = ReflectUtil.getinstance();

			for (int i = 0; i < propNames.length; i++) {
				if (types.length > i) {
					properties.put(propNames[i], new ConstructorProperty(propNames[i], rUtil.classOfType(types[i])));
				}
			}
			readers = config.getPropertyAccessorFactory().getPublicFinalFieldReadAccessor().getReadableProperties(immu.getClass()).entrySet().stream()
					.filter(entry -> properties.containsKey(entry.getKey()))
					.collect(Collectors.toMap(ppv -> ppv.getKey(), ppv -> ppv.getValue()));
			readers.putAll(config.getPropertyAccessorFactory().getPublicGetterReadAccessor().getReadableProperties(immu.getClass()).entrySet().stream()
					.filter(entry -> properties.containsKey(entry.getKey()))
					.collect(Collectors.toMap(ppv -> ppv.getKey(), ppv -> ppv.getValue()))
			);
			// cache the result by classname
			READABLE_PROPS_BY_CLASSNAME.put(immu.getClass().getName(), readers);
		}
		nvMap = readers.entrySet().stream()
				.map(entry -> entry.getValue())
				.map(reader -> reader.readValue(immu))
				.collect(Collectors.toMap(ppv -> ppv.getKey(), ppv -> ppv.getValue()));
		return nvMap;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + immu.getClass().getName() + "]";
	}

	/**
	 * Acceptor for PojoValue
	 */
	public static class Acceptor extends ValueTypeAcceptor<ImmutableValue> {

		public Acceptor(EnonConfig config) {
			super(config);
		}

		@Override
		public AcceptLevel accept(Class t) {
			if (t == null) {
				return AcceptLevel.NEVER;
			}
			try {
				Class c = ReflectUtil.getinstance().classOfType(t); // classOfType will throw if type can't resolve to a class
				int modifiers = c.getModifiers();
				// can't instantiate abstract or non-public classes
				if (Modifier.isAbstract(modifiers) ||  !Modifier.isPublic(modifiers))
					return AcceptLevel.NEVER;
				// need to have a constructor with @ConstructorProperties annotation
				for (Constructor con : c.getConstructors()) {
					if (con.getDeclaredAnnotationsByType(ConstructorProperties.class).length > 0) {
						return AcceptLevel.PREFER;
					}
				}
				return AcceptLevel.NEVER;
			} catch (EnonException x) {
				return AcceptLevel.NEVER;
			}
		}

		@Override
		public ImmutableValue toNativeValue(Class type, Object value) {
			return new ImmutableValue(value, config);
		}

	}
}
