/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.HashMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.codec.DataCategory;

/**
 *
 * @author clininger $Id: $
 */
public class StringValue extends ScalarValue<String> {

	public StringValue(String value) {
		super(DataCategory.STRING, String.class, value);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + String.valueOf(getValue()) + "]";
	}

	public static class Acceptor extends ValueTypeAcceptor<StringValue> {
		
		private static final Map<Class, AcceptLevel> ACCEPTS = new HashMap<>();
		static {
			ACCEPTS.put(String.class, AcceptLevel.PREFER);
			ACCEPTS.put(char.class, AcceptLevel.PREFER);
			ACCEPTS.put(Character.class, AcceptLevel.PREFER);
			ACCEPTS.put(Enum.class, AcceptLevel.PREFER);
		}

		public Acceptor(EnonConfig config) {
			super(config);
		}

		@Override
		protected Map<Class, AcceptLevel> acceptsClasses() {
			return ACCEPTS;
		}

//		@Override
//		public AcceptLevel accept(Class t) {
//			if (t != null) {
//				if (String.class == t || String.class.isAssignableFrom(t) || ) {
//					// accept anything that is a String
//					return AcceptLevel.PREFER;
//				}
//				if (t.isArray()) {
//					// accept char[] or Character[]
//					Class componentType = t.getComponentType();
//					if (componentType == char.class || componentType == Character.class) {
//						return AcceptLevel.PREFER;
//					}
//				}
//			}
//			return AcceptLevel.NEVER;
//		}

		@Override
		public StringValue toNativeValue(Class t, Object u) {
			return new StringValue(Enum.class.isAssignableFrom(u.getClass()) ? ((Enum)u).name() : String.valueOf(u));
		}

	}
}
