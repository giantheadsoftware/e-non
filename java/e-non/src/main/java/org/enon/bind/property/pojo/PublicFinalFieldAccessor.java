/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property.pojo;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.enon.bind.property.PublicFinalFieldProperty;
import org.enon.bind.property.ReadAccessor;
import org.enon.bind.property.ReadableProperty;
import org.enon.bind.value.NativeValueFactory;
import org.enon.cache.EnonCache;
import org.enon.cache.EnonCacheFactory;

/**
 * Scan a class for fields that are public and final.
 * @author clininger $Id: $
 */
public class PublicFinalFieldAccessor implements ReadAccessor {

	private final NativeValueFactory nativeValueFactory;
	private final EnonCache<String, ? super Map<String, ReadableProperty>> cache;

	public PublicFinalFieldAccessor(NativeValueFactory nativeValueFactory) {
		this.nativeValueFactory = nativeValueFactory;
		cache = EnonCacheFactory.getInstance().create(UUID.randomUUID().toString(), String.class, Map.class);
	}

	@Override
	public Map<String, ? extends ReadableProperty> getReadableProperties(Class fieldClass) {
		String className = fieldClass.getName();
		Map<String, ReadableProperty> props = (Map<String, ReadableProperty>) cache.get(className);
		if (props == null) {
			props = locateReadableProperties(fieldClass);
			synchronized (cache) {
				cache.put(className, props);
			}
		}
		return props;
	}

	/**
	 * Identify the public final fields.
	 *
	 * @param c Class to scan for fields
	 * @return list of public final fields
	 */
	private Map<String, ReadableProperty> locateReadableProperties(Class c) {
		Field[] fields = c.getFields();
		return  Arrays.stream(fields)
				.map(this::readablePropertyFor)
				.filter(pff -> pff != null)
				.collect(Collectors.toMap(ReadableProperty::getName, Function.identity()));
	}
	
	private ReadableProperty readablePropertyFor(final Field field) {
		int mods = field.getModifiers();
		if (Modifier.isFinal(mods) && Modifier.isPublic(mods)) {
			return new PublicFinalFieldProperty(field, nativeValueFactory);
		}
		return null;
	}
}
