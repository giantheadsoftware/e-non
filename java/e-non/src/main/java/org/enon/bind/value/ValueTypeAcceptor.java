/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.Collections;
import java.util.Map;
import org.enon.EnonConfig;

/**
 * A ValueTypeAcceptor is used to determine what types are acceptable to a NativeValue type. For example, a NativeValue implementation that handles primitive types will return true
 * when a primitive class is passed to the Predicate function. The ValueTypeAcceptor also serves as a factory for a NativeValue. An instance is returned by the BiFunction.
 *
 * A ValueTypeAccessor implementation is associated with a single NativeValue class. ValueTypeAccessor implementations are inner classes within the NativeValue classes that they
 * support.
 *
 * @author clininger $Id: $
 * @param <T> Specific NativeValue type produced by this Acceptor
 */
public abstract class ValueTypeAcceptor<T extends NativeValue> {

	/**
	 * Levels of "acceptance" ordered from most preferred to least preferred
	 */
	public enum AcceptLevel {
		/** prefer to accept */
		PREFER,
		/** can accept if no other prefers */
		IF_NECESSARY,
		/** can handle what no other will accept */
		LAST_RESORT,
		/** can't accept */
		NEVER
	}
	
	protected final EnonConfig config;

	/**
	 * Constructor
	 * @param config The current config, which can be used to determine what types can be accepted.
	 */
	protected ValueTypeAcceptor(EnonConfig config) {
		this.config = config;
	}

	/**
	 * Return the ACCEPT_LEVEL of this Acceptor for the Class provided.
	 * The default implementation:
	 * <ol>
	 * <li>Never accepts null.  At least one subclass needs to accept null values.</li>
	 * <li>Gets the ACCEPT_LEVELs from the subclass using acceptsClasses()</li>
	 * <li>If the class <b>t</b> is contained in acceptsClasses(), return that level</li>
	 * <li>If the class <b>t</b> is a subclass of anything in acceptsClasses(), return <code>IF_NECESARY</code></li>
	 * </ol>
	 *
	 * @param t Class to test
	 * @return The level at which the NativeValue prefers to handle the class
	 */
	public AcceptLevel accept(Class t) {
		if (t == null) {
			// generally, we dont' accept null.  At least 1 subclass needs to override this method to handle null.
			return AcceptLevel.NEVER;
		}
		Map<Class, AcceptLevel> classes = acceptsClasses();
		AcceptLevel accepts = classes.get(t);
		if (accepts != null) {
			// if the class is explicitly entered in the acceptsClasses() map, return that level
			return accepts;
		}
		// if the class is not specifically listed, see if t is a subclass of anything that was listed
		for (Class c : classes.keySet()) {
			if (c.isAssignableFrom(t)) {
				// t is a subclass, we can accept it "if necessary"
				return AcceptLevel.IF_NECESSARY;
			}
		}
		return AcceptLevel.NEVER;
	}

	/**
	 * Default implementation returns the empty set.
	 *
	 * @return The set of classes that can be handled by this instance.
	 */
	protected Map<Class, AcceptLevel> acceptsClasses() {
		return Collections.EMPTY_MAP;
	}

	/**
	 * Create an instance of NativeValue to wrap the object passed in.
	 *
	 * @param t The declared type of the object (i.e. what a method producing the object is declared to return)
	 * @param u The actual object containing the native value
	 * @return A new NativeValue instance that wraps the object.
	 */
	public abstract T toNativeValue(Class t, Object u);

}
