/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property.pojo;

import java.lang.reflect.Method;
import org.enon.exception.EnonException;

/**
 *
 * @author clininger $Id: $
 */
public class PojoPropertyUtil {

	/**
	 * Test method to match getter convention:
	 * <ul>
	 * <li> method has no args
	 * <li> method starts with "get" followed by a capital letter -or- method returns boolean and starts with "is" followed by a capital letter
	 * </ul>
	 *
	 * @param method The method to test as a getter
	 * @return String "property_name::return_type" if this is a getter candidate, null if not
	 */
	public String getterName(Method method) {
		if (method.getParameterCount() == 0) {
			String methodName = method.getName();
			if (methodName.startsWith("get") && methodName.length() > 3 && Character.isUpperCase(methodName.charAt(3))) {
				return propertyName(method) + "::" + method.getReturnType().getName();
			}
			if (methodName.startsWith("is") && method.getReturnType() == boolean.class
					&& methodName.length() > 2 && Character.isUpperCase(methodName.charAt(2))) {
				return propertyName(method) + "::" + method.getReturnType().getName();
			}
		}
		return null;
	}

	/**
	 * Test method to match getter convention:
	 * <ul>
	 * <li> method has 1 arg
	 * <li> method starts with "set" followed by a capital letter
	 * </ul>
	 *
	 * @param method The method to test as a setter
	 * @return String "property_name::arg_type" if this is a setter candidate, null if not
	 */
	public String setterName(Method method) {
		if (method.getParameterCount() == 1) {
			String methodName = method.getName();
			if (methodName.startsWith("set") && methodName.length() > 3 && Character.isUpperCase(methodName.charAt(3))) {
				return propertyName(method) + "::" + method.getParameterTypes()[0].getName();
			}
		}
		return null;
	}

	/**
	 * Determine a property name using the standard camel-case property naming conventions. Getters start either with "get" or with "is" if the property is boolean (primitive, not
	 * Boolean)
	 *
	 * @param method The getter method to extract the name from
	 * @return the property name, if valid. The name will be camel case, matching the getter method declaration starting with a lower case char.
	 */
	public String propertyName(Method method) {
		String methodName = method.getName();
		if (methodName.startsWith("get") || methodName.startsWith("set")) {
			return Character.toLowerCase(methodName.charAt(3)) + methodName.substring(4);
		}
		if (methodName.startsWith("is")) {
			return Character.toLowerCase(methodName.charAt(2)) + methodName.substring(3);
		}
		throw new EnonException("Invalid property getter method name: " + methodName);
	}

}
