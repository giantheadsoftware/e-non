/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.temporal;

import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author clininger $Id: $
 */
public class TemporalMap {
	public static final byte TERMINATOR =(byte)'}';

	private final Map<EnonTemporalField, Object> temporalFields;

	public TemporalMap(Map<EnonTemporalField, Object> temporalFields, ZoneId zoneId) {
		this.temporalFields = temporalFields;
	}

	public Object get(EnonTemporalField field) {
		return temporalFields.get(field);
	}

	/**
	 * Get the map field as a Long.
	 * @param field Field to return as Long value
	 * @return the Long value of the field, or null if doesn't exist
	 * @throws ClassCastException if the value is not a Number
	 */
	public Long getLong(EnonTemporalField field) {
		return ((Number)temporalFields.get(field)).longValue();
	}

	/**
	 * Get the map field as a String.
	 * @param field the field
	 * @return the Long value of the field, or null if doesn't exist
	 * @throws ClassCastException if the value is not Long
	 */
	public String getString(EnonTemporalField field) {
		Object value = temporalFields.get(field);
		return value != null ? String.valueOf(value) : null;
	}

	public Set<EnonTemporalField> keySet() {
		return temporalFields.keySet();
	}

	/**
	 * Return all the data in this TemporalMap, including the timezone ID, if not null.
	 * @return Map of all values
	 */
	public Map<EnonTemporalField, Object> asMap() {
		Map<EnonTemporalField, Object> result = new HashMap<>();
		result.putAll(temporalFields);

		return result;
	}
}
