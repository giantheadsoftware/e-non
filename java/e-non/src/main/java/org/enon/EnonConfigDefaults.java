/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import org.enon.bind.value.DefaultNativeValueFactory;
import org.enon.bind.value.NativeValueFactory;
import org.enon.bind.property.DefaultPropertyAccessorFactory;
import org.enon.codec.coder.CoderFactory;
import org.enon.codec.coder.DefaultCoderFactory;
import org.enon.codec.decoder.DecoderFactory;
import org.enon.codec.decoder.DefaultDecoderFactory;
import org.enon.bind.property.PropertyAccessorFactory;
import org.enon.element.reader.BinaryElementReaderFactory;
import org.enon.element.reader.ElementReaderFactory;
import org.enon.element.writer.DefaultElementWriterFactory;
import org.enon.element.writer.ElementWriterFactory;

/**
 * This interface defines the defaults that will be used by the EnonConfig unless overridden in the EnonConfig.Builder. This interface can be extended and installed by calling
 * EnonConfig.Builder.installDefaults();
 *
 * @author clininger $Id: $
 */
public interface EnonConfigDefaults {

	default Set<FeatureSet> features() {
		return EnumSet.noneOf(FeatureSet.class);
	}

	default Class<? extends PropertyAccessorFactory> accessorFactoryImpl() {
		return DefaultPropertyAccessorFactory.class;
	}

	default Class<? extends NativeValueFactory> valueFactoryImpl() {
		return DefaultNativeValueFactory.class;
	}

	default Class<? extends CoderFactory> coderFactoryImpl() {
		return DefaultCoderFactory.class;
	}

	default Class<? extends DecoderFactory> decoderFactoryImpl() {
		return DefaultDecoderFactory.class;
	}

	default Class<? extends ElementWriterFactory> elementWriterFactoryImpl() {
		return DefaultElementWriterFactory.class;
	}

	default Class<? extends ElementReaderFactory> elementReaderFactoryImpl() {
		return BinaryElementReaderFactory.class;
	}

	default Map<LimitParam, Long> limits() {
		return Collections.EMPTY_MAP;
	}

	default short minReadVersion() {
		return EnonConfig.FORMAT_VERSION;
	}
}
