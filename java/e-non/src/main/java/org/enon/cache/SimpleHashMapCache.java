/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author clininger $Id: $
 * @param <K> Key type
 * @param <V> Value type
 */
public class SimpleHashMapCache<K, V> implements EnonCache<K, V> {

	private Map<K, V> cache = new HashMap<>();
	private final String name;

	public SimpleHashMapCache(String name) {
		this.name = name;
	}

	@Override
	public V get(K k) {
		return cache.get(k);
	}

	@Override
	public Map<K, V> getAll(Set<? extends K> set) {
		Map<K, V> result = new HashMap<>();
		for (K k : set) {
			if (cache.containsKey(k)) {
				result.put(k, cache.get(k));
			}
		}
		return result;
	}

	@Override
	public boolean containsKey(K k) {
		return cache.containsKey(k);
	}

	@Override
	public void put(K k, V v) {
		if (k == null || v == null) {
			throw new NullPointerException("Cache key and value may not be null");
		}
		cache.put(k, v);
	}

	@Override
	public V getAndPut(K k, V v) {
		if (k == null || v == null) {
			throw new NullPointerException("Cache key and value may not be null");
		}
		return cache.put(k, v);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> map) {
		if (map.containsKey(null) || map.containsValue(null)) {
			throw new NullPointerException("Cache key and value may not be null");
		}
		cache.putAll(map);
	}

	@Override
	public boolean putIfAbsent(K k, V v) {
		if (!cache.containsKey(k)) {
			cache.put(k, v);
			return true;
		}
		return false;
	}

	@Override
	public boolean remove(K k) {
		return cache.remove(k) != null;
	}

	@Override
	public boolean remove(K k, V v) {
		if (contains(k, v)) {
			return remove(k);
		}
		return false;
	}

	public boolean contains(K k, V v) {
		V value = cache.get(k);
		return value == v;  // use reference equivalence rather than value equivalence
	}

	@Override
	public V getAndRemove(K k) {
		return cache.remove(k);
	}

	@Override
	public boolean replace(K k, V v, V v1) {
		if (contains(k, v)) {
			cache.put(k, v1);
			return true;
		}
		return false;
	}

	@Override
	public boolean replace(K k, V v) {
		if (cache.containsKey(k)) {
			cache.put(k, v);
			return true;
		}
		return false;
	}

	@Override
	public V getAndReplace(K k, V v) {
		if (cache.containsKey(k)) {
			return cache.put(k, v);
		}
		return null;
	}

	@Override
	public void removeAll(Set<? extends K> set) {
		for (K k : set) {
			cache.remove(k);
		}
	}

	@Override
	public void removeAll() {
		cache.clear();
	}

	@Override
	public void clear() {
		cache.clear();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public EnonCacheManager getCacheManager() {
		return null;
	}

	@Override
	public void close() {
		if (cache != null) {
			cache.clear();
		}
		cache = null;
	}

	@Override
	public boolean isClosed() {
		return cache == null;
	}

	@Override
	public <T> T unwrap(Class<T> type) {
		throw new IllegalArgumentException("not supported");
	}

	@Override
	public Iterator<Map.Entry<K, V>> iterator() {
		Set<Map.Entry<K, V>> entries = new LinkedHashSet<>();
		for (Map.Entry<K, V> entry : cache.entrySet()) {
			entries.add(new CacheEntry<>(entry));
		}
		return entries.iterator();
	}

	private static class CacheEntry<K, V> implements Map.Entry<K, V> {

		private final Map.Entry<K, V> entry;

		public CacheEntry(Map.Entry<K, V> entry) {
			this.entry = entry;
		}

		@Override
		public K getKey() {
			return entry.getKey();
		}

		@Override
		public V getValue() {
			return entry.getValue();
		}

		@Override
		public V setValue(V value) {
			return entry.setValue(value);
		}

	}
}
