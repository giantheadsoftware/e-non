/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.cache;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A no-op cache stub. Nothing is ever cached, but the calls will not fail.
 *
 * @author clininger $Id: $
 * @param <K> Key type
 * @param <V> Value type
 */
public class EmptyCache<K, V> implements EnonCache<K, V> {

	public static final EmptyCache INSTANCE = new EmptyCache(EmptyCache.class.getName());;

	private final String name;
	private boolean closed;

	public EmptyCache(String name) {
		this.name = name;
	}

	@Override
	public V get(K arg0) {
		return null;
	}

	@Override
	public Map<K, V> getAll(Set<? extends K> arg0) {
		return new HashMap<>();
	}

	@Override
	public boolean containsKey(K arg0) {
		return false;
	}

	@Override
	public void put(K arg0, V arg1) {
		// no-op
	}

	@Override
	public V getAndPut(K arg0, V arg1) {
		return null;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> arg0) {
		// no-op
	}

	@Override
	public boolean putIfAbsent(K arg0, V arg1) {
		return true;
	}

	@Override
	public boolean remove(K arg0) {
		return false;
	}

	@Override
	public boolean remove(K arg0, V arg1) {
		return false;
	}

	@Override
	public V getAndRemove(K arg0) {
		return null;
	}

	@Override
	public boolean replace(K arg0, V arg1, V arg2) {
		return false;
	}

	@Override
	public boolean replace(K arg0, V arg1) {
		return false;
	}

	@Override
	public V getAndReplace(K arg0, V arg1) {
		return null;
	}

	@Override
	public void removeAll(Set<? extends K> arg0) {
		// no-op
	}

	@Override
	public void removeAll() {
		// no-op
	}

	@Override
	public void clear() {
		// no-op
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public EnonCacheManager getCacheManager() {
		return null;
	}

	@Override
	public void close() {
		closed = true;
	}

	@Override
	public boolean isClosed() {
		return closed;
	}

	@Override
	public <T> T unwrap(Class<T> arg0) {
		throw new IllegalArgumentException("not supported");
	}

	@Override
	public Iterator<Map.Entry<K, V>> iterator() {
		return new HashSet<Map.Entry<K, V>>(0).iterator();
	}

}
