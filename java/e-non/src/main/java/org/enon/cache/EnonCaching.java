/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.cache;

import org.enon.exception.EnonCacheException;

/**
 *
 * @author clininger $Id: $
 */
public class EnonCaching {
	public static final String CACHING_PROVIDER_PROPERTY = "enon.caching.provider";
	
	private static EnonCachingProvider defaultProvider;
	
	public static EnonCachingProvider getCachingProvider() {
		String providerClassName = System.getProperty(CACHING_PROVIDER_PROPERTY);
		if (providerClassName == null) {
			return null;
		}
		try {
			Class<? extends EnonCachingProvider> providerClass = (Class<? extends EnonCachingProvider>) Class.forName(providerClassName);
			return providerClass.newInstance();
		} catch (ClassNotFoundException x) {
			return getDefaultProvider();
		} catch (IllegalAccessException | InstantiationException x) {
			throw new EnonCacheException("Unable to load specified caching provider "+providerClassName, x);
		}
	}
	
	public static EnonCachingProvider getDefaultProvider() {
		if (defaultProvider == null) {
			defaultProvider = new EnonDefaultCachingProvider();
		}
		return defaultProvider;
	}
}
