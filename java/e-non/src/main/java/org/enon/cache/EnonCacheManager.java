/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.cache;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author clininger $Id: $
 */
public class EnonCacheManager {
	private final Map<String, EnonCache> cachesByName = new HashMap<>();
	
	private Properties properties;
	
	

	public EnonCachingProvider getCachingProvider() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public URI getURI() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public ClassLoader getClassLoader() {
		return this.getClass().getClassLoader();
	}

	public Properties getProperties() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public <K, V> EnonCache<String, Object> createCache(String cacheName) throws IllegalArgumentException {
		return createCache(cacheName, String.class, Object.class);
	}

	public <K, V> EnonCache<K, V> createCache(String cacheName, Class<K> k, Class<V> v) throws IllegalArgumentException {
		if (cachesByName.containsKey(cacheName)) {
			throw new IllegalArgumentException(cacheName+": cache already exists");
		}
		return new SimpleHashMapCache<>(cacheName);
	}

	public <K, V> EnonCache<K, V> getCache(String string, Class<K> type, Class<V> type1) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public <K, V> EnonCache<K, V> getCache(String string) {
		return cachesByName.get(string);
	}

	public Iterable<String> getCacheNames() {
		return cachesByName.keySet();
	}

	public void destroyCache(String string) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public void close() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public boolean isClosed() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public <T> T unwrap(Class<T> type) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
