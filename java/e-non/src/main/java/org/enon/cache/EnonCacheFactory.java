/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.cache;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.enon.exception.EnonCacheException;

/**
 *
 * @author clininger $Id: $
 */
public class EnonCacheFactory {

	private static EnonCacheFactory instance;
	private boolean cacheProviderSearched;
	private EnonCacheManager cacheManager;
	private Map<String, SimpleHashMapCache> simpleCaches;

//	private CacheCreateMode defaultCreateMode = CacheCreateMode.PROVIDER_OR_DEFAULT;

	private EnonCacheFactory() {
	}

	public static EnonCacheFactory getInstance() {
		if (instance == null) {
			instance = new EnonCacheFactory();
		}
		return instance;
	}

	/**
	 * Set the cache impl that the factory will create when not specified.
	 *
	 * @param cacheImpl The default CacheImple to use
	 * @return this instance
	 */
//	public EnonCacheFactory withDefaultCreateMode(CacheCreateMode cacheImpl) {
//		if (cacheImpl == null) {
//			this.defaultCreateMode = CacheCreateMode.PROVIDER_OR_DEFAULT;
//		} else {
//			this.defaultCreateMode = cacheImpl;
//		}
//		return this;
//	}

//	public CacheCreateMode getDefaultCreateMode() {
//		return defaultCreateMode;
//	}

//	/**
//	 * Create a cache using the default CacheCreateMode.  If valueClass is not String, then use the hashmap cache.
//	 *
//	 * @param <K> Cache key type
//	 * @param <V> Cache value type
//	 * @param name Cache name - must be unique. Cache provider may throw exception if not unique.
//	 * @param keyClass class of cache key
//	 * @param valueClass class of cache value
//	 * @return A new cache.
//	 */
//	public <K, V> EnonCache<K, V> create(String name, Class<K> keyClass, Class<V> valueClass) {
//		return create(name, keyClass, valueClass, valueClass == String.class ? CacheCreateMode.PROVIDER_OR_DEFAULT : CacheCreateMode.DEFAULT);
//	}

	/**
	 * Create a cache using the specified CacheCreateMode.
	 *
	 * @param <K> Cache key type
	 * @param <V> Cache value type
	 * @param name Cache name - must be unique. Cache provider may throw exception if not unique.
	 * @param keyClass class of cache key
	 * @param valueClass class of cache value
	 * @return A new cache.
	 */
	public <K, V> EnonCache<K, V> create(String name, Class<K> keyClass, Class<V> valueClass) {
			if (locateCacheManager() != null) {
				return cacheManager.createCache(name, keyClass, valueClass);
			}
			return newSimpleCache(name, keyClass, valueClass);
	}

	private EnonCacheManager locateCacheManager() {
		if (!cacheProviderSearched) {
			cacheProviderSearched = true;
			EnonCachingProvider cachingProvider = null;
			try {
				cachingProvider = EnonCaching.getCachingProvider();
			} catch (EnonCacheException x) {
				// Failed to locate CachingProvider.  Using default cache: HashMap
			}

			cacheManager = cachingProvider != null ? cachingProvider.getCacheManager() : null;
		}
		return cacheManager;
	}

	private <K, V> SimpleHashMapCache<K, V> newSimpleCache(String name, Class<K> k , Class<V> v) {
		if (simpleCaches == null) {
			simpleCaches = new HashMap<>();
		}
		if (simpleCaches.containsKey(name)) {
			throw new EnonCacheException("Cache '" + name + "' already exists");
		}
		SimpleHashMapCache cache = new SimpleHashMapCache(name);
		simpleCaches.put(name, cache);
		return cache;
	}

	/**
	 * If the cache exists, destroy it.
	 *
	 * @param name the name of the cache to destroy
	 */
	public void destroyCache(String name) {
		if (cacheManager != null) {
			cacheManager.destroyCache(name);
		}

		if (simpleCaches != null) {
			SimpleHashMapCache cache = simpleCaches.remove(name);
			if (cache != null) {
				cache.close();
			}
		}
	}

	public void close() {
		if (cacheManager != null) {
			cacheManager.close();
		}

		if (simpleCaches != null) {
			Set<String> names = new HashSet<>(simpleCaches.keySet());
			for (String name : names) {
				destroyCache(name);
			}
			simpleCaches.clear();
			simpleCaches = null;
		}
	}
}
