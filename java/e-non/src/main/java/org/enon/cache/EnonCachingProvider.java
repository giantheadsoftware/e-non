/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.cache;

import java.net.URI;
import java.util.Properties;

/**
 *
 * @author clininger $Id: $
 */
public interface EnonCachingProvider {

	public EnonCacheManager getCacheManager(URI uri, ClassLoader cl, Properties prprts);

	public ClassLoader getDefaultClassLoader();

	public URI getDefaultURI();

	public Properties getDefaultProperties();

	public EnonCacheManager getCacheManager(URI uri, ClassLoader cl);

	public EnonCacheManager getCacheManager();

	public void close();

	public void close(ClassLoader cl);

	public void close(URI uri, ClassLoader cl);

}
