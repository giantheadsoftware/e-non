/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.enon.cache;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author clininger
 * $Id: $
 * @param <K> Reference to Key type
 * @param <V> Reference to Value type
 */
public interface EnonCache<K,V> {

	public Iterator<Map.Entry<K, V>> iterator();

	public <T> T unwrap(Class<T> type);

	public boolean isClosed();

	public void close();

	public EnonCacheManager getCacheManager();

	public String getName();

	public void clear();

	public void removeAll();

	public void removeAll(Set<? extends K> set);

	public V getAndReplace(K k, V v);

	public boolean replace(K k, V v);

	public boolean replace(K k, V v, V v1);

	public V getAndRemove(K k);

	public boolean remove(K k, V v);

	public boolean remove(K k);

	public boolean putIfAbsent(K k, V v);

	public void putAll(Map<? extends K, ? extends V> map);

	public V getAndPut(K k, V v);

	public void put(K k, V v);

	public boolean containsKey(K k);

	public Map<K, V> getAll(Set<? extends K> set);

	public V get(K k);

}
