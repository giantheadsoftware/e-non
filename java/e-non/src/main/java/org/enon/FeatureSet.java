/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author clininger $Id: $
 */
public enum FeatureSet {
	G((byte) 0x02),
	M((byte) 0x04),
	N((byte) 0x40),
	S((byte) 0x08),
	X((byte) 0x01),
	Y((byte) 0x20),
	Z((byte) 0x10);

	public final byte bitmask;

	private FeatureSet(byte bitmask) {
		this.bitmask = bitmask;
	}

	public static Set<FeatureSet> fromBitmask(byte bits) {
		Set<FeatureSet> features = new HashSet<>();
		for (FeatureSet fs : values()) {
			if (0 != (fs.bitmask & bits)) {
				features.add(fs);
			}
		}
		return features;
	}

	public static byte toBitmask(Set<FeatureSet> features) {
		byte bitmask = 0;
		for (FeatureSet fs : features) {
			bitmask = (byte) (bitmask | fs.bitmask);
		}
		return bitmask;
	}
}
