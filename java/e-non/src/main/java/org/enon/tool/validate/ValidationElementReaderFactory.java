/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.tool.validate;

import java.io.IOException;
import org.enon.EnonConfig;
import org.enon.ReaderContext;
import org.enon.element.ConstantElement;
import org.enon.element.Element;
import org.enon.element.ElementSubtype;
import org.enon.element.ElementType;
import org.enon.element.reader.BinaryElementReader;
import org.enon.element.reader.BinaryElementReaderFactory;

/**
 * Provide ElementReader instances for validation.
 * @author clininger $Id: $
 */
public class ValidationElementReaderFactory extends BinaryElementReaderFactory {

	private final ElementValidator validator;

	public ValidationElementReaderFactory(EnonConfig config) {
		super(config);
		validator = new ElementValidator(config);
	}

	@Override
	public ValidationElementReader lookupReader(ElementType type, ElementSubtype subType) {
		return new ValidationElementReader(type, subType);
	}

	/**
	 * Reader instance that defers to the ElementValidator
	 */
	public class ValidationElementReader implements BinaryElementReader {
		private final ElementType type;
		private final ElementSubtype subType;

		public ValidationElementReader(ElementType type, ElementSubtype subType) {
			this.type = type;
			this.subType = subType;
		}

		/**
		 * Validate the next element
		 * @param ctx The reader context
		 * @return instance of NullElement
		 * @throws IOException on io error
		 */
		@Override
		public Element read(ReaderContext ctx) throws IOException {
			validator.validate(type, subType, ctx);
			return ConstantElement.nullInstance();
		}

	}
}

