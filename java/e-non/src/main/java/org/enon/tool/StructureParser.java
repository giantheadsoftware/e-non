/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.tool;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.enon.Prolog;
import org.enon.ReaderContext;
import org.enon.element.ByteElement;
import org.enon.element.DoubleElement;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.ElementSubtype;
import org.enon.element.ElementType;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.LongElement;
import org.enon.element.ShortElement;
import org.enon.element.array.BitArrayElement;
import org.enon.element.meta.MetaSize;
import org.enon.exception.EnonException;

/**
 *
 * @author clininger $Id: $
 */
public class StructureParser {

	private final DataInputStream dis;
	private final Map<Integer, String> indents = new HashMap<>();
	private RootHeaderDTO rootStruct;
	private int indentSize = 3;
	private int indent = 0;

	public StructureParser(InputStream enonStream) {
		this.dis = wrapStream(enonStream);
	}

	public StructureParser indentSize(int size) {
		this.indentSize = size;
		return this;
	}

	public RootHeaderDTO parse() {
		try {
			Prolog prolog = readProlog();
			rootStruct = new RootHeaderDTO(prolog);

			rootStruct.contents.addAll(readList(null));

		} catch (Exception x) {
			throw new EnonException(x.getMessage(), x);
		}
		return rootStruct;
	}

	private List<ElementHeaderDTO> readList(Long size) throws IOException {
		List<ElementHeaderDTO> list = new LinkedList<>();
		long count = 0;
		boolean end = size != null && count >= size;
		while (!end) {
			ElementHeaderDTO element = parseElement();
			if (element == null) {
				break;
			}
			list.add(element);
			end = size != null && ++count >= size;
		}
		if (size != null && count < size) {
			throw new EnonException("Could not read expected number of elements: " + count + " < " + size);
		}
		return list;
	}

	private Map<ElementHeaderDTO, ElementHeaderDTO> readMap(long size) throws IOException {
		Map<ElementHeaderDTO, ElementHeaderDTO> map = new LinkedHashMap<>();
		long count = 0;
		boolean end = count >= size;
		while (!end) {
			ElementHeaderDTO key = parseElement();
			if (key == null) {
				break;
			}
			ElementHeaderDTO value = parseElement();
			if (value == null) {
				break;
			}
			map.put(key, value);
			end = ++count >= size;
		}
		if (count < size) {
			throw new EnonException("Could not read expected number of element pairs: " + count + " < " + size);
		}
		return map;
	}

	private ElementHeaderDTO parseElement() throws IOException {
		byte prefix;
		try {
			prefix = dis.readByte();

			ElementType elementType = ElementType.forPrefix(prefix);
			if (elementType == null) {
				throw new EnonException("No element type for prefix " + prefix + '[' + (char) prefix + ']');
			}
			ElementSubtype subType = null;
			if (elementType.hasSubTypes()) {
				subType = elementType.subtypeForPrefix(dis.readByte());
				if (subType == null) {
					throw new EnonException("Subtype required");
				}
			}
			long size = parseSize(elementType);

			switch (elementType) {
				case LIST_ELEMENT:
					ListHeaderDTO list = new ListHeaderDTO(elementType, size);
					list.contents.addAll(readList(size));
					return list;
				case MAP_ELEMENT:
					MapHeaderDTO map = new MapHeaderDTO(elementType, size);
					map.contents.putAll(readMap(size));
					return map;
				case ARRAY_ELEMENT:
					ElementHeaderDTO array = new ElementHeaderDTO(elementType, subType, size);
					if (size > 0) {
						switch ((ArrayElementSubtype)subType) {
							case BYTE_SUB_ELEMENT:
								dis.skip(size * ByteElement.CONTENT_LENGTH);
								break;
							case DOUBLE_SUB_ELEMENT:
								dis.skip(size * DoubleElement.CONTENT_LENGTH);
								break;
							case FALSE_SUB_ELEMENT:
								dis.skip(BitArrayElement.computeSize((int)size));
								break;
							case FLOAT_SUB_ELEMENT:
								dis.skip(size * FloatElement.CONTENT_LENGTH);
								break;
							case INT_SUB_ELEMENT:
								dis.skip(size * IntElement.CONTENT_LENGTH);
								break;
							case LONG_SUB_ELEMENT:
								dis.skip(size * LongElement.CONTENT_LENGTH);
								break;
							case SHORT_SUB_ELEMENT:
								dis.skip(size * ShortElement.CONTENT_LENGTH);
								break;
						}
					}
					return array;
				default:
					ElementHeaderDTO element = new ElementHeaderDTO(elementType, size);
					if (size > 0) {
						dis.skip(size);
					}
					return element;
			}
		} catch (EOFException x) {
			return null;
		}
	}

	private DataInputStream wrapStream(InputStream is) {
		return is instanceof DataInputStream ? (DataInputStream) is : new DataInputStream(is);
	}

	private Prolog readProlog() throws IOException {
		return new ReaderContext(dis).readProlog();
	}

	private long parseSize(ElementType elementType) {
		try {
			switch (elementType) {
				case FALSE_ELEMENT:
				case TRUE_ELEMENT:
				case NULL_ELEMENT:
				case NANO_INT_ELEMENT:
					return 0;
				case BYTE_ELEMENT:
					return ByteElement.CONTENT_LENGTH;
				case DOUBLE_ELEMENT:
					return DoubleElement.CONTENT_LENGTH;
				case FLOAT_ELEMENT:
					return FloatElement.CONTENT_LENGTH;
				case INT_ELEMENT:
					return IntElement.CONTENT_LENGTH;
				case LONG_ELEMENT:
					return LongElement.CONTENT_LENGTH;
				case SHORT_ELEMENT:
					return ShortElement.CONTENT_LENGTH;
				case ARRAY_ELEMENT:
				case BLOB_ELEMENT:
				case LIST_ELEMENT:
				case MAP_ELEMENT:
				case NUMBER_ELEMENT:
				case STRING_ELEMENT:
					MetaSize metaSize = MetaSize.READER.read(dis);
					return metaSize.getSize().getSize();
				default:
					throw new IllegalArgumentException("Can not process element type: " + elementType.toString());
			}
		} catch (IOException x) {
			throw new EnonException("Could not read element size: ", x);
		}
	}

	private String indent() {
		String indentStr = indents.get(indent);
		if (indentStr == null) {
			char[] chars = new char[indentSize * indent];
			Arrays.fill(chars, ' ');
			indentStr = '\n' + String.valueOf(chars);
			indents.put(indent, indentStr);
		}
		return indentStr;
	}

	public void print(PrintStream ps) {
		if (rootStruct == null) {
			parse();
		}
		ps.println("start of eNON data:");
		ps.println(rootStruct.toString());
		ps.println("end of eNON data.");
	}

	/**
	 * DTO class to hold element summary
	 */
	public class ElementHeaderDTO {

		public final ElementType type;
		public final ElementSubtype subType;
		public final long size;

		public ElementHeaderDTO(ElementType type, long size) {
			this(type, null, size);
		}

		public ElementHeaderDTO(ElementType type, ElementSubtype subType, long size) {
			this.type = type;
			this.subType = subType;
			this.size = size;
		}

		@Override
		public String toString() {
			if (type == null) {
				return "";
			}
			String string = indent() + "|--- element: " + type.toString()
					+ (subType != null ? (" of "+subType) : "")
					+ "  Size: " + size;
			return string;
		}
	}

	/**
	 * DTO class to hold list summary
	 */
	public class ListHeaderDTO extends ElementHeaderDTO {

		public final List<ElementHeaderDTO> contents = new LinkedList<>();

		public ListHeaderDTO(ElementType type, long size) {
			super(type, size);
		}

		@Override
		public String toString() {
			final StringBuilder stringBuilder = new StringBuilder(super.toString());
			indent++;
			contents.stream().forEach((elementStruct) -> stringBuilder.append(elementStruct.toString()));
			indent--;
			return stringBuilder.toString();
		}
	}

	/**
	 * DTO class to hold map summary
	 */
	public class MapHeaderDTO extends ElementHeaderDTO {

		public final Map<ElementHeaderDTO, ElementHeaderDTO> contents = new LinkedHashMap<>();

		public MapHeaderDTO(ElementType type, long size) {
			super(type, size);
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder(super.toString());
			indent++;
			contents.entrySet().stream().forEach((kv)
					-> sb.append(indent()).append("key: ").append(kv.getKey().toString())
							.append(indent()).append("value: ").append(kv.getValue().toString())
			);
			indent--;
			return sb.toString();
		}
	}

		/**
	 * DTO class to hold root summary
	 */
public class RootHeaderDTO extends ListHeaderDTO {

		public final Prolog prolog;

		public RootHeaderDTO(Prolog prolog) {
			super(null, -1);
			this.prolog = prolog;
		}

		@Override
		public String toString() {
			return prolog.toString()
					+ super.toString()
					+ "\nTotal root elements: " + contents.size();
		}
	}
}
