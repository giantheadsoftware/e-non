/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.tool.validate;

import java.io.IOException;
import java.io.InputStream;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.ReaderContext;
import org.enon.element.ByteElement;
import org.enon.element.DoubleElement;
import org.enon.element.ElementType;
import static org.enon.element.ElementType.*;
import org.enon.element.IntElement;
import org.enon.element.LongElement;
import org.enon.element.NanoIntElement;
import org.enon.element.ShortElement;
import org.enon.element.meta.Size;
import org.enon.element.ConstantElement;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.ElementSubtype;
import org.enon.element.FloatElement;
import org.enon.element.meta.MetaSize;
import org.enon.event.EnonEvent;
import org.enon.event.ErrorEvent;
import org.enon.event.EventPublisher;
import org.enon.event.EventType;

/**
 *
 * @author clininger $Id: $
 */
public class ElementValidator {

	/**
	 * element types valid for eNON-0 streams
	 */
	public static final Set<ElementType> ENON_0_TYPES = EnumSet.of(
			BLOB_ELEMENT, DOUBLE_ELEMENT, FALSE_ELEMENT, INFINITY_NEG_ELEMENT, INFINITY_POS_ELEMENT, INT_ELEMENT,
			LIST_ELEMENT, MAP_ELEMENT, NANO_INT_ELEMENT, NAN_ELEMENT, NULL_ELEMENT, NUMBER_ELEMENT, STRING_ELEMENT, TRUE_ELEMENT);

	/**
	 * element types valid for eNON-X streams
	 */
	public static final Set<ElementType> ENON_X_TYPES = EnumSet.of(
			ARRAY_ELEMENT, BYTE_ELEMENT, FLOAT_ELEMENT, LONG_ELEMENT, SHORT_ELEMENT);
	
	private final Map<ElementType, Integer> fixedSizes = new EnumMap<>(ElementType.class);
	private final Set<ElementType> legalInputTypes;

	private EventPublisher eventPublisher;

	public ElementValidator(EnonConfig config) {
		// establish fixed size elements
		setFixedSizes();
		
		// establish legal element types based on the current reader config
		Set<ElementType> types = EnumSet.copyOf(ENON_0_TYPES);
		if (config.hasFeature(FeatureSet.X)) {
			types.addAll(ENON_X_TYPES);
		}

		legalInputTypes = types;
	}

	/**
	 * Skip over the bytes for the specified ElementType in the input stream.
	 * For List and Map elements, try to skip over 
	 * @param type The current element type
	 * @param subType The current element subtype
	 * @param ctx reader context
	 */
	public void validate(ElementType type, ElementSubtype subType, ReaderContext ctx) {
		eventPublisher = ctx.getEventPublisher();
		eventPublisher.publish(new EnonEvent(EventType.INFO, "Validating "+type));
		try {
			if (!legalInputTypes.contains(type)) {
				throw new EnonValidationException("Invalid element type for this Reader configuration: "+type);
			}
			// get the set of types declared as legal by the stream prolog
			Set<ElementType> streamTypes = EnumSet.copyOf(ENON_0_TYPES);
			if (ctx.getRequiredFeatures().contains(FeatureSet.X)) {
				streamTypes.addAll(ENON_X_TYPES);
			}
			if (!streamTypes.contains(type)) {
				throw new EnonValidationException("Invalid element type for this stream: "+type);
			}
			skipElement(type, subType, ctx);
		} catch (EnonValidationException x) {
			eventPublisher.publish(new ErrorEvent(x.getMessage()));
			throw x;
		}
		catch (Exception x) {
			eventPublisher.publish(new ErrorEvent(x.getMessage()));
			throw new EnonValidationException(x.getMessage(), x);
		}
	}
	
	private void skipElement(ElementType type, ElementSubtype subType, ReaderContext ctx) throws IOException {
		Integer fixedSize = fixedSizes.get(type);
		if (fixedSize != null) {
			skipBytes(ctx.getInputStream(), fixedSize);
			return;
		}
		if (type == ARRAY_ELEMENT) {
			skipArray(ctx, (ArrayElementSubtype) subType);
			return;
		}
		if (subType != null) {
			throw new EnonValidationException("Subtype should be null, but was "+subType);
		}
		// read the expected size from the stream
		MetaSize metaSize;
		try {
			metaSize = MetaSize.READER.read(ctx.getDataInputStream());
		} catch (IOException x) {
			throw new EnonValidationException("Error reading size/meta information ", x);
		}
		Size elementSize = metaSize.getSize();
		switch (type) {
			case LIST_ELEMENT:
				skipList(ctx, elementSize.getSize());
				break;
			case MAP_ELEMENT:
				skipList(ctx, elementSize.getSize() * 2);
				break;
			case STRING_ELEMENT:
				byte[] stringBytes = new byte[(int)elementSize.getSize()];
				ctx.getInputStream().read(stringBytes);
				eventPublisher.publish(new EnonEvent(EventType.INFO, new String(stringBytes)));
				break;
			default:
				skipBytes(ctx.getInputStream(), elementSize.getSize());
				break;
		}
	}

	private void skipBytes(InputStream in, long length) {
		try {
			eventPublisher.publish(new EnonEvent(EventType.INFO, "Skippping "+length+" bytes"));
			long skipped = in.skip(length);
			if (skipped < length) {
				throw new EnonValidationException("End of Data while skippping "+length+" bytes");
			}
		} catch (IOException x) {
			throw new EnonValidationException("End of Data while skippping "+length+" bytes", x);
		}
	}

	private void skipList(ReaderContext ctx, long count) {
		try {
			for (int i = 0; i < count; i++) {
				eventPublisher.publish(new EnonEvent(EventType.DEBUG, "Skippping list entry #"+(i+1)+" of "+count));
				if (null == ctx.parseNextElement()) {
					throw new EnonValidationException("Not enough elements in container. Expected "+count+", found "+i);
				}
			}
		} catch (IOException x) {
			throw new EnonValidationException("Error while reading container element: ", x);
		}
	}

	private void skipArray(ReaderContext ctx, ArrayElementSubtype subType) throws IOException {
		if (subType == null || !ARRAY_ELEMENT.isValidSubType(subType)) {
			throw new EnonValidationException("Invalid array subtype: " + (subType != null ? (subType.prefix + (char)subType.prefix) : "null"));
		}

		// read the expected array length from the stream
		MetaSize metaSize;
		try {
			metaSize = MetaSize.READER.read(ctx.getDataInputStream());
		} catch (IOException x) {
			throw new EnonValidationException("Error reading size/meta information ", x);
		}
		int length = (int)metaSize.getSize().getSize();

		switch (subType) {
			case BYTE_SUB_ELEMENT:
				skipBytes(ctx.getInputStream(), length);
				break;
			case DOUBLE_SUB_ELEMENT:
				skipBytes(ctx.getInputStream(), length * DoubleElement.CONTENT_LENGTH);
				break;
			case FALSE_SUB_ELEMENT:
				skipBytes(ctx.getInputStream(),  length / 8 + (length % 8 > 0 ? 1 : 0));
				break;
			case FLOAT_SUB_ELEMENT:
				skipBytes(ctx.getInputStream(), length * FloatElement.CONTENT_LENGTH);
				break;
			case INT_SUB_ELEMENT:
				skipBytes(ctx.getInputStream(), length * IntElement.CONTENT_LENGTH);
				break;
			case LONG_SUB_ELEMENT:
				skipBytes(ctx.getInputStream(), length * LongElement.CONTENT_LENGTH);
				break;
			case SHORT_SUB_ELEMENT:
				skipBytes(ctx.getInputStream(), length * ShortElement.CONTENT_LENGTH);
				break;
		}
	}

	private void setFixedSizes() {
		fixedSizes.put(BYTE_ELEMENT, ByteElement.CONTENT_LENGTH);
		fixedSizes.put(DOUBLE_ELEMENT, DoubleElement.CONTENT_LENGTH);
		fixedSizes.put(FALSE_ELEMENT, ConstantElement.CONTENT_LENGTH);
		fixedSizes.put(FLOAT_ELEMENT, FloatElement.CONTENT_LENGTH);
		fixedSizes.put(INFINITY_NEG_ELEMENT, ConstantElement.CONTENT_LENGTH);
		fixedSizes.put(INFINITY_POS_ELEMENT, ConstantElement.CONTENT_LENGTH);
		fixedSizes.put(INT_ELEMENT, IntElement.CONTENT_LENGTH);
		fixedSizes.put(LONG_ELEMENT, LongElement.CONTENT_LENGTH);
		fixedSizes.put(NANO_INT_ELEMENT, NanoIntElement.CONTENT_LENGTH);
		fixedSizes.put(NAN_ELEMENT, ConstantElement.CONTENT_LENGTH);
		fixedSizes.put(NULL_ELEMENT, ConstantElement.CONTENT_LENGTH);
		fixedSizes.put(SHORT_ELEMENT, ShortElement.CONTENT_LENGTH);
		fixedSizes.put(TRUE_ELEMENT, ConstantElement.CONTENT_LENGTH);
	}
}
