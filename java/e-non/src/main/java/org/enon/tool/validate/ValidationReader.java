/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.tool.validate;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Reader;
import org.enon.event.EnonEvent;
import org.enon.event.ErrorEvent;
import org.enon.event.EventPublisher;
import org.enon.event.EventType;
import org.enon.exception.EnonReadException;

/**
 * Reader that performs validation rather than constructing elements.
 *
 * @author clininger
 */
public class ValidationReader extends Reader {

	private final List<EnonValidationException> errors = new LinkedList<>();

	/**
	 * Constructor
	 *
	 * @param fromStream Input stream to validate
	 * @param config Contains Feature sets to determine what is legal in the stream 
	 */
	protected ValidationReader(InputStream fromStream, EnonConfig config) {
		super(fromStream, config);
	}

	public static ValidationReader create(InputStream fromStream, Set<FeatureSet> legalFeatures) {
		EnonConfig config = new EnonConfig.Builder(legalFeatures)
				.elementReaderFactoryImpl(ValidationElementReaderFactory.class)
				.build();
		return new ValidationReader(fromStream, config);
	}

	@Override
	public ValidationReader eventPublisher(EventPublisher eventPublisher) {
		return super.eventPublisher(eventPublisher);
	}

	/**
	 * Validate the eNON stream.
	 * @return list of errors encountered
	 */
	public List<EnonValidationException> validate() {
		try {
			super.read(null);
			getReaderContext().getEventPublisher().publish(new EnonEvent(EventType.COMPLETE));
		} catch (EnonValidationException x) {
			errors.add(x);
			getReaderContext().getEventPublisher().publish(new EnonEvent(EventType.ERROR, x.getMessage()));
		} catch (Exception x) {
			errors.add(new EnonValidationException("Unexpected error:  ", x));
			getReaderContext().getEventPublisher().publish(new ErrorEvent(x));
		}
		return errors;
	}

	@Override
	protected void validateProlog() {
		try {
			super.validateProlog();
		} catch (EnonReadException x) {
			getReaderContext().getEventPublisher().publish(new ErrorEvent("Invalid Prolog: ", x));
			throw new EnonValidationException("Invalid prolog: ", x);
		}
	}

}
