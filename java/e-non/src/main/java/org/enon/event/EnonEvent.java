/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.event;

/**
 *
 * @author clininger $Id: $
 */
public class EnonEvent {

	private final EventType type;
	private String message;

	public EnonEvent(EventType type) {
		this.type = type;
	}

	public EnonEvent(EventType type, String message) {
		this.type = type;
		this.message = message;
	}

	public EventType getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

	public EnonEvent message(String message) {
		this.message = message;
		return this;
	}

	@Override
	public String toString() {
		return String.valueOf(type) + ": " + message;
	}
}
