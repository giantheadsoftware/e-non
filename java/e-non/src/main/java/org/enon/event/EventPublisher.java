/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.event;

/**
 * Publish events to subscribers using a private thread.
 *
 * @author clininger $Id: $
 */
public interface EventPublisher {

	void complete();

	void fail(ErrorEvent error);

	void publish(EnonEvent event);

//	@Override
//	public void subscribe(Subscriber<? super EnonEvent> subscriber)
//	{
//		if (publisherThread == null)
//		{
//			publisherThread = new PublisherThread();
//			publisherThread.start();
//		}
//		EnonEventSubscription subscription = new EnonEventSubscription(subscriber);
//		synchronized(subscriptions)
//		{
//			subscriptions.add(subscription);
//		}
//		subscriber.onSubscribe(subscription);
//	}
//	/**
//	 * Java9 code
//	 */
//	public class EnonEventSubscription implements Subscription
//	{
//		private final Subscriber<? super EnonEvent> subscriber;
//		private long demand = 0l;
//
//		public EnonEventSubscription(Subscriber<? super EnonEvent> subscriber)
//		{
//			this.subscriber = subscriber;
//		}
//
//		@Override
//		public void request(long n)
//		{
//			synchronized(subscriptions)
//			{
//				demand += n;
//			}
//		}
//
//		private boolean hasDemand()
//		{
//			if (demand > 0)
//			{
//				demand--;
//				return true;
//			}
//			return false;
//		}
//
//		@Override
//		public void cancel()
//		{
//			synchronized(subscriptions)
//			{
//				subscriptions.remove(this);
//			}
//		}
//
//	}
}
