/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.io.IOException;
import java.io.InputStream;
import java.util.EnumMap;
import java.util.Map;
import org.enon.element.reader.ElementReader;
import org.enon.element.ElementType;
import org.enon.element.Element;
import org.enon.event.EventPublisher;
import org.enon.exception.EnonException;
import org.enon.exception.EnonReadException;

/**
 *
 * @author clininger $Id: $
 */
public class Reader {

	protected final ReaderContext ctx;

	protected final Map<ElementType, ElementReader> elementReaders = new EnumMap<>(ElementType.class);

	/**
	 * Allow subclasses to use a custom context
	 * @param ctx Context
	 */
	protected Reader(ReaderContext ctx) {
		this.ctx = ctx;
	}

	/**
	 * Create a Reader with the default conf settings (eNON-0)
	 *
	 * @param fromStream Stream to receive the data
	 */
	public Reader(InputStream fromStream) {
		this(fromStream, EnonConfig.defaults());
	}

	/**
	 * Create a Reader with the provided config.
	 *
	 * @param in Stream to receive the data
	 * @param config Configuration to use.
	 */
	public Reader(InputStream in, EnonConfig config) {
		if (in == null) {
			throw new EnonException("Reader requires an input stream.");
		}
		ctx = new ReaderContext(in, config);
	}

	public EnonConfig getConfig() {
		return ctx.getConfig();
	}

	public <R extends Reader> R eventPublisher(EventPublisher eventPublisher) {
		ctx.eventPublisher(eventPublisher);
		return (R) this;
	}

	protected <RC extends ReaderContext> RC getReaderContext() {
		return (RC) ctx;
	}

	/**
	 * Read the first element from the eNON stream and attempt to return it as type T
	 *
	 * @param <T> The desired type to return
	 * @param asType Class instance of the desired type
	 * @return the first element as type T
	 */
	public <T> T read(Class<T> asType) {
		try {
			validateProlog();
			Element next = ctx.parseNextElement();
			if (next != null) {
				next.inContainer(ctx.getRoot());
				return ctx.decode(asType, next);
			}
			return null;
		} catch (IOException ex) {
			throw new EnonReadException("Unable to read as type "+(asType != null ? asType.getName() : "Object"), ex);
		}
	}

	/**
	 * Read the prolog from the input stream (via ReaderContext.readProlog()) and verify that the current
	 * config has all the FeatureSets required by the prolog.
	 */
	protected void validateProlog() {
		Prolog prolog = ctx.readProlog();
		if (!ctx.getConfig().getFeatures().containsAll(prolog.getRequiredFeatures())) {
			throw new EnonReadException("The current config does not have the features necessary to read this stream."
					+ "  The stream requires "+prolog.getRequiredFeatures());
		}
	}

	public void close() {
		try {
			ctx.getInputStream().close();
		} catch (Exception ex) {
			throw new EnonReadException("Unable to close input stream", ex);
		}

	}

}
