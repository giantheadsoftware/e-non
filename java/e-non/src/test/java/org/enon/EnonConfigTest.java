/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.util.EnumSet;
import java.util.Set;
import org.enon.bind.value.DefaultNativeValueFactory;
import org.enon.bind.value.NativeValueFactory;
import org.enon.bind.property.DefaultPropertyAccessorFactory;
import org.enon.codec.coder.CoderFactory;
import org.enon.codec.coder.DefaultCoderFactory;
import org.enon.codec.decoder.DecoderFactory;
import org.enon.codec.decoder.DefaultDecoderFactory;
import org.enon.exception.EnonException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.enon.bind.property.PropertyAccessorFactory;
import org.enon.element.reader.BinaryElementReaderFactory;
import org.enon.element.writer.DefaultElementWriterFactory;
import org.enon.element.writer.ElementWriterFactory;

/**
 *
 * @author clininger
 */
public class EnonConfigTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnonConfigTest.class.getName());

	private EnonConfigDefaults configDefaults;

	public EnonConfigTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		configDefaults = new EnonConfigDefaults() {};
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getMinReadVersion method, of class EnonConfig.
	 */
	@Test
	public void testGetMinReadVersion() {
		LOGGER.info("getMinReadVersion");
		// test default config
		EnonConfig instance = EnonConfig.defaults();
		short expResult = EnonConfig.FORMAT_VERSION;
		short result = instance.getMinReadVersion();
		assertEquals(expResult, result);

		// override the default
		expResult = 7;
		instance = new EnonConfig.Builder().minReadVersion(expResult).build();
		result = instance.getMinReadVersion();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getFeatures method, of class EnonConfig.
	 */
	@Test
	public void testGetFeatures() {
		LOGGER.info("getFeatures");
		// test default config
		EnonConfig instance = EnonConfig.defaults();
		Set<FeatureSet> expResult = EnumSet.noneOf(FeatureSet.class);
		Set<FeatureSet> result = instance.getFeatures();
		assertEquals(expResult, result);

		// override the default
		expResult = EnumSet.of(FeatureSet.G, FeatureSet.X);
		instance = new EnonConfig.Builder(expResult).build();
		result = instance.getFeatures();
		assertEquals(expResult, result);
	}

	/**
	 * Test of hasFeature method, of class EnonConfig.
	 */
	@Test
	public void testHasFeature() {
		LOGGER.info("hasFeature");
		// test default config
		EnonConfig instance = EnonConfig.defaults();
		for (FeatureSet fs : FeatureSet.values()) {
			assertFalse(instance.hasFeature(fs));
		}

		// override the default
		Set<FeatureSet> features = EnumSet.of(FeatureSet.G, FeatureSet.M);
		instance = new EnonConfig.Builder(features).build();
		for (FeatureSet fs : FeatureSet.values()) {
			assertEquals(features.contains(fs), instance.hasFeature(fs));
		}
	}

	/**
	 * Test of getObjectAccessorFactory method, of class EnonConfig.
	 */
	@Test
	public void testGetObjectAccessorFactory() {
		LOGGER.info("getObjectAccessorFactory");
		// test default config
		EnonConfig instance = EnonConfig.defaults();
		Class<? extends PropertyAccessorFactory> expectedType = configDefaults.accessorFactoryImpl();
		PropertyAccessorFactory result = instance.getPropertyAccessorFactory();
		assertEquals(expectedType, result.getClass());

		// 2nd request should return cached instance
		assertSame(result, instance.getPropertyAccessorFactory());
	}

	@Test(/*expected = EnonException.class*/)
	public void testGetObjectAccessorFactoryFailConstructor() {
		LOGGER.info("getObjectAccessorFactoryFailConstructor");

		Exception x = assertThrows(EnonException.class, () -> {
			EnonConfig instance = new EnonConfig.Builder().accessorFactoryImpl(InvalidAccessorFactory.class).build();

			instance.getPropertyAccessorFactory();
		});
	}

	/**
	 * Test of getNativeValueFactory method, of class EnonConfig.
	 */
	@Test
	public void testGetNativeValueFactory() {
		LOGGER.info("getNativeValueFactory");
		// test default config
		EnonConfig instance = EnonConfig.defaults();
		Class<? extends NativeValueFactory> expectedType = configDefaults.valueFactoryImpl();
		NativeValueFactory result = instance.getNativeValueFactory();
		assertEquals(expectedType, result.getClass());

		// 2nd request should return cached instance
		assertSame(result, instance.getNativeValueFactory());
	}

	@Test(/*expected = EnonException.class*/)
	public void testGetNativeValueFactoryFailConstructor() {
		LOGGER.info("getNativeValueFactoryFailConstructor");
		Exception x = assertThrows(EnonException.class, () -> {

			EnonConfig instance = new EnonConfig.Builder().valueFactoryImpl(InvalidNativeValueFactory.class).build();

			instance.getNativeValueFactory();
		});
	}

	/**
	 * Test of getCoderFactory method, of class EnonConfig.
	 */
	@Test
	public void testGetCoderFactory() {
		LOGGER.info("getCoderFactory");
		// test default config
		EnonConfig instance = EnonConfig.defaults();
		Class<? extends CoderFactory> expectedType = configDefaults.coderFactoryImpl();
		CoderFactory result = instance.getCoderFactory();
		assertEquals(expectedType, result.getClass());

		// 2nd request should return cached instance
		assertSame(result, instance.getCoderFactory());
	}

	@Test(/*expected = EnonException.class*/)
	public void testGetCoderFactoryFailConstructor() {
		LOGGER.info("getCoderFactoryFailConstructor");
		Exception x = assertThrows(EnonException.class, () -> {

			EnonConfig instance = new EnonConfig.Builder().coderFactoryImpl(InvalidCoderFactory.class).build();

			instance.getCoderFactory();
		});
	}

	/**
	 * Test of getDecoderFactory method, of class EnonConfig.
	 */
	@Test
	public void testGetDecoderFactory() {
		LOGGER.info("getDecoderFactory");
		// test default config
		EnonConfig instance = EnonConfig.defaults();
		Class<? extends DecoderFactory> expectedType = configDefaults.decoderFactoryImpl();
		DecoderFactory result = instance.getDecoderFactory();
		assertEquals(expectedType, result.getClass());

		// 2nd request should return cached instance
		assertSame(result, instance.getDecoderFactory());
	}

	@Test(/*expected = EnonException.class*/)
	public void testGetDecoderFactoryFailConstructor() {
		LOGGER.info("getDecoderFactoryFailConstructor");
		Exception x = assertThrows(EnonException.class, () -> {

			EnonConfig instance = new EnonConfig.Builder().decoderFactoryImpl(InvalidDecoderFactory.class).build();

			instance.getDecoderFactory();
		});
	}

	@Test(/*expected = EnonException.class*/)
	public void testGetElementReaderFactoryFailConstructor() {
		LOGGER.info("getElementReaderFactoryFailConstructor");
		Exception x = assertThrows(EnonException.class, () -> {

			EnonConfig instance = new EnonConfig.Builder().elementReaderFactoryImpl(InvalidElementReaderFactory.class).build();

			instance.getElementReaderFactory();
		});
	}

	@Test(/*expected = EnonException.class*/)
	public void testGetElementWriterFactoryFailConstructor() {
		LOGGER.info("getElementREaderFactoryFailConstructor");
		Exception x = assertThrows(EnonException.class, () -> {

			EnonConfig instance = new EnonConfig.Builder().elementWriterFactoryImpl(ElementWriterFactory.class).build();

			instance.getElementWriterFactory();
		});
	}

	@Test
	public void testBuilderFromConfig() {
		LOGGER.info("builderFromConfig");

		EnonConfig fromConfig = new EnonConfig.Builder(EnonConfig.defaults()).build();

		assertEquals(0, EnonConfig.defaults().compareTo(fromConfig));
		assertEquals(0, fromConfig.compareTo(EnonConfig.defaults()));

		EnonConfig nonDefault = new EnonConfig.Builder()
				.feature(FeatureSet.S).feature(FeatureSet.X)
				.accessorFactoryImpl(InvalidAccessorFactory.class)
				.coderFactoryImpl(InvalidCoderFactory.class)
				.decoderFactoryImpl(InvalidDecoderFactory.class)
				.elementReaderFactoryImpl(InvalidElementReaderFactory.class)
				.elementWriterFactoryImpl(InvalidElementWriterFactiry.class)
				.valueFactoryImpl(InvalidNativeValueFactory.class)
				.limit(LimitParam.MAX_BLOB_SIZE, 10000)
				.minReadVersion((short) 5)
				.build();

		fromConfig = new EnonConfig.Builder(nonDefault).build();

		assertEquals(0, nonDefault.compareTo(fromConfig));
		assertEquals(0, fromConfig.compareTo(nonDefault));

		assertNotEquals(0, EnonConfig.defaults().compareTo(nonDefault));
	}

	/**
	 * Does not have the proper constructor
	 */
	private class InvalidAccessorFactory extends DefaultPropertyAccessorFactory {

		public InvalidAccessorFactory() {
			super(EnonConfig.defaults());
		}

	}

	/**
	 * Does not have the proper constructor
	 */
	private class InvalidCoderFactory extends DefaultCoderFactory {

		public InvalidCoderFactory() {
			super(EnonConfig.defaults());
		}

	}

	/**
	 * Does not have the proper constructor
	 */
	private class InvalidDecoderFactory extends DefaultDecoderFactory {

		public InvalidDecoderFactory() {
			super(EnonConfig.defaults());
		}

	}

	/**
	 * Does not have the proper constructor
	 */
	private class InvalidNativeValueFactory extends DefaultNativeValueFactory {

		public InvalidNativeValueFactory() {
			super(EnonConfig.defaults());
		}

	}

	/**
	 * Does not have the proper constructor
	 */
	private class InvalidElementReaderFactory extends BinaryElementReaderFactory {

		public InvalidElementReaderFactory() {
			super(EnonConfig.defaults());
		}

	}

	/**
	 * Does not have the proper constructor
	 */
	private class InvalidElementWriterFactiry extends DefaultElementWriterFactory {

		public InvalidElementWriterFactiry() {
			super(EnonConfig.defaults());
		}

	}
}
