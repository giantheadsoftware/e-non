/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.bind.value.CollectionValue.Acceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class CollectionValueTest {

	public CollectionValueTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testTest() {
		List<String> list = Arrays.asList(new String[]{"test value", null});
		Set<String> set = new HashSet<>(list);

		Acceptor acceptor = new Acceptor(EnonConfig.defaults());
		// PREFER to accept collections and non-primitive arrays
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(list.getClass()));
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(set.getClass()));
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(Byte[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(Object[].class));
		// accept primitive arrays as a LAST_RESORT
		assertSame(ValueTypeAcceptor.AcceptLevel.LAST_RESORT, acceptor.accept(int[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.LAST_RESORT, acceptor.accept(byte[].class));
		// NEVER accept null
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(null));

	}

	@Test
	public void testApply() {
		String[] array = new String[]{"test value", null};
		List<String> list = Arrays.asList(array);
		Set<String> set = new HashSet<>(list);

		Acceptor acceptor = new Acceptor(EnonConfig.defaults());

		CollectionValue value = acceptor.toNativeValue(array.getClass(), array);
		for (int i = 0; i < array.length; i++)
			assertEquals(array[i], ((List<NativeValue>)value.getValue()).get(i).getValue());
		assertTrue(value.toString().contains(array.getClass().getSimpleName()));
		assertTrue(value.toString().contains(String.valueOf(list.size())));
		
		value = acceptor.toNativeValue(list.getClass(), list);
		for (int i = 0; i < list.size(); i++)
			assertEquals(list.get(i), ((List<NativeValue>)value.getValue()).get(i).getValue());
		assertTrue(value.toString().contains(list.getClass().getSimpleName()));
		assertTrue(value.toString().contains(String.valueOf(list.size())));

		value = acceptor.toNativeValue(set.getClass(), set);
		List<String> setList = new ArrayList<>(set);
		value.getValue().stream()
				.map(nv -> nv.getValue())
				.forEach(v -> assertTrue(set.contains(v)));
		assertTrue(value.toString().contains(set.getClass().getSimpleName()));
		assertTrue(value.toString().contains(String.valueOf(set.size())));
	}
}
