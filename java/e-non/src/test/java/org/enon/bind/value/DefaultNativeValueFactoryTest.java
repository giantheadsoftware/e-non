/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.exception.EnonException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class DefaultNativeValueFactoryTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultNativeValueFactoryTest.class.getName());

	public DefaultNativeValueFactoryTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getCache method, of class DefaultNativeValueFactory.
	 */
	@Test
	public void testGetCache() {
		LOGGER.info("getCache");
		DefaultNativeValueFactory instance = new DefaultNativeValueFactory(EnonConfig.defaults(), Collections.EMPTY_LIST);
		Map<String, ValueTypeAcceptor> result = instance.getCache();
		// getCache should always return a cache
		assertNotNull(result);
		// the initial cache should be empty
		assertTrue(result.isEmpty());
		// the same cache should always be returned
		assertSame(result, instance.getCache());
	}

	/**
	 * Test of nativeValue method, of class DefaultNativeValueFactory.
	 */
	@Test
	public void testNativeValue() {
		LOGGER.info("nativeValue");
		Integer object = 12345;
		// create a factory that uses only our dummy acceptor
		DefaultNativeValueFactory instance = new DefaultNativeValueFactory(
				EnonConfig.defaults(), Arrays.asList(new Class[]{DummyAcceptor.class}));

		// factory should handle an integer
		NativeValue result = instance.nativeValue(object);

		assertNotNull(result);
		assertEquals(StringValue.class, result.getClass());

		// factory should not handle a string
		result = instance.nativeValue("fail");

		assertNull(result);
	}

	/**
	 * Test of getCached method, of class DefaultNativeValueFactory.
	 */
	@Test
	public void testGetCached_Object() {
		LOGGER.info("getCachedByObject");
		Object object = "test string";
		DefaultNativeValueFactory instance = new DefaultNativeValueFactory(EnonConfig.defaults());
		ValueTypeAcceptor result = instance.getCached(object);

		// cache should miss
		assertNull(result);

		// should cache the acceptor
		instance.nativeValue(object);

		// cache should hit
		result = instance.getCached(object);
		assertSame(StringValue.Acceptor.class, result.getClass());

		// cache should return same instance
		assertSame(result, instance.getCached(object));
	}

	/**
	 * Test of getCached method, of class DefaultNativeValueFactory.
	 */
	@Test
	public void testGetCached_ClassName() {
		LOGGER.info("getCachedByClassName");
		Object object = 'c';
		DefaultNativeValueFactory instance = new DefaultNativeValueFactory(EnonConfig.defaults());
		ValueTypeAcceptor result = instance.getCached(object.getClass().getName());

		// cache should miss
		assertNull(result);

		// should cache the acceptor
		assertNotNull(instance.nativeValue(object));

		// cache should hit
		result = instance.getCached(object.getClass().getName());
		assertNotNull(result);
		assertSame(StringValue.Acceptor.class, result.getClass());

		// cache should return same instance
		assertSame(result, instance.getCached(object.getClass().getName()));

		// should not have cached version of Integer.class
		assertNull(instance.getCached(Integer.class.getName()));
	}

	/**
	 * Test of cache method, of class DefaultNativeValueFactory.
	 */
	@Test
	public void testCache() {
		LOGGER.info("cache");
		String className = byte[].class.getName();
		DefaultNativeValueFactory instance = new DefaultNativeValueFactory(EnonConfig.defaults());
		ValueTypeAcceptor acceptor = new BlobValue.Acceptor(EnonConfig.defaults());

		// cache should miss
		assertNull(instance.getCached(className));

		// should cache the acceptor
		instance.cache(className, acceptor);

		// cache should hit
		assertSame(acceptor, instance.getCached(className));
	}

	/**
	 * Test of acceptorForClass method, of class DefaultNativeValueFactory.
	 */
	@Test
	public void testAcceptorForClass() {
		LOGGER.info("acceptorForClass");
		// create a factory that uses only our dummy acceptor
		DefaultNativeValueFactory instance = new DefaultNativeValueFactory(EnonConfig.defaults(),
				Arrays.asList(new Class[]{DummyAcceptor.class}));

		// our factory doesn't know how to handle String
		ValueTypeAcceptor result = instance.acceptorForClass(String.class);
		assertNull(result);

		// our factory doesn't know how to handle Object
		result = instance.acceptorForClass(Object.class);
		assertNull(result);

		// factory should handle Integer
		result = instance.acceptorForClass(Integer.class);
		assertNotNull(result);
		assertSame(DummyAcceptor.class, result.getClass());

	}

	@Test(/*expected = EnonException.class*/)
	public void testFactoryInstantiateFail() {
		LOGGER.info("instantiateFail");

		Exception x = assertThrows(EnonException.class, () -> {
			// create a factory that contains our "fail" acceptor
			DefaultNativeValueFactory instance = new DefaultNativeValueFactory(EnonConfig.defaults(),
					Arrays.asList(new Class[]{DummyAcceptor.class, FailAcceptor.class}));

			// succceed
			ValueTypeAcceptor result = instance.acceptorForClass(Integer.class);

			// fail
			result = instance.acceptorForClass(String.class);
		});
	}

	/**
	 * Create a Dummy acceptor so we can focus tests on the factory, not the acceptor implementations.
	 */
	private static class DummyAcceptor extends ValueTypeAcceptor<ScalarValue> {

		public DummyAcceptor(EnonConfig config) {
			super(config);
		}

		@Override
		public ScalarValue toNativeValue(Class t, Object u) {
			return new StringValue("success");
		}

		@Override
		protected Map<Class, AcceptLevel> acceptsClasses() {
			Map<Class, AcceptLevel> map = new HashMap<>();
			map.put(Integer.class, AcceptLevel.PREFER);
			return map;
		}

	}

	/**
	 * Create an acceptor that will fail construction - does not have expected constructor
	 */
	private static class FailAcceptor extends ValueTypeAcceptor<NativeValue> {

		public FailAcceptor() {
			super(EnonConfig.defaults());
		}

		@Override
		public NativeValue toNativeValue(Class t, Object u) {
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

	}
}
