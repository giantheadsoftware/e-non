/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.enon.EnonConfig;
import org.enon.bind.value.MapValue.Acceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class MapValueTest {

	public MapValueTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getPojo method, of class PojoValue.
	 */
	@Test
	public void testAll() {
		final String name = "Test pojo";
		Map<Object, Object> map = new HashMap<>();
		map.put("name", name);
		map.put("size", name.length());
		map.put(125L, "Non-string-key");
		map.put("null", null);
		map.put(null, "null");

		Acceptor acceptor = new Acceptor(EnonConfig.defaults());
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(Map.class));
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(map.getClass()));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(Object.class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(null));

		MapValue value = acceptor.toNativeValue(map.getClass(), map);
		Map<NativeValue, NativeValue> mapValue = value.getValue();

		// declared type should be map
		assertTrue(Map.class.isAssignableFrom(value.getDeclaredType()));

		// valueMap should have same size as the original
		assertEquals(map.size(), mapValue.size());

		// all the map entries should match the origimal
		for (Entry<NativeValue, NativeValue> entry : mapValue.entrySet()) {
			Object key = entry.getKey().getValue();
			Object entryValue = entry.getValue().getValue();

			assertEquals(map.get(key), entryValue);

			if (entryValue != null) {
				assertSame(entryValue.getClass(), entry.getValue().getDeclaredType());
			} else {
				assertSame(Void.class, entry.getValue().getDeclaredType());
			}
		}

		// should retuen same map instance next time
		assertSame(mapValue, value.getValue());

		assertTrue(value.toString().contains(map.getClass().getSimpleName()));
		assertTrue(value.toString().contains(String.valueOf(map.size())));
	}

}
