/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property.pojo;

import org.enon.EnonConfig;
import org.enon.bind.value.DefaultNativeValueFactory;
import org.enon.bind.value.NativeValueFactory;
import org.enon.bind.value.PojoPropertyValue;
import org.enon.exception.EnonBindException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class PublicPojoPropertyTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PublicPojoPropertyTest.class.getName());

	private final NativeValueFactory nativeValueFactory = new DefaultNativeValueFactory(EnonConfig.defaults());
	private final PublicPojoPropertyAccessor accessor = new PublicPojoPropertyAccessor(nativeValueFactory);

	public PublicPojoPropertyTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getName method, of class PublicPojoProperty.
	 */
	@Test
	public void testGetName() {
		LOGGER.info("getName");
		PublicPojoProperty nameProp = (PublicPojoProperty) accessor.getReadableProperties(Pojo.class).get("name");

		assertEquals("name", nameProp.getName());
	}

	/**
	 * Test of readValue method, of class PublicPojoProperty.
	 */
	@Test
	public void testReadValue() {
		LOGGER.info("readValue");
		PublicPojoProperty nameProp = (PublicPojoProperty) accessor.getReadableProperties(Pojo.class).get("name");
		Pojo pojo = new Pojo("testing", 0, true);

		assertEquals("testing", nameProp.readValue(pojo).getValue().getValue());
	}

	/**
	 * Test of toString method, of class PublicPojoProperty.
	 */
	@Test
	public void testToString() {
		LOGGER.info("toString");
		PublicPojoProperty nameProp = (PublicPojoProperty) accessor.getReadableProperties(Pojo.class).get("name");

		assertTrue(nameProp.toString().contains(nameProp.getName()));
	}

	/**
	 * Test of writeValue method, of class PublicPojoProperty.
	 */
	@Test
	public void testWriteValue() {
		LOGGER.info("writeValue");
		PublicPojoProperty nameProp = (PublicPojoProperty) accessor.getReadableProperties(Pojo.class).get("name");
		Pojo pojo = new Pojo();

		nameProp.writeValue(pojo, "test writing");
		assertEquals("test writing", pojo.name);
	}

	@Test(/*expected = EnonBindException.class*/)
	public void testReadValueWrongInstanceFail() {
		LOGGER.info("readValueWrongInstanceFail");
		Exception x = assertThrows(EnonBindException.class, () -> {
			PublicPojoProperty nameProp = (PublicPojoProperty) accessor.getReadableProperties(Pojo.class).get("name");

			nameProp.readValue("can't read name from this!").getValue().getValue();
		});
	}

	@Test(/*expected = EnonBindException.class*/)
	public void testWriteValueWrongInstanceFail() {
		LOGGER.info("writeValueWrongInstanceFail");
		Exception x = assertThrows(EnonBindException.class, () -> {
			PublicPojoProperty nameProp = (PublicPojoProperty) accessor.getReadableProperties(Pojo.class).get("name");

			nameProp.writeValue("can't write to this!", "test writing");
		});
	}

	@Test(/*expected = EnonBindException.class*/)
	public void testWriteValueIllegalValueFail() {
		LOGGER.info("writeValueWrongInstanceFail");
		Exception x = assertThrows(EnonBindException.class, () -> {
			PublicPojoProperty nameProp = (PublicPojoProperty) accessor.getReadableProperties(Pojo.class).get("name");
			Pojo pojo = new Pojo();

			nameProp.writeValue(pojo, 12345);
		});
	}

	private static class Pojo {

		private String name;
		private int quality;
		private boolean available;

		public Pojo() {
		}

		public Pojo(String name, int quality, boolean available) {
			this.name = name;
			this.quality = quality;
			this.available = available;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getQuality() {
			return quality;
		}

		public void setQuality(int quality) {
			this.quality = quality;
		}

		public boolean isAvailable() {
			return available;
		}

		public void setAvailable(boolean available) {
			this.available = available;
		}

		// not valid getter
		public int get() {
			return 3;
		}

		// not valid getter
		public boolean is() {
			return false;
		}

		// not valid getter
		public int getter() {
			return 3;
		}

		// not valid getter
		public boolean isnt() {
			return false;
		}

		// not valid getter
		public Boolean isNot() {
			return false;
		}

		// getter & setter type do not match
		public String getSomething() {
			return null;
		}

		public void setSomething(Integer i) {

		}
	}

}
