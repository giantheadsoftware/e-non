/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.temporal;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TemporalVOTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(TemporalVOTest.class.getName());

	
	public TemporalVOTest() {
	}
	
	@BeforeAll
	public static void setUpClass() {
	}
	
	@AfterAll
	public static void tearDownClass() {
	}
	
	@BeforeEach
	public void setUp() {
	}
	
	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of createInstant method, of class TemporalVO.
	 */
	@Test
	public void testCreateInstant() {
		LOGGER.info("createInstant");
		long instant = 1L;
		TemporalVO result = TemporalVO.createInstant(instant);
		assertEquals(instant, result.instant.longValue());
		assertNull(result.offsetSeconds);
		assertNull(result.zoneId);
		assertNull(result.year);
		assertNull(result.month);
		assertNull(result.day);
		assertEquals(0, result.hour.longValue());
		assertEquals(0, result.minute.longValue());
		assertEquals(0, result.sec.longValue());
		assertEquals(0, result.nanos.longValue());
		assertNull(result.getMsInMinute());
		
		assertTrue(result.isInstant());
		assertFalse(result.isOffset());
		assertFalse(result.isZoned());
		assertTrue(result.isLocal());
		assertFalse(result.isDate());
		assertFalse(result.isDateTime());
		assertFalse(result.isTime());
		assertTrue(result.isDefaultTime());
		
		validateGet(result);
	}

	/**
	 * Test of createInstantOffset method, of class TemporalVO.
	 */
	@Test
	public void testCreateInstantOffset() {
		LOGGER.info("createInstantOffset");
		long instant = 15L;
		int offsetSeconds = 95;
		TemporalVO result = TemporalVO.createInstantOffset(instant, offsetSeconds);
		assertEquals(instant, result.instant.longValue());
		assertEquals(offsetSeconds, result.offsetSeconds.intValue());
		assertNull(result.zoneId);
		assertNull(result.year);
		assertNull(result.month);
		assertNull(result.day);
		assertEquals(0, result.hour.longValue());
		assertEquals(0, result.minute.longValue());
		assertEquals(0, result.sec.longValue());
		assertEquals(0, result.nanos.longValue());
		assertNull(result.getMsInMinute());
		
		assertTrue(result.isInstant());
		assertTrue(result.isOffset());
		assertFalse(result.isZoned());
		assertFalse(result.isLocal());
		assertFalse(result.isDate());
		assertFalse(result.isDateTime());
		assertFalse(result.isTime());
		assertTrue(result.isDefaultTime());
		
		validateGet(result);
	}

	/**
	 * Test of createInstantZoned method, of class TemporalVO.
	 */
	@Test
	public void testCreateInstantZoned() {
		LOGGER.info("createInstantZoned");
		long instant = 1502L;
		String zoneId = "America/New_York";
		TemporalVO result = TemporalVO.createInstantZoned(instant, zoneId);
		assertEquals(instant, result.instant.longValue());
		assertNull(result.offsetSeconds);
		assertEquals(zoneId, result.zoneId);
		assertNull(result.year);
		assertNull(result.month);
		assertNull(result.day);
		assertEquals(0, result.hour.longValue());
		assertEquals(0, result.minute.longValue());
		assertEquals(0, result.sec.longValue());
		assertEquals(0, result.nanos.longValue());
		assertNull(result.getMsInMinute());
		
		assertTrue(result.isInstant());
		assertFalse(result.isOffset());
		assertTrue(result.isZoned());
		assertFalse(result.isLocal());
		assertFalse(result.isDate());
		assertFalse(result.isDateTime());
		assertFalse(result.isTime());
		assertTrue(result.isDefaultTime());
		
		validateGet(result);
	}

	/**
	 * Test of createDate method, of class TemporalVO.
	 */
	@Test
	public void testCreateDate() {
		LOGGER.info("createDate");
		int year = 1999;
		byte month = 8;
		byte day = 13;
		TemporalVO result = TemporalVO.createDate(year, month, day);
		assertNull(result.instant);
		assertNull(result.offsetSeconds);
		assertNull(result.zoneId);
		assertEquals(year, result.year.intValue());
		assertEquals(month, result.month.intValue());
		assertEquals(day, result.day.intValue());
		assertEquals(0, result.hour.longValue());
		assertEquals(0, result.minute.longValue());
		assertEquals(0, result.sec.longValue());
		assertEquals(0, result.nanos.longValue());
		assertNull(result.getMsInMinute());
		
		assertFalse(result.isInstant());
		assertFalse(result.isOffset());
		assertFalse(result.isZoned());
		assertTrue(result.isLocal());
		assertTrue(result.isDate());
		assertFalse(result.isDateTime());
		assertFalse(result.isTime());
		assertTrue(result.isDefaultTime());
		
		validateGet(result);
	}

	/**
	 * Test of createTime method, of class TemporalVO.
	 */
	@Test
	public void testCreateTime() {
		LOGGER.info("createTime");
		byte hour = 14;
		byte minute = 42;
		byte sec = 9;
		TemporalVO result = TemporalVO.createTime(hour, minute, sec);
		assertNull(result.instant);
		assertNull(result.offsetSeconds);
		assertNull(result.zoneId);
		assertNull(result.year);
		assertNull(result.month);
		assertNull(result.day);
		assertEquals(hour, result.hour.longValue());
		assertEquals(minute, result.minute.longValue());
		assertEquals(sec, result.sec.longValue());
		assertEquals(0, result.nanos.longValue());
		assertNull(result.getMsInMinute());
		
		assertFalse(result.isInstant());
		assertFalse(result.isOffset());
		assertFalse(result.isZoned());
		assertTrue(result.isLocal());
		assertFalse(result.isDate());
		assertFalse(result.isDateTime());
		assertTrue(result.isTime());
		assertFalse(result.isDefaultTime());
		
		validateGet(result);
	}

	/**
	 * Test of createTimeOffset method, of class TemporalVO.
	 */
	@Test
	public void testCreateTimeOffset() {
		LOGGER.info("createTimeOffset");
		byte hour = 23;
		byte minute = 19;
		byte sec = 57;
		int offsetSeconds = -3600;
		TemporalVO result = TemporalVO.createTimeOffset(hour, minute, sec, offsetSeconds);
		assertNull(result.instant);
		assertEquals(offsetSeconds, result.offsetSeconds.intValue());
		assertNull(result.zoneId);
		assertNull(result.year);
		assertNull(result.month);
		assertNull(result.day);
		assertEquals(hour, result.hour.longValue());
		assertEquals(minute, result.minute.longValue());
		assertEquals(sec, result.sec.longValue());
		assertEquals(0, result.nanos.longValue());
		assertNull(result.getMsInMinute());
		
		assertFalse(result.isInstant());
		assertTrue(result.isOffset());
		assertFalse(result.isZoned());
		assertFalse(result.isLocal());
		assertFalse(result.isDate());
		assertFalse(result.isDateTime());
		assertTrue(result.isTime());
		assertFalse(result.isDefaultTime());
		
		validateGet(result);
	}

	/**
	 * Test of createDateTime method, of class TemporalVO.
	 */
	@Test
	public void testCreateDateTime() {
		LOGGER.info("createDateTime");
		int year = 2015;
		byte month = 6;
		byte day = 9;
		byte hour = 14;
		byte minute = 22;
		byte sec = 12;
		TemporalVO result = TemporalVO.createDateTime(year, month, day, hour, minute, sec);
		assertNull(result.instant);
		assertNull(result.offsetSeconds);
		assertNull(result.zoneId);
		assertEquals(year, result.year.intValue());
		assertEquals(month, result.month.byteValue());
		assertEquals(day, result.day.byteValue());
		assertEquals(hour, result.hour.byteValue());
		assertEquals(minute, result.minute.byteValue());
		assertEquals(sec, result.sec.byteValue());
		assertEquals(0, result.nanos.longValue());
		assertNull(result.getMsInMinute());
		
		assertFalse(result.isInstant());
		assertFalse(result.isOffset());
		assertFalse(result.isZoned());
		assertTrue(result.isLocal());
		assertTrue(result.isDate());
		assertTrue(result.isDateTime());
		assertTrue(result.isTime());
		assertFalse(result.isDefaultTime());
		
		validateGet(result);
	}

	/**
	 * Test of createDateTime method, of class TemporalVO.
	 */
	@Test
	public void testCreateZonedDateTime() {
		LOGGER.info("createZonedDateTime");
		int year = 2112;
		byte month = 8;
		byte day = 23;
		byte hour = 19;
		byte minute = 05;
		byte sec = 34;
		String zoneId = "America/Hawaii";
		TemporalVO result = TemporalVO.createZonedDateTime(year, month, day, hour, minute, sec, zoneId);
		assertNull(result.instant);
		assertNull(result.offsetSeconds);
		assertEquals(zoneId, result.zoneId);
		assertEquals(year, result.year.intValue());
		assertEquals(month, result.month.byteValue());
		assertEquals(day, result.day.byteValue());
		assertEquals(hour, result.hour.byteValue());
		assertEquals(minute, result.minute.byteValue());
		assertEquals(sec, result.sec.byteValue());
		assertEquals(0, result.nanos.longValue());
		assertNull(result.getMsInMinute());
		
		assertFalse(result.isInstant());
		assertFalse(result.isOffset());
		assertTrue(result.isZoned());
		assertFalse(result.isLocal());
		assertTrue(result.isDate());
		assertTrue(result.isDateTime());
		assertTrue(result.isTime());
		assertFalse(result.isDefaultTime());
		
		validateGet(result);
	}

	/**
	 * Test of getMsInMinute method, of class TemporalVO.
	 */
	@Test
	public void testGetMsInMinute() {
		LOGGER.info("getMsInMinute");
		// nanos == 0
		TemporalVO instance = new TemporalVO.Builder().build();
		assertNull(instance.getMsInMinute());
		
		// nanos < 1ms
		instance = new TemporalVO.Builder().nanos(500000).build();
		assertNull(instance.getMsInMinute());
		
		// nanos > 1ms
		instance = new TemporalVO.Builder().nanos(5000000).build();
		assertEquals(5, instance.getMsInMinute().intValue());
		
		// nanos < 1000ms
		instance = new TemporalVO.Builder().nanos(999000000).build();
		assertEquals(999, instance.getMsInMinute().intValue());
		
		// nanos < 0
		instance = new TemporalVO.Builder().nanos(-5000000).build();
		assertNull(instance.getMsInMinute());
		
		// nanos > 999ms
		instance = new TemporalVO.Builder().nanos(1000000000).build();
		assertNull(instance.getMsInMinute());

		// nanos < 1000ms, sec > 0
		instance = new TemporalVO.Builder().second((byte)15).nanos(999000000).build();
		assertEquals(15999, instance.getMsInMinute().intValue());

		// nanos < 1000ms, sec < 0
		instance = new TemporalVO.Builder().second((byte)-15).nanos(999000000).build();
		assertNull(instance.getMsInMinute());

		// nanos < 1000ms, sec > 59
		instance = new TemporalVO.Builder().second((byte)60).nanos(999000000).build();
		assertNull(instance.getMsInMinute());
		
	}

	/**
	 * Test of get method, of class TemporalVO.
	 * @param tvo
	 */
	public void validateGet(TemporalVO tvo) {
		LOGGER.info("TVO: "+tvo.toString());
		for (EnonTemporalField field : EnonTemporalField.values()) {
			switch (field) {
				case INSTANT:
					assertEquals(tvo.instant, tvo.get(field));
					break;
				case ERA:
					assertEquals(tvo.era, tvo.get(field));
					break;
				case YEAR:
					assertEquals(tvo.year, tvo.get(field));
					break;
				case MONTH:
					assertEquals(tvo.month, tvo.get(field));
					break;
				case DAY:
					assertEquals(tvo.day, tvo.get(field));
					break;
				case HOUR:
					assertEquals(tvo.hour, tvo.get(field));
					break;
				case MINUTE:
					assertEquals(tvo.minute, tvo.get(field));
					break;
				case SECOND:
					assertEquals(tvo.sec, tvo.get(field));
					break;
				case MS_IN_MINUTE:
					assertEquals(tvo.getMsInMinute(), tvo.get(field));
					break;
				case NANOS:
					assertEquals(tvo.nanos, tvo.get(field));
					break;
				case OFFSET_SECONDS:
					assertEquals(tvo.offsetSeconds, tvo.get(field));
					break;
				case OFFSET_INTERVALS:
					assertEquals(tvo.getOffsetIntervals(), tvo.get(field));
					break;
				case ZONE_ID:
					assertEquals(tvo.zoneId, tvo.get(field));
					break;
			}
		}
	}
	
}
