/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.Arrays;
import org.enon.EnonConfig;
import org.enon.bind.value.BlobValue.Acceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class BlobValueTest {

	public BlobValueTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getBytes method, of class BlobValue.
	 */
	@Test
	public void testAll() {
		byte[] bytes = "This is a byte array".getBytes();

		Acceptor acceptor = new Acceptor(EnonConfig.defaults());
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(byte[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(Byte[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(null));

		BlobValue value = acceptor.toNativeValue(byte[].class, bytes);

		assertSame(bytes, value.getValue());

		assertTrue(value.toString().contains(String.valueOf(bytes.length)));
	}

}
