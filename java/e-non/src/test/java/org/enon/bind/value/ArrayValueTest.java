/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.bind.value.ArrayValue.Acceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class ArrayValueTest {

	public ArrayValueTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testAll() {

		Acceptor acceptor0 = new Acceptor(EnonConfig.defaults());
		Acceptor acceptorX = new Acceptor(new EnonConfig.Builder(FeatureSet.X).build());
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptorX.accept(boolean[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptorX.accept(int[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptorX.accept(long[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptorX.accept(double[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptorX.accept(float[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptorX.accept(short[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.IF_NECESSARY, acceptorX.accept(byte[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptorX.accept(char[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptorX.accept(Byte[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptorX.accept(String.class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptorX.accept(null));

		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(boolean[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(int[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(long[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(double[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(float[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(short[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(byte[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(char[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(Byte[].class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(String.class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor0.accept(null));

		int[] ints = new int[]{0, Integer.MIN_VALUE, Integer.MAX_VALUE};

		ArrayValue value = acceptor0.toNativeValue(int[].class, ints);

		assertSame(ints, value.getValue());
		assertTrue(value.toString().contains(ints.getClass().getComponentType().getSimpleName()));
	}

}
