/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.enon.EnonConfig;
import org.enon.bind.value.ScalarValue.Acceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class ScalarValueTest {

	public ScalarValueTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testAcceptorTest() {
		ScalarValue.Acceptor acceptor = new Acceptor(EnonConfig.defaults());
		for (Class c : acceptor.acceptsClasses().keySet()) {
			assertSame(ValueTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(c));
		}

		for (Class c : new Class[]{null, boolean[].class, char[].class, byte[].class,
			short[].class, int[].class, long[].class, float[].class, double[].class,
		String.class, char.class, Character.class, Character[].class}) {
			assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(c));
		}
	}

	@Test
	public void testAccpetorApply() {
		ScalarValue.Acceptor acceptor = new Acceptor(EnonConfig.defaults());
		Object[] objects = new Object[]{
			Boolean.FALSE,
			(byte) -14,
			(short) 1234,
			123456,
			123456789L,
			98.87f,
			98765.4321,
			new BigInteger("1234567890123456789"),
			new BigDecimal("1234567890.123456789")
		};
		for (Object object : objects) {
			assertNotSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(object.getClass()));
			ScalarValue value = acceptor.toNativeValue(object.getClass(), object);
			assertEquals(object, value.getValue());
			assertTrue(value.toString().contains(object.getClass().getSimpleName()));
		}
	}

	@Test
	public void testAccpetorApplyPrimitive() {
		ScalarValue.Acceptor acceptor = new Acceptor(EnonConfig.defaults());
		Map<Class, Object> primitives = new HashMap<>();
		primitives.put(boolean.class, true);
		primitives.put(byte.class, (byte) 1);
		primitives.put(short.class, (short) 245);
		primitives.put(int.class, -548764);
		primitives.put(long.class, 55487981L);
		primitives.put(float.class, -19.7f);
		primitives.put(double.class, 4564.5541);
		for (Entry<Class, Object> entry : primitives.entrySet()) {
			ScalarValue value = acceptor.toNativeValue(entry.getKey(), entry.getValue());
			assertEquals(entry.getValue(), value.getValue());
			assertTrue(value.toString().contains(entry.getKey().getSimpleName()));
		}
	}
}
