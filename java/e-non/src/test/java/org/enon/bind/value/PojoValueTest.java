/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.Map;
import java.util.Map.Entry;
import org.enon.EnonConfig;
import org.enon.bind.value.PojoValue.Acceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class PojoValueTest {

	public PojoValueTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getPojo method, of class PojoValue.
	 */
	@Test
	public void testAll() {
		final String name = "Test pojo";
		Pojo pojo = new Pojo(name);

		Acceptor acceptor = new Acceptor(EnonConfig.defaults());
		assertSame(ValueTypeAcceptor.AcceptLevel.LAST_RESORT, acceptor.accept(Pojo.class));
		assertSame(ValueTypeAcceptor.AcceptLevel.LAST_RESORT, acceptor.accept(Object.class));
		assertSame(ValueTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(null));

		PojoValue value = acceptor.toNativeValue(pojo.getClass(), pojo);
		Map<StringValue, NativeValue> pojoMap = value.getValue();

		assertSame(pojo, value.getPojo());
		assertTrue(Map.class.isAssignableFrom(value.getDeclaredType()));

		assertEquals(2, pojoMap.size());

		for (Entry<StringValue, NativeValue> entry : pojoMap.entrySet()) {
			switch (entry.getKey().getValue()) {
				case "name":
					assertEquals(name, entry.getValue().getValue());
					break;
				case "size":
					assertEquals(9, entry.getValue().getValue());
					break;
				default:
					fail("Unexpected property: " + entry.getKey().getValue());
			}

			assertTrue(entry.getKey().toString().contains(entry.getKey().getValue()));

			assertSame(entry.getValue().getValue().getClass(), entry.getValue().getDeclaredType());
		}

		// should retuen same map instance next time
		assertSame(pojoMap, value.getValue());

		assertTrue(value.toString().contains(pojo.getClass().getSimpleName()));
	}

	public static class Pojo {

		private String name;
		private int size;

		public Pojo(String name) {
			this.name = name;
			this.size = name.length();
		}

		public String getName() {
			return name;
		}

		public int getSize() {
			return size;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setSize(int size) {
			this.size = size;
		}
	}
}
