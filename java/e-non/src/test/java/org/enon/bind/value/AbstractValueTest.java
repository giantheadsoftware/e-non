/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import java.util.Date;
import org.enon.codec.DataCategory;
import org.enon.exception.EnonException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class AbstractValueTest {

	public AbstractValueTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getCategory method, of class AbstractValue.
	 */
	@Test
	public void testGetCategory() {
		System.out.println("getCategory");
		AbstractValue instance = new AbstractValueImpl(DataCategory.TEMPORAL, Date.class, new Date());
		assertEquals(DataCategory.TEMPORAL, instance.getCategory());
	}

	/**
	 * Test of getDeclaredType method, of class AbstractValue.
	 */
	@Test
	public void testGetDeclaredType() {
		System.out.println("getDeclaredType");
		AbstractValue instance = new AbstractValueImpl(DataCategory.TEMPORAL, Date.class, new Date());
		assertSame(Date.class, instance.getDeclaredType());
	}

	/**
	 * Test of getValue method, of class AbstractValue.
	 */
	@Test
	public void testGetValue() {
		System.out.println("getValue");
		Date value = new Date();
		AbstractValue instance = new AbstractValueImpl(DataCategory.TEMPORAL, value.getClass(), value);
		assertSame(value, instance.getValue());

		assertTrue(instance.toString().contains(value.toString()));
	}

	@Test(/*expected = EnonException.class*/)
	public void testConstructorNoCatFail() {
		Exception x = assertThrows(EnonException.class, () -> {
			new AbstractValueImpl(null, Date.class, new Date());
		});
	}

	@Test(/*expected = EnonException.class*/)
	public void testConstructorNoTypeFail() {
		Exception x = assertThrows(EnonException.class, () -> {
			new AbstractValueImpl(DataCategory.TEMPORAL, null, new Date());
		});
	}

	@Test(/*expected = EnonException.class*/)
	public void testConstructorNoValueFail() {
		Exception x = assertThrows(EnonException.class, () -> {
			new AbstractValueImpl(DataCategory.TEMPORAL, Date.class, null);
		});
	}

	public class AbstractValueImpl extends AbstractValue {

		public AbstractValueImpl(DataCategory category, Class type, Object value) {
			super(category, type, value);
		}

	}

}
