/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.value;

import org.enon.codec.DataCategory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class NullNativeValueTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(NullNativeValueTest.class.getName());

	public NullNativeValueTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getCategory method, of class NullNativeValue.
	 */
	@Test
	public void testGetCategory() {
		LOGGER.info("getCategory");

		assertEquals(DataCategory.NULL, NullNativeValue.INSTANCE.getCategory());
	}

	/**
	 * Test of getDeclaredType method, of class NullNativeValue.
	 */
	@Test
	public void testGetDeclaredType() {
		LOGGER.info("getDeclaredType");

		assertEquals(Void.class, NullNativeValue.INSTANCE.getDeclaredType());
	}

	/**
	 * Test of getValue method, of class NullNativeValue.
	 */
	@Test
	public void testGetValue() {
		LOGGER.info("getValue");
		assertNull(NullNativeValue.INSTANCE.getValue());
	}

}
