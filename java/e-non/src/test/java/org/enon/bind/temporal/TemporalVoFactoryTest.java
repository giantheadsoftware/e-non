/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.temporal;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.function.BiConsumer;
import static org.enon.bind.temporal.TemporalVO.*;
import org.enon.exception.EnonCoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TemporalVoFactoryTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(TemporalVoFactoryTest.class.getName());

	private final TemporalVoFactory factory = TemporalVoFactory.getInstance();

	public TemporalVoFactoryTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of create method, of class TemporalMapFactory.
	 */
	@Test
	public void testCreate() {
		LOGGER.info("create");
		// collect tests in a Map
		Map<Object, BiConsumer<Object, TemporalVO>> tests = new LinkedHashMap<>();

		final Date now = new Date();
		// current time instance
		tests.put(now, (object, tvo) -> {
			assertEquals(((Date) object).getTime(), (long) tvo.instant);
		});

		// current date calendar
		tests.put(new Calendar.Builder().setInstant(now).build(), (object, tvo) -> {
			assertEquals(INSTANT_MASK + ZONEID_MASK, tvo.fieldMask);
			assertEquals(((Calendar) object).getTimeInMillis(), (long) tvo.instant);
			assertEquals(ZoneId.systemDefault().getId(), tvo.zoneId);
		});

		// PM time calendar
		tests.put(new Calendar.Builder().setTimeOfDay(14, 05, 23).build(), (object, tvo) -> {
			assertEquals(INSTANT_MASK + ZONEID_MASK, tvo.fieldMask);
			assertEquals(((Calendar) object).getTimeInMillis(), (long) tvo.instant);
			assertEquals(ZoneId.systemDefault().getId(), tvo.zoneId);
			// should not include time/date fields
			assertNull(tvo.year);
			assertNull(tvo.month);
			assertNull(tvo.day);
			assertEquals(0, tvo.hour.longValue());
			assertEquals(0, tvo.minute.longValue());
			assertEquals(0, tvo.sec.longValue());
			assertNull(tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(0, tvo.nanos.longValue());
		});

		// year-day calendar
		tests.put(new Calendar.Builder().setFields(Calendar.YEAR, 1927, Calendar.DAY_OF_YEAR, 75).build(), (object, tvo) -> {
			assertEquals(INSTANT_MASK + ZONEID_MASK, tvo.fieldMask);
			assertEquals(((Calendar) object).getTimeInMillis(), (long) tvo.instant);
			assertEquals(ZoneId.systemDefault().getId(), tvo.zoneId);
			// should not include time/date fields
			assertNull(tvo.year);
			assertNull(tvo.month);
			assertNull(tvo.day);
			assertEquals(0, tvo.hour.longValue());
			assertEquals(0, tvo.minute.longValue());
			assertEquals(0, tvo.sec.longValue());
			assertNull(tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(0, tvo.nanos.longValue());
		});

		// month-day
		tests.put(MonthDay.of(Month.MARCH, 14), (object, tvo) -> {
			assertEquals(MONTH_MASK + DAY_MASK, tvo.fieldMask);
			assertEquals(((MonthDay) object).getMonthValue(), (int) tvo.month);
			assertEquals(((MonthDay) object).getDayOfMonth(), (int) tvo.day);
			assertNull(tvo.zoneId);
			assertNull(tvo.year);
			assertEquals(0, tvo.hour.longValue());// should not include time of day fields
			assertEquals(0, tvo.minute.longValue());// should not include time of day fields
			assertEquals(0, tvo.sec.longValue());// should not include time of day fields
			assertNull(tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(0, tvo.nanos.longValue());// should not include time of day fields
		});

		// DST offset calendar
		tests.put(new Calendar.Builder().setDate(2019, 1, 14).setTimeOfDay(13, 25, 15).setTimeZone(TimeZone.getTimeZone("America/New_York")).build(), (cal, tvo) -> {
			assertEquals(INSTANT_MASK + ZONEID_MASK, tvo.fieldMask);
			assertEquals(((Calendar) cal).getTimeInMillis(), (long) tvo.instant);
			assertEquals(((Calendar) cal).getTimeZone().getID(), tvo.zoneId);
			// should not include time/date fields
			assertNull(tvo.year);
			assertNull(tvo.month);
			assertNull(tvo.day);
			assertEquals(0, tvo.hour.longValue());
			assertEquals(0, tvo.minute.longValue());
			assertEquals(0, tvo.sec.longValue());
			assertNull(tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(0, tvo.nanos.longValue());
		});

		// ZonedDateTime
		tests.put(ZonedDateTime.of(2019, 3, 18, 16, 17, 54, 0, ZoneId.of("America/Chicago")), (zdt, tvo) -> {
			assertEquals(INSTANT_MASK + ZONEID_MASK, tvo.fieldMask);
			assertEquals(((ZonedDateTime) zdt).toInstant().toEpochMilli(), (long) tvo.instant);
			assertEquals("America/Chicago", tvo.zoneId);
			// should not include time/date fields
			assertNull(tvo.year);
			assertNull(tvo.month);
			assertNull(tvo.day);
			assertEquals(0, tvo.hour.longValue());
			assertEquals(0, tvo.minute.longValue());
			assertEquals(0, tvo.sec.longValue());
			assertNull(tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(0, tvo.nanos.longValue());
		});

		// OffsetDateTime -- will convert 20,000,000ns to 20ms
		tests.put(OffsetDateTime.of(2019, 3, 18, 16, 17, 54, 20000000, ZoneOffset.UTC), (odt, tvo) -> {
			assertEquals(INSTANT_MASK + OFFSET_MASK, tvo.fieldMask);
			assertEquals(((OffsetDateTime) odt).toInstant().toEpochMilli(), (long) tvo.instant);
			assertEquals(0, (int) tvo.offsetSeconds);
			// should not include time/date fields
			assertNull(tvo.year);
			assertNull(tvo.month);
			assertNull(tvo.day);
			assertEquals(0, tvo.hour.longValue());
			assertEquals(0, tvo.minute.longValue());
			assertEquals(0, tvo.sec.longValue());
			assertNull(tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(0, tvo.nanos.longValue());
		});

		// OffsetDateTime -- this one has 5ns: can't produce millis
		tests.put(OffsetDateTime.of(2019, 3, 18, 16, 17, 54, 5, ZoneOffset.UTC), (odt, tvo) -> {
			assertEquals(INSTANT_MASK + NANO_MASK + OFFSET_MASK, tvo.fieldMask);
			assertEquals(((OffsetDateTime) odt).toInstant().toEpochMilli(), (long) tvo.instant);
			assertEquals(0, (int) tvo.offsetSeconds);
			assertNull(tvo.zoneId);
			assertNull(tvo.year);
			assertNull(tvo.month);
			assertNull(tvo.day);
			assertEquals(0, tvo.hour.longValue());
			assertEquals(0, tvo.minute.longValue());
			assertEquals(0, tvo.sec.longValue());
			assertNull(tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(5, (int) tvo.nanos);
			assertEquals(0, (int) tvo.offsetSeconds);
		});

		// LocalDateTime -- does not support INSTANT and has no zone offset
		tests.put(LocalDateTime.of(2019, 3, 18, 16, 17, 54, 50000), (ldt, tvo) -> {
			assertEquals(FULL_DATE_TIME_NS_MASK, tvo.fieldMask);
			assertNull(tvo.instant);
			assertNull(tvo.zoneId);
			assertEquals(2019, (int) tvo.year);
			assertEquals(3, (int) tvo.month);
			assertEquals(18, (int) tvo.day);
			assertEquals(16, (int) tvo.hour);
			assertEquals(17, (int) tvo.minute);
			assertEquals(54, (int) tvo.sec);
			assertNull(tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(50000, (int) tvo.nanos);
			assertNull(tvo.offsetSeconds);
		});

		// LocalDateTime -- use MS_IN_MINUTE rather then sec+nanos
		tests.put(LocalDateTime.of(2019, 3, 18, 16, 17, 54, 50000000), (ldt, tvo) -> {
			assertEquals(FULL_DATE_TIME_MS_MASK, tvo.fieldMask);
			assertNull(tvo.instant);
			assertNull(tvo.zoneId);
			assertEquals(2019, (int) tvo.year);
			assertEquals(3, (int) tvo.month);
			assertEquals(18, (int) tvo.day);
			assertEquals(16, (int) tvo.hour);
			assertEquals(17, (int) tvo.minute);
			assertEquals(54, (int) tvo.sec);
			assertEquals(54050, tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(50000000, (int) tvo.nanos);
			assertNull(tvo.offsetSeconds);
		});

		// OffsetDateTime
		tests.put(OffsetTime.of(16, 17, 54, 5, ZoneOffset.UTC), (odt, tvo) -> {
			assertEquals(TIME_S_MASK + NANO_MASK + OFFSET_MASK, tvo.fieldMask);
			assertNull(tvo.instant);
			assertNull(tvo.zoneId);
			// should not include time/date fields
			assertNull(tvo.year);
			assertNull(tvo.month);
			assertNull(tvo.day);
			assertEquals(16, (int) tvo.hour);
			assertEquals(17, (int) tvo.minute);
			assertEquals(54, (int) tvo.sec);
			assertNull(tvo.get(EnonTemporalField.MS_IN_MINUTE));
			assertEquals(5, (int) tvo.nanos);
			assertEquals(0, (int) tvo.offsetSeconds);
		});

		// run the tests
		for (Map.Entry<Object, BiConsumer<Object, TemporalVO>> test : tests.entrySet()) {
			TemporalVO result = factory.create(test.getKey());
			LOGGER.info("Temporal thing " + test.getKey().getClass().getSimpleName() + " produced VO " + result.toString());
			// run the test provided
			test.getValue().accept(test.getKey(), result);
		}
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testCreateInvalidDateObjectFail() {
		LOGGER.info("createInvalidDateObjectFail");

		Exception x = assertThrows(EnonCoderException.class, () -> {
			factory.create("String is not a temporal type");
		});
	}

}
