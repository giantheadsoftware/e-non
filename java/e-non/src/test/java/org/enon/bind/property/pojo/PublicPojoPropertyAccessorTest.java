/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.bind.property.pojo;

import java.util.HashSet;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.bind.property.ReadableProperty;
import org.enon.bind.property.WritableProperty;
import org.enon.bind.value.DefaultNativeValueFactory;
import org.enon.bind.value.NativeValueFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class PublicPojoPropertyAccessorTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(PublicPojoPropertyAccessorTest.class.getName());

	private final NativeValueFactory nativeValueFactory = new DefaultNativeValueFactory(EnonConfig.defaults());
	
	public PublicPojoPropertyAccessorTest() {
	}
	
	@BeforeAll
	public static void setUpClass() {
	}
	
	@AfterAll
	public static void tearDownClass() {
	}
	
	@BeforeEach
	public void setUp() {
	}
	
	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of streamReadableProperties method, of class PublicPojoPropertyAccessor.
	 */
	@Test
	public void testStreamReadableProperties() {
		LOGGER.info("streamReadableProperties");
		PublicPojoPropertyAccessor accessor = new PublicPojoPropertyAccessor(nativeValueFactory);
		
		Pojo pojo = new Pojo("testing", 45, true);
		Set<String> propertiesAccessed = new HashSet<>();
		accessor.getReadableProperties(Pojo.class).entrySet().stream().forEach(entry -> {
			String key = entry.getKey();
			ReadableProperty prop = entry.getValue();
			propertiesAccessed.add(prop.getName());
			switch(prop.getName()) {
				case "name":
					assertEquals(pojo.name, prop.readValue(pojo).getValue().getValue());
					break;
				case "quality":
					assertEquals(pojo.quality, prop.readValue(pojo).getValue().getValue());
					break;
				case "available":
					assertEquals(pojo.available, prop.readValue(pojo).getValue().getValue());
					break;
				default:
					fail("Should not receive property "+prop.getName());
			}
		});
		assertEquals(3, propertiesAccessed.size());
	}

	/**
	 * Test of getwritableProperties method, of class PublicPojoPropertyAccessor.
	 */
	@Test
	public void testGetwritableProperties() {
		LOGGER.info("getwritableProperties");
		PublicPojoPropertyAccessor accessor = new PublicPojoPropertyAccessor(nativeValueFactory);
		
		Pojo pojo = new Pojo();
		Set<String> propertiesAccessed = new HashSet<>();
		accessor.getwritableProperties(Pojo.class).entrySet().stream().forEach(entry -> {
			String key = entry.getKey();
			WritableProperty prop = entry.getValue();
			assertEquals(key, prop.getName());
			propertiesAccessed.add(key);
			switch(prop.getName()) {
				case "name":
					prop.writeValue(pojo, "test writing");
					assertEquals("test writing", pojo.name);
					break;
				case "quality":
					prop.writeValue(pojo, 97);
					assertEquals(97, pojo.quality);
					break;
				case "available":
					prop.writeValue(pojo, true);
					assertEquals(true, pojo.available);
					break;
				default:
					fail("Should not receive property "+prop.getName());
			}
		});
		assertEquals(3, propertiesAccessed.size());
	}
	
	@Test
	public void testPojoCache() {
		LOGGER.info("getwritableProperties");
		PublicPojoPropertyAccessor accessor = new PublicPojoPropertyAccessor(nativeValueFactory);
		
		assertSame(accessor.getwritableProperties(Pojo.class), accessor.getReadableProperties(Pojo.class));
	}
	
	private static class Pojo {
		private String name;
		private int quality;
		private boolean available;

		public Pojo() {
		}

		public Pojo(String name, int quality, boolean available) {
			this.name = name;
			this.quality = quality;
			this.available = available;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getQuality() {
			return quality;
		}

		public void setQuality(int quality) {
			this.quality = quality;
		}

		public boolean isAvailable() {
			return available;
		}

		public void setAvailable(boolean available) {
			this.available = available;
		}
		
		// not valid getter
		public int get() {
			return 3;
		}
		
		// not valid getter
		public boolean is() {
			return false;
		}
		
		// not valid getter
		public int getter() {
			return 3;
		}
		
		// not valid getter
		public boolean isnt() {
			return false;
		}
		
		// not valid getter
		public Boolean isNot() {
			return false;
		}
		
		// getter & setter type do not match
		public String getSomething() {
			return null;
		}
		
		public void setSomething(Integer i) {
			
		}
	}
}
