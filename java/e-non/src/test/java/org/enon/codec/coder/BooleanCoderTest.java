/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.math.BigDecimal;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.bind.value.PojoValue;
import org.enon.bind.value.ScalarValue;
import org.enon.bind.value.StringValue;
import org.enon.codec.DataCategory;
import org.enon.element.ConstantElement;
import org.enon.exception.EnonCoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class BooleanCoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(BooleanCoderTest.class.getName());

	private static final ScalarValue<Boolean> trueValue = new ScalarValue.Acceptor(EnonConfig.defaults()).toNativeValue(Boolean.class, Boolean.TRUE);
	private static final ScalarValue<Boolean> falseValue = new ScalarValue.Acceptor(EnonConfig.defaults()).toNativeValue(Boolean.class, Boolean.FALSE);

	public BooleanCoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of handles method, of class BooleanCoder.
	 */
	@Test
	public void testHandles() {
		LOGGER.info("handles");
		BooleanCoder instance = new BooleanCoder(EnonConfig.defaults());
		Set<DataCategory> result = instance.handles();
		assertEquals(1, result.size());
		assertTrue(result.contains(DataCategory.BOOLEAN));
	}

	/**
	 * Test of getElement method, of class BooleanCoder.
	 */
	@Test
	public void testGetElement() {
		LOGGER.info("getElement");
		BooleanCoder instance = new BooleanCoder(EnonConfig.defaults());
		ConstantElement t = (ConstantElement) instance.getElement(trueValue);
		instance = new BooleanCoder(EnonConfig.defaults());
		ConstantElement f = (ConstantElement) instance.getElement(falseValue);
	}

	/**
	 * Test of extractBoolean method, of class BooleanCoder.
	 */
	@Test
	public void testExtractBoolean() {
		LOGGER.info("extractBoolean");
		BooleanCoder coder = new BooleanCoder(EnonConfig.defaults());
		for (String t : coder.trueStrings()) {
			assertTrue(coder.extractBoolean(createStringValue(t)));
		}

		for (String f : coder.falseStrings()) {
			assertFalse(coder.extractBoolean(createStringValue(f)));
		}

		for (String t : new String[]{"tRUe", "oN"}) {
			assertTrue(coder.extractBoolean(createStringValue(t)));
		}

		for (String f : new String[]{"fALSe", "oFf"}) {
			assertFalse(coder.extractBoolean(createStringValue(f)));
		}

		for (Character t : new Character[]{'t', 'T', '1'}) {
			assertTrue(coder.extractBoolean(createCharValue(t)));
		}

		for (Character f : new Character[]{'f', 'F', '0'}) {
			assertFalse(coder.extractBoolean(createCharValue(f)));
		}

		for (Number t : new Number[]{1, 1.0, 5, 12.9f, new BigDecimal("12345")}) {
			assertTrue(coder.extractBoolean(createNumberValue(t)));
		}

		for (Number f : new Number[]{0, 0L, 0.0, new BigDecimal("0.0")}) {
			assertFalse(coder.extractBoolean(createNumberValue(f)));
		}
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testExtractBooleanStringFail() {
		LOGGER.info("extractBooleanFail");
		Exception x = assertThrows(EnonCoderException.class, () -> {
			new BooleanCoder(EnonConfig.defaults()).extractBoolean(createStringValue("nothing"));
		});
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testExtractBooleanCharFail() {
		LOGGER.info("extractBooleanFail");
		Exception x = assertThrows(EnonCoderException.class, () -> {
			new BooleanCoder(EnonConfig.defaults()).extractBoolean(createCharValue('a'));
		});
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testExtractBooleanFail() {
		LOGGER.info("extractBooleanFail");
		Exception x = assertThrows(EnonCoderException.class, () -> {
			new BooleanCoder(EnonConfig.defaults()).extractBoolean(createPojoValue(new Object()));
		});
	}

	private StringValue createStringValue(String value) {
		return new StringValue.Acceptor(EnonConfig.defaults()).toNativeValue(String.class, value);
	}

	private StringValue createCharValue(Character value) {
		return new StringValue.Acceptor(EnonConfig.defaults()).toNativeValue(Character.class, value);
	}

	private ScalarValue<Number> createNumberValue(Number value) {
		return new ScalarValue.Acceptor(EnonConfig.defaults()).toNativeValue(value.getClass(), value);
	}

	private PojoValue createPojoValue(Object value) {
		return new PojoValue.Acceptor(EnonConfig.defaults()).toNativeValue(value.getClass(), value);
	}
}
