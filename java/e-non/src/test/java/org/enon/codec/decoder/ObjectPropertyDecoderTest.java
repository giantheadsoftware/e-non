/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.enon.EnonConfig;
import org.enon.ReaderContext;
import org.enon.bind.property.WriteAccessor;
import org.enon.element.ConstantElement;
import org.enon.element.Element;
import org.enon.element.IntElement;
import org.enon.element.ListElement;
import org.enon.element.MapElement;
import org.enon.element.NumberElement;
import org.enon.element.RootContainer;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.enon.test.util.ExceptionInputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ObjectPropertyDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ObjectPropertyDecoderTest.class.getName());

	private final ObjectPropertyDecoder.Acceptor acceptor = new ObjectPropertyDecoder.Acceptor(EnonConfig.defaults());

	private ReaderContext ctx;
	private RootContainer root;

	public ObjectPropertyDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		ctx = new ReaderContext(ExceptionInputStream.INSTANCE);
		root = ctx.getRoot();
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class ObjectPropertyDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");

		Decoder<PublicPojo> decoder = acceptor.decoderFor(PublicPojo.class);

		List<Integer> sizes = Arrays.asList(new Integer[]{3, 6, 9});
		ListElement sizesElement = new ListElement(sizes.stream().map(IntElement::new).collect(Collectors.toList()));

		Map<StringElement, Element> data = new LinkedHashMap<>();
		data.put(new StringElement("name"), new StringElement("Tracy"));
		data.put(new StringElement("id"), new StringElement("9876541230"));
		data.put(new StringElement("sizes"), sizesElement);
		data.put(new StringElement("any"), new NumberElement(BigInteger.valueOf(Integer.MAX_VALUE)));
		data.put(new StringElement("unknown"), ConstantElement.falseInstance()); // not part of Pojo class

		MapElement me = new MapElement((Map) data);
		me.inContainer(root);
		sizesElement.inContainer(me);

		PublicPojo pojo = decoder.decode(me);

		assertNotNull(pojo);
		assertEquals("Tracy", pojo.getName());
		assertEquals(9876541230L, pojo.getId());
		assertEquals(sizes, pojo.getSizes());
		assertEquals(Integer.MAX_VALUE, pojo.getAny());
		assertNull(pojo.getMissing());
	}

	@Test
	public void testDecodeNull() {
		LOGGER.info("decodeNull");
		Decoder<PublicPojo> decoder = acceptor.decoderFor(PublicPojo.class);

		assertNull(decoder.decode(ConstantElement.nullInstance()));
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeInvalidElementFail() {
		LOGGER.info("decode");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			Decoder<PublicPojo> decoder = acceptor.decoderFor(PublicPojo.class);

			decoder.decode(ConstantElement.trueInstance());
		});
	}

	/**
	 * Test of newO method, of class ObjectPropertyDecoder.
	 *
	 * @throws java.lang.NoSuchMethodException
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testNewOPrivateConstructorFail() throws NoSuchMethodException {
		LOGGER.info("newO");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			Constructor<PrivateConstructorPojo> c = PrivateConstructorPojo.class.getDeclaredConstructor();
			WriteAccessor accessor = EnonConfig.defaults().getPropertyAccessorFactory().getPojoWriteAccessor();
			ObjectPropertyDecoder<PrivateConstructorPojo> decoder = new ObjectPropertyDecoder<>(c, accessor);

			decoder.newO();
		});
	}

	@Test
	public void testFactoryNull() {
		LOGGER.info("factoryNull");
		assertNull(acceptor.decoderFor((Type) null));
		assertNull(acceptor.decoderFor((String) null));
	}

	@Test
	public void testFactoryInvalidPojo() {
		LOGGER.info("factoryInvalidPojo");
		// should reject abstract class
		assertNull(acceptor.decoderFor(AbstractPojo.class));

		// should reject private class
		assertNull(acceptor.decoderFor(PrivatePojo.class));

		// should reject class with private constructor
		assertNull(acceptor.decoderFor(PrivateConstructorPojo.class));
	}

	@Test
	public void testFactoryCache() {
		LOGGER.info("factoryCache");
		// requesting the same type 2x should return same instance
		ObjectPropertyDecoder<PublicPojo> decoder = acceptor.decoderFor(PublicPojo.class);
		assertNotNull(decoder);
		assertSame(decoder, acceptor.decoderFor(PublicPojo.class));
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static abstract class AbstractPojo {

		private String name;
		private long id;

		public AbstractPojo() {
		}

		public AbstractPojo(String name, long id) {
			this.name = name;
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static class PublicPojo extends AbstractPojo {

		private List<Integer> sizes;
		private Object any;
		private Object missing;

		public PublicPojo() {
		}

		public PublicPojo(String name, long id) {
			super(name, id);
		}

		public PublicPojo(String name, long id, List<Integer> sizes, Object any, Object missing) {
			super(name, id);
			this.sizes = sizes;
			this.any = any;
			this.missing = missing;
		}

		public List<Integer> getSizes() {
			return sizes;
		}

		public void setSizes(List<Integer> sizes) {
			this.sizes = sizes;
		}

		public Object getAny() {
			return any;
		}

		public void setAny(Object any) {
			this.any = any;
		}

		public Object getMissing() {
			return missing;
		}

		public void setMissing(Object missing) {
			this.missing = missing;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	private static class PrivatePojo {

		public PrivatePojo() {
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static class PrivateConstructorPojo extends AbstractPojo {

		private PrivateConstructorPojo() {
		}
	}
}
