/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.util.Collections;
import org.enon.element.ByteElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.ListElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class IntDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntDecoderTest.class.getName());

	private final IntDecoder.Acceptor acceptor = new IntDecoder.Acceptor();

	public IntDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class IntDecoder.
	 */
	@Test
	public void testDecodeNull() {
		LOGGER.info("decode");

		IntDecoder decoder = acceptor.decoderFor(Byte.class.getName());
		Number value = decoder.decode(ConstantElement.nullInstance());

		assertNull(value);

	}

	/**
	 * Test of decode method, of class IntDecoder.
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeNullPrimitiveFail() {
		LOGGER.info("decode");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			IntDecoder decoder = acceptor.decoderFor(byte.class.getName());

			decoder.decode(ConstantElement.nullInstance());
		});
	}

	/**
	 * Test of decode method, of class IntDecoder.
	 */
	@Test
	public void testDecodeByte() {
		LOGGER.info("decode");

		IntDecoder decoder = acceptor.decoderFor(byte.class.getName());

		Number value = decoder.decode(new ByteElement((byte) -23));
		assertTypeAndValue(Byte.class, (byte) -23, value);

		decoder = acceptor.decoderFor(Byte.class.getName());

		value = decoder.decode(new ByteElement((byte) -23));
		assertTypeAndValue(Byte.class, (byte) -23, value);

		value = decoder.decode(new NanoIntElement((byte) 62));
		assertTypeAndValue(Byte.class, (byte) 62, value);

		value = decoder.decode(new NumberElement("-23"));
		assertTypeAndValue(Byte.class, (byte) -23, value);

		value = decoder.decode(new StringElement("-23"));
		assertTypeAndValue(Byte.class, (byte) -23, value);

		value = decoder.decode(new StringElement('7'));
		assertTypeAndValue(Byte.class, (byte) 7, value);

		value = decoder.decode(new FloatElement(7.0f));
		assertTypeAndValue(Byte.class, (byte) 7, value);

		value = decoder.decode(new DoubleElement(-7.0f));
		assertTypeAndValue(Byte.class, (byte) -7, value);

		value = decoder.decode(ConstantElement.falseInstance());
		assertTypeAndValue(Byte.class, (byte) 0, value);

		value = decoder.decode(ConstantElement.trueInstance());
		assertTypeAndValue(Byte.class, (byte) 1, value);
	}

	private void assertTypeAndValue(Class expectedClass, Number expectedValue, Number value) {
		assertSame(expectedClass, value.getClass());
		assertEquals(expectedValue, value);
	}

	/**
	 * Test of decode method, of class IntDecoder.
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeByteTooBigFail() {
		LOGGER.info("decodeByteTooBigFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			IntDecoder decoder = acceptor.decoderFor(byte.class.getName());

			decoder.decode(new IntElement(230));
		});
	}

	/**
	 * Test of decode method, of class IntDecoder.
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeByteTooBigDoubleFail() {
		LOGGER.info("decodeByteTooBigDoubleFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			IntDecoder decoder = acceptor.decoderFor(byte.class);

			decoder.decode(new DoubleElement(230.5));
		});
	}

	/**
	 * Test of decode method, of class IntDecoder.
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeByteBadStringFail() {
		LOGGER.info("ecodeByteBadStringFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			IntDecoder decoder = acceptor.decoderFor(Byte.class);

			decoder.decode(new StringElement("hello"));
		});
	}

	/**
	 * Test of decode method, of class IntDecoder.
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeByteBadCharFail() {
		LOGGER.info("decodeByteBadCharFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			IntDecoder decoder = acceptor.decoderFor(byte.class.getName());

			decoder.decode(new StringElement('h'));
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeListFail() {
		LOGGER.info("decodeListFail");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			acceptor.decoderFor(byte.class).decode(new ListElement(Collections.EMPTY_LIST));
		});
	}

	@Test
	public void testFactoryCache() {
		LOGGER.info("factoryCache");
		assertSame(acceptor.decoderFor(Integer.class), acceptor.decoderFor(Integer.class.getName()));
	}

	@Test
	public void testFactoryNull() {
		LOGGER.info("factoryNull");
		assertNull(acceptor.decoderFor(""));
		assertNull(acceptor.decoderFor(String.class));
		assertNull(acceptor.decoderFor(Float.class));
	}
}
