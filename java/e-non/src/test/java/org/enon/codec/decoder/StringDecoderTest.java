/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import org.enon.element.ConstantElement;
import org.enon.element.LongElement;
import org.enon.element.StringElement;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class StringDecoderTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(StringDecoderTest.class.getName());

	public StringDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class StringDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");

		StringDecoder decoder = new StringDecoder.Acceptor().decoderFor(String.class);

		assertEquals("test string", decoder.decode(new StringElement("test string")));

		assertEquals("12345", decoder.decode(new LongElement(12345)));

		assertEquals("true", decoder.decode(ConstantElement.trueInstance()));

		assertNull(decoder.decode(ConstantElement.nullInstance()));

	}

}
