/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.math.BigDecimal;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.bind.value.PojoValue;
import org.enon.bind.value.ScalarValue;
import org.enon.bind.value.StringValue;
import org.enon.codec.DataCategory;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.exception.EnonCoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class RealCoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(RealCoderTest.class.getName());

	public RealCoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of handles method, of class RealCoder.
	 */
	@Test
	public void testHandles() {
		LOGGER.info("handles");
		RealCoder instance = new RealCoder(EnonConfig.defaults());
		Set<DataCategory> result = instance.handles();
		assertEquals(1, result.size());
		assertTrue(result.contains(DataCategory.NUM_REAL));
	}

	/**
	 * Test of extractBoolean method, of class RealCoder.
	 */
	@Test
	public void testGetElement() {
		LOGGER.info("getElement");
		EnonConfig configX = EnonConfig.extended();

		String[] intStrings = new String[]{"0.0", "-3.0", "12439875232.343"};
		for (String t : intStrings) {
			assertEquals(new BigDecimal(t), new BigDecimal(String.valueOf(new RealCoder(EnonConfig.defaults()).getElement(createStringValue(t)).getValue())));
		}
		for (String t : intStrings) {
			assertEquals(new BigDecimal(t), new BigDecimal(String.valueOf(new RealCoder(configX).getElement(createStringValue(t)).getValue())));
		}

		Character[] digits = new Character[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		for (Character d : digits) {
			assertEquals(d.toString(), new RealCoder(configX).getElement(createCharValue(d)).getValue());
		}

		Number[] reals = new Number[]{0.0, -5.75f, -543.0265412, 75201.54987, new BigDecimal("59516516987.123456")};
		// test enon0
		for (Number i : reals) {
			Element e = new RealCoder(EnonConfig.defaults()).getElement(createNumberValue(i));
			if (i instanceof Double) {
				assertSame(ElementType.DOUBLE_ELEMENT, e.getType());
			} else if (i instanceof Float) {
				assertSame(ElementType.NUMBER_ELEMENT, e.getType());
			}
			assertEquals(i.toString(), String.valueOf(e.getValue()));
		}
		// test enonX
		for (Number i : reals) {
			Element e = new RealCoder(configX).getElement(createNumberValue(i));
			if (i instanceof Double) {
				assertSame(ElementType.DOUBLE_ELEMENT, e.getType());
			} else if (i instanceof Float) {
				assertSame(ElementType.FLOAT_ELEMENT, e.getType());
			}
			assertEquals(i.toString(), String.valueOf(e.getValue()));
		}
	}

	@Test
	public void testGetElementExtendedFeatureSet() {
		LOGGER.info("getElementExtended");
		Number[] ext = new Number[]{Float.NaN, Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY, Double.NaN, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
		// test enon0
		for (Number i : ext) {
			try {
				Element e = new RealCoder(EnonConfig.defaults()).getElement(createNumberValue(i));
				fail("should not be supported by eNON-0");
			} catch (EnonCoderException x) {
				//success
			}
		}
		// test enonX
		for (Number i : ext) {
			Element e = new RealCoder(EnonConfig.extended()).getElement(createNumberValue(i));
			if (i instanceof Double && ((Double) i).isInfinite()) {
				assertSame((Double) i < 0 ? ElementType.INFINITY_NEG_ELEMENT : ElementType.INFINITY_POS_ELEMENT, e.getType());
			} else if (i instanceof Double && ((Double) i).isNaN()) {
				assertSame(ElementType.NAN_ELEMENT, e.getType());
			}
			if (i instanceof Float && ((Float) i).isInfinite()) {
				assertSame((Float) i < 0 ? ElementType.INFINITY_NEG_ELEMENT : ElementType.INFINITY_POS_ELEMENT, e.getType());
			} else if (i instanceof Float && ((Float) i).isNaN()) {;
				assertSame(ElementType.NAN_ELEMENT, e.getType());
			}
			assertEquals(i.toString(), String.valueOf(e.getValue()));
		}
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testGetElementStringFail() {
		LOGGER.info("getElementStringFail");
		Exception x = assertThrows(EnonCoderException.class, () -> {
			new RealCoder(EnonConfig.defaults()).getElement(createStringValue("nothing"));
		});
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testGetElementCharFail() {
		LOGGER.info("getElementCharFail");
		Exception x = assertThrows(EnonCoderException.class, () -> {
			new RealCoder(EnonConfig.defaults()).getElement(createCharValue('a'));
		});
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testGetElementFail() {
		LOGGER.info("getElementFail");
		Exception x = assertThrows(EnonCoderException.class, () -> {
			new RealCoder(EnonConfig.defaults()).getElement(createPojoValue(new Object()));
		});
	}

	private StringValue createStringValue(String value) {
		return new StringValue.Acceptor(EnonConfig.defaults()).toNativeValue(String.class, value);
	}

	private StringValue createCharValue(Character value) {
		return new StringValue.Acceptor(EnonConfig.defaults()).toNativeValue(Character.class, value);
	}

	private ScalarValue<Number> createNumberValue(Number value) {
		return new ScalarValue.Acceptor(EnonConfig.defaults()).toNativeValue(value.getClass(), value);
	}

	private PojoValue createPojoValue(Object value) {
		return new PojoValue.Acceptor(EnonConfig.defaults()).toNativeValue(value.getClass(), value);
	}
}
