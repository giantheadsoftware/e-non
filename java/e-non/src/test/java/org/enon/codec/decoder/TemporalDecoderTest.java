/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.chrono.IsoEra;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.bind.temporal.TemporalVO;
import org.enon.bind.value.DefaultNativeValueFactory;
import org.enon.bind.value.NativeValue;
import org.enon.bind.value.NativeValueFactory;
import org.enon.codec.coder.CoderFactory;
import org.enon.codec.coder.DefaultCoderFactory;
import org.enon.element.ConstantElement;
import org.enon.element.Element;
import org.enon.element.LongElement;
import org.enon.element.NumberElement;
import org.enon.element.TemporalElement;
import org.enon.exception.EnonDecoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TemporalDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(TemporalDecoderTest.class.getName());

	/**
	 * map of TemporalAccessor implementations and the set of TemporalAccessors that can be converted to them
	 */
	private static final Map<Class<? extends TemporalAccessor>, Set<Class>> TA_MATRIX = new HashMap<>();
	/**
	 * map of target types and the src types than can de decoded to that target tpe
	 */
	private static final Map<Class, Set<Class>> DECODE_MATRIX = new HashMap<>();

	private final EnonConfig config = new EnonConfig.Builder(FeatureSet.X).build();
	private final TemporalDecoder.Acceptor acceptor = new TemporalDecoder.Acceptor(config);

	private final ZoneOffset localOffset = config.getLocalZoneId().getRules().getOffset(Instant.now());
	private final int offsetHours = localOffset.getTotalSeconds() / 3600;
	private final int offsetMinutes = (localOffset.getTotalSeconds() % 3600) / 60;
	private final int offsetSeconds = ((localOffset.getTotalSeconds() % 3600) / 60) % 60;

	public TemporalDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
		TA_MATRIX.put(Instant.class, new HashSet<>(Arrays.asList(new Class[]{
			OffsetDateTime.class, ZonedDateTime.class
		})));
		TA_MATRIX.put(LocalDate.class, new HashSet<>(Arrays.asList(new Class[]{
			LocalDateTime.class, OffsetDateTime.class, ZonedDateTime.class
		})));
		TA_MATRIX.put(LocalDateTime.class, new HashSet<>(Arrays.asList(new Class[]{
			OffsetDateTime.class, ZonedDateTime.class
		})));
		TA_MATRIX.put(LocalTime.class, new HashSet<>(Arrays.asList(new Class[]{
			LocalDateTime.class, OffsetDateTime.class, OffsetTime.class, ZonedDateTime.class
		})));
		TA_MATRIX.put(MonthDay.class, new HashSet<>(Arrays.asList(new Class[]{
			LocalDate.class, LocalDateTime.class, OffsetDateTime.class, ZonedDateTime.class
		})));
		TA_MATRIX.put(OffsetDateTime.class, new HashSet<>(Arrays.asList(new Class[]{
			ZonedDateTime.class
		})));
		TA_MATRIX.put(OffsetTime.class, new HashSet<>(Arrays.asList(new Class[]{
			OffsetDateTime.class, ZonedDateTime.class
		})));
		TA_MATRIX.put(YearMonth.class, new HashSet<>(Arrays.asList(new Class[]{
			LocalDate.class, LocalDateTime.class, OffsetDateTime.class, ZonedDateTime.class
		})));
		TA_MATRIX.put(ZonedDateTime.class, new HashSet<>(Arrays.asList(new Class[]{
			OffsetDateTime.class, ZonedDateTime.class
		})));

		DECODE_MATRIX.put(Date.class, new HashSet<>(Arrays.asList(new Class[]{
			GregorianCalendar.class, Instant.class, LocalDate.class, LocalDateTime.class, OffsetDateTime.class, ZonedDateTime.class
		})));
		DECODE_MATRIX.put(GregorianCalendar.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, Instant.class, LocalDate.class, LocalDateTime.class, OffsetDateTime.class, ZonedDateTime.class
		})));
		DECODE_MATRIX.put(Instant.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, GregorianCalendar.class, LocalDate.class, LocalDateTime.class, OffsetDateTime.class, ZonedDateTime.class
		})));
		DECODE_MATRIX.put(LocalDate.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, Instant.class, GregorianCalendar.class, LocalDateTime.class, OffsetDateTime.class, ZonedDateTime.class
		})));
		DECODE_MATRIX.put(LocalDateTime.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, GregorianCalendar.class, Instant.class, LocalDate.class, OffsetDateTime.class, ZonedDateTime.class
		})));
		DECODE_MATRIX.put(LocalTime.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, GregorianCalendar.class, Instant.class, LocalDate.class, LocalDateTime.class, OffsetDateTime.class, OffsetTime.class, ZonedDateTime.class,
			Year.class, YearMonth.class, MonthDay.class
		})));
		DECODE_MATRIX.put(MonthDay.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, GregorianCalendar.class, Instant.class, LocalDate.class, LocalDateTime.class, OffsetDateTime.class, ZonedDateTime.class
		})));
		DECODE_MATRIX.put(OffsetDateTime.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, GregorianCalendar.class, Instant.class, LocalDate.class, LocalDateTime.class, ZonedDateTime.class
		})));
		DECODE_MATRIX.put(OffsetTime.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, GregorianCalendar.class, Instant.class, LocalDate.class, LocalDateTime.class, LocalTime.class, OffsetDateTime.class, ZonedDateTime.class,
			Year.class, YearMonth.class, MonthDay.class
		})));
		DECODE_MATRIX.put(Year.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, GregorianCalendar.class, Instant.class, LocalDate.class, LocalDateTime.class, OffsetDateTime.class, YearMonth.class, ZonedDateTime.class
		})));
		DECODE_MATRIX.put(YearMonth.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, GregorianCalendar.class, Instant.class, LocalDate.class, LocalDateTime.class, OffsetDateTime.class, YearMonth.class, ZonedDateTime.class
		})));
		DECODE_MATRIX.put(ZonedDateTime.class, new HashSet<>(Arrays.asList(new Class[]{
			Date.class, GregorianCalendar.class, Instant.class, LocalDate.class, LocalDateTime.class, OffsetDateTime.class, ZonedDateTime.class
		})));
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class TemporalDecoder.
	 */
	@Test
	public void testDecodeDate() {
		LOGGER.info("decodeDate");
		long now = System.currentTimeMillis();
		TemporalDecoder dateDecoder = acceptor.decoderFor(Date.class);

		Object result = dateDecoder.decode(new LongElement(now));

		assertSame(Date.class, result.getClass());
		assertEquals(new Date(now), result);

		// TemporalElement as instant
		TemporalVO tvo = TemporalVO.createInstant(now);

		result = dateDecoder.decode(TemporalElement.create(tvo));

		assertSame(Date.class, result.getClass());
		assertEquals(new Date(now), result);

		// TemporalElement as date-time fields
		tvo = new TemporalVO.Builder()
				.date(2019, (byte) 12, (byte) 17)
				.time((byte) 14, (byte) 25, (byte) 13)
				.nanos(343000000)
				.build();

		result = dateDecoder.decode(TemporalElement.create(tvo));

		assertSame(Date.class, result.getClass());
		Calendar cal = Calendar.getInstance();  // create a calendar from the date result to gain access to the temporal fields
		cal.setTime((Date) result);
		assertEquals((long) tvo.year, cal.get(Calendar.YEAR));
		assertEquals((long) tvo.month, cal.get(Calendar.MONTH) + 1);
		assertEquals((long) tvo.day, cal.get(Calendar.DAY_OF_MONTH));
		assertEquals((long) tvo.hour, cal.get(Calendar.HOUR_OF_DAY));
		assertEquals((long) tvo.minute, cal.get(Calendar.MINUTE));
		assertEquals((long) tvo.sec, cal.get(Calendar.SECOND));
		assertEquals((long) tvo.nanos, cal.get(Calendar.MILLISECOND) * 1000000);
	}

	/**
	 * Test of decode method, of class TemporalDecoder.
	 */
	@Test
	public void testDecodeCalendar() {
		LOGGER.info("decodeCalendar");
		long now = System.currentTimeMillis();
		TemporalDecoder calDecoder = acceptor.decoderFor(GregorianCalendar.class);

		Object result = calDecoder.decode(new LongElement(now));

		assertTrue(result instanceof Calendar);
		assertEquals(((Calendar) result).getTimeInMillis(), now);

		// TemporalElement as instant with offset
		int offset = 7 * 60 * 60; //+7:00
		TemporalVO tvo = TemporalVO.createInstantOffset(now, offset);

		result = calDecoder.decode(TemporalElement.create(tvo));
		assertTrue(result instanceof Calendar);
		assertEquals(((Calendar) result).getTimeInMillis(), now);
		assertEquals(offset * 1000, ((Calendar) result).getTimeZone().getRawOffset());

		// TemporalElement as instant with zoneid
		tvo = TemporalVO.createInstantZoned(now, "Asia/Bangkok");

		result = calDecoder.decode(TemporalElement.create(tvo));
		assertTrue(result instanceof Calendar);
		assertEquals(now, ((Calendar) result).getTimeInMillis());
		assertEquals(tvo.zoneId, ((Calendar) result).getTimeZone().getID());

		// TemporalElement as a local date
		tvo = TemporalVO.createDate(2020, (byte) 11, (byte) 15);
		Calendar cal = new GregorianCalendar(tvo.year, tvo.month - 1, tvo.day);

		result = calDecoder.decode(TemporalElement.create(tvo));
		assertTrue(result instanceof Calendar);
		assertEquals(cal, result);

		// TemporalElement as a local time
		tvo = TemporalVO.createTime((byte) 9, (byte) 29, (byte) 42);
		cal = new GregorianCalendar();
		cal.clear();
		cal.set(Calendar.HOUR_OF_DAY, tvo.hour);
		cal.set(Calendar.MINUTE, tvo.minute);
		cal.set(Calendar.SECOND, tvo.sec);

		try {
			calDecoder.decode(TemporalElement.create(tvo));
			fail("Should not decode Calendar without a date");
		} catch (EnonDecoderException x) {
			// success
		}

		// TemporalElement as a local time with timezone
		tvo = new TemporalVO.Builder().from(tvo).zoneId("America/Chicago").build();
		cal = new GregorianCalendar();
		cal.clear();
		cal.set(Calendar.HOUR_OF_DAY, tvo.hour);
		cal.set(Calendar.MINUTE, tvo.minute);
		cal.set(Calendar.SECOND, tvo.sec);
		cal.setTimeZone(TimeZone.getTimeZone(tvo.zoneId));

		try {
			calDecoder.decode(TemporalElement.create(tvo));
			fail("Should not decode Calendar without a date");
		} catch (EnonDecoderException x) {
			// success
		}

		// TemporalElement as a local date time with timezone
		tvo = new TemporalVO.Builder().date(2009, (byte) 2, (byte) 14).time((byte) 7, (byte) 30, (byte) 05).build();
		cal = new GregorianCalendar(tvo.year, tvo.month - 1, tvo.day, tvo.hour, tvo.minute, tvo.sec);

		result = calDecoder.decode(TemporalElement.create(tvo));
		assertTrue(result instanceof Calendar);
		assertEquals(cal, result);

		// TemporalElement as a month-day
		tvo = new TemporalVO.Builder().month((byte) 9).day((byte) 21).build();
		cal = new GregorianCalendar();
		cal.clear();
		cal.set(Calendar.MONTH, tvo.month - 1);
		cal.set(Calendar.DAY_OF_MONTH, tvo.day);

		try {
			calDecoder.decode(TemporalElement.create(tvo));
			fail("Should not decode Calendar without a full date");
		} catch (EnonDecoderException x) {
			// success
		}
	}

	/**
	 * Test of decode method, of class TemporalDecoder.
	 */
	@Test
	public void testDecodeInstant() {
		LOGGER.info("decodeInstant");
		long now = System.currentTimeMillis();

		TemporalDecoder dateDecoder = acceptor.decoderFor(Instant.class);

		// decode from a LongElement
		Object object = dateDecoder.decode(new LongElement(now));

		assertTrue(object instanceof Instant);
		assertEquals(now / 1000, ((Instant) object).getLong(ChronoField.INSTANT_SECONDS));
		assertEquals(now % 1000, ((Instant) object).getLong(ChronoField.MILLI_OF_SECOND));

		// decode from a TemporalElement
		TemporalVO tvo = TemporalVO.createInstant(now);
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof Instant);
		assertEquals(now / 1000, ((Instant) object).getLong(ChronoField.INSTANT_SECONDS));
		assertEquals(now % 1000, ((Instant) object).getLong(ChronoField.MILLI_OF_SECOND));

		// add a zoneoffset to the element.  The instnat should not be affected
		int offset = -60 * 60 * 5;
		tvo = new TemporalVO.Builder().from(tvo).offsetSeconds(offset).build();
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof Instant);
		assertEquals(now / 1000, ((Instant) object).getLong(ChronoField.INSTANT_SECONDS));
		assertEquals(now % 1000, ((Instant) object).getLong(ChronoField.MILLI_OF_SECOND));
	}

	/**
	 * Test of decode method, of class TemporalDecoder.
	 */
	@Test
	public void testDecodeLocalDate() {
		LOGGER.info("decodeLocalDate");
		long now = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(now);

		TemporalDecoder dateDecoder = acceptor.decoderFor(LocalDate.class);

		// LongElement
		Object object = dateDecoder.decode(new LongElement(now));

		assertTrue(object instanceof LocalDate);
		LocalDate localDate = (LocalDate) object;
		assertEquals(cal.get(Calendar.YEAR), localDate.getLong(ChronoField.YEAR));
		assertEquals(cal.get(Calendar.MONTH) + 1, localDate.getLong(ChronoField.MONTH_OF_YEAR));
		assertEquals(cal.get(Calendar.DAY_OF_MONTH), localDate.getLong(ChronoField.DAY_OF_MONTH));

		//NumberElement
		object = dateDecoder.decode(new NumberElement(now));

		assertTrue(object instanceof LocalDate);
		localDate = (LocalDate) object;
		assertEquals(cal.get(Calendar.YEAR), localDate.getLong(ChronoField.YEAR));
		assertEquals(cal.get(Calendar.MONTH) + 1, localDate.getLong(ChronoField.MONTH_OF_YEAR));
		assertEquals(cal.get(Calendar.DAY_OF_MONTH), localDate.getLong(ChronoField.DAY_OF_MONTH));

		// TemporalElement as instant
		TemporalVO tvo = TemporalVO.createInstant(now);
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof LocalDate);
		localDate = (LocalDate) object;
		assertEquals(cal.get(Calendar.YEAR), localDate.getLong(ChronoField.YEAR));
		assertEquals(cal.get(Calendar.MONTH) + 1, localDate.getLong(ChronoField.MONTH_OF_YEAR));
		assertEquals(cal.get(Calendar.DAY_OF_MONTH), localDate.getLong(ChronoField.DAY_OF_MONTH));

		// TemporalElement as date-time fields
		tvo = TemporalVO.createDateTime(2019, (byte) 12, (byte) 17, (byte) 14, (byte) 25, (byte) 13);
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof LocalDate);
		localDate = (LocalDate) object;
		assertEquals((long) tvo.year, localDate.getLong(ChronoField.YEAR));
		assertEquals((long) tvo.month, localDate.getLong(ChronoField.MONTH_OF_YEAR));
		assertEquals((long) tvo.day, localDate.getLong(ChronoField.DAY_OF_MONTH));

		// add a zoneoffset to the element.  The resulting date is not affected because LocalDate doesn't care about zones
		int offset = -60 * 60 * 5;  // offset -5:00
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof LocalDate);
		localDate = (LocalDate) object;
		assertEquals((long) tvo.year, localDate.getLong(ChronoField.YEAR));
		assertEquals((long) tvo.month, localDate.getLong(ChronoField.MONTH_OF_YEAR));
		assertEquals((long) tvo.day, localDate.getLong(ChronoField.DAY_OF_MONTH));

	}

	/**
	 * Test of decode method, of class TemporalDecoder.
	 */
	@Test
	public void testDecodeLocalDateTime() {
		LOGGER.info("decodeLocalDateTime");
		long now = System.currentTimeMillis();

		TemporalDecoder dateDecoder = acceptor.decoderFor(LocalDateTime.class);

		// LongElement
		Object object = dateDecoder.decode(new LongElement(now));

		assertTrue(object instanceof LocalDateTime);
		Instant instant = ((LocalDateTime) object).toInstant(localOffset);
		assertEquals(now / 1000, instant.getLong(ChronoField.INSTANT_SECONDS));
		assertEquals(now % 1000, instant.getLong(ChronoField.MILLI_OF_SECOND));

		//NumberElement
		object = dateDecoder.decode(new NumberElement(now));

		assertTrue(object instanceof LocalDateTime);
		instant = ((LocalDateTime) object).toInstant(localOffset);
		assertEquals(now / 1000, instant.getLong(ChronoField.INSTANT_SECONDS));
		assertEquals(now % 1000, instant.getLong(ChronoField.MILLI_OF_SECOND));

		// TemporalElement as instant
		TemporalVO tvo = TemporalVO.createInstant(now);
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof LocalDateTime);
		instant = ((LocalDateTime) object).toInstant(localOffset);
		assertEquals(now / 1000, instant.getLong(ChronoField.INSTANT_SECONDS));
		assertEquals(now % 1000, instant.getLong(ChronoField.MILLI_OF_SECOND));

		// add a zoneoffset to the element.  The result should not be affected
		int offset = -60 * 60 * 5;
		tvo = new TemporalVO.Builder().from(tvo).offsetSeconds(offset).build();
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof LocalDateTime);
		instant = ((LocalDateTime) object).toInstant(ZoneOffset.UTC);
		assertEquals(now / 1000 + offset, instant.getLong(ChronoField.INSTANT_SECONDS));
		assertEquals(now % 1000, instant.getLong(ChronoField.MILLI_OF_SECOND));

		// TemporalElement as date-time fields
		tvo = TemporalVO.createDateTime(2019, (byte) 12, (byte) 17, (byte) 14, (byte) 25, (byte) 13);
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof LocalDateTime);
		LocalDateTime ldt = (LocalDateTime) object;
		assertEquals((long) tvo.year, ldt.getLong(ChronoField.YEAR));
		assertEquals((long) tvo.month, ldt.getLong(ChronoField.MONTH_OF_YEAR));
		assertEquals((long) tvo.day, ldt.getLong(ChronoField.DAY_OF_MONTH));
		assertEquals((long) tvo.hour, ldt.getLong(ChronoField.HOUR_OF_DAY));
		assertEquals((long) tvo.minute, ldt.getLong(ChronoField.MINUTE_OF_HOUR));
		assertEquals((long) tvo.sec, ldt.getLong(ChronoField.SECOND_OF_MINUTE));
		assertEquals((long) tvo.nanos, ldt.getLong(ChronoField.NANO_OF_SECOND));
	}

	/**
	 * Test of decode method, of class TemporalDecoder.
	 */
	@Test
	public void testDecodeLocalTime() {
		LOGGER.info("decodeLocalTime");
		long now = System.currentTimeMillis();

		// create a Calendar as a test reference
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone(ZoneOffset.UTC));
		cal.setTimeInMillis(now);

		TemporalDecoder dateDecoder = acceptor.decoderFor(LocalTime.class);

		// LongElement
		Object object = dateDecoder.decode(new LongElement(now));

		assertTrue(object instanceof LocalTime);
		LocalTime localTime = (LocalTime) object;
		assertEquals((cal.get(Calendar.HOUR_OF_DAY) + offsetHours) % 24, localTime.getLong(ChronoField.HOUR_OF_DAY));
		assertEquals(cal.get(Calendar.MINUTE) + offsetMinutes, localTime.getLong(ChronoField.MINUTE_OF_HOUR));
		assertEquals(cal.get(Calendar.SECOND) + offsetSeconds, localTime.getLong(ChronoField.SECOND_OF_MINUTE));
		assertEquals(cal.get(Calendar.MILLISECOND), localTime.getLong(ChronoField.MILLI_OF_SECOND));

		//NumberElement
		object = dateDecoder.decode(new NumberElement(now));

		assertTrue(object instanceof LocalTime);
		localTime = (LocalTime) object;
		assertEquals((cal.get(Calendar.HOUR_OF_DAY) + offsetHours) % 24, localTime.getLong(ChronoField.HOUR_OF_DAY));
		assertEquals(cal.get(Calendar.MINUTE) + offsetMinutes, localTime.getLong(ChronoField.MINUTE_OF_HOUR));
		assertEquals(cal.get(Calendar.SECOND) + offsetSeconds, localTime.getLong(ChronoField.SECOND_OF_MINUTE));
		assertEquals(cal.get(Calendar.MILLISECOND), localTime.getLong(ChronoField.MILLI_OF_SECOND));

		// TemporalElement as instant
		TemporalVO tvo = TemporalVO.createInstant(now);
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof LocalTime);
		localTime = (LocalTime) object;
		assertEquals((cal.get(Calendar.HOUR_OF_DAY) + offsetHours) % 24, localTime.getLong(ChronoField.HOUR_OF_DAY));
		assertEquals(cal.get(Calendar.MINUTE) + offsetMinutes, localTime.getLong(ChronoField.MINUTE_OF_HOUR));
		assertEquals(cal.get(Calendar.SECOND) + offsetSeconds, localTime.getLong(ChronoField.SECOND_OF_MINUTE));
		assertEquals(cal.get(Calendar.MILLISECOND), localTime.getLong(ChronoField.MILLI_OF_SECOND));

		// add a zoneoffset to the element. 
		int offset = -60 * 60 * 5;  // -5:00
		tvo = new TemporalVO.Builder().from(tvo).offsetSeconds(offset).build();
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof LocalTime);
		localTime = (LocalTime) object;
		int hourOfDay = (cal.get(Calendar.HOUR_OF_DAY) + (offset / 3600)) % 24;
		if (hourOfDay < 0) {
			hourOfDay += 24;
		}
		assertEquals(hourOfDay, localTime.getLong(ChronoField.HOUR_OF_DAY));
		assertEquals(cal.get(Calendar.MINUTE), localTime.getLong(ChronoField.MINUTE_OF_HOUR));
		assertEquals(cal.get(Calendar.SECOND), localTime.getLong(ChronoField.SECOND_OF_MINUTE));
		assertEquals(cal.get(Calendar.MILLISECOND), localTime.getLong(ChronoField.MILLI_OF_SECOND));

		// TemporalElement as date-time fields
		tvo = TemporalVO.createTime((byte) 14, (byte) 25, (byte) 13);
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof LocalTime);
		localTime = (LocalTime) object;
		assertEquals((long) tvo.hour, localTime.getLong(ChronoField.HOUR_OF_DAY));
		assertEquals((long) tvo.minute, localTime.getLong(ChronoField.MINUTE_OF_HOUR));
		assertEquals((long) tvo.sec, localTime.getLong(ChronoField.SECOND_OF_MINUTE));
	}

	/**
	 * Test of decode method, of class TemporalDecoder.
	 */
	@Test
	public void testDecodeOffsetDateTime() {
		LOGGER.info("decodeOffsetDateTime");
		long now = System.currentTimeMillis();

		TemporalDecoder dateDecoder = acceptor.decoderFor(OffsetDateTime.class);

		Object object = dateDecoder.decode(new LongElement(now));

		assertTrue(object instanceof OffsetDateTime);
		assertEquals(((OffsetDateTime) object).getLong(ChronoField.INSTANT_SECONDS), now / 1000);
		assertEquals(((OffsetDateTime) object).getLong(ChronoField.MILLI_OF_SECOND), now % 1000);

		//NumberElement
		object = dateDecoder.decode(new NumberElement(now));

		assertTrue(object instanceof OffsetDateTime);
		assertEquals(((OffsetDateTime) object).getLong(ChronoField.INSTANT_SECONDS), now / 1000);
		assertEquals(((OffsetDateTime) object).getLong(ChronoField.MILLI_OF_SECOND), now % 1000);

		// TemporalElement as date-time fields
		TemporalVO tvo = TemporalVO.createZonedDateTime(2019, (byte) 12, (byte) 17, (byte) 14, (byte) 25, (byte) 13, "America/New_York");
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof OffsetDateTime);
		OffsetDateTime odt = (OffsetDateTime) object;
		assertEquals(odt.getLong(ChronoField.YEAR), (long) tvo.year);
		assertEquals(odt.getLong(ChronoField.MONTH_OF_YEAR), (long) tvo.month);
		assertEquals(odt.getLong(ChronoField.DAY_OF_MONTH), (long) tvo.day);
		assertEquals(odt.getLong(ChronoField.HOUR_OF_DAY), (long) tvo.hour);
		assertEquals(odt.getLong(ChronoField.MINUTE_OF_HOUR), (long) tvo.minute);
		assertEquals(odt.getLong(ChronoField.SECOND_OF_MINUTE), (long) tvo.sec);
		assertEquals(odt.getLong(ChronoField.NANO_OF_SECOND), (long) tvo.nanos);
	}

	/**
	 * Test of decode method, of class TemporalDecoder.
	 */
	@Test
	public void testDecodeZonedDateTime() {
		LOGGER.info("decodeZonedDateTime");
		long now = System.currentTimeMillis();

		TemporalDecoder dateDecoder = acceptor.decoderFor(ZonedDateTime.class);

		Object object = dateDecoder.decode(new LongElement(now));

		assertTrue(object instanceof ZonedDateTime);
		assertEquals(((ZonedDateTime) object).getLong(ChronoField.INSTANT_SECONDS), now / 1000);
		assertEquals(((ZonedDateTime) object).getLong(ChronoField.MILLI_OF_SECOND), now % 1000);

		//NumberElement
		object = dateDecoder.decode(new NumberElement(now));

		assertTrue(object instanceof ZonedDateTime);
		assertEquals(((ZonedDateTime) object).getLong(ChronoField.INSTANT_SECONDS), now / 1000);
		assertEquals(((ZonedDateTime) object).getLong(ChronoField.MILLI_OF_SECOND), now % 1000);

		// TemporalElement as date-time fields
		TemporalVO tvo = new TemporalVO.Builder().date(2019, (byte) 12, (byte) 17).time((byte) 14, (byte) 25, (byte) 13).offsetSeconds(3600 * 7).build();
		object = dateDecoder.decode(TemporalElement.create(tvo));

		assertTrue(object instanceof ZonedDateTime);
		ZonedDateTime zdt = (ZonedDateTime) object;
		assertEquals(zdt.getLong(ChronoField.YEAR), (long) tvo.year);
		assertEquals(zdt.getLong(ChronoField.MONTH_OF_YEAR), (long) tvo.month);
		assertEquals(zdt.getLong(ChronoField.DAY_OF_MONTH), (long) tvo.day);
		assertEquals(zdt.getLong(ChronoField.HOUR_OF_DAY), (long) tvo.hour);
		assertEquals(zdt.getLong(ChronoField.MINUTE_OF_HOUR), (long) tvo.minute);
		assertEquals(zdt.getLong(ChronoField.SECOND_OF_MINUTE), (long) tvo.sec);
		assertEquals(zdt.getLong(ChronoField.NANO_OF_SECOND), (long) tvo.nanos);
	}

	/**
	 * Test of decode method, of class TemporalDecoder.
	 */
	@Test
	public void testDecodeTemporalAccessor() {
		LOGGER.info("decodeTemporalAccessor");
		long now = System.currentTimeMillis();

		TemporalDecoder dateDecoder = acceptor.decoderFor(ZonedDateTime.class);

		Object object = dateDecoder.decode(new LongElement(now));

		assertTrue(object instanceof ZonedDateTime);
		assertEquals(((ZonedDateTime) object).getLong(ChronoField.INSTANT_SECONDS), now / 1000);
		assertEquals(((ZonedDateTime) object).getLong(ChronoField.MILLI_OF_SECOND), now % 1000);

		//NumberElement
		object = dateDecoder.decode(new NumberElement(now));

		assertTrue(object instanceof ZonedDateTime);
		assertEquals(((ZonedDateTime) object).getLong(ChronoField.INSTANT_SECONDS), now / 1000);
		assertEquals(((ZonedDateTime) object).getLong(ChronoField.MILLI_OF_SECOND), now % 1000);
	}

	@Test
	public void testDecodeMatrix() {
		LOGGER.info("decodeMatrix");
		NativeValueFactory nativeValueFactory = new DefaultNativeValueFactory(new EnonConfig.Builder(FeatureSet.X).build());
		CoderFactory coderFactory = new DefaultCoderFactory(new EnonConfig.Builder(FeatureSet.X).build());

		NativeValue[] temporalObjects = new NativeValue[]{
			nativeValueFactory.nativeValue(new Date()),
			nativeValueFactory.nativeValue(new GregorianCalendar()),
			nativeValueFactory.nativeValue(Instant.now()),
			nativeValueFactory.nativeValue(LocalDate.now()),
			nativeValueFactory.nativeValue(LocalDateTime.now()),
			nativeValueFactory.nativeValue(LocalTime.now()),
			nativeValueFactory.nativeValue(MonthDay.now()),
			nativeValueFactory.nativeValue(OffsetDateTime.now()),
			nativeValueFactory.nativeValue(OffsetTime.now()),
			nativeValueFactory.nativeValue(Year.now()),
			nativeValueFactory.nativeValue(YearMonth.now()),
			nativeValueFactory.nativeValue(ZonedDateTime.now())
		};

		for (NativeValue nativeValue : temporalObjects) {
			Class targetType = nativeValue.getDeclaredType();
			TemporalDecoder decoder = acceptor.decoderFor(targetType);
			for (NativeValue srcObject : temporalObjects) {
				Class srcType = srcObject.getDeclaredType();
				LOGGER.info("DECODE " + targetType.getSimpleName() + " FROM " + srcType.getSimpleName());
				Element e = coderFactory.encode(srcObject);
				if (targetType == srcType || DECODE_MATRIX.get(targetType).contains(srcType)) {
					Object destTa = decoder.decode(e);
					assertNotNull(destTa);
					assertSame(nativeValue.getDeclaredType(), destTa.getClass());
				} else {
					try {
						Object fail = decoder.decode(e);
						fail("Should not create " + targetType.getSimpleName() + " from " + srcType.getSimpleName());
					} catch (EnonDecoderException x) {
						// success
					}
				}
			}
		}
	}

	@Test
	public void testNull() {
		LOGGER.info("null");
		assertNull(acceptor.decoderFor(LocalDateTime.class).decode(ConstantElement.nullInstance()));
	}

//	@Test
//	public void testAsTemporalAccessor() {
//		LOGGER.info("asTemporalAccessor");
//
//		TemporalVO tvo;
//		TemporalDecoder decoder = acceptor.decoderFor(TemporalAccessor.class);
//		TemporalAccessor ta;
//
//		long now = System.currentTimeMillis();
//
//		// as simple instant, UTC
//		tvo = TemporalVO.createInstant(now);
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(OffsetDateTime.class, ta.getClass());
//		assertEquals(now / 1000, ((OffsetDateTime)ta).getLong(ChronoField.INSTANT_SECONDS));
//		assertEquals(now % 1000, ((OffsetDateTime)ta).getLong(ChronoField.MILLI_OF_SECOND));
//
//		// as offset instant
//		tvo = TemporalVO.createInstantOffset(now, -7*60*60);
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(OffsetDateTime.class, ta.getClass());
//		assertEquals(now / 1000, ((OffsetDateTime)ta).getLong(ChronoField.INSTANT_SECONDS));
//		assertEquals(now % 1000, ((OffsetDateTime)ta).getLong(ChronoField.MILLI_OF_SECOND));
//		assertEquals((int)tvo.offsetSeconds, ((OffsetDateTime)ta).get(ChronoField.OFFSET_SECONDS));
//
//		// as offset instant
//		tvo = TemporalVO.createInstantZoned(now, "Asia/Tokyo");
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(ZonedDateTime.class, ta.getClass());
//		assertEquals(now / 1000, ((ZonedDateTime)ta).getLong(ChronoField.INSTANT_SECONDS));
//		assertEquals(now % 1000, ((ZonedDateTime)ta).getLong(ChronoField.MILLI_OF_SECOND));
//		assertEquals(tvo.zoneId, ((ZonedDateTime)ta).getZone().getId());
//
//		// as simple date-time
//		tvo = TemporalVO.createDateTime(2015, (byte)6, (byte)9, (byte)14, (byte)15, (byte)59);
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(LocalDateTime.class, ta.getClass());
//		assertEquals(tvo.year.intValue(), ta.get(ChronoField.YEAR));
//		assertEquals(tvo.month.intValue(), ta.get(ChronoField.MONTH_OF_YEAR));
//		assertEquals(tvo.day.intValue(), ta.get(ChronoField.DAY_OF_MONTH));
//		assertEquals(tvo.hour.intValue(), ta.get(ChronoField.HOUR_OF_DAY));
//		assertEquals(tvo.minute.intValue(), ta.get(ChronoField.MINUTE_OF_HOUR));
//		assertEquals(tvo.sec.intValue(), ta.get(ChronoField.SECOND_OF_MINUTE));
//		assertEquals(0, ta.getLong(ChronoField.MILLI_OF_SECOND));
//
//		// as offset date-time
//		tvo = new TemporalVO.Builder().date(2015, (byte)6, (byte)9).time((byte)14, (byte)15, (byte)59).offsetSeconds(3600).build();
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(OffsetDateTime.class, ta.getClass());
//		assertEquals(tvo.year.intValue(), ta.get(ChronoField.YEAR));
//		assertEquals(tvo.month.intValue(), ta.get(ChronoField.MONTH_OF_YEAR));
//		assertEquals(tvo.day.intValue(), ta.get(ChronoField.DAY_OF_MONTH));
//		assertEquals(tvo.hour.intValue(), ta.get(ChronoField.HOUR_OF_DAY));
//		assertEquals(tvo.minute.intValue(), ta.get(ChronoField.MINUTE_OF_HOUR));
//		assertEquals(tvo.sec.intValue(), ta.get(ChronoField.SECOND_OF_MINUTE));
//		assertEquals(0, ta.getLong(ChronoField.MILLI_OF_SECOND));
//		assertEquals(tvo.offsetSeconds.intValue(), ta.get(ChronoField.OFFSET_SECONDS));
//
//		// as zoned date-time
//		tvo = TemporalVO.createZonedDateTime(2015, (byte)6, (byte)9, (byte)14, (byte)15, (byte)59, "America/Denver");
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(ZonedDateTime.class, ta.getClass());
//		assertEquals(tvo.year.intValue(), ta.get(ChronoField.YEAR));
//		assertEquals(tvo.month.intValue(), ta.get(ChronoField.MONTH_OF_YEAR));
//		assertEquals(tvo.day.intValue(), ta.get(ChronoField.DAY_OF_MONTH));
//		assertEquals(tvo.hour.intValue(), ta.get(ChronoField.HOUR_OF_DAY));
//		assertEquals(tvo.minute.intValue(), ta.get(ChronoField.MINUTE_OF_HOUR));
//		assertEquals(tvo.sec.intValue(), ta.get(ChronoField.SECOND_OF_MINUTE));
//		assertEquals(0, ta.getLong(ChronoField.MILLI_OF_SECOND));
//		assertEquals(tvo.zoneId, ((ZonedDateTime)ta).getZone().getId());
//
//		// as simple time of day
//		tvo = TemporalVO.createTime((byte)14, (byte)15, (byte)59);
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(LocalTime.class, ta.getClass());
//		assertEquals(tvo.hour.intValue(), ta.get(ChronoField.HOUR_OF_DAY));
//		assertEquals(tvo.minute.intValue(), ta.get(ChronoField.MINUTE_OF_HOUR));
//		assertEquals(tvo.sec.intValue(), ta.get(ChronoField.SECOND_OF_MINUTE));
//		assertEquals(0, ta.getLong(ChronoField.MILLI_OF_SECOND));
//
//		// as offset time of day
//		tvo = TemporalVO.createTimeOffset((byte)14, (byte)15, (byte)59, 2*3600);
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(OffsetTime.class, ta.getClass());
//		assertEquals(tvo.hour.intValue(), ta.get(ChronoField.HOUR_OF_DAY));
//		assertEquals(tvo.minute.intValue(), ta.get(ChronoField.MINUTE_OF_HOUR));
//		assertEquals(tvo.sec.intValue(), ta.get(ChronoField.SECOND_OF_MINUTE));
//		assertEquals(0, ta.getLong(ChronoField.MILLI_OF_SECOND));
//		assertEquals(tvo.offsetSeconds.intValue(), ta.get(ChronoField.OFFSET_SECONDS));
//
//		// as simple date
//		tvo = TemporalVO.createDate(2014, (byte)7, (byte)14);
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(LocalDate.class, ta.getClass());
//		assertEquals(tvo.year.intValue(), ta.get(ChronoField.YEAR));
//		assertEquals(tvo.month.intValue(), ta.get(ChronoField.MONTH_OF_YEAR));
//		assertEquals(tvo.day.intValue(), ta.get(ChronoField.DAY_OF_MONTH));
//
//		// as simple year
//		tvo = new TemporalVO.Builder().year(2014).build();
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(Year.class, ta.getClass());
//		assertEquals(tvo.year.intValue(), ta.get(ChronoField.YEAR));
//
//		// as year-month
//		tvo = new TemporalVO.Builder().year(2014).month((byte)1).build();
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(YearMonth.class, ta.getClass());
//		assertEquals(tvo.year.intValue(), ta.get(ChronoField.YEAR));
//		assertEquals(tvo.month.intValue(), ta.get(ChronoField.MONTH_OF_YEAR));
//
//		// as month-day
//		tvo = new TemporalVO.Builder().month((byte)1).day((byte)5).build();
//		ta = decoder.asTemporalAccessor(tvo);
//		assertSame(MonthDay.class, ta.getClass());
//		assertEquals(tvo.month.intValue(), ta.get(ChronoField.MONTH_OF_YEAR));
//		assertEquals(tvo.day.intValue(), ta.get(ChronoField.DAY_OF_MONTH));
//
//		// as unhandled combination
//		tvo = new TemporalVO.Builder().month((byte)1).hour((byte)5).build();
//		ta = decoder.asTemporalAccessor(tvo);
//		assertNull(ta);
//	}
//
//	@Test
//	public void testFromTa() {
//		LOGGER.info("fromTa");
//		Class<TemporalAccessor>[] taClasses = new Class[]{
//			Instant.class, LocalDate.class, LocalDateTime.class, LocalTime.class, MonthDay.class, OffsetDateTime.class, OffsetTime.class, YearMonth.class, ZonedDateTime.class
//		};
//
//		OffsetDateTime odt = OffsetDateTime.of(2019, 3, 14, 15, 35, 30, 275000000, ZoneOffset.ofHours(10));
//		ZonedDateTime zdt = ZonedDateTime.of(2019, 3, 14, 15, 35, 30, 275000000, ZoneOffset.ofHours(10));
//
//		for(Class<TemporalAccessor> taClass : taClasses) {
//			TemporalDecoder decoder = acceptor.decoderFor(taClass);
//
//			TemporalAccessor ta = decoder.fromTa(odt);
//			assertSame(taClass, ta.getClass());
//			assertTaMatch(odt, ta);
//
//			ta = decoder.fromTa(zdt);
//			assertSame(taClass, ta.getClass());
//			assertTaMatch(zdt, ta);
//		}
//
//		// test non-temporal accessor targetType
//		TemporalDecoder decoder = acceptor.decoderFor(Date.class);
//
//		TemporalAccessor ta = decoder.fromTa(odt);
//		assertNull(ta);
//	}
//	@Test
//	public void testFromTaMatrix() {
//		LOGGER.info("fromTaMatrix");
//		TemporalAccessor[] taObjects = new TemporalAccessor[]{
//			Instant.now(),
//			LocalDate.now(),
//			LocalDateTime.now(),
//			LocalTime.now(),
//			MonthDay.now(),
//			OffsetDateTime.now(),
//			OffsetTime.now(),
//			YearMonth.now(),
//			ZonedDateTime.now()
//		};
//
//		for (TemporalAccessor taType : taObjects) {
//			TemporalDecoder decoder = acceptor.decoderFor(taType.getClass().getName());
//			for (TemporalAccessor srcTa : taObjects) {
//				if (taType.getClass() == srcTa.getClass() || TA_MATRIX.get(taType.getClass()).contains(srcTa.getClass())) {
//						TemporalAccessor destTa = decoder.fromTa(srcTa);
//						assertSame(taType.getClass(), destTa.getClass());
//						assertTaMatch(srcTa, destTa);
//				} else {
//					try {
//						decoder.fromTa(srcTa);
//						fail("Should not create "+taType.getClass().getSimpleName()+" from "+srcTa.getClass().getSimpleName());
//					} catch (DateTimeException x) {
//						// success
//					}
//				}
//			}
//		}
//	}
	@Test
	public void testDefaults() {
		LOGGER.info("defaults");

		// set only the hour
		TemporalVO tvo = new TemporalVO.Builder().hour((byte) 9).build();

		TemporalDecoder decoder = acceptor.decoderFor(LocalTime.class);

		LocalTime lt = (LocalTime) decoder.decode(TemporalElement.create(tvo));
		assertEquals(tvo.hour.intValue(), lt.getHour());
		assertEquals(0, lt.getMinute());
		assertEquals(0, lt.getSecond());
		assertEquals(0, lt.getNano());

		// set only the minute
		tvo = new TemporalVO.Builder().minute((byte) 9).build();

		lt = (LocalTime) decoder.decode(TemporalElement.create(tvo));
		assertEquals(0, lt.getHour());
		assertEquals(tvo.minute.intValue(), lt.getMinute());
		assertEquals(0, lt.getSecond());
		assertEquals(0, lt.getNano());

		// set only the second
		tvo = new TemporalVO.Builder().second((byte) 9).build();

		lt = (LocalTime) decoder.decode(TemporalElement.create(tvo));
		assertEquals(0, lt.getHour());
		assertEquals(0, lt.getMinute());
		assertEquals(tvo.sec.intValue(), lt.getSecond());
		assertEquals(0, lt.getNano());

		// set only the nanos
		tvo = new TemporalVO.Builder().nanos((byte) 9).build();

		lt = (LocalTime) decoder.decode(TemporalElement.create(tvo));
		assertEquals(0, lt.getHour());
		assertEquals(0, lt.getMinute());
		assertEquals(0, lt.getSecond());
		assertEquals(tvo.nanos.intValue(), lt.getNano());

		// set only the day
		tvo = new TemporalVO.Builder().day((byte) 9).build();

		lt = (LocalTime) decoder.decode(TemporalElement.create(tvo));
		assertEquals(0, lt.getHour());
		assertEquals(0, lt.getMinute());
		assertEquals(0, lt.getSecond());
		assertEquals(0, lt.getNano());

		// set nothing
		tvo = new TemporalVO.Builder().build();

		lt = (LocalTime) decoder.decode(TemporalElement.create(tvo));
		assertEquals(0, lt.getHour());
		assertEquals(0, lt.getMinute());
		assertEquals(0, lt.getSecond());
		assertEquals(0, lt.getNano());
	}

	private void assertTaMatch(TemporalAccessor expected, TemporalAccessor actual) {
		ChronoField[] supportedEnonFields = new ChronoField[]{
			ChronoField.YEAR, ChronoField.MONTH_OF_YEAR, ChronoField.DAY_OF_MONTH,
			ChronoField.HOUR_OF_DAY, ChronoField.MINUTE_OF_HOUR, ChronoField.SECOND_OF_MINUTE, ChronoField.NANO_OF_SECOND,
			ChronoField.OFFSET_SECONDS
		};
		for (ChronoField field : supportedEnonFields) {
			if (actual.isSupported(field)) {
				assertEquals(expected.getLong(field), actual.getLong(field));
			}
		}
	}

	@Test
	public void testUnsupportedTAFail() {
		LOGGER.info("unsupportedTAFail");

		// currently not supporting IsoEra as a long value
		assertNull(acceptor.decoderFor(IsoEra.class));
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testUnsupportedElementFail() {
		LOGGER.info("unsupportedElementFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			// can't decode a NaN element
			acceptor.decoderFor(Date.class).decode(ConstantElement.nanInstance());
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testInvalidNumberFail() {
		LOGGER.info("invalidNumberFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			// can't decode a NaN element
			acceptor.decoderFor(GregorianCalendar.class).decode(new NumberElement("bob"));
		});
	}

}
