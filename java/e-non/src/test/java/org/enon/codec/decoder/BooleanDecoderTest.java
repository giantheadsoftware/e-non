/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import org.enon.codec.Value;
import org.enon.element.ByteElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.Element;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.ListElement;
import org.enon.element.LongElement;
import org.enon.element.NumberElement;
import org.enon.element.ShortElement;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class BooleanDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(BooleanDecoderTest.class.getName());

	private final BooleanDecoder.Acceptor acceptor = new BooleanDecoder.Acceptor();

	public BooleanDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class BooleanDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");
		assertSame(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(boolean.class.getName()));
		BooleanDecoder instance = acceptor.decoderFor(boolean.class.getName());

		// fixed value elements
		assertNull(instance.decode(ConstantElement.nullInstance()));
		assertTrue(instance.decode(ConstantElement.trueInstance()));
		assertTrue(instance.decode(ConstantElement.negativeInfinityInstance()));
		assertTrue(instance.decode(ConstantElement.positiveInfinityInstance()));
		assertFalse(instance.decode(ConstantElement.falseInstance()));
		assertFalse(instance.decode(ConstantElement.nanInstance()));

		//number elements
		for (Element t : new Element[]{
			new ByteElement((byte) 1),
			new ShortElement((short) 15),
			new IntElement(-1),
			new LongElement(Long.MAX_VALUE),
			new FloatElement(1f),
			new DoubleElement(-1.0),
			new NumberElement(new BigDecimal(Float.MIN_VALUE)),
			new NumberElement(BigInteger.ONE)
		}) {
			assertTrue(instance.decode(t), "Should evaluate to TRUE: " + t.getValue());
		}

		for (Element f : new Element[]{
			new ByteElement((byte) 0),
			new ShortElement((short) 0),
			new IntElement(0),
			new LongElement(0),
			new FloatElement(0f),
			new DoubleElement(-0.0),
			new NumberElement(BigDecimal.ZERO),
			new NumberElement(BigInteger.ZERO)
		}) {
			assertFalse(instance.decode(f), "Should evaluate to FALSE: " + f.getValue());
		}

		//String elements
		for (String t : Value.TRUE_STRINGS) {
			assertTrue(instance.decode(new StringElement(t)));
			if (t.length() == 1) {
				assertTrue(instance.decode(new StringElement(t.charAt(0))));
			} else {
				// change the case of a char
				char c1 = t.charAt(1);
				String t2 = Character.isUpperCase(c1) ? t.replace(c1, Character.toLowerCase(c1)) : t.replace(c1, Character.toUpperCase(c1));
				assertTrue(instance.decode(new StringElement(t2)));
			}
		}

		for (String f : Value.FALSE_STRINGS) {
			assertFalse(instance.decode(new StringElement(f)));
			if (f.length() == 1) {
				assertFalse(instance.decode(new StringElement(f.charAt(0))));
			} else {
				// change the case of a char
				char c1 = f.charAt(1);
				String f2 = Character.isUpperCase(c1) ? f.replace(c1, Character.toLowerCase(c1)) : f.replace(c1, Character.toUpperCase(c1));
				assertFalse(instance.decode(new StringElement(f2)));
			}
		}
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeBadStringFail() {
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			acceptor.decoderFor(Boolean.class).decode(new StringElement("xoxoxo"));
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeBadElementFail() {
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			acceptor.decoderFor(Boolean.class).decode(new ListElement(Collections.EMPTY_LIST));
		});
	}

	@Test
	public void testFactoryCache() {
		assertSame(acceptor.decoderFor(boolean.class), acceptor.decoderFor(Boolean.class));
	}

	@Test
	public void testFactoryNull() {
		LOGGER.info("factoryNull");
		assertNull(acceptor.decoderFor(""));
		assertNull(acceptor.decoderFor(String.class));
	}

//	@Test(/*expected = UnsupportedOperationException.class*/)
//	public void testFactoryCollectionDecoderFail() {
//		LOGGER.info("factoryCollectionDecoderFail");
//
//		factory.collectionDecoder(List.class, Integer.class);
//	}
//
//	@Test(/*expected = UnsupportedOperationException.class*/)
//	public void testFactoryMapDecoderFail() {
//		LOGGER.info("factoryCollectionDecoderFail");
//
//		factory.mapDecoder(Map.class, String.class, Integer.class);
//	}
}
