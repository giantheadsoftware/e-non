/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.bind.value.TemporalValue;
import org.enon.codec.DataCategory;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TemporalCoderTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(TemporalCoderTest.class.getName());

	private final TemporalCoder coderX = new TemporalCoder(new EnonConfig.Builder(FeatureSet.X).build());
	private final TemporalCoder coder0 = new TemporalCoder(new EnonConfig.Builder().build());

	public TemporalCoderTest() {
	}
	
	@BeforeAll
	public static void setUpClass() {
	}
	
	@AfterAll
	public static void tearDownClass() {
	}
	
	@BeforeEach
	public void setUp() {
	}
	
	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of handles method, of class TemporalCoder.
	 */
	@Test
	public void testHandles() {
		LOGGER.info("handles");
		assertTrue(coderX.handles().contains(DataCategory.TEMPORAL));
		assertTrue(coder0.handles().isEmpty());
		assertFalse(coderX.handles().contains(DataCategory.BOOLEAN));
	}

	/**
	 * Test of getElement method, of class TemporalCoder.
	 */
	@Test
	public void testGetElement() {
		LOGGER.info("getElement");
		Object temporal = LocalDate.of(2018, 11, 10);
		TemporalValue temporalValue = new TemporalValue.Acceptor(EnonConfig.defaults()).toNativeValue(temporal.getClass(), temporal);
		
		// test with extended types
		Element result = coderX.getElement(temporalValue);
		assertNotNull(result);
		assertSame(ElementType.TEMPORAL_ELEMENT, result.getType());
		
		// test without extended types
		result = coder0.getElement(temporalValue);
		assertNotNull(result);
		assertSame(ElementType.MAP_ELEMENT, result.getType());
		
		// test with nanoseconds 
		temporal = LocalDateTime.of(2019, Month.MARCH, 26, 22, 47, 05, 5);
		temporalValue = new TemporalValue.Acceptor(EnonConfig.defaults()).toNativeValue(temporal.getClass(), temporal);
		// test with extended types
		result = coderX.getElement(temporalValue);
		assertNotNull(result);
		assertSame(ElementType.TEMPORAL_ELEMENT, result.getType());
	}
	
}
