package org.enon.codec.decoder;

import org.enon.element.Element;
import org.enon.element.IntElement;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class EnumDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumDecoderTest.class.getName());

	public EnumDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testAcceptor() {
		LOGGER.info("accept & create");
		EnumDecoder.Acceptor acceptor = new EnumDecoder.Acceptor();
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(TestEnum.class));

		assertEquals(TargetTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(String.class));

		EnumDecoder decoder = acceptor.create(TestEnum.class);
		assertNotNull(decoder);

		// should have been cached, not a new instance
		assertSame(decoder, acceptor.create(TestEnum.class));
	}

	/**
	 * Test of decode method, of class EnumDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");
		EnumDecoder.Acceptor acceptor = new EnumDecoder.Acceptor();

		EnumDecoder decoder = acceptor.create(TestEnum.class);

		assertSame(TestEnum.TEST, decoder.decode(new StringElement(TestEnum.TEST.name())));
		assertSame(TestEnum.TEST, decoder.decode(new IntElement(0)));

		assertSame(TestEnum.THESE, decoder.decode(new StringElement(TestEnum.THESE.name())));
		assertSame(TestEnum.THESE, decoder.decode(new IntElement(1)));
		assertSame(TestEnum.VALUES, decoder.decode(new StringElement(TestEnum.VALUES.name())));
		assertSame(TestEnum.VALUES, decoder.decode(new IntElement(2)));
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeFailBadString() {
		LOGGER.info("decodeFailBadString");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			EnumDecoder.Acceptor acceptor = new EnumDecoder.Acceptor();

			EnumDecoder decoder = acceptor.create(TestEnum.class);

			decoder.decode(new StringElement("FIX"));
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeFailBadOrd() {
		LOGGER.info("decodeFailBadOrd");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			EnumDecoder.Acceptor acceptor = new EnumDecoder.Acceptor();

			EnumDecoder decoder = acceptor.create(TestEnum.class);

			decoder.decode(new IntElement(-1));
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeFailBadOrd2() {
		LOGGER.info("decodeFailBadOrd2");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			EnumDecoder.Acceptor acceptor = new EnumDecoder.Acceptor();

			EnumDecoder decoder = acceptor.create(TestEnum.class);

			decoder.decode(new IntElement(5));
		});
	}

	private enum TestEnum {
		TEST,
		THESE,
		VALUES
	}
}
