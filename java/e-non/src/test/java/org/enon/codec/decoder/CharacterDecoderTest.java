/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import org.enon.element.ByteElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.Element;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.LongElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class CharacterDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(CharacterDecoderTest.class.getName());

	CharacterDecoder.Acceptor acceptor = new CharacterDecoder.Acceptor();

	public CharacterDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class CharacterDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");

		CharacterDecoder decoder = acceptor.decoderFor(char.class.getName());

		Character value = decoder.decode(new ByteElement((byte) 3));
		assertTypeAndValue(Character.class, (char) 3, value);

		value = decoder.decode(new StringElement("c"));
		assertTypeAndValue(Character.class, 'c', value);

		value = decoder.decode(new NumberElement("9"));
		assertTypeAndValue(Character.class, '9', value);

		value = decoder.decode(new NanoIntElement((byte) 35));
		assertTypeAndValue(Character.class, (char) 35, value);

		value = decoder.decode(new IntElement('\n'));
		assertTypeAndValue(Character.class, '\n', value);

		value = decoder.decode(new LongElement('D'));
		assertTypeAndValue(Character.class, 'D', value);

		assertNull(decoder.decode(ConstantElement.nullInstance()));

	}

	private void assertTypeAndValue(Class expectedClass, Character expectedValue, Character value) {
		assertSame(expectedClass, value.getClass());
		assertEquals(expectedValue, value);
	}

	@Test
	public void testAccept() {
		LOGGER.info("accept");

		assertSame(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(char.class.getName()));
		assertNotNull(acceptor.decoderFor(char.class.getName()));

		assertSame(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(char.class));
		assertNotNull(acceptor.decoderFor(char.class));

		assertSame(TargetTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(Byte.class.getName()));
		assertNull(acceptor.decoderFor(Byte.class.getName()));

		assertSame(TargetTypeAcceptor.AcceptLevel.NEVER, acceptor.accept(int.class));
		assertNull(acceptor.decoderFor(int.class));
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testBadCodePointLongFail() {
		LOGGER.info("badCodePointLongFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			acceptor.decoderFor(char.class).decode(new LongElement(Long.MAX_VALUE));
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testBadCodePointSupplementaryFail() {
		LOGGER.info("badCodePointSupplementaryFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			acceptor.decoderFor(char.class).decode(new IntElement(0x10080));
		});
	}

	@Test
	public void testCantDecodeElement() {
		LOGGER.info("cantDecodeElement");

		Element[] elements = new Element[]{
			new FloatElement(7.0f),
			new DoubleElement(-7.0f),
			new IntElement(-200),
			ConstantElement.falseInstance(),
			ConstantElement.trueInstance(),
			ConstantElement.nanInstance(),
			ConstantElement.negativeInfinityInstance(),
			ConstantElement.positiveInfinityInstance()
		};
		CharacterDecoder decoder = acceptor.decoderFor(char.class.getName());
		for (Element e : elements) {
			try {
				decoder.decode(e);
				fail("should fail to decode " + e.getType());
			} catch (EnonDecoderException x) {
				// success
			}
		}
	}
}
