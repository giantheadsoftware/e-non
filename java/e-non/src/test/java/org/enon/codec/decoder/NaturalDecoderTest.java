/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.enon.ReaderContext;
import org.enon.element.ConstantElement;
import org.enon.element.Element;
import org.enon.element.ListElement;
import org.enon.element.StringElement;
import org.enon.test.util.ExceptionInputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class NaturalDecoderTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(NaturalDecoderTest.class.getName());

	public NaturalDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class NaturalDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");

		NaturalDecoder instance = new NaturalDecoder.Acceptor().decoderFor((Type)null);

		ReaderContext ctx = new ReaderContext(new ExceptionInputStream());

		Element element = ConstantElement.nullInstance();
		assertEquals(element.getValue(), instance.decode(element));

		element = ConstantElement.falseInstance();
		assertEquals(element.getValue(), instance.decode(element));

		element = new StringElement("testing");
		assertEquals(element.getValue(), instance.decode(element));

		element = new StringElement('x');
		assertEquals(element.getValue(), instance.decode(element));


		element = new ListElement(Collections.EMPTY_LIST);
		element.inContainer(ctx.getRoot());
		assertEquals(ArrayList.class, instance.decode(element).getClass());
	}

	@Test
	public void testFactoryCache() {
		LOGGER.info("factoryCache");
		NaturalDecoder.Acceptor factory = new NaturalDecoder.Acceptor();
		assertSame(factory.decoderFor(ArrayList.class), factory.decoderFor(ArrayList.class.getName()));
	}

	@Test
	public void testFactoryNull() {
		LOGGER.info("factoryNull");
		NaturalDecoder.Acceptor factory = new NaturalDecoder.Acceptor();
		assertNull(factory.decoderFor(""));
		assertNull(factory.decoderFor(String.class));
		assertNull(factory.decoderFor(Map.class));
	}

}
