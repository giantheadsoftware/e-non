/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import org.enon.element.array.ByteArrayElement;
import org.enon.element.ByteElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.LongElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.ShortElement;
import org.enon.element.StringElement;
import org.enon.util.SerializeUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class BlobDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(BlobDecoderTest.class.getName());

	private final BlobDecoder.Acceptor acceptor = new BlobDecoder.Acceptor();

	public BlobDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class BlobDecoder.
	 * @throws java.io.IOException
	 */
	@Test
	public void testDecode() throws IOException {
		LOGGER.info("decode");
		assertSame(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(byte[].class));
		BlobDecoder instance = acceptor.decoderFor(byte[].class.getName());

		// fixed value elements
		assertNull(instance.decode(ConstantElement.nullInstance()));
//		assertTrue(instance.decode(ConstantElement.negativeInfinityInstance()));
//		assertTrue(instance.decode(ConstantElement.positiveInfinityInstance()));
//		assertFalse(instance.decode(ConstantElement.nanInstance()));

		// blob elements
		byte[] bytes = instance.decode(new ByteArrayElement("Blob test".getBytes()));
		assertEquals(new String(bytes), "Blob test");

		// byte-size elements
		bytes = instance.decode(new ByteElement((byte)75));
		assertEquals(1, bytes.length);
		assertEquals((byte)75, bytes[0]);

		bytes = instance.decode(new NanoIntElement((byte)62));
		assertEquals(1, bytes.length);
		assertEquals((byte)62, bytes[0]);

		bytes = instance.decode(new ShortElement((short)-504));
		assertEquals(ShortElement.CONTENT_LENGTH, bytes.length);
		assertEquals((short)-504, new DataInputStream(new ByteArrayInputStream(bytes)).readShort());

		bytes = instance.decode(new IntElement(-504));
		assertEquals(IntElement.CONTENT_LENGTH, bytes.length);
		assertEquals(-504, new DataInputStream(new ByteArrayInputStream(bytes)).readInt());

		bytes = instance.decode(new LongElement(Long.MIN_VALUE));
		assertEquals(LongElement.CONTENT_LENGTH, bytes.length);
		assertEquals(Long.MIN_VALUE, new DataInputStream(new ByteArrayInputStream(bytes)).readLong());

		bytes = instance.decode(new FloatElement(Float.MIN_VALUE));
		assertEquals(FloatElement.CONTENT_LENGTH, bytes.length);
		assertEquals(Float.MIN_VALUE, new DataInputStream(new ByteArrayInputStream(bytes)).readFloat(), Float.MIN_VALUE);

		bytes = instance.decode(new DoubleElement(Double.MIN_VALUE));
		assertEquals(DoubleElement.CONTENT_LENGTH, bytes.length);
		assertEquals(Double.MIN_VALUE, new DataInputStream(new ByteArrayInputStream(bytes)).readDouble(), Double.MIN_VALUE);

		//String elements
		StringElement se = new StringElement("String el. test");
		assertEquals(se.getValue(), new String(instance.decode(se)));

		NumberElement ne = new NumberElement("12345.67890");
		assertEquals(ne.getValue(), new String(instance.decode(ne)));

		// Char element
		StringElement ce = new StringElement('x');
		bytes = instance.decode(ce);
		assertEquals(1, bytes.length);
		assertEquals(ce.getValue().charAt(0), new String(bytes).charAt(0));

		ce = new StringElement('\u0e01');
		bytes = instance.decode(ce);
		assertEquals(3, bytes.length);
		assertEquals(ce.getValue().charAt(0), new String(bytes).charAt(0));

		// fixed value elements
		bytes = instance.decode(ConstantElement.trueInstance());
		assertEquals(1, bytes.length);
		assertEquals((byte)1, bytes[0]);

		bytes = instance.decode(ConstantElement.falseInstance());
		assertEquals(1, bytes.length);
		assertEquals((byte)0, bytes[0]);

		bytes = instance.decode(ConstantElement.nanInstance());
		assertEquals(4, bytes.length);
		assertArrayEquals(SerializeUtil.getInstance().networkBytes(Float.NaN), bytes);

		bytes = instance.decode(ConstantElement.negativeInfinityInstance());
		assertEquals(4, bytes.length);
		assertArrayEquals(SerializeUtil.getInstance().networkBytes(Float.NEGATIVE_INFINITY), bytes);

		bytes = instance.decode(ConstantElement.positiveInfinityInstance());
		assertEquals(4, bytes.length);
		assertArrayEquals(SerializeUtil.getInstance().networkBytes(Float.POSITIVE_INFINITY), bytes);
	}

	@Test
	public void testFactoryCache() {
		LOGGER.info("factoryCache");
		assertSame(acceptor.decoderFor(byte[].class), acceptor.decoderFor(byte[].class.getName()));
	}

	@Test
	public void testFactoryNull() {
		LOGGER.info("factoryNull");
		assertNull(acceptor.decoderFor(""));
		assertNull(acceptor.decoderFor(String.class));
	}

//	@Test(/*expected = UnsupportedOperationException.class*/)
//	public void testFactoryCollectionDecoderFail() {
//		LOGGER.info("factoryCollectionDecoderFail");
//
//		factory.collectionDecoder(List.class, Integer.class);
//	}
//
//	@Test(/*expected = UnsupportedOperationException.class*/)
//	public void testFactoryMapDecoderFail() {
//		LOGGER.info("factoryCollectionDecoderFail");
//
//		factory.mapDecoder(Map.class, String.class, Integer.class);
//	}
}
