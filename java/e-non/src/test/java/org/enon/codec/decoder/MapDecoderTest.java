/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.enon.ReaderContext;
import org.enon.element.ConstantElement;
import org.enon.element.Element;
import org.enon.element.IntElement;
import org.enon.element.MapElement;
import org.enon.element.MapEntryElement;
import org.enon.element.RootContainer;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.enon.test.util.ExceptionInputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class MapDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(MapDecoderTest.class.getName());

	private MapDecoder.Acceptor factory;

	private ReaderContext ctx;

	private RootContainer root;

	public MapDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		factory = new MapDecoder.Acceptor();
		ctx = new ReaderContext(ExceptionInputStream.INSTANCE);
		root = ctx.getRoot();
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testGetInstanceClassClassClass() {
		LOGGER.info("defaultCollection");

		MapElement me = new MapElement(Collections.EMPTY_MAP);
		me.inContainer(root);

		// default intances
		assertSame(LinkedHashMap.class, factory.decoderFor(null, null, null).decode(me).getClass());
		assertSame(LinkedHashMap.class, factory.decoderFor(Map.class, null, null).decode(me).getClass());
		assertSame(HashMap.class, factory.decoderFor(HashMap.class, null, null).decode(me).getClass());
		assertSame(TreeMap.class, factory.decoderFor(SortedMap.class, null, null).decode(me).getClass());
		assertSame(TreeMap.class, factory.decoderFor(NavigableMap.class, null, null).decode(me).getClass());
		assertSame(ConcurrentHashMap.class, factory.decoderFor(ConcurrentMap.class, null, null).decode(me).getClass());

		Map<Element, Element> data = new HashMap();
		data.put(new IntElement(99), new StringElement("12345"));
		me = new MapElement(data);
		me.inContainer(root);

		MapDecoder decoder = factory.decoderFor(Map.class, String.class, Number.class);
		Map m = decoder.decode(me);

		assertSame(LinkedHashMap.class, m.getClass());
		assertEquals(1, m.size());
		Map.Entry entry = (Map.Entry) m.entrySet().iterator().next();
		assertSame(String.class, entry.getKey().getClass());
		assertSame(Short.class, entry.getValue().getClass());

		// verify cache
		assertSame(decoder, factory.decoderFor(Map.class, String.class, Number.class));
	}

	@Test
	public void testGetInstanceType() throws NoSuchMethodException {
		LOGGER.info("getInstanceType");

		Map<Element, Element> data = new HashMap();
		data.put(new IntElement(99), new StringElement("12345"));
		MapElement me = new MapElement(data);
		me.inContainer(root);

		// null
		MapDecoder decoder = factory.decoderFor((Type) null);
		Map m = decoder.decode(me);

		assertSame(LinkedHashMap.class, m.getClass());
		assertEquals(1, m.size());
		Map.Entry entry = (Map.Entry) m.entrySet().iterator().next();
		assertSame(Integer.class, entry.getKey().getClass());
		assertSame(String.class, entry.getValue().getClass());

		// verify cache
		assertSame(decoder, factory.decoderFor((Type) null));

		// Map
		Type type = MapPojo.class.getMethod("setMap", Map.class).getGenericParameterTypes()[0];
		decoder = factory.decoderFor(type);
		m = decoder.decode(me);

		assertSame(LinkedHashMap.class, m.getClass());
		assertEquals(1, m.size());
		entry = (Map.Entry) m.entrySet().iterator().next();
		assertSame(Integer.class, entry.getKey().getClass());
		assertSame(String.class, entry.getValue().getClass());

		// verify cache
		assertSame(decoder, factory.decoderFor(type));

		///////////////////////
		type = MapPojo.class.getMethod("setMapStringLong", Map.class).getGenericParameterTypes()[0];
		decoder = factory.decoderFor(type);
		m = decoder.decode(me);

		assertSame(LinkedHashMap.class, m.getClass());
		assertEquals(1, m.size());
		entry = (Map.Entry) m.entrySet().iterator().next();
		assertSame(String.class, entry.getKey().getClass());
		assertSame(Long.class, entry.getValue().getClass());

		// verify cache
		assertSame(decoder, factory.decoderFor(type));
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDefaultCollectionNoDefaultFail() {
		LOGGER.info("defaultCollectionNoDefaultFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			MapElement le = new MapElement(Collections.EMPTY_MAP);
			le.inContainer(root);

			factory.decoderFor(UnknownMapInterface.class, null, null).decode(le).getClass();
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testFindConstructorNoMatchingFail() {
		LOGGER.info("findConstructorNoMatchingFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			MapElement le = new MapElement(Collections.EMPTY_MAP);
			le.inContainer(root);

			factory.decoderFor(MissingConstructorMap.class, null, null).decode(le).getClass();
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testFindConstructorExceptionFail() {
		LOGGER.info("findConstructorPrivateFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			MapElement le = new MapElement(Collections.EMPTY_MAP);
			le.inContainer(root);

			factory.decoderFor(ExceptionMap.class, null, null).decode(le).getClass();
		});
	}

	/**
	 * Test of decode method, of class MapDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");
		Map<Element, Element> elementMap = new LinkedHashMap<>();
		elementMap.put(new StringElement("test key"), new IntElement(105));
		elementMap.put(new StringElement("this key"), new IntElement(987));
		elementMap.put(new StringElement("map key"), new IntElement(998764));
		MapElement<Element, Element> me = new MapElement<>(elementMap);

		me.inContainer(root);

		List<String> strings = new ArrayList<>();
		LOGGER.info(strings.getClass().toGenericString());
		MapDecoder instance = factory.decoderFor(HashMap.class, String.class, Long.class);

		Map result = instance.decode(me);

		assertSame(HashMap.class, result.getClass());
		assertEquals(me.getValue().size(), result.size());
		for (MapEntryElement<Element, Element> entry : me.getValue()) {
			String key = (String) entry.getValue().getKey().getValue();
			assertEquals(((Integer) entry.getValue().getValue().getValue()).longValue(), result.get(key));
		}

		// cached constructor
		assertEquals(result, instance.decode(me));

		// decode null
		assertNull(instance.decode(ConstantElement.nullInstance()));
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeWrongElementTypeFail() {
		LOGGER.info("decodeWrongElementTypeFail");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			factory.decoderFor(null, null, null).decode(ConstantElement.nanInstance());
		});
	}

	@Test
	public void testFactoryCache() {
		LOGGER.info("factoryCache");
		assertSame(factory.decoderFor(HashMap.class), factory.decoderFor(HashMap.class.getName()));
	}

	@Test
	public void testFactoryNull() {
		LOGGER.info("factoryNull");
		assertNull(factory.decoderFor(""));
		assertNull(factory.decoderFor(String.class));
		assertNull(factory.decoderFor(List.class));
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	private static class MapPojo {

		public void setMap(Map map) {
		}

		public void setMapStringLong(Map<String, Long> map) {
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	private static interface UnknownMapInterface<K, V> extends Map<K, V> {

	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	private static class MissingConstructorMap<K, V> extends HashMap<K, V> {

		public MissingConstructorMap(Map<? extends K, V> m) {
			super(m);
		}

	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	private static class ExceptionMap<K, V> extends HashMap<K, V> {

		public ExceptionMap(int size) {
			throw new IllegalArgumentException("expected by test");
		}
	}
}
