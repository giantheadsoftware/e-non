/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import org.enon.ReaderContext;
import org.enon.element.ConstantElement;
import org.enon.element.ListElement;
import org.enon.element.RootContainer;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.enon.test.util.ExceptionInputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class CollectionDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(CollectionDecoderTest.class.getName());

	private RootContainer root;

	private CollectionDecoder.Acceptor acceptor;
	private ReaderContext ctx;

	public CollectionDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		acceptor = new CollectionDecoder.Acceptor();
		ctx = new ReaderContext(ExceptionInputStream.INSTANCE);
		root = ctx.getRoot();
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testGetInstanceClassClass() {
		LOGGER.info("getInstanceClassClass");

		ListElement le = new ListElement(Collections.EMPTY_LIST);
		le.inContainer(root);

		// default instances
		assertSame(ArrayList.class, acceptor.decoderFor(null, null).decode(le).getClass());
		assertSame(ArrayList.class, acceptor.decoderFor(List.class, null).decode(le).getClass());
		assertSame(LinkedList.class, acceptor.decoderFor(LinkedList.class, null).decode(le).getClass());
		assertSame(LinkedHashSet.class, acceptor.decoderFor(Set.class, null).decode(le).getClass());
		assertSame(HashSet.class, acceptor.decoderFor(HashSet.class, null).decode(le).getClass());
		assertSame(TreeSet.class, acceptor.decoderFor(SortedSet.class, null).decode(le).getClass());
		assertSame(TreeSet.class, acceptor.decoderFor(NavigableSet.class, null).decode(le).getClass());
		assertSame(ArrayDeque.class, acceptor.decoderFor(Queue.class, null).decode(le).getClass());
		assertSame(ArrayDeque.class, acceptor.decoderFor(Deque.class, null).decode(le).getClass());

		le = new ListElement(Arrays.asList(new StringElement("12345")));
		le.inContainer(root);

		CollectionDecoder decoder = acceptor.decoderFor(List.class, Number.class);
		Collection c = decoder.decode(le);

		assertSame(ArrayList.class, c.getClass());
		assertEquals(1, c.size());
		assertSame(Short.class, c.iterator().next().getClass());

		// verify cache
		assertSame(decoder, acceptor.decoderFor(List.class, Number.class));
	}

	@Test
	public void testGetInstanceType() throws NoSuchMethodException {
		LOGGER.info("getInstanceType");

		ListElement le = new ListElement(Arrays.asList(new StringElement("12345")));
		le.inContainer(root);

		// null
		assertNull(acceptor.decoderFor((Type) null));
		CollectionDecoder decoder = acceptor.decoderFor(null, null);
		Collection c = decoder.decode(le);
		// verify cache
		assertSame(decoder, acceptor.decoderFor(null, null));

		assertSame(ArrayList.class, c.getClass());
		assertEquals(1, c.size());
		assertSame(String.class, c.iterator().next().getClass());

		// List<>
		Type pt = Pojo.class.getMethod("setList", List.class).getGenericParameterTypes()[0];

		decoder = acceptor.decoderFor(pt);
		c = decoder.decode(le);

		assertSame(ArrayList.class, c.getClass());
		assertEquals(1, c.size());
		assertSame(String.class, c.iterator().next().getClass());
		// verify cache
		assertSame(decoder, acceptor.decoderFor(pt));

		// List<String>
		pt = Pojo.class.getMethod("setListString", List.class).getGenericParameterTypes()[0];

		decoder = acceptor.decoderFor(pt);
		c = decoder.decode(le);

		assertSame(ArrayList.class, c.getClass());
		assertEquals(1, c.size());
		assertSame(String.class, c.iterator().next().getClass());

		// Set<Integer>
		pt = Pojo.class.getMethod("setSetInt", HashSet.class).getGenericParameterTypes()[0];

		decoder = acceptor.decoderFor(pt);
		c = decoder.decode(le);

		assertSame(HashSet.class, c.getClass());
		assertEquals(1, c.size());
		assertSame(Integer.class, c.iterator().next().getClass());

		// Set<List<Integer>>
		ListElement le2 = new ListElement(Arrays.asList(le));
		le.inContainer(le2);
		le2.inContainer(root);
		pt = Pojo.class.getMethod("setSetListInt", HashSet.class).getGenericParameterTypes()[0];

		decoder = acceptor.decoderFor(pt);
		c = decoder.decode(le2);

		assertSame(HashSet.class, c.getClass());
		assertEquals(1, c.size());
		List<Integer> firstItem = (List<Integer>) c.iterator().next();
		assertSame(ArrayList.class, firstItem.getClass());
		assertEquals(12345, (int) firstItem.get(0));
	}

	/**
	 * Test situation where subclass of collection has no type variables but passes an arg up to Collection
	 *
	 * @throws java.lang.NoSuchMethodException
	 */
	@Test
	public void testGetInstanceNoArgType() throws NoSuchMethodException {
		LOGGER.info("getInstanceTypeParamSwitch");

		ListElement le = new ListElement(Arrays.asList(new StringElement("12345")));
		le.inContainer(root);

		// List<>
		Type pt = Pojo.class.getMethod("setNoArgList", NoArgList.class).getGenericParameterTypes()[0];

		CollectionDecoder decoder = acceptor.decoderFor(pt);
		Collection c = decoder.decode(le);

		assertSame(NoArgList.class, c.getClass());
		assertEquals(1, c.size());
		assertSame(Integer.class, c.iterator().next().getClass());
	}

	/**
	 * Test situation where subclass of collection has multiple type variables and the first one is not the one passed up to Collection
	 */
	@Test
	public void testGetInstanceTypeParamSwitch() {
		LOGGER.info("getInstanceTypeParamSwitch");
	}

	@Test()
	public void testGetInstanceParameteriedTypeNotCollectionFail() throws NoSuchMethodException {
		LOGGER.info("getInstanceParameterizedType");

		ListElement le = new ListElement(Arrays.asList(new StringElement("12345")));
		le.inContainer(root);

		// List<String>
		ParameterizedType pt = (ParameterizedType) Pojo.class.getMethod("setMap", HashMap.class).getGenericParameterTypes()[0];

		assertNull(acceptor.decoderFor(pt));
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDefaultCollectionNoDefaultFail() {
		LOGGER.info("defaultCollectionNoDefaultFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			ListElement le = new ListElement(Collections.EMPTY_LIST);
			le.inContainer(root);

			acceptor.decoderFor(BlockingQueue.class, null).decode(le).getClass();
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testFindConstructorNoMatchingFail() {
		LOGGER.info("findConstructorNoMatchingFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			ListElement le = new ListElement(Collections.EMPTY_LIST);
			le.inContainer(root);

			acceptor.decoderFor(MissingConstructorSet.class, null).decode(le).getClass();
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testFindConstructorExceptionFail() {
		LOGGER.info("findConstructorPrivateFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			ListElement le = new ListElement(Collections.EMPTY_LIST);
			le.inContainer(root);

			acceptor.decoderFor(ExceptionSet.class, null).decode(le).getClass();
		});
	}

	/**
	 * Test of decode method, of class ListDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");
		ListElement<StringElement> le = new ListElement(Arrays.asList(new StringElement[]{
			new StringElement("test"), new StringElement("this"), new StringElement("list")}));

		le.inContainer(root);

		List<String> strings = new ArrayList<>();
		LOGGER.info(strings.getClass().toGenericString());
		CollectionDecoder instance = acceptor.decoderFor(ArrayList.class, String.class);

		Collection result = instance.decode(le);

		assertSame(ArrayList.class, result.getClass());
		assertEquals(le.getValue().size(), result.size());
		for (int i = 0; i < result.size(); i++) {
			assertEquals(le.getValue().get(i).getValue(), ((List) result).get(i));
		}

		// cached constructor
		assertEquals(result, instance.decode(le));

		// decode null
		assertNull(instance.decode(ConstantElement.nullInstance()));
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeWrongElementTypeFail() {
		LOGGER.info("decodeWrongElementTypeFail");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			acceptor.decoderFor(null, null).decode(ConstantElement.nanInstance());
		});
	}

	@Test
	public void testFactoryCache() {
		LOGGER.info("factoryCache");
		assertSame(acceptor.decoderFor(ArrayList.class), acceptor.decoderFor(ArrayList.class.getName()));
	}

	@Test
	public void testFactoryNull() {
		LOGGER.info("factoryNull");
		assertNull(acceptor.decoderFor(""));
		assertNull(acceptor.decoderFor(String.class));
		assertNull(acceptor.decoderFor(Map.class));
	}

	//////////////////////////////////////////////////////////////////////
	private static class MissingConstructorSet<T> extends HashSet<T> {

		public MissingConstructorSet(Collection<? extends T> c) {
			super(c);
		}

	}

	//////////////////////////////////////////////////////////////////////
	private static class ExceptionSet<T> extends HashSet<T> {

		public ExceptionSet(int size) {
			throw new IllegalArgumentException("expected by test");
		}

	}

	//////////////////////////////////////////////////////////////////////
	private static class Pojo {

		public void setList(List stringList) {

		}

		public void setListString(List<String> stringList) {

		}

		public void setSetInt(HashSet<Integer> intSet) {

		}

		public void setSetListInt(HashSet<List<Integer>> intSet) {

		}

		public void setMap(HashMap<String, Integer> map) {

		}

		public void setNoArgList(NoArgList list) {

		}

		public void setFunkyList(FunkyList<Integer, Long> funkyList) {

		}
	}

	//////////////////////////////////////////////////////////////////////
	private static class NoArgList extends ArrayList<Integer> {

		public NoArgList() {
		}

	}

	//////////////////////////////////////////////////////////////////////
	private static class FunkyList<A, B> extends ArrayList<B> {

		public FunkyList() {
		}

	}
}
