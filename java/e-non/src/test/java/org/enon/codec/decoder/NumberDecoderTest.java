/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.enon.element.array.ByteArrayElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.LongElement;
import org.enon.element.NumberElement;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class NumberDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(NumberDecoderTest.class.getName());

	public NumberDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class NumberDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");

		NumberDecoder decoder = new NumberDecoder.Acceptor().decoderFor(Number.class);

		Number n = decoder.decode(new LongElement(12345));
		assertEquals((short) 12345, n);

		n = decoder.decode(new DoubleElement(12345));
		assertEquals(12345.0, n);

		n = decoder.decode(new NumberElement("12345"));
		assertEquals((short) 12345, n);

		n = decoder.decode(new StringElement("12345.6"));
		assertEquals(new BigDecimal("12345.6"), n);

		n = decoder.decode(new StringElement('6'));
		assertEquals((byte) 6, n);

		n = decoder.decode(ConstantElement.falseInstance());
		assertEquals((byte) 0, n);

		n = decoder.decode(ConstantElement.trueInstance());
		assertEquals((byte) 1, n);

		n = decoder.decode(ConstantElement.negativeInfinityInstance());
		assertEquals(Float.NEGATIVE_INFINITY, n);

		n = decoder.decode(ConstantElement.positiveInfinityInstance());
		assertEquals(Float.POSITIVE_INFINITY, n);

		assertNull(decoder.decode(ConstantElement.nullInstance()));

		n = decoder.decode(new StringElement("bob"));
		assertEquals(Float.NaN, n);

		n = decoder.decode(new StringElement('c'));
		assertEquals(Float.NaN, n);

		n = decoder.decode(ConstantElement.nanInstance());
		assertEquals(Float.NaN, n);
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeUnknownElementFail() {
		LOGGER.info("decodeUnknownElementFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			new NumberDecoder.Acceptor().decoderFor(Number.class).decode(new ByteArrayElement(new byte[]{}));
		});
	}

	@Test
	public void testFactoryCache() {
		LOGGER.info("factoryCache");
		NumberDecoder.Acceptor factory = new NumberDecoder.Acceptor();
		assertSame(factory.decoderFor(Double.class), factory.decoderFor(Double.class.getName()));
	}

	@Test
	public void testFactoryNull() {
		LOGGER.info("factoryNull");
		NumberDecoder.Acceptor factory = new NumberDecoder.Acceptor();
		assertNull(factory.decoderFor(""));
		assertNull(factory.decoderFor(String.class));
		assertNull(factory.decoderFor(Map.class));
	}
}
