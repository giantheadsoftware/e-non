/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import org.enon.ReaderContext;
import org.enon.element.ConstantElement;
import org.enon.element.IntElement;
import org.enon.element.ListElement;
import org.enon.element.MapElement;
import org.enon.element.RootContainer;
import org.enon.element.ShortElement;
import org.enon.element.StringElement;
import org.enon.element.array.ShortArrayElement;
import org.enon.exception.EnonDecoderException;
import org.enon.test.util.ExceptionInputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ArrayDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArrayDecoderTest.class.getName());

	private static final RootContainer ROOT = new ReaderContext(new ExceptionInputStream()).getRoot();

	private ArrayDecoder.Acceptor acceptor;

	public ArrayDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		acceptor = new ArrayDecoder.Acceptor();
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class ArrayDecoder.
	 */
	@Test
	public void testDecode() {
		LOGGER.info("decode");

		ListElement le = new ListElement(Arrays.asList(new StringElement("123"), new ShortElement((short) -29)));
		le.inContainer(ROOT);

		//primitive short array
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(short[].class));
		ArrayDecoder<short[]> shortDecoder = acceptor.decoderFor(short[].class);
		short[] shortArray = shortDecoder.decode(le);
		assertEquals((short) 123, shortArray[0]);
		assertEquals((short) -29, shortArray[1]);

		//primitive int array
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(int[].class));
		ArrayDecoder<int[]> intDecoder = acceptor.decoderFor(int[].class);
		int[] intArray = intDecoder.decode(le);
		assertEquals(123, intArray[0]);
		assertEquals(-29, intArray[1]);

		//primitive long array
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(long[].class));
		ArrayDecoder<long[]> longDecoder = acceptor.decoderFor(long[].class);
		long[] longArray = longDecoder.decode(le);
		assertEquals(123L, longArray[0]);
		assertEquals(-29L, longArray[1]);

		//primitive float array
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(float[].class));
		ArrayDecoder<float[]> floatDecoder = acceptor.decoderFor(float[].class);
		float[] floatArray = floatDecoder.decode(le);
		assertEquals(123f, floatArray[0], Float.MIN_VALUE);
		assertEquals(-29f, floatArray[1], Float.MIN_VALUE);

		//primitive double array
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(double[].class));
		ArrayDecoder<double[]> doubleDecoder = acceptor.decoderFor(double[].class);
		double[] doubleArray = doubleDecoder.decode(le);
		assertEquals(123.0, doubleArray[0], Double.MIN_VALUE);
		assertEquals(-29.0, doubleArray[1], Double.MIN_VALUE);

		// leBool for boolean values
		ListElement leBool = new ListElement(Arrays.asList(new StringElement("T"), ConstantElement.falseInstance(), ConstantElement.trueInstance(), new ShortElement((short) 1234)));
		leBool.inContainer(ROOT);

		//primitive bool array
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(boolean[].class));
		ArrayDecoder<boolean[]> boolDecoder = acceptor.decoderFor(boolean[].class);
		boolean[] boolArray = boolDecoder.decode(leBool);
		assertTrue(boolArray[0]);
		assertFalse(boolArray[1]);
		assertTrue(boolArray[2]);
		assertTrue(boolArray[3]);

		// leChar for char values
		StringElement seChar = new StringElement("TF5");
		seChar.inContainer(ROOT);

		// primitive char array
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(char[].class));
		ArrayDecoder<char[]> charDecoder = acceptor.decoderFor(char[].class);
		char[] charArray = charDecoder.decode(seChar);
		assertEquals('T', charArray[0]);
		assertEquals('F', charArray[1]);
		assertEquals('5', charArray[2]);

		// le2 has a null entry -- can't use for primitive array
		ListElement le2 = new ListElement(Arrays.asList(new StringElement("12345"), ConstantElement.nullInstance(), new ShortElement((short) 1234)));
		le2.inContainer(ROOT);

		// Integer
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(Integer[].class));
		ArrayDecoder<Integer[]> decoder = acceptor.decoderFor(Integer[].class);
		Integer[] array = decoder.decode(le2);
		assertEquals(12345, (int) array[0]);
		assertNull(array[1]);
		assertEquals(1234, (int) array[2]);

		// String
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(String[].class));
		ArrayDecoder<String[]> stringDecoder = acceptor.decoderFor(String[].class);
		String[] stringArray = stringDecoder.decode(le2);
		assertEquals("12345", stringArray[0]);
		assertNull(stringArray[1]);
		assertEquals("1234", stringArray[2]);

		// Object array -- items should retain their "natural" value types
		assertEquals(TargetTypeAcceptor.AcceptLevel.PREFER, acceptor.accept(Object[].class));
		ArrayDecoder<Object[]> objectDecoder = acceptor.decoderFor(Object[].class);
		Object[] objectArray = objectDecoder.decode(le2);
		assertEquals("12345", objectArray[0]);
		assertNull(objectArray[1]);
		assertEquals((short) 1234, objectArray[2]);

		// null value
		assertNull(objectDecoder.decode(ConstantElement.nullInstance()));

		// can't decode target class of null
		assertEquals(TargetTypeAcceptor.AcceptLevel.NEVER, acceptor.accept((String) null));
		assertNull(acceptor.decoderFor((Type) null));
	}

	@Test
	public void testDecodeArray() {
		LOGGER.info("decodeArray");

		ShortArrayElement sae = new ShortArrayElement(new short[]{(short) 0, Short.MIN_VALUE, Short.MAX_VALUE});
		ArrayDecoder<short[]> shortDecoder = acceptor.decoderFor(short[].class);
		assertArrayEquals(sae.getValue(), shortDecoder.decode(sae.inContainer(ROOT)));

		// decode the same short[] into a Short[]
		ArrayDecoder<Short[]> shortObjectDecoder = acceptor.decoderFor(Short[].class);
		Short[] shortObs = shortObjectDecoder.decode(sae);
		assertEquals(sae.getValue().length, shortObs.length);
		for (int i = 0; i < shortObs.length; i++) {
			assertEquals(sae.getValue()[i], (short) shortObs[i]);
		}
	}

	@Test
	public void testDecodeNotListElement() {
		LOGGER.info("decodeNotListElement");

		//read single string to string array
		String[] result = (String[]) acceptor.decoderFor(String[].class).decode(new StringElement("single entry").inContainer(ROOT));

		assertEquals(1, result.length);
		assertEquals("single entry", result[0]);

		//read single number to string array
		result = (String[]) acceptor.decoderFor(String[].class).decode(new IntElement(123456789).inContainer(ROOT));

		assertEquals(1, result.length);
		assertEquals("123456789", result[0]);
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodePrimitiveNullFail() {
		LOGGER.info("decodePrimitiveNullFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			// le2 has a null entry -- can't use for primitive array
			ListElement le2 = new ListElement(Arrays.asList(new StringElement("12345"), ConstantElement.nullInstance(), new ShortElement((short) 1234)));
			le2.inContainer(ROOT);

			//primitive int array
			ArrayDecoder<int[]> intDecoder = acceptor.decoderFor(int[].class);
			intDecoder.decode(le2);
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testUnsupportedElementFail() {
		LOGGER.info("testUnsupportedElementFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			// MAP_ELEMENT can't be decoded to array
			acceptor.decoderFor(String[].class).decode(new MapElement(Collections.EMPTY_MAP).inContainer(ROOT));
		});
	}

	@Test
	public void testFactoryInstanceCache() {
		LOGGER.info("factoryInstanceCache");

		assertSame(acceptor.decoderFor(int[].class), acceptor.decoderFor(int[].class));
	}

//	@Test(/*expected = UnsupportedOperationException.class*/)
//	public void testFactoryCollectionDecoderFail() {
//		LOGGER.info("factoryCollectionDecoderFail");
//
//		acceptor.collectionDecoder(List.class, Integer.class);
//	}
//
//	@Test(/*expected = UnsupportedOperationException.class*/)
//	public void testFactoryMapDecoderFail() {
//		LOGGER.info("factoryCollectionDecoderFail");
//
//		acceptor.mapDecoder(Map.class, String.class, Integer.class);
//	}
}
