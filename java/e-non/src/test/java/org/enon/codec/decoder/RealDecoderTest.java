/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.enon.element.ByteElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.FloatElement;
import org.enon.element.ListElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.Element;
import org.enon.element.StringElement;
import org.enon.exception.EnonDecoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class RealDecoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(RealDecoderTest.class.getName());

	private final RealDecoder.Acceptor factory = new RealDecoder.Acceptor();

	public RealDecoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test
	public void testDecodeNull() {
		LOGGER.info("decode");

		RealDecoder decoder = factory.decoderFor(Float.class.getName());
		Number value = decoder.decode(ConstantElement.nullInstance());

		assertNull(value);

	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeNullPrimitiveFail() {
		LOGGER.info("decode");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			RealDecoder decoder = factory.decoderFor(float.class.getName());
			Number value = decoder.decode(ConstantElement.nullInstance());

			assertNull(value);
		});

	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test
	public void testDecodeFloat() {
		LOGGER.info("decodeFloat");

		RealDecoder decoder = factory.decoderFor(float.class.getName());

		Number value = decoder.decode(new FloatElement(Float.MIN_VALUE));
		assertTypeAndValue(Float.class, Float.MIN_VALUE, value);

		value = decoder.decode(new FloatElement(Float.MAX_VALUE));
		assertTypeAndValue(Float.class, Float.MAX_VALUE, value);

		value = decoder.decode(new ByteElement((byte) -23));
		assertTypeAndValue(Float.class, -23f, value);

		decoder = factory.decoderFor(Float.class.getName());
		value = decoder.decode(new ByteElement((byte) -23));
		assertTypeAndValue(Float.class, -23f, value);

		value = decoder.decode(new NanoIntElement((byte) 62));
		assertTypeAndValue(Float.class, 62f, value);

		value = decoder.decode(new NumberElement("-23"));
		assertTypeAndValue(Float.class, -23f, value);

		value = decoder.decode(new StringElement("-23"));
		assertTypeAndValue(Float.class, -23f, value);

		value = decoder.decode(new StringElement('7'));
		assertTypeAndValue(Float.class, 7f, value);

		value = decoder.decode(new FloatElement(7.0f));
		assertTypeAndValue(Float.class, 7f, value);

		value = decoder.decode(new DoubleElement(-7.0f));
		assertTypeAndValue(Float.class, -7f, value);

		value = decoder.decode(ConstantElement.positiveInfinityInstance());
		assertTypeAndValue(Float.class, Float.POSITIVE_INFINITY, value);

		value = decoder.decode(ConstantElement.negativeInfinityInstance());
		assertTypeAndValue(Float.class, Float.NEGATIVE_INFINITY, value);

		value = decoder.decode(ConstantElement.nanInstance());
		assertTypeAndValue(Float.class, Float.NaN, value);
	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test
	public void testDecodeDouble() {
		LOGGER.info("decodeDouble");

		RealDecoder decoder = factory.decoderFor(double.class.getName());

		Number value = decoder.decode(new FloatElement(Float.MIN_VALUE));
		assertTypeAndValue(Double.class, (double) Float.MIN_VALUE, value);

		value = decoder.decode(new FloatElement(Float.MAX_VALUE));
		assertTypeAndValue(Double.class, (double) Float.MAX_VALUE, value);

		value = decoder.decode(new DoubleElement(Double.MIN_VALUE));
		assertTypeAndValue(Double.class, Double.MIN_VALUE, value);

		value = decoder.decode(new DoubleElement(Double.MAX_VALUE));
		assertTypeAndValue(Double.class, Double.MAX_VALUE, value);

		value = decoder.decode(new ByteElement((byte) -23));
		assertTypeAndValue(Double.class, -23.0, value);

		decoder = factory.decoderFor(Double.class.getName());
		value = decoder.decode(new ByteElement((byte) -23));
		assertTypeAndValue(Double.class, -23.0, value);

		value = decoder.decode(new NanoIntElement((byte) 62));
		assertTypeAndValue(Double.class, 62.0, value);

		value = decoder.decode(new NumberElement("-23"));
		assertTypeAndValue(Double.class, -23.0, value);

		value = decoder.decode(new StringElement("-23"));
		assertTypeAndValue(Double.class, -23.0, value);

		value = decoder.decode(new StringElement('7'));
		assertTypeAndValue(Double.class, 7.0, value);

		value = decoder.decode(new FloatElement(7.0f));
		assertTypeAndValue(Double.class, 7.0, value);

		value = decoder.decode(new DoubleElement(-7.0f));
		assertTypeAndValue(Double.class, -7.0, value);

		value = decoder.decode(ConstantElement.positiveInfinityInstance());
		assertTypeAndValue(Double.class, Double.POSITIVE_INFINITY, value);

		value = decoder.decode(ConstantElement.negativeInfinityInstance());
		assertTypeAndValue(Double.class, Double.NEGATIVE_INFINITY, value);

		value = decoder.decode(ConstantElement.nanInstance());
		assertTypeAndValue(Double.class, Double.NaN, value);
	}

	private void assertTypeAndValue(Class expectedClass, Number expectedValue, Number value) {
		assertSame(expectedClass, value.getClass());
		assertEquals(expectedValue, value);
	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test()
	public void testDecodeFloatBadStringNaN() {
		LOGGER.info("decodeFloatBadStringNaN");

		RealDecoder decoder = factory.decoderFor(float.class.getName());
		assertEquals(Float.NaN, decoder.decode(new StringElement("hello")));
	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test()
	public void testDecodeDoubleBadStringNaN() {
		LOGGER.info("decodeDoubleBadStringNaN");

		RealDecoder decoder = factory.decoderFor(double.class.getName());
		assertEquals(Double.NaN, decoder.decode(new StringElement("hello")));
	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeBigBadStringNaN() {
		LOGGER.info("decodeBigBadStringNaN");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			RealDecoder decoder = factory.decoderFor(BigDecimal.class.getName());
			decoder.decode(new StringElement("hello"));
		});
	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeFloatTooBigFail() {
		LOGGER.info("decodeFloatTooBigFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			RealDecoder decoder = factory.decoderFor(float.class.getName());

			decoder.decode(new NumberElement(new BigDecimal(Double.MAX_VALUE).multiply(BigDecimal.TEN)));
		});
	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeFloatTooBigDoubleFail() {
		LOGGER.info("decodeFloatTooBigDoubleFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			RealDecoder decoder = factory.decoderFor(float.class.getName());

			decoder.decode(new DoubleElement((double) Float.MAX_VALUE * 10));
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeBigNotFiniteFail() {
		LOGGER.info("decodeBigNotFiniteFail");
		Exception ex = assertThrows(EnonDecoderException.class, () -> {
			RealDecoder decoder = factory.decoderFor(BigDecimal.class.getName());
			for (Element e : new Element[]{ConstantElement.negativeInfinityInstance(), ConstantElement.positiveInfinityInstance(), ConstantElement.nanInstance()}) {
				try {
					decoder.decode(e);
					fail("Should fail");
				} catch (IllegalArgumentException x) {
					// success
				}
			}
		});
	}

	/**
	 * Test of decode method, of class RealDecoder.
	 */
	@Test(/*/*expected = EnonDecoderException.class*/)
	public void testDecodeFloatBadCharNaN() {
		LOGGER.info("decodeFloatBadCharNaN");

		RealDecoder decoder = factory.decoderFor(float.class.getName());

		assertEquals(Float.NaN, decoder.decode(new StringElement('h')));
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecodeListFail() {
		LOGGER.info("decodeListFail");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			factory.decoderFor(double.class).decode(new ListElement(Collections.EMPTY_LIST));
		});
	}

	@Test
	public void testDecodeTrueFalse() {
		LOGGER.info("decodeTrueFalse");
		assertEquals(1.0, factory.decoderFor(double.class).decode(ConstantElement.trueInstance()));
		assertEquals(0.0, factory.decoderFor(double.class).decode(ConstantElement.falseInstance()));
	}

	@Test
	public void testFactoryCache() {
		LOGGER.info("factoryCache");
		assertSame(factory.decoderFor(Double.class), factory.decoderFor(Double.class.getName()));
	}

	@Test
	public void testFactoryNull() {
		LOGGER.info("factoryNull");
		assertNull(factory.decoderFor(""));
		assertNull(factory.decoderFor(String.class));
		assertNull(factory.decoderFor(Integer.class));
	}
}
