/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.bind.value.MapValue;
import org.enon.codec.DataCategory;
import org.enon.element.ElementType;
import org.enon.element.MapElement;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class MapCoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(MapCoderTest.class.getName());

	/**
	 * config for eNON-0
	 */
	private final EnonConfig config0 = new EnonConfig.Builder().build();

	public MapCoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of handles method, of class MapCoder.
	 */
	@Test
	public void testHandles() {
		LOGGER.info("handles");
		MapCoder instance = new MapCoder(config0);
		Set<DataCategory> result = instance.handles();
		assertTrue(result.contains(DataCategory.MAP));
	}

	/**
	 * Test of getElement method, of class MapCoder.
	 */
	@Test
	public void testGetElementEmpty() {
		LOGGER.info("getElementEmpty");
		Map<Object, Object> map = new HashMap<>();
		MapValue mapValue = new MapValue.Acceptor(config0).toNativeValue(map.getClass(), map);
		MapCoder instance = new MapCoder(config0);

		MapElement result = instance.getElement(mapValue);
		assertEquals(0, result.getSize().getSize());
	}

	/**
	 * Test of getElement method, of class MapCoder.
	 */
	@Test
	public void testGetElementNotEmpty() {
		LOGGER.info("getElementNotEmpty");
		Map<Object, Object> map = new LinkedHashMap<>();
		map.put("FirstKey", 27);
		map.put(95, "2nd value");
		MapValue mapValue = new MapValue.Acceptor(config0).toNativeValue(map.getClass(), map);
		MapCoder instance = new MapCoder(config0);

		MapElement result = instance.getElement(mapValue);
		assertEquals(ElementType.MAP_ELEMENT, result.getType());
		assertEquals(2, result.getSize().getSize());

		Map<Object, Object> nested = new LinkedHashMap<>();
		nested.put("nested key", 22.75);
		nested.put("nested 2", null);
		map.put("nested", nested);

		mapValue = new MapValue.Acceptor(config0).toNativeValue(map.getClass(), map);
		instance = new MapCoder(config0);

		result = instance.getElement(mapValue);
		assertEquals(ElementType.MAP_ELEMENT, result.getType());
		assertEquals(3, result.getSize().getSize());
	}
}
