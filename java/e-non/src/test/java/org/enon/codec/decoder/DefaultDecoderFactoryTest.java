/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.decoder;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.exception.EnonDecoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class DefaultDecoderFactoryTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDecoderFactoryTest.class.getName());

	public DefaultDecoderFactoryTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of decoder method, of class DefaultDecoderFactory.
	 */
	@Test
	public void testDecoder() {
		LOGGER.info("decoder");
		DefaultDecoderFactory instance = new DefaultDecoderFactory(EnonConfig.defaults());

		Decoder result = instance.decoderFor((String) null);
		assertSame(NaturalDecoder.class, result.getClass());

		assertSame(result, instance.decoderFor((Class) null)); // should return same instance

		result = instance.decoderFor(String.class.getName());
		assertSame(StringDecoder.class, result.getClass());

		result = instance.decoderFor(Integer.class.getName());
		assertSame(IntDecoder.class, result.getClass());

		result = instance.decoderFor(Double.class.getName());
		assertSame(RealDecoder.class, result.getClass());

		result = instance.decoderFor(Number.class.getName());
		assertSame(NumberDecoder.class, result.getClass());

		result = instance.decoderFor(HashMap.class.getName());
		assertSame(MapDecoder.class, result.getClass());

		result = instance.decoderFor(HashSet.class.getName());
		assertSame(CollectionDecoder.class, result.getClass());
	}

	/**
	 * Test of getDecoderCache method, of class DefaultDecoderFactory.
	 */
	@Test
	public void testGetDecoderCache() {
		LOGGER.info("getDecoderCache");
		DefaultDecoderFactory factory = new DefaultDecoderFactory(EnonConfig.defaults());
		Map<String, Decoder> cache = factory.getDecoderCache();

		assertNotNull(cache);

		assertSame(cache, factory.getDecoderCache());
	}

	/**
	 * Test of locateDecoder method, of class DefaultDecoderFactory.
	 */
	@Test
	public void testCreateDecoder() {
		LOGGER.info("createDecoder");
		DefaultDecoderFactory instance = new DefaultDecoderFactory(EnonConfig.defaults());

		assertSame(NaturalDecoder.class, instance.createDecoder(Object.class).getClass());
		assertSame(CollectionDecoder.class, instance.createDecoder(List.class).getClass());
		assertSame(MapDecoder.class, instance.createDecoder(Map.class).getClass());
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testDecoderNotFoundFail() {
		LOGGER.info("decoderNotFoundFail");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			DefaultDecoderFactory instance = new DefaultDecoderFactory(EnonConfig.defaults());

			instance.decoderFor("Not a class");
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testCreateDecoderNotFoundFail() {
		LOGGER.info("createDecoderNotFoundFail");
		Exception x = assertThrows(EnonDecoderException.class, () -> {
			DefaultDecoderFactory instance = new DefaultDecoderFactory(EnonConfig.defaults());

			instance.decoderFor(Class.class);
		});
	}

	@Test
	public void testCache() {
		LOGGER.info("cache");

		DefaultDecoderFactory instance = new DefaultDecoderFactory(EnonConfig.defaults());
		assertSame(instance.decoderFor(Integer.class), instance.decoderFor(Integer.class));
		assertSame(instance.decoderFor(Integer.class.getName()), instance.decoderFor(Integer.class.getName()));
	}

	@Test
	public void testContainerDecoders() {
		LOGGER.info("containerDecoders");

		DefaultDecoderFactory instance = new DefaultDecoderFactory(EnonConfig.defaults());

		assertSame(CollectionDecoder.class, instance.collectionDecoder(Collection.class, Integer.class).getClass());
		assertSame(MapDecoder.class, instance.mapDecoder(Map.class, String.class, Integer.class).getClass());
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testCreateFactoryListBadNoArgFail() {
		LOGGER.info("createFactoryListBadNoArgFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			new ErrorDecoderFactoryPrivateConstructor(EnonConfig.defaults()).createDecoder(String.class);
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testCreateFactoryListIllegalArgFail() {
		LOGGER.info("createFactoryListIllegalArgFail");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			new ErrorDecoderFactoryIllegalArg().createDecoder(String.class);
		});
	}

	@Test(/*expected = EnonDecoderException.class*/)
	public void testCreateFactoryListIllegalArgFail2() {
		LOGGER.info("createFactoryListIllegalArgFail2");

		Exception x = assertThrows(EnonDecoderException.class, () -> {
			new ErrorDecoderFactoryIllegalArg2(null).createDecoder(String.class);
		});
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	private static class ErrorDecoderFactoryPrivateConstructor extends DefaultDecoderFactory {

		private ErrorDecoderFactoryPrivateConstructor(EnonConfig config) {
			super(config);
		}

		@Override
		public List<Class<? extends TargetTypeAcceptor<? extends Decoder>>> getTargetTypeAcceptors() {
			// NOTE: Never put this class back into the factory impls list -- would be recursive.
			// this is only for a failure test!
			return Arrays.asList(new Class[]{this.getClass()});
		}

	}

	////////////////////////////////////////////////////////////////////////////////////////////
	private static class ErrorDecoderFactoryIllegalArg extends DefaultDecoderFactory {

		// this constructor can be used
		public ErrorDecoderFactoryIllegalArg() {
			super(EnonConfig.defaults());
		}

		// this one blows up
		public ErrorDecoderFactoryIllegalArg(EnonConfig config) {
			super(config);
			throw new IllegalArgumentException("expected by test");
		}

		@Override
		public List<Class<? extends TargetTypeAcceptor<? extends Decoder>>> getTargetTypeAcceptors() {
			// NOTE: Never put this class back into the factory impls list -- would be recursive.
			// this is only for a failure test!
			return Arrays.asList(new Class[]{this.getClass()});
		}

	}

	////////////////////////////////////////////////////////////////////////////////////////////
	private static class ErrorDecoderFactoryIllegalArg2 extends DefaultDecoderFactory {

		// this constructor can be used
		public ErrorDecoderFactoryIllegalArg2(Object unused) {
			super(EnonConfig.defaults());
		}

		// this one blows up
		public ErrorDecoderFactoryIllegalArg2() {
			super(EnonConfig.defaults());
			throw new IllegalArgumentException("expected by test");
		}

		@Override
		public List<Class<? extends TargetTypeAcceptor<? extends Decoder>>> getTargetTypeAcceptors() {
			// NOTE: Never put this class back into the factory impls list -- would be recursive.
			// this is only for a failure test!
			return Arrays.asList(new Class[]{this.getClass()});
		}

	}
}
