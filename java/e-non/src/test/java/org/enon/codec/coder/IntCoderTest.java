/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.codec.coder;

import java.math.BigInteger;
import java.util.EnumSet;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.bind.value.PojoValue;
import org.enon.bind.value.ScalarValue;
import org.enon.bind.value.StringValue;
import org.enon.codec.DataCategory;
import org.enon.exception.EnonCoderException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class IntCoderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntCoderTest.class.getName());

	public IntCoderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of handles method, of class IntCoder.
	 */
	@Test
	public void testHandles() {
		LOGGER.info("handles");
		IntCoder instance = new IntCoder(EnonConfig.defaults());
		Set<DataCategory> result = instance.handles();
		assertEquals(1, result.size());
		assertTrue(result.contains(DataCategory.NUM_INT));
	}

	/**
	 * Test of getElement method, of class IntCoder.
	 */
	@Test
	public void testGetElement() {
		LOGGER.info("getElement");
		EnonConfig configX = new EnonConfig.Builder(EnumSet.of(FeatureSet.X)).build();

		String[] intStrings = new String[]{"0", "-3", "12439875232343"};
		for (String t : intStrings) {
			assertEquals(t, String.valueOf(new IntCoder(EnonConfig.defaults()).getElement(createStringValue(t)).getValue()));
		}
		for (String t : intStrings) {
			assertEquals(t, String.valueOf(new IntCoder(configX).getElement(createStringValue(t)).getValue()));
		}

		Character[] digits = new Character[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		for (Character d : digits) {
			assertEquals(Byte.parseByte(d.toString()), new IntCoder(configX).getElement(createCharValue(d)).getValue());
		}

		Number[] ints = new Number[]{0, -5, -543, 75201, new BigInteger("59516516987")};
		for (Number i : ints) {
			assertEquals(i.toString(), String.valueOf(new IntCoder(EnonConfig.defaults()).getElement(createNumberValue(i)).getValue()));
		}
		for (Number i : ints) {
			assertEquals(i.toString(), String.valueOf(new IntCoder(configX).getElement(createNumberValue(i)).getValue()));
		}
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testGetElementStringFail() {
		LOGGER.info("getElementStringFail");
		Exception x = assertThrows(EnonCoderException.class, () -> {
			new IntCoder(EnonConfig.defaults()).getElement(createStringValue("nothing"));
		});
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testGetElementCharFail() {
		LOGGER.info("getElementCharFail");
		Exception x = assertThrows(EnonCoderException.class, () -> {
			new IntCoder(EnonConfig.defaults()).getElement(createCharValue('a'));
		});
	}

	@Test(/*expected = EnonCoderException.class*/)
	public void testGetElementFail() {
		LOGGER.info("getElementFail");
		Exception x = assertThrows(EnonCoderException.class, () -> {
			new IntCoder(EnonConfig.defaults()).getElement(createPojoValue(new Object()));
		});
	}

	private StringValue createStringValue(String value) {
		return new StringValue.Acceptor(EnonConfig.defaults()).toNativeValue(String.class, value);
	}

	private StringValue createCharValue(Character value) {
		return new StringValue.Acceptor(EnonConfig.defaults()).toNativeValue(Character.class, value);
	}

	private ScalarValue<Number> createNumberValue(Number value) {
		return new ScalarValue.Acceptor(EnonConfig.defaults()).toNativeValue(value.getClass(), value);
	}

	private PojoValue createPojoValue(Object value) {
		return new PojoValue.Acceptor(EnonConfig.defaults()).toNativeValue(value.getClass(), value);
	}

	private PojoValue createPrimitiveValue(byte value) {
		return new PojoValue.Acceptor(EnonConfig.defaults()).toNativeValue(byte.class, value);
	}
}
