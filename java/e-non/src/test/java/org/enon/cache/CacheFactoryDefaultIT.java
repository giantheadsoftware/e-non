/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.cache;

import org.enon.exception.EnonException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class CacheFactoryDefaultIT {

	private static final Logger LOGGER = LoggerFactory.getLogger(CacheFactoryDefaultIT.class.getName());

	public CacheFactoryDefaultIT() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testCreateCacheAndClose() {
		long time0 = System.currentTimeMillis();
		EnonCache cache = EnonCacheFactory.getInstance().create("test", String.class, Object.class);
		long time1 = System.currentTimeMillis() - time0;
		assertEquals(SimpleHashMapCache.class, cache.getClass());
		LOGGER.info("cache created in {}ms", time1);
		cache.close();
		assertTrue(cache.isClosed());
	}

	@Test(/*expected = EnonException.class*/)
	public void testCreateCacheProviderFail() {
		Exception x = assertThrows(EnonException.class, () -> {
			EnonCacheFactory.getInstance().create("test-fail", String.class, Object.class);
		});
	}

	@Test
	public void speedCreateTest() {
		final int count = 10;
		long time0 = System.currentTimeMillis();
		for (int i = 0; i < count; i++) {
			EnonCacheFactory.getInstance().create("test" + i, String.class, Object.class);
		}
		long time1 = System.currentTimeMillis() - time0;
		LOGGER.info("{} HashMap caches created in {}ms", count, time1);
	}
}
