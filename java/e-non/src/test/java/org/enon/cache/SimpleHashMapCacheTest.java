/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.cache;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class SimpleHashMapCacheTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleHashMapCacheTest.class.getName());

	private static final EnonCacheFactory CACHE_FACTORY = EnonCacheFactory.getInstance();
	private static SimpleHashMapCache<String, Object> stringObjectCache;

	public SimpleHashMapCacheTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
		CACHE_FACTORY.close();
	}

	@BeforeEach
	public void setUp() {
		stringObjectCache = (SimpleHashMapCache<String, Object>) CACHE_FACTORY.create("s.o.cache", String.class, Object.class);
		stringObjectCache.put("test", "value");
	}

	@AfterEach
	public void tearDown() {
		CACHE_FACTORY.destroyCache("s.o.cache");
	}

	/**
	 * Test of get method, of class SimpleHashMapCache.
	 */
	@Test
	public void testGet() {
		LOGGER.info("get");
		Object k = "test";
		SimpleHashMapCache instance = stringObjectCache;
		Object expResult = "value";
		Object result = instance.get(k);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getAll method, of class SimpleHashMapCache.
	 */
	@Test
	public void testGetAll() {
		LOGGER.info("getAll");
		stringObjectCache.put("get", 15);
		Set<String> set = new HashSet<>(Arrays.asList(new String[]{"get", "all"}));
		SimpleHashMapCache instance = stringObjectCache;
		Map<String, Object> result = instance.getAll(set);
		assertNull(result.get("all"));
		for (Map.Entry<String, Object> entry : result.entrySet()) {
			assertTrue(instance.containsKey(entry.getKey()));
			assertTrue(instance.contains(entry.getKey(), entry.getValue()));
		}
	}

	/**
	 * Test of containsKey method, of class SimpleHashMapCache.
	 */
	@Test
	public void testContainsKey() {
		LOGGER.info("containsKey");
		String k = "test";
		SimpleHashMapCache instance = stringObjectCache;
		assertTrue(instance.containsKey(k));

		k = "missing";
		assertFalse(instance.containsKey(k));
	}

	/**
	 * Test of put method, of class SimpleHashMapCache.
	 */
	@Test
	public void testPut() {
		LOGGER.info("put");
		String k = "put";
		Object v = "result";
		SimpleHashMapCache instance = stringObjectCache;
		assertFalse(instance.contains(k, v));
		instance.put(k, v);

		assertTrue(instance.contains(k, v));
	}

	@Test(/*expected = NullPointerException.class*/)
	public void testPutNullKeyFail() {
		LOGGER.info("putNullKeyFail");
		Exception x = assertThrows(NullPointerException.class, () -> {
			String k = null;
			Object v = "result";
			SimpleHashMapCache instance = stringObjectCache;
			instance.put(k, v);
		});
	}

	@Test(/*expected = NullPointerException.class*/)
	public void testPutNullValueFail() {
		LOGGER.info("putNullValueFail");
		Exception x = assertThrows(NullPointerException.class, () -> {
			String k = "put";
			Object v = null;
			SimpleHashMapCache instance = stringObjectCache;
			instance.put(k, v);
		});
	}

	/**
	 * Test of getAndPut method, of class SimpleHashMapCache.
	 */
	@Test
	public void testGetAndPut() {
		LOGGER.info("getAndPut");
		String k = "test";
		Object v = "new value";
		SimpleHashMapCache instance = stringObjectCache;
		Object expResult = "value";
		Object result = instance.getAndPut(k, v);
		assertEquals(expResult, result);
		assertEquals(v, instance.get(k));
	}

	@Test(/*expected = NullPointerException.class*/)
	public void testGetAndPutNullKeyFail() {
		LOGGER.info("getAndPututNullKeyFail");
		Exception x = assertThrows(NullPointerException.class, () -> {
			String k = null;
			Object v = "result";
			SimpleHashMapCache instance = stringObjectCache;
			instance.getAndPut(k, v);
		});
	}

	@Test(/*expected = NullPointerException.class*/)
	public void testGetAndPutNullValueFail() {
		LOGGER.info("getAndPutNullValueFail");
		Exception x = assertThrows(NullPointerException.class, () -> {
			String k = "put";
			Object v = null;
			SimpleHashMapCache instance = stringObjectCache;
			instance.getAndPut(k, v);
		});
	}

	/**
	 * Test of putAll method, of class SimpleHashMapCache.
	 */
	@Test
	public void testPutAll() {
		LOGGER.info("putAll");
		Map<String, Object> map = new HashMap<>();
		map.put("put", "put value");
		map.put("all", 13);
		SimpleHashMapCache instance = stringObjectCache;
		for (String key : map.keySet()) {
			assertFalse(instance.containsKey(key));
		}

		instance.putAll(map);

		for (Map.Entry<String, Object> entry : map.entrySet()) {
			assertTrue(instance.contains(entry.getKey(), entry.getValue()));
		}
	}

	/**
	 * Test of putAll method, of class SimpleHashMapCache.
	 */
	@Test(/*expected = NullPointerException.class*/)
	public void testPutAllNullKeyFail() {
		LOGGER.info("putAll");
		Exception x = assertThrows(NullPointerException.class, () -> {
			Map<String, Object> map = new HashMap<>();
			map.put("put", "put value");
			map.put("all", 13);
			map.put(null, "null");

			stringObjectCache.putAll(map);
		});
	}

	/**
	 * Test of putAll method, of class SimpleHashMapCache.
	 */
	@Test(/*expected = NullPointerException.class*/)
	public void testPutAllNullValueFail() {
		LOGGER.info("putAll");
		Exception x = assertThrows(NullPointerException.class, () -> {
			Map<String, Object> map = new HashMap<>();
			map.put("put", "put value");
			map.put("all", 13);
			map.put("null", null);

			stringObjectCache.putAll(map);
		});
	}

	/**
	 * Test of putIfAbsent method, of class SimpleHashMapCache.
	 */
	@Test
	public void testPutIfAbsent() {
		LOGGER.info("putIfAbsent");
		String k = "test";
		Object v = "value";
		SimpleHashMapCache instance = stringObjectCache;
		assertTrue(instance.contains(k, v));
		assertFalse(instance.putIfAbsent(k, v));

		k = "absent";
		assertFalse(instance.contains(k, v));
		assertTrue(instance.putIfAbsent(k, v));
		assertTrue(instance.contains(k, v));
	}

	/**
	 * Test of remove method, of class SimpleHashMapCache.
	 */
	@Test
	public void testRemoveKey() {
		LOGGER.info("removeKey");
		Object k = "test";
		SimpleHashMapCache instance = stringObjectCache;
		assertTrue(instance.containsKey(k));
		assertTrue(instance.remove(k));
		assertFalse(instance.containsKey(k));
		assertFalse(instance.remove(k));
	}

	/**
	 * Test of remove method, of class SimpleHashMapCache.
	 */
	@Test
	public void testRemoveKeyValue() {
		LOGGER.info("remove");
		String k = "test";
		Object v = "non-value";
		SimpleHashMapCache instance = stringObjectCache;

		assertFalse(instance.contains(k, v));
		assertFalse(instance.remove(k, v));

		v = "value";

		assertTrue(instance.contains(k, v));
		assertTrue(instance.remove(k, v));
		assertFalse(instance.contains(k, v));
		assertFalse(instance.remove(k, v));
	}

	/**
	 * Test of contains method, of class SimpleHashMapCache.
	 */
	@Test
	public void testContains() {
		LOGGER.info("contains");
		SimpleHashMapCache instance = stringObjectCache;

		Object k = "absent";
		Object v = "missing";
		assertFalse(instance.contains(k, v));

		k = "test";
		assertFalse(instance.contains(k, v));

		k = "absent";
		v = "value";
		assertFalse(instance.contains(k, v));

		k = "test";
		v = "value";
		assertTrue(instance.contains(k, v));
	}

	/**
	 * Test of getAndRemove method, of class SimpleHashMapCache.
	 */
	@Test
	public void testGetAndRemove() {
		LOGGER.info("getAndRemove");
		String k = "test";
		SimpleHashMapCache instance = stringObjectCache;
		assertTrue(instance.containsKey(k));
		assertEquals("value", instance.getAndRemove(k));
		assertFalse(instance.containsKey(k));
	}

	/**
	 * Test of replace method, of class SimpleHashMapCache.
	 */
	@Test
	public void testReplaceKeyValueNew() {
		LOGGER.info("replace");
		SimpleHashMapCache instance = stringObjectCache;
		String r = "replaced";
		assertFalse(instance.contains("test", r));
		assertFalse(instance.replace("absent", "missing", r));
		assertFalse(instance.replace("test", "missing", r));
		assertFalse(instance.replace("absent", "value", r));
		assertFalse(instance.contains("test", r));
		assertTrue(instance.replace("test", "value", r));
		assertTrue(instance.contains("test", r));
		assertFalse(instance.contains("test", "value"));
	}

	/**
	 * Test of replace method, of class SimpleHashMapCache.
	 */
	@Test
	public void testReplaceKeyValue() {
		LOGGER.info("replace");
		Object r = "replaced";
		SimpleHashMapCache instance = stringObjectCache;
		assertFalse(instance.contains("test", r));
		assertFalse(instance.replace("absent", r));
		assertFalse(instance.contains("test", r));

		assertTrue(instance.replace("test", r));
		assertTrue(instance.contains("test", r));
		assertFalse(instance.contains("test", "value"));
	}

	/**
	 * Test of getAndReplace method, of class SimpleHashMapCache.
	 */
	@Test
	public void testGetAndReplace() {
		LOGGER.info("getAndReplace");
		Object r = "replaced";
		SimpleHashMapCache instance = stringObjectCache;
		assertFalse(instance.contains("test", r));
		assertNull(instance.getAndReplace("absent", r));
		assertFalse(instance.contains("test", r));

		assertEquals("value", instance.getAndReplace("test", r));
		assertTrue(instance.contains("test", r));
		assertFalse(instance.contains("test", "value"));
	}

	/**
	 * Test of removeAll method, of class SimpleHashMapCache.
	 */
	@Test
	public void testRemoveAll_Set() {
		LOGGER.info("removeAll");
		Set<String> set = new HashSet<>(Arrays.asList(new String[]{"test", "remove"}));
		SimpleHashMapCache instance = stringObjectCache;
		boolean containsSome = false;
		for (String k : set) {
			if (instance.containsKey(k)) {
				containsSome = true;
			}
		}
		assertTrue(containsSome);

		instance.removeAll(set);

		for (String k : set) {
			assertFalse(instance.containsKey(k));
		}
	}

	/**
	 * Test of removeAll method, of class SimpleHashMapCache.
	 */
	@Test
	public void testRemoveAll() {
		LOGGER.info("removeAll");
		SimpleHashMapCache instance = stringObjectCache;
		assertTrue(instance.containsKey("test"));
		instance.removeAll();
		assertFalse(instance.containsKey("test"));
	}

	/**
	 * Test of clear method, of class SimpleHashMapCache.
	 */
	@Test
	public void testClear() {
		LOGGER.info("clear");
		SimpleHashMapCache instance = stringObjectCache;
		assertTrue(instance.containsKey("test"));
		instance.clear();
		assertFalse(instance.containsKey("test"));
	}

	/**
	 * Test of getName method, of class SimpleHashMapCache.
	 */
	@Test
	public void testGetName() {
		LOGGER.info("getName");
		SimpleHashMapCache instance = stringObjectCache;
		assertEquals("s.o.cache", instance.getName());
	}

	/**
	 * Test of getCacheManager method, of class SimpleHashMapCache.
	 */
	@Test
	public void testGetCacheManager() {
		LOGGER.info("getCacheManager");
		SimpleHashMapCache instance = stringObjectCache;
		assertNull(instance.getCacheManager());
	}

	/**
	 * Test of close method, of class SimpleHashMapCache.
	 */
	@Test
	public void testClose() {
		LOGGER.info("close");
		SimpleHashMapCache instance = stringObjectCache;
		assertFalse(instance.isClosed());
		instance.close();
		assertTrue(instance.isClosed());
	}

	/**
	 * Test of unwrap method, of class SimpleHashMapCache.
	 */
	@Test(/*expected = IllegalArgumentException.class*/)
	public void testUnwrap() {
		LOGGER.info("unwrap");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			stringObjectCache.unwrap(Object.class);
		});
	}

	/**
	 * Test of iterator method, of class SimpleHashMapCache.
	 *
	 * @param <K>
	 * @param <V>
	 */
	@Test
	public <K extends String, V extends Object> void testIterator() {
		LOGGER.info("iterator");
		SimpleHashMapCache instance = stringObjectCache;
		Iterator<Map.Entry<K, V>> result = instance.iterator();
		result.forEachRemaining(entry -> assertTrue(instance.contains(entry.getKey(), entry.getValue())));
	}

}
