/*
 */
package org.enon.cache;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class EmptyCacheTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmptyCacheTest.class.getName());

	private EmptyCache<String, Object> emptyCache;

	public EmptyCacheTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		emptyCache = new EmptyCache<>("empty");
		emptyCache.put("test", "value");
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of get method, of class EmptyCache.
	 */
	@Test
	public void testGet() {
		LOGGER.info("get");
		assertNull(emptyCache.get("test"));
	}

	/**
	 * Test of getAll method, of class EmptyCache.
	 */
	@Test
	public void testGetAll() {
		LOGGER.info("getAll");
		assertTrue(emptyCache.getAll(null).isEmpty());
	}

	/**
	 * Test of containsKey method, of class EmptyCache.
	 */
	@Test
	public void testContainsKey() {
		LOGGER.info("containsKey");
		assertFalse(emptyCache.containsKey("test"));
	}

	/**
	 * Test of put method, of class EmptyCache.
	 */
	@Test
	public void testPut() {
		LOGGER.info("put");
		emptyCache.put("test", "value");
		assertFalse(emptyCache.containsKey("test"));
	}

	/**
	 * Test of getAndPut method, of class EmptyCache.
	 */
	@Test
	public void testGetAndPut() {
		LOGGER.info("getAndPut");
		assertNull(emptyCache.getAndPut("test", "value"));
		assertFalse(emptyCache.containsKey("test"));
	}

	/**
	 * Test of putAll method, of class EmptyCache.
	 */
	@Test
	public void testPutAll() {
		LOGGER.info("putAll");
		Map<String, Object> map = new HashMap<>();
		map.put("one", "two");
		emptyCache.putAll(map);
		for (String k : map.keySet()) {
			assertFalse(emptyCache.containsKey(k));
		}
	}

	/**
	 * Test of putIfAbsent method, of class EmptyCache.
	 */
	@Test
	public void testPutIfAbsent() {
		LOGGER.info("putIfAbsent");
		String k = "test";
		Object v = "value";
		assertFalse(emptyCache.containsKey(k));
		assertTrue(emptyCache.putIfAbsent(k, v));

		k = "absent";
		assertFalse(emptyCache.containsKey(k));
		assertTrue(emptyCache.putIfAbsent(k, v));
		assertFalse(emptyCache.containsKey(k));
	}

	/**
	 * Test of remove method, of class EmptyCache.
	 */
	@Test
	public void testRemoveKey() {
		LOGGER.info("remove");
		assertFalse(emptyCache.remove("test"));
	}

	/**
	 * Test of remove method, of class EmptyCache.
	 */
	@Test
	public void testRemoveKeyValue() {
		LOGGER.info("remove");
		assertFalse(emptyCache.remove("test", "value"));
	}

	/**
	 * Test of getAndRemove method, of class EmptyCache.
	 */
	@Test
	public void testGetAndRemove() {
		LOGGER.info("getAndRemove");
		assertNull(emptyCache.getAndRemove("test"));
	}

	/**
	 * Test of replace method, of class EmptyCache.
	 */
	@Test
	public void testReplaceKeyValueNew() {
		LOGGER.info("replaceKeyValueNew");
		assertFalse(emptyCache.replace("test", "value", "replaced"));
	}

	/**
	 * Test of replace method, of class EmptyCache.
	 */
	@Test
	public void testReplaceKeyValue() {
		LOGGER.info("replaceKeyValue");
		assertFalse(emptyCache.replace("test", "value"));
	}

	/**
	 * Test of getAndReplace method, of class EmptyCache.
	 */
	@Test
	public void testGetAndReplace() {
		LOGGER.info("getAndReplace");
		assertNull(emptyCache.getAndReplace("test", "replaced"));
	}

	/**
	 * Test of removeAll method, of class EmptyCache.
	 */
	@Test
	public void testRemoveAll_Set() {
		LOGGER.info("removeAll");
		emptyCache.removeAll(null);
	}

	/**
	 * Test of removeAll method, of class EmptyCache.
	 */
	@Test
	public void testRemoveAll() {
		LOGGER.info("removeAll");
		emptyCache.removeAll();
	}

	/**
	 * Test of clear method, of class EmptyCache.
	 */
	@Test
	public void testClear() {
		LOGGER.info("clear");
		emptyCache.clear();
	}

	/**
	 * Test of getName method, of class EmptyCache.
	 */
	@Test
	public void testGetName() {
		LOGGER.info("getName");
		assertEquals("empty", emptyCache.getName());
	}

	/**
	 * Test of getCacheManager method, of class EmptyCache.
	 */
	@Test
	public void testGetCacheManager() {
		LOGGER.info("getCacheManager");
		assertNull(emptyCache.getCacheManager());
	}

	/**
	 * Test of isClosed method, of class EmptyCache.
	 */
	@Test
	public void testClose() {
		LOGGER.info("close");
		assertFalse(emptyCache.isClosed());
		emptyCache.close();
		assertTrue(emptyCache.isClosed());
	}

	/**
	 * Test of unwrap method, of class EmptyCache.
	 */
	@Test(/*expected = IllegalArgumentException.class*/)
	public void testUnwrap() {
		LOGGER.info("unwrap");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			assertNull(emptyCache.unwrap(Object.class));
		});
	}

	/**
	 * Test of iterator method, of class EmptyCache.
	 */
	@Test
	public void testIterator() {
		LOGGER.info("iterator");
		assertFalse(emptyCache.iterator().hasNext());
	}

}
