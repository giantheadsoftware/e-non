/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.io.OutputStream;
import java.util.EnumSet;
import org.enon.cache.EnonCache;
import org.enon.test.util.ExceptionOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class WriterContextTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(WriterContextTest.class.getName());


	public WriterContextTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getInputStream method, of class WriterContext.
	 */
	@Test
	public void testGetOutputStream() {
		LOGGER.info("getOutputStream");

		OutputStream out = new ExceptionOutputStream();
		WriterContext ctx = new WriterContext(out);
		assertSame(out, ctx.getOutputStream());
	}

	@Test
	public void testGetWriteGlossary() {
		LOGGER.info("getOutputStream");

		WriterContext ctx = new WriterContext(new ExceptionOutputStream());
		assertNull(ctx.getWriteGlossary());

		ctx = new WriterContext(new ExceptionOutputStream(), new EnonConfig.Builder(EnumSet.of(FeatureSet.G)).build());
		EnonCache glossary = ctx.getWriteGlossary();
		assertNotNull(glossary);
		// make sure we always get the same one
		assertSame(glossary, ctx.getWriteGlossary());

		glossary.close();
	}
}
