/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import org.enon.exception.EnonException;
import org.enon.exception.EnonReadException;
import org.enon.test.util.ExceptionInputStream;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ReaderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReaderTest.class.getName());

	public ReaderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getConfig method, of class Reader.
	 */
	@Test
	public void testGetConfig() {
		LOGGER.info("getConfig");
		TestDataOutputStream tdos = new TestDataOutputStream();
		Reader instance = new TestReaderImpl(tdos.dataInputStream());
		EnonConfig result = instance.getConfig();

		// should create default config
		assertNotNull(result);

		EnonConfig config = new EnonConfig.Builder().build();
		instance = new TestReaderImpl(tdos.dataInputStream(), config);
		result = instance.getConfig();

		assertSame(config, result);
	}

	@Test
	public void testReadAsClass() {
		LOGGER.info("readAsClass");

		TestDataOutputStream tdos = new TestDataOutputStream();
		Writer writer = new Writer(tdos.stream());

		PublicPojo pojo = new PublicPojo("Tracy", 9876541230L, Arrays.asList(new Integer[]{3, 6, 9}), Integer.MAX_VALUE + 1L);
		writer.write(pojo);
		Reader reader = new Reader(tdos.dataInputStream());

		PublicPojo result = reader.read(PublicPojo.class);

		assertNotNull(result);
		assertEquals(pojo.getName(), result.getName());
		assertEquals(pojo.getId(), result.getId());
		assertEquals(pojo.getSizes(), result.getSizes());
		assertEquals(pojo.getAny(), result.getAny());

		tdos.close();
	}

	/**
	 * Test of close method, of class Reader.
	 */
	@Test
	public void testCloseOK() {
		LOGGER.info("closeOK");

		TestDataOutputStream tdos = new TestDataOutputStream();
		Reader instance = new TestReaderImpl(tdos.dataInputStream());
		instance.close();
		tdos.close();
	}

	/**
	 * Test of close method, of class Reader.
	 */
	@Test(/*expected = EnonException.class*/)
	public void testCloseNoStreamFail() {
		LOGGER.info("closeNoStreamFail");

		Exception x = assertThrows(EnonException.class, () -> {
			Reader instance = new TestReaderImpl(null);
			instance.close();
		});
	}

	/**
	 * Test of close method, of class Reader.
	 */
	@Test(/*expected = EnonException.class*/)
	public void testCloseErrStreamFail() {
		LOGGER.info("closeNoStreamFail");

		Exception x = assertThrows(EnonException.class, () -> {
			Reader instance = new TestReaderImpl(new ExceptionInputStream());
			instance.close();
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReadFail() {
		LOGGER.info("readFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			Reader instance = new TestReaderImpl(new ExceptionInputStream());
			instance.read(Object.class);
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReadIoErrorFail() {
		LOGGER.info("readIoErrorFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			new TestReaderImpl(new ExceptionInputStream()).read(Object.class);
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReadNullIoErrorFail() {
		LOGGER.info("readNullIoErrorFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			new TestReaderImpl(new ExceptionInputStream()).read(null);
		});
	}

	@Test
	public void testAlternateConstructor() {
		LOGGER.info("alternateConstructor");

		assertNotNull(new Reader(new ExceptionInputStream()).getConfig());
		assertNotNull(new Reader(new ExceptionInputStream(), EnonConfig.defaults()).getConfig());
		assertNotNull(new Reader(new ExceptionInputStream(), null).getConfig());
	}

//////////////////////////////////////////////////////////////////
	public class TestReaderImpl extends Reader {

		private Prolog prolog;

		public TestReaderImpl(InputStream in) {
			super(in);
		}

		public TestReaderImpl(InputStream in, EnonConfig config) {
			super(in, config);
		}

		public TestReaderImpl(InputStream in, Prolog prolog) {
			super(in);
			this.prolog = prolog;
		}

		@Override
		protected void validateProlog() {
			// do nothing
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static abstract class AbstractPojo {

		private String name;
		private long id;

		public AbstractPojo() {
		}

		public AbstractPojo(String name, long id) {
			this.name = name;
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static class PublicPojo extends AbstractPojo {

		private List<Integer> sizes;
		private Object any;

		public PublicPojo() {
		}

		public PublicPojo(String name, long id) {
			super(name, id);
		}

		public PublicPojo(String name, long id, List<Integer> sizes, Object any) {
			super(name, id);
			this.sizes = sizes;
			this.any = any;
		}

		public List<Integer> getSizes() {
			return sizes;
		}

		public void setSizes(List<Integer> sizes) {
			this.sizes = sizes;
		}

		public Object getAny() {
			return any;
		}

		public void setAny(Object any) {
			this.any = any;
		}
	}

}
