/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import org.enon.exception.EnonException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class IntTypeTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntTypeTest.class.getName());

	public IntTypeTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of valueOfType method, of class IntType.
	 */
	@Test
	public void testValueOfType() {
		LOGGER.info("valueOfType");
		for (IntType it : IntType.values()) {
			Class<? extends Number> type = it.type;
			assertSame(it, IntType.valueOfType(type));
		}
	}

	/**
	 * Test of isPrimitive method, of class IntType.
	 */
	@Test
	public void testIsPrimitive() {
		LOGGER.info("isPrimitive");
		for (IntType it : IntType.values()) {
			Class<? extends Number> type = it.type;
			assertEquals(type.isPrimitive(), it.isPrimitive());
		}
	}

	/**
	 * Test of inRange method, of class IntType.
	 */
	@Test
	public void testInRangeByte() {
		LOGGER.info("inRangeByte");
		IntType byteTypeO = IntType.BYTE_OBJECT;
		IntType byteType = IntType.BYTE;

		for (int okValue : new int[]{0, -1, 1, 103, -113, Byte.MIN_VALUE, Byte.MAX_VALUE}) {
			assertTrue(byteType.inRange(okValue), okValue + " should be OK");
			assertTrue(byteTypeO.inRange(okValue), okValue + " should be OK");
		}

		for (byte okValue : new byte[]{(byte) 0, (byte) -1, (byte) 1, (byte) 103, (byte) -113, Byte.MIN_VALUE, Byte.MAX_VALUE}) {
			assertTrue(byteType.inRange(okValue), okValue + " should be OK");
			assertTrue(byteTypeO.inRange(okValue), okValue + " should be OK");
		}

		for (int notOkValue : new int[]{Byte.MIN_VALUE - 2, Byte.MIN_VALUE - 1, Byte.MAX_VALUE + 1, 1000, -13872}) {
			assertFalse(byteType.inRange(notOkValue), notOkValue + " should not be OK");
			assertFalse(byteTypeO.inRange(notOkValue), notOkValue + " should not be OK");
		}
	}

	/**
	 * Test of inRange method, of class IntType.
	 */
	@Test
	public void testInRangeShort() {
		LOGGER.info("inRangeShort");
		IntType shortTypeO = IntType.SHORT_OBJECT;
		IntType shortType = IntType.SHORT;

		for (int okValue : new int[]{0, -1, 1, 103, -113, Byte.MIN_VALUE, Byte.MAX_VALUE}) {
			assertTrue(shortType.inRange(okValue), okValue + " should be OK");
			assertTrue(shortTypeO.inRange(okValue),okValue + " should be OK");
		}

		for (int okValue : new int[]{0, -1, 1, 2003, -2130, Short.MIN_VALUE, Short.MAX_VALUE}) {
			assertTrue(shortType.inRange(okValue), okValue + " should be OK");
			assertTrue(shortTypeO.inRange(okValue), okValue + " should be OK");
		}

		for (int notOkValue : new int[]{Short.MIN_VALUE - 2, Short.MIN_VALUE - 1, Short.MAX_VALUE + 1, 100000, -73872}) {
			assertFalse(shortType.inRange(notOkValue), notOkValue + " should not be OK");
			assertFalse(shortTypeO.inRange(notOkValue), notOkValue + " should not be OK");
		}
	}

	/**
	 * Test of inRange method, of class IntType.
	 */
	@Test
	public void testInRangeInt() {
		LOGGER.info("inRangeInt");
		IntType intTypeO = IntType.INT_OBJECT;
		IntType intType = IntType.INT;

		for (int okValue : new int[]{0, -1, 1, 103, -113, Byte.MIN_VALUE, Byte.MAX_VALUE}) {
			assertTrue(intType.inRange(okValue), okValue + " should be OK");
			assertTrue(intTypeO.inRange(okValue), okValue + " should be OK");
		}

		for (long okValue : new long[]{(long) 0, (long) -1, (long) 1, (long) 200306, (long) -213057, (long) Integer.MIN_VALUE, (long) Integer.MAX_VALUE}) {
			assertTrue(intType.inRange(okValue), okValue + " should be OK");
			assertTrue(intTypeO.inRange(okValue), okValue + " should be OK");
		}

		for (BigInteger okValue : new BigInteger[]{BigInteger.valueOf(Integer.MIN_VALUE), BigInteger.valueOf(Integer.MAX_VALUE)}) {
			assertTrue(intType.inRange(okValue), okValue + " should not be OK");
			assertTrue(intTypeO.inRange(okValue), okValue + " should not be OK");
		}

		for (long notOkValue : new long[]{(long) Integer.MIN_VALUE - 2, (long) Integer.MIN_VALUE - 1, (long) Integer.MAX_VALUE + 1, 7000000000L, -7387200089L}) {
			assertFalse(intType.inRange(notOkValue), notOkValue + " should not be OK");
			assertFalse(intTypeO.inRange(notOkValue), notOkValue + " should not be OK");
		}

		for (BigInteger notOkValue : new BigInteger[]{
			BigInteger.valueOf(Long.MIN_VALUE).subtract(BigInteger.TEN),
			BigInteger.valueOf(Long.MIN_VALUE).subtract(BigInteger.ONE),
			BigInteger.valueOf(Long.MAX_VALUE).add(BigInteger.ONE)
		}) {
			assertFalse(intType.inRange(notOkValue), notOkValue + " should not be OK");
			assertFalse(intTypeO.inRange(notOkValue), notOkValue + " should not be OK");
		}
	}

	/**
	 * Test of inRange method, of class IntType.
	 */
	@Test
	public void testInRangeLong() {
		LOGGER.info("inRangeInt");
		IntType longTypeO = IntType.LONG_OBJECT;
		IntType longType = IntType.LONG;

		for (int okValue : new byte[]{(byte) 0, (byte) -1, (byte) 1, (byte) 103, (byte) -113, Byte.MIN_VALUE, Byte.MAX_VALUE}) {
			assertTrue(longType.inRange(okValue), okValue + " should be OK");
			assertTrue(longTypeO.inRange(okValue), okValue + " should be OK");
		}

		for (long okValue : new long[]{0L, -1L, 1L, 7000000000L, -7387200089L, Long.MIN_VALUE, Long.MAX_VALUE}) {
			assertTrue(longType.inRange(okValue), okValue + " should be OK");
			assertTrue(longTypeO.inRange(okValue), okValue + " should be OK");
		}

		for (BigInteger notOkValue : new BigInteger[]{
			BigInteger.valueOf(Long.MIN_VALUE).subtract(BigInteger.TEN),
			BigInteger.valueOf(Long.MIN_VALUE).subtract(BigInteger.ONE),
			BigInteger.valueOf(Long.MAX_VALUE).add(BigInteger.ONE)
		}) {
			assertFalse(longType.inRange(notOkValue), notOkValue + " should not be OK");
			assertFalse(longTypeO.inRange(notOkValue), notOkValue + " should not be OK");
		}
	}

	/**
	 * Test of inRange method, of class IntType.
	 */
	@Test
	public void testInRangeBigInteger() {
		LOGGER.info("inRangeBigInt");
		IntType bigType = IntType.BIG_INTEGER;

		for (int okValue : new byte[]{(byte) 0, (byte) -1, (byte) 1, (byte) 103, (byte) -113, Byte.MIN_VALUE, Byte.MAX_VALUE}) {
			assertTrue(bigType.inRange(okValue), okValue + " should be OK");
		}

		for (long okValue : new long[]{0L, -1L, 1L, 7000000000L, -7387200089L, Long.MIN_VALUE, Long.MAX_VALUE}) {
			assertTrue(bigType.inRange(okValue), okValue + " should be OK");
		}

		for (BigInteger okValue : new BigInteger[]{
			BigInteger.valueOf(Long.MIN_VALUE).subtract(BigInteger.TEN),
			BigInteger.valueOf(Long.MIN_VALUE).subtract(BigInteger.ONE),
			BigInteger.valueOf(Long.MAX_VALUE).add(BigInteger.ONE)
		}) {
			assertTrue(bigType.inRange(okValue), okValue + " should be OK");
		}
	}

	/**
	 * Test of inRange method, of class IntType.
	 */
	@Test
	public void testInRangeDouble() {
		LOGGER.info("inRangeDouble");
		for (IntType it : IntType.values()) {
			assertFalse(it.inRange(2500.4));
		}
	}

	/**
	 * Test of compact method, of class IntType.
	 */
	@Test
	public void testCompact() {
		LOGGER.info("compact");

		Number[] byteValues = new Number[]{(byte) 15, (short) -18, 125, -123L, BigInteger.valueOf(9)};
		for (Number number : byteValues) {
			Number compact = IntType.compact(number);
			assertSame(Byte.class, compact.getClass());
			assertEquals(number.byteValue(), compact);
		}

		Number[] shortValues = new Number[]{(short) -180, 1250, -7238L, BigInteger.valueOf(198)};
		for (Number number : shortValues) {
			Number compact = IntType.compact(number);
			assertSame(Short.class, compact.getClass());
			assertEquals(number.shortValue(), compact);
		}

		Number[] intValues = new Number[]{-180801, 12532480, (long) Integer.MIN_VALUE, BigInteger.valueOf(Integer.MAX_VALUE)};
		for (Number number : intValues) {
			Number compact = IntType.compact(number);
			assertSame(Integer.class, compact.getClass());
			assertEquals(number.intValue(), compact);
		}

		Number[] longValues = new Number[]{-18080156451L, -87412532480L, Long.MIN_VALUE, BigInteger.valueOf(Long.MAX_VALUE)};
		for (Number number : longValues) {
			Number compact = IntType.compact(number);
			assertSame(Long.class, compact.getClass(), number + " should be long value");
			assertEquals(number.longValue(), compact);
		}

		Number[] bigValues = new Number[]{BigInteger.valueOf(Long.MAX_VALUE).add(BigInteger.ONE), BigInteger.valueOf(Long.MIN_VALUE).subtract(BigInteger.ONE)};
		for (Number number : bigValues) {
			Number compact = IntType.compact(number);
			assertSame(BigInteger.class, compact.getClass());
			assertEquals(number, compact);
		}

		// attempting to compact a non-integral value should return the same value as passed in
		Double d = 259.95;
		Number compact = IntType.compact(d);
		assertSame(d, compact);
	}

	/**
	 * Test of cast method, of class IntType.
	 */
	@Test
	public void testCast() {
		LOGGER.info("cast");
		Number cast = IntType.BYTE.cast(35L);
		assertSame(Byte.class, cast.getClass());
		assertEquals((byte) 35, cast);

		cast = IntType.BYTE.cast(35f);
		assertSame(Byte.class, cast.getClass());
		assertEquals((byte) 35, cast);

		cast = IntType.SHORT.cast(35L);
		assertSame(Short.class, cast.getClass());
		assertEquals((short) 35, cast);

		cast = IntType.INT.cast(35);
		assertSame(Integer.class, cast.getClass());
		assertEquals(35, cast);

		cast = IntType.LONG.cast(35);
		assertSame(Long.class, cast.getClass());
		assertEquals(35L, cast);

		cast = IntType.BIG_INTEGER.cast(35L);
		assertSame(BigInteger.class, cast.getClass());
		assertEquals(35L, cast.longValue());

		cast = IntType.BIG_INTEGER.cast(35.0);
		assertSame(BigInteger.class, cast.getClass());
		assertEquals(35L, cast.longValue());
	}

	/**
	 * Test of cast method, of class IntType.
	 */
	@Test(/*expected = IllegalArgumentException.class*/)
	public void testCastOutOfRangeFail() {
		LOGGER.info("cast");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			IntType.BYTE.cast(Byte.MAX_VALUE + 1);
		});
	}

	@Test
	public void testFromString() {
		LOGGER.info("fromString");

		// bytes
		for (String s : new String[]{"0", String.valueOf(Byte.MAX_VALUE), String.valueOf(Byte.MIN_VALUE), "7.0"}) {
			assertEquals(new BigDecimal(s).byteValueExact(), IntType.BYTE_OBJECT.fromString(s));
			assertEquals(new BigDecimal(s).byteValueExact(), IntType.BYTE.fromString(s));
		}

		// shorts
		for (String s : new String[]{"0", String.valueOf(Short.MAX_VALUE), String.valueOf(Short.MIN_VALUE), "518.0"}) {
			assertEquals(new BigDecimal(s).shortValueExact(), IntType.SHORT_OBJECT.fromString(s));
			assertEquals(new BigDecimal(s).shortValueExact(), IntType.SHORT.fromString(s));
		}

		// ints
		for (String s : new String[]{"0", String.valueOf(Integer.MAX_VALUE), String.valueOf(Integer.MIN_VALUE), "-123456.0"}) {
			assertEquals(new BigDecimal(s).intValueExact(), IntType.INT_OBJECT.fromString(s));
			assertEquals(new BigDecimal(s).intValueExact(), IntType.INT.fromString(s));
		}

		// longs
		for (String s : new String[]{"0", String.valueOf(Long.MAX_VALUE), String.valueOf(Long.MIN_VALUE), "216871268454.0"}) {
			assertEquals(new BigDecimal(s).longValueExact(), IntType.LONG_OBJECT.fromString(s));
			assertEquals(new BigDecimal(s).longValueExact(), IntType.LONG.fromString(s));
		}

		// big ints
		for (String s : new String[]{"0", String.valueOf(Long.MAX_VALUE), String.valueOf(Long.MIN_VALUE), "54987618849981.0"}) {
			assertEquals(new BigDecimal(s).toBigIntegerExact(), IntType.BIG_INTEGER.fromString(s));
		}

		// error because values are too large for the specified type
		for (Number n : new Number[]{Short.MAX_VALUE, Integer.MIN_VALUE, Long.MIN_VALUE, BigInteger.valueOf(Long.MAX_VALUE).multiply(BigInteger.TEN)}) {
			for (IntType it : IntType.values()) {
				if (!it.inRange(n)) {
					try {
						it.fromString(String.valueOf(n));
						fail("Number should be out of range for " + it.toString() + ": " + n);
					} catch (IllegalArgumentException x) {
						// success
					}
				}
			}
		}

		// error due to loss of precision
		for (Number n : new Number[]{3.75f, -897.514f, 2154985.215}) {
			for (IntType it : IntType.values()) {
				try {
					it.fromString(String.valueOf(n));
					fail("Number should fail on precision for " + it.toString() + ": " + n);
				} catch (EnonException x) {
					// success
				}
			}
		}
	}

	@Test()
	public void testCastFail() {
		LOGGER.info("castFail");

		// non-zero fraction
		for (IntType it : IntType.values()) {
			try {
				assertEquals(it.cast(100), it.cast(100.7));
				fail("should fail");
			} catch (EnonException x) {
				// success
			}
		}

		// NaN
		for (Number n : new Number[]{Double.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NaN}) {
			for (IntType it : IntType.values()) {
				try {
					it.cast(n);
					fail("Should fail.");
				} catch (EnonException x) {
					// success
				}
			}
		}
	}
}
