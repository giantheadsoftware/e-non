/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.util;

import java.io.IOException;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class SerializeUtilTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(SerializeUtilTest.class.getName());

	public SerializeUtilTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}
	/**
	 * Test of networkBytes method, of class SerializeUtil.
	 */
	@Test
	public void testNetworkBytes_byte() throws IOException {
		LOGGER.info("networkBytes");
		byte value = 9;

		// assert that Serialize util produces teh same result as DataOutputStream
		TestDataOutputStream tdos = new TestDataOutputStream();
		tdos.stream().writeByte(value);

		byte[] result = SerializeUtil.getInstance().networkBytes(value);

		assertArrayEquals(tdos.bytes(), result);
	}

	/**
	 * Test of networkBytes method, of class SerializeUtil.
	 */
	@Test
	public void testNetworkBytes_short() throws IOException {
		LOGGER.info("networkBytes");
		short value = -241;

		// assert that Serialize util produces teh same result as DataOutputStream
		TestDataOutputStream tdos = new TestDataOutputStream();
		tdos.stream().writeShort(value);

		byte[] result = SerializeUtil.getInstance().networkBytes(value);

		assertArrayEquals(tdos.bytes(), result);
	}

	/**
	 * Test of networkBytes method, of class SerializeUtil.
	 */
	@Test
	public void testNetworkBytes_int() throws IOException {
		LOGGER.info("networkBytes");
		int value = 1234567890;

		// assert that Serialize util produces teh same result as DataOutputStream
		TestDataOutputStream tdos = new TestDataOutputStream();
		tdos.stream().writeInt(value);

		byte[] result = SerializeUtil.getInstance().networkBytes(value);

		assertArrayEquals(tdos.bytes(), result);
	}

	/**
	 * Test of networkBytes method, of class SerializeUtil.
	 */
	@Test
	public void testNetworkBytes_long() throws IOException {
		LOGGER.info("networkBytes");
		long value = 987654321012345678L;

		// assert that Serialize util produces teh same result as DataOutputStream
		TestDataOutputStream tdos = new TestDataOutputStream();
		tdos.stream().writeLong(value);

		byte[] result = SerializeUtil.getInstance().networkBytes(value);

		assertArrayEquals(tdos.bytes(), result);
	}

	/**
	 * Test of networkBytes method, of class SerializeUtil.
	 */
	@Test
	public void testNetworkBytes_float() throws IOException {
		LOGGER.info("networkBytes");
		float value = 99543.1665484754651265748425F;

		// assert that Serialize util produces teh same result as DataOutputStream
		TestDataOutputStream tdos = new TestDataOutputStream();
		tdos.stream().writeFloat(value);

		byte[] result = SerializeUtil.getInstance().networkBytes(value);

		assertArrayEquals(tdos.bytes(), result);
	}

	/**
	 * Test of networkBytes method, of class SerializeUtil.
	 */
	@Test
	public void testNetworkBytes_double() throws IOException {
		LOGGER.info("networkBytes");
		double value = 13469784613285.21687459951255598431559862132129874122;

		// assert that Serialize util produces teh same result as DataOutputStream
		TestDataOutputStream tdos = new TestDataOutputStream();
		tdos.stream().writeDouble(value);

		byte[] result = SerializeUtil.getInstance().networkBytes(value);

		assertArrayEquals(tdos.bytes(), result);
	}

}
