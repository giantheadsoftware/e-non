/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.enon.exception.EnonException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ReflectTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReflectTest.class.getName());

	public ReflectTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testFindArgsOfSuperCollection() throws NoSuchMethodException {
		LOGGER.info("findArgsOfSuperCollection");

		ReflectUtil instance = ReflectUtil.getinstance();

		Type fromType = CollectionPojo.class.getMethod("setList", List.class).getGenericParameterTypes()[0];
		Type[] args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertSame(null, args[0]);

		fromType = CollectionPojo.class.getMethod("setListString", List.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertSame(String.class, args[0]);

		fromType = CollectionPojo.class.getMethod("setSetInt", HashSet.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertSame(Integer.class, args[0]);

		fromType = CollectionPojo.class.getMethod("setSetListInt", HashSet.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertEquals("java.util.List<java.lang.Integer>", args[0].getTypeName());

		fromType = CollectionPojo.class.getMethod("setNoArgList", NoArgList.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertSame(Integer.class, args[0]);

		fromType = CollectionPojo.class.getMethod("setFakeArgList", FakeArgList.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertSame(Integer.class, args[0]);

		fromType = CollectionPojo.class.getMethod("setWildcardListLong", WildCardList.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertSame(Long.class, args[0]);

		fromType = CollectionPojo.class.getMethod("setWildcardList", WildCardList.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertSame(null, args[0]);

		fromType = CollectionPojo.class.getMethod("setSubListClassTypeVar", SubList.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertEquals(Comparable.class.getTypeName() + "<T>", args[0].getTypeName());

		fromType = CollectionPojo.class.getMethod("setSubListMethodTypeVar", SubList.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertEquals(Number.class, args[0]);

		fromType = CollectionPojo.class.getMethod("setFunkyList", FunkyList2.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Collection.class, fromType);
		assertEquals(1, args.length);
		assertSame(Integer.class, args[0]);
	}

	@Test
	public void testFindArgsOfSuperMap() throws NoSuchMethodException {
		LOGGER.info("findArgsOfSuperMap");

		ReflectUtil instance = ReflectUtil.getinstance();

		Type fromType = MapPojo.class.getMethod("setNoArgMap", NoArgMap.class).getGenericParameterTypes()[0];
		Type[] args = instance.findArgsOfSuper(Map.class, fromType);
		assertEquals(2, args.length);
		assertSame(String.class, args[0]);
		assertSame(Long.class, args[1]);

		fromType = MapPojo.class.getMethod("setMap", Map.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Map.class, fromType);
		assertEquals(2, args.length);
		assertSame(null, args[0]);
		assertSame(null, args[1]);

		fromType = MapPojo.class.getMethod("setMapStringLong", Map.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Map.class, fromType);
		assertEquals(2, args.length);
		assertSame(String.class, args[0]);
		assertSame(Long.class, args[1]);

		fromType = MapPojo.class.getMethod("setMapStringInt", HashMap.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Map.class, fromType);
		assertEquals(2, args.length);
		assertSame(String.class, args[0]);
		assertSame(Integer.class, args[1]);

		fromType = MapPojo.class.getMethod("setMapStringListInt", HashMap.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Map.class, fromType);
		assertEquals(2, args.length);
		assertSame(String.class, args[0]);
		assertEquals("java.util.List<java.lang.Integer>", args[1].getTypeName());

		fromType = MapPojo.class.getMethod("setFunkyMap", FunkyMap2.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Map.class, fromType);
		assertEquals(2, args.length);
		assertSame(Long.class, args[0]);
		assertSame(String.class, args[1]);

		fromType = MapPojo.class.getMethod("setSubMap", SubMap.class).getGenericParameterTypes()[0];
		args = instance.findArgsOfSuper(Map.class, fromType);
		assertEquals(2, args.length);
		assertSame(String.class, args[0]);
		assertSame(Integer.class, args[1]);
	}

	@Test
	public void testFindArgsOfSuperNoArgType() {
		LOGGER.info("findArgsOfSuperNoArgType");
		assertEquals(0, ReflectUtil.getinstance().findArgsOfSuper(String.class, null).length);
	}

	@Test(/*expected = EnonException.class*/)
	public void testfindArgsOfSuperTooManyBoundsFail() throws NoSuchMethodException {
		LOGGER.info("findArgsOfSuperTooManyBoundsFail");
		Exception x = assertThrows(EnonException.class, () -> {
			Type fromType = CollectionPojo.class.getMethod("setSubList2", SubList.class).getGenericParameterTypes()[0];
			ReflectUtil.getinstance().findArgsOfSuper(Collection.class, fromType);
		});
	}

	@Test(/*expected = EnonException.class*/)
	public void testfindArgsOfSuperTooManyWildcardBoundsFail() throws NoSuchMethodException {
		LOGGER.info("findArgsOfSuperTooManyWildcardBoundsFail");
		Exception x = assertThrows(EnonException.class, () -> {
			Type fromType = CollectionPojo.class.getMethod("setSubList3", SubList.class).getGenericParameterTypes()[0];
			Type[] result = ReflectUtil.getinstance().findArgsOfSuper(Collection.class, fromType);
			ReflectUtil.getinstance().classOfType(result[0]);
		});
	}

	@Test
	public void testClassOfTypeTypeVar() throws NoSuchMethodException {
		LOGGER.info("classOfTypeTypeVar");
		ReflectUtil reflect = ReflectUtil.getinstance();

		Type type = TypeVarPojo.class.getMethod("setNumber", Number.class).getGenericParameterTypes()[0];
		assertSame(Number.class, reflect.classOfType(type));

		type = TypeVarPojo.class.getMethod("setInteger", Integer.class).getGenericParameterTypes()[0];
		assertSame(Integer.class, reflect.classOfType(type));

		type = TypeVarPojo.class.getMethod("setInteger", Number.class).getGenericParameterTypes()[0];
		assertSame(Number.class, reflect.classOfType(type));
	}

	@Test
	public void testSuperTowardTypeSameType() {
		LOGGER.info("testSuperTowardTypeSameType");
		assertSame(Collection.class, ReflectUtil.getinstance().superTowardType(Collection.class, Collection.class));
	}

	//////////////////////////////////////////////////////////
	private static class CollectionPojo<T extends Comparable<T>> {

		public void setList(List stringList) {
		}

		public void setListString(List<String> stringList) {
		}

		public void setSetInt(HashSet<Integer> intSet) {
		}

		public void setSetListInt(HashSet<List<Integer>> intSet) {
		}

		public void setNoArgList(NoArgList list) {
		}

		public void setFakeArgList(FakeArgList<Number> list) {
		}

		public void setWildcardListLong(WildCardList<? extends Long> list) {
		}

		public void setWildcardList(WildCardList<?> list) {
		}

		// class-declared type var
		public void setSubListClassTypeVar(SubList<T> list) {
		}

		// method-declared type var
		public <N extends Number> void setSubListMethodTypeVar(SubList<N> list) {
		}

		// fail -- 2 bounds not supported
		public <N extends Map & Comparable> void setSubList2(SubList<N> list) {
		}

		// fail -- 2 bounds not supported
		public <N extends Map & Comparable> void setSubList3(SubList<? extends N> list) {
		}

		public void setFunkyList(FunkyList2<Integer, Long> funkyList) {
		}
	}

	//////////////////////////////////////////////////////////////////
	private static class SubList<SL> extends ArrayList<SL> {

	}

	///////////////////////////////////////////////////////////////////
	private static class NoArgList extends ArrayList<Integer> {

		public NoArgList() {
		}

	}

	///////////////////////////////////////////////////////////////////
	private static class FakeArgList<N> extends NoArgList {

		public FakeArgList() {
		}

	}

	/////////////////////////////////////////////////////////////////////
	private static class WildCardList<N extends Number> extends ArrayList<N> {

		public WildCardList() {
		}

	}

	/////////////////////////////////////////////////////////////////////
	private static class NestedList<L extends List<L>> extends ArrayList<L> {

		public NestedList() {
		}

	}

	///////////////////////////////////////////////////////////////////////////
	private static class FunkyList<A, B> extends SubList<B> {

		public FunkyList() {
		}

	}

	////////////////////////////////////////////////////////////////////////////
	private static class FunkyList2<C, D> extends FunkyList<D, C> {

		public FunkyList2() {
		}

	}

//////////////////////////////////////////////////////////////////
	private static class MapPojo {

		public void setMap(Map stringMap) {
		}

		public void setMapStringLong(Map<String, Long> stringLongMap) {
		}

		public void setMapStringInt(HashMap<String, Integer> map) {
		}

		public void setMapStringListInt(HashMap<String, List<Integer>> map) {
		}

		public void setNoArgMap(NoArgMap map) {
		}

		public void setFunkyMap(FunkyMap2<String, Integer, Long> funkyMap) {
		}

		public void setSubMap(SubMap<? extends String, ? extends Integer> funkyMap) {
		}
	}

	/////////////////////////////////////////////////////////////////
	private static class NoArgMap extends HashMap<String, Long> {

		public NoArgMap() {
		}

	}

	///////////////////////////////////////////////////////////////////
	private static class SubMap<KEY, VAL> extends HashMap<KEY, VAL> {

		public SubMap() {
		}

	}

	///////////////////////////////////////////////////////////////////
	private static class FunkyMap<X, Y, Z> extends SubMap<Y, Z> {

		public FunkyMap() {
		}

	}

	//////////////////////////////////////////////////////////////////////
	private static class FunkyMap2<X1, Y1, Z1> extends FunkyMap<Y1, Z1, X1> {

		public FunkyMap2() {
		}

	}

	////////////////////////////////////////////////////////////////////////
	private static class TypeVarPojo<N extends Number> {

		public void setNumber(N n) {
		}

		public <I extends Integer> void setInteger(I i) {
		}

		public <I extends N> void setInteger(I i) {
		}
	}
}
