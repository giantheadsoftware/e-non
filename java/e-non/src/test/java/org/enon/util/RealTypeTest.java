/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import org.enon.exception.EnonException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class RealTypeTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(RealTypeTest.class.getName());

	public RealTypeTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of valueOfType method, of class RealType.
	 */
	@Test
	public void testValueOfType() {
		LOGGER.info("valueOfType");
		for (RealType rt : RealType.values()) {
			Class<? extends Number> type = rt.type;
			assertSame(rt, RealType.valueOfType(type));
		}
	}

	/**
	 * Test of isPrimitive method, of class RealType.
	 */
	@Test
	public void testIsPrimitive() {
		LOGGER.info("isPrimitive");
		for (RealType rt : RealType.values()) {
			Class<? extends Number> type = rt.type;
			assertEquals(type.isPrimitive(), rt.isPrimitive());
		}
	}

//	/**
//	 * Test of inRange method, of class RealType.
//	 */
//	@Test
//	public void testInRangeFloat() {
//		LOGGER.info("inRangeFloat");
//		RealType floatTypeO = RealType.FLOAT_OBJECT;
//		RealType floatType = RealType.FLOAT;
//
//		for (float okValue : new float[]{0f, -1f, 1f, 103.202f, -113f, Float.MIN_VALUE, Float.MAX_VALUE, -Float.MIN_VALUE, -Float.MAX_VALUE}) {
//			assertTrue(okValue+" should be OK", floatType.inRange(okValue));
//			assertTrue(okValue+" should be OK", floatTypeO.inRange(okValue));
//		}
//
//		for (Number notOkValue : new Number[]{(byte)0, 5, 0.5, new BigDecimal(BigInteger.ONE)}) {
//			assertFalse(notOkValue+" should not be OK", floatType.inRange(notOkValue));
//			assertFalse(notOkValue+" should not be OK", floatTypeO.inRange(notOkValue));
//		}
//	}
//	/**
//	 * Test of inRange method, of class RealType.
//	 */
//	@Test
//	public void testInRangeDouble() {
//		LOGGER.info("inRangeDouble");
//		RealType doubleTypeO = RealType.DOUBLE_OBJECT;
//		RealType doubleType = RealType.DOUBLE;
//
//		for (float okValue : new float[]{0f, -1f, 1f, 103.202f, -113f, Float.MIN_VALUE, Float.MAX_VALUE, -Float.MIN_VALUE, -Float.MAX_VALUE}) {
//			assertTrue(okValue+" should be OK", doubleType.inRange(okValue));
//			assertTrue(okValue+" should be OK", doubleTypeO.inRange(okValue));
//		}
//
//		for (double okValue : new double[]{0.0, -1.0, 1.0, 103.202, -113.0, Double.MIN_VALUE, Double.MAX_VALUE, -Double.MIN_VALUE, -Double.MAX_VALUE}) {
//			assertTrue(okValue+" should be OK", doubleType.inRange(okValue));
//			assertTrue(okValue+" should be OK", doubleTypeO.inRange(okValue));
//		}
//
//		for (Number notOkValue : new Number[]{(byte)0, 5, new BigDecimal(BigInteger.ONE)}) {
//			assertFalse(notOkValue+" should not be OK", doubleType.inRange(notOkValue));
//			assertFalse(notOkValue+" should not be OK", doubleTypeO.inRange(notOkValue));
//		}
//	}
	/**
	 * Test of inRange method, of class RealType.
	 */
//	@Test
//	public void testInRangeBigDecimal() {
//		LOGGER.info("inRangeBigDecimal");
//		RealType bigType = RealType.BIG_DECIMAL;
//
//		for (float okValue : new float[]{0f, -1f, 1f, 103.202f, -113f, Float.MIN_VALUE, Float.MAX_VALUE, -Float.MIN_VALUE, -Float.MAX_VALUE}) {
//			assertTrue(okValue+" should be OK", bigType.inRange(okValue));
//		}
//
//		for (double okValue : new double[]{0.0, -1.0, 1.0, 103.202, -113.0, Double.MIN_VALUE, Double.MAX_VALUE, -Double.MIN_VALUE, -Double.MAX_VALUE}) {
//			assertTrue(okValue+" should be OK", bigType.inRange(okValue));
//		}
//
//		for (Number okValue : new Number[]{new BigDecimal(BigInteger.ONE), new BigDecimal(Double.MAX_VALUE)}) {
//			assertTrue(okValue+" should be OK", bigType.inRange(okValue));
//		}
//
//		for (Number notOkValue : new Number[]{Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, Float.NaN, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NaN}) {
//			assertFalse(notOkValue+" should not be OK", bigType.inRange(notOkValue));
//		}
//	}
	/**
	 * Test of compact method, of class RealType.
	 */
	@Test
	public void testCompact() {
		LOGGER.info("compact");

		Number[] Values = new Number[]{15f, -125.321, BigDecimal.valueOf(9)};
		for (Number number : Values) {
			assertSame(number, RealType.compact(number));
		}
	}

	/**
	 * Test of cast method, of class RealType.
	 */
	@Test
	public void testCast() {
		LOGGER.info("cast");
		Number cast = RealType.FLOAT.cast(35f);
		assertSame(Float.class, cast.getClass());
		assertEquals(35f, cast);

		cast = RealType.FLOAT_OBJECT.cast(35f);
		assertSame(Float.class, cast.getClass());
		assertEquals(35f, cast);

		cast = RealType.FLOAT_OBJECT.cast(35.9);
		assertSame(Float.class, cast.getClass());
		assertEquals(35.9f, cast);

		cast = RealType.DOUBLE.cast(35.546f);
		assertSame(Double.class, cast.getClass());
		assertTrue(Math.abs(35.546f - cast.doubleValue()) <= Float.MIN_VALUE);

		cast = RealType.DOUBLE.cast(35.546);
		assertSame(Double.class, cast.getClass());
		assertEquals(35.546, cast);

		cast = RealType.DOUBLE_OBJECT.cast(35.546);
		assertSame(Double.class, cast.getClass());
		assertEquals(35.546, cast);

		cast = RealType.DOUBLE.cast(new BigDecimal(35.546));
		assertSame(Double.class, cast.getClass());
		assertEquals(35.546, cast);

		cast = RealType.BIG_DECIMAL.cast(35.546f);
		assertSame(BigDecimal.class, cast.getClass());
		assertEquals(new BigDecimal(35.546f), cast);

		cast = RealType.BIG_DECIMAL.cast(35.546);
		assertSame(BigDecimal.class, cast.getClass());
		assertEquals(new BigDecimal(35.546), cast);

		BigDecimal big = BigDecimal.valueOf(123456.78);
		cast = RealType.BIG_DECIMAL.cast(big);
		assertSame(big, cast);
	}

	/**
	 * Test of cast method, of class RealType.
	 */
	@Test(/*expected = IllegalArgumentException.class*/)
	public void testCastFloatOutOfRangeDoubleFail() {
		LOGGER.info("castFloatOutOfRangeDoubleFail");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.FLOAT.cast(Double.MAX_VALUE);
		});
	}

	/**
	 * Test of cast method, of class RealType.
	 */
	@Test(/*expected = IllegalArgumentException.class*/)
	public void testCastFloatOutOfRangeBigFail() {
		LOGGER.info("castFloatOutOfRangeBigFail");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.FLOAT.cast(new BigDecimal(-Double.MAX_VALUE));
		});
	}

	/**
	 * Test of cast method, of class RealType.
	 */
	@Test(/*expected = IllegalArgumentException.class*/)
	public void testCastDoubleOutOfRangeBigFail() {
		LOGGER.info("castDoubleOutOfRangeBigFail");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.DOUBLE.cast(new BigDecimal(-Double.MAX_VALUE).multiply(BigDecimal.TEN));
		});
	}

	/**
	 * Test of cast method, of class RealType.
	 */
	@Test(/*expected = IllegalArgumentException.class*/)
	public void testCastBigOutOfRangeNegInfinityFail() {
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.BIG_DECIMAL.cast(Float.NEGATIVE_INFINITY);
		});
	}

	/**
	 * Test of cast method, of class RealType.
	 */
	@Test(/*expected = IllegalArgumentException.class*/)
	public void testCastBigOutOfRangePosInfinityFail() {
		LOGGER.info("testCastBigOutOfRangePosInfinityFail");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.BIG_DECIMAL.cast(Double.POSITIVE_INFINITY);
		});
	}

	/**
	 * Test of cast method, of class RealType.
	 */
	@Test(/*expected = IllegalArgumentException.class*/)
	public void testCastBigOutOfRangeNanFail() {
		LOGGER.info("testCastBigOutOfRangeNanFail");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.BIG_DECIMAL.cast(Float.NaN);
		});
	}

	@Test
	public void testIsInfinite() {
		LOGGER.info("isInfinite");
		for (Number n : new Number[]{Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY}) {
			assertTrue(RealType.isInfinite(n));
		}
		// not infinite
		for (Number n : new Number[]{105.8f, 289.31578, new BigDecimal(Double.MAX_VALUE), 123, Float.NaN, Double.NaN}) {
			assertFalse(RealType.isInfinite(n));
		}
	}

	@Test
	public void testFromString() {
		LOGGER.info("fromString");

		// floats
		for (String s : new String[]{"0", String.valueOf(Float.MAX_VALUE), String.valueOf(Float.MIN_VALUE)}) {
			assertEquals(Float.valueOf(s), RealType.FLOAT_OBJECT.fromString(s));
			assertEquals(Float.valueOf(s), RealType.FLOAT.fromString(s));
		}

		// doubles
		for (String s : new String[]{"0", String.valueOf(Double.MAX_VALUE), String.valueOf(Double.MIN_VALUE)}) {
			assertEquals(Double.valueOf(s), RealType.DOUBLE_OBJECT.fromString(s));
			assertEquals(Double.valueOf(s), RealType.DOUBLE.fromString(s));
		}

		// big decimals
		for (String s : new String[]{"0", String.valueOf(Double.MAX_VALUE), String.valueOf(Double.MIN_VALUE)}) {
			assertEquals(new BigDecimal(s), RealType.BIG_DECIMAL.fromString(s));
		}

		// NaN
		for (String s : new String[]{"not", "", "9x"}) {
			assertEquals(Float.NaN, RealType.FLOAT_OBJECT.fromString(s));
			assertEquals(Float.NaN, RealType.FLOAT.fromString(s));
			assertEquals(Double.NaN, RealType.DOUBLE_OBJECT.fromString(s));
			assertEquals(Double.NaN, RealType.DOUBLE.fromString(s));
		}
	}

	@Test(/*expected = IllegalArgumentException.class*/)
	public void testFromStringFloatTooLargeFail() {
		LOGGER.info("fromStringFloatTooLargeFail");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.FLOAT_OBJECT.fromString(String.valueOf(Double.MAX_VALUE));
		});
	}

	@Test(/*expected = IllegalArgumentException.class*/)
	public void testFromStringFloatTooNegativeFail() {
		LOGGER.info("fromStringFloatTooNegativeFail");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.FLOAT_OBJECT.fromString(String.valueOf(-Double.MAX_VALUE));
		});
	}

	@Test(/*expected = IllegalArgumentException.class*/)
	public void testFromStringDoubleTooLargeFail() {
		LOGGER.info("fromStringFloatTooLargeFail");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.DOUBLE_OBJECT.fromString(new BigDecimal(Double.MAX_VALUE).multiply(BigDecimal.TEN).toString());
		});
	}

	@Test(/*expected = IllegalArgumentException.class*/)
	public void testFromStringDoubleTooNegativeFail() {
		LOGGER.info("fromStringFloatTooNegativeFail");
		Exception x = assertThrows(IllegalArgumentException.class, () -> {
			RealType.DOUBLE_OBJECT.fromString(new BigDecimal(-Double.MAX_VALUE).multiply(BigDecimal.TEN).toString());
		});
	}

	@Test(/*expected = EnonException.class*/)
	public void testFromStringBigNanFail() {
		LOGGER.info("fromStringBigNanFail");
		Exception x = assertThrows(EnonException.class, () -> {
			RealType.BIG_DECIMAL.fromString("bad");
		});
	}

//	@Test
//	public void testFromInt() {
//		LOGGER.info("fromInt");
//
//		// finite
//		for (Number n : new Number[]{(byte)0, (short)257, 134578, Long.MAX_VALUE, Long.MIN_VALUE}) {
//			assertEquals(n.floatValue(), RealType.FLOAT_OBJECT.fromInt(n));
//			assertEquals(n.floatValue(), RealType.FLOAT.fromInt(n));
//			assertEquals(n.doubleValue(), RealType.DOUBLE_OBJECT.fromInt(n));
//			assertEquals(n.doubleValue(), RealType.DOUBLE.fromInt(n));
//			assertEquals(new BigDecimal(n.doubleValue()), RealType.BIG_DECIMAL.fromInt(n));
//		}
//
//		// too big for float
//		for (Number n : new Number[]{new BigDecimal(Double.MAX_VALUE).toBigInteger(), new BigDecimal(-Double.MAX_VALUE).toBigInteger()}) {
//			try {
//				RealType.FLOAT_OBJECT.fromInt(n);
//				fail("should fail");
//			} catch (IllegalArgumentException x) {
//				// success
//			}
//		}
//
//		// too big for double
//		for (Number n : new Number[]{new BigDecimal(Double.MAX_VALUE).multiply(BigDecimal.TEN).toBigInteger(), new BigDecimal(-Double.MAX_VALUE).multiply(BigDecimal.TEN).toBigInteger()}) {
//			try {
//				RealType.DOUBLE_OBJECT.fromInt(n);
//				fail("should fail");
//			} catch (IllegalArgumentException x) {
//				// success
//			}
//		}
//	}
}
