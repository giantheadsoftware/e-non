/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.e2e;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Reader;
import org.enon.Writer;
import org.enon.exception.EnonException;
import org.enon.test.model.simple.EnonXAll;
import org.enon.test.util.TestDataOutputStream;
import org.enon.tool.LoggingEventPublisher;
import org.enon.tool.validate.EnonValidationException;
import org.enon.tool.validate.ValidationReader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class EnonXe2eIT {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnonXe2eIT.class.getName());

	private final EnonConfig config0 = EnonConfig.defaults();
	private final EnonConfig configX = new EnonConfig.Builder().feature(FeatureSet.X).build();

	public EnonXe2eIT() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testRoundTripSingle() {
		LOGGER.info("roundTripSingle");
		// The EnonXAll object should be OK for e-NON-0 config.  The writer should choose valis elements
		roundTrip(config0, EnonXAll.generateRandomInstance(true));
		roundTrip(config0, EnonXAll.generateNullInstance());
		roundTrip(config0, EnonXAll.generateMaxInstance());
		roundTrip(config0, EnonXAll.generateMinInstance());
		roundTrip(configX, EnonXAll.generateRandomInstance(true));
		roundTrip(configX, EnonXAll.generateNullInstance());
		roundTrip(configX, EnonXAll.generateMaxInstance());
		roundTrip(configX, EnonXAll.generateMinInstance());
	}

	@Test
	public void testRoundTripMultiple() {
		LOGGER.info("roundTripMultiple");
		for (int i = 0; i < 100; i++) {
			// The EnonXAll object should be OK for e-NON-0 config.  The writer should choose valis elements
			roundTrip(config0, EnonXAll.generateRandomInstance(true));
			roundTrip(config0, EnonXAll.generateNullInstance());
			roundTrip(config0, EnonXAll.generateMaxInstance());
			roundTrip(config0, EnonXAll.generateMinInstance());
			roundTrip(configX, EnonXAll.generateRandomInstance(true));
			roundTrip(configX, EnonXAll.generateNullInstance());
			roundTrip(configX, EnonXAll.generateMaxInstance());
			roundTrip(configX, EnonXAll.generateMinInstance());
		}
	}

	@Test
	public void testRoundTripMultipleConcurrent() {
		final int concurrency = 10;
		ExecutorService execService = Executors.newFixedThreadPool(concurrency);
		List<Future> futures = new ArrayList<>();
		for (int i = 0; i < concurrency * 5; i++) {
			futures.add(execService.submit(() -> {
				for (int j = 0; j < 100; j++) {
					// The EnonXAll object should be OK for e-NON-0 config.  The writer should choose valis elements
					roundTrip(config0, EnonXAll.generateRandomInstance(true));
					roundTrip(config0, EnonXAll.generateNullInstance());
					roundTrip(config0, EnonXAll.generateMaxInstance());
					roundTrip(config0, EnonXAll.generateMinInstance());
					roundTrip(configX, EnonXAll.generateRandomInstance(true));
					roundTrip(configX, EnonXAll.generateNullInstance());
					roundTrip(configX, EnonXAll.generateMaxInstance());
					roundTrip(configX, EnonXAll.generateMinInstance());
				}
			}));
		}
		try {
			for (Future future : futures) {
				future.get();
			}
		} catch (ExecutionException | InterruptedException x) {
			LOGGER.error(x.getMessage(), x);
			fail(x.getMessage());
		}
	}

	private void roundTrip(EnonConfig config, EnonXAll object) {

		try {
			TestDataOutputStream tdos = new TestDataOutputStream();

			Writer writer = new Writer(tdos.stream(), EnonConfig.defaults()/*config*/);

			writer.write(object);
			writer.flush();

			// run the validator on the stream
			ValidationReader validator = ValidationReader.create(tdos.dataInputStream(), config.getFeatures());
//					.eventPublisher(new LoggingEventPublisher(getClass().getSimpleName()));
			
			List<EnonValidationException> errors = validator.validate();
			errors.stream().forEach(error -> LOGGER.error(error.getMessage(), error));
			assertTrue(errors.isEmpty());

			// read the stream as an object and compare it to the original
			Reader reader = new Reader(tdos.dataInputStream(), config);

			EnonXAll result = reader.read(object.getClass());

			tdos.close();

			result.assertEqualTo(object);
//			if (!object.equals(result)) {
//				fail("round trip failed for instance: "+object.toString());
//			}
		} catch (IOException x) {
			throw new EnonException("Exception during roundTrip: ", x);
		}
	}
}
