/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import org.enon.EnonTestException;

/**
 * Generate random strings
 * @author clininger $Id: $
 */
public class ValueGenerator {
	
	private static ValueGenerator INSTANCE;
	private final String[] RANDOM_STRINGS = new String[48];
	private final double STRING_NEXT_INCREMENT = Math.random()* 10.0;
	private int nextStringIndex = randomInt(0, 10);

	private ValueGenerator() {
		init();
	}
	
	public static ValueGenerator getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ValueGenerator();
		}
		return INSTANCE;
	}
	
	private void init() {
		// generate a set of "random" strings.  Math.random() is expensive, so try to minimize it
		for (int i = 0; i < RANDOM_STRINGS.length; i++) {
			StringBuilder sb = new StringBuilder();
			switch ( i % 4 ) {
				case 0: {
					// generate a random string
					for (int r = 0; r < 15; r++) {
						sb.append(Character.toChars(randomInt(32, 0xD7FF)));  // range from 'A' up to the reserved range starting at D800
					}
				} break;
				
				case 1:
				case 2:
				case 3: {
					// append the previous string 2x to create longer strings
					sb.append(RANDOM_STRINGS[i-1]).append(RANDOM_STRINGS[i-1]);
				}
			}
			RANDOM_STRINGS[i] = sb.toString();
		}
	}
	
	/**
	 * @return  a random float value, 50% chance of being negative
	 */
	public float randomFloat() {
		return (float)(Math.random() * Float.MAX_VALUE * Double.compare(0.5, Math.random()));
	}	
	
	public float[] randomFloatArray() {
		float[] array = new float[(randomInt(0, 20))];
		for (int i = 0; i < array.length; i++) {
			array[i] = randomFloat();
		}
		return array;
	}

	/**
	 * @return  a random double value, 50% chance of being negative
	 */
	public double randomDouble() {
		return Math.random() * Double.MAX_VALUE * Double.compare(0.5, Math.random());
	}
	
	public double[] randomDoubleArray() {
		double[] array = new double[(randomInt(0, 20))];
		for (int i = 0; i < array.length; i++) {
			array[i] = randomDouble();
		}
		return array;
	}

	/**
	 * @return  a random boolean value, 50% chance 
	 */
	public boolean randomBool() {
		return Double.compare(0.5, Math.random()) >= 0;
	}
	
	public boolean[] randomBoolArray() {
		boolean[] array = new boolean[(randomInt(0, 20))];
		for (int i = 0; i < array.length; i++) {
			array[i] = randomBool();
		}
		return array;
	}

	/**
	 * Random short within the provided range
	 * @param min min value
	 * @param max max value
	 * @return random short
	 */
	public short randomShort(short min, short max) {
		return (short)(Math.random() * (max - min) + min);
	}
	
	public short[] randomShortArray() {
		short[] array = new short[(randomInt(0, 20))];
		for (int i = 0; i < array.length; i++) {
			array[i] = randomShort(Short.MIN_VALUE, Short.MAX_VALUE);
		}
		return array;
	}

	/**
	 * Random int within the provided range
	 * @param min min value
	 * @param max max value
	 * @return random int
	 */
	public int randomInt(int min, int max) {
		return (int)(Math.random() * (max - min) + min);
	}
	
	public int[] randomIntArray() {
		int[] array = new int[(randomInt(0, 20))];
		for (int i = 0; i < array.length; i++) {
			array[i] = randomInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
		}
		return array;
	}
	
	/**
	 * Random long within the provided range
	 * @param min min value
	 * @param max max value
	 * @return random long
	 */
	public long randomLong(long min, long max) {
		return (long)(Math.random() * (max - min) + min);
	}
	
	public long[] randomLongArray() {
		long[] array = new long[(randomInt(0, 20))];
		for (int i = 0; i < array.length; i++) {
			array[i] = randomLong(Long.MIN_VALUE, Long.MAX_VALUE);
		}
		return array;
	}

	public String randomString() {
		synchronized (RANDOM_STRINGS) {
			nextStringIndex += STRING_NEXT_INCREMENT;
			if (nextStringIndex >= RANDOM_STRINGS.length) {
				nextStringIndex -= RANDOM_STRINGS.length;
			}
			return RANDOM_STRINGS[(int)nextStringIndex];
		}
	}

	/**
	 * @return Random large decimal, larger than Long.MAX_VALUE, 50% chance of being negative.
	 */
	public Number randomLargeNumber() {
		return new BigDecimal(Math.random())
				.multiply(BigDecimal.valueOf(Double.MAX_VALUE))
				.add(BigDecimal.valueOf(Double.MAX_VALUE))
				.multiply(BigDecimal.valueOf(Double.compare(0.5, Math.random())));
	}

	public byte[] randomBytes() {
		int count = randomInt(20, 50);
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			for (int i = 0; i < count; i++) {
				baos.write(randomString().getBytes());
			}
			return baos.toByteArray();
		} catch (IOException x) {
			throw new EnonTestException(x.getMessage(), x);
		}
	}
}
