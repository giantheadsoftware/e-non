/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.model.simple;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.enon.element.NanoIntElement;
import org.enon.test.util.ValueGenerator;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This POJO has standard getter/setter methods. It also has some non-property methods (i.e. toString) and some get/set methods that are mismatched.
 *
 * @author clininger $Id: $
 */
public class EnonXAll {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnonXAll.class.getName());

	private static final ValueGenerator DATA_GEN = ValueGenerator.getInstance();

	// scalar
	private Object nullProp;
	private boolean trueProp;
	private boolean falseProp;
	private byte nanoIntProp;
	private byte byteProp;
	private short shortProp;
	private int intProp;
	private long longProp;
	private float floatProp;
	private double doubleProp;
	private char charProp;
	private Boolean trueObjectProp;
	private Boolean falseObjectProp;
	private Byte nanoIntObjectProp;
	private Byte byteObjectProp;
	private Short shortObjectProp;
	private Integer intObjectProp;
	private Long longObjectProp;
	private Float floatObjectProp;
	private Double doubleObjectProp;
	private Character charObjectProp;
	private String stringProp;
	private byte[] dataProp;
	private Number numberProp;

	// arrays
	private boolean[] boolArray;
	private short[] shortArray;
	private int[] intArray;
	private long[] longArray;
	private float[] floatArray;
	private double[] doubleArray;

	// collections
	private List<String> stringList;
	private Set<Integer> intSet;
	private Map<String, Integer> stringIntMap;
	private EnonXAll nested;
	private Map<String, EnonXAll> nestedByString;

	public EnonXAll() {
	}

	public EnonXAll(
			byte nanoIntProp,
			byte byteProp,
			short shortProp,
			int intProp,
			long longProp,
			float floatProp,
			double doubleProp,
			char charProp,
			Byte nanoIntObjectProp,
			Byte byteObjectProp,
			Short shortObjectProp,
			Integer intObjectProp,
			Long longObjectProp,
			Float floatObjectProp,
			Double doubleObjectProp,
			Character charObjectProp,
			String stringProp,
			byte[] dataProp,
			Number numberProp,
			// arrays
			boolean[] boolArray,
			short[] shortArray,
			int[] intarray,
			long[] longArray,
			float[] floatArray,
			double[] doubleArray,
			// collections
			List<String> stringList,
			Set<Integer> intSet,
			Map<String, Integer> stringIntMap,
			EnonXAll nested,
			Map<String, EnonXAll> nestedByString) {
		this.nullProp = null;
		this.trueProp = true;
		this.falseProp = false;
		this.nanoIntProp = nanoIntProp;
		this.byteProp = byteProp;
		this.shortProp = shortProp;
		this.intProp = intProp;
		this.longProp = longProp;
		this.floatProp = floatProp;
		this.doubleProp = doubleProp;
		this.charProp = charProp;
		this.trueObjectProp = Boolean.TRUE;
		this.falseObjectProp = Boolean.FALSE;
		this.nanoIntObjectProp = nanoIntObjectProp;
		this.byteObjectProp = byteObjectProp;
		this.shortObjectProp = shortObjectProp;
		this.intObjectProp = intObjectProp;
		this.longObjectProp = longObjectProp;
		this.floatObjectProp = floatObjectProp;
		this.doubleObjectProp = doubleObjectProp;
		this.charObjectProp = charObjectProp;
		this.stringProp = stringProp;
		this.dataProp = dataProp;
		this.numberProp = numberProp;
		//arrays
		this.boolArray = boolArray;
		this.shortArray = shortArray;
		this.intArray = intarray;
		this.longArray = longArray;
		this.floatArray = floatArray;
		this.doubleArray = doubleArray;
		// collections
		this.stringList = stringList;
		this.intSet = intSet;
		this.stringIntMap = stringIntMap;
		this.nested = nested;
		this.nestedByString = nestedByString;
	}

	public Object getNullProp() {
		return nullProp;
	}

	public void setNullProp(Object nullProp) {
		this.nullProp = nullProp;
	}

	public boolean isTrueProp() {
		return trueProp;
	}

	public void setTrueProp(boolean trueProp) {
		this.trueProp = trueProp;
	}

	public boolean isFalseProp() {
		return falseProp;
	}

	public void setFalseProp(boolean falseProp) {
		this.falseProp = falseProp;
	}

	public byte getNanoIntProp() {
		return nanoIntProp;
	}

	public void setNanoIntProp(byte nanoIntProp) {
		this.nanoIntProp = nanoIntProp;
	}

	public int getIntProp() {
		return intProp;
	}

	public void setIntProp(int intProp) {
		this.intProp = intProp;
	}

	public double getDoubleProp() {
		return doubleProp;
	}

	public void setDoubleProp(double doubleProp) {
		this.doubleProp = doubleProp;
	}

	public char getCharProp() {
		return charProp;
	}

	public void setCharProp(char charProp) {
		this.charProp = charProp;
	}

	public Boolean getTrueObjectProp() {
		return trueObjectProp;
	}

	public void setTrueObjectProp(Boolean trueObjectProp) {
		this.trueObjectProp = trueObjectProp;
	}

	public Boolean getFalseObjectProp() {
		return falseObjectProp;
	}

	public void setFalseObjectProp(Boolean falseObjectProp) {
		this.falseObjectProp = falseObjectProp;
	}

	public Byte getNanoIntObjectProp() {
		return nanoIntObjectProp;
	}

	public void setNanoIntObjectProp(Byte nanoIntObjectProp) {
		this.nanoIntObjectProp = nanoIntObjectProp;
	}

	public Integer getIntObjectProp() {
		return intObjectProp;
	}

	public void setIntObjectProp(Integer intObjectProp) {
		this.intObjectProp = intObjectProp;
	}

	public Double getDoubleObjectProp() {
		return doubleObjectProp;
	}

	public void setDoubleObjectProp(Double doubleObjectProp) {
		this.doubleObjectProp = doubleObjectProp;
	}

	public Character getCharObjectProp() {
		return charObjectProp;
	}

	public void setCharObjectProp(Character charObjectProp) {
		this.charObjectProp = charObjectProp;
	}

	public String getStringProp() {
		return stringProp;
	}

	public void setStringProp(String stringProp) {
		this.stringProp = stringProp;
	}

	public byte[] getDataProp() {
		return dataProp;
	}

	public void setDataProp(byte[] dataProp) {
		this.dataProp = dataProp;
	}

	public Number getNumberProp() {
		return numberProp;
	}

	public void setNumberProp(Number numberProp) {
		this.numberProp = numberProp;
	}

	public boolean[] getBoolArray() {
		return boolArray;
	}

	public void setBoolArray(boolean[] boolArray) {
		this.boolArray = boolArray;
	}

	public short[] getShortArray() {
		return shortArray;
	}

	public void setShortArray(short[] shortArray) {
		this.shortArray = shortArray;
	}

	public int[] getIntArray() {
		return intArray;
	}

	public void setIntArray(int[] intArray) {
		this.intArray = intArray;
	}

	public long[] getLongArray() {
		return longArray;
	}

	public void setLongArray(long[] longArray) {
		this.longArray = longArray;
	}

	public float[] getFloatArray() {
		return floatArray;
	}

	public void setFloatArray(float[] floatArray) {
		this.floatArray = floatArray;
	}

	public double[] getDoubleArray() {
		return doubleArray;
	}

	public void setDoubleArray(double[] doubleArray) {
		this.doubleArray = doubleArray;
	}

	public List<String> getStringList() {
		return stringList;
	}

	public void setStringList(List<String> stringList) {
		this.stringList = stringList;
	}

	public Set<Integer> getIntSet() {
		return intSet;
	}

	public void setIntSet(Set<Integer> intSet) {
		this.intSet = intSet;
	}

	public Map<String, Integer> getStringIntMap() {
		return stringIntMap;
	}

	public void setStringIntMap(Map<String, Integer> stringIntMap) {
		this.stringIntMap = stringIntMap;
	}

	public EnonXAll getNested() {
		return nested;
	}

	public void setNested(EnonXAll nested) {
		this.nested = nested;
	}

	public Map<String, EnonXAll> getNestedByString() {
		return nestedByString;
	}

	public void setNestedByString(Map<String, EnonXAll> nestedByString) {
		this.nestedByString = nestedByString;
	}

	public byte getByteProp() {
		return byteProp;
	}

	public void setByteProp(byte byteProp) {
		this.byteProp = byteProp;
	}

	public short getShortProp() {
		return shortProp;
	}

	public void setShortProp(short shortProp) {
		this.shortProp = shortProp;
	}

	public long getLongProp() {
		return longProp;
	}

	public void setLongProp(long longProp) {
		this.longProp = longProp;
	}

	public float getFloatProp() {
		return floatProp;
	}

	public void setFloatProp(float floatProp) {
		this.floatProp = floatProp;
	}

	public Byte getByteObjectProp() {
		return byteObjectProp;
	}

	public void setByteObjectProp(Byte byteObjectProp) {
		this.byteObjectProp = byteObjectProp;
	}

	public Short getShortObjectProp() {
		return shortObjectProp;
	}

	public void setShortObjectProp(Short shortObjectProp) {
		this.shortObjectProp = shortObjectProp;
	}

	public Long getLongObjectProp() {
		return longObjectProp;
	}

	public void setLongObjectProp(Long longObjectProp) {
		this.longObjectProp = longObjectProp;
	}

	public Float getFloatObjectProp() {
		return floatObjectProp;
	}

	public void setFloatObjectProp(Float floatObjectProp) {
		this.floatObjectProp = floatObjectProp;
	}

//	@Override
//	public boolean equals(Object o) {
//		if (o.getClass() != this.getClass()) {
//			return false;
//		}
//		EnonXAll that = (EnonXAll) o;
//		return
//				nullProp == null && that.nullProp == null
//				&& trueObjectProp == that.trueProp
//				&& falseProp == that.falseProp
//				&& nanoIntProp == that.nanoIntProp
//				&& byteProp == that.byteProp
//				&& shortProp == that.shortProp
//				&& intProp == that.intProp
//				&& longProp == that.longProp
//				&& Float.isNaN(floatProp) ? Float.isNaN(that.floatProp) : floatProp == that.floatProp
//				&& Double.isNaN(doubleProp) ? Double.isNaN(that.doubleProp) : doubleProp == that.doubleProp
//				&& charProp == that.charProp
//				&& assertEqualObject("trueObjectProp", trueObjectProp, that.trueObjectProp)
//				&& assertEqualObject("falseObjectProp", falseObjectProp, that.falseObjectProp)
//				&& assertEqualObject("nanoIntObjectProp", nanoIntObjectProp, that.nanoIntObjectProp)
//				&& assertEqualObject("byteObjectProp", byteObjectProp, that.byteObjectProp)
//				&& assertEqualObject("shortObjectProp", shortObjectProp, that.shortObjectProp)
//				&& assertEqualObject("intObjectProp", intObjectProp, that.intObjectProp)
//				&& assertEqualObject("longObjectProp", longObjectProp, that.longObjectProp)
//				&& assertEqualObject("floatObjectProp", floatObjectProp, that.floatObjectProp)
//				&& assertEqualObject("doubleObjectProp", doubleObjectProp, that.doubleObjectProp)
//				&& assertEqualObject("charObjectProp", charObjectProp, that.charObjectProp)
//				&& assertEqualObject("stringProp", stringProp, that.stringProp)
//				&& Arrays.equals(dataProp, that.dataProp)
//				&& numberEquals("numberProp", numberProp, that.numberProp)
//
//				//arrays
//				&& Arrays.equals(boolArray, that.boolArray)
//				&& Arrays.equals(shortArray, that.shortArray)
//				&& Arrays.equals(intArray, that.intArray)
//				&& Arrays.equals(longArray, that.longArray)
//				&& Arrays.equals(floatArray, that.floatArray)
//				&& Arrays.equals(doubleArray, that.doubleArray)
//
//				// collections
//				&& assertEqualList("stringList", stringList, that.stringList)
//				&& setEquals("intSet", intSet, that.intSet)
//				&& mapEquals("stringIntMap", stringIntMap, that.stringIntMap)
//				&& assertEqualObject("nested", nested, that.nested)
//				&& mapEquals("nestedByString", nestedByString, that.nestedByString);
//	}

	public void assertEqualTo(Object o) {
		assertSame(this.getClass(), o.getClass(), "Class:");
		EnonXAll that = (EnonXAll) o;
		assertNull(nullProp, "this.nullProp");
		assertNull(that.nullProp, "that.nullProp");
		assertTrue(trueProp, "trueProp");
		assertEquals(trueProp, that.trueProp, "trueProp");
		assertFalse(falseProp, "falseProp");
		assertEquals(falseProp, that.falseProp, "falseProp");
		assertEquals(nanoIntProp, that.nanoIntProp, "nanoIntProp");
		assertEquals(byteProp, that.byteProp, "byteProp");
		assertEquals(shortProp, that.shortProp, "shortProp");
		assertEquals(intProp, that.intProp, "intProp");
		assertEquals(longProp, that.longProp, "longProp");
		if (Float.isNaN(floatProp)) {
			assertTrue(Float.isNaN(that.floatProp), "floatProp == NaN");
		} else {
			assertEquals(floatProp, that.floatProp, Float.MIN_VALUE, "floatProp");
		}
		if (Double.isNaN(doubleProp)) {
			assertTrue(Double.isNaN(that.doubleProp), "doubleProp == NaN");
		} else {
			 assertEquals(doubleProp, that.doubleProp, Double.MIN_VALUE, "doubleProp");
		}
		assertEquals(charProp, that.charProp,"charProp");
		assertEquals(trueObjectProp, that.trueObjectProp, "trueObjectProp");
		assertEquals(falseObjectProp, that.falseObjectProp, "falseObjectProp");
		assertEquals(nanoIntObjectProp, that.nanoIntObjectProp, "nanoIntObjectProp");
		assertEquals(byteObjectProp, that.byteObjectProp, "byteObjectProp");
		assertEquals(shortObjectProp, that.shortObjectProp, "shortObjectProp");
		assertEquals(intObjectProp, that.intObjectProp, "intObjectProp");
		assertEquals(longObjectProp, that.longObjectProp, "longObjectProp");
		assertEquals(floatObjectProp, that.floatObjectProp, "floatObjectProp");
		assertEquals(doubleObjectProp, that.doubleObjectProp, "doubleObjectProp");
		if (charObjectProp != null && !charObjectProp.equals(that.charObjectProp)) {
			LOGGER.info((int)charObjectProp+" : "+(int)that.charObjectProp);
		}
		assertEquals(charObjectProp, that.charObjectProp, "charObjectProp");
		assertEquals(stringProp, that.stringProp, "stringProp");
		assertArrayEquals(dataProp, that.dataProp, "dataProp");
		assertEqualNumber("numberProp", numberProp, that.numberProp);

		//arrays
		assertArrayEquals(boolArray, that.boolArray);
		assertArrayEquals(shortArray, that.shortArray);
		assertArrayEquals(intArray, that.intArray);
		assertArrayEquals(longArray, that.longArray);
		assertArrayEquals(floatArray, that.floatArray, Float.MIN_VALUE);
		assertArrayEquals(doubleArray, that.doubleArray, Double.MIN_VALUE);

		// collections
		assertEqualList("stringList", stringList, that.stringList);
		assertEqualSet("intSet", intSet, that.intSet);
		assertEqualMap("stringIntMap", stringIntMap, that.stringIntMap);
		if (nested != null) {
			nested.assertEqualTo(that.nested);
		} else {
			assertNull(that.nested, "nested");
		}
		assertEqualMap("nestedByString", nestedByString, that.nestedByString);
	}

	private void assertEqualObject(String label, Object o1, Object o2) {
		if (o1 == null) {
			assertNull(o2, label);
		}
		if (o1 instanceof EnonXAll) {
			((EnonXAll)o1).assertEqualTo(o2);
		} else {
			assertEquals(o1, o2, label);
		}
	}

	private void assertEqualList(String label, List l1, List l2) {
		if (l1 == null) {
			assertNull(l2);
		} else if (l2 == null) {
			fail(label+": l2 == null != l1");
		} else {
			assertEquals(l1.size(), l2.size(), label);
			for (int i = 0; i < l1.size(); i++) {
				assertEqualObject(label+": entry", l1.get(i), l2.get(i));
			}
		}
	}

	private void assertEqualSet(String label, Set s1, Set s2) {
		if (s1 == null) {
			assertNull(s2, label);
		} else if (s2 == null) {
			fail(label+": s2 == null != s1");
		} else {
			assertEquals(s1.size(), s2.size(), label);
			assertTrue(s2.containsAll(s1), label);
		}
	}

	private void assertEqualMap(String label, Map m1, Map m2) {
		if (m1 == null) {
			assertNull(m2);
		} else if (m2 == null) {
			fail(label+": m2 == null != m1");
		} else {
			assertEquals(m1.size(), m2.size(), label);
			assertTrue(m2.keySet().containsAll(m1.keySet()), label);
			for (Object key : m1.keySet()) {
				assertEqualObject(label+": entry", m1.get(key), m2.get(key));
			}
		}
	}

	private void assertEqualNumber(String label, Number n1, Number n2) {
		label = label + "(" +String.valueOf(n1) + " != " + String.valueOf(n2) + ")";
		if (n1 == null) {
			assertNull(n2);
		} else if (n2 == null) {
			fail(label+": n2 == null != n1");
		} else {
//			assertSame(label, n1.getClass(), n2.getClass());
			assertEquals(normalizeNumber(n1), normalizeNumber(n2), label);
		}
	}

	/**
	 * Attempt to resolve to a BigInteger or BigDecimal
	 * @param n Some non-null number
	 * @return BigInteger or BigDecimal
	 */
	private Number normalizeNumber(Number n) {
		Class<? extends Number> nClass = (Class<Number>) n.getClass();
		if (nClass == BigInteger.class) {
			return n;
		}
		if (nClass == BigDecimal.class) {
			// try to reduce to BigInteger if possible
			try {
				return ((BigDecimal)n).toBigIntegerExact();
			} catch (ArithmeticException x) {
				return n;
			}
		}
		if (nClass == Double.class || nClass == Float.class) {
			return normalizeNumber(BigDecimal.valueOf(n.doubleValue()));
		}
		return BigInteger.valueOf(n.longValue());
	}

	public static EnonXAll generateRandomInstance(boolean nest) {
		List<String> stringList = new LinkedList<>();
		int count = DATA_GEN.randomInt(10, 20);
		for (int i = 0; i < count; i++) {
			stringList.add(DATA_GEN.randomString());
		}

		Set<Integer> intSet = new HashSet<>();
		count = DATA_GEN.randomInt(10, 20);
		for (int i = 0; i < count; i++) {
			intSet.add(DATA_GEN.randomInt(Integer.MIN_VALUE, Integer.MAX_VALUE));
		}

		Map<String, Integer> stringIntMap = new LinkedHashMap<>();
		count = DATA_GEN.randomInt(10, 20);
		for (int i = 0; i < count; i++) {
			stringIntMap.put(DATA_GEN.randomString(), DATA_GEN.randomInt(Integer.MIN_VALUE, Integer.MAX_VALUE));
		}

		Map<String, EnonXAll> nestedByString = new LinkedHashMap<>();
		if (nest) {
			count = DATA_GEN.randomInt(10, 20);
			for (int i = 0; i < count; i++) {
				EnonXAll value = generateRandomInstance(false);
				nestedByString.put(value.getStringProp(), value);
			}
		}

		return new EnonXAll(
				(byte)DATA_GEN.randomInt(NanoIntElement.MIN_VALUE, NanoIntElement.MAX_VALUE),
				(byte)DATA_GEN.randomInt(NanoIntElement.MAX_VALUE + 1, Byte.MAX_VALUE),
				(short)DATA_GEN.randomInt(Byte.MAX_VALUE + 1, Short.MAX_VALUE),
				DATA_GEN.randomInt(Short.MAX_VALUE + 1, Integer.MAX_VALUE),
				DATA_GEN.randomLong(Integer.MAX_VALUE + 1, Long.MAX_VALUE),
				DATA_GEN.randomFloat(),
				DATA_GEN.randomDouble(),
				Character.toChars(DATA_GEN.randomInt(0, 255))[0],
				(byte)DATA_GEN.randomInt(NanoIntElement.MIN_VALUE, NanoIntElement.MAX_VALUE),
				(byte)DATA_GEN.randomInt(Byte.MIN_VALUE, NanoIntElement.MIN_VALUE - 1),
				(short)DATA_GEN.randomInt(Short.MIN_VALUE, Byte.MIN_VALUE - 1),
				DATA_GEN.randomInt(Integer.MIN_VALUE, Short.MIN_VALUE - 1),
				DATA_GEN.randomLong(Long.MIN_VALUE, Integer.MIN_VALUE - 1),
				DATA_GEN.randomFloat(),
				DATA_GEN.randomDouble(),
				Character.toChars(DATA_GEN.randomInt(0, 255))[0],
				DATA_GEN.randomString(),
				DATA_GEN.randomBytes(),
				DATA_GEN.randomLargeNumber(),
				DATA_GEN.randomBoolArray(),
				DATA_GEN.randomShortArray(),
				DATA_GEN.randomIntArray(),
				DATA_GEN.randomLongArray(),
				DATA_GEN.randomFloatArray(),
				DATA_GEN.randomDoubleArray(),
				stringList,
				intSet,
				stringIntMap,
				nest ? generateRandomInstance(false) : null,
				nestedByString);
	}

	public static EnonXAll generateNullInstance() {
		return new EnonXAll(
				(byte)0,  // nano-int Prop
				(byte)0,  // byteProp
				(short)0, // shortProp
				0, // int prop
				0L, // longProp
				Float.NaN,  // floatProp
				Double.NaN, // doubleProp
				(char)0,  // charProp
				null,  // nanoIntObject
				null,  // byteObject
				null,  // shortObject
				null,  // intObject
				null,  // longObject
				null,  // floatObject
				null,  // doubleObject
				null,  // charObject
				null,  // stringObject
				new byte[0],  // blobObject
				null,  // number
				null,  // bool[]
				null,  // short[]
				null,  // int[]
				null,  // long[]
				null,  // float[]
				null,  // double[]
				null,  // list
				null,  // set
				null,  // map
				null,  // nested
				null); // nestedByString
	}

	public static EnonXAll generateMaxInstance() {
		return new EnonXAll(
				(byte)NanoIntElement.MAX_VALUE,
				Byte.MAX_VALUE,
				Short.MAX_VALUE,
				Integer.MAX_VALUE,
				Long.MAX_VALUE,
				Float.MAX_VALUE,
				Double.MAX_VALUE,
				Character.toChars(255)[0],
				(byte)NanoIntElement.MAX_VALUE,
				Byte.MAX_VALUE,
				Short.MAX_VALUE,
				Integer.MAX_VALUE,
				Long.MAX_VALUE,
				Float.POSITIVE_INFINITY,
				Double.POSITIVE_INFINITY,
				Character.toChars(255)[0],
				null,  // what would be a max string???
				new byte[0], // what would be a max byte[]?
				null, // there is no max large number... we already test larger than Double.MAX_VALUE
				new boolean[0],
				new short[0],
				new int[0],
				new long[0],
				new float[0],
				new double[0],
				null, // how to max collections?
				null,
				null,
				null,
				null);
	}

	public static EnonXAll generateMinInstance() {
		return new EnonXAll(
				(byte)NanoIntElement.MIN_VALUE,
				Byte.MIN_VALUE,
				Short.MIN_VALUE,
				Integer.MIN_VALUE,
				Long.MIN_VALUE,
				Float.MIN_VALUE,
				Double.MIN_VALUE,
				Character.toChars(0)[0],
				(byte)NanoIntElement.MIN_VALUE,
				Byte.MIN_VALUE,
				Short.MIN_VALUE,
				Integer.MIN_VALUE,
				Long.MIN_VALUE,
				Float.MIN_VALUE,
				Double.NEGATIVE_INFINITY,
				Character.toChars(0)[0],
				"",
				new byte[0],
				new BigDecimal(Double.MIN_VALUE).divide(BigDecimal.TEN), // 1/10 Double.MIN_VALUE
				new boolean[0],
				new short[0],
				new int[0],
				new long[0],
				new float[0],
				new double[0],
				Collections.EMPTY_LIST,
				Collections.EMPTY_SET,
				Collections.EMPTY_MAP,
				null,
				Collections.EMPTY_MAP);
	}

	/**
	 * @return
	 * not a property
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName()+"{"
				+ "\n  nullProp=" + nullProp
				+ ", \n  trueProp=" + trueProp
				+ ", \n  falseProp=" + falseProp
				+ ", \n  nanoIntProp=" + nanoIntProp
				+ ", \n  byteProp=" + byteProp
				+ ", \n  shortProp=" + shortProp
				+ ", \n  intProp=" + intProp
				+ ", \n  longProp=" + longProp
				+ ", \n  floatProp=" + floatProp
				+ ", \n  doubleProp=" + doubleProp
				+ ", \n  trueObjectProp=" + trueObjectProp
				+ ", \n  falseObjectProp=" + falseObjectProp
				+ ", \n  nanoIntObjectProp=" + nanoIntObjectProp
				+ ", \n  byteObjectProp=" + byteObjectProp
				+ ", \n  shortObjectProp=" + shortObjectProp
				+ ", \n  intObjectProp=" + intObjectProp
				+ ", \n  longObjectProp=" + longObjectProp
				+ ", \n  floatObjectProp=" + floatObjectProp
				+ ", \n  doubleObjectProp=" + doubleObjectProp
				+ ", \n  stringProp=" + stringProp
				+ ", \n  dataProp=" + Arrays.toString(dataProp)
				+ ", \n  numberProp=" + numberProp
				+ ", \n  boolArray=" + Arrays.toString(boolArray)
				+ ", \n  shortArray=" + Arrays.toString(shortArray)
				+ ", \n  intArray=" + Arrays.toString(intArray)
				+ ", \n  longArray=" + Arrays.toString(longArray)
				+ ", \n  floatArray=" + Arrays.toString(floatArray)
				+ ", \n  doubleArray=" + Arrays.toString(doubleArray)
				+ ", \n  stringList=" + stringList
				+ ", \n  intSet=" + intSet
				+ ", \n  stringIntMap=" + stringIntMap
				+ ", \n  nested=" + nested
				+ ", \n  nestedByString=" + nestedByString
				+ "\n";
	}


}
