/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.e2e;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Reader;
import org.enon.Writer;
import org.enon.exception.EnonException;
import org.enon.test.model.simple.Enon0All;
import org.enon.test.util.TestDataOutputStream;
import org.enon.tool.validate.EnonValidationException;
import org.enon.tool.validate.ValidationReader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class Enon0e2eIT {
	private static final Logger LOGGER = LoggerFactory.getLogger(Enon0e2eIT.class.getName());

	private final EnonConfig config = EnonConfig.defaults();

	public Enon0e2eIT() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testRoundTripSingle() {
		LOGGER.info("roundTripSingle");
		roundTrip(config, Enon0All.generateRandomInstance(true));
		roundTrip(config, Enon0All.generateNullInstance());
		roundTrip(config, Enon0All.generateMaxInstance());
		roundTrip(config, Enon0All.generateMinInstance());
	}

	@Test
	public void testRoundTripMultiple() {
		LOGGER.info("roundTripMultiple");
		for (int i = 0; i < 100; i++) {
			roundTrip(config, Enon0All.generateRandomInstance(true));
			roundTrip(config, Enon0All.generateNullInstance());
			roundTrip(config, Enon0All.generateMaxInstance());
			roundTrip(config, Enon0All.generateMinInstance());
		}
	}

	@Test
	public void testRoundTripMultipleConcurrent() {
		final int concurrency = 10;
		ExecutorService execService = Executors.newFixedThreadPool(concurrency);
		List<Future> futures = new ArrayList<>();
		for (int i = 0; i < concurrency * 5; i++) {
			futures.add(execService.submit(() -> {
				for (int j = 0; j < 100; j++) {
					roundTrip(config, Enon0All.generateRandomInstance(true));
					roundTrip(config, Enon0All.generateNullInstance());
					roundTrip(config, Enon0All.generateMaxInstance());
					roundTrip(config, Enon0All.generateMinInstance());
				}
			}));
		}
		try {
			for (Future future : futures) {
				future.get();
			}
		} catch (ExecutionException | InterruptedException x) {
			LOGGER.error(x.getMessage(), x);
			fail(x.getMessage());
		}
	}

	private void roundTrip(EnonConfig config, Object object) {

		try {
			TestDataOutputStream tdos = new TestDataOutputStream();

			Writer writer = new Writer(tdos.stream(), config);

			writer.write(object);
			writer.flush();

			List<EnonValidationException> errors = ValidationReader.create(tdos.dataInputStream(), EnumSet.noneOf(FeatureSet.class)).validate();
			errors.stream().forEach(error -> LOGGER.error(error.getMessage(), error));
			assertTrue(errors.isEmpty());

			Reader reader = new Reader(tdos.dataInputStream(), config);

			Object result = reader.read(object.getClass());

			tdos.close();

			if (!object.equals(result)) {
				fail("round trip failed for instance: "+object.toString());
			}
		} catch (IOException x) {
			throw new EnonException("Exception during roundTrip: ", x);
		}
	}
}
