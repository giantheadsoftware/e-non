/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.model.abstraction;

/**
 *
 * @author clininger $Id: $
 */
public abstract class AbstractZooAnimal implements ZooAnimal {
	private String bioClassName;
	private String bioFamilyName;
	private String commonName;
	private int age;

	public AbstractZooAnimal() {
	}

	public AbstractZooAnimal(String bioClassName, String biofamilyName, String commonName, int age) {
		this.bioClassName = bioClassName;
		this.bioFamilyName = biofamilyName;
		this.commonName = commonName;
		this.age = age;
	}

	@Override
	public String getBioClassName() {
		return bioClassName;
	}

	@Override
	public void setBioClassName(String bioClassName) {
		this.bioClassName = bioClassName;
	}

	@Override
	public String getBioFamilyName() {
		return bioFamilyName;
	}

	@Override
	public void setBioFamilyName(String biofamilyName) {
		this.bioFamilyName = biofamilyName;
	}

	@Override
	public String getCommonName() {
		return commonName;
	}

	@Override
	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public void setAge(int age) {
		this.age = age;
	}
}
