/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.model.simple;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.enon.element.NanoIntElement;
import org.enon.test.util.ValueGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This POJO has standard getter/setter methods. It also has some non-property methods (i.e. toString) and some get/set methods that are mismatched.
 *
 * @author clininger $Id: $
 */
public class Enon0All {
	private static final Logger LOGGER = LoggerFactory.getLogger(Enon0All.class.getName());
	
	private static final ValueGenerator dataGen = ValueGenerator.getInstance();

	// scalar
	private Object nullProp;
	private boolean trueProp;
	private boolean falseProp;
	private byte nanoIntProp;
	private int intProp;
	private double doubleProp;
	private Boolean trueObjectProp;
	private Boolean falseObjectProp;
	private Byte nanoIntObjectProp;
	private Integer intObjectProp;
	private Double doubleObjectProp;
	private String stringProp;
	private byte[] dataProp;
	private Number numberProp;

	// collections
	private List<String> stringList;
	private Set<Integer> intSet;
	private Map<String, Integer> stringIntMap;
	private Enon0All nested;
	private Map<String, Enon0All> nestedByString;

	public Enon0All() {
	}

	public Enon0All(
			byte nanoIntProp,
			int intProp,
			double doubleProp,
			Byte nanoIntObjectProp,
			Integer intObjectProp,
			Double doubleObjectProp,
			String stringProp,
			byte[] dataProp,
			Number numberProp,
			List<String> stringList,
			Set<Integer> intSet,
			Map<String, Integer> stringIntMap,
			Enon0All nested,
			Map<String, Enon0All> nestedByString) {
		this.nullProp = null;
		this.trueProp = true;
		this.falseProp = false;
		this.nanoIntProp = nanoIntProp;
		this.intProp = intProp;
		this.doubleProp = doubleProp;
		this.trueObjectProp = Boolean.TRUE;
		this.falseObjectProp = Boolean.FALSE;
		this.nanoIntObjectProp = nanoIntObjectProp;
		this.intObjectProp = intObjectProp;
		this.doubleObjectProp = doubleObjectProp;
		this.stringProp = stringProp;
		this.dataProp = dataProp;
		this.numberProp = numberProp;
		this.stringList = stringList;
		this.intSet = intSet;
		this.stringIntMap = stringIntMap;
		this.nested = nested;
		this.nestedByString = nestedByString;
	}

	public Object getNullProp() {
		return nullProp;
	}

	public void setNullProp(Object nullProp) {
		this.nullProp = nullProp;
	}

	public boolean isTrueProp() {
		return trueProp;
	}

	public void setTrueProp(boolean trueProp) {
		this.trueProp = trueProp;
	}

	public boolean isFalseProp() {
		return falseProp;
	}

	public void setFalseProp(boolean falseProp) {
		this.falseProp = falseProp;
	}

	public byte getNanoIntProp() {
		return nanoIntProp;
	}

	public void setNanoIntProp(byte nanoIntProp) {
		this.nanoIntProp = nanoIntProp;
	}

	public int getIntProp() {
		return intProp;
	}

	public void setIntProp(int intProp) {
		this.intProp = intProp;
	}

	public double getDoubleProp() {
		return doubleProp;
	}

	public void setDoubleProp(double doubleProp) {
		this.doubleProp = doubleProp;
	}

	public Boolean getTrueObjectProp() {
		return trueObjectProp;
	}

	public void setTrueObjectProp(Boolean trueObjectProp) {
		this.trueObjectProp = trueObjectProp;
	}

	public Boolean getFalseObjectProp() {
		return falseObjectProp;
	}

	public void setFalseObjectProp(Boolean falseObjectProp) {
		this.falseObjectProp = falseObjectProp;
	}

	public Byte getNanoIntObjectProp() {
		return nanoIntObjectProp;
	}

	public void setNanoIntObjectProp(Byte nanoIntObjectProp) {
		this.nanoIntObjectProp = nanoIntObjectProp;
	}

	public Integer getIntObjectProp() {
		return intObjectProp;
	}

	public void setIntObjectProp(Integer intObjectProp) {
		this.intObjectProp = intObjectProp;
	}

	public Double getDoubleObjectProp() {
		return doubleObjectProp;
	}

	public void setDoubleObjectProp(Double doubleObjectProp) {
		this.doubleObjectProp = doubleObjectProp;
	}

	public String getStringProp() {
		return stringProp;
	}

	public void setStringProp(String stringProp) {
		this.stringProp = stringProp;
	}

	public byte[] getDataProp() {
		return dataProp;
	}

	public void setDataProp(byte[] dataProp) {
		this.dataProp = dataProp;
	}

	public Number getNumberProp() {
		return numberProp;
	}

	public void setNumberProp(Number numberProp) {
		this.numberProp = numberProp;
	}

	public List<String> getStringList() {
		return stringList;
	}

	public void setStringList(List<String> stringList) {
		this.stringList = stringList;
	}

	public Set<Integer> getIntSet() {
		return intSet;
	}

	public void setIntSet(Set<Integer> intSet) {
		this.intSet = intSet;
	}

	public Map<String, Integer> getStringIntMap() {
		return stringIntMap;
	}

	public void setStringIntMap(Map<String, Integer> stringIntMap) {
		this.stringIntMap = stringIntMap;
	}

	public Enon0All getNested() {
		return nested;
	}

	public void setNested(Enon0All nested) {
		this.nested = nested;
	}

	public Map<String, Enon0All> getNestedByString() {
		return nestedByString;
	}

	public void setNestedByString(Map<String, Enon0All> nestedByString) {
		this.nestedByString = nestedByString;
	}

	@Override
	public boolean equals(Object o) {
		if (o.getClass() != this.getClass()) {
			return false;
		}
		Enon0All that = (Enon0All) o;
		return
				nullProp == null && that.nullProp == null
				&& trueObjectProp == that.trueProp
				&& falseProp == that.falseProp
				&& nanoIntProp == that.nanoIntProp
				&& intProp == that.intProp
				&& doubleProp == that.doubleProp
				&& equal("trueObjectProp", trueObjectProp, that.trueObjectProp)
				&& equal("falseObjectProp", falseObjectProp, that.falseObjectProp)
				&& equal("nanoIntObjectProp", nanoIntObjectProp, that.nanoIntObjectProp)
				&& equal("intObjectProp", intObjectProp, that.intObjectProp)
				&& equal("doubleObjectProp", doubleObjectProp, that.doubleObjectProp)
				&& equal("stringProp", stringProp, that.stringProp)
				&& Arrays.equals(dataProp, that.dataProp)
				&& numberEquals("numberProp", numberProp, that.numberProp)

	// collections
				&& listEquals("stringList", stringList, that.stringList)
				&& setEquals("intSet", intSet, that.intSet)
				&& mapEquals("stringIntMap", stringIntMap, that.stringIntMap)
				&& equal("nested", nested, that.nested)
				&& mapEquals("nestedByString", nestedByString, that.nestedByString);
	}

	private boolean equal(String label, Object o1, Object o2) {
		if (o1 == null) {
			return o2 == null;
		}
		if (o1.equals(o2)) {
			return true;
		}
		LOGGER.error(label + ": " + String.valueOf(o1)+ " != " + String.valueOf(o2));
		return false;
	}

	private boolean listEquals(String label, List l1, List l2) {
		if (l1 == null) {
			return l2 == null;
		}
		if (l2 == null) {
			return false;
		}
		if (l1.size() != l2.size()) {
			return false;
		}
		for (int i = 0; i < l1.size(); i++) {
			if (!equal(label+": entry", l1.get(i), l2.get(i))) {
				return false;
			}
		}
		return true;
	}

	private boolean setEquals(String label, Set s1, Set s2) {
		if (s1 == null) {
			return s2 == null;
		}
		if (s2 == null) {
			return false;
		}
		return s1.size() == s2.size() && s2.containsAll(s1);
	}

	private boolean mapEquals(String label, Map m1, Map m2) {
		if (m1 == null) {
			return m2 == null;
		}
		if (m2 == null) {
			return false;
		}
		if (m1.size() == m2.size() && m2.keySet().containsAll(m1.keySet())) {
			for (Object key : m1.keySet()) {
				if (!equal(label+": entry", m1.get(key), m2.get(key))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	private boolean numberEquals(String label, Number n1, Number n2) {
		if (n1 == null) {
			return n2 == null;
		}
		if (n2 == null) {
			return false;
		}
		if (n1.getClass() == n2.getClass()) {
			return equal(label, n1, n2);
		}
		return equal(label, normalizeNumber(n1), normalizeNumber(n2));
	}

	/**
	 * Attempt to resolve to a BigInteger or BigDecimal
	 * @param n Some non-null number
	 * @return BigInteger or BigDecimal
	 */
	private Number normalizeNumber(Number n) {
		Class<? extends Number> nClass = (Class<Number>) n.getClass();
		if (nClass == BigInteger.class) {
			return n;
		}
		if (nClass == BigDecimal.class) {
			// try to reduce to BigInteger if possible
			try {
				return ((BigDecimal)n).toBigIntegerExact();
			} catch (ArithmeticException x) {
				return n;
			}
		}
		if (nClass == Double.class || nClass == Float.class) {
			return normalizeNumber(BigDecimal.valueOf(n.doubleValue()));
		}
		return BigInteger.valueOf(n.longValue());
	}

	public static Enon0All generateRandomInstance(boolean nest) {
		List<String> stringList = new LinkedList<>();
		int count = dataGen.randomInt(10, 20);
		for (int i = 0; i < count; i++) {
			stringList.add(dataGen.randomString());
		}

		Set<Integer> intSet = new HashSet<>();
		count = dataGen.randomInt(10, 20);
		for (int i = 0; i < count; i++) {
			intSet.add(dataGen.randomInt(Integer.MIN_VALUE, Integer.MAX_VALUE));
		}

		Map<String, Integer> stringIntMap = new LinkedHashMap<>();
		count = dataGen.randomInt(10, 20);
		for (int i = 0; i < count; i++) {
			stringIntMap.put(dataGen.randomString(), dataGen.randomInt(Integer.MIN_VALUE, Integer.MAX_VALUE));
		}

		Map<String, Enon0All> nestedByString = new LinkedHashMap<>();
		if (nest) {
			count = dataGen.randomInt(10, 20);
			for (int i = 0; i < count; i++) {
				Enon0All value = generateRandomInstance(false);
				nestedByString.put(value.getStringProp(), value);
			}
		}

		return new Enon0All(
				(byte)dataGen.randomInt(NanoIntElement.MIN_VALUE, NanoIntElement.MAX_VALUE),
				dataGen.randomInt(Integer.MIN_VALUE, Integer.MAX_VALUE),
				dataGen.randomDouble(),
				(byte)dataGen.randomInt(NanoIntElement.MIN_VALUE, NanoIntElement.MAX_VALUE),
				dataGen.randomInt(Integer.MIN_VALUE, Integer.MAX_VALUE),
				dataGen.randomDouble(),
				dataGen.randomString(),
				dataGen.randomBytes(),
				dataGen.randomLargeNumber(),
				stringList,
				intSet,
				stringIntMap,
				nest ? generateRandomInstance(false) : null,
				nestedByString);
	}

	public static Enon0All generateNullInstance() {
		return new Enon0All(
				(byte)0,
				0,
				0.0,
				null,
				null,
				Double.NaN,
				null,
				new byte[0],
				null,
				null,
				null,
				null,
				null,
				null);
	}

	public static Enon0All generateMaxInstance() {
		return new Enon0All(
				(byte)NanoIntElement.MAX_VALUE,
				Integer.MAX_VALUE,
				Double.MAX_VALUE,
				(byte)NanoIntElement.MAX_VALUE,
				Integer.MAX_VALUE,
				Double.POSITIVE_INFINITY,
				null,  // what would be a max string???
				new byte[0], // what would be a max byte[]?
				null, // there is no max large number... we already test larger than Double.MAX_VALUE
				null, // how to max collections?
				null,
				null,
				null,
				null);
	}

	public static Enon0All generateMinInstance() {
		return new Enon0All(
				(byte)NanoIntElement.MIN_VALUE,
				Integer.MIN_VALUE,
				Double.MIN_VALUE,
				(byte)NanoIntElement.MIN_VALUE,
				Integer.MIN_VALUE,
				Double.NEGATIVE_INFINITY,
				"",
				new byte[0],
				BigDecimal.ZERO, // there is no min large number... we already test larger than Double.MAX_VALUE
				Collections.EMPTY_LIST,
				Collections.EMPTY_SET,
				Collections.EMPTY_MAP,
				null,
				Collections.EMPTY_MAP);
	}

	/**
	 * not a property
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName()+"{" + "nullProp=" + nullProp + ", trueProp=" + trueProp + ", falseProp=" + falseProp + ", nanoIntProp=" + nanoIntProp + ", intProp=" + intProp + ", doubleProp=" + doubleProp + ", trueObjectProp=" + trueObjectProp + ", falseObjectProp=" + falseObjectProp + ", nanoIntObjectProp=" + nanoIntObjectProp + ", intObjectProp=" + intObjectProp + ", doubleObjectProp=" + doubleObjectProp + ", stringProp=" + stringProp + ", dataProp=" + Arrays.toString(dataProp) + ", numberProp=" + numberProp + ", stringList=" + stringList + ", intSet=" + intSet + ", stringIntMap=" + stringIntMap + ", nested=" + nested + ", nestedByString=" + nestedByString + '}';
	}


}
