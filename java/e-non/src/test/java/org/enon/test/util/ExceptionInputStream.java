/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.util;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author clininger $Id: $
 */
public class ExceptionInputStream extends InputStream {
	public static final ExceptionInputStream INSTANCE = new ExceptionInputStream();

	private final Long failAtByte;
	private final InputStream stream;
	private final RuntimeException exception;
	private long bytesRead;
	
	/**
	 * Create an instance that will fail immediately on read access
	 */
	public ExceptionInputStream() {
		this(null);
	}

	/**
	 * Create an instance that will fail immediately on read access
	 * @param x The exception to throw
	 */
	public ExceptionInputStream(RuntimeException x) {
		failAtByte = null;
		stream = null;
		this.exception = x;
	}

	/**
	 * Cause the stream to fail (IOException) at a specific offset from the head of the stream
	 * @param stream Stream to defer to until the fail byte is set
	 * @param failAtByte offset into the stream where the failure should occur
	 */
	public ExceptionInputStream(InputStream stream, long failAtByte) {
		this(stream, 0, null);
	}

	/**
	 * Cause the stream to fail (IOException) at a specific offset from the head of the stream
	 * @param stream Stream to defer to until the fail byte is set
	 * @param failAtByte offset into the stream where the failure should occur
	 * @param x The exception to be thrown
	 */
	public ExceptionInputStream(InputStream stream, long failAtByte, RuntimeException x) {
		if (stream == null) {
			throw new IllegalArgumentException("stream is required when setting a 'failAtByte'");
		}
		this.failAtByte = failAtByte;
		this.stream = stream;
		this.exception = x;
	}
	
	@Override
	public int read() throws IOException {
		if (failAtByte == null) {
			if (exception != null) {
				throw exception;
			}
			throw new IOException("Expected by test");
		}
		if (bytesRead >= failAtByte) {
			if (exception != null) {
				throw exception;
			}
			throw new IOException("Expected by test");
		}
		int read = stream.read();
		if (read != -1) {
			bytesRead++;
		}
		return read;
	}

	@Override
	public int read(byte[] b) throws IOException {
		if (failAtByte == null) {
			if (exception != null) {
				throw exception;
			}
			throw new IOException("Expected by test");
		}
		if (b.length + bytesRead >= failAtByte) {
			if (exception != null) {
				throw exception;
			}
			throw new IOException("Expected by test");
		}
		int len = stream.read(b);
		if (len >= 0) {
			bytesRead += len;
		}
		return len;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (failAtByte == null) {
			if (exception != null) {
				throw exception;
			}
			throw new IOException("Expected by test");
		}
		if (b.length + bytesRead >= failAtByte) {
			if (exception != null) {
				throw exception;
			}
			throw new IOException("Expected by test");
		}
		int readLen = stream.read(b, off, len);
		if (readLen >= 0) {
			bytesRead += readLen;
		}
		return readLen;
	}
	
	@Override
	public long skip(long n) throws IOException {
		if (failAtByte == null) {
			if (exception != null) {
				throw exception;
			}
			throw new IOException("Expected by test");
		}
		if (n + bytesRead >= failAtByte) {
			if (exception != null) {
				throw exception;
			}
			throw new IOException("Expected by test");
		}
		long skipped = stream.skip(n);
		bytesRead += skipped;
		return skipped;
	}

	@Override
	public void close() throws IOException {
		if (stream != null) {
			stream.close();
		}
		if (exception != null) {
			throw exception;
		}
		throw new IOException("Expected by test");
	}

}
