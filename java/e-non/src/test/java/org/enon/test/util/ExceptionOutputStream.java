/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.util;

import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author clininger $Id: $
 */
public class ExceptionOutputStream extends OutputStream {
	public static final ExceptionOutputStream INSTANCE = new ExceptionOutputStream();

	@Override
	public void write(int i) throws IOException {
		throw new IOException("Expected by test");
	}

	@Override
	public void close() throws IOException {
		throw new IOException("Expected by test");
	}

}
