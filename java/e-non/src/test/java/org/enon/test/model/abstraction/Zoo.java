/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.model.abstraction;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author clininger $Id: $
 */
public class Zoo {
	private List<ZooAnimal> animals;

	public Zoo() {
	}

	public Zoo(List<ZooAnimal> animals) {
		this.animals = animals;
	}

	
	public List<ZooAnimal> getAnimals() {
		return animals;
	}

	public void setAnimals(List<ZooAnimal> animals) {
		this.animals = animals;
	}
	
	public static Zoo getInstance() {
		List<ZooAnimal> animals = new LinkedList<>();
		animals.add(new Cat("Leopard Cat", 3, "Spotted"));
		animals.add(new Other("Reptilia", "Alligatoridae", "Chinese Alligator", 14,	"Scary"));
		return new Zoo(animals);
	}
}
