/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.model.abstraction;

/**
 *
 * @author clininger $Id: $
 */
public class Cat extends AbstractZooAnimal {
	private String furColor;

	public Cat() {
	}

	public Cat(String commonName, int age, String furColor) {
		super("Mammalia", "Felidae", commonName, age);
		this.furColor = furColor;
	}

	/**
	 * "immutable" field 
	 */
	@Override
	public void setBioClassName(String bioClassName) {
	}
	
	/**
	 * "immutable" field 
	 */
	@Override
	public void setBioFamilyName(String name) {
	}

	public String getFurColor() {
		return furColor;
	}

	public void setFurColor(String furColor) {
		this.furColor = furColor;
	}
	

}
