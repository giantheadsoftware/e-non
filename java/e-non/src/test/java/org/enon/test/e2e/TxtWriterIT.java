/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.e2e;

import java.util.LinkedHashMap;
import java.util.Map;
import org.enon.test.model.simple.Enon0All;
import org.enon.txt.TxtWriter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TxtWriterIT {
	private static final Logger LOGGER = LoggerFactory.getLogger(TxtWriterIT.class.getName());

	public TxtWriterIT() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of write method, of class TxtWriter.
	 */
	@Test
	public void testWrite() {
		LOGGER.info("write");

		Object data = Enon0All.generateRandomInstance(true);

		TxtWriter txtWriter = TxtWriter.create(System.out);

		txtWriter.write(data);
	}

	@Test
	public void testWriteMax() {
		LOGGER.info("write");

		Object data = Enon0All.generateMaxInstance();

		TxtWriter txtWriter = TxtWriter.create(System.out);

		txtWriter.write(data);
	}

	@Test
	public void testWriteMin() {
		LOGGER.info("write");

		Object data = Enon0All.generateMinInstance();

		TxtWriter txtWriter = TxtWriter.create(System.out);

		txtWriter.write(data);
	}

	@Test
	public void testWriteNull() {
		LOGGER.info("write");

		Object data = Enon0All.generateNullInstance();

		TxtWriter txtWriter = TxtWriter.create(System.out);

		txtWriter.write(data);
	}

	@Test
	public void testLongStrings() {
		LOGGER.info("longStrings");
		TxtWriter txtWriter = TxtWriter.create(System.out);

		txtWriter.write(longStrings());
	}

	@Test
	public void testLongStringsMinified() {
		LOGGER.info("longStringsMinified");
		TxtWriter txtWriter = TxtWriter.create(System.out).minify();

		txtWriter.write(longStrings());
	}

	@Test
	public void testEnonX() {
		LOGGER.info("enon-X");

		Map<String, Object> data = new LinkedHashMap<>();

		data.put("character", 'C');
		data.put("byte", (byte)-45);
		data.put("float", 99.21549f);
		data.put("long", 54962132226541L);

		TxtWriter txtWriter = TxtWriter.create(System.out);
		txtWriter.write(data);
	}

	private Map<String, String> longStrings() {
		Map<String, String> strings = new LinkedHashMap<>();
		strings.put("single line", "123456789012345678901234567890123456789");

		strings.put("hard break", "123456789012345678901234567890123456789\n0123456789654321035498");

		strings.put("end with newline", "1234567890123456789012345678901234567890123456789654321035498\n");

		strings.put("soft break", "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");

		strings.put("exact length",     "123456789012345678901234567890123456789012345678901234567890123456789012345");

		strings.put("exact length + 1", "1234567890123456789012345678901234567890123456789012345678901234567890123456");

		strings.put("exact with newline", "123456789012345678901234567890123456789012345678901234567890123456789012345\n");

		strings.put("exact + 1, newline", "1234567890123456789012345678901234567890123456789012345678901234567890123456\n");

		strings.put("multi break", "123456789012345678901234567890123456789\n\n\n0123456789654321035498");

		strings.put("word break", "0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789");

		strings.put("hyphen break", "0123456789-0123456789-0123456789-0123456789-0123456789-0123456789-0123456789-0123456789-0123456789");

		strings.put("forced break", "0123456789-0123456789-0123456789-0123456789-0123456789-012345678901234567890123456789-0123456789");

		strings.put("forced break", "0123456789-0123456789-0123456789-0123456789-0123456789-012345678901234567890123456789-0123456789");

		strings.put("declaration", "When in the Course of human events, it becomes necessary for one people to dissolve the political bands "
				+ "which have connected them with another, and to assume among the powers of the earth, the separate and equal station to which "
				+ "the Laws of Nature and of Nature's God entitle them, a decent respect to the opinions of mankind requires that they should "
				+ "declare the causes which impel them to the separation.\n\n"
				+ "We hold these truths to be self-evident, that all men are created equal, that they are endowed by their Creator with certain "
				+ "unalienable Rights, that among these are Life, Liberty and the pursuit of Happiness.");
		return strings;
	}
}
