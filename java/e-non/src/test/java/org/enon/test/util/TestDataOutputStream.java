/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.test.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.enon.exception.EnonException;

/**
 *
 * @author clininger $Id: $
 */
public class TestDataOutputStream {

	private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
	private final DataOutputStream dos = new DataOutputStream(baos);
	private byte[] bytes;

	public TestDataOutputStream() {
	}

	public DataOutputStream stream() {
		return dos;
	}

	public byte[] bytes() {
		if (bytes == null) {
			bytes = baos.toByteArray();
		}
		return bytes;
	}
	
	public InputStream inputStream() {
		return new ByteArrayInputStream(bytes());
	}

	public DataInputStream dataInputStream() {
		return new DataInputStream(inputStream());
	}
	
	public BufferedReader reader() {
		return new BufferedReader(new InputStreamReader(inputStream()));
	}

	public void close() {
		try {
			dos.close();
			baos.close();
		} catch (IOException x) {
			throw new EnonException("Failed to close stream(s)");
		}
	}
}
