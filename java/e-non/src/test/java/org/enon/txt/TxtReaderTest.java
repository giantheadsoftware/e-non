/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.enon.EnonConfig;
import org.enon.element.ElementType;
import org.enon.exception.EnonException;
import org.enon.exception.EnonReadException;
import org.enon.test.util.ExceptionInputStream;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TxtReaderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(TxtReaderTest.class.getName());

	private TestDataOutputStream tdos;
	private OutputStreamWriter writer;

	public TxtReaderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		tdos = new TestDataOutputStream();
		writer = new OutputStreamWriter(tdos.stream());
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of create method, of class TxtReader.
	 */
	@Test
	public void testCreateInputStream() {
		LOGGER.info("createInputStream");

		InputStream in = new ExceptionInputStream();

		TxtReader reader = TxtReader.create(in);

		assertSame(in, reader.getReaderContext().getInputStream());
		assertSame(StandardCharsets.UTF_8, reader.getReaderContext().getCharset());
		assertEquals(TxtElementReaderFactory.class, reader.getReaderContext().getConfig().getElementReaderFactory().getClass());
	}

	/**
	 * Test of create method, of class TxtReader.
	 */
	@Test
	public void testCreateInputStreamCharset() {
		LOGGER.info("createInputStreamCharset");

		InputStream in = new ExceptionInputStream();

		TxtReader reader = TxtReader.create(in, StandardCharsets.UTF_16);

		assertSame(in, reader.getReaderContext().getInputStream());
		assertSame(StandardCharsets.UTF_16, reader.getReaderContext().getCharset());
		assertEquals(TxtElementReaderFactory.class, reader.getReaderContext().getConfig().getElementReaderFactory().getClass());
	}

	/**
	 * Test of create method, of class TxtReader.
	 */
	@Test
	public void testCreateInputConfigCharset() {
		LOGGER.info("createInputStreamCharset");

		InputStream in = new ExceptionInputStream();

		// set the proper TxtElementReaderFactory in the config
		EnonConfig config = new EnonConfig.Builder().elementReaderFactoryImpl(TxtElementReaderFactory.class).build();
		TxtReader reader = TxtReader.create(in, config, StandardCharsets.UTF_16);

		assertSame(in, reader.getReaderContext().getInputStream());
		assertSame(StandardCharsets.UTF_16, reader.getReaderContext().getCharset());
		assertEquals(TxtElementReaderFactory.class, reader.getReaderContext().getConfig().getElementReaderFactory().getClass());
	}

	/**
	 * Test of create method, of class TxtReader.
	 */
	@Test(/*expected = EnonException.class*/)
	public void testCreateBadConfigFail() {
		LOGGER.info("createInputStreamCharset");

		Exception x = assertThrows(EnonException.class, () -> {
			InputStream in = new ExceptionInputStream();

			// don't set the proper TxtElementReaderFactory in the config
			TxtReader.create(in, EnonConfig.defaults(), StandardCharsets.UTF_16);
		});
	}

	/**
	 * Test of validateProlog method, of class TxtReader.
	 */
	@Test
	public void testValidateProlog() throws IOException {
		LOGGER.info("validateProlog");

		writer.append(TxtElementType.PROLOG.prefix + '\n');
		writer.flush();

		TxtReader reader = TxtReader.create(tdos.inputStream());

		assertNull(reader.read(Object.class));
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testValidatePrologEodFail() throws IOException {
		LOGGER.info("validatePrologEodFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			// nothing written to the stream
			TxtReader reader = TxtReader.create(tdos.inputStream());

			reader.read(Object.class);
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testValidatePrologMissingFail() throws IOException {
		LOGGER.info("validatePrologMissingFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			// data written without prolog
			writer.append(TxtElementType.NULL.prefix + '\n');
			writer.flush();

			TxtReader reader = TxtReader.create(tdos.inputStream());

			reader.read(Object.class);
		});
	}

	@Test
	public void testRead() throws IOException {
		LOGGER.info("validateProlog");

		writer.append(TxtElementType.PROLOG.prefix + '\n');
		writer.append(TxtElementType.STRING.prefix + "Read a string\n");
		writer.flush();

		TxtReader reader = TxtReader.create(tdos.inputStream());

		Object result = reader.read(null);
		assertNotNull(result);
		assertSame(String.class, result.getClass());
		assertEquals("Read a string", result);
	}

}
