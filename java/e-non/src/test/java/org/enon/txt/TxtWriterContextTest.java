/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.enon.EnonConfig;
import org.enon.test.util.ExceptionOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TxtWriterContextTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(TxtWriterContextTest.class.getName());

	public TxtWriterContextTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getPrintWriter method, of class TxtWriterContext.
	 */
	@Test
	public void testGetPrintWriter() {
		LOGGER.info("getPrintWriter");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintWriter pw = new TxtWriterContext(baos).getPrintWriter();

		pw.write("hello");
		pw.flush();
		assertEquals("hello", baos.toString());
	}

	/**
	 * Test of getCharset method, of class TxtWriterContext.
	 */
	@Test
	public void testGetCharset() {
		LOGGER.info("getCharset");

		// default charset
		assertSame(StandardCharsets.UTF_8, new TxtWriterContext(new ExceptionOutputStream()).getCharset());
		assertSame(StandardCharsets.UTF_8, new TxtWriterContext(new ExceptionOutputStream(), EnonConfig.defaults()).getCharset());

		// assigned charset
		assertSame(StandardCharsets.US_ASCII, new TxtWriterContext(new ExceptionOutputStream(), StandardCharsets.US_ASCII).getCharset());
		assertSame(StandardCharsets.US_ASCII, new TxtWriterContext(new ExceptionOutputStream(), EnonConfig.defaults(), StandardCharsets.US_ASCII).getCharset());
	}

	/**
	 * Test of minify method, of class TxtWriterContext.
	 */
	@Test
	public void testMinify() {
		LOGGER.info("minify");

		TxtWriterContext ctx = new TxtWriterContext(new ExceptionOutputStream()).minify();

		assertNull(ctx.getBlockWidth());
		assertEquals(0, ctx.getTabWidth());
		assertFalse(ctx.isPadded());
	}

	/**
	 * Test of tabWidth method, of class TxtWriterContext.
	 */
	@Test
	public void testTabWidth() {
		LOGGER.info("tabWidth");

		TxtWriterContext ctx = new TxtWriterContext(new ExceptionOutputStream()).tabWidth(9);
		assertEquals(9, ctx.getTabWidth());
	}

	/**
	 * Test of blockWidth method, of class TxtWriterContext.
	 */
	@Test
	public void testBlockWidth() {
		LOGGER.info("blockWidth");

		TxtWriterContext ctx = new TxtWriterContext(new ExceptionOutputStream()).blockWidth(99);
		assertEquals(99, (int)ctx.getBlockWidth());
	}

	/**
	 * Test of padded method, of class TxtWriterContext.
	 */
	@Test
	public void testPadded() {
		LOGGER.info("padded");

		TxtWriterContext ctx = new TxtWriterContext(new ExceptionOutputStream()).padded(false);
		assertFalse(ctx.isPadded());
	}

	/**
	 * Test of getIndentLevel method, of class TxtWriterContext.
	 */
	@Test
	public void testGetIndentLevel() {
		LOGGER.info("getIndentLevel");

		TxtWriterContext ctx = new TxtWriterContext(new ExceptionOutputStream());

		assertEquals(0, ctx.getIndentLevel());

		ctx.containerBegin();
		assertEquals(1, ctx.getIndentLevel());

		ctx.containerEnd();
		assertEquals(0, ctx.getIndentLevel());
	}

}
