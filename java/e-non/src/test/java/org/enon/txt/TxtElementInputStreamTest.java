/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.enon.exception.EnonException;
import org.enon.exception.EnonReadException;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TxtElementInputStreamTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(TxtElementInputStreamTest.class.getName());

	private TestDataOutputStream tdos;
	private OutputStreamWriter writer;

	public TxtElementInputStreamTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		tdos = new TestDataOutputStream();
		writer = new OutputStreamWriter(tdos.stream());
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of readTxtElement method, of class BufferedLineReader.
	 *
	 * @throws java.io.IOException
	 */
	@Test
	public void testRead() throws IOException {
		LOGGER.info("readTxtElement");

		// generate a set of valid single-line elements in eNON-txt format
		String[] lines = new String[]{
			TxtElementType.BLOB.prefix + " fake blob data",
			TxtElementType.BYTE.prefix + " 13",
			TxtElementType.DOUBLE.prefix + " 13.321",
			TxtElementType.FALSE.prefix,
			TxtElementType.FLOAT.prefix + "\t331.554",
			TxtElementType.INFINITY_NEG.prefix,
			TxtElementType.INFINITY_POS.prefix,
			TxtElementType.INT.prefix + "   99999",
			TxtElementType.LONG.prefix + "-9999999  ",
			TxtElementType.NAN.prefix,
			TxtElementType.NULL.prefix,
			TxtElementType.NUMBER.prefix + "225.884E134",
			TxtElementType.PROLOG.prefix,
			TxtElementType.SHORT.prefix + "15004",
			TxtElementType.STRING.prefix + "hello!",
			TxtElementType.STRING.prefix + "c", // single char
			TxtElementType.STRING.prefix + "\u0e00", // single char, unicode
			// all the same strings w/o unicode should also work as STRING_UNICODE
			TxtElementType.STRING_UNICODE.prefix + "hello!",
			TxtElementType.STRING_UNICODE.prefix + "c", // single char
			TxtElementType.STRING_UNICODE.prefix + "\u0e00", // single char, unicode
			TxtElementType.STRING_UNICODE.prefix + "c\u0e02",
			TxtElementType.TRUE.prefix,
			TxtElementType.LIST.prefix + TxtElementType.LIST_END.prefix,
			TxtElementType.MAP.prefix + TxtElementType.MAP_END.prefix
		};

		// write the elements to stream
		Arrays.stream(lines).forEach(line -> {
			try {
				writer.write(line + '\n');
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
		});
		writer.flush();

		TxtElementInputStream lineReader = new TxtElementInputStream(tdos.inputStream(), StandardCharsets.UTF_8);

		// validate lines
		for (String line : lines) {
			if (line.isEmpty()) {
				continue;  // should be skipping blank lines
			}
			TxtElement txtElement = lineReader.read();
			assertNotNull(txtElement);
			// verify that we read th eintended prefix
			assertTrue(line.startsWith(txtElement.getType().prefix));
			// verify that we read the line correctly
			assertEquals(line, txtElement.getType().prefix + txtElement.getStrValue());
		}

		// assert end of stream reached
		assertNull(lineReader.read());
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReadBadPrefixFail() throws IOException {
		LOGGER.info("readBadPrefixFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			writer.write("q is not a valid prefix\n");
			writer.flush();

			new TxtElementInputStream(tdos.inputStream(), StandardCharsets.UTF_8).read();
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReadUnexpectedContinuationFail() throws IOException {
		LOGGER.info("readUnexpectedContinuationFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			writer.write("> should not have a free-standing continutation\n");
			writer.flush();

			new TxtElementInputStream(tdos.inputStream(), StandardCharsets.UTF_8).read();
		});
	}

	@Test
	public void testContinuation() throws IOException {
		LOGGER.info("continuation");

		String[] lines = new String[]{
			TxtElementType.BLOB.prefix + " fake blob data\n",
			TxtContinuationType.BREAK.prefix + " more fake blob data \n",
			TxtContinuationType.SOFT_BREAK.prefix + " with a soft break also \n",
			TxtElementType.STRING.prefix + "hello!\n",
			TxtContinuationType.BREAK.prefix + " more string data \n",
			TxtContinuationType.SOFT_BREAK.prefix + " with a soft break also \n",
			TxtElementType.STRING_UNICODE.prefix + "c\u0e02\n",};

		Arrays.stream(lines).forEach(line -> {
			try {
				writer.write("   " + line); // add some white space -- it should be trimmed
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
		});
		writer.flush();
		TxtElementInputStream lineReader = new TxtElementInputStream(tdos.inputStream(), StandardCharsets.UTF_8);

		// validate lines
		for (int i = 0; i < lines.length;) {
			String line = lines[i++];
			if (line.isEmpty()) {
				continue;  // should be skipping blank lines
			}

			TxtElement txtElement = lineReader.read();
			assertNotNull(txtElement);
			assertTrue(line.startsWith(txtElement.getType().prefix));
			assertEquals(line, txtElement.getType().prefix + txtElement.getStrValue() + "\n");
			// make sure the reader picked up the continuation lines correctly
			for (TxtElement.ContinuationElement cont : txtElement.getContinuationElements()) {
				line = lines[i++];
				assertTrue(line.charAt(0) == cont.type.prefix);
				assertEquals(line, cont.type.prefix + cont.value + "\n");
			}
		}

		// assert end of stream reached
		assertNull(lineReader.read());
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testMissingArraySubtypeFail() throws IOException {
		LOGGER.info("testMissingArraySubtypeFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			writer.write(TxtElementType.ARRAY.prefix + "\n");
			writer.flush();

			TxtElementInputStream lineReader = new TxtElementInputStream(tdos.inputStream(), StandardCharsets.UTF_8);
			lineReader.read();
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testInvalidArraySubtypeFail1() throws IOException {
		LOGGER.info("invalidArraySubtypeFail1");

		Exception x = assertThrows(EnonReadException.class, () -> {
			// For the array subtype, use a char codepoint that is beyond the range of a single byte
			writer.write(TxtElementType.ARRAY.prefix + String.valueOf(Character.toChars(256)) + "\n");
			writer.flush();

			TxtElementInputStream lineReader = new TxtElementInputStream(tdos.inputStream(), StandardCharsets.UTF_8);
			lineReader.read();
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testInvalidArraySubtypeFail2() throws IOException {
		LOGGER.info("invalidArraySubtypeFail2");

		Exception x = assertThrows(EnonReadException.class, () -> {
			// For the array subtype, use a char 'x' that is not a valid subtype at all
			writer.write(TxtElementType.ARRAY.prefix + "x\n");
			writer.flush();

			TxtElementInputStream lineReader = new TxtElementInputStream(tdos.inputStream(), StandardCharsets.UTF_8);
			lineReader.read();
		});
	}
}
