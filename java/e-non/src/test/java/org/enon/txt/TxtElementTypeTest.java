/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.util.Arrays;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TxtElementTypeTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(TxtElementTypeTest.class.getName());

	public TxtElementTypeTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of forPrefix method, of class TxtElement.
	 */
	@Test
	public void testForPrefix() {
		LOGGER.info("forPrefix");
		for (TxtElementType te : TxtElementType.values()) {
			assertSame(te, TxtElementType.forPrefix(te.prefix));
		}
	}

	/**
	 * Test of forElementType method, of class TxtElement.
	 */
	@Test
	public void testForElementType() {
		LOGGER.info("forElementType");
		for (TxtElementType te : TxtElementType.values()) {
			if (te.elementType == null) {
				assertNull(TxtElementType.forElementType(te.elementType));
			} else {
				assertSame(te, TxtElementType.forElementType(te.elementType));
			}
		}
	}

	/**
	 * Test of forLine method, of class TxtElement.
	 */
	@Test
	public void testForLine() {
		LOGGER.info("forLine");
		// empty line
		String line = "";
		assertNull(TxtElementType.forLine(line));

		// long blank line
		char[] chars = new char[10000];
		Arrays.fill(chars, ' ');
		assertNull(TxtElementType.forLine(String.valueOf(chars)));

		// valid lines
		for (TxtElementType te : TxtElementType.values()) {
			assertSame(te, TxtElementType.forLine(te.prefix)); // only the prefix on the line
			assertSame(te, TxtElementType.forLine(te.prefix)); // whitespace before the prefix
			assertSame(te, TxtElementType.forLine(te.prefix+"\t   ")); // whitespace before & after the prefix
			assertSame(te, TxtElementType.forLine(te.prefix+"gibberish")); // whitespace before the prefix, text after
			assertSame(te, TxtElementType.forLine(te.prefix+" gibberish")); // whitespace before the prefix, text after
		}
	}

}
