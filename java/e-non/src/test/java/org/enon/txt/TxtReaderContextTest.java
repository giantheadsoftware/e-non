/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import org.enon.EnonConfig;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.StringElement;
import org.enon.exception.EnonException;
import org.enon.test.util.ExceptionInputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TxtReaderContextTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(TxtReaderContextTest.class.getName());

	public TxtReaderContextTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of create method, of class TxtReaderContext.
	 */
	@Test(/*expected = EnonException.class*/)
	public void testCreateBadConfigFail() {
		LOGGER.info("createBadConfigFail");

		Exception x = assertThrows(EnonException.class, () -> {
			TxtReaderContext instance = TxtReaderContext.create(new ExceptionInputStream(), EnonConfig.defaults(), StandardCharsets.UTF_16);
		});
	}

	/**
	 * Test of getCharset method, of class TxtReaderContext.
	 */
	@Test
	public void testGetCharset() {
		LOGGER.info("getCharset");
		TxtReaderContext instance = TxtReaderContext.create(new ExceptionInputStream());
		assertSame(StandardCharsets.UTF_8, instance.getCharset());

		instance = TxtReaderContext.create(new ExceptionInputStream(), StandardCharsets.US_ASCII);
		assertSame(StandardCharsets.US_ASCII, instance.getCharset());

		// set up a config using our dummy factory
		EnonConfig config = new EnonConfig.Builder().elementReaderFactoryImpl(DummyElementReaderFactory.class).build();
		instance = TxtReaderContext.create(new ExceptionInputStream(), config, StandardCharsets.UTF_16);
		assertSame(StandardCharsets.UTF_16, instance.getCharset());
	}

	/**
	 * Test of parseNextElement method, of class TxtReaderContext.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testParseNextElement() throws Exception {
		LOGGER.info("parseNextElement");

		// set up a config using our dummy factory
		EnonConfig config = new EnonConfig.Builder().elementReaderFactoryImpl(DummyElementReaderFactory.class).build();

		Element e = TxtReaderContext.create(new ExceptionInputStream(), config).parseNextElement();

		assertSame(ElementType.STRING_ELEMENT, e.getType());
		assertEquals("dummy reader!", e.getValue());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static class DummyElementReaderFactory extends TxtElementReaderFactory {

		public DummyElementReaderFactory(EnonConfig config) {
			super(config);
		}

		@Override
		public TxtElementReader reader(InputStream in) {
			return new TxtElementReader() {
				@Override
				public Element read(TxtReaderContext ctx) throws IOException {
					return new StringElement("dummy reader!");
				}

			};
		}

	}
}
