/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import org.enon.EnonConfig;
import org.enon.test.util.ExceptionOutputStream;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TxtWriterTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(TxtWriterTest.class.getName());

	private TxtWriterContext ctx;

	public TxtWriterTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		ctx = new TxtWriterContext(new ExceptionOutputStream());
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of minify method, of class TxtWriterContext.
	 */
	@Test
	public void testMinify() {
		LOGGER.info("minify");

		// settings should propagate to the ctx
		TxtWriter writer = new TestTxtWriter(ctx).minify();

		assertNull(ctx.getBlockWidth());
		assertEquals(0, ctx.getTabWidth());
		assertFalse(ctx.isPadded());
		assertNull(writer.getBlockWidth());
		assertEquals(0, writer.getTabWidth());
		assertFalse(writer.isPadded());
	}

	/**
	 * Test of tabWidth method, of class TxtWriterContext.
	 */
	@Test
	public void testTabWidth() {
		LOGGER.info("tabWidth");

		// settings should propagate to the ctx
		TxtWriter writer = new TestTxtWriter(ctx).tabWidth(9);
		assertEquals(9, ctx.getTabWidth());
		assertEquals(9, writer.getTabWidth());
	}

	/**
	 * Test of blockWidth method, of class TxtWriterContext.
	 */
	@Test
	public void testBlockWidth() {
		LOGGER.info("blockWidth");

		// settings should propagate to the ctx
		TxtWriter writer = new TestTxtWriter(ctx).blockWidth(99);
		assertEquals(99, (int)ctx.getBlockWidth());
		assertEquals(99, (int)writer.getBlockWidth());
	}

	/**
	 * Test of padded method, of class TxtWriterContext.
	 */
	@Test
	public void testPadded() {
		LOGGER.info("padded");

		// settings should propagate to the ctx
		TxtWriter writer = new TestTxtWriter(ctx).padded(false);
		assertFalse(ctx.isPadded());
		assertFalse(writer.isPadded());
	}


	@Test
	public void testCreateOut() {
		LOGGER.info("createOut");

		TestDataOutputStream tdos = new TestDataOutputStream();
		TxtWriter writer = TxtWriter.create(tdos.stream());

		writer.write("hello");
		writer.flush();
		assertEquals(TxtElementType.PROLOG.prefix+'\n'+TxtElementType.STRING.prefix+"hello\n", new String(tdos.bytes(), StandardCharsets.UTF_8));
	}


	@Test
	public void testCreateOutCharset() {
		LOGGER.info("createOutCharset");

		TestDataOutputStream tdos = new TestDataOutputStream();
		TxtWriter writer = TxtWriter.create(tdos.stream(), StandardCharsets.US_ASCII);

		writer.write("hello");
		writer.flush();
		assertEquals(TxtElementType.PROLOG.prefix+'\n'+TxtElementType.STRING.prefix+"hello\n", new String(tdos.bytes(), StandardCharsets.US_ASCII));
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private class TestTxtWriter extends TxtWriter {

		public TestTxtWriter(TxtWriterContext ctx) {
			super(ctx);
		}

	}
}
