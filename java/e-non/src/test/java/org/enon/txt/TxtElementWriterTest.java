/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.regex.Pattern;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.element.array.ByteArrayElement;
import org.enon.element.ByteElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.Element;
import org.enon.element.ElementContainer;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.ElementType;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.ListElement;
import org.enon.element.MapElement;
import org.enon.element.MapEntryElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.ShortElement;
import org.enon.element.StringElement;
import org.enon.element.array.ArrayElement;
import org.enon.element.array.DoubleArrayElement;
import org.enon.element.array.LongArrayElement;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TxtElementWriterTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(TxtElementWriterTest.class.getName());

	private static final String WORD_WRAP = "This is a very long string that has enough spaces to allow a natural word break at the specified "
			+ "block witdth, assuming a block width has been set and is less than the length of this string.";
	private static final String HYPHEN_WRAP = "This-is-another-very-long-string-that-uses-hyphens-instead-of-spaces-to-allow-a-"
			+ "natural-word-break-at-the-specified-block-witdth--assuming-a-block-width-has-been-set-and-is-less-than-the-length-of-this-string.";
	private static final String FORCE_WRAP = "This_is_another_very_long_string_that_uses_underscores_to_prevent_word_wrapping__The_line_will_be_broken_"
			+ "arbitrarily_at_the_specified_block_witdth__assuming_a_block_width_has_been_set_and_is_less_than_the_length_of_this_string.";
	private static final String HARD_BREAK = "When in the Course of human events, it becomes necessary for one people to dissolve the political bands "
				+ "which have connected them with another, and to assume among the powers of the earth, the separate and equal station to which "
				+ "the Laws of Nature and of Nature's God entitle them, a decent respect to the opinions of mankind requires that they should "
				+ "declare the causes which impel them to the separation.\n\n"
				+ "We hold these truths to be self-evident, that all men are created equal, that they are endowed by their Creator with certain "
				+ "unalienable Rights, that among these are Life, Liberty and the pursuit of Happiness.";

	private EnonConfig txtConfig;
	private TestDataOutputStream  tdos;

	public TxtElementWriterTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		txtConfig = new EnonConfig.Builder(EnumSet.of(FeatureSet.X)).elementWriterFactoryImpl(TxtElementWriterFactory.class).build();
		tdos = new TestDataOutputStream();
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of write method, of class TxtElementWriter.
	 * @throws java.io.IOException
	 */
	@Test
	public void testWrite() throws IOException {
		LOGGER.info("write");

		TxtWriterContext txtCtx = new TxtWriterContext(tdos.stream(), txtConfig);

		List<Element> elements = writeScalarValues(txtCtx, txtCtx.getRoot());

		// get a reader for the data
		BufferedReader reader = tdos.reader();

		validateScalarElements(txtCtx, reader, elements);

		// verify end of stream
		assertNull(reader.readLine());
	}

	@Test
	public void testWriteMinify() throws IOException {
		LOGGER.info("writeMinify");

		TxtWriterContext txtCtx = new TxtWriterContext(tdos.stream(), txtConfig).minify();

		List<Element> elements = writeScalarValues(txtCtx, txtCtx.getRoot());

		// get a reader for the data
		BufferedReader reader = tdos.reader();

		validateScalarElements(txtCtx, reader, elements);

		// verify end of stream
		assertNull(reader.readLine());
	}

	/**
	 * Validate expected conditions in the eNON-txt stream for scalar elements.
	 * Handles any number of scalar elements.  Does not handle List or Map.  Tests
	 * for proper indentation, padding, and continuation lines for STRING and BLOB.
	 * @param txtCtx The context used to write the stream.
	 * @param reader BufferedReader of the stream
	 * @param elements The elements that were written to the stream.
	 * @throws IOException
	 */
	private void validateScalarElements(TxtWriterContext txtCtx, BufferedReader reader, List<Element> elements) throws IOException {
		String pad = txtCtx.isPadded() ? " " : "";
		String nextLine = null;
		String line;
		int nestLevel = 0;

		for (Element e : elements) {
			// read the next line unless it was already read
			if (nextLine == null) {
				line = reader.readLine();
			} else {
				line = nextLine;
				nextLine = null;
			}

			// test indentation level
			int indentation = countWhiteChars(line);
			assertEquals(txtCtx.getTabWidth() * nestLevel, indentation);
			// trim indentation
			line = line.substring(indentation);

			// get the txt type associated with the element type
			TxtElementType txtType = TxtElementType.forLine(line);
			assertNotNull(txtType,"Could not find a TxtElementType for "+e.getType()+" on line: "+line);

			switch (e.getType()) {
				case STRING_ELEMENT:
					line = line.substring(TxtElementType.forLine(line).prefix.length());
					nextLine = validateString((String)e.getValue(), reader, line, txtCtx);
					break;

				case BLOB_ELEMENT:
					nextLine = validateBlob((byte[])e.getValue(), reader, line.substring(1), txtCtx);
					break;

				case BYTE_ELEMENT:
				case SHORT_ELEMENT:
				case LONG_ELEMENT:
				case INT_ELEMENT:
				case FLOAT_ELEMENT:
				case DOUBLE_ELEMENT:
				case NUMBER_ELEMENT:
					assertEquals(txtType.prefix+pad+e.getValue(), line);
					break;

				case NANO_INT_ELEMENT:
					assertEquals(TxtElementType.BYTE.prefix+pad+e.getValue(), line);
					break;

				case INFINITY_NEG_ELEMENT:
				case INFINITY_POS_ELEMENT:
				case NAN_ELEMENT:
				case NULL_ELEMENT:
				case TRUE_ELEMENT:
				case FALSE_ELEMENT:
					assertEquals(txtType.prefix, line);
					break;

				default:
					fail("Unhandled element: "+e.getType());
			}
		}

	}

	/**
	 * Accumulate any continuation lines after the initial string element.  Validate the total accumulated string against the
	 * original string.
	 * @param expected Original string
	 * @param reader to read the continuation lines.  The initial string line should have been read already
	 * @param accumulatedValue The value of the initial string line
	 * @return The next line from the reader that is not a continuation line.
	 * @throws IOException if the reader fails
	 */
	private String validateString(String expected, BufferedReader reader, String accumulatedValue, TxtWriterContext txtCtx) throws IOException {
		if (txtCtx.getBlockWidth() != null) {
			assertTrue(accumulatedValue.length() <= txtCtx.getBlockWidth());
		}
		String nextLine = reader.readLine();
		String continuation = getContinuation(nextLine);
		while (continuation != null) {
			if (txtCtx.getBlockWidth() != null) {
				assertTrue(continuation.length() <= txtCtx.getBlockWidth(), "Continuation string length should be less than block width");
			} else if (!accumulatedValue.endsWith(TxtElementWriter.CR_UNICODE_ESCAPE) && continuation.charAt(0) != '\n') {
				fail("When block width is unset there should be no soft continuation lines for strings.");
			}
			accumulatedValue += continuation;
			nextLine = reader.readLine();
			continuation = getContinuation(nextLine);
		}
		assertEquals(expected.replaceAll("\r", "\\\\uD;"), accumulatedValue);
		return nextLine;
	}

	/**
	 * Accumulate any continuation lines after the initial string element.  Validate the total accumulated string against the
	 * original string.
	 * @param expected Original string
	 * @param reader to read the continuation lines.  The initial string line should have been read already
	 * @param accumulatedValue The value of the initial string line
	 * @return The next line from the reader that is not a continuation line.
	 * @throws IOException if the reader fails
	 */
	private String validateBlob(byte[] expected, BufferedReader reader, String accumulatedValue, TxtWriterContext txtCtx) throws IOException {
		if (txtCtx.isPadded()) {
			assertEquals(' ', accumulatedValue.charAt(0));
		}
		accumulatedValue = accumulatedValue.trim();
		if (txtCtx.getBlockWidth() != null) {
			assertTrue(accumulatedValue.length() <= txtCtx.getBlockWidth());
		}
		// loop through any continuation lines, add them to the accumulated value
		String nextLine = reader.readLine();
		String continuation = getContinuation(nextLine);
		while (continuation != null) {
			assertNotNull(txtCtx.getBlockWidth(), "When block width is unset there should be no continuation lines for BLOBs.");
//			assertEquals("Soft breaks are not legal for BLOB continuation.", '\n', continuation.charAt(0));
			accumulatedValue += continuation.trim();
			assertTrue(continuation.trim().length() <= txtCtx.getBlockWidth(), "Continuation Base64 length should be less than block width");
			nextLine = reader.readLine();
			continuation = getContinuation(nextLine);
		}
		// finally, verify the content decoded from Base64 matches the original content
		Base64.Decoder decoder = Base64.getDecoder();
		byte[] result = decoder.decode(accumulatedValue);
		assertArrayEquals(expected, result);

		return nextLine;
	}

	/**
	 * If the line is a continuation, return the continuation text.  If the continuation is a hard break,
	 * prepend a newline to the result.
	 * @param line The line to test as a continuation.
	 * @return Continuation text value, or null if it is not a continuation.
	 */
	private String getContinuation(String line) {
		TxtContinuationType cont = TxtContinuationType.forLine(line.trim());
		if (cont != null) {
			switch (cont) {
				case BREAK:
					// add a newline
					return "\n" + line.substring(line.indexOf(cont.prefix) + 1);
				case SOFT_BREAK:
					// don't add a newline
					return line.substring(line.indexOf(cont.prefix) + 1);
			}
		}
		return null;
	}

	@Test
	public void testStringSplit() throws IOException {
		LOGGER.info("stringSplit");

		TxtWriterContext txtCtx = new TxtWriterContext(tdos.stream(), txtConfig).blockWidth(76);

		ListIterator<String> strings = writeLongStrings(txtCtx).listIterator();

		// get a reader for the data
		BufferedReader reader = tdos.reader();

		// test the wrapped string
		String string = strings.next();
		String line = reader.readLine();
		assertEquals((char)ElementType.STRING_ELEMENT.prefix, line.charAt(0));
		String start = line.substring(1, line.length());
		assertTrue(start.length() <= txtCtx.getBlockWidth());
		assertTrue(string.startsWith(start));

		// read the continuation line
		line = reader.readLine();
		assertEquals(TxtContinuationType.SOFT_BREAK.prefix, line.charAt(0));
		String all = start + line.substring(1);
		assertEquals(string, all);

		// test the hyphen-wrapped string
		string = strings.next();
		line = reader.readLine();
		assertEquals((char)ElementType.STRING_ELEMENT.prefix, line.charAt(0));
		start = line.substring(1);
		assertTrue(start.length() <= txtCtx.getBlockWidth());
		assertTrue(string.startsWith(start));
		line = reader.readLine();
		assertEquals(TxtContinuationType.SOFT_BREAK.prefix, line.charAt(0));
		all = start + line.substring(1);
		assertEquals(string, all);

		// test the hard break string
		string = strings.next();
		int wrapIndex = string.indexOf('\n');
		assertEquals((char)ElementType.STRING_ELEMENT.prefix+string.substring(0, wrapIndex), reader.readLine());
		assertEquals(TxtContinuationType.BREAK.prefix+string.substring(wrapIndex+1), reader.readLine());

		// test the newline-at-end string
		string = strings.next();
		assertEquals((char)ElementType.STRING_ELEMENT.prefix+string.substring(0, string.indexOf('\n')), reader.readLine());
		assertEquals(String.valueOf(TxtContinuationType.BREAK.prefix), reader.readLine());

		// test the slash-newline-at-end string
		string = strings.next();
		line = reader.readLine();
		assertEquals((char)ElementType.STRING_ELEMENT.prefix, line.charAt(0));
		assertTrue(line.endsWith(String.valueOf(TxtContinuationType.SOFT_BREAK.prefix)));
		start = line.substring(1);
		assertEquals(string.trim(), start);
		assertEquals(String.valueOf(TxtContinuationType.BREAK.prefix), reader.readLine());

		// test the forced break string
		line = reader.readLine();
		strings.next();
		assertEquals((char)ElementType.STRING_ELEMENT.prefix, line.charAt(0));
		assertEquals((int)txtCtx.getBlockWidth(), line.length());
		line = reader.readLine();
		assertEquals(TxtContinuationType.SOFT_BREAK.prefix, line.charAt(0));

		// should be at end of stream
		assertFalse(strings.hasNext());
		assertNull(reader.readLine());
	}

	@Test
	public void testNoStringSplit() throws IOException {
		LOGGER.info("noStringSplit");

		// set the block width to null to disable line wrapping
		TxtWriterContext txtCtx = new TxtWriterContext(tdos.stream(), txtConfig).blockWidth(null);

		ListIterator<String> strings = writeLongStrings(txtCtx).listIterator();

		// get a reader for the data
		BufferedReader reader = tdos.reader();

		// there should be no soft-breaks in these results
		// lines will split only where they are naturally split by hard newlines in the source text.
		// in the special case where a backslash is immediately follwed by a newline, an extra \ is appended to the line before the newline.
		while (strings.hasNext()) {
			String text = strings.next();
			String[] lines = text.split("\n");
			for (int i = 0; i  <lines.length; i++) {
				char prefix = i == 0 ? (char)ElementType.STRING_ELEMENT.prefix : TxtContinuationType.BREAK.prefix;
				String line = reader.readLine();
				assertEquals(prefix + lines[i], line);
			}
			// String.split won't return a final token if the string ends with the delimiter... so check this manually
			if (text.endsWith("\n")) {
				assertEquals(String.valueOf(TxtContinuationType.BREAK.prefix), reader.readLine());
			}
		}

		// should be at end of stream
		assertFalse(strings.hasNext());
		assertNull(reader.readLine());
	}

	@Test
	public void testWriteList() throws IOException {
		LOGGER.info("writeList");

		TxtWriterContext txtCtx = new TxtWriterContext(tdos.stream());

		List<Element> elements = new LinkedList<>();
		elements.add(new StringElement("hello"));
		elements.add(new StringElement(HARD_BREAK));

		ListElement le = new ListElement(elements).inContainer(txtCtx.getRoot());

		txtCtx.write(le);
		txtCtx.flush();

		BufferedReader reader = tdos.reader();
		validateContainerElement(le, reader, txtCtx);

		assertNull(reader.readLine());
	}
	
	@Test
	public void testWriteArray() throws IOException {
		LOGGER.info("writeArray");
		
		TxtWriterContext txtCtx = new TxtWriterContext(tdos.stream());

		List<ArrayElement> elements = new LinkedList<>();
		// empty array
		elements.add(new DoubleArrayElement(new double[]{}));
		// single entry
		elements.add(new DoubleArrayElement(new double[]{987654.123456}));
		// should wrap lines
		elements.add(new LongArrayElement(new long[]{
			987654L, 123456L, 111111111L, 22222222222L, 333333333333L, 44444444444444L, 5555555555555L, 666666666666666L, 77777777777777L, 88888888888888L,
			99999999999999L, 100000000000000L
		}));

		for (ArrayElement element : elements) {
			txtCtx.write(element.inContainer(txtCtx.getRoot()));
		}
		txtCtx.flush();

		BufferedReader reader = tdos.reader();
		int i = 0;
		String line = reader.readLine();
		while (line != null) {
			assertTrue(line.length() <= txtCtx.getBlockWidth());
			String linePattern = "";
			switch (i++) {
				case 0: // validate empty array
					linePattern = "\\"+TxtElementType.ARRAY.prefix+(char)ArrayElementSubtype.DOUBLE_SUB_ELEMENT.prefix;
					break;
				case 1: // validate single-entry array
					linePattern = "\\"+TxtElementType.ARRAY.prefix+(char)ArrayElementSubtype.DOUBLE_SUB_ELEMENT.prefix+"\\s*987654.123456";
					break;
				case 2: // validate 1st line of sontinued array
					linePattern = "\\"+TxtElementType.ARRAY.prefix+(char)ArrayElementSubtype.LONG_SUB_ELEMENT.prefix
							+"\\s*987654 123456 111111111 22222222222 333333333333 44444444444444 ";
					break;
				case 3: // validate 1st continuation line
					linePattern = "\\"+TxtContinuationType.SOFT_BREAK.prefix
							+"\\s*5555555555555 666666666666666 77777777777777 88888888888888 ";
					break;
				case 4: // validate 2nd continuation line
					linePattern = "\\"+TxtContinuationType.SOFT_BREAK.prefix + "\\s*99999999999999 100000000000000";
					break;
			}
			assertTrue(Pattern.compile("^\\s*"+linePattern+"$").matcher(line).matches());
			line = reader.readLine();
		}
	}

	@Test
	public void testWriteMap() throws IOException {
		LOGGER.info("writeMap");

		TxtWriterContext txtCtx = new TxtWriterContext(tdos.stream());

		Map<Element, Element> elements = new LinkedHashMap<>();
		elements.put(new StringElement("hello"), new StringElement(HARD_BREAK));
		elements.put(new IntElement(12345), new StringElement(String.valueOf(Character.MIN_VALUE)));

		MapElement me = new MapElement(elements).inContainer(txtCtx.getRoot());

		txtCtx.write(me);
		txtCtx.flush();

		BufferedReader reader = tdos.reader();
		validateContainerElement(me, reader, txtCtx);

		assertNull(reader.readLine());
	}

	private void validateContainerElement(ListElement<? extends Element> le, BufferedReader reader, TxtWriterContext txtCtx) throws IOException {
		TxtElementType txtType = TxtElementType.forElementType(le.getType());
		assertNotNull(txtType);

		String line = reader.readLine();
		// test indentation level
		int indentation = countWhiteChars(line);
		assertEquals(txtCtx.getTabWidth() * txtCtx.getIndentLevel(), indentation);
		// trim indentation
		line = line.substring(indentation);

		// test the container prefix
		assertEquals(txtType.prefix, line);

		// inform the context of new container
		txtCtx.containerBegin();

		TxtElementType terminator = null;
		// collect the contents of the container
		List<Element> elements = new LinkedList<>();
		switch (le.getType()) {
			case LIST_ELEMENT:
				elements.addAll(le.getContents());
				terminator = TxtElementType.LIST_END;
				break;
			case MAP_ELEMENT:
				// if the container is a map, "unroll" the key-value pairs to a flat list
				((MapElement<Element, Element>)le).getContents().stream().forEach(entry -> {
					elements.add(entry.getEntryKey());
					elements.add(entry.getEntryValue());
				});
				terminator = TxtElementType.MAP_END;
				break;
			default:
				fail("can't handle "+le.getType());
				break;
		}

		// iterate through the contents
		String nextLine = null;
		for (Element e : elements) {
			if (nextLine != null) {
				line = nextLine;
				nextLine = null;
			} else {
				line = stripIndentation(reader.readLine(), txtCtx);
			}
			// skip any blank lines
			while (line.trim().length() == 0) {
				line = stripIndentation(reader.readLine(), txtCtx);
			}

			txtType = TxtElementType.forLine(line);
			if (txtType != null) {
				assertSame(e.getType(), txtType.elementType);
				switch (txtType) {
					case LIST_END:
					case MAP_END:
						fail("Should not encounter "+txtType+" without a corresponding container.");
						break;

					case LIST:
						validateContainerElement((ListElement)e, reader, txtCtx);
						break;

					case MAP:
						break;

					case STRING:
					case BLOB:
						// skip past continuation lines
						do {
							nextLine = reader.readLine();
							if (terminator == TxtElementType.forLine(nextLine.trim())) {
								// we might encounter the list end while sipping continuation lines
								break;
							}
							nextLine = stripIndentation(nextLine, txtCtx);
						} while(TxtContinuationType.forLine(nextLine) != null);
						break;

					default:
						// just go to the next line
						break;
				}
			} else {
				// txtType is null -- why?
				fail("Could not recognize prefix within list: "+line);
			}
		}
		// inform the context of end container
		txtCtx.containerEnd();

		// check for the list ending line
		if (nextLine != null) {
			line = nextLine;
		} else {
			line = reader.readLine();
		}
		line = stripIndentation(line, txtCtx);
		assertSame(terminator, TxtElementType.forLine(line));
	}

	@Test
	public void testWriteEmptyContainer() throws IOException {
		LOGGER.info("writeEmptyContainer");

		// default padding
		TxtWriterContext txtCtx = new TxtWriterContext(tdos.stream());

		ListElement emptyList = new ListElement(Collections.EMPTY_LIST);
		MapElement emptyMap = new MapElement(Collections.EMPTY_MAP);

		txtCtx.write(emptyList.inContainer(txtCtx.getRoot()));
		txtCtx.write(emptyMap.inContainer(txtCtx.getRoot()));
		txtCtx.flush();

		BufferedReader reader = tdos.reader();
		assertEquals("[ ]", reader.readLine());
		assertEquals("{ }", reader.readLine());

		// no padding
		tdos = new TestDataOutputStream();
		txtCtx = new TxtWriterContext(tdos.stream()).padded(false);

		txtCtx.write(emptyList.inContainer(txtCtx.getRoot()));
		txtCtx.write(emptyMap.inContainer(txtCtx.getRoot()));
		txtCtx.flush();

		reader = tdos.reader();
		assertEquals("[]", reader.readLine());
		assertEquals("{}", reader.readLine());
	}

	@Test
	public void testUnknownElement() throws IOException {
		LOGGER.info("unknownElement");

		// can't process a MAP_ENTRY_ELEMENT
		TxtWriterContext txtCtx = new TxtWriterContext(tdos.stream());

		MapEntryElement mee = new MapEntryElement(ConstantElement.nullInstance(), ConstantElement.nullInstance());

		txtCtx.write(mee.inContainer(txtCtx.getRoot()));
		txtCtx.flush();

		BufferedReader reader = tdos.reader();
		assertEquals("// unknown element "+mee.getType(), reader.readLine());
	}

	/**
	 * Validate expected indentation level and strip indentation off.
	 * @param line eNON-txt line
	 * @param txtCt Contains indentation params
	 * @return line stripped of indentation spaces.
	 */
	private String stripIndentation(String line, TxtWriterContext txtCtx) {
		if (line.trim().length() == 0) {
			// all-blank lines are always OK
			return line;
		}
		// test indentation level
		int indentation = countWhiteChars(line);
		assertEquals(txtCtx.getTabWidth() * txtCtx.getIndentLevel(), indentation, "Line indentation error: line = '"+line+"'");
		// trim indentation
		return line.substring(indentation);
	}

	/**
	 * Write several simple values to the stream.
	 * @param txtCtx Defines the properties of the writer
	 * @return list of the elements that were written
	 */
	private List<Element> writeScalarValues(TxtWriterContext txtCtx, ElementContainer container) {

		List<Element> values = Arrays.asList(new Element[]{
			new StringElement("testing string"),
			new StringElement("String ending with newline.\n"),
			new StringElement("String ending with CR.\r"),
			new StringElement("String with \r CR embedded."),
			new StringElement(""), // empty string
			new StringElement("\u0e00\u0e01\u0e02"),
			new StringElement(WORD_WRAP),
			new StringElement(HYPHEN_WRAP),
			new StringElement(FORCE_WRAP),
			new StringElement(HARD_BREAK),
			// write various characters
			new StringElement("z"),
			new StringElement(" "),
			new StringElement("\n"),
			new StringElement("\t"),
			new StringElement("\r"),
			new StringElement("\\"),
			new StringElement(String.valueOf(Character.toChars(0xe005e))),
			// numeric elements
			new ByteElement((byte)-97),
			new ShortElement((short)54321),
			new IntElement(Integer.MIN_VALUE),
			new NumberElement(BigDecimal.valueOf(Double.MAX_VALUE)),
			new DoubleElement(Double.MAX_VALUE),
			new FloatElement(645.216548f),
			new NanoIntElement((byte)NanoIntElement.MIN_VALUE),
			// BLOB values
			new ByteArrayElement("this will be utf-8 encoded bytes".getBytes(StringElement.CHARSET)),
			new ByteArrayElement(WORD_WRAP.getBytes(StringElement.CHARSET)),
			// constant elements
			ConstantElement.nullInstance(),
			ConstantElement.falseInstance(),
			ConstantElement.trueInstance(),
			ConstantElement.negativeInfinityInstance(),
			ConstantElement.positiveInfinityInstance(),
			ConstantElement.nanInstance(),
		});

		values.stream().forEach(e -> txtCtx.write(e.inContainer(container)));

		// flush the output
		txtCtx.flush();

		return values;
	}

	/**
	 * Write a series of long strings to the stream in the context.
	 * @param txtCtx
	 */
	private List<String> writeLongStrings(TxtWriterContext txtCtx) {

		List<String> strings = new LinkedList<>();

		// write a word-wrapped string
		String wordWrapString = "testing string longer than the block width set in the context, which by default is 76, but may be "
				+ "explicitly set to a specific value.";
 		txtCtx.write(new StringElement(wordWrapString).inContainer(txtCtx.getRoot()));
		strings.add(wordWrapString);

		// write a hyphen-wrapped string
		String hyphenWrappedString = "testing string longer than the block width set in the context, which-by-default-is-76,-but-may-be-"
				+ "explicitly-set-to-a-specific-value.";
 		txtCtx.write(new StringElement(hyphenWrappedString).inContainer(txtCtx.getRoot()));
		strings.add(hyphenWrappedString);

		// write a hard-break string
		String hardBreakString = "testing string longer than the block width set in the context, \nwhich by default is 76, but may be "
				+ "explicitly set to a specific value.";
 		txtCtx.write(new StringElement(hardBreakString).inContainer(txtCtx.getRoot()));
		strings.add(hardBreakString);

		// write a hard-break at the end
		String endNewline = "String ending in newline\n";
 		txtCtx.write(new StringElement(endNewline).inContainer(txtCtx.getRoot()));
		strings.add(endNewline);

		// write a hard-break at the end
		String endSlashNewline = "String ending in newline slash + newline\\\n";
 		txtCtx.write(new StringElement(endSlashNewline).inContainer(txtCtx.getRoot()));
		strings.add(endSlashNewline);

		// forced break at block width
		String forcedBreak = "String can't wordbreak at good spot 12345678901234567890123456789012345678901234567890123456789012345678901234567890";
 		txtCtx.write(new StringElement(forcedBreak).inContainer(txtCtx.getRoot()));
		strings.add(forcedBreak);

		// flush the output
		txtCtx.flush();

		return strings;
	}

	private int countWhiteChars(String line) {
		int i = 0;
		while (i < line.length()) {
			if (!Character.isWhitespace(line.codePointAt(i))) {
				break;
			}
			i++;
		}
		return i;
	}
}
