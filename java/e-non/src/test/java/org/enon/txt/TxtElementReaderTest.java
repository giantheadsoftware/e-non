/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.txt;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import org.enon.element.Element;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.ElementType;
import org.enon.element.ListElement;
import org.enon.element.MapElement;
import org.enon.element.StringElement;
import org.enon.element.array.ArrayElement;
import org.enon.element.array.ByteArrayElement;
import org.enon.exception.EnonException;
import org.enon.exception.EnonReadException;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TxtElementReaderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(TxtElementReaderTest.class.getName());

	private TestDataOutputStream tdos;
	private OutputStreamWriter writer;

	public TxtElementReaderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		tdos = new TestDataOutputStream();
		writer = new OutputStreamWriter(tdos.stream());
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of read method, of class TxtElementReader.
	 *
	 * @throws java.io.IOException
	 */
	@Test
	public void testEndOfStream() throws IOException {
		LOGGER.info("endOfStream");
		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();
		assertNull(teReader.read(txtctx));
	}

	@Test
	public void testReadSingleLines() throws Exception {
		LOGGER.info("readSingleLines");
		// create a map that associates a txt line with a test function for that line
		Map<String, Consumer<Element>> lineTests = new LinkedHashMap<>();

		// Generate numerous examples of single-line elements to exercise specific code branches
		// BLOB (from string)
		lineTests.put(TxtElementType.BLOB.prefix + Base64.getEncoder().encodeToString("real Base64 blob data\n".getBytes()),
				e -> {
					assertSame(ElementType.BLOB_ELEMENT, e.getType());
					assertEquals("real Base64 blob data\n", new String(((ByteArrayElement) e).getValue()));
				});
		// nano-int
		lineTests.put(TxtElementType.BYTE.prefix + " 13",
				e -> {
					assertSame(ElementType.NANO_INT_ELEMENT, e.getType());
					assertEquals((byte) 13, e.getValue());
				});
		// not nano-int
		lineTests.put(TxtElementType.BYTE.prefix + " -107",
				e -> {
					assertSame(ElementType.BYTE_ELEMENT, e.getType());
					assertEquals((byte) -107, e.getValue());
				});
		// simple char
		lineTests.put(TxtElementType.STRING.prefix + "c",
				e -> {
					assertSame(ElementType.STRING_ELEMENT, e.getType());
					assertEquals("c", ((StringElement) e).getValue());
				});
		// unicode char with \\u prefix
		lineTests.put(TxtElementType.STRING_UNICODE.prefix + "\\u0e00;",
				e -> {
					assertSame(ElementType.STRING_ELEMENT, e.getType());
					assertEquals("\u0e00", ((StringElement) e).getValue());
				});
		// double with padding
		lineTests.put(TxtElementType.DOUBLE.prefix + " 13.321",
				e -> {
					assertSame(ElementType.DOUBLE_ELEMENT, e.getType());
					assertEquals(13.321, (double) e.getValue(), Double.MIN_VALUE);
				});
		// false
		lineTests.put(TxtElementType.FALSE.prefix,
				e -> {
					assertSame(ElementType.FALSE_ELEMENT, e.getType());
					assertFalse((boolean) e.getValue());
				});
		// true
		lineTests.put(TxtElementType.TRUE.prefix,
				e -> {
					assertSame(ElementType.TRUE_ELEMENT, e.getType());
					assertTrue((boolean) e.getValue());
				});
		// float with tab used for padding
		lineTests.put(TxtElementType.FLOAT.prefix + "\t331.554",
				e -> {
					assertSame(ElementType.FLOAT_ELEMENT, e.getType());
					assertEquals(331.554f, (float) e.getValue(), Float.MIN_VALUE);
				});
		// negative infinity
		lineTests.put(TxtElementType.INFINITY_NEG.prefix,
				e -> {
					assertSame(ElementType.INFINITY_NEG_ELEMENT, e.getType());
					assertEquals(Float.NEGATIVE_INFINITY, (float) e.getValue(), Float.MIN_VALUE);
				});
		// positive infinity
		lineTests.put(TxtElementType.INFINITY_POS.prefix,
				e -> {
					assertSame(ElementType.INFINITY_POS_ELEMENT, e.getType());
					assertEquals(Float.POSITIVE_INFINITY, (float) e.getValue(), Float.MIN_VALUE);
				});
		// int with multi-space padding
		lineTests.put(TxtElementType.INT.prefix + "   99999",
				e -> {
					assertSame(ElementType.INT_ELEMENT, e.getType());
					assertEquals(99999, e.getValue());
				});
		// long with trailing white space
		lineTests.put(TxtElementType.LONG.prefix + "-9999999  ",
				e -> {
					assertSame(ElementType.LONG_ELEMENT, e.getType());
					assertEquals(-9999999L, e.getValue());
				});
		// NaN
		lineTests.put(TxtElementType.NAN.prefix,
				e -> {
					assertSame(ElementType.NAN_ELEMENT, e.getType());
					assertEquals(Float.NaN, e.getValue());
				});
		// null
		lineTests.put(TxtElementType.NULL.prefix,
				e -> {
					assertSame(ElementType.NULL_ELEMENT, e.getType());
					assertNull(e.getValue());
				});
		// number (big-decimal)
		lineTests.put(TxtElementType.NUMBER.prefix + "225.884E134",
				e -> {
					assertSame(ElementType.NUMBER_ELEMENT, e.getType());
					assertEquals(new BigDecimal("225.884E134"), new BigDecimal((String) e.getValue()));
				});
		// short
		lineTests.put(TxtElementType.SHORT.prefix + "15004",
				e -> {
					assertSame(ElementType.SHORT_ELEMENT, e.getType());
					assertEquals((short) 15004, e.getValue());
				});
		// string
		lineTests.put(TxtElementType.STRING.prefix + "hello!",
				e -> {
					assertSame(ElementType.STRING_ELEMENT, e.getType());
					assertEquals("hello!", e.getValue());
				});
		// empty string
		lineTests.put(TxtElementType.STRING.prefix,
				e -> {
					assertSame(ElementType.STRING_ELEMENT, e.getType());
					assertEquals("", e.getValue());
				});
		// unicode string with escaped char
		lineTests.put(TxtElementType.STRING_UNICODE.prefix + "c\\u0e02;",
				e -> {
					assertSame(ElementType.STRING_ELEMENT, e.getType());
					assertEquals("c\u0e02", e.getValue());
				});
		// unicode string with improperly escaped char
		lineTests.put(TxtElementType.STRING_UNICODE.prefix + "c\\u0e02 too much space;", // semicolon too far from \\u to be Unicode
				e -> {
					assertSame(ElementType.STRING_ELEMENT, e.getType());
					assertEquals("c\\u0e02 too much space;", e.getValue());  // the unicode won't be unescaped
				});
		// another unicode string with improperly escaped char
		lineTests.put(TxtElementType.STRING_UNICODE.prefix + "c\\u0e02", // semicolon missing after \\u
				e -> {
					assertSame(ElementType.STRING_ELEMENT, e.getType());
					assertEquals("c\\u0e02", e.getValue());  // the unicode won't be unescaped
				});
		// yet another unicode string with improperly escaped char
		lineTests.put(TxtElementType.STRING_UNICODE.prefix + "c\\u0e02 too much space and no semi", // semicolon too far from \\u to be Unicode
				e -> {
					assertSame(ElementType.STRING_ELEMENT, e.getType());
					assertEquals("c\\u0e02 too much space and no semi", e.getValue());  // the unicode won't be unescaped
				});
		// empty list
		lineTests.put(TxtElementType.LIST.prefix + TxtElementType.LIST_END.prefix, // empty list
				e -> {
					assertSame(ElementType.LIST_ELEMENT, e.getType());
					assertTrue(((List) e.getValue()).isEmpty());
				});
		// empty map
		lineTests.put(TxtElementType.MAP.prefix + TxtElementType.MAP_END.prefix, // empty map
				e -> {
					assertSame(ElementType.MAP_ELEMENT, e.getType());
					assertTrue(((List) e.getValue()).isEmpty());
				});

		// write the test lines
		for (String line : lineTests.keySet()) {
			writer.write(line + '\n');
		}
		writer.flush();

		// create the txt reader
		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		// read the elements and execute the tests
		for (Consumer<Element> test : lineTests.values()) {
			Element e = teReader.read(txtctx);
			test.accept(e);
		}
	}

	@Test
	public void testSingleLineFail() throws IOException {
		LOGGER.info("singleLineFail");

		// generate several invalid eNON-txt lines for negative tests
		String[] lines = new String[]{
			TxtElementType.STRING_UNICODE.prefix + "unicode \\uFE0ABC1501; string", // escape sequence too long (out of int range)
			TxtElementType.STRING_UNICODE.prefix + "unicode \\uFFFFFF; string", // invalid codepoint
		};

		Arrays.stream(lines).forEach(line -> {
			try {
				writer.write(line + '\n');
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
		});
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		for (String line : lines) {
			try {
				teReader.read(txtctx);
				fail("Should fail: " + line);
			} catch (EnonReadException x) {
				// success
			}
		}
	}

	@Test
	public void testReadContinuation() throws Exception {
		LOGGER.info("readContinuation");

		byte[] blob = "real Base64 blob data, long enough to break into multiple lines\n".getBytes();
		String base64 = Base64.getEncoder().encodeToString(blob);
		int midpoint = base64.length() / 2;

		// write out elements that use continuation lines
		String[] lines = new String[]{
			TxtElementType.BLOB.prefix + " " + base64.substring(0, midpoint),
			TxtContinuationType.BREAK.prefix + " " + base64.substring(midpoint),
			"",
			"",
			TxtElementType.STRING.prefix + "hello!",
			TxtContinuationType.BREAK.prefix + "This is a hard break continuation",
			TxtContinuationType.SOFT_BREAK.prefix + " followed by a soft break continuation",
			TxtContinuationType.BREAK.prefix + "and another hard break",
			String.valueOf(TxtContinuationType.BREAK.prefix),
			String.valueOf(TxtContinuationType.BREAK.prefix),
			TxtContinuationType.BREAK.prefix + "With a couple blank lines.",
			TxtElementType.STRING_UNICODE.prefix + "c\u0e02;\n",};

		List<String> continuedStrings = new LinkedList<>();

		final StringBuilder continuedString = new StringBuilder();
		Arrays.stream(lines).forEach(line -> {
			try {
				writer.write(line + '\n');
				if (TxtElementType.forLine(line) != null) {
					// we're starting a new element, add any previous string to the list
					if (continuedString.length() > 0) {
						continuedStrings.add(continuedString.toString());
					}
					// init the string builder to the new element
					continuedString.setLength(0);
					continuedString.append(line.substring(TxtElementType.forLine(line).prefix.length()));
				} else if (TxtContinuationType.forLine(line) != null) {
					switch (TxtContinuationType.forLine(line)) {
						case BREAK:
							continuedString.append("\n").append(line.substring(1));
							break;
						case SOFT_BREAK:
							continuedString.append(line.substring(1));
							break;
					}
				}
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
		});
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());

		TxtElementReader teReader = new TxtElementReader();
		for (String line : continuedStrings) {
			if (line.trim().isEmpty()) {
				continue;
			}
			Element e = teReader.read(txtctx);
			switch (e.getType()) {
				case STRING_ELEMENT:
					assertEquals(line, e.getValue());
					break;

				case BLOB_ELEMENT:
					assertArrayEquals(blob, (byte[]) e.getValue());
					break;
			}
		}
	}

	@Test
	public void testList() throws IOException {
		LOGGER.info("list");

		// write a list with 3 entries
		String[] lines = new String[]{
			TxtElementType.LIST.prefix,
			// add list contents
			TxtElementType.DOUBLE.prefix + " 13.321",
			TxtElementType.FALSE.prefix,
			"",
			"",
			TxtElementType.INFINITY_NEG.prefix,
			// terminate list
			TxtElementType.LIST_END.prefix,};

		Arrays.stream(lines).forEach(line -> {
			try {
				writer.write(line + '\n');
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
		});
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		Element e = teReader.read(txtctx);
		assertSame(ElementType.LIST_ELEMENT, e.getType());
		assertEquals(3, ((ListElement) e).getValue().size());
	}

	@Test
	public void testArray() throws IOException {
		LOGGER.info("array");
		// write some arrays to a stream
		String[] lines = new String[]{
			// write an int array with 3 entries, all on 1 line
			TxtElementType.ARRAY.prefix + (char) ArrayElementSubtype.INT_SUB_ELEMENT.prefix + " 0 12345  67890",
			// write a boolean array using multiple lines
			TxtElementType.ARRAY.prefix + (char) ArrayElementSubtype.FALSE_SUB_ELEMENT.prefix,
			TxtContinuationType.BREAK.prefix + "false ",
			TxtContinuationType.SOFT_BREAK.prefix + " true",
			TxtContinuationType.BREAK.prefix + "t f f t",
			// write a byte array
			TxtElementType.ARRAY.prefix + (char) ArrayElementSubtype.BYTE_SUB_ELEMENT.prefix + " 0 123 \t -67",
			// write a short array using multiple lines
			TxtElementType.ARRAY.prefix + (char) ArrayElementSubtype.SHORT_SUB_ELEMENT.prefix + " 0 1234 ",
			TxtContinuationType.BREAK.prefix + "-6785",
			// write a long array
			TxtElementType.ARRAY.prefix + (char) ArrayElementSubtype.LONG_SUB_ELEMENT.prefix + "0 1234567890123 -6754871",
			// write a float array
			TxtElementType.ARRAY.prefix + (char) ArrayElementSubtype.FLOAT_SUB_ELEMENT.prefix + "0.2 123.456 -6754.871",
			// write a double array
			TxtElementType.ARRAY.prefix + (char) ArrayElementSubtype.DOUBLE_SUB_ELEMENT.prefix + "0.2 123.456 -6754.871",};

		Arrays.stream(lines).forEach(line -> {
			try {
				writer.write(line + '\n');
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
		});
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		// read the int array
		Element e = teReader.read(txtctx);
		assertSame(ElementType.ARRAY_ELEMENT, e.getType());
		assertSame(ArrayElementSubtype.INT_SUB_ELEMENT, ((ArrayElement) e).getSubType());
		assertArrayEquals(new int[]{0, 12345, 67890}, (int[]) e.getValue());

		// read the boolean array
		e = teReader.read(txtctx);
		assertSame(ElementType.ARRAY_ELEMENT, e.getType());
		assertSame(ArrayElementSubtype.FALSE_SUB_ELEMENT, ((ArrayElement) e).getSubType());
		assertArrayEquals(new boolean[]{false, true, true, false, false, true}, (boolean[]) e.getValue());

		// read the byte array
		e = teReader.read(txtctx);
		assertSame(ElementType.BLOB_ELEMENT, e.getType()); // byte arrays report themselves as BLOB
		assertSame(ArrayElementSubtype.BYTE_SUB_ELEMENT, ((ArrayElement) e).getSubType());
		assertArrayEquals(new byte[]{(byte) 0, (byte) 123, (byte) -67}, (byte[]) e.getValue());

		// read the short array
		e = teReader.read(txtctx);
		assertSame(ElementType.ARRAY_ELEMENT, e.getType());
		assertSame(ArrayElementSubtype.SHORT_SUB_ELEMENT, ((ArrayElement) e).getSubType());
		assertArrayEquals(new short[]{(short) 0, (short) 1234, (short) -6785}, (short[]) e.getValue());

		// read the long array
		e = teReader.read(txtctx);
		assertSame(ElementType.ARRAY_ELEMENT, e.getType());
		assertSame(ArrayElementSubtype.LONG_SUB_ELEMENT, ((ArrayElement) e).getSubType());
		assertArrayEquals(new long[]{0L, 1234567890123L, -6754871L}, (long[]) e.getValue());

		// read the float array
		e = teReader.read(txtctx);
		assertSame(ElementType.ARRAY_ELEMENT, e.getType());
		assertSame(ArrayElementSubtype.FLOAT_SUB_ELEMENT, ((ArrayElement) e).getSubType());
		assertArrayEquals(new float[]{0.2f, 123.456f, -6754.871f}, (float[]) e.getValue(), Float.MIN_VALUE);

		// read the double array
		e = teReader.read(txtctx);
		assertSame(ElementType.ARRAY_ELEMENT, e.getType());
		assertSame(ArrayElementSubtype.DOUBLE_SUB_ELEMENT, ((ArrayElement) e).getSubType());
		assertArrayEquals(new double[]{0.2, 123.456, -6754.871}, (double[]) e.getValue(), Double.MIN_VALUE);
	}

	@Test
	public void testMap() throws IOException {
		LOGGER.info("map");

		// write a map with 2 key value pairs
		String[] lines = new String[]{
			TxtElementType.MAP.prefix,
			// add map contents
			TxtElementType.DOUBLE.prefix + " 13.321",
			TxtElementType.FALSE.prefix,
			"",
			"",
			TxtElementType.FLOAT.prefix + "\t331.554",
			TxtElementType.INFINITY_NEG.prefix,
			// terminate map
			TxtElementType.MAP_END.prefix,};

		Arrays.stream(lines).forEach(line -> {
			try {
				writer.write(line + '\n');
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
		});
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		Element e = teReader.read(txtctx);
		assertSame(ElementType.MAP_ELEMENT, e.getType());
		assertEquals(2, ((MapElement) e).getValue().size());
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testUnterminatedListFail() throws IOException {
		LOGGER.info("unterminatedListFail");

		Exception ex = assertThrows(EnonReadException.class, () -> {
			// write list & map that terminate on the next line
			String[] lines = new String[]{
				TxtElementType.LIST.prefix,
				// add list contents
				TxtElementType.DOUBLE.prefix + " 13.321",
				TxtElementType.FALSE.prefix,
				"",
				"",
				TxtElementType.INFINITY_NEG.prefix, // don't terminate list
			};

			Arrays.stream(lines).forEach(line -> {
				try {
					writer.write(line + '\n');
				} catch (IOException x) {
					LOGGER.error(x.getMessage(), x);
					throw new EnonException(x.getMessage(), x);
				}
			});
			writer.flush();

			TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
			TxtElementReader teReader = new TxtElementReader();

			teReader.read(txtctx);
		});
	}

	@Test
	public void testReadBlobFile() throws IOException {
		LOGGER.info("readBlobFile");
		// create a tmp file to read
		File tmpFile = File.createTempFile("test_", ".tar xz");
		tmpFile.deleteOnExit();
		FileOutputStream fos = new FileOutputStream(tmpFile);
		// get the data for the test file
		InputStream testData = getClass().getResourceAsStream("/pom.tar.xz");
		byte[] testBytes = new byte[testData.available()];
		testData.read(testBytes);
		testData.close();
		// write the test file
		fos.write(testBytes);
		fos.close();

		// create a BLOB file entry
		writer.write(TxtElementType.BLOB.prefix + TxtElementType.STRING.prefix + tmpFile.getPath() + '\n');
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		// read the blob element
		Element e = teReader.read(txtctx);
		assertSame(ElementType.BLOB_ELEMENT, e.getType());
		// the data in teh blob element should match what was in the file
		assertArrayEquals(testBytes, (byte[]) e.getValue());
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReadBlobFileNotFoundFail() throws IOException {
		LOGGER.info("readBlobFileNotFoundFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			// create a BLOB file entry -- invalid faile path
			writer.write(TxtElementType.BLOB.prefix + TxtElementType.STRING.prefix + "does/not/exist\n");
			writer.flush();

			TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
			TxtElementReader teReader = new TxtElementReader();

			teReader.read(txtctx);
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testUnterminatedMapFail() throws IOException {
		LOGGER.info("unterminatedMapFail");

		Exception ex = assertThrows(EnonReadException.class, () -> {
			// write a map that has no terminator
			String[] lines = new String[]{
				TxtElementType.MAP.prefix,
				// add map contents
				TxtElementType.DOUBLE.prefix + " 13.321",
				TxtElementType.FALSE.prefix,
				"",
				"", // don't terminate map
			};

			Arrays.stream(lines).forEach(line -> {
				try {
					writer.write(line + '\n');
				} catch (IOException x) {
					LOGGER.error(x.getMessage(), x);
					throw new EnonException(x.getMessage(), x);
				}
			});
			writer.flush();

			TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
			TxtElementReader teReader = new TxtElementReader();

			teReader.read(txtctx);
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testUnterminatedMapFail2() throws IOException {
		LOGGER.info("unterminatedMapFail2");

		Exception ex = assertThrows(EnonReadException.class, () -> {
			// write a map that has no terminator and ends without a paired key-value
			String[] lines = new String[]{
				TxtElementType.MAP.prefix,
				// add map contents
				TxtElementType.DOUBLE.prefix + " 13.321",
				TxtElementType.FALSE.prefix,
				"",
				"",
				TxtElementType.STRING.prefix + "unpaired key"
			// don't terminate map
			};

			Arrays.stream(lines).forEach(line -> {
				try {
					writer.write(line + '\n');
				} catch (IOException x) {
					LOGGER.error(x.getMessage(), x);
					throw new EnonException(x.getMessage(), x);
				}
			});
			writer.flush();

			TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
			TxtElementReader teReader = new TxtElementReader();

			teReader.read(txtctx);
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testUnpairedMapEntryFail() throws IOException {
		LOGGER.info("unpairedMapEntryFail");

		Exception ex = assertThrows(EnonReadException.class, () -> {
			// write a map that has an odd number of elements
			String[] lines = new String[]{
				TxtElementType.MAP.prefix,
				// add map contents
				TxtElementType.DOUBLE.prefix + " 13.321",
				TxtElementType.FALSE.prefix,
				"",
				"",
				TxtElementType.INFINITY_NEG.prefix, // key without paired value
				//terminate map
				TxtElementType.MAP_END.prefix
			};

			Arrays.stream(lines).forEach(line -> {
				try {
					writer.write(line + '\n');
				} catch (IOException x) {
					LOGGER.error(x.getMessage(), x);
					throw new EnonException(x.getMessage(), x);
				}
			});
			writer.flush();

			TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
			TxtElementReader teReader = new TxtElementReader();

			teReader.read(txtctx);
		});
	}

	@Test
	public void testEmptyContainer2Line() throws IOException {
		LOGGER.info("emptyContainer2Line");

		// write list & map that terminate on the next line
		String[] lines = new String[]{
			TxtElementType.LIST.prefix,
			TxtElementType.LIST_END.prefix, // empty list
			TxtElementType.MAP.prefix,
			TxtElementType.MAP_END.prefix // empty map
		};

		Arrays.stream(lines).forEach(line -> {
			try {
				writer.write(line + '\n');
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
		});
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		Element e = teReader.read(txtctx);
		assertSame(ElementType.LIST_ELEMENT, e.getType());
		assertTrue(((ListElement) e).getValue().isEmpty());

		e = teReader.read(txtctx);
		assertSame(ElementType.MAP_ELEMENT, e.getType());
		assertTrue(((MapElement) e).getValue().isEmpty());
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testSpuriousListEnd() throws IOException {
		LOGGER.info("spuriousListEnd");
		Exception ex = assertThrows(EnonReadException.class, () -> {
			try {
				writer.write(TxtElementType.LIST_END.prefix + "\n");
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
			writer.flush();

			TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
			TxtElementReader teReader = new TxtElementReader();

			teReader.read(txtctx);
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testSpuriousMapEnd() throws IOException {
		LOGGER.info("spuriousMapEnd");
		Exception ex = assertThrows(EnonReadException.class, () -> {
			try {
				writer.write(TxtElementType.MAP_END.prefix + "\n");
			} catch (IOException x) {
				LOGGER.error(x.getMessage(), x);
				throw new EnonException(x.getMessage(), x);
			}
			writer.flush();

			TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
			TxtElementReader teReader = new TxtElementReader();

			teReader.read(txtctx);
		});
	}

	@Test
	public void testUnicodeChar() throws IOException {
		LOGGER.info("unicodeChar");

		char[][] chars = new char[][]{
			Character.toChars(0x0e01),
			Character.toChars(0x0e05),};

		for (char[] ch : chars) {
			writer.write(TxtElementType.STRING.prefix + new String(ch) + '\n');  // write as UTF-8
			writer.write(TxtElementType.STRING_UNICODE.prefix + "\\u" + Integer.toHexString(Character.codePointAt(ch, 0)) + ";\n");  // write as escaped
		}
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		for (char[] ch : chars) {
			StringElement utf8 = (StringElement) teReader.read(txtctx);
			StringElement escaped = (StringElement) teReader.read(txtctx);

			assertEquals(new String(ch), utf8.getValue());
			assertEquals(new String(ch), escaped.getValue());
		}
	}

	@Test
	public void testUnicodeSuppChar() throws IOException {
		LOGGER.info("unicodeSuppChar");

		char[][] chars = new char[][]{
			Character.toChars(0xe005e), // supplementary (2-char) codepoint
		};

		for (char[] ch : chars) {
			writer.write(TxtElementType.STRING_UNICODE.prefix + "\\u" + Integer.toHexString(Character.codePointAt(ch, 0)) + ";\n");
		}
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		for (char[] ch : chars) {
			StringElement escaped = (StringElement) teReader.read(txtctx);

			assertEquals(new String(ch), escaped.getValue());
		}
	}

	@Test
	public void testUnicodeString() throws IOException {
		LOGGER.info("unicodeString");

		char[][] chars = new char[][]{
			Character.toChars(0x0e01),
			Character.toChars(0x0e05),
			Character.toChars(0xe005e), // supplementary (2-char) codepoint
		};

		String unicodeValue = "randomly " + new String(chars[0]) + " inserted " + new String(chars[1]) + " unicode chars " + new String(chars[2]) + "";
		String[] strings = new String[]{
			unicodeValue,
			"randomly "
			+ "\\u" + Integer.toHexString(Character.codePointAt(chars[0], 0)) + ';'
			+ " inserted "
			+ "\\u" + Integer.toHexString(Character.codePointAt(chars[1], 0)) + ';'
			+ " unicode chars "
			+ "\\u" + Integer.toHexString(Character.codePointAt(chars[2], 0)) + ';'
		};
		for (String string : strings) {
			writer.write(TxtElementType.STRING.prefix + string + '\n');
			writer.write(TxtElementType.STRING_UNICODE.prefix + string + "\n");
		}
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
		TxtElementReader teReader = new TxtElementReader();

		for (String string : strings) {
			StringElement chElement = (StringElement) teReader.read(txtctx);
			StringElement UnicodeElement = (StringElement) teReader.read(txtctx);

			assertEquals(string, chElement.getValue());
			assertEquals(unicodeValue, UnicodeElement.getValue());
		}
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testUnicodeBadCodepointFail() throws IOException {
		LOGGER.info("unicodeBadCodepointFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			writer.write(TxtElementType.STRING_UNICODE.prefix + "\\ufff101;\n");
			writer.flush();

			TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream());
			new TxtElementReader().read(txtctx);
		});
	}

	@Test
	public void testCharsetUtf16() throws IOException {
		LOGGER.info("charsetUtf16");

		char[][] chars = new char[][]{
			Character.toChars(0x0e01),
			Character.toChars(0x0e05),
			Character.toChars(0xe005e), // supplementary (2-char) codepoint
		};

		StringBuilder sb = new StringBuilder();
		Arrays.stream(chars).forEach(sb::append);

		writer = new OutputStreamWriter(tdos.stream(), StandardCharsets.UTF_16BE);
		writer.write(TxtElementType.STRING.prefix + sb.toString() + '\n');
		writer.flush();

		TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream(), StandardCharsets.UTF_16BE);
		TxtElementReader teReader = new TxtElementReader();

		// verify that the string data was not corrupted
		assertEquals(sb.toString(), teReader.read(txtctx).getValue());
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testCharsetMismatchFail() throws IOException {
		LOGGER.info("charsetMismatchFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			char[][] chars = new char[][]{
				Character.toChars(0x0e01),
				Character.toChars(0x0e05),
				Character.toChars(0xe005e), // supplementary (2-char) codepoint
			};

			StringBuilder sb = new StringBuilder();
			Arrays.stream(chars).forEach(sb::append);

			writer = new OutputStreamWriter(tdos.stream(), StandardCharsets.UTF_16BE);
			writer.write(TxtElementType.STRING.prefix + sb.toString() + '\n');
			writer.flush();

			TxtReaderContext txtctx = TxtReaderContext.create(tdos.inputStream(), StandardCharsets.US_ASCII);
			TxtElementReader teReader = new TxtElementReader();

			// can't read ASCII from UTF-16 stream
			teReader.read(txtctx);
		});
	}
}
