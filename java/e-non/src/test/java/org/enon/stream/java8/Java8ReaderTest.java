/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.stream.java8;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Writer;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.IntElement;
import org.enon.element.NanoIntElement;
import org.enon.element.StringElement;
import org.enon.element.ConstantElement;
import org.enon.exception.EnonReadException;
import org.enon.stream.java8.util.LimitedConsumer;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class Java8ReaderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(Java8ReaderTest.class.getName());

	public Java8ReaderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of parseAll method, of class Java8Reader.
	 */
	@Test
	public void testParseAllSingleEnon0() {
		LOGGER.info("ParseAllSingleEnon0");
		final String value = "Test single string with eNON-0 config";
		List<Element> results = new LinkedList<>();
		// create an unlimted consumer
		LimitedConsumer<Element> consumer = new LimitedConsumer<>((Long) null, (Element element) -> {
			results.add(element);
		});

		// create the test data
		TestDataOutputStream tdos = generateSingleString(value);
		Java8Reader instance = new Java8Reader(tdos.dataInputStream());

		// parse
		instance.parseAll(consumer);

		assertEquals(2, results.size());
		assertEquals(ElementType.STRING_ELEMENT, results.get(0).getType());
		assertEquals(value, ((StringElement) results.get(0)).getValue());
		assertNull(results.get(1));

		tdos.close();
	}

	/**
	 * Test of parseAll method, of class Java8Reader.
	 */
	@Test
	public void testParseAllMultipleEnon0() {
		LOGGER.info("ParseAllMultipleEnon0");
		List values = new LinkedList();
		final String value1 = "Test multiple top-level with eNON-0 config";
		values.add(value1);
		values.add(true);
		values.add(null);
		List<Element> results = new LinkedList<>();
		// create an unlimted consumer
		LimitedConsumer<Element> consumer = new LimitedConsumer<>((Long) null, (Element element) -> {
			results.add(element);
		});

		// create the test data
		TestDataOutputStream tdos = generateMultiple(values, null);
		Java8Reader instance = new Java8Reader(tdos.dataInputStream());

		// parse
		instance.parseAll(consumer);

		assertEquals(2, results.size(), "Should only read first value + terminator");
		assertEquals(ElementType.STRING_ELEMENT, results.get(0).getType());
		assertEquals(value1, ((StringElement) results.get(0)).getValue());
		assertNull(results.get(1));

		tdos.close();
	}

	/**
	 * Test of parseAll method, of class Java8Reader.
	 */
	@Test
	public void testParseAllMultipleEnonS() {
		LOGGER.info("ParseAllMultipleEnonS");
		List values = new LinkedList();
		values.add("Test multiple top-level with eNON-S config");
		values.add(true);
		values.add(null);
		values.add(5483157);
		values.add(-19);
		values.add(108);
		List<Element> results = new LinkedList<>();
		// create an unlimted consumer
		LimitedConsumer<Element> consumer = new LimitedConsumer<>((Long) null, (Element element) -> {
			results.add(element);
		});

		// create the test data
		EnonConfig config = new EnonConfig.Builder().feature(FeatureSet.S).build();
		TestDataOutputStream tdos = generateMultiple(values, config);
		Java8Reader instance = new Java8Reader(tdos.dataInputStream(), config);

		// parse
		instance.parseAll(consumer);

		assertEquals(values.size() + 1, results.size(), "Should read all values + terminator");

		int i = 0;
		assertEquals(ElementType.STRING_ELEMENT, results.get(i).getType());
		assertEquals(values.get(i), ((StringElement) results.get(i)).getValue());

		i++;
		assertEquals(ElementType.TRUE_ELEMENT, results.get(i).getType());
		assertEquals(values.get(i), ((ConstantElement) results.get(i)).getValue());

		i++;
		assertEquals(ElementType.NULL_ELEMENT, results.get(i).getType());
		assertEquals(values.get(i), ((ConstantElement) results.get(i)).getValue());

		i++;
		assertEquals(ElementType.INT_ELEMENT, results.get(i).getType());
		assertEquals(values.get(i), ((IntElement) results.get(i)).getValue());

		i++;
		assertEquals(ElementType.NANO_INT_ELEMENT, results.get(i).getType());
		assertEquals(values.get(i), (int) ((NanoIntElement) results.get(i)).getValue());

		i++;
		assertEquals(ElementType.INT_ELEMENT, results.get(i).getType());
		assertEquals(values.get(i), ((IntElement) results.get(i)).getValue());

		i++;
		assertNull(results.get(i));

		tdos.close();
	}

	/**
	 * Test of parseAll method, of class Java8Reader.
	 *
	 * @throws java.io.IOException
	 */
	@Test
	public void testParseAllExtraData() throws IOException {
		LOGGER.info("parseAllExtraData");
		final String value = "Test parse with extra bytes";
		List<Element> results = new LinkedList<>();
		// create an unlimted consumer
		LimitedConsumer<Element> consumer = new LimitedConsumer<>((Long) null, (Element element) -> {
			results.add(element);
		});

		// create the test data
		TestDataOutputStream tdos = generateSingleString(value);
		// write extra bytes
		tdos.stream().writeShort(100);
		Java8Reader instance = new Java8Reader(tdos.dataInputStream());

		// parse
		instance.parseAll(consumer);

		assertEquals(2, results.size());
		assertEquals(ElementType.STRING_ELEMENT, results.get(0).getType());
		assertEquals(value, ((StringElement) results.get(0)).getValue());
		assertNull(results.get(1));

		tdos.close();
	}

	/**
	 * Test of parseAll method, of class Java8Reader.
	 *
	 * @throws java.io.IOException
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testParseAllTruncated() throws IOException {
		LOGGER.info("parseAllTruncated");
		Exception x = assertThrows(EnonReadException.class, () -> {
		final String value = "Test parse with missing bytes";
		// create a no-op consumer
		LimitedConsumer<Element> consumer = new LimitedConsumer<>(1l, (Element element) -> {
		});

		// create the test data
		TestDataOutputStream tdos = generateSingleString(value);
		// throw away some of the bytes
		byte[] bytes = Arrays.copyOf(tdos.bytes(), tdos.bytes().length - 4);
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(bytes));
		Java8Reader instance = new Java8Reader(dis);

		// attempt parse
		instance.parseAll(consumer);
		});
	}

	private TestDataOutputStream generateSingleString(String value) {
		TestDataOutputStream tdos = new TestDataOutputStream();
		Writer writer = new Java8Writer(tdos.stream());
		writer.write(value);
		return tdos;
	}

	/**
	 * Write the objects to a test stream, including prolog
	 * @param values Objects to write
	 * @param config the config containing factories
	 * @return a readable stream containing the data that was written
	 */
	private TestDataOutputStream generateMultiple(Collection<Object> values, EnonConfig config) {
		TestDataOutputStream tdos = new TestDataOutputStream();
 		new Java8Writer(tdos.stream(), config).writeStream(values.stream());
		return tdos;
	}
}
