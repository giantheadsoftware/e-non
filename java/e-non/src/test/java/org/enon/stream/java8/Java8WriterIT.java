/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.stream.java8;

import org.enon.test.model.simple.Enon0All;
import java.util.LinkedList;
import java.util.List;
import org.enon.test.util.TestDataOutputStream;
import org.enon.tool.StructureParser;
import org.enon.tool.StructureParser.RootHeaderDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class Java8WriterIT {

	private static final Logger LOGGER = LoggerFactory.getLogger(Java8WriterIT.class.getName());

	public Java8WriterIT() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of write method, of class Java8Writer.
	 */
	@Test
	public void testWritePojoEnon0() {
		LOGGER.info("writePojoEnon0");
		Enon0All pojo1 = Enon0All.generateRandomInstance(true);

		TestDataOutputStream tdos = new TestDataOutputStream();
		Java8Writer instance = new Java8Writer(tdos.stream());

		instance.write(pojo1);

		RootHeaderDTO rootStruct = new StructureParser(tdos.dataInputStream())
				.parse();
		LOGGER.info(rootStruct.toString());
	}

	@Test
	public void testWriteLongEnon0() {
		LOGGER.info("writeLongEnon0");
		Long value = (long)Integer.MAX_VALUE + 1;
		assertTrue(value > Integer.MAX_VALUE);

		TestDataOutputStream tdos = new TestDataOutputStream();
		Java8Writer instance = new Java8Writer(tdos.stream());

		instance.write(value);

		RootHeaderDTO rootStruct = new StructureParser(tdos.dataInputStream())
				.parse();
		LOGGER.info(rootStruct.toString());
	}

	/**
	 * Test of write method, of class Java8Writer.
	 */
	@Test
	public void testWriteListEnon0() {
		LOGGER.info("writeListEnon0");
		List list = new LinkedList();
		list.add(75);
		list.add("string");
		list.add("string".getBytes());
		list.add(98L);
		list.add(-14);

		List subList = new LinkedList();
		subList.add("begin sub list");
		subList.add('x');
		subList.add("end sub list");
		list.add(subList);

		list.add((byte) 125);
		list.add(Enon0All.generateRandomInstance(true));

		TestDataOutputStream tdos = new TestDataOutputStream();
		Java8Writer instance = new Java8Writer(tdos.stream());

		instance.write(list);

		RootHeaderDTO rootStruct = new StructureParser(tdos.dataInputStream())
				.parse();
		LOGGER.info(rootStruct.toString());
	}

}
