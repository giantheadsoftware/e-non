/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.io.OutputStream;
import java.util.stream.Stream;
import org.enon.exception.EnonException;
import org.enon.test.util.ExceptionOutputStream;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class WriterTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(WriterTest.class.getName());

	public WriterTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of close method, of class Writer.
	 */
	@Test
	public void testClose() {
		LOGGER.info("close");
		Writer instance = new WriterImpl();
		instance.close();
	}

	@Test(/*expected = EnonException.class*/)
	public void testCloseIOExceptionFail() {
		LOGGER.info("close");
		Exception x = assertThrows(EnonException.class, () -> {
			Writer instance = new WriterImpl(new ExceptionOutputStream());
			instance.close();
		});
	}

	public class WriterImpl extends Writer {

		public WriterImpl() {
			super(new TestDataOutputStream().stream());
		}

		public WriterImpl(OutputStream toStream) {
			super(toStream);
		}

		public Writer write(Stream<Object> objects) {
			return null;
		}
	}

}
