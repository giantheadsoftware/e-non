/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import org.enon.WriterContext;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ByteElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ByteElementTest.class.getName());

	public ByteElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of writeContent method, of class ByteElement.
	 */
	@Test
	public void testWriteContent() throws Exception {
		LOGGER.info("writeContent");
		byte value = (byte) 38;
		TestDataOutputStream tdos = new TestDataOutputStream();
		ByteElement instance = new ByteElement(value);
		new ByteElement.Writer().writeContent(instance, tdos.stream());

		assertEquals(ByteElement.CONTENT_LENGTH, tdos.bytes().length);
		assertEquals(value, tdos.bytes()[0]);
		assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));

		// test negative value
		value = (byte) -138;
		tdos = new TestDataOutputStream();
		instance = new ByteElement(value);
		new ByteElement.Writer().writeContent(instance, tdos.stream());

		assertEquals(ByteElement.CONTENT_LENGTH, tdos.bytes().length);
		assertEquals(value, tdos.bytes()[0]);
		assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));
	}

	@Test
	public void testWrite() throws IOException {
		LOGGER.info("write");
		byte value = (byte) 38;
		TestDataOutputStream tdos = new TestDataOutputStream();
		RootContainer root = new WriterContext(tdos.stream()).getRoot();
		ByteElement instance = (ByteElement) new ByteElement(value).inContainer(root);
		new ByteElement.Writer().write(instance);

		assertEquals(ByteElement.CONTENT_LENGTH + 1, tdos.bytes().length);
		assertEquals(ElementType.BYTE_ELEMENT.prefix, tdos.bytes()[0]);
		assertEquals(value, tdos.bytes()[1]);
		assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));

		// test negative value
		value = (byte) -138;
		tdos = new TestDataOutputStream();
		root = new WriterContext(tdos.stream()).getRoot();
		instance = (ByteElement) new ByteElement(value).inContainer(root);
		new ByteElement.Writer().write(instance);

		assertEquals(ByteElement.CONTENT_LENGTH + 1, tdos.bytes().length);
		assertEquals(ElementType.BYTE_ELEMENT.prefix, tdos.bytes()[0]);
		assertEquals(value, tdos.bytes()[1]);
		assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));
	}
}
