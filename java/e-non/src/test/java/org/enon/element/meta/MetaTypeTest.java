/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.meta;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class MetaTypeTest {

	public MetaTypeTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of forPrefix method, of class MetaType.
	 */
	@Test
	public void testForPrefixAll() {
		System.out.println("forPrefixAll");
		for (MetaType mt : MetaType.values()) {
			byte prefix = mt.prefix;
			MetaType expResult = mt;
			MetaType result = MetaType.forPrefix(prefix);
			assertEquals(expResult, result);
		}
	}

	/**
	 * Test of forPrefix method, of class MetaType.
	 */
	@Test
	public void testForPrefixInvalid() {
		System.out.println("forPrefixInvalid");
		// this prefix is expected to be invalid.  If the prefix is now valid, change it to something else.
		byte prefix = 'q';
		MetaType result = MetaType.forPrefix(prefix);
		assertNull(result);

	}
}
