/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.MetaElement;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.cache.EnonCache;
import org.enon.exception.EnonException;
import org.enon.test.util.Util;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class AbstractElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractElementTest.class);

	private WriterContext writerContext = new WriterContext(null);

	public AbstractElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of inContainer method, of class AbstractElement.
	 */
	@Test
	public void testInContainer() {
		LOGGER.info("inContainer");
		ElementContainer containerElement = writerContext.getRoot();
		AbstractElement instance = new AbstractElementImpl();
		assertNull(instance.getContainer());
		Element expResult = instance;
		Element result = instance.inContainer(containerElement);
		assertEquals(expResult, result);
		assertEquals(containerElement, instance.getContainer());
	}
	
	@Test
	public void getRootContainerDeferred() {
		LOGGER.info("rootContainerDeferred");
		// root container not available when inContainer is called, but is available later...
		MapElement me = new MapElement(Collections.EMPTY_MAP);
		StringElement string = new StringElement("not attache to root yet");
		
		string.inContainer(me);  // there is no connection to root yet
	
		try {
			string.getRootContainer();
			fail("should not be able to reach root container");
		} catch (EnonException x) {
			// success
		}

		me.inContainer(writerContext.getRoot());  // now there is a path to root
		
		assertEquals(writerContext.getRoot(), string.getRootContainer());
	}

	/**
	 * Test of getType method, of class AbstractElement.
	 */
	@Test
	public void testGetTypeAndSize() {
		LOGGER.info("getTypeAndSize");
		AbstractElement instance = new AbstractElementImpl(ElementType.INT_ELEMENT, 4);
		ElementType expResult = ElementType.INT_ELEMENT;
		ElementType result = instance.getType();
		assertEquals(expResult, result);
		assertEquals(4, instance.getSize().getSize());
	}

	/**
	 * Test of getMeta method, of class AbstractElement.
	 */
	@Test
	public void testGetMeta() {
		LOGGER.info("getMeta");
		MetaElement randomMeta = Util.dummyMetaElement();
		List<MetaElement> metaList = Arrays.asList(new MetaElement[]{randomMeta});
		AbstractElement instance = new AbstractElementImpl(ElementType.INT_ELEMENT, 0, metaList);
		List<MetaElement> result = instance.getMeta();
		assertTrue(result.contains(randomMeta), "Expect meta list to contain meta element");
		assertEquals(1, result.size());

		// create another meta element
		MetaElement randomMeta2 = Util.dummyMetaElement();
		// create a container with the 2nd meta
		ElementContainer containerElement = new ContainerElementImpl(ElementType.MAP_ELEMENT, 0, Arrays.asList(new MetaElement[]{randomMeta2}));
		// put an element in the container
		instance.inContainer(containerElement);
		// verify the element can access the container's metadata
		result = instance.getMeta();
		// should now get meta from both elements, from child element first
		assertEquals(2, result.size());
		assertEquals(randomMeta, result.get(0), "Expect meta list to contain meta element 1");
		assertEquals(randomMeta2, result.get(1), "Expect meta list to contain meta element 2");
	}

	/**
	 * Test of addMeta method, of class AbstractElement.
	 */
	@Test
	public void testAddMeta() {
		LOGGER.info("addMeta");
		MetaElement meta = Util.dummyMetaElement();
		AbstractElement instance = new AbstractElementImpl(ElementType.INT_ELEMENT, 0);
		assertTrue(instance.getMeta().isEmpty());

		Element expResult = instance;
		Element result = instance.addMeta(meta);
		assertEquals(expResult, result);
		assertEquals(1, instance.getMeta().size());
		assertTrue(result.getMeta().contains(meta));
	}

	/**
	 * Test of asRoot method, of class MapElement.
	 */
	@Test
	public void testAsRoot() {
		LOGGER.info("asRoot");
		AbstractElement instance = new AbstractElementImpl(ElementType.INT_ELEMENT, 0);
		AbstractElement expResult = instance;
		assertFalse(instance.isRoot());
		RootContainer root = writerContext.getRoot();
		AbstractElement result = instance.inContainer(root);
		assertEquals(expResult, result);
		assertTrue(instance.isRoot());
	}

	@Test()
	public void testToString() {
		LOGGER.info("toString");

		AbstractElement instance = new AbstractElementImpl(ElementType.INT_ELEMENT, 4l);

		String value = instance.toString();
		assertTrue(value.contains(instance.getType().name()));
		assertTrue(value.contains(String.valueOf(instance.getSize().getSize())));

		// test null type
		instance = new AbstractElementImpl();

		value = instance.toString();
		assertTrue(value.contains("NO_TYPE"));
	}

	public class AbstractElementImpl extends AbstractElement {

		public AbstractElementImpl() {
			super(null, 0L);
		}

		public AbstractElementImpl(ElementType type, long size) {
			super(type, size);
		}

		public AbstractElementImpl(ElementType type, long size, List<MetaElement> metaList) {
			super(type, size, metaList);
		}

		@Override
		public Object getValue() {
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}
	}

	public class ContainerElementImpl extends AbstractElement implements ElementContainer {

		private WriterContext writerContext;
		private ReaderContext readerContext;

		public ContainerElementImpl(ElementType type, long size) {
			super(type, size);
		}

		public ContainerElementImpl(ElementType type, long size, List<MetaElement> metaList) {
			super(type, size, metaList);
		}

		@Override
		public EnonCache<String, Long> getMapIds() {
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public Object getValue() {
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

		public void setWriterContext(WriterContext writerContext) {
			this.writerContext = writerContext;
		}

		public void setReaderContext(ReaderContext readerContext) {
			this.readerContext = readerContext;
		}

		@Override
		public Collection getContents() {
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}

	}
}
