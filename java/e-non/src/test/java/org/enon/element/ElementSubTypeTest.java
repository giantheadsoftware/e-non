/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.array.ArrayElementSubtype;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ElementSubTypeTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(ElementSubTypeTest.class.getName());

	
	public ElementSubTypeTest() {
	}
	
	@BeforeAll
	public static void setUpClass() {
	}
	
	@AfterAll
	public static void tearDownClass() {
	}
	
	@BeforeEach
	public void setUp() {
	}
	
	@AfterEach
	public void tearDown() {
	}


	/**
	 * Test of forPrefix method, of class ElementSubType.
	 */
	@Test
	public void testForPrefixAll() {
		LOGGER.info("forPrefixAll");
		for (ArrayElementSubtype est : ArrayElementSubtype.values()) {
			byte prefix = est.prefix;
			ArrayElementSubtype expResult = est;
			ArrayElementSubtype result = ArrayElementSubtype.forPrefix(prefix);
			if (prefix != 0) {
				assertEquals(expResult, result);
			} else {
				assertNull(result);
			}
		}
	}
	
}
