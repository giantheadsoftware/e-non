/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.element.ConstantElement;
import org.enon.element.ElementType;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class BitArrayElementTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(BitArrayElementTest.class.getName());

	
	public BitArrayElementTest() {
	}
	
	@BeforeAll
	public static void setUpClass() {
	}
	
	@AfterAll
	public static void tearDownClass() {
	}
	
	@BeforeEach
	public void setUp() {
	}
	
	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getContentBytes method, of class BitArrayElement.
	 */
	@Test
	public void testGetContentBytes() {
		LOGGER.info("getContentBytes");
		boolean[] array = new boolean[]{true, false, false, false, true, false, true, false};  //length 8
		byte[] bits = new byte[]{(byte)0x8A};
		BitArrayElement instance = new BitArrayElement(array);
		byte[] result = instance.getContentBytes();
		assertArrayEquals(bits, result);
		
		array = new boolean[]{true, false, false, false, true, false, true};  //length 7
		bits = new byte[]{(byte)0x8A};
		instance = new BitArrayElement(array);
		result = instance.getContentBytes();
		assertArrayEquals(bits, result);
		
		array = new boolean[]{false, true, true, true, false, true, false, true, true};  //length 9
		bits = new byte[]{(byte)0x75, (byte)0x80};
		instance = new BitArrayElement(array);
		result = instance.getContentBytes();
		assertArrayEquals(bits, result);
		
		assertSame(result, instance.getContentBytes());
	}
	
	@Test
	public void testAsElements() {
		LOGGER.info("asElements");
		boolean[] array = new boolean[]{true, false, false, false, true, false, true, false};  //length 8

		BitArrayElement instance = new BitArrayElement(array);
		List<ConstantElement> elements = instance.asElements();
		assertEquals(array.length, elements.size());
		int i = 0;
		for (ConstantElement e : elements) {
			assertEquals(array[i++], e.getValue());
		}
	}
	
	@Test
	public void testReaderRead() throws IOException{
		LOGGER.info("readerRead");
		
		boolean[] array = new boolean[]{false, true, true, true, false, true, false, true, true};  //length 9
		BitArrayElement instance = new BitArrayElement(array);
		
		TestDataOutputStream tdos = new TestDataOutputStream();
		WriterContext ctx = new WriterContext(tdos.stream());
		instance.inContainer(ctx.getRoot());
		
		new BitArrayElement.Writer().write(instance);
		
		InputStream in = tdos.inputStream();
		assertEquals(ElementType.ARRAY_ELEMENT.prefix, in.read());
		assertEquals(ElementType.FALSE_ELEMENT.prefix, in.read());
		
		BitArrayElement result = new BitArrayElement.Reader().read(new ReaderContext(in));
		
		assertArrayEquals(array, result.getValue());
		
		// repeat test with opposite values - and add up to 7 extra bits
		
		array = new boolean[]{
			true, false, false, false, true, false, true, false, 
			false, true, false, false, true, false, true};  //length 15
		instance = new BitArrayElement(array);
		
		tdos = new TestDataOutputStream();
		ctx = new WriterContext(tdos.stream());
		instance.inContainer(ctx.getRoot());
		
		new BitArrayElement.Writer().write(instance);
		
		in = tdos.inputStream();
		assertEquals(ElementType.ARRAY_ELEMENT.prefix, in.read());
		assertEquals(ElementType.FALSE_ELEMENT.prefix, in.read());
		
		result = new BitArrayElement.Reader().read(new ReaderContext(in));
		
		assertArrayEquals(array, result.getValue());
		
		// repeat test with multiple of 8 bits
		
		array = new boolean[]{
			true, false, false, false, true, false, true, false, 
			false, true, false, false, true, false, true, true};  //length 16
		instance = new BitArrayElement(array);
		
		tdos = new TestDataOutputStream();
		ctx = new WriterContext(tdos.stream());
		instance.inContainer(ctx.getRoot());
		
		new BitArrayElement.Writer().write(instance);
		
		in = tdos.inputStream();
		assertEquals(ElementType.ARRAY_ELEMENT.prefix, in.read());
		assertEquals(ElementType.FALSE_ELEMENT.prefix, in.read());
		
		result = new BitArrayElement.Reader().read(new ReaderContext(in));
		
		assertArrayEquals(array, result.getValue());
	}
}
