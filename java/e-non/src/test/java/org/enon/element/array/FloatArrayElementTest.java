/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.element.ElementType;
import org.enon.element.FloatElement;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class FloatArrayElementTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(FloatArrayElementTest.class.getName());

	
	public FloatArrayElementTest() {
	}
	
	@BeforeAll
	public static void setUpClass() {
	}
	
	@AfterAll
	public static void tearDownClass() {
	}
	
	@BeforeEach
	public void setUp() {
	}
	
	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getContentBytes method, of class ArrayElement.
	 * @throws java.io.IOException
	 */
	@Test
	public void testGetContentBytes() throws IOException {
		LOGGER.info("getContentBytes");
		
		FloatArrayElement instance = new FloatArrayElement(new float[]{0f, Float.MIN_VALUE, -Float.MIN_VALUE, Float.MAX_VALUE, -Float.MAX_VALUE});
		byte[] result = instance.getContentBytes();
		assertEquals(instance.entrySize * instance.getSize().getSize(), result.length);
		// should return same instance next time
		assertSame(result, instance.getContentBytes());
		// verify network byte order
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(result));
		for (float value : instance.getValue()) {
			assertEquals(value, dis.readFloat(), Float.MIN_VALUE);
		}
	}
	
	@Test
	public void testReaderRead() throws IOException {
		LOGGER.info("readerRead");
		FloatArrayElement instance = new FloatArrayElement(new float[]{0f, Float.MIN_VALUE, Float.MAX_VALUE, -Float.MIN_VALUE, -Float.MAX_VALUE});
		
		TestDataOutputStream tdos = new TestDataOutputStream();
		WriterContext ctx = new WriterContext(tdos.stream());
		instance.inContainer(ctx.getRoot());
		new FloatArrayElement.Writer().write(instance);
		
		InputStream in = tdos.inputStream();
		assertEquals(ElementType.ARRAY_ELEMENT.prefix, in.read());
		assertEquals(ElementType.FLOAT_ELEMENT.prefix, in.read());
		
		FloatArrayElement result = new FloatArrayElement.Reader().read(new ReaderContext(in));
		
		for (int i = 0; i < instance.getValue().length; i++) {
			assertEquals(instance.getValue()[i], result.getValue()[i], Float.MIN_VALUE);
		}
	}
	
	@Test
	public void testAsElements() throws IOException {
		LOGGER.info("asElements");
		
		FloatArrayElement instance = new FloatArrayElement(new float[]{0f, Float.MIN_VALUE, Float.MAX_VALUE, -Float.MIN_VALUE, -Float.MAX_VALUE});
		List<FloatElement> elements = (List<FloatElement>) instance.asElements();
		assertEquals(instance.getValue().length, elements.size());
		int i = 0;
		for (FloatElement e : elements) {
			assertEquals(instance.getValue()[i++], e.getValue(), Float.MIN_VALUE);
		}
	}
	
}
