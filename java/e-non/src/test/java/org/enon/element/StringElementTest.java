/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.SizeCode;
import java.io.DataInputStream;
import java.io.IOException;
import org.enon.WriterContext;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class StringElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(StringElementTest.class.getName());

	public StringElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of writeContent method, of class StringElement.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testWriteContent() throws Exception {
		LOGGER.info("writeContent");
		String value = "test string";
		TestDataOutputStream tdos = new TestDataOutputStream();
		StringElement instance = new StringElement(value);
		new StringElement.Writer().writeContent(instance, tdos.stream());

		assertEquals(value.getBytes().length, tdos.bytes().length);
		assertEquals(value, new String(tdos.bytes(), Element.CHARSET));

		tdos.close();

		// test multibyte value
		value = "\u0afe unicode \u0e01";
		tdos = new TestDataOutputStream();
		instance = new StringElement(value);
		new StringElement.Writer().writeContent(instance, tdos.stream());

		assertEquals(value.getBytes().length, tdos.bytes().length);
		assertEquals(value, new String(tdos.bytes(), Element.CHARSET));
		assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));

		tdos.close();
	}

	@Test
	public void testWriteSmall() throws IOException {
		LOGGER.info("write");
		String value = "New test String";
		int length = value.getBytes().length;
		TestDataOutputStream tdos = new TestDataOutputStream();
		RootContainer root = new WriterContext(tdos.stream()).getRoot();
		StringElement instance = (StringElement) new StringElement(value).inContainer(root);
		new StringElement.Writer().write(instance);

		assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));

		assertEquals(length + 2, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		// read the string prefix
		assertEquals(ElementType.STRING_ELEMENT.prefix, dis.readByte());
		// read the size byte
		assertEquals(length, dis.readUnsignedByte());
		// read the string data
		assertEquals(value, new String(tdos.bytes(), 2, length, Element.CHARSET));

		tdos.close();
	}

	@Test
	public void testWriteMedium() throws IOException {
		LOGGER.info("write");
		final int contentOffset = 4;
		String value = "";
		for (int i = 0; i < 260; i++) {
			value += "x";
		}
		int length = value.getBytes().length;
		TestDataOutputStream tdos = new TestDataOutputStream();
		RootContainer root = new WriterContext(tdos.stream()).getRoot();
		StringElement instance = (StringElement) new StringElement(value).inContainer(root);
		new StringElement.Writer().write(instance);

		assertTrue(instance.toString().contains("..."));

		assertEquals(length + contentOffset, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		// read the string prefix
		assertEquals(ElementType.STRING_ELEMENT.prefix, dis.readByte());
		// read the size prefix
		assertEquals((byte) SizeCode.MEDIUM.code, dis.readByte());
		// read the size value
		assertEquals(length, dis.readUnsignedShort());
		// read the string data
		assertEquals(value, new String(tdos.bytes(), contentOffset, length, Element.CHARSET));

		tdos.close();
	}

	@Test
	public void testWriteLarge() throws IOException {
		LOGGER.info("write");
		final int contentOffset = 10;
		String value = "";
		for (int i = 0; i < 6500; i++) {
			value += "0123456789\u0e07:";
		}
		int length = value.getBytes().length;
		TestDataOutputStream tdos = new TestDataOutputStream();
		RootContainer root = new WriterContext(tdos.stream()).getRoot();
		StringElement instance = (StringElement) new StringElement(value).inContainer(root);
		new StringElement.Writer().write(instance);

		assertTrue(instance.toString().contains("..."));

		assertEquals(length + contentOffset, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		// read the string prefix
		assertEquals(ElementType.STRING_ELEMENT.prefix, dis.readByte());
		// read the size prefix
		assertEquals((byte) SizeCode.LARGE.code, dis.readByte());
		// read the size value
		assertEquals(length, dis.readLong());
		// read the string data
		assertEquals(value, new String(tdos.bytes(), contentOffset, length, Element.CHARSET));

		tdos.close();
	}
}
