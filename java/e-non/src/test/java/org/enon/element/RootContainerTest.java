/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.EnonConfig;
import org.enon.EnonContext;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.exception.EnonElementException;
import org.enon.exception.EnonReadException;
import org.enon.exception.EnonWriteException;
import org.enon.test.util.ExceptionInputStream;
import org.enon.test.util.ExceptionOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class RootContainerTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(RootContainerTest.class.getName());

	public RootContainerTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of add method, of class RootContainer.
	 */
	@Test(/*expected = EnonElementException.class*/)
	public void testAdd() {
		LOGGER.info("add");
		Exception x = assertThrows(EnonElementException.class, () -> {
			new WriterContext(new ExceptionOutputStream()).getRoot().add(new StringElement(""));
		});
	}

	/**
	 * Test of getContext method, of class RootContainer.
	 */
	@Test
	public void testGetContext() {
		LOGGER.info("getContext");
		EnonContext ctx = new EnonContext(EnonConfig.defaults()) {
		};

		assertSame(ctx, ctx.getRoot().getContext());
	}

	/**
	 * Test of getWriterContext method, of class RootContainer.
	 */
	@Test
	public void testGetWriterContext() {
		LOGGER.info("getWriterContext");
		EnonContext ctx = new WriterContext(new ExceptionOutputStream());

		assertSame(ctx, ctx.getRoot().getWriterContext());

	}

	@Test(/*expected = EnonWriteException.class*/)
	public void testGetWriterContextFail() {
		LOGGER.info("getWriterContextFail");
		Exception x = assertThrows(EnonWriteException.class, () -> {
			EnonContext ctx = new ReaderContext(new ExceptionInputStream());

			assertSame(ctx, ctx.getRoot().getWriterContext());
		});
	}

	/**
	 * Test of getReaderContext method, of class RootContainer.
	 */
	@Test
	public void testGetReaderContext() {
		LOGGER.info("getReaderContext");
		EnonContext ctx = new ReaderContext(new ExceptionInputStream());

		assertSame(ctx, ctx.getRoot().getReaderContext());
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testGetReaderContextFail() {
		LOGGER.info("getReaderContextFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			EnonContext ctx = new WriterContext(new ExceptionOutputStream());

			assertSame(ctx, ctx.getRoot().getReaderContext());
		});
	}

	/**
	 * Test of getContainer method, of class RootContainer.
	 */
	@Test
	public void testGetContainer() {
		LOGGER.info("getContainer");

		assertNull(new WriterContext(new ExceptionOutputStream()).getRoot().getContainer());
	}

}
