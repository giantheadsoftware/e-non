/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.util.List;
import org.enon.element.meta.MetaElement;
import org.enon.exception.EnonElementException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ArrayElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArrayElementTest.class.getName());

	public ArrayElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of create method, of class ArrayElement.
	 */
	@Test
	public void testCreateByteArray() {
		LOGGER.info("createByteArray");
		byte[] array = new byte[]{(byte) 0, Byte.MIN_VALUE, Byte.MAX_VALUE};
		List<MetaElement> metaList = null;
		ByteArrayElement result = ArrayElement.create(array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.BYTE_SUB_ELEMENT, result.getSubType());
		assertSame(ByteArrayElement.Writer.class, result.getBinaryWriter().getClass());

		result = (ByteArrayElement) ArrayElement.create((Object) array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.BYTE_SUB_ELEMENT, result.getSubType());
		assertSame(ByteArrayElement.Writer.class, result.getBinaryWriter().getClass());
	}

	/**
	 * Test of create method, of class ArrayElement.
	 */
	@Test
	public void testCreateBooleanArray() {
		LOGGER.info("createBoolArray");
		boolean[] array = new boolean[]{true, false, true, true, false};
		List<MetaElement> metaList = null;
		BitArrayElement result = ArrayElement.create(array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.FALSE_SUB_ELEMENT, result.getSubType());
		assertSame(BitArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());

		result = (BitArrayElement) ArrayElement.create((Object) array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.FALSE_SUB_ELEMENT, result.getSubType());
		assertSame(BitArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());
	}

	/**
	 * Test of create method, of class ArrayElement.
	 */
	@Test
	public void testCreateShortArray() {
		LOGGER.info("createShortArray");
		short[] array = new short[]{0, Short.MIN_VALUE, Short.MAX_VALUE};
		List<MetaElement> metaList = null;
		ShortArrayElement result = ArrayElement.create(array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.SHORT_SUB_ELEMENT, result.getSubType());
		assertSame(ShortArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());

		result = (ShortArrayElement) ArrayElement.create((Object) array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.SHORT_SUB_ELEMENT, result.getSubType());
		assertSame(ShortArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());
	}

	/**
	 * Test of create method, of class ArrayElement.
	 */
	@Test
	public void testCreateIntArray() {
		LOGGER.info("createIntArray");
		int[] array = new int[]{0, Integer.MIN_VALUE, Integer.MAX_VALUE};
		List<MetaElement> metaList = null;
		IntArrayElement result = ArrayElement.create(array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.INT_SUB_ELEMENT, result.getSubType());
		assertSame(IntArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());

		result = (IntArrayElement) ArrayElement.create((Object) array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.INT_SUB_ELEMENT, result.getSubType());
		assertSame(IntArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());
	}

	/**
	 * Test of create method, of class ArrayElement.
	 */
	@Test
	public void testCreateLongArray() {
		LOGGER.info("createLongArray");
		long[] array = new long[]{0, Long.MIN_VALUE, Long.MAX_VALUE};
		List<MetaElement> metaList = null;
		LongArrayElement result = ArrayElement.create(array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.LONG_SUB_ELEMENT, result.getSubType());
		assertSame(LongArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());

		result = (LongArrayElement) ArrayElement.create((Object) array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.LONG_SUB_ELEMENT, result.getSubType());
		assertSame(LongArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());
	}

	/**
	 * Test of create method, of class ArrayElement.
	 */
	@Test
	public void testCreateFloatArray() {
		LOGGER.info("createFloatArray");
		float[] array = new float[]{0f, Float.MIN_VALUE, -Float.MIN_VALUE, Float.MAX_VALUE, -Float.MAX_VALUE};
		List<MetaElement> metaList = null;
		FloatArrayElement result = ArrayElement.create(array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.FLOAT_SUB_ELEMENT, result.getSubType());
		assertSame(FloatArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());

		result = (FloatArrayElement) ArrayElement.create((Object) array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.FLOAT_SUB_ELEMENT, result.getSubType());
		assertSame(FloatArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());
	}

	/**
	 * Test of create method, of class ArrayElement.
	 */
	@Test
	public void testCreateDoubleArray() {
		LOGGER.info("createDoubleArray");
		double[] array = new double[]{0f, Double.MIN_VALUE, -Double.MIN_VALUE, Double.MAX_VALUE, -Double.MAX_VALUE};
		List<MetaElement> metaList = null;
		DoubleArrayElement result = ArrayElement.create(array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.DOUBLE_SUB_ELEMENT, result.getSubType());
		assertSame(DoubleArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());

		// use the "anonymous" array creator
		result = (DoubleArrayElement) ArrayElement.create((Object) array, metaList);
		assertSame(array, result.getValue());
		assertEquals(ArrayElementSubtype.DOUBLE_SUB_ELEMENT, result.getSubType());
		assertSame(DoubleArrayElement.Writer.class, result.getBinaryWriter().getClass());
		assertEquals(array.length, result.getSize().getSize());
	}

	@Test(/*expected = EnonElementException.class*/)
	public void testCreateAnonArrayNotArrayFail() {
		LOGGER.info("createAnonArrayNotArrayFail");
		Exception x = assertThrows(EnonElementException.class, () -> {
			ArrayElement.create(new Object(), null);
		});
	}

	@Test(/*expected = EnonElementException.class*/)
	public void testCreateAnonArrayWrongTypeFail() {
		LOGGER.info("createAnonArrayWrongTypeFail");
		Exception x = assertThrows(EnonElementException.class, () -> {
			ArrayElement.create(new char[]{}, null);
		});
	}

	@Test(/*expected = EnonElementException.class*/)
	public void testEmptyInvalidSubtypeFail() {
		LOGGER.info("testEmptyInvalidSubtypeFail");
		Exception x = assertThrows(EnonElementException.class, () -> {
			ArrayElement.createEmpty(null, 0, null);  // null is not a valid subtype
		});
	}
}
