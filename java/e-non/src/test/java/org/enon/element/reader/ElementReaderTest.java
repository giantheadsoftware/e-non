/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.reader;

import java.io.IOException;
import org.enon.ReaderContext;
import org.enon.element.Element;
import org.enon.element.meta.Size;
import org.enon.exception.EnonReadException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ElementReaderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElementReaderTest.class.getName());

	public ElementReaderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of read method, of class ElementReader.
	 *
	 * @throws java.lang.Exception
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testReadBytesTooLargeFail() throws Exception {
		LOGGER.info("readBytesTooLargeFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			new ElementReaderImpl().readBytes(new Size(Integer.MAX_VALUE + 1L), null);
		});
	}

	/**
	 * Test of readMeta method, of class ElementReader.
	 */
	@Test
	public void testReadMeta() {
		LOGGER.info("readMeta");

		BinaryElementReader reader = new ElementReaderImpl();
		assertNull(reader.readMetaSize(null));
	}

	///////////////////////////////////////////////////////////////////////////////////////
	public class ElementReaderImpl implements BinaryElementReader {

		@Override
		public Element read(ReaderContext ctx) throws IOException {
			return null;
		}
	}

}
