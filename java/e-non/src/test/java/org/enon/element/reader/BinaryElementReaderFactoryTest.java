/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.reader;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.enon.EnonConfig;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.ElementType;
import org.enon.exception.EnonReadException;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class BinaryElementReaderFactoryTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(BinaryElementReaderFactoryTest.class.getName());

	public BinaryElementReaderFactoryTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of reader method, of class BinaryElementReaderFactory.
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testReaderMissingSubtypeFail() throws Exception {
		LOGGER.info("readerMissingSubtypeFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			TestDataOutputStream tdos = new TestDataOutputStream();
			// write the array element prefix, but don't write the sub-prefix
			tdos.stream().write(ElementType.ARRAY_ELEMENT.prefix);

			BinaryElementReaderFactory instance = new BinaryElementReaderFactory(EnonConfig.defaults());
			instance.reader(tdos.inputStream());
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReaderInvalidSubtypeFail() throws Exception {
		LOGGER.info("readerInvalidSubtypeFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			TestDataOutputStream tdos = new TestDataOutputStream();
			// write the array element prefix, but write an invalid sub-prefix
			tdos.stream().write(ElementType.ARRAY_ELEMENT.prefix);
			tdos.stream().write((byte) 0);

			BinaryElementReaderFactory instance = new BinaryElementReaderFactory(EnonConfig.defaults());
			instance.reader(tdos.inputStream());
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReaderNoReaderFail() throws Exception {
		LOGGER.info("readerNoReaderFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			TestDataOutputStream tdos = new TestDataOutputStream();
			// write the array element prefix, but write an invalid sub-prefix
			tdos.stream().write(ElementType.INT_ELEMENT.prefix);

			// create a custom factyry instance that doesn't know how to resolve the prefix
			BinaryElementReaderFactory instance = new BinaryElementReaderFactory(EnonConfig.defaults()) {
				@Override
				protected Map<ElementType, BinaryElementReader> createReaderMap() {
					return new HashMap<>();
				}

			};
			instance.reader(tdos.inputStream());
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReaderNoSubReaderFail() throws Exception {
		LOGGER.info("readerNoSubReaderFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			TestDataOutputStream tdos = new TestDataOutputStream();
			// write the array element prefix, but write an invalid sub-prefix
			tdos.stream().write(ElementType.ARRAY_ELEMENT.prefix);

			// create a custom factyry instance that doesn't know how to resolve the prefix
			BinaryElementReaderFactory instance = new BinaryElementReaderFactory(EnonConfig.defaults()) {
				@Override
				protected Map<ElementType, BinaryElementReader> createReaderMap() {
					return new HashMap<>();
				}

			};
			instance.reader(tdos.inputStream());
		});
	}

	/**
	 * Test of lookupReader method, of class BinaryElementReaderFactory.
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testLookupReaderInvalidTypeFail() {
		LOGGER.info("lookupReaderInvalidTypeFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			BinaryElementReaderFactory instance = new BinaryElementReaderFactory(EnonConfig.defaults());
			// pass a subtype to a type that does not accept subtypes
			instance.lookupReader(ElementType.INT_ELEMENT, ArrayElementSubtype.BYTE_SUB_ELEMENT);
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testLookupReaderNoSubtyppesFail() {
		LOGGER.info("lookupReaderNoSubtypesFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			// create a custom factyry instance that doesn't initialize subtypes 
			BinaryElementReaderFactory instance = new BinaryElementReaderFactory(EnonConfig.defaults()) {
				@Override
				protected Map<ArrayElementSubtype, BinaryElementReader> createArrayReaderMap() {
					return null;
				}

			};

			instance.lookupReader(ElementType.ARRAY_ELEMENT, ArrayElementSubtype.BYTE_SUB_ELEMENT);
		});
	}

	/**
	 * Test of getReaders method, of class BinaryElementReaderFactory.
	 */
	@Test
	public void testGetReaders() {
		LOGGER.info("getReaders");
		final Map<ElementType, BinaryElementReader> readers = new HashMap<>();
		// create a custom factyry instance that doesn't know how to resolve the prefix
		BinaryElementReaderFactory instance = new BinaryElementReaderFactory(EnonConfig.defaults()) {
			@Override
			protected Map<ElementType, BinaryElementReader> createReaderMap() {
				return readers;
			}
		};

		assertSame(readers, instance.getReaders());
		// should produce same next time
		assertSame(readers, instance.getReaders());
	}
}
