/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.io.IOException;
import org.enon.element.array.ByteArrayElement;
import java.io.InputStream;
import java.util.List;
import java.util.zip.CRC32;
import org.enon.element.AbstractElement;
import org.enon.element.ByteElement;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ByteArrayElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ByteArrayElementTest.class.getName());

	private static final String DATA_PATH = "/" + AbstractElement.class.getName().replace(".", "/") + ".class";

	public ByteArrayElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of writeContent method, of class BlobElement.
	 */
	@Test
	public void testWriteContent() throws Exception {
		LOGGER.info("writeContent");
		InputStream dataStream = getClass().getResourceAsStream(DATA_PATH);
		byte[] data = new byte[dataStream.available()];
		dataStream.read(data);

		ByteArrayElement instance = new ByteArrayElement(data);

		TestDataOutputStream tdos = new TestDataOutputStream();
		new ByteArrayElement.Writer().writeContent(instance, tdos.stream());
		byte[] result = tdos.bytes();

		tdos.close();

		assertEquals(data.length, result.length);
		CRC32 expected = new CRC32();
		expected.update(data);
		CRC32 actual = new CRC32();
		actual.update(result);
		assertEquals(expected.getValue(), actual.getValue());
	}
	
	@Test
	public void testAsElements() throws IOException {
		LOGGER.info("asElements");
		InputStream dataStream = getClass().getResourceAsStream(DATA_PATH);
		byte[] data = new byte[dataStream.available()];
		dataStream.read(data);

		ByteArrayElement instance = new ByteArrayElement(data);
		List<ByteElement> elements = (List<ByteElement>) instance.asElements();
		assertEquals(data.length, elements.size());
		int i = 0;
		for (ByteElement e : elements) {
			assertEquals((byte)data[i++], (byte)e.getValue());
		}
	}

}
