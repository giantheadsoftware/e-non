/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.DataInputStream;
import java.io.IOException;
import org.enon.WriterContext;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class IntElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntElementTest.class.getName());

	public IntElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of writeContent method, of class IntElement.
	 */
	@Test
	public void testWriteContent() throws Exception {
		LOGGER.info("writeContent");
		int value = 1964875;
		TestDataOutputStream tdos = new TestDataOutputStream();
		IntElement instance = new IntElement(value);
		new IntElement.Writer().writeContent(instance, (tdos.stream()));

		assertEquals(IntElement.CONTENT_LENGTH, tdos.bytes().length);
		assertEquals(value, tdos.dataInputStream().readInt());

		tdos.close();

		// test negative value
		value = -650199851;
		tdos = new TestDataOutputStream();
		instance = new IntElement(value);
		new IntElement.Writer().writeContent(instance, (tdos.stream()));

		assertEquals(IntElement.CONTENT_LENGTH, tdos.bytes().length);
		assertEquals(value, tdos.dataInputStream().readInt());

		assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));
		
		// assert that contentBytes are the same as what was written
		byte[] contentBytes = instance.getContentBytes();
		assertArrayEquals(contentBytes, tdos.bytes());
		assertSame(contentBytes, instance.getContentBytes()); // should be same instance 2nd time

		tdos.close();
	}

	@Test
	public void testWrite() throws IOException {
		LOGGER.info("write");
		int value = 5143;
		TestDataOutputStream tdos = new TestDataOutputStream();
		RootContainer root = new WriterContext(tdos.stream()).getRoot();
		IntElement instance = (IntElement) new IntElement(value).inContainer(root);
		new IntElement.Writer().write(instance);

		assertEquals(IntElement.CONTENT_LENGTH + 1, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		assertEquals(ElementType.INT_ELEMENT.prefix, dis.readByte());
		assertEquals(value, dis.readInt(), 0.00001);
		assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));

		tdos.close();

		// test negative value
		value = -13832;
		tdos = new TestDataOutputStream();
		root = new WriterContext(tdos.stream()).getRoot();
		instance = (IntElement) new IntElement(value).inContainer(root);
		new IntElement.Writer().write(instance);
		tdos.close();

		assertEquals(IntElement.CONTENT_LENGTH + 1, tdos.bytes().length);
		dis = tdos.dataInputStream();
		assertEquals(ElementType.INT_ELEMENT.prefix, dis.readByte());
		assertEquals(value, dis.readInt(), 0.00001);
		assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));

		tdos.close();
	}
}
