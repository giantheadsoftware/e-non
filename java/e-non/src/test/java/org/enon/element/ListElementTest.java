/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.Size;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.cache.EnonCache;
import org.enon.element.meta.CountMeta;
import org.enon.element.meta.MetaElement;
import org.enon.exception.EnonReadException;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ListElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ListElementTest.class.getName());

	private TestDataOutputStream tdos;
	private WriterContext writerContext;
	private RootContainer emptyRoot;

	public ListElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		tdos = new TestDataOutputStream();
		writerContext = new WriterContext(tdos.stream());
		emptyRoot = writerContext.getRoot();
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testWIthMetadata() {
		List<MetaElement> meta = new ArrayList<>();
		meta.add(new CountMeta(13));

		Set<Element> elements = new HashSet<>();
		elements.add(new ByteElement((byte) 45));
		elements.add(ConstantElement.trueInstance());

		ListElement listElement = new ListElement(elements, meta);

		assertTrue(listElement.getValue().containsAll(elements));
		assertEquals(listElement.getValue().size(), elements.size());
		assertTrue(listElement.getMeta().containsAll(meta));
		assertEquals(listElement.getMeta().size(), meta.size());
	}

	/**
	 * Test of getMapIds method, of class ListElement.
	 */
	@Test
	public void testGetMapIds() {
		LOGGER.info("getMapIds");
		ListElement instance = new ListElement(Collections.EMPTY_LIST);
		// set the container with G feature set
		EnonConfig config = new EnonConfig.Builder(EnumSet.of(FeatureSet.G)).build();
		writerContext = new WriterContext(null, config);
		emptyRoot = writerContext.getRoot();
		instance.inContainer(emptyRoot);

		EnonCache<String, Long> expResult = null;
		EnonCache<String, Long> result = instance.getMapIds();
		assertEquals(expResult, result);
		assertNull(result);

		// mapIds cache should pass down from container above
		MapElement mapElement = (MapElement) new MapElement(Collections.EMPTY_MAP).inContainer(emptyRoot);
		instance = new ListElement(Collections.EMPTY_SET).inContainer(mapElement);
		expResult = mapElement.getMapIds();
		result = instance.getMapIds();
		assertSame(expResult, result);
	}

	/**
	 * Test of write method, of class ListElement.
	 *
	 * @throws java.io.IOException
	 */
	@Test
	public void testWriteNoContent() throws IOException {
		LOGGER.info("write");
		ListElement instance = new ListElement(Collections.EMPTY_LIST).inContainer(emptyRoot);
		new ListElement.Writer().write(instance);

		DataInputStream in = tdos.dataInputStream();

		// verify the data length
		assertEquals(2, tdos.bytes().length);
		// verify the prefix
		assertEquals(ElementType.LIST_ELEMENT.prefix, in.readUnsignedByte(), "Element prefix:");
		// verify the size
		assertEquals(0, in.readUnsignedByte());

		in.close();
		tdos.close();
	}

	/**
	 * Test of writeContent method, of class ListElement.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testWriteContent() throws Exception {
		LOGGER.info("writeContent");
		List<Element> list = new LinkedList<>();
		list.add(ConstantElement.trueInstance());
		list.add(ConstantElement.falseInstance());

		ListElement instance = new ListElement(list).inContainer(emptyRoot);

		new ListElement.Writer().writeContent(instance, tdos.stream());

		DataInputStream in = tdos.dataInputStream();

		// verify the first value
		assertEquals(ElementType.TRUE_ELEMENT.prefix, in.readByte());

		// verify the 2nd value
		assertEquals(ElementType.FALSE_ELEMENT.prefix, in.readByte());

		// verify the stream is fully consumed
		assertEquals(0, in.available());

		in.close();
		tdos.close();
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReaderSizeTooLargeFail() throws IOException {
		LOGGER.info("readerSizeTooLargeFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			// create the e-NON stream to appear like a super-long list element
			new Size(Integer.MAX_VALUE + 1L).write(tdos.stream());

			tdos.close();

			// then try to read...
			new ListElement.Reader().read(new ReaderContext(tdos.dataInputStream()));
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testReaderEndOfDataFail() throws IOException {
		LOGGER.info("readerEndOfDataFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			// create the e-NON stream to appear like a list element expecting 2 entries
			new Size(2).write(tdos.stream());
			// only write 1 entry
			writerContext.write(new StringElement("too short").inContainer(emptyRoot));

			tdos.close();
			// then try to read...
			new ListElement.Reader().read(new ReaderContext(tdos.dataInputStream()));
		});
	}

	@Test
	public void testConstructWithCapacityOnly() {
		LOGGER.info("constructWithCapacityOnly");

		assertEquals(10, new ListElement(10).getSize().getSize());
	}
}
