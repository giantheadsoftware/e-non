/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.writer;

import java.io.DataInputStream;
import org.enon.element.ConstantElement;
import org.enon.element.ElementType;
import org.enon.element.StringElement;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class BinaryElementWriterTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(BinaryElementWriterTest.class.getName());

	public BinaryElementWriterTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of writeHeader method, of class BinaryElementWriter.
	 */
	@Test
	public void testWriteHeader() throws Exception {
		LOGGER.info("writeHeader");

		// true element
		TestDataOutputStream tdos = new TestDataOutputStream();
		new ConstantElement.Writer().writeHeader(ConstantElement.trueInstance(), tdos.stream());
		int result = tdos.dataInputStream().readUnsignedByte();
		tdos.close();

		assertEquals(1, tdos.bytes().length);
		assertEquals(ElementType.TRUE_ELEMENT.prefix, result);

		// false element
		tdos = new TestDataOutputStream();
		new ConstantElement.Writer().writeHeader(ConstantElement.falseInstance(), tdos.stream());
		result = tdos.dataInputStream().readUnsignedByte();
		tdos.close();

		assertEquals(1, tdos.bytes().length);
		assertEquals(ElementType.FALSE_ELEMENT.prefix, result);

		// null element
		tdos = new TestDataOutputStream();
		new ConstantElement.Writer().writeHeader(ConstantElement.nullInstance(), tdos.stream());
		result = tdos.dataInputStream().readUnsignedByte();
		tdos.close();

		assertEquals(1, tdos.bytes().length);
		assertEquals(ElementType.NULL_ELEMENT.prefix, result);
	}

	@Test
	public void testWriteHeaderNoMeta() throws Exception {
		LOGGER.info("writeHeaderNoMeta");
		TestDataOutputStream tdos = new TestDataOutputStream();
		StringElement instance = new StringElement("Testable String");
		new StringElement.Writer().writeHeader(instance, tdos.stream());

		// should be able to read the header from the byte stream
		DataInputStream in = tdos.dataInputStream();
		assertEquals(instance.getType().prefix, in.readByte());
		assertEquals(instance.getSize().getSize(), in.readByte());

		tdos.close();
	}


}
