/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.enon.WriterContext;
import org.enon.element.meta.CountMeta;
import org.enon.element.meta.MetaElement;
import org.enon.test.util.TestDataOutputStream;
import org.enon.txt.TxtWriterContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class MapElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(MapElementTest.class.getName());
	private TestDataOutputStream tdos;
	private WriterContext ctx;

	public MapElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		tdos = new TestDataOutputStream();
		ctx = new WriterContext(tdos.stream());
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testWithMetadata() {
		LOGGER.info("withMetadata");
		List<MetaElement> meta = new ArrayList<>();
		meta.add(new CountMeta(13));

		Map<Element, Element> entries = new HashMap<>();
		entries.put(new StringElement("short"), new ShortElement((short) 45));
		entries.put(new StringElement("float"), new FloatElement(19.54f));

		MapElement mapElement = new MapElement(entries, meta);

		assertTrue(mapElement.getMap().keySet().containsAll(entries.keySet()));
		assertEquals(mapElement.getValue().size(), entries.size());
		assertTrue(mapElement.getMeta().containsAll(meta));
		assertEquals(mapElement.getMeta().size(), meta.size());
	}

	/**
	 * Test of getMapIds method, of class MapElement.
	 */
//	@Test
//	public void testGetMapIdsFailWithoutG() {
//		LOGGER.info("getMapIdsFailWithoutG");
//		MapElement instance = new MapElement(Collections.EMPTY_MAP);
//		Cache<String, Long> expResult = null;
//		Cache<String, Long> result = instance.getMapIds();
//		assertEquals(expResult, result);
//		assertNull(result);
//
//		// set the container without G feature set
//		instance.inContainer(emptyRoot);
//		result = instance.getMapIds();
//		assertNull("G feature set is not specified: should have no mapIds", result);
//	}

	/**
	 * Test of getMapIds method, of class MapElement.
	 */
//	@Test
//	public void testGetMapIds() {
//		LOGGER.info("getMapIds");
//		MapElement instance = new MapElement(Collections.EMPTY_MAP);
//		instance.inContainer(emptyRoot);
//		Cache<String, Long> expResult = null;
//		Cache<String, Long> result = instance.getMapIds();
//		assertEquals(expResult, result);
//		assertNull(result);
//
//		// set the container with G feature set
//		EnonConfig config = new EnonConfig.Builder(EnumSet.of(FeatureSet.G)).build();
//		writerContext = new WriterContext(null, config);
//		emptyRoot = new RootElement(new Prolog(EnonConfig.FORMAT_VERSION, config.getFeatures()));
//		instance.inContainer(emptyRoot);
//		result = instance.getMapIds();
//		assertNotNull(result);
//
//		// mapIds cache should pass down from container above
//		instance = (MapElement) new MapElement(Collections.EMPTY_MAP).inContainer(instance);
//		expResult = result;
//		result = instance.getMapIds();
//		assertSame(expResult, result);
//	}

	/**
	 * Test of cacheMapId method, of class MapElement.
	 */
//	@Test
//	public void testCacheMapId() {
//		LOGGER.info("cacheMapId");
//		MapElement<Element, Element> instance = new MapElement(Collections.EMPTY_MAP);
//		// set the container with G feature set
//		writer = new Writer(null, new EnonConfig.Builder(EnumSet.of(FeatureSet.G)).build());
//		writerContext = new WriterContext(writer);
//		emptyRoot = new RootElement(writerContext);
//		instance.inContainer(emptyRoot);
//		Size myMapId = instance.cacheMapId();
//
//		Long id = instance.getMapIds().get(instance.getElementId());
//		assertEquals(myMapId.getSize(), (long) id);
//	}

	/**
	 * Test of write method, of class MapElement.
	 *
	 * @throws java.io.IOException
	 */
	@Test
	public void testWriteNoContent() throws IOException {
		LOGGER.info("write");
		MapElement instance = new MapElement(Collections.EMPTY_MAP);
		instance.inContainer(ctx.getRoot());
		new MapElement.Writer().write(instance);

		DataInputStream in = tdos.dataInputStream();
		// verify the data length
		assertEquals(2, tdos.bytes().length);
		// verify the prefix
		assertEquals(ElementType.MAP_ELEMENT.prefix, in.readUnsignedByte());
		// verify the size
		assertEquals(0, in.readUnsignedByte());

		tdos.close();
	}

	/**
	 * Test of writeMapId method, of class MapElement.
	 *
	 * @throws java.lang.Exception
	 */
//	@Test
//	public void testWriteMapIdEnon0() throws Exception {
//		LOGGER.info("writeMapId");
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		DataOutputStream out = new DataOutputStream(baos);
//		MapElement instance = new MapElement(Collections.EMPTY_MAP);
//		instance.inContainer(emptyRoot);
//		instance.writeMapId(out);
//
//		byte[] bytes = baos.toByteArray();
//		DataInputStream in = new DataInputStream(new ByteArrayInputStream(bytes));
//
//		// verify the data length - with eNON-0 no map ID should be written
//		assertEquals(0, bytes.length);
////		// verify the map-id
////		assertEquals(instance.getMyMapId().getSize(), in.readUnsignedByte());
//
//		in.close();
//		out.close();
//	}

	/**
	 * Test of writeMapId method, of class MapElement.
	 *
	 * @throws java.lang.Exception
	 */
//	@Test
//	public void testWriteMapIdEnonG() throws Exception {
//		LOGGER.info("writeMapId");
//		writer = new Writer(null, new EnonConfig.Builder(EnumSet.of(FeatureSet.G)).build());
//		writerContext = new WriterContext(writer);
//		emptyRoot = new RootElement(writerContext);
//
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		DataOutputStream out = new DataOutputStream(baos);
//		MapElement instance = new MapElement(Collections.EMPTY_MAP);
//		instance.inContainer(emptyRoot);
//		instance.writeMapId(out);
//
//		byte[] bytes = baos.toByteArray();
//		DataInputStream in = new DataInputStream(new ByteArrayInputStream(bytes));
//
//		// verify the data length - with eNON-0 no map ID should be written
//		assertEquals(1, bytes.length);
//		// verify the map-id
//		assertEquals(instance.getMyMapId().getSize(), in.readUnsignedByte());
//
//		in.close();
//		out.close();
//	}

	/**
	 * Test of writeContent method, of class MapElement.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testWriteContent() throws Exception {
		LOGGER.info("writeContent");
		Map<Element, Element> map = new LinkedHashMap<>();
		map.put(new StringElement("key1"), ConstantElement.trueInstance());
		map.put(new StringElement("key2"), ConstantElement.falseInstance());

		MapElement mapElement = new MapElement(map);
		mapElement.inContainer(ctx.getRoot());

		new MapElement.Writer().writeContent(mapElement, tdos.stream());

		DataInputStream in = tdos.dataInputStream();

		// verify the first key prefix
		assertEquals(ElementType.STRING_ELEMENT.prefix, in.readUnsignedByte());
		// verify the first key size
		int keyLen = in.readUnsignedByte();
		assertEquals("key1".getBytes("utf-8").length, keyLen);

		// verify the first key
		byte[] inBytes = new byte[keyLen];
		in.read(inBytes);
		assertEquals("key1", new String(inBytes, "utf-8"));

		// verify the first value prefix
		assertEquals(ElementType.TRUE_ELEMENT.prefix, in.readUnsignedByte());

		// verify the 2nd key prefix
		assertEquals(ElementType.STRING_ELEMENT.prefix, in.readUnsignedByte());
		// verify the 2nd key size
		keyLen = in.readUnsignedByte();
		assertEquals("key2".getBytes("utf-8").length, keyLen);

		// verify the first key
		inBytes = new byte[keyLen];
		in.read(inBytes);
		assertEquals("key2", new String(inBytes, "utf-8"));

		// verify the first value prefix
		assertEquals(ElementType.FALSE_ELEMENT.prefix, in.readUnsignedByte());

		// verify the stream is fully consumed
		assertEquals(0, in.available());

		in.close();
		tdos.close();
	}

	@Test
	public void testAlternateConstructors() {
		LOGGER.info("alternateConstructors");

		// test (Collection) constructor
		MapElement me = new MapElement(Collections.EMPTY_LIST);
		assertEquals(0, me.getSize().getSize());
		assertTrue(me.getMeta().isEmpty());

		// test (int) constructor
		me = new MapElement(10);
		assertEquals(10, me.getSize().getSize());
		assertTrue(me.getMeta().isEmpty());
	}
}
