/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element.array;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.element.AbstractElement;
import org.enon.element.ElementType;
import org.enon.element.ShortElement;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ShortArrayElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ShortArrayElementTest.class.getName());

	private static final String DATA_PATH = "/" + AbstractElement.class.getName().replace(".", "/") + ".class";

	public ShortArrayElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getContentBytes method, of class ArrayElement.
	 * @throws java.io.IOException
	 */
	@Test
	public void testGetContentBytes() throws IOException {
		LOGGER.info("getContentBytes");

		ShortArrayElement instance = new ShortArrayElement(new short[]{(short)0, Short.MIN_VALUE, Short.MAX_VALUE});
		byte[] result = instance.getContentBytes();
		assertEquals(instance.entrySize * instance.getSize().getSize(), result.length);

		// should return same instance next time
		assertSame(result, instance.getContentBytes());

		// verify network byte order
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(result));
		for (int value : instance.getValue()) {
			assertEquals(value, dis.readShort());
		}
	}
	
	@Test
	public void testReaderRead() throws IOException {
		LOGGER.info("readerRead");
		ShortArrayElement instance = new ShortArrayElement(new short[]{(short)0, Short.MIN_VALUE, Short.MAX_VALUE});
		
		TestDataOutputStream tdos = new TestDataOutputStream();
		WriterContext ctx = new WriterContext(tdos.stream());
		instance.inContainer(ctx.getRoot());
		new ShortArrayElement.Writer().write(instance);
		
		InputStream in = tdos.inputStream();
		assertEquals(ElementType.ARRAY_ELEMENT.prefix, in.read());
		assertEquals(ElementType.SHORT_ELEMENT.prefix, in.read());
		
		ShortArrayElement result = new ShortArrayElement.Reader().read(new ReaderContext(in));
		
		assertArrayEquals(instance.getValue(), result.getValue());
	}
	
	@Test
	public void testAsElements() throws IOException {
		LOGGER.info("asElements");
		
		ShortArrayElement instance = new ShortArrayElement(new short[]{(short)0, Short.MIN_VALUE, Short.MAX_VALUE});
		List<ShortElement> elements = (List<ShortElement>) instance.asElements();
		assertEquals(instance.getValue().length, elements.size());
		int i = 0;
		for (ShortElement e : elements) {
			assertEquals(instance.getValue()[i++], (short)e.getValue());
		}
	}

}
