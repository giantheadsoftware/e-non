/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.array.ArrayElementSubtype;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ElementTypeTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElementTypeTest.class);

	public ElementTypeTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of forPrefix method, of class ElementType.
	 */
	@Test
	public void testForPrefixAll() {
		LOGGER.info("forPrefixAll");
		for (ElementType et : ElementType.values()) {
			byte prefix = et.prefix;
			ElementType expResult = et;
			ElementType result = ElementType.forPrefix(prefix);
			if (prefix != 0) {
				assertEquals(expResult, result);
			} else {
				assertNull(result);
			}
		}
	}

	/**
	 * Test of forPrefix method, of class ElementType.
	 */
	@Test
	public void testForPrefixInvalid() {
		LOGGER.info("forPrefixInvalid");
		// this prefix is expected to be invalid.  If the prefix is now valid, change it to something else.
		byte prefix = 'q';
		ElementType result = ElementType.forPrefix(prefix);
		assertNull(result);
	}

	@Test
	public void testNanoIntPrefixRange() {
		for (short prefix = 0; prefix <= ElementType.MAX_NANO_INT_PREFIX; prefix++) {
			if (prefix < ElementType.MIN_NANO_INT_PREFIX) {
				assertNotEquals(ElementType.NANO_INT_ELEMENT, ElementType.forPrefix((byte) prefix));
			} else {
				assertEquals(ElementType.NANO_INT_ELEMENT, ElementType.forPrefix((byte) prefix));
			}
		}
	}
	
	@Test
	public void testSubtypes() {
		LOGGER.info("subTypes");
		
		// has subtypes
		for (ElementType type : ElementType.values()) {
			switch (type) {
				case ARRAY_ELEMENT:
					assertTrue(type.hasSubTypes());
					List<? extends ElementSubtype> arraySubTypes = type.getSubTypes();
					for (ArrayElementSubtype subType : ArrayElementSubtype.values()) {
						if (arraySubTypes.contains(subType)) {
							assertTrue(type.isValidSubType(subType));
						} else {
							assertFalse(type.isValidSubType(subType));
						}
					}
					break;
				case TEMPORAL_ELEMENT:
					assertTrue(type.hasSubTypes());
					List<? extends ElementSubtype> temporalSubTypes = type.getSubTypes();
					for (TemporalElementSubtype subType : TemporalElementSubtype.values()) {
						if (temporalSubTypes.contains(subType)) {
							assertTrue(type.isValidSubType(subType));
						} else {
							assertFalse(type.isValidSubType(subType));
						}
					}
					break;
				default:
					assertFalse(type.hasSubTypes());
					assertNull(type.getSubTypes());
					for (ArrayElementSubtype subType : ArrayElementSubtype.values()) {
						assertFalse(type.isValidSubType(subType));
					}
					break;
			}
		}
	}
}
