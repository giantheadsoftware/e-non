/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.util.Map;
import org.enon.WriterContext;
import org.enon.exception.EnonWriteException;
import org.enon.test.util.ExceptionOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class MapEntryElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(MapEntryElementTest.class.getName());

	public MapEntryElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of write method, of class MapEntryElement.
	 */
	@Test(/*expected = EnonWriteException.class*/)
	public void testWrite() {
		LOGGER.info("write");
		Exception x = assertThrows(EnonWriteException.class, () -> {
			MapEntryElement mee = new MapEntryElement(new StringElement("key"), ConstantElement.falseInstance());
			mee.inContainer(new WriterContext(new ExceptionOutputStream()).getRoot());
			new MapEntryElement.Writer().write(mee);
		});
	}

	@Test(/*expected = UnsupportedOperationException.class*/)
	public void testCreateEntryImmutableFail() {
		LOGGER.info("createEntryImmutableFail");

		Exception x = assertThrows(UnsupportedOperationException.class, () -> {
			MapEntryElement<StringElement, LongElement> me = new MapEntryElement<>(new StringElement("key"), new LongElement(55));

			Map.Entry<StringElement, LongElement> creaedEntry = me.getValue();
			creaedEntry.setValue(new LongElement(100));  // should be illegal
		});
	}
}
