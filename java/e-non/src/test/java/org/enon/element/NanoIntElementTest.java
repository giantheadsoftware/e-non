/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import java.util.Arrays;
import org.enon.WriterContext;
import org.enon.exception.EnonElementException;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class NanoIntElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(NanoIntElementTest.class.getName());

	public NanoIntElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of isNanoInt method, of class NanoIntElement.
	 */
	@Test
	public void testIsNanoInt() {
		LOGGER.info("isNanoInt");
		long[] values = new long[]{0L, -64, -63, 64, 65, 20, -15, -100, 200};
		boolean[] expResult = new boolean[]{true, false, true, true, false, true, true, false, false};
		Object[] results = Arrays.stream(values)
				.mapToObj(NanoIntElement::isNanoInt)
				.toArray();
		for (int i = 0; i < expResult.length; i++) {
			assertEquals(expResult[i], results[i], "Expected " + expResult[i] + " for " + values[i]);
		}
	}

	/**
	 * Test of writeHeader method, of class NanoIntElement.
	 *
	 * @throws java.io.IOException
	 */
	@Test
	public void testWriteHeader() throws IOException {
		LOGGER.info("writeHeader");
		for (byte value = -100; value <= 100; value++) {
			try {
				TestDataOutputStream tdos = new TestDataOutputStream();
				new NanoIntElement.Writer().writeHeader(new NanoIntElement(value), tdos.stream());
				int result = tdos.dataInputStream().readUnsignedByte();
				tdos.close();

				assertEquals(1, tdos.bytes().length);
				assertEquals(value + 191, result);
			} catch (EnonElementException x) {
				assertFalse(NanoIntElement.isNanoInt(value));
			}
		}
	}

	/**
	 * Test of writeContent method, of class NanoIntElement.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testWriteContent() throws Exception {
		LOGGER.info("writeContent");
		// passing null as the output stream should be OK - it's not supposed to write anything
		new NanoIntElement.Writer().writeContent(new NanoIntElement((byte)27), null);
	}

	/**
	 * Test of writeHeader method, of class NanoIntElement.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testWrite() throws Exception {
		LOGGER.info("write");
		for (byte value = -100; value <= 100; value++) {
			try {
				TestDataOutputStream tdos = new TestDataOutputStream();
				RootContainer root = new WriterContext(tdos.stream()).getRoot();
				NanoIntElement instance = (NanoIntElement) new NanoIntElement(value).inContainer(root);
				new NanoIntElement.Writer().write(instance);
				int result = tdos.dataInputStream().readUnsignedByte();
				tdos.close();

				assertEquals(1, tdos.bytes().length);
				assertEquals(value + 191, result);
				assertTrue(instance.toString().contains(String.valueOf(instance.getValue())));
			} catch (EnonElementException x) {
				assertFalse(NanoIntElement.isNanoInt(value));
			}
		}
	}
}
