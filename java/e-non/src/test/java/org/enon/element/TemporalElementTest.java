/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import java.io.InputStream;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.bind.temporal.TemporalVO;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class TemporalElementTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(TemporalElementTest.class.getName());
	
	public TemporalElementTest() {
	}
	
	@BeforeAll
	public static void setUpClass() {
	}
	
	@AfterAll
	public static void tearDownClass() {
	}
	
	@BeforeEach
	public void setUp() {
	}
	
	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of create method, of class TemporalElement.
	 * @throws java.io.IOException
	 */
	@Test
	public void testCreate() throws IOException {
		LOGGER.info("create");
		TemporalVO tvo = new TemporalVO.Builder().build(); // empty vo
		assertEquals(0, tvo.accuracy);
		TemporalElement result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.NONE, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().year(1925).build(); // only year
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.YEAR, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().year(1925).month((byte)5).day((byte)17).build(); // only date
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.FULL_DATE, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().year(Short.MAX_VALUE+1).month((byte)5).day((byte)17).build(); // date with year beyond compact range
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.MAP, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).second((byte)15).build(); // hour minute sec
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.LOCAL_TIME_S, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).second((byte)15).nanos(945812500).build(); //  hour minute sec ns
		assertEquals(9, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.MAP, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		long now = System.currentTimeMillis();
		tvo = new TemporalVO.Builder().instant(now).build(); //  instant
		assertEquals(3, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.INSTANT_UTC, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).offsetSeconds(60*15).build(); // hour minute + offset, 15 minutes
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.OFFSET_TIME_HM, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).offsetSeconds(60*17).build(); // hour minute + offset, not 15 minutes
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.MAP, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).offsetSeconds(60*60*19).build(); // hour minute + offset > 18h
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.MAP, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).second((byte)43).offsetSeconds(-60*15).build(); // hour minute sec + offset, 15 minutes
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.OFFSET_TIME_S, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).second((byte)43).nanos(0).offsetSeconds(-60*15).build(); // hour minute sec ns(round) + offset, 15 minutes
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.OFFSET_TIME_S, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().instant(now).offsetSeconds(30*60*15).build(); //  instant, 15minute interval offset
		assertEquals(3, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.OFFSET_INSTANT, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).second((byte)43).nanos((short)0).zoneId("Asia/Tokyo").build(); // hour minute sec + offset, 15 minutes
		assertEquals(0, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.ZONED_TIME_MS, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).second((byte)43).nanos(19000000).zoneId("Asia/Tokyo").build(); // hour minute sec + offset, 15 minutes
		assertEquals(3, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.ZONED_TIME_MS, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().hour((byte)19).minute((byte)25).second((byte)43).nanos((short)19).zoneId("Asia/Tokyo").build(); // hour minute sec + offset, 15 minutes
		assertEquals(9, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.MAP, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		tvo = new TemporalVO.Builder().instant(now).zoneId("Europe/Paris").build(); //  instant, 15minute interval offset
		assertEquals(3, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.ZONED_INSTANT, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
		
		// unusual field configuration: month, day, millis, zoneid
		tvo = new TemporalVO.Builder().month((byte)19).day((byte)25).nanos(43000000).zoneId("Asia/Tokyo").build();
		assertEquals(3, tvo.accuracy);
		result = TemporalElement.create(tvo);
		assertSame(TemporalElementSubtype.MAP, result.getSubType()); 
		assertEquals(result.getSubType().size, result.getSize().getSize());
		assertSame(tvo, result.getValue());
		writeThenRead(result);
	}
	
	private void writeThenRead(TemporalElement te) throws IOException {
		TestDataOutputStream tdos = new TestDataOutputStream();
		WriterContext wCtx = new WriterContext(tdos.stream(), new EnonConfig.Builder(FeatureSet.X).build());
		te.inContainer(wCtx.getRoot());
		
		TemporalElement.Writer writer = new TemporalElement.Writer();
		writer.write(te);
		
		byte[] bytes = tdos.bytes();
		if (0 != te.getSize().getSize()) {
			assertEquals(bytes.length, te.getSize().getSize() + 2);  // size + prefix + subtype prefix
		}
		
		InputStream in = tdos.inputStream();
		ReaderContext rCtx = new ReaderContext(in, wCtx.getConfig());
		
		// read the temporal prefix
		assertEquals(ElementType.TEMPORAL_ELEMENT.prefix, in.read());
		
		TemporalElement.Reader reader = new TemporalElement.Reader();
		TemporalElement result = reader.read(rCtx);
		assertEqual(te.getValue(), result.getValue());

		// should be end of stream
		assertEquals(-1, in.read());
		tdos.close();
	}
	
	private void assertEqual(TemporalVO tvo1, TemporalVO tvo2) {
		assertEquals(tvo1.accuracy, tvo2.accuracy);
		assertEquals(tvo1.day, tvo2.day);
		assertEquals(tvo1.era, tvo2.era);
		assertEquals(tvo1.fieldMask, tvo2.fieldMask);
		assertEquals(tvo1.hour, tvo2.hour);
		assertEquals(tvo1.instant, tvo2.instant);
		assertEquals(tvo1.minute, tvo2.minute);
		assertEquals(tvo1.month, tvo2.month);
		assertEquals(tvo1.nanos, tvo2.nanos);
		assertEquals(tvo1.offsetSeconds, tvo2.offsetSeconds);
		assertEquals(tvo1.sec, tvo2.sec);
		assertEquals(tvo1.year, tvo2.year);
		assertEquals(tvo1.zoneId, tvo2.zoneId);
	}
}
