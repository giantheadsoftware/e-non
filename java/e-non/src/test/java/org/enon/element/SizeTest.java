/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.SizeCode;
import org.enon.element.meta.Size;
import java.io.DataInputStream;
import java.io.IOException;
import org.enon.element.meta.MetaElement;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class SizeTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(SizeTest.class.getName());

	public SizeTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of write method, of class Size.
	 */
	@Test
	public void testWriteSmall() throws Exception {
		LOGGER.info("writeSmall");
		TestDataOutputStream tdos = new TestDataOutputStream();
		Size instance = new Size(0xe4);
		instance.write(tdos.stream());

		// read the size data
		assertEquals(1, tdos.bytes().length);
		DataInputStream in = tdos.dataInputStream();
		assertEquals(instance.getSize(), in.readUnsignedByte());
		tdos.close();
	}

	/**
	 * Test of write method, of class Size.
	 */
	@Test
	public void testWriteMedium() throws Exception {
		LOGGER.info("writeMedium");
		TestDataOutputStream tdos = new TestDataOutputStream();
		Size instance = new Size(0x7b83);
		instance.write(tdos.stream());

		// read the size data
		assertEquals(3, tdos.bytes().length);
		DataInputStream in = tdos.dataInputStream();
		assertEquals((int) SizeCode.MEDIUM.code, in.readByte());
		assertEquals(instance.getSize(), in.readUnsignedShort());
		tdos.close();
	}

	/**
	 * Test of write method, of class Size.
	 */
	@Test
	public void testWriteLarge() throws Exception {
		LOGGER.info("writeLarge");
		TestDataOutputStream tdos = new TestDataOutputStream();
		Size instance = new Size(0x9a7b83);
		instance.write(tdos.stream());

		// read the size data
		assertEquals(9, tdos.bytes().length);
		DataInputStream in = tdos.dataInputStream();
		assertEquals((int) SizeCode.LARGE.code, in.readByte());
		assertEquals(instance.getSize(), in.readLong());
		tdos.close();
	}

	/**
	 * Test of write method, of class Size.
	 */
	@Test
	public void testWriteUnbounded() throws Exception {
		LOGGER.info("writeUnbounded");
		TestDataOutputStream tdos = new TestDataOutputStream();
		Size instance = new Size(-1);
		instance.write(tdos.stream());

		// read the size data
		assertEquals(1, tdos.bytes().length);
		DataInputStream in = tdos.dataInputStream();
		assertEquals((int) SizeCode.UNBOUNDED.code, in.readByte());
		tdos.close();
	}

	@Test
	public void testReaderReadSmall() throws IOException {
		LOGGER.info("readerReadSmall");
		TestDataOutputStream tdos = new TestDataOutputStream();

		// write a small value
		Size small = new Size(250);
		tdos.stream().writeByte(250);

		Size size = new Size.Reader().read(tdos.dataInputStream());

		assertEquals(SizeCode.SMALL, size.getCode());
		assertEquals(250, size.getSize());

		assertTrue(size.toString().contains(size.getCode().toString()));
		assertTrue(size.toString().contains(String.valueOf(size.getSize())));

		tdos.close();
	}

	@Test
	public void testReaderReadMedium() throws IOException {
		LOGGER.info("readerReadMedium");
		TestDataOutputStream tdos = new TestDataOutputStream();

		// write a medium value
		Size medium = new Size(251);
		medium.write(tdos.stream());

		Size size = new Size.Reader().read(tdos.dataInputStream());

		assertEquals(SizeCode.MEDIUM, size.getCode());
		assertEquals(251, size.getSize());

		assertTrue(size.toString().contains(size.getCode().toString()));
		assertTrue(size.toString().contains(String.valueOf(size.getSize())));

		tdos.close();
	}

	@Test
	public void testReaderReadLarge() throws IOException {
		LOGGER.info("readerReadLarge");
		TestDataOutputStream tdos = new TestDataOutputStream();

		// write a large value
		Size large = new Size(0x10000);
		assertEquals(SizeCode.LARGE, large.getCode());
		large.write(tdos.stream());

		Size size = new Size.Reader().read(tdos.dataInputStream());

		assertEquals(SizeCode.LARGE, size.getCode());
		assertEquals(large.getSize(), size.getSize());

		assertTrue(size.toString().contains(size.getCode().toString()));
		assertTrue(size.toString().contains(String.valueOf(size.getSize())));

		tdos.close();
	}

	@Test
	public void testReaderReadUnbounded() throws IOException {
		LOGGER.info("readerReadUnbounded");
		TestDataOutputStream tdos = new TestDataOutputStream();

		// write a large value
		Size large = new Size(-1);
		assertEquals(SizeCode.UNBOUNDED, large.getCode());
		large.write(tdos.stream());

		Size size = new Size.Reader().read(tdos.dataInputStream());

		assertEquals(SizeCode.UNBOUNDED, size.getCode());
		assertEquals(large.getSize(), size.getSize());

		assertTrue(size.toString().contains(size.getCode().toString()));
		assertTrue(size.toString().contains(String.valueOf(size.getSize())));

		tdos.close();
	}

	@Test
	public void testReaderNotSizeCodeResturnNull() throws IOException {
		LOGGER.info("readerNotSizeCodeResturnNull");

		Size size = new Size.Reader().read(MetaElement.META_CODE, null);

		assertNull(size);
	}
}
