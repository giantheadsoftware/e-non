/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import org.enon.element.meta.SizeCode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class SizeCodeTest {

	public SizeCodeTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of forSize method, of class SizeCode.
	 */
	@Test
	public void testForSizeSmall() {
		System.out.println("forSizeSmall");
		long size = 0L;
		SizeCode expResult = SizeCode.SMALL;
		SizeCode result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		size = 1L;
		result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		size = 250L;
		result = SizeCode.forSize(size);
		assertEquals(expResult, result);
	}

	/**
	 * Test of forSize method, of class SizeCode.
	 */
	@Test
	public void testForSizeMedium() {
		System.out.println("forSizeMedium");
		long size = 251L;
		SizeCode expResult = SizeCode.MEDIUM;
		SizeCode result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		size = Short.MAX_VALUE;
		result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		size = 0xFFFFL; // max unsigned short value
		result = SizeCode.forSize(size);
		assertEquals(expResult, result);
	}

	/**
	 * Test of forSize method, of class SizeCode.
	 */
	@Test
	public void testForSizeLarge() {
		System.out.println("forSizeLarge");
		long size = 0x10000L;
		SizeCode expResult = SizeCode.LARGE;
		SizeCode result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		size = 0x10000L; // max short +1
		result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		size = 0xF953201;
		result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		size = Long.MAX_VALUE;
		result = SizeCode.forSize(size);
		assertEquals(expResult, result);
	}

	@Test()
	public void testForSizeUnbounded() {
		System.out.println("forSizeUnbounded");
		SizeCode expResult = SizeCode.UNBOUNDED;

		// test various unbounded sizes (negative long values)
		long size = -1;
		SizeCode result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		size = 0xF9532012;
		result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		size = Long.MAX_VALUE + 1;
		result = SizeCode.forSize(size);
		assertEquals(expResult, result);

		result = SizeCode.forSize(null);
		assertEquals(expResult, result);
	}
}
