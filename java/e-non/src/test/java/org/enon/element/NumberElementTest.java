/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.DataInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.enon.WriterContext;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class NumberElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(NumberElementTest.class.getName());

	private TestDataOutputStream tdos;
	private RootContainer root;
	
	public NumberElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		tdos = new TestDataOutputStream();
		root = new WriterContext(tdos.stream()).getRoot();
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of writeContent method, of class NumberElement.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testWriteContentString() throws Exception {
		LOGGER.info("writeContent");
		String value = "12345";
		NumberElement instance = (NumberElement) new NumberElement(value).inContainer(root);
		new NumberElement.Writer().writeContent(instance, tdos.stream());

		assertEquals(value.getBytes().length, tdos.bytes().length);
		assertEquals(value, new String(tdos.bytes(), Element.CHARSET));

		tdos.close();

		// test float value
		value = "180.54";
		tdos = new TestDataOutputStream();
		instance = (NumberElement) new NumberElement(value).inContainer(root);
		new NumberElement.Writer().writeContent(instance, tdos.stream());

		assertEquals(value.getBytes().length, tdos.bytes().length);
		assertEquals(value, new String(tdos.bytes(), Element.CHARSET));

		tdos.close();
	}

	@Test
	public void testWriteNumberString() throws IOException {
		LOGGER.info("writeNumberString");
		String value = "98765.24E+5";
		int length = value.getBytes().length;
		NumberElement instance = (NumberElement) new NumberElement(value).inContainer(root);
		new NumberElement.Writer().write(instance);

		assertEquals(length + 2, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		// read the string prefix
		assertEquals(ElementType.NUMBER_ELEMENT.prefix, dis.readByte());
		// read the size byte
		assertEquals(length, dis.readUnsignedByte());
		// read the string data
		assertEquals(value, new String(tdos.bytes(), 2, length, Element.CHARSET));

		tdos.close();
	}

	@Test
	public void testWriteInteger() throws IOException {
		LOGGER.info("writeInteger");
		Integer value = Integer.parseInt("98765");
		int length = String.valueOf(value).getBytes().length;
		NumberElement instance = (NumberElement) new NumberElement(value).inContainer(root);
		new NumberElement.Writer().write(instance);

		assertEquals(length + 2, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		// read the string prefix
		assertEquals(ElementType.NUMBER_ELEMENT.prefix, dis.readByte());
		// read the size byte
		assertEquals(length, dis.readUnsignedByte());
		// read the string data
		assertEquals((int)value, Integer.parseInt(new String(tdos.bytes(), 2, length, Element.CHARSET)));

		tdos.close();
	}

	@Test
	public void testWriteDouble() throws IOException {
		LOGGER.info("writeDouble");
		Double value = Double.parseDouble("98765.8744");
		int length = String.valueOf(value).getBytes().length;
		NumberElement instance = (NumberElement) new NumberElement(value).inContainer(root);
		new NumberElement.Writer().write(instance);

		assertEquals(length + 2, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		// read the string prefix
		assertEquals(ElementType.NUMBER_ELEMENT.prefix, dis.readByte());
		// read the size byte
		assertEquals(length, dis.readUnsignedByte());
		// read the string data
		assertEquals(value, Double.parseDouble(new String(tdos.bytes(), 2, length, Element.CHARSET)), Double.MIN_VALUE);

		tdos.close();
	}

	@Test
	public void testWriteShort() throws IOException {
		LOGGER.info("writeShort");
		Short value = Short.parseShort("-25641");
		int length = String.valueOf(value).getBytes().length;
		NumberElement instance = (NumberElement) new NumberElement(value).inContainer(root);
		new NumberElement.Writer().write(instance);

		assertEquals(length + 2, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		// read the string prefix
		assertEquals(ElementType.NUMBER_ELEMENT.prefix, dis.readByte());
		// read the size byte
		assertEquals(length, dis.readUnsignedByte());
		// read the string data
		assertEquals((short)value, Short.parseShort(new String(tdos.bytes(), 2, length, Element.CHARSET)));

		tdos.close();
	}

	@Test
	public void testWriteBigInt() throws IOException {
		LOGGER.info("writeBigInt");
		BigInteger value = new BigInteger(String.valueOf(Long.MAX_VALUE) + "25");
		int length = String.valueOf(value).getBytes().length;
		NumberElement instance = (NumberElement) new NumberElement(value).inContainer(root);
		new NumberElement.Writer().write(instance);

		assertEquals(length + 2, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		// read the string prefix
		assertEquals(ElementType.NUMBER_ELEMENT.prefix, dis.readByte());
		// read the size byte
		assertEquals(length, dis.readUnsignedByte());
		// read the string data
		assertEquals(value, new BigInteger(new String(tdos.bytes(), 2, length, Element.CHARSET)));

		tdos.close();
	}

	@Test
	public void testWriteBigDecimal() throws IOException {
		LOGGER.info("writeBigDecimal");
		BigDecimal value = new BigDecimal(String.valueOf(Long.MAX_VALUE) + "25.6541258984351");
		int length = String.valueOf(value).getBytes().length;
		NumberElement instance = (NumberElement) new NumberElement(value).inContainer(root);
		new NumberElement.Writer().write(instance);

		assertEquals(length + 2, tdos.bytes().length);
		DataInputStream dis = tdos.dataInputStream();
		// read the string prefix
		assertEquals(ElementType.NUMBER_ELEMENT.prefix, dis.readByte());
		// read the size byte
		assertEquals(length, dis.readUnsignedByte());
		// read the string data
		assertEquals(value, new BigDecimal(new String(tdos.bytes(), 2, length, Element.CHARSET)));

		tdos.close();
	}
}
