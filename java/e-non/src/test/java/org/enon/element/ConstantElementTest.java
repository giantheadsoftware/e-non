/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.element;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.enon.ReaderContext;
import org.enon.WriterContext;
import org.enon.exception.EnonReadException;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ConstantElementTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConstantElementTest.class.getName());

	private TestDataOutputStream tdos;
	private RootContainer root;
	public ConstantElementTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		tdos = new TestDataOutputStream();
		root = new WriterContext(tdos.stream()).getRoot();
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of writeContent method, of class NullElement.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testWriteContent() throws Exception {
		LOGGER.info("writeContent");
		// passing null as the output stream should be OK - it's not supposed to write anything
		new ConstantElement.Writer().writeContent(ConstantElement.nullInstance(), null);
		
		// passing null as the output stream should be OK - it's not supposed to write anything
		new ConstantElement.Writer().writeContent(ConstantElement.falseInstance(), null);
		
		// passing null as the output stream should be OK - it's not supposed to write anything
		new ConstantElement.Writer().writeContent(ConstantElement.trueInstance(), null);
		
		// passing null as the output stream should be OK - it's not supposed to write anything
		new ConstantElement.Writer().writeContent(ConstantElement.positiveInfinityInstance(), null);
		
		// passing null as the output stream should be OK - it's not supposed to write anything
		new ConstantElement.Writer().writeContent(ConstantElement.negativeInfinityInstance(), null);
		
		// passing null as the output stream should be OK - it's not supposed to write anything
		new ConstantElement.Writer().writeContent(ConstantElement.nanInstance(), null);
	}

	/**
	 * Test of write method, of class NullElement.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testWriteNull() throws Exception {
		LOGGER.info("writeNull");
		new ConstantElement.Writer().write(ConstantElement.nullInstance().inContainer(root));
		int result = tdos.dataInputStream().readUnsignedByte();
		tdos.close();

		assertEquals(1, tdos.bytes().length);
		assertEquals(ElementType.NULL_ELEMENT.prefix, result);
	}
	
	@Test
	public void testWriteFalse() throws Exception {
		LOGGER.info("writeFalse");
		new ConstantElement.Writer().write(ConstantElement.falseInstance().inContainer(root));
		int result = tdos.dataInputStream().readUnsignedByte();
		tdos.close();

		assertEquals(1, tdos.bytes().length);
		assertEquals(ElementType.FALSE_ELEMENT.prefix, result);
	}
	
	@Test
	public void testWriteTrue() throws Exception {
		LOGGER.info("writeTrue");
		
		new ConstantElement.Writer().write(ConstantElement.trueInstance().inContainer(root));

		int result = tdos.dataInputStream().readUnsignedByte();
		tdos.close();

		assertEquals(1, tdos.bytes().length);
		assertEquals(ElementType.TRUE_ELEMENT.prefix, result);
	}
	
	@Test
	public void testWritePosInf() throws Exception {
		LOGGER.info("writePosInf");
		
		new ConstantElement.Writer().write(ConstantElement.positiveInfinityInstance().inContainer(root));

		int result = tdos.dataInputStream().readUnsignedByte();
		tdos.close();

		assertEquals(1, tdos.bytes().length);
		assertEquals(ElementType.INFINITY_POS_ELEMENT.prefix, result);
	}
	
	@Test
	public void testWriteNegInf() throws Exception {
		LOGGER.info("writeNegInf");
		
		new ConstantElement.Writer().write(ConstantElement.negativeInfinityInstance().inContainer(root));

		int result = tdos.dataInputStream().readUnsignedByte();
		tdos.close();

		assertEquals(1, tdos.bytes().length);
		assertEquals(ElementType.INFINITY_NEG_ELEMENT.prefix, result);
	}
	
	@Test
	public void testWriteNan() throws Exception {
		LOGGER.info("writeNan");
		
		new ConstantElement.Writer().write(ConstantElement.nanInstance().inContainer(root));

		int result = tdos.dataInputStream().readUnsignedByte();
		tdos.close();

		assertEquals(1, tdos.bytes().length);
		assertEquals(ElementType.NAN_ELEMENT.prefix, result);
	}
	
	@Test
	public void testInstanceOf() {
		LOGGER.info("instanceOf");
		for (ElementType constanttType : ConstantElement.CONSTANT_TYPES) {
			assertSame(constanttType, ConstantElement.instanceOf(constanttType).type);
		}
	}
}
