/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.enon.element.array.ByteArrayElement;
import org.enon.element.ByteElement;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.FloatElement;
import org.enon.element.IntElement;
import org.enon.element.ListElement;
import org.enon.element.LongElement;
import org.enon.element.MapElement;
import org.enon.element.NanoIntElement;
import org.enon.element.NumberElement;
import org.enon.element.RootContainer;
import org.enon.element.ShortElement;
import org.enon.element.StringElement;
import org.enon.element.array.ArrayElement;
import org.enon.element.array.BitArrayElement;
import org.enon.element.array.DoubleArrayElement;
import org.enon.element.array.FloatArrayElement;
import org.enon.element.array.IntArrayElement;
import org.enon.element.array.LongArrayElement;
import org.enon.element.array.ShortArrayElement;
import org.enon.exception.EnonReadException;
import org.enon.test.util.ExceptionInputStream;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ReaderContextTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReaderContextTest.class.getName());

	private static final List<Element> ELEMENT_LIST = Arrays.asList(new Element[]{
		new StringElement("list entry"),
		new NanoIntElement((byte) 0)
	});

	private static final Map<Element, Element> ELEMENT_MAP = new LinkedHashMap<>();

	private static Element[] allElements;

	public ReaderContextTest() {
	}

	@BeforeAll
	public static void setUpClass() {
		// write one of everything to kitchenSink
		ELEMENT_MAP.clear();
		ELEMENT_MAP.put(new StringElement("key1"), ConstantElement.falseInstance());
		ELEMENT_MAP.put(new IntElement(12345), new NumberElement("1234567890"));

		allElements = new Element[]{
			new ByteArrayElement("String as a BLOB".getBytes()),
			new ByteElement((byte) 0xAF),
			new StringElement("g"),
			new DoubleElement(-5514.8874),
			ConstantElement.falseInstance(),
			new FloatElement(1206f),
			new IntElement(99019852),
			new LongElement(888444411),
			new NanoIntElement((byte) -15),
			ConstantElement.nullInstance(),
			new NumberElement("555"),
			new ShortElement((short) 13000),
			new StringElement("testable string element"),
			new ListElement(ELEMENT_LIST),
			new MapElement(ELEMENT_MAP),
			ConstantElement.trueInstance(),
			ConstantElement.nanInstance(),
			ConstantElement.positiveInfinityInstance(),
			ConstantElement.negativeInfinityInstance(),
			new ShortArrayElement(new short[]{(short) 0, Short.MIN_VALUE, Short.MAX_VALUE}),
			new IntArrayElement(new int[]{0, Integer.MIN_VALUE, Integer.MAX_VALUE}),
			new LongArrayElement(new long[]{0, Long.MIN_VALUE, Long.MAX_VALUE}),
			new FloatArrayElement(new float[]{0, Float.MIN_VALUE, Float.MAX_VALUE, -Float.MIN_VALUE, -Float.MAX_VALUE}),
			new DoubleArrayElement(new double[]{0, Double.MIN_VALUE, Double.MAX_VALUE, -Double.MIN_VALUE, -Double.MAX_VALUE}),
			new BitArrayElement(new boolean[]{true, false, true, true, false, true, false, false})
		};
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getInputStream method, of class ReaderContext.
	 */
	@Test
	public void testGetInputStream() {
		LOGGER.info("getInputStream");

		InputStream in = new ExceptionInputStream();
		ReaderContext ctx = new ReaderContext(in);
		assertTrue(ctx.getDataInputStream() instanceof DataInputStream);
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testgetVersionBeforeReadPrologFail() {
		LOGGER.info("getVersionBeforeReadPrologFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			ReaderContext ctx = new ReaderContext(new ExceptionInputStream());
			ctx.getVersion();
		});
	}

	@Test(/*expected = EnonReadException.class*/)
	public void testgetFeaturesBeforeReadPrologFail() {
		LOGGER.info("getFeaturesBeforeReadPrologFail");

		Exception x = assertThrows(EnonReadException.class, () -> {
			ReaderContext ctx = new ReaderContext(new ExceptionInputStream());
			ctx.getRequiredFeatures();
		});
	}

	/**
	 * Test of parseNextElement method, of class Reader.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void testParseNextElementOK() throws Exception {
		LOGGER.info("parseNextElementOK");
		// write all of the elements to a test stream (prolog is not written)
		final TestDataOutputStream kitchenSink = new TestDataOutputStream();

		WriterContext writerCtx = new WriterContext(kitchenSink.stream(), EnonConfig.defaults());
		RootContainer root = writerCtx.getRoot();

		// write a bunch of elements (prolog not written)
		for (Element e : allElements) {
			e.inContainer(root);
			writerCtx.write(e);
		}

		// create a ReaderContext to read the elements
		ReaderContext instance = new ReaderContext(kitchenSink.dataInputStream());

		int count = 0;

		// verify the elements are read in the same order they originally appeared.
		for (Element e : allElements) {
			Element result = instance.parseNextElement();
			assertNotNull(result, "result " + count + " is null");
			assertEquals(e.getType(), result.getType());
			assertTrue(e.toString().contains(e.getType().toString()));
			if (e.getValue() instanceof Comparable) {
				assertEquals(e.getValue(), result.getValue(), "Value of " + e.getType().name() + " value mismatch:");
			} else if (e.getType() == ElementType.BLOB_ELEMENT) {
				assertArrayEquals((byte[]) e.getValue(), (byte[]) result.getValue());
			} else if (e.getType() == ElementType.NULL_ELEMENT) {
				assertEquals(e.getType(), result.getType());
			} else if (e.getType() == ElementType.LIST_ELEMENT) {
				assertEquals(e.getType(), result.getType());
				assertEquals(ELEMENT_LIST.size(), ((ListElement) e).getValue().size());
				assertEquals(ELEMENT_LIST, e.getValue());
			} else if (e.getType() == ElementType.MAP_ELEMENT) {
				assertEquals(e.getType(), result.getType());
				assertEquals(ELEMENT_MAP.size(), ((MapElement) e).getValue().size());
				assertEquals(ELEMENT_MAP, ((MapElement) e).getMap());
			} else if (e.getType() == ElementType.ARRAY_ELEMENT) {
				switch (((ArrayElement) e).getSubType()) {
					case BYTE_SUB_ELEMENT:
						assertArrayEquals((byte[]) e.getValue(), (byte[]) result.getValue());
						break;
					case DOUBLE_SUB_ELEMENT:
						for (int i = 0; i < ((double[]) e.getValue()).length; i++) {
							assertEquals(((double[]) e.getValue())[i], ((double[]) result.getValue())[i], Double.MIN_VALUE);
						}
						break;
					case FALSE_SUB_ELEMENT:
						assertArrayEquals((boolean[]) e.getValue(), (boolean[]) result.getValue());
						break;
					case FLOAT_SUB_ELEMENT:
						for (int i = 0; i < ((float[]) e.getValue()).length; i++) {
							assertEquals(((float[]) e.getValue())[i], ((float[]) result.getValue())[i], Double.MIN_VALUE);
						}
						break;
					case INT_SUB_ELEMENT:
						assertArrayEquals((int[]) e.getValue(), (int[]) result.getValue());
						break;
					case LONG_SUB_ELEMENT:
						assertArrayEquals((long[]) e.getValue(), (long[]) result.getValue());
						break;
					case SHORT_SUB_ELEMENT:
						assertArrayEquals((short[]) e.getValue(), (short[]) result.getValue());
						break;
				}
			} else {
				fail("Unexpected element type: " + e.getType().name());
			}
			count++;
		}

		kitchenSink.close();
	}

	/**
	 * Test of parseNextElement method, of class Reader.
	 *
	 * @throws java.lang.Exception
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testParseNextElementBadPrefixFail() throws Exception {
		LOGGER.info("parseNextElementBadPrefixFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			// write all of the elements to a test stream (prolog is not written)
			final TestDataOutputStream tdos = new TestDataOutputStream();
			assertNull(ElementType.forPrefix((byte) 'P'), "This is supposed to be an invalid element prefix: ");
			tdos.stream().writeByte('P');

			ReaderContext instance = new ReaderContext(tdos.dataInputStream());
			tdos.close();

			instance.parseNextElement();
		});
	}
}
