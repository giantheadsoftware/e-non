/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.tool.validate;

import java.io.IOException;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Prolog;
import org.enon.Writer;
import org.enon.WriterContext;
import org.enon.element.StringElement;
import org.enon.test.model.simple.Enon0All;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ValidatorTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidatorTest.class.getName());


	public ValidatorTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}


	/**
	 * Test of isValid method, of class Validator.
	 */
	@Test
	public void testIsValid() {
		LOGGER.info("isValid");

		TestDataOutputStream tdos = new TestDataOutputStream();

		Writer writer = new Writer(tdos.stream());

		Object data = Enon0All.generateRandomInstance(true);

		writer.write(data);

		List<EnonValidationException> errors = ValidationReader.create(tdos.dataInputStream(), writer.getConfig().getFeatures()).validate();
		errors.stream().forEach(error -> LOGGER.error(error.getMessage(), error));
		assertTrue(errors.isEmpty());

	}

	@Test
	public void testIsValidMismatchProlog() {
		LOGGER.info("isValidMismatchProlog");

		TestDataOutputStream tdos = new TestDataOutputStream();

		// set up a writer with extended features
		Writer writer = new Writer(tdos.stream(), new EnonConfig.Builder(EnumSet.of(FeatureSet.X)).build());

		Object data = Enon0All.generateRandomInstance(true);

		writer.write(data);

		List<EnonValidationException> errors = ValidationReader.create(tdos.dataInputStream(), Collections.EMPTY_SET).validate();
		assertFalse(errors.isEmpty());

	}

	@Test
	public void testIsValidRogueElement() throws IOException {
		LOGGER.info("isValidRogueElement");

		TestDataOutputStream tdos = new TestDataOutputStream();

		// write a prolog specifying eNON-0 level
		new Prolog.Writer().write(new Prolog(EnonConfig.FORMAT_VERSION, EnumSet.of(FeatureSet.X)), tdos.stream());

		// write an element not supported by eNON-0
		WriterContext ctx = new WriterContext(tdos.stream());
		ctx.write(new StringElement('c').inContainer(ctx.getRoot()));

		tdos.close();

		List<EnonValidationException> errors = ValidationReader.create(tdos.dataInputStream(), Collections.EMPTY_SET).validate();
		assertFalse(errors.isEmpty());
	}

}
