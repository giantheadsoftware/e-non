/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.tool;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Writer;
import org.enon.element.ElementType;
import org.enon.test.util.TestDataOutputStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class StructureParserTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(StructureParserTest.class.getName());

	public StructureParserTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of write method, of class Java8Writer.
	 */
	@Test
	public void testAllEnon0() {
		LOGGER.info("testAllEnon0");
		List list = generateTestList();

		TestDataOutputStream tdos = new TestDataOutputStream();
		Writer instance = new Writer(tdos.stream());

		instance.write(list);

		StructureParser parser = new StructureParser(tdos.dataInputStream())
				.indentSize(2);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		parser.print(new PrintStream(baos));
		
		String string = baos.toString();
		// these types are not supported by eNON-0
		assertFalse(string.contains(ElementType.ARRAY_ELEMENT.toString()));
		assertFalse(string.contains(ElementType.BYTE_ELEMENT.toString()));
		assertFalse(string.contains(ElementType.FLOAT_ELEMENT.toString()));
		assertFalse(string.contains(ElementType.GLOSSARY_REF_ELEMENT.toString()));
		assertFalse(string.contains(ElementType.LONG_ELEMENT.toString()));
		assertFalse(string.contains(ElementType.MAP_REF_ELEMENT.toString()));
		assertFalse(string.contains(ElementType.SHORT_ELEMENT.toString()));

		LOGGER.info(string);
	}

	/**
	 * Test of write method, of class Java8Writer.
	 */
	@Test
	public void testAllEnonX() {
		LOGGER.info("testAllEnonX");

		TestDataOutputStream tdos = new TestDataOutputStream();
		Writer instance = new Writer(tdos.stream(), new EnonConfig.Builder(EnumSet.of(FeatureSet.X)).build());

		List list = generateTestList();
		instance.write(list);

		StructureParser parser = new StructureParser(tdos.dataInputStream());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		parser.print(new PrintStream(baos));

		String string = baos.toString();
		// these types should be present for eNON-X
		assertTrue(string.contains(ElementType.ARRAY_ELEMENT.toString()));
		assertTrue(string.contains(ElementType.BYTE_ELEMENT.toString()));
		assertTrue(string.contains(ElementType.FLOAT_ELEMENT.toString()));
		assertTrue(string.contains(ElementType.LONG_ELEMENT.toString()));
		assertTrue(string.contains(ElementType.SHORT_ELEMENT.toString()));
		
		// these types are not supported without eNON-G
		assertFalse(string.contains(ElementType.GLOSSARY_REF_ELEMENT.toString()));
		assertFalse(string.contains(ElementType.MAP_REF_ELEMENT.toString()));

		LOGGER.info(string);
	}

	private List generateTestList() {
		List list = new LinkedList();
		list.add(75);
		list.add("string");
		list.add("string".getBytes());
		list.add('x');
		list.add('\u0E21');
		list.add(98L);
		list.add(-14);
		list.add(-123456);
		list.add((long) Integer.MAX_VALUE + 1);
		list.add(256);
		list.add(23.7);
		list.add(19.23f);
		list.add(12345.67890);
		list.add(new BigInteger("12345678901234567890"));
		list.add(new BigDecimal("1234567890.1234567890"));
		list.add(true);
		list.add(false);
		list.add(null);
		
		list.add(new int[]{1, 2, -1, 123});

		List subList = new LinkedList();
		subList.add("begin sub list");
		subList.add('x');
		subList.add("end sub list");
		list.add(subList);

		list.add((byte) 125);
		list.add(new Pojo1("pojo1", 25, 22.5f));
		return list;
	}

	public static class Pojo1 {

		private String name;
		private int age;
		private float size;

		public Pojo1(String name, int age, float size) {
			this.name = name;
			this.age = age;
			this.size = size;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		public float getSize() {
			return size;
		}

		public void setSize(float size) {
			this.size = size;
		}

	}
}
