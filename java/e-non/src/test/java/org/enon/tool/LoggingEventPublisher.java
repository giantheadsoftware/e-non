/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.tool;

import org.enon.event.EnonEvent;
import org.enon.event.ErrorEvent;
import org.enon.event.EventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger $Id: $
 */
public class LoggingEventPublisher implements EventPublisher{
	private static final Logger LOGGER = LoggerFactory.getLogger(LoggingEventPublisher.class.getName());

	private final String name;

	public LoggingEventPublisher(String name) {
		this.name = name;
	}
	
	@Override
	public void complete() {
		LOGGER.info(name+": Operation Complete.");
	}

	@Override
	public void fail(ErrorEvent error) {
		LOGGER.error(name+": Operation failed: "+error.toString());
	}

	@Override
	public void publish(EnonEvent event) {
		switch(event.getType()) {
			case COMPLETE:
				LOGGER.info(name + ": Complete: "+event.toString());
				break;
			case DEBUG:
				LOGGER.debug(name + ": " + event.toString());
				break;
			case INFO:
				LOGGER.info(name + ": " + event.toString());
				break;
			case WARNING:
				LOGGER.warn(name + ": " + event.toString());
				break;
			case ERROR:
				LOGGER.error(name + ": " + event.toString());
				break;
			case ELEMENT_READ:
				LOGGER.info(name + ": " + event.toString());
				break;
		}
	}
	
	

}
