/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.tool.validate;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Writer;
import org.enon.WriterContext;
import org.enon.element.array.ArrayElementSubtype;
import org.enon.element.ElementType;
import org.enon.test.model.simple.Enon0All;
import org.enon.test.model.simple.EnonXAll;
import org.enon.test.util.ExceptionInputStream;
import org.enon.test.util.TestDataOutputStream;
import org.enon.tool.LoggingEventPublisher;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class ValidationReaderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationReaderTest.class.getName());

	private TestDataOutputStream tdos;

	public ValidationReaderTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
		tdos = new TestDataOutputStream();
	}

	@AfterEach
	public void tearDown() {
		tdos.close();
	}

	@Test
	public void testValidate() throws IOException {
		LOGGER.info("validate");

		Writer writer = new Writer(tdos.stream());
		writer.write("Just a string");
		DataInputStream in = tdos.dataInputStream();
		ValidationReader reader = ValidationReader.create(in, Collections.EMPTY_SET)
				.eventPublisher(new LoggingEventPublisher(getClass().getSimpleName()));
		assertTrue(reader.validate().isEmpty());

		// should be at end of stream
		assertEquals(0, in.available());
	}

	/**
	 * Test of readRoot method, of class ValidationReader.
	 *
	 * @throws java.lang.Exception
	 */
	@Test()
	public void testReadRootInvalidFeatureFail() throws Exception {
		LOGGER.info("readRootInvalidFeatureFail");

		// create a writer with extra features
		Writer writer = new Writer(tdos.stream(), new EnonConfig.Builder(EnumSet.of(FeatureSet.X)).build());
		writer.write(new Object());

		ValidationReader reader = ValidationReader.create(tdos.dataInputStream(), EnumSet.noneOf(FeatureSet.class))
				.eventPublisher(new LoggingEventPublisher(getClass().getSimpleName()));

		try {
			reader.read(null);
			fail("should fail");
		} catch (EnonValidationException x) {
			tdos.close();
		} catch (Exception x) {
			tdos.close();
			throw x;
		}

	}

	/**
	 * Test of readRoot method, of class ValidationReader.
	 *
	 * @throws java.lang.Exception
	 */
	@Test(/*expected = EnonValidationException.class*/)
	public void testReadRootIoExceptionFail() throws Exception {
		LOGGER.info("readRootIoExceptionFail");

		Exception x = assertThrows(EnonValidationException.class, () -> {
			ValidationReader.create(new ExceptionInputStream(), Collections.EMPTY_SET)
					.eventPublisher(new LoggingEventPublisher(getClass().getSimpleName()))
					.read(null);
		});
	}

	@Test
	public void testReadEnon0All() {
		LOGGER.info("readEnon0All");
		// create an instance
		Enon0All all = Enon0All.generateRandomInstance(false);

		// write the instance to a stream
		Writer writer = new Writer(tdos.stream());
		writer.write(all);

		ValidationReader reader = ValidationReader.create(tdos.inputStream(), Collections.EMPTY_SET);
		List<EnonValidationException> errors = reader.validate();
		if (!errors.isEmpty()) {
			errors.stream().forEach(exception -> LOGGER.error(exception.getMessage(), exception));
			fail("Validation errors");
		}
	}

	@Test
	public void testReadEnonXAll() {
		LOGGER.info("readeadEnonXAll");
		// create an instance
		EnonXAll all = EnonXAll.generateRandomInstance(false);

		// write the instance to a stream
		Writer writer = new Writer(tdos.stream(), new EnonConfig.Builder(FeatureSet.X).build());
		writer.write(all);

		ValidationReader reader = ValidationReader.create(tdos.inputStream(), EnumSet.of(FeatureSet.X));

		List<EnonValidationException> errors = reader.validate();
		if (!errors.isEmpty()) {
			errors.stream().forEach(exception -> LOGGER.error(exception.getMessage(), exception));
			fail("Validation errors");
		}
	}

	@Test
	public void testSkipByteArray() throws IOException {
		LOGGER.info("skipByteArray");

		DataOutputStream out = tdos.stream();

		WriterContext wc = new WriterContext(out, new EnonConfig.Builder(FeatureSet.X).build());
		wc.writeProlog();

		// Have to manually manufacture a byte array -- the standard writer will output a BLOB prefix
		out.write(ElementType.ARRAY_ELEMENT.prefix);
		out.write(ArrayElementSubtype.BYTE_SUB_ELEMENT.prefix);
		out.write(1);  // write the size
		out.write(15); // write a data entry
		out.flush();

		// attempt to validate and expect errors
		ValidationReader reader = ValidationReader.create(tdos.inputStream(), EnumSet.of(FeatureSet.X));

		List<EnonValidationException> errors = reader.validate();
		if (!errors.isEmpty()) {
			errors.stream().forEach(exception -> LOGGER.error(exception.getMessage(), exception));
			fail("Validation errors");
		}
	}

	@Test
	public void testReadNeedFeatureSetFail() {
		LOGGER.info("readNeedFeatureSetFail");
		// create an X instance
		EnonXAll all = EnonXAll.generateRandomInstance(false);

		// write the instance to a stream
		Writer writer = new Writer(tdos.stream(), new EnonConfig.Builder(FeatureSet.X).build());
		writer.write(all);

		// don't set the X feature set
		ValidationReader reader = ValidationReader.create(tdos.inputStream(), Collections.EMPTY_SET);

		List<EnonValidationException> errors = reader.validate();
		if (errors.isEmpty()) {
			fail("Should fail");
		}
	}

	@Test
	public void testSkipBytesEoDFail() throws IOException {
		LOGGER.info("skipBytesEoDFail");

		DataOutputStream out = tdos.stream();

		WriterContext wc = new WriterContext(out);
		wc.writeProlog();

		// write a prefix, but don't write the necessary bytes
		out.write(ElementType.DOUBLE_ELEMENT.prefix);
		out.flush();

		// attempt to validate and expect errors
		ValidationReader reader = ValidationReader.create(tdos.inputStream(), Collections.EMPTY_SET);

		List<EnonValidationException> errors = reader.validate();
		if (errors.isEmpty()) {
			fail("Should fail");
		}
	}

	@Test
	public void testListIncompleteFail() throws IOException {
		LOGGER.info("listIncompleteFail");

		DataOutputStream out = tdos.stream();

		WriterContext wc = new WriterContext(out);
		wc.writeProlog();  // 2 bytes

		// Manually manufacture a list that is missing its contents
		out.write(ElementType.LIST_ELEMENT.prefix);
		out.write(1);  // write the size
		// don't write the list entry
		out.flush();

		// attempt to validate and expect errors
		ValidationReader reader = ValidationReader.create(tdos.inputStream(), EnumSet.of(FeatureSet.X));

		List<EnonValidationException> errors = reader.validate();
		if (errors.isEmpty()) {
			fail("Should fail");
		}
	}

	@Test
	public void testMetasizeExceptionFail() throws IOException {
		LOGGER.info("metasizeExceptionFail");

		DataOutputStream out = tdos.stream();

		WriterContext wc = new WriterContext(out);
		wc.writeProlog();  // 2 bytes

		// Manually manufacture a string element
		out.write(ElementType.STRING_ELEMENT.prefix);
		out.write("test".getBytes().length);  // write the size
		out.write("test".getBytes(StandardCharsets.UTF_8)); // write the string data
		out.flush();

		// create a stream that will fail at the point of reading the metasize
		ExceptionInputStream in = new ExceptionInputStream(tdos.inputStream(), 3);
		// attempt to validate and expect errors
		ValidationReader reader = ValidationReader.create(in, EnumSet.of(FeatureSet.X));

		List<EnonValidationException> errors = reader.validate();
		if (errors.isEmpty()) {
			fail("Should fail");
		}
	}

	@Test
	public void testListExceptionFail() throws IOException {
		LOGGER.info("listExceptionFail");

		DataOutputStream out = tdos.stream();

		WriterContext wc = new WriterContext(out);
		wc.writeProlog();  // 2 bytes

		// Manually manufacture a list element containing a stringstring element
		out.write(ElementType.LIST_ELEMENT.prefix);
		out.write(1);  // write the size
		out.write(ElementType.STRING_ELEMENT.prefix);
		out.write("test".getBytes().length);  // write the size
		out.write("test".getBytes(StandardCharsets.UTF_8)); // write the string data
		out.flush();

		// create a stream that will fail at the point of reading the first list entry
		ExceptionInputStream in = new ExceptionInputStream(tdos.inputStream(), 4);
		// attempt to validate and expect errors
		ValidationReader reader = ValidationReader.create(in, EnumSet.of(FeatureSet.X));

		List<EnonValidationException> errors = reader.validate();
		if (errors.isEmpty()) {
			fail("Should fail");
		}
	}

	@Test
	public void testArrayMetasizeExceptionFail() throws IOException {
		LOGGER.info("arrayMetasizeExceptionFail");

		DataOutputStream out = tdos.stream();

		WriterContext wc = new WriterContext(out);
		wc.writeProlog();  // 2 bytes

		// Manually manufacture a long array
		out.write(ElementType.ARRAY_ELEMENT.prefix);
		out.write(ArrayElementSubtype.LONG_SUB_ELEMENT.prefix);
		out.write(1);  // write the size
		out.writeLong(15); // write one array entry
		out.flush();

		// create a stream that will fail at the point of reading the metasize
		ExceptionInputStream in = new ExceptionInputStream(tdos.inputStream(), 4);
		// attempt to validate and expect errors
		ValidationReader reader = ValidationReader.create(in, EnumSet.of(FeatureSet.X));

		List<EnonValidationException> errors = reader.validate();
		if (errors.isEmpty()) {
			fail("Should fail");
		}
	}

	@Test
	public void testUnexpectedExceptionFail() throws IOException {
		LOGGER.info("unexpectedExceptionFail");

		DataOutputStream out = tdos.stream();

		WriterContext wc = new WriterContext(out);
		wc.writeProlog();  // 2 bytes

		// Manually maufacture a long array
		out.write(ElementType.ARRAY_ELEMENT.prefix);
		out.write(ArrayElementSubtype.LONG_SUB_ELEMENT.prefix);
		out.write(1);  // write the size
		out.writeLong(15); // write one array entry
		out.flush();

		// create a stream that will fail at the point of reading the metasize
		ExceptionInputStream in = new ExceptionInputStream(tdos.inputStream(), 4, new RuntimeException("random error"));
		// attempt to validate and expect errors
		ValidationReader reader = ValidationReader.create(in, EnumSet.of(FeatureSet.X));

		List<EnonValidationException> errors = reader.validate();
		if (errors.isEmpty()) {
			fail("Should fail");
		}
	}

}
