/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.util.EnumSet;
import java.util.Set;
import org.enon.exception.EnonReadException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class PrologTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrologTest.class.getName());

	public PrologTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	/**
	 * Test of getVersion method, of class Prolog.
	 */
	@Test
	public void testGetVersion() {
		LOGGER.info("getVersion");
		Prolog instance = new Prolog((short) 5, EnumSet.noneOf(FeatureSet.class));
		short expResult = 5;
		short result = instance.getVersion();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getRequiredFeatures method, of class Prolog.
	 */
	@Test
	public void testGetRequiredFeatures() {
		LOGGER.info("getRequiredFeatures");
		Set<FeatureSet> expResult = EnumSet.of(FeatureSet.X, FeatureSet.Z);
		Prolog instance = new Prolog((short) 0, expResult);
		Set<FeatureSet> result = instance.getRequiredFeatures();
		assertNotSame(expResult, result);
		assertTrue(result.containsAll(expResult));
		assertEquals(expResult.size(), result.size());
	}

	/**
	 * Test of testReadability method, of class Prolog.
	 */
	@Test
	public void testTestReadabilityOK() {
		LOGGER.info("testReadabilityOK");
		EnonConfig config = new EnonConfig.Builder().build();
		Prolog instance = new Prolog((short) 0, EnumSet.noneOf(FeatureSet.class));
		instance.testReadability(config);

		config = new EnonConfig.Builder().feature(FeatureSet.X).build();
		instance = new Prolog((short) 0, EnumSet.noneOf(FeatureSet.class));
		instance.testReadability(config);

		instance = new Prolog((short) 0, EnumSet.of(FeatureSet.X));
		instance.testReadability(config);
	}

	/**
	 * Test of testReadability method, of class Prolog.
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testTestReadabilityBadVersionUnderFail() {
		LOGGER.info("testReadabilityBadVersionUnderFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			EnonConfig config = new EnonConfig.Builder().minReadVersion((short) 3).build();
			Prolog instance = new Prolog((short) 0, EnumSet.noneOf(FeatureSet.class));
			instance.testReadability(config);
		});
	}

	/**
	 * Test of testReadability method, of class Prolog.
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testTestReadabilityBadVersionOverFail() {
		LOGGER.info("testReadabilityBadVersionOverFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			EnonConfig config = new EnonConfig.Builder().minReadVersion((short) 3).build();
			Prolog instance = new Prolog((short) 4, EnumSet.noneOf(FeatureSet.class));
			instance.testReadability(config);
		});
	}

	/**
	 * Test of testReadability method, of class Prolog.
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testTestReadabilityFeatureSetFail() {
		LOGGER.info("testReadabilityFeatureSetFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			EnonConfig config = new EnonConfig.Builder().build();
			Prolog instance = new Prolog((short) 0, EnumSet.of(FeatureSet.X));
			instance.testReadability(config);
		});
	}

	/**
	 * Test of testReadability method, of class Prolog.
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testTestReadabilityFeatureSet2Fail() {
		LOGGER.info("testReadabilityFeatureSet2Fail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			EnonConfig config = new EnonConfig.Builder().feature(FeatureSet.X).build();
			Prolog instance = new Prolog((short) 0, EnumSet.of(FeatureSet.X, FeatureSet.G));
			instance.testReadability(config);
		});
	}

	/**
	 * Test of testWritability method, of class Prolog.
	 */
	@Test
	public void testTestWritabilityOK() {
		LOGGER.info("testWritabilityOK");
		EnonConfig config = new EnonConfig.Builder().build();
		Prolog instance = new Prolog((short) 0, EnumSet.noneOf(FeatureSet.class));
		instance.testWritability(config);

		config = new EnonConfig.Builder().feature(FeatureSet.X).build();
		instance = new Prolog((short) 0, EnumSet.noneOf(FeatureSet.class));
		instance.testWritability(config);

		instance = new Prolog((short) 0, EnumSet.of(FeatureSet.X));
		instance.testWritability(config);
	}

	/**
	 * Test of testReadability method, of class Prolog.
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testTestWritabilityBadVersionFail() {
		LOGGER.info("testWritabilityBadVersionFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			EnonConfig config = new EnonConfig.Builder().build();
			Prolog instance = new Prolog((short) (EnonConfig.FORMAT_VERSION + 1), EnumSet.noneOf(FeatureSet.class));

			instance.testWritability(config);
		});
	}

	/**
	 * Test of testReadability method, of class Prolog.
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testTestWritabilityFeatureSetFail() {
		LOGGER.info("testWritabilityFeatureSetFail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			EnonConfig config = new EnonConfig.Builder().build();
			Prolog instance = new Prolog((short) 0, EnumSet.of(FeatureSet.X));

			instance.testWritability(config);
		});
	}

	/**
	 * Test of testReadability method, of class Prolog.
	 */
	@Test(/*expected = EnonReadException.class*/)
	public void testTestWritabilityFeatureSet2Fail() {
		LOGGER.info("testWritabilityFeatureSet2Fail");
		Exception x = assertThrows(EnonReadException.class, () -> {
			EnonConfig config = new EnonConfig.Builder().feature(FeatureSet.X).feature(FeatureSet.S).build();
			Prolog instance = new Prolog((short) 0, EnumSet.of(FeatureSet.X, FeatureSet.G));

			instance.testWritability(config);
		});
	}

}
