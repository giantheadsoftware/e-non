/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author clininger
 */
public class FeatureSetTest {

	public FeatureSetTest() {
	}

	@BeforeAll
	public static void setUpClass() {
	}

	@AfterAll
	public static void tearDownClass() {
	}

	@BeforeEach
	public void setUp() {
	}

	@AfterEach
	public void tearDown() {
	}

	@Test
	public void testToFromBitsetEmpty() {
		byte result = FeatureSet.toBitmask(EnumSet.noneOf(FeatureSet.class));
		assertEquals(0, result);
		assertTrue(FeatureSet.fromBitmask(result).isEmpty());
	}

	@Test
	public void testToFromBitsetOneValue() {
		for (FeatureSet addFeature : FeatureSet.values()) {
			// create a bitmask for each possible feature set, one feature at a time
			byte bitset = FeatureSet.toBitmask(Collections.singleton(addFeature));
			// verify that the correct bit was set
			assertEquals(addFeature.bitmask, bitset);
			// Recreate a Set from the bitset
			Set<FeatureSet> result = FeatureSet.fromBitmask(bitset);
			// verify that only the correct FeatureSet is included in the result
			for (FeatureSet fs : FeatureSet.values()) {
				if (fs == addFeature) {
					assertTrue(result.contains(fs));
				} else {
					assertFalse(result.contains(fs));
				}
			}
		}
	}

	/**
	 * Test of valueOf method, of class FeatureSet.
	 */
	@Test
	public void testToFromBitsetMultiValue() {
		Set<FeatureSet> accumFeatures = EnumSet.noneOf(FeatureSet.class);
		// add each feature to the set until they are all added
		for (FeatureSet addFeature : FeatureSet.values()) {
			accumFeatures.add(addFeature);
			byte bitset = FeatureSet.toBitmask(accumFeatures);
			// Recreate a Set from the bitset
			Set<FeatureSet> result = FeatureSet.fromBitmask(bitset);
			assertEquals(accumFeatures.size(), result.size());
			assertTrue(accumFeatures.containsAll(result));
		}
	}

}
