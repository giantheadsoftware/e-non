/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.enon.json.xform;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import org.enon.EnonConfig;
import org.enon.Xformer;
import org.enon.json.JsonReaderContext;
import org.enon.txt.TxtWriterContext;

/**
 *
 * @author clininger
 * $Id: $
 */
public class JsonEnonTxtXformer extends Xformer {

	/**
	 * Full constructor
	 * @param src source stream, text
	 * @param dest destination stream, binary
	 * @param srcConfig config used to read the source stream
	 * @param destConfig config used to write the output stream
   * @param charset Charset used to write the EnonTxt
	 */
	public JsonEnonTxtXformer(InputStream src, OutputStream dest, EnonConfig srcConfig, EnonConfig destConfig, Charset charset) {
		super(new JsonReaderContext(src, srcConfig), new TxtWriterContext(dest, destConfig, charset));
	}

	/**
	 * Minimal constructor, uses default EnonConfig and UTF-8 charset.
	 * @param src source stream, binary
	 * @param dest destination stream, text
	 */
	public JsonEnonTxtXformer(InputStream src, OutputStream dest) {
		this(src, dest, EnonConfig.defaults(), EnonConfig.defaults(), null);
	}
	
}
