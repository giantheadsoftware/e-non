/*
 * Copyright (c) 2020-2020 Gianthead Software LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.json;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonException;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;
import org.enon.EnonConfig;
import org.enon.FeatureSet;
import org.enon.Prolog;
import org.enon.ReaderContext;
import org.enon.element.Element;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.enon.element.ConstantElement;
import org.enon.element.ListElement;
import org.enon.element.MapElement;
import org.enon.element.MapEntryElement;
import org.enon.element.NumberElement;
import org.enon.element.StringElement;
import org.enon.exception.EnonReadException;

public class JsonReaderContext extends ReaderContext {

  private final InputStreamReader jsonStreamReader;
  private boolean parsed;

  public JsonReaderContext(InputStream jsonStream, EnonConfig config) {
    super(jsonStream, config);
    jsonStreamReader = new InputStreamReader(jsonStream);
  }
  
  public JsonReaderContext(InputStream jsonStream) {
    this(jsonStream, EnonConfig.defaults());
  }

  private Element parseJson() {
    try {
      Object jsonThing = Jsoner.deserialize(jsonStreamReader);
      parsed = true;
      return toElement(jsonThing);
    } catch (JsonException ex) {
      throw new EnonReadException("Unable to parse JSON: ", ex);
    }
  }

  /**
   * Recursively extract elements from the JSON data
   * @param jsonThing
   * @return 
   */
  private Element toElement(Object jsonThing) {
    if (jsonThing == null) {
      return ConstantElement.nullInstance();
    } 
    if (jsonThing.getClass() == Boolean.class) {
      return ((Boolean) jsonThing) ? ConstantElement.trueInstance() : ConstantElement.falseInstance();
    } 
    if (jsonThing instanceof Number) {
      return new NumberElement((Number) jsonThing);
    }
    if (jsonThing instanceof String) {
      return new StringElement((String) jsonThing);
    } 
    if (jsonThing instanceof JsonArray) {
      List<Element> elements = new LinkedList<>();
      ((JsonArray)jsonThing).forEach(jsonElement -> elements.add(toElement(jsonElement)));
      return new ListElement(elements);
    }
    if (jsonThing instanceof JsonObject) {
      List<MapEntryElement<StringElement, Element>> entries = new LinkedList<>();
      ((JsonObject)jsonThing).entrySet().forEach(entry -> entries.add(new MapEntryElement<>(new StringElement(entry.getKey()), toElement(entry.getValue()))));
      return new MapElement(entries);
    }
    return null;
  }

  @Override
  public Prolog readProlog() {
    //  there won't be a prolog in a JSON file, but we know that we don't need any special features for it
    return new Prolog(getConfig().getMinReadVersion(), EnumSet.noneOf(FeatureSet.class));
  }

  @Override
  public Set<FeatureSet> getRequiredFeatures() {
    // just return what's already in the config; this will always pass the validation
    return config.getFeatures();
  }

  @Override
  public Element parseNextElement() throws IOException {
    if (parsed) {
      return null;
    }
    return parseJson();
  }
}
