package org.enon.json;

/*
 * Copyright (c) 2018-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.enon.EnonConfig;
import org.enon.Reader;

import java.io.InputStream;

public class JsonReader extends Reader {

  public JsonReader(InputStream jsonStream, EnonConfig config) {
    super(new JsonReaderContext(jsonStream, config));
  }

  public JsonReader(InputStream jsonStream) {
    super(new JsonReaderContext(jsonStream, EnonConfig.defaults()));
  }

  @Override
  public <T> T read(Class<T> asType) {
    return super.read(asType);
  }
}
