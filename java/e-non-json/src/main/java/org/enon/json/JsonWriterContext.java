/*
 * Copyright (c) 2020-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.enon.json;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.enon.WriterContext;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.ListElement;
import org.enon.element.MapElement;
import org.enon.element.MapEntryElement;
import org.enon.element.TemporalElement;
import org.enon.element.array.ArrayElement;
import org.enon.element.array.ByteArrayElement;
import org.enon.exception.EnonWriteException;

/**
 *
 * @author clininger
 * $Id: $
 */
public class JsonWriterContext extends WriterContext {
  private final Charset charset = StandardCharsets.UTF_8;
    
  private final OutputStream out;

  public JsonWriterContext(OutputStream out) {
    this.out = out;
  }

  @Override
  public void write(Element e) {
    if (e == null) {
      return;
    }
    try {
      switch (e.getType()) {
        case BLOB_ELEMENT:
          writeBlob(out, (ByteArrayElement) e);
          break;
        case BYTE_ELEMENT:
        case DOUBLE_ELEMENT:
        case FLOAT_ELEMENT:
        case INT_ELEMENT:
        case LONG_ELEMENT:
        case NANO_INT_ELEMENT:
        case NUMBER_ELEMENT:
        case SHORT_ELEMENT:
          out.write(String.valueOf(e.getValue()).getBytes(charset));
          break;
          
        case FALSE_ELEMENT:
          out.write(String.valueOf("false").getBytes(charset));
          break;
          
        case TRUE_ELEMENT:
          out.write(String.valueOf("true").getBytes(charset));
          break;
          
        case NULL_ELEMENT:
          out.write(String.valueOf("null").getBytes(charset));
          break;
          
        case STRING_ELEMENT:
          out.write(('"'+escapeString(((String)e.getValue()))+'"').getBytes(charset));
          break;
          
        case ARRAY_ELEMENT:
          writeArray(out, (ArrayElement) e);
          break;
          
        case LIST_ELEMENT:
          writeList(out, (ListElement<? extends Element>) e);
          break;
          
        case MAP_ELEMENT:
          writeMap(out, (MapElement) e);
          break;

        case TEMPORAL_ELEMENT:
          out.write(('"'+((TemporalElement)e).getValue().toIsoString()+'"').getBytes(charset));
          break;

        case INFINITY_POS_ELEMENT:
        case INFINITY_NEG_ELEMENT:
        case NAN_ELEMENT:
          throw new EnonWriteException("JSON does not support +/-Infinity or NaN");
          
      }
    } catch (IOException x) {
      throw new EnonWriteException("Could not write element to JSON stream: ", x);
    }
  }
  
  private String escapeString(String jsonString)  {
    StringBuffer sb = new StringBuffer();
    String insert = null;
    int begin = 0;
    int insertPoint = 0;
    int i;
    for (i = 0; i < jsonString.length(); i++) {
      switch (jsonString.charAt(i)) {
        case '"':
        case '\\':
          insert = "\\"+ jsonString.charAt(i);
          break;
        case '\n':
          insert = "\\n";
          break;
        case '\r':
          insert = "\\r";
          break;
        case '\t':
          insert = "\\t";
          break;
        case 0x08:
          insert = "\\b";
          break;
        case 0x0C:
          insert = "\\f";
          break;
      }
      if (insert != null) {
        sb.append(jsonString.substring(begin, i)).append(insert);
        begin = i+1;
        insert = null;
      }
    }
    if (sb.length() > 0) {
      if (begin < jsonString.length()) {
        sb.append(jsonString.substring(begin, jsonString.length()));
      }
      return sb.toString();
    }
    return jsonString;
  }

  private void writeArray(OutputStream out, ArrayElement e) throws IOException {
    out.write("[".getBytes(charset));
    boolean first = true;
    for (Element subElement : e.asElements()) {
      if (!first) {
        out.write(",".getBytes(charset));
      }
      write(subElement);
      first = false;
    }
    out.write("]".getBytes(charset));
  }

  private void writeList(OutputStream out, ListElement<? extends Element> e) throws IOException {
    out.write("[".getBytes(charset));
    boolean first = true;
    for (Element subElement : e.getContents()) {
      if (!first) {
        out.write(",".getBytes(charset));
      }
      write(subElement);
      first = false;
    }
    out.write("]".getBytes(charset));
  }

  private void writeMap(OutputStream out, MapElement<?, ?> e) throws IOException {
    out.write("{".getBytes(charset));
    boolean first = true;
    for (MapEntryElement<?,?> entry : e.getContents()) {
      if (entry.getEntryKey().getType() == ElementType.STRING_ELEMENT) {
        if (!first) {
          out.write(",".getBytes(charset));
        }
        write(entry.getEntryKey());
        out.write(":".getBytes(charset));
        write(entry.getEntryValue());
        first = false;
      } else {
        throw new EnonWriteException("JSON requires all object keys to be Strings");
      }
    }
    out.write("}".getBytes(charset));
  }
  
  private void writeBlob(OutputStream out, ByteArrayElement e) throws IOException {
    out.write("\"".getBytes(charset));
    out.write(Base64.getEncoder().encode(e.getContentBytes()));
    out.write("\"".getBytes(charset));
  }

  @Override
  public void writeProlog() {
    // there is no prolog
  }
	
}
 