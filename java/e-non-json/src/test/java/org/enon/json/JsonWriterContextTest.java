/*
 * Copyright (c) 2020-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.enon.bind.temporal.TemporalVoFactory;
import org.enon.element.ConstantElement;
import org.enon.element.DoubleElement;
import org.enon.element.Element;
import org.enon.element.ListElement;
import org.enon.element.MapElement;
import org.enon.element.MapEntryElement;
import org.enon.element.NumberElement;
import org.enon.element.StringElement;
import org.enon.element.TemporalElement;
import org.enon.element.array.ByteArrayElement;
import org.enon.element.array.IntArrayElement;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class JsonWriterContextTest {
  private static final Logger LOGGER = LoggerFactory.getLogger(JsonWriterContextTest.class.getName());

  
  public JsonWriterContextTest() {
  }
  
  @BeforeAll
  public static void setUpClass() {
  }
  
  @AfterAll
  public static void tearDownClass() {
  }
  
  @BeforeEach
  public void setUp() {
  }
  
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of write method, of class JsonWriterContext.
   * @throws java.lang.Exception
   */
  @Test
  public void testWrite() throws Exception {
    try {
      LOGGER.info("write");
      ByteArrayOutputStream baos = new ByteArrayOutputStream();

      Element e = new StringElement("test string");
      JsonWriterContext instance = new JsonWriterContext(baos);
      instance.write(e);
      assertEquals("\"test string\"", new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      e = new NumberElement("1234567.89");
      instance = new JsonWriterContext(baos);
      instance.write(e);
      assertEquals("1234567.89", new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      e = ConstantElement.falseInstance();
      instance = new JsonWriterContext(baos);
      instance.write(e);
      assertEquals("false", new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      e = ConstantElement.trueInstance();
      instance = new JsonWriterContext(baos);
      instance.write(e);
      assertEquals("true", new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      e = ConstantElement.falseInstance();
      instance = new JsonWriterContext(baos);
      instance.write(e);
      assertEquals("false", new String(baos.toByteArray(), StandardCharsets.UTF_8)); 

      baos.reset();
      e = new DoubleElement(555.333);
      instance = new JsonWriterContext(baos);
      instance.write(e);
      assertEquals("555.333", new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      e = new IntArrayElement(new int[]{555,333,-127});
      instance = new JsonWriterContext(baos);
      instance.write(e);
      assertEquals("[555,333,-127]", new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      String test = "b64 encode the bytes of this string";
      e = new ByteArrayElement(test.getBytes(StandardCharsets.UTF_8));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      String encoded = Base64.getEncoder().encodeToString(test.getBytes(StandardCharsets.UTF_8));
      assertEquals('"'+encoded+'"', new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      Instant instant = Instant.now();
      e = TemporalElement.create(TemporalVoFactory.getInstance().create(instant));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      String result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals('"'+instant.toString()+'"', result);

      baos.reset();
      LocalDate ld = LocalDate.of(2020, Month.MARCH, 15);
      e = TemporalElement.create(TemporalVoFactory.getInstance().create(ld));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals('"'+ld.toString()+'"', new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      LocalTime lt = LocalTime.of(3, 19, 15);
      e = TemporalElement.create(TemporalVoFactory.getInstance().create(lt));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals('"'+lt.toString()+'"', new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      lt = LocalTime.of(23, 19, 15).plusNanos(140000000);
      e = TemporalElement.create(TemporalVoFactory.getInstance().create(lt));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals('"'+lt.toString()+'"', new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      lt = LocalTime.of(23, 19, 15).plusNanos(140909000);
      e = TemporalElement.create(TemporalVoFactory.getInstance().create(lt));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals('"'+lt.toString()+'"', new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      LocalDateTime ldt = LocalDateTime.of(ld, lt);
      e = TemporalElement.create(TemporalVoFactory.getInstance().create(ldt));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals('"'+ldt.toString()+'"', new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      OffsetDateTime odt = OffsetDateTime.of(ld, lt, ZoneOffset.ofHoursMinutes(-4, -30));
      e = TemporalElement.create(TemporalVoFactory.getInstance().create(odt));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals('"'+odt.toString()+'"', new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      odt = OffsetDateTime.of(ld, lt, ZoneOffset.UTC);
      e = TemporalElement.create(TemporalVoFactory.getInstance().create(odt));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals('"'+odt.toString()+'"', new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      ZonedDateTime zdt = ZonedDateTime.of(ld, lt, ZoneId.of("Asia/Tokyo"));
      e = TemporalElement.create(TemporalVoFactory.getInstance().create(zdt));
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals('"'+zdt.toString()+'"', new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      List<Element> elements = new ArrayList<>();
      elements.add(new StringElement("array entry"));
      elements.add(ConstantElement.falseInstance());
      elements.add(new NumberElement(8675309));
      e = new ListElement(elements);
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals("[\"array entry\",false,8675309]", new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.reset();
      List<MapEntryElement<?,?>> netries = new ArrayList<>();
      netries.add(new MapEntryElement<>(new StringElement("key1"), new NumberElement(555)));
      netries.add(new MapEntryElement<>(new StringElement("bool"), ConstantElement.falseInstance()));
      e = new MapElement(netries);
      instance = new JsonWriterContext(baos);
      instance.write(e);
      result = new String(baos.toByteArray(), StandardCharsets.UTF_8);
      LOGGER.info(result);
      assertEquals("{\"key1\":555,\"bool\":false}", new String(baos.toByteArray(), StandardCharsets.UTF_8));

      baos.close();    
    } catch (Exception x) {
      LOGGER.error(x.getMessage(), x);
      throw x;
    }
  }

  @Test
  public void testEscapeJsonString() throws IOException {
    LOGGER.info("escapeJsonString");
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    JsonWriterContext instance = new JsonWriterContext(baos);
    instance.write(new StringElement("Contains \"quotes\""));
    assertEquals("\"Contains \\\"quotes\\\"\"", baos.toString());
    
    baos.reset();
    instance.write(new StringElement("Contains \t tab and \n newline"));
    assertEquals("\"Contains \\t tab and \\n newline\"", baos.toString());
    
    baos.reset();
    instance.write(new StringElement("Contains \\ backslash & \r CR"));
    assertEquals("\"Contains \\\\ backslash & \\r CR\"", baos.toString());

    baos.close();
  }

  /**
   * Test of writeProlog method, of class JsonWriterContext.
   * @throws java.io.IOException
   */
  @Test
  public void testWriteProlog() throws IOException {
    LOGGER.info("writeProlog");
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    JsonWriterContext instance = new JsonWriterContext(baos);
    instance.writeProlog();
    assertEquals(0, baos.size());
    baos.close();
  }
  
}
