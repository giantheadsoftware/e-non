/*
 * Copyright (c) 2020-2020 Giant Head Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.enon.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class JsonWriterTest {
  private static final Logger LOGGER = LoggerFactory.getLogger(JsonWriterTest.class.getName());

  
  public JsonWriterTest() {
  }
  
  @BeforeAll
  public static void setUpClass() {
  }
  
  @AfterAll
  public static void tearDownClass() {
  }
  
  @BeforeEach
  public void setUp() {
  }
  
  @AfterEach
  public void tearDown() {
  }

  @Test
  public void testWrite() throws IOException {
    LOGGER.info("write");
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    
    JsonWriter jsonWriter = new JsonWriter(baos);
    
    jsonWriter.write("hello JSON");
    assertEquals("\"hello JSON\"", baos.toString());
    
    baos.reset();
    jsonWriter.write(new String[]{"one", "two", "three"});
    assertEquals("[\"one\",\"two\",\"three\"]", baos.toString());
    
    baos.close();
  }
  
}
