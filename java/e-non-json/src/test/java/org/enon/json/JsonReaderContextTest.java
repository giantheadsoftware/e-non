/*
 */
package org.enon.json;

import java.io.ByteArrayInputStream;
import org.enon.Prolog;
import org.enon.element.Element;
import org.enon.element.ElementType;
import org.enon.element.ListElement;
import org.enon.element.MapElement;
import org.enon.element.NumberElement;
import org.enon.element.StringElement;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author clininger
 */
public class JsonReaderContextTest {
  private static final Logger LOGGER = LoggerFactory.getLogger(JsonReaderContextTest.class.getName());

  
  public JsonReaderContextTest() {
  }
  
  @BeforeAll
  public static void setUpClass() {
  }
  
  @AfterAll
  public static void tearDownClass() {
  }
  
  @BeforeEach
  public void setUp() {
  }
  
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of readProlog method, of class JsonReaderContext.
   */
  @Test
  public void testReadProlog() {
    LOGGER.info("readProlog");
    JsonReaderContext instance = new JsonReaderContext(new ByteArrayInputStream(new byte[0]));
    Prolog result = instance.readProlog();
    assertTrue(result.getRequiredFeatures().isEmpty());
    assertTrue(instance.getRequiredFeatures().isEmpty());
  }

  /**
   * Test of parseNextElement method, of class JsonReaderContext.
   */
  @Test
  public void testParseNextElement() throws Exception {
    LOGGER.info("parseNextElement");
    JsonReaderContext ctx = new JsonReaderContext(getClass().getResourceAsStream("/test1.json"));
    Element e = ctx.parseNextElement();
    assertNotNull(e);
    LOGGER.info("Element Type 1: {}", e.getType().name());
    assertSame(ElementType.MAP_ELEMENT, e.getType());
    assertEquals(1, ((MapElement)e).getSize().getSize());


    ctx = new JsonReaderContext(getClass().getResourceAsStream("/test2.json"));
    e = ctx.parseNextElement();
    assertNotNull(e);
    LOGGER.info("Element Type 2: {}", e.getType().name());
    assertSame(ElementType.STRING_ELEMENT, e.getType());
    assertEquals("just a string", ((StringElement)e).getValue());


    ctx = new JsonReaderContext(getClass().getResourceAsStream("/test3.json"));
    e = ctx.parseNextElement();
    assertNotNull(e);
    LOGGER.info("Element Type 3: {}", e.getType().name());
    assertSame(ElementType.NULL_ELEMENT, e.getType());


    ctx = new JsonReaderContext(getClass().getResourceAsStream("/test4.json"));
    e = ctx.parseNextElement();
    assertNotNull(e);
    LOGGER.info("Element Type 4: {}", e.getType().name());
    assertSame(ElementType.LIST_ELEMENT, e.getType());
    assertEquals(8, ((ListElement)e).getSize().getSize());


    ctx = new JsonReaderContext(getClass().getResourceAsStream("/test5.json"));
    e = ctx.parseNextElement();
    assertNotNull(e);
    LOGGER.info("Element Type 5: {}", e.getType().name());
    assertSame(ElementType.MAP_ELEMENT, e.getType());
    assertEquals(5, ((MapElement)e).getSize().getSize());


    ctx = new JsonReaderContext(getClass().getResourceAsStream("/test6.json"));
    e = ctx.parseNextElement();
    assertNotNull(e);
    LOGGER.info("Element Type 6: {}", e.getType().name());
    assertSame(ElementType.NUMBER_ELEMENT, e.getType());
    assertEquals("1234567.89", ((NumberElement)e).getValue());

  }
  
}
